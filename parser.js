const data = `
Region I (Ilocos Region)	Reg
Ilocos Norte	Prov
Adams	Mun
Adams (Pob.)	Bgy
Bacarra	Mun
Bani	Bgy
Buyon	Bgy
Cabaruan	Bgy
Cabulalaan	Bgy
Cabusligan	Bgy
Cadaratan	Bgy
Calioet-Libong	Bgy
Casilian	Bgy
Corocor	Bgy
Duripes	Bgy
Ganagan	Bgy
Libtong	Bgy
Macupit	Bgy
Nambaran	Bgy
Natba	Bgy
Paninaan	Bgy
Pasiocan	Bgy
Pasngal	Bgy
Pipias	Bgy
Pulangi	Bgy
Pungto	Bgy
San Agustin I (Pob.)	Bgy
San Agustin II (Pob.)	Bgy
San Andres I (Pob.)	Bgy
San Andres II (Pob.)	Bgy
San Gabriel I (Pob.)	Bgy
San Gabriel II (Pob.)	Bgy
San Pedro I (Pob.)	Bgy
San Pedro II (Pob.)	Bgy
San Roque I (Pob.)	Bgy
San Roque II (Pob.)	Bgy
San Simon I (Pob.)	Bgy
San Simon II (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Sangil	Bgy
Santa Filomena I (Pob.)	Bgy
Santa Filomena II (Pob.)	Bgy
Santa Rita (Pob.)	Bgy
Santo Cristo I (Pob.)	Bgy
Santo Cristo II (Pob.)	Bgy
Tambidao	Bgy
Teppang	Bgy
Tubburan	Bgy
Badoc	Mun
Alay-Nangbabaan	Bgy
Alogoog	Bgy
Ar-arusip	Bgy
Aring	Bgy
Balbaldez	Bgy
Bato	Bgy
Camanga	Bgy
Canaan (Pob.)	Bgy
Caraitan	Bgy
Gabut Norte	Bgy
Gabut Sur	Bgy
Garreta (Pob.)	Bgy
Labut	Bgy
Lacuben	Bgy
Lubigan	Bgy
Mabusag Norte	Bgy
Mabusag Sur	Bgy
Madupayas	Bgy
Morong	Bgy
Nagrebcan	Bgy
Napu	Bgy
La Virgen Milagrosa	Bgy
Pagsanahan Norte	Bgy
Pagsanahan Sur	Bgy
Paltit	Bgy
Parang	Bgy
Pasuc	Bgy
Santa Cruz Norte	Bgy
Santa Cruz Sur	Bgy
Saud	Bgy
Turod	Bgy
Bangui	Mun
Abaca	Bgy
Bacsil	Bgy
Banban	Bgy
Baruyen	Bgy
Dadaor	Bgy
Lanao	Bgy
Malasin	Bgy
Manayon	Bgy
Masikil	Bgy
Nagbalagan	Bgy
Payac	Bgy
San Lorenzo (Pob.)	Bgy
Taguiporo	Bgy
Utol	Bgy
City of Batac	City
Aglipay (Pob.)	Bgy
Baay	Bgy
Baligat	Bgy
Bungon	Bgy
Baoa East	Bgy
Baoa West	Bgy
Barani (Pob.)	Bgy
Ben-agan (Pob.)	Bgy
Bil-loca	Bgy
Biningan	Bgy
Callaguip (Pob.)	Bgy
Camandingan	Bgy
Camguidan	Bgy
Cangrunaan (Pob.)	Bgy
Capacuan	Bgy
Caunayan (Pob.)	Bgy
Valdez Pob.	Bgy
Colo	Bgy
Pimentel	Bgy
Dariwdiw	Bgy
Acosta Pob.	Bgy
Ablan Pob.	Bgy
Lacub (Pob.)	Bgy
Mabaleng	Bgy
Magnuang	Bgy
Maipalig	Bgy
Nagbacalan	Bgy
Naguirangan	Bgy
Ricarte Pob.	Bgy
Palongpong	Bgy
Palpalicong (Pob.)	Bgy
Parangopong	Bgy
Payao	Bgy
Quiling Norte	Bgy
Quiling Sur	Bgy
Quiom	Bgy
Rayuray	Bgy
San Julian (Pob.)	Bgy
San Mateo	Bgy
San Pedro	Bgy
Suabit (Pob.)	Bgy
Sumader	Bgy
Tabug	Bgy
Burgos	Mun
Ablan Sarat	Bgy
Agaga	Bgy
Bayog	Bgy
Bobon	Bgy
Buduan	Bgy
Nagsurot	Bgy
Paayas	Bgy
Pagali	Bgy
Poblacion	Bgy
Saoit	Bgy
Tanap	Bgy
Carasi	Mun
Angset	Bgy
Barbaqueso (Pob.)	Bgy
Virbira	Bgy
Currimao	Mun
Anggapang Norte	Bgy
Anggapang Sur	Bgy
Bimmanga	Bgy
Cabuusan	Bgy
Comcomloong	Bgy
Gaang	Bgy
Lang-ayan-Baramban	Bgy
Lioes	Bgy
Maglaoi Centro	Bgy
Maglaoi Norte	Bgy
Maglaoi Sur	Bgy
Paguludan-Salindeg	Bgy
Pangil	Bgy
Pias Norte	Bgy
Pias Sur	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Salugan	Bgy
San Simeon	Bgy
Santa Cruz	Bgy
Tapao-Tigue	Bgy
Torre	Bgy
Victoria	Bgy
Dingras	Mun
Albano (Pob.)	Bgy
Bacsil	Bgy
Bagut	Bgy
Parado	Bgy
Baresbes	Bgy
Barong	Bgy
Bungcag	Bgy
Cali	Bgy
Capasan	Bgy
Dancel (Pob.)	Bgy
Foz	Bgy
Guerrero (Pob.)	Bgy
Lanas	Bgy
Lumbad	Bgy
Madamba (Pob.)	Bgy
Mandaloque	Bgy
Medina	Bgy
Ver	Bgy
San Marcelino	Bgy
Puruganan (Pob.)	Bgy
Peralta (Pob.)	Bgy
Root	Bgy
Sagpatan	Bgy
Saludares	Bgy
San Esteban	Bgy
Espiritu	Bgy
Sulquiano	Bgy
San Francisco	Bgy
Suyo	Bgy
San Marcos	Bgy
Elizabeth	Bgy
Dumalneg	Mun
Cabaritan	Bgy
San Isidro	Bgy
Kalaw	Bgy
Quibel	Bgy
Banna	Mun
Balioeg	Bgy
Bangsar	Bgy
Barbarangay	Bgy
Bomitog	Bgy
Bugasi	Bgy
Caestebanan	Bgy
Caribquib	Bgy
Catagtaguen	Bgy
Crispina	Bgy
Hilario (Pob.)	Bgy
Imelda	Bgy
Lorenzo (Pob.)	Bgy
Macayepyep	Bgy
Marcos (Pob.)	Bgy
Nagpatayan	Bgy
Valdez	Bgy
Sinamar	Bgy
Tabtabagan	Bgy
Valenciano (Pob.)	Bgy
Binacag	Bgy
City of Laoag (Capital)	City
Bgy. No. 42, Apaya	Bgy
Bgy. No. 36, Araniw	Bgy
Bgy. No. 56-A, Bacsil North	Bgy
Bgy. No. 56-B, Bacsil South	Bgy
Bgy. No. 41, Balacad	Bgy
Bgy. No. 40, Balatong	Bgy
Bgy. No. 55-A, Barit-Pandan	Bgy
Bgy. No. 47, Bengcag	Bgy
Bgy. No. 50, Buttong	Bgy
Bgy. No. 60-A, Caaoacan	Bgy
Bry. No. 48-A, Cabungaan North	Bgy
Bgy. No. 48-B, Cabungaan South	Bgy
Bgy. No. 37, Calayab	Bgy
Bgy. No. 54-B, Camangaan	Bgy
Bgy. No. 58, Casili	Bgy
Bgy. No. 61, Cataban	Bgy
Bgy. No. 43, Cavit	Bgy
Bgy. No. 49-A, Darayday	Bgy
Bgy. No. 59-B, Dibua North	Bgy
Bgy. No. 59-A, Dibua South	Bgy
Bgy. No. 34-B, Gabu Norte East	Bgy
Bgy. No. 34-A, Gabu Norte West	Bgy
Bgy. No. 35, Gabu Sur	Bgy
Bgy. No. 32-C La Paz East	Bgy
Bgy. No. 33-B, La Paz Proper	Bgy
Bgy. No. 32-B, La Paz West	Bgy
Bgy. No. 54-A, Lagui-Sail	Bgy
Bgy. No. 32-A, La Paz East	Bgy
Bgy. No. 33-A, La Paz Proper	Bgy
Bgy. No. 52-B, Lataag	Bgy
Bgy. No. 60-B, Madiladig	Bgy
Bgy. No. 38-A, Mangato East	Bgy
Bgy. No. 38-B, Mangato West	Bgy
Bgy. No. 62-A, Navotas North	Bgy
Bgy. No. 62-B, Navotas South	Bgy
Bgy. No. 46, Nalbo	Bgy
Bgy. No. 51-A, Nangalisan East	Bgy
Bgy. No. 51-B, Nangalisan West	Bgy
Bgy. No. 24, Nstra. Sra. De Consolacion (Pob.)	Bgy
Bgy. No. 7-A, Nstra. Sra. De Natividad (Pob.)	Bgy
Bgy. No. 7-B, Nstra. Sra. De Natividad (Pob.)	Bgy
Bgy. No. 27, Nstra. Sra. De Soledad (Pob.)	Bgy
Bgy. No. 13, Nstra. Sra. De Visitacion (Pob.)	Bgy
Bgy. No. 3, Nstra. Sra. Del Rosario (Pob.)	Bgy
Bgy. No. 57, Pila	Bgy
Bgy. No. 49-B, Raraburan	Bgy
Bgy. No. 53, Rioeng	Bgy
Bgy. No. 55-B, Salet-Bulangon	Bgy
Bgy. No. 6, San Agustin (Pob.)	Bgy
Bgy. No. 22, San Andres (Pob.)	Bgy
Bgy. No. 28, San Bernardo (Pob.)	Bgy
Bgy. No. 17, San Francisco (Pob.)	Bgy
Bgy. No. 4, San Guillermo (Pob.)	Bgy
Bgy. No. 15, San Guillermo (Pob.)	Bgy
Bgy. No. 12, San Isidro (Pob.)	Bgy
Bgy. No. 16, San Jacinto (Pob.)	Bgy
Bgy. No. 10, San Jose (Pob.)	Bgy
Bgy. No. 1, San Lorenzo (Pob.)	Bgy
Bgy. No. 26, San Marcelino (Pob.)	Bgy
Bgy. No. 52-A, San Mateo	Bgy
Bgy. No. 23, San Matias (Pob.)	Bgy
Bgy. No. 20, San Miguel (Pob.)	Bgy
Bgy. No. 21, San Pedro (Pob.)	Bgy
Bgy. No. 5, San Pedro (Pob.)	Bgy
Bry. No. 18, San Quirino (Pob.)	Bgy
Bgy. No. 8, San Vicente (Pob.)	Bgy
Bgy. No. 9, Santa Angela (Pob.)	Bgy
Bgy. No. 11, Santa Balbina (Pob.)	Bgy
Bgy. No. 25, Santa Cayetana (Pob.)	Bgy
Bgy. No. 2, Santa Joaquina (Pob.)	Bgy
Bgy. No. 19, Santa Marcela (Pob.)	Bgy
Bgy. No. 30-B, Santa Maria	Bgy
Bgy. No. 39, Santa Rosa	Bgy
Bgy. No. 14, Santo Tomas (Pob.)	Bgy
Bgy. No. 29, Santo Tomas (Pob.)	Bgy
Bgy. No. 30-A, Suyo	Bgy
Bgy. No. 31, Talingaan	Bgy
Bgy. No. 45, Tangid	Bgy
Bgy. No. 55-C, Vira	Bgy
Bgy. No. 44, Zamboanga	Bgy
Marcos	Mun
Pacifico	Bgy
Imelda	Bgy
Elizabeth	Bgy
Daquioag	Bgy
Escoda	Bgy
Ferdinand	Bgy
Fortuna	Bgy
Lydia (Pob.)	Bgy
Mabuti	Bgy
Valdez	Bgy
Tabucbuc	Bgy
Santiago	Bgy
Cacafean	Bgy
Nueva Era	Mun
Acnam	Bgy
Barangobong	Bgy
Barikir	Bgy
Bugayong	Bgy
Cabittauran	Bgy
Caray	Bgy
Garnaden	Bgy
Naguillan	Bgy
Poblacion	Bgy
Santo Niño	Bgy
Uguis	Bgy
Pagudpud	Mun
Aggasi	Bgy
Baduang	Bgy
Balaoi	Bgy
Burayoc	Bgy
Caunayan	Bgy
Dampig	Bgy
Ligaya	Bgy
Pancian	Bgy
Pasaleng	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Saguigui	Bgy
Saud	Bgy
Subec	Bgy
Tarrag	Bgy
Caparispisan	Bgy
Paoay	Mun
Bacsil	Bgy
Cabagoan	Bgy
Cabangaran	Bgy
Callaguip	Bgy
Cayubog	Bgy
Dolores	Bgy
Laoa	Bgy
Masintoc	Bgy
Monte	Bgy
Mumulaan	Bgy
Nagbacalan	Bgy
Nalasin	Bgy
Nanguyudan	Bgy
Oaig-Upay-Abulao	Bgy
Pambaran	Bgy
Pannaratan (Pob.)	Bgy
Paratong	Bgy
Pasil	Bgy
Salbang (Pob.)	Bgy
San Agustin	Bgy
San Blas (Pob.)	Bgy
San Juan	Bgy
San Pedro	Bgy
San Roque (Pob.)	Bgy
Sangladan Pob.	Bgy
Santa Rita (Pob.)	Bgy
Sideg	Bgy
Suba	Bgy
Sungadan	Bgy
Surgui	Bgy
Veronica	Bgy
Pasuquin	Mun
Batuli	Bgy
Binsang	Bgy
Nalvo	Bgy
Caruan	Bgy
Carusikis	Bgy
Carusipan	Bgy
Dadaeman	Bgy
Darupidip	Bgy
Davila	Bgy
Dilanis	Bgy
Dilavo	Bgy
Estancia	Bgy
Naglicuan	Bgy
Nagsanga	Bgy
Ngabangab	Bgy
Pangil	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Pragata	Bgy
Puyupuyan	Bgy
Sulongan	Bgy
Salpad	Bgy
San Juan	Bgy
Santa Catalina	Bgy
Santa Matilde	Bgy
Sapat	Bgy
Sulbec	Bgy
Surong	Bgy
Susugaen	Bgy
Tabungao	Bgy
Tadao	Bgy
Piddig	Mun
Ab-abut	Bgy
Abucay	Bgy
Anao (Pob.)	Bgy
Arua-ay	Bgy
Bimmanga	Bgy
Boyboy	Bgy
Cabaroan (Pob.)	Bgy
Calambeg	Bgy
Callusa	Bgy
Dupitac	Bgy
Estancia	Bgy
Gayamat	Bgy
Lagandit	Bgy
Libnaoan	Bgy
Loing (Pob.)	Bgy
Maab-abaca	Bgy
Mangitayag	Bgy
Maruaya	Bgy
San Antonio	Bgy
Santa Maria	Bgy
Sucsuquen	Bgy
Tangaoan	Bgy
Tonoton	Bgy
Pinili	Mun
Aglipay	Bgy
Apatut-Lubong	Bgy
Badio	Bgy
Barbar	Bgy
Buanga	Bgy
Bulbulala	Bgy
Bungro	Bgy
Cabaroan	Bgy
Capangdanan	Bgy
Dalayap	Bgy
Darat	Bgy
Gulpeng	Bgy
Liliputen	Bgy
Lumbaan-Bicbica	Bgy
Nagtrigoan	Bgy
Pagdilao (Pob.)	Bgy
Pugaoan	Bgy
Puritac	Bgy
Sacritan	Bgy
Salanap	Bgy
Santo Tomas	Bgy
Tartarabang	Bgy
Puzol	Bgy
Upon	Bgy
Valbuena (Pob.)	Bgy
San Nicolas	Mun
San Francisco (Pob.)	Bgy
San Ildefonso (Pob.)	Bgy
San Agustin	Bgy
San Baltazar (Pob.)	Bgy
San Bartolome (Pob.)	Bgy
San Cayetano (Pob.)	Bgy
San Eugenio (Pob.)	Bgy
San Fernando (Pob.)	Bgy
San Gregorio (Pob.)	Bgy
San Guillermo	Bgy
San Jose (Pob.)	Bgy
San Juan Bautista (Pob.)	Bgy
San Lorenzo	Bgy
San Lucas (Pob.)	Bgy
San Marcos	Bgy
San Miguel (Pob.)	Bgy
San Pablo	Bgy
San Paulo (Pob.)	Bgy
San Pedro	Bgy
San Rufino (Pob.)	Bgy
San Silvestre (Pob.)	Bgy
Santa Asuncion	Bgy
Santa Cecilia	Bgy
Santa Monica	Bgy
Sarrat	Mun
San Agustin (Pob.)	Bgy
San Andres	Bgy
San Antonio	Bgy
San Bernabe	Bgy
San Cristobal	Bgy
San Felipe	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
San Joaquin (Pob.)	Bgy
San Jose	Bgy
San Juan	Bgy
San Leandro (Pob.)	Bgy
San Lorenzo	Bgy
San Manuel	Bgy
San Marcos	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Roque	Bgy
San Vicente (Pob.)	Bgy
Santa Barbara (Pob.)	Bgy
Santa Magdalena	Bgy
Santa Rosa	Bgy
Santo Santiago	Bgy
Santo Tomas	Bgy
Solsona	Mun
Aguitap	Bgy
Bagbag	Bgy
Bagbago	Bgy
Barcelona	Bgy
Bubuos	Bgy
Capurictan	Bgy
Catangraran	Bgy
Darasdas	Bgy
Juan (Pob.)	Bgy
Laureta (Pob.)	Bgy
Lipay	Bgy
Maananteng	Bgy
Manalpac	Bgy
Mariquet	Bgy
Nagpatpatan	Bgy
Nalasin	Bgy
Puttao	Bgy
San Juan	Bgy
San Julian	Bgy
Santa Ana	Bgy
Santiago	Bgy
Talugtog	Bgy
Vintar	Mun
Abkir	Bgy
Alsem	Bgy
Bago	Bgy
Bulbulala	Bgy
Cabangaran	Bgy
Cabayo	Bgy
Cabisocolan	Bgy
Canaam	Bgy
Columbia	Bgy
Dagupan	Bgy
Pedro F. Alviar	Bgy
Dipilat	Bgy
Esperanza	Bgy
Ester	Bgy
Isic Isic	Bgy
Lubnac	Bgy
Mabanbanag	Bgy
Alejo Malasig	Bgy
Manarang	Bgy
Margaay	Bgy
Namoroc	Bgy
Malampa	Bgy
Parparoroc	Bgy
Parut	Bgy
Salsalamagui	Bgy
San Jose	Bgy
San Nicolas (Pob.)	Bgy
San Pedro (Pob.)	Bgy
San Ramon (Pob.)	Bgy
San Roque (Pob.)	Bgy
Santa Maria (Pob.)	Bgy
Tamdagan	Bgy
Visaya	Bgy
Ilocos Sur	Prov
Alilem	Mun
Alilem Daya (Pob.)	Bgy
Amilongan	Bgy
Anaao	Bgy
Apang	Bgy
Apaya	Bgy
Batbato	Bgy
Daddaay	Bgy
Dalawa	Bgy
Kiat	Bgy
Banayoyo	Mun
Bagbagotot	Bgy
Banbanaal	Bgy
Bisangol	Bgy
Cadanglaan	Bgy
Casilagan Norte	Bgy
Casilagan Sur	Bgy
Elefante	Bgy
Guardia	Bgy
Lintic	Bgy
Lopez	Bgy
Montero	Bgy
Naguimba	Bgy
Pila	Bgy
Poblacion	Bgy
Bantay	Mun
Aggay	Bgy
An-annam	Bgy
Balaleng	Bgy
Banaoang	Bgy
Bulag	Bgy
Buquig	Bgy
Cabalanggan	Bgy
Cabaroan	Bgy
Cabusligan	Bgy
Capangdanan	Bgy
Guimod	Bgy
Lingsat	Bgy
Malingeb	Bgy
Mira	Bgy
Naguiddayan	Bgy
Ora	Bgy
Paing	Bgy
Puspus	Bgy
Quimmarayan	Bgy
Sagneb	Bgy
Sagpat	Bgy
San Mariano	Bgy
San Isidro	Bgy
San Julian	Bgy
Sinabaan	Bgy
Taguiporo	Bgy
Taleb	Bgy
Tay-ac	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Burgos	Mun
Ambugat	Bgy
Balugang	Bgy
Bangbangar	Bgy
Bessang	Bgy
Cabcaburao	Bgy
Cadacad	Bgy
Callitong	Bgy
Dayanki	Bgy
Lesseb	Bgy
Lubing	Bgy
Lucaban	Bgy
Luna	Bgy
Macaoayan	Bgy
Mambug	Bgy
Manaboc	Bgy
Mapanit	Bgy
Poblacion Sur	Bgy
Nagpanaoan	Bgy
Dirdirig	Bgy
Paduros	Bgy
Patac	Bgy
Poblacion Norte	Bgy
Sabangan Pinggan	Bgy
Subadi Norte	Bgy
Subadi Sur	Bgy
Taliao	Bgy
Cabugao	Mun
Alinaay	Bgy
Aragan	Bgy
Arnap	Bgy
Baclig (Pob.)	Bgy
Bato	Bgy
Bonifacio (Pob.)	Bgy
Bungro	Bgy
Cacadiran	Bgy
Caellayan	Bgy
Carusipan	Bgy
Catucdaan	Bgy
Cuancabal	Bgy
Cuantacla	Bgy
Daclapan	Bgy
Dardarat	Bgy
Lipit	Bgy
Maradodon	Bgy
Margaay	Bgy
Nagsantaan	Bgy
Nagsincaoan	Bgy
Namruangan	Bgy
Pila	Bgy
Pug-os	Bgy
Quezon (Pob.)	Bgy
Reppaac	Bgy
Rizal (Pob.)	Bgy
Sabang	Bgy
Sagayaden	Bgy
Salapasap	Bgy
Salomague	Bgy
Sisim	Bgy
Turod	Bgy
Turod-Patac	Bgy
City of Candon	City
Allangigan Primero	Bgy
Allangigan Segundo	Bgy
Amguid	Bgy
Ayudante	Bgy
Bagani Camposanto	Bgy
Bagani Gabor	Bgy
Bagani Tocgo	Bgy
Bagani Ubbog	Bgy
Bagar	Bgy
Balingaoan	Bgy
Bugnay	Bgy
Calaoaan	Bgy
Calongbuyan	Bgy
Caterman	Bgy
Cubcubboot	Bgy
Darapidap	Bgy
Langlangca Primero	Bgy
Langlangca Segundo	Bgy
Oaig-Daya	Bgy
Palacapac	Bgy
Paras	Bgy
Parioc Primero	Bgy
Parioc Segundo	Bgy
Patpata Primero	Bgy
Patpata Segundo	Bgy
Paypayad	Bgy
Salvador Primero	Bgy
Salvador Segundo	Bgy
San Agustin	Bgy
San Andres	Bgy
San Antonio (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Jose (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Nicolas	Bgy
San Pedro	Bgy
Santo Tomas	Bgy
Tablac	Bgy
Talogtog	Bgy
Tamurong Primero	Bgy
Tamurong Segundo	Bgy
Villarica	Bgy
Caoayan	Mun
Anonang Mayor	Bgy
Anonang Menor	Bgy
Baggoc	Bgy
Callaguip	Bgy
Caparacadan	Bgy
Fuerte	Bgy
Manangat	Bgy
Naguilian	Bgy
Nansuagao	Bgy
Pandan	Bgy
Pantay-Quitiquit	Bgy
Don Dimas Querubin (Pob.)	Bgy
Puro	Bgy
Pantay Tamurong	Bgy
Villamar	Bgy
Don Alejandro Quirolgico (Pob.)	Bgy
Don Lorenzo Querubin (Pob.)	Bgy
Cervantes	Mun
Aluling	Bgy
Comillas North	Bgy
Comillas South	Bgy
Concepcion (Pob.)	Bgy
Dinwede East	Bgy
Dinwede West	Bgy
Libang	Bgy
Pilipil	Bgy
Remedios	Bgy
Rosario (Pob.)	Bgy
San Juan	Bgy
San Luis	Bgy
Malaya	Bgy
Galimuyod	Mun
Abaya	Bgy
Baracbac	Bgy
Bidbiday	Bgy
Bitong	Bgy
Borobor	Bgy
Calimugtong	Bgy
Calongbuyan	Bgy
Calumbaya	Bgy
Daldagan	Bgy
Kilang	Bgy
Legaspi	Bgy
Mabayag	Bgy
Matanubong	Bgy
Mckinley	Bgy
Nagsingcaoan	Bgy
Oaig-Daya	Bgy
Pagangpang	Bgy
Patac	Bgy
Poblacion	Bgy
Rubio	Bgy
Sabangan-Bato	Bgy
Sacaang	Bgy
San Vicente	Bgy
Sapang	Bgy
Gregorio del Pilar	Mun
Alfonso	Bgy
Bussot	Bgy
Concepcion	Bgy
Dapdappig	Bgy
Matue-Butarag	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Lidlidda	Mun
Banucal	Bgy
Bequi-Walin	Bgy
Bugui	Bgy
Calungbuyan	Bgy
Carcarabasa	Bgy
Labut	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
San Vicente	Bgy
Suysuyan	Bgy
Tay-ac	Bgy
Magsingal	Mun
Alangan	Bgy
Bacar	Bgy
Barbarit	Bgy
Bungro	Bgy
Cabaroan	Bgy
Cadanglaan	Bgy
Caraisan	Bgy
Dacutan	Bgy
Labut	Bgy
Maas-asin	Bgy
Macatcatud	Bgy
Namalpalan	Bgy
Manzante	Bgy
Maratudo	Bgy
Miramar	Bgy
Napo	Bgy
Pagsanaan Norte	Bgy
Pagsanaan Sur	Bgy
Panay Norte	Bgy
Panay Sur	Bgy
Patong	Bgy
Puro	Bgy
San Basilio (Pob.)	Bgy
San Clemente (Pob.)	Bgy
San Julian (Pob.)	Bgy
San Lucas (Pob.)	Bgy
San Ramon (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santa Monica	Bgy
Sarsaracat	Bgy
Nagbukel	Mun
Balaweg	Bgy
Bandril	Bgy
Bantugo	Bgy
Cadacad	Bgy
Casilagan	Bgy
Casocos	Bgy
Lapting	Bgy
Mapisi	Bgy
Mission	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Taleb	Bgy
Narvacan	Mun
Abuor	Bgy
Ambulogan	Bgy
Aquib	Bgy
Banglayan	Bgy
Bulanos	Bgy
Cadacad	Bgy
Cagayungan	Bgy
Camarao	Bgy
Casilagan	Bgy
Codoog	Bgy
Dasay	Bgy
Dinalaoan	Bgy
Estancia	Bgy
Lanipao	Bgy
Lungog	Bgy
Margaay	Bgy
Marozo	Bgy
Naguneg	Bgy
Orence	Bgy
Pantoc	Bgy
Paratong	Bgy
Parparia	Bgy
Quinarayan	Bgy
Rivadavia	Bgy
San Antonio	Bgy
San Jose (Pob.)	Bgy
San Pablo	Bgy
San Pedro	Bgy
Santa Lucia (Pob.)	Bgy
Sarmingan	Bgy
Sucoc	Bgy
Sulvec	Bgy
Turod	Bgy
Bantay Abot	Bgy
Quirino	Mun
Banoen	Bgy
Cayus	Bgy
Patungcaleo	Bgy
Malideg	Bgy
Namitpit	Bgy
Patiacan	Bgy
Legleg (Pob.)	Bgy
Suagayan	Bgy
Lamag	Bgy
Salcedo	Mun
Atabay	Bgy
Calangcuasan	Bgy
Balidbid	Bgy
Baluarte	Bgy
Baybayading	Bgy
Boguibog	Bgy
Bulala-Leguey	Bgy
Kaliwakiw	Bgy
Culiong	Bgy
Dinaratan	Bgy
Kinmarin	Bgy
Lucbuban	Bgy
Madarang	Bgy
Maligcong	Bgy
Pias	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
San Gaspar	Bgy
San Tiburcio	Bgy
Sorioan	Bgy
Ubbog	Bgy
San Emilio	Mun
Cabaroan (Pob.)	Bgy
Kalumsing	Bgy
Lancuas	Bgy
Matibuey	Bgy
Paltoc	Bgy
Sibsibbu	Bgy
Tiagan	Bgy
San Miliano	Bgy
San Esteban	Mun
Ansad	Bgy
Apatot	Bgy
Bateria	Bgy
Cabaroan	Bgy
Cappa-cappa	Bgy
Poblacion	Bgy
San Nicolas	Bgy
San Pablo	Bgy
San Rafael	Bgy
Villa Quirino	Bgy
San Ildefonso	Mun
Arnap	Bgy
Bahet	Bgy
Belen	Bgy
Bungro	Bgy
Busiing Sur	Bgy
Busiing Norte	Bgy
Dongalo	Bgy
Gongogong	Bgy
Iboy	Bgy
Otol-Patac	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Kinamantirisan	Bgy
Sagneb	Bgy
Sagsagat	Bgy
San Juan	Mun
Bacsil	Bgy
Baliw	Bgy
Bannuar (Pob.)	Bgy
Barbar	Bgy
Cabanglotan	Bgy
Cacandongan	Bgy
Camanggaan	Bgy
Camindoroan	Bgy
Caronoan	Bgy
Darao	Bgy
Dardarat	Bgy
Guimod Norte	Bgy
Guimod Sur	Bgy
Immayos Norte	Bgy
Immayos Sur	Bgy
Labnig	Bgy
Lapting	Bgy
Lira (Pob.)	Bgy
Malamin	Bgy
Muraya	Bgy
Nagsabaran	Bgy
Nagsupotan	Bgy
Pandayan (Pob.)	Bgy
Refaro	Bgy
Resurreccion (Pob.)	Bgy
Sabangan	Bgy
San Isidro	Bgy
Saoang	Bgy
Solotsolot	Bgy
Sunggiam	Bgy
Surngit	Bgy
Asilang	Bgy
San Vicente	Mun
Bantaoay	Bgy
Bayubay Norte	Bgy
Bayubay Sur	Bgy
Lubong	Bgy
Poblacion	Bgy
Pudoc	Bgy
San Sebastian	Bgy
Santa	Mun
Ampandula	Bgy
Banaoang	Bgy
Basug	Bgy
Bucalag	Bgy
Cabangaran	Bgy
Calungboyan	Bgy
Casiber	Bgy
Dammay	Bgy
Labut Norte	Bgy
Labut Sur	Bgy
Mabilbila Sur	Bgy
Mabilbila Norte	Bgy
Magsaysay District (Pob.)	Bgy
Manueva	Bgy
Marcos (Pob.)	Bgy
Nagpanaoan	Bgy
Namalangan	Bgy
Oribi	Bgy
Pasungol	Bgy
Quezon (Pob.)	Bgy
Quirino (Pob.)	Bgy
Rancho	Bgy
Rizal	Bgy
Sacuyya Norte	Bgy
Sacuyya Sur	Bgy
Tabucolan	Bgy
Santa Catalina	Mun
Cabaroan	Bgy
Cabittaogan	Bgy
Cabuloan	Bgy
Pangada	Bgy
Paratong	Bgy
Poblacion	Bgy
Sinabaan	Bgy
Subec	Bgy
Tamorong	Bgy
Santa Cruz	Mun
Amarao	Bgy
Babayoan	Bgy
Bacsayan	Bgy
Banay	Bgy
Bayugao Este	Bgy
Bayugao Oeste	Bgy
Besalan	Bgy
Bugbuga	Bgy
Calaoaan	Bgy
Camanggaan	Bgy
Candalican	Bgy
Capariaan	Bgy
Casilagan	Bgy
Coscosnong	Bgy
Daligan	Bgy
Dili	Bgy
Gabor Norte	Bgy
Gabor Sur	Bgy
Lalong	Bgy
Lantag	Bgy
Las-ud	Bgy
Mambog	Bgy
Mantanas	Bgy
Nagtengnga	Bgy
Padaoil	Bgy
Paratong	Bgy
Pattiqui	Bgy
Pidpid	Bgy
Pilar	Bgy
Pinipin	Bgy
Poblacion Este	Bgy
Poblacion Norte	Bgy
Poblacion Weste	Bgy
Poblacion Sur	Bgy
Quinfermin	Bgy
Quinsoriano	Bgy
Sagat	Bgy
San Antonio	Bgy
San Jose	Bgy
San Pedro	Bgy
Saoat	Bgy
Sevilla	Bgy
Sidaoen	Bgy
Suyo	Bgy
Tampugo	Bgy
Turod	Bgy
Villa Garcia	Bgy
Villa Hermosa	Bgy
Villa Laurencia	Bgy
Santa Lucia	Mun
Alincaoeg	Bgy
Angkileng	Bgy
Arangin	Bgy
Ayusan (Pob.)	Bgy
Banbanaba	Bgy
Bao-as	Bgy
Barangobong (Pob.)	Bgy
Buliclic	Bgy
Burgos (Pob.)	Bgy
Cabaritan	Bgy
Catayagan	Bgy
Conconig East	Bgy
Conconig West	Bgy
Damacuag	Bgy
Lubong	Bgy
Luba	Bgy
Nagrebcan	Bgy
Nagtablaan	Bgy
Namatican	Bgy
Nangalisan	Bgy
Palali Norte	Bgy
Palali Sur	Bgy
Paoc Norte	Bgy
Paoc Sur	Bgy
Paratong	Bgy
Pila East	Bgy
Pila West	Bgy
Quinabalayangan	Bgy
Ronda	Bgy
Sabuanan	Bgy
San Juan	Bgy
San Pedro	Bgy
Sapang	Bgy
Suagayan	Bgy
Vical	Bgy
Bani	Bgy
Santa Maria	Mun
Ag-agrao	Bgy
Ampuagan	Bgy
Baballasioan	Bgy
Baliw Daya	Bgy
Baliw Laud	Bgy
Bia-o	Bgy
Butir	Bgy
Cabaroan	Bgy
Danuman East	Bgy
Danuman West	Bgy
Dunglayan	Bgy
Gusing	Bgy
Langaoan	Bgy
Laslasong Norte	Bgy
Laslasong Sur	Bgy
Laslasong West	Bgy
Lesseb	Bgy
Lingsat	Bgy
Lubong	Bgy
Maynganay Norte	Bgy
Maynganay Sur	Bgy
Nagsayaoan	Bgy
Nagtupacan	Bgy
Nalvo	Bgy
Pacang	Bgy
Penned	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Silag	Bgy
Sumagui	Bgy
Suso	Bgy
Tangaoan	Bgy
Tinaan	Bgy
Santiago	Mun
Al-aludig	Bgy
Ambucao	Bgy
San Jose	Bgy
Baybayabas	Bgy
Bigbiga	Bgy
Bulbulala	Bgy
Busel-busel	Bgy
Butol	Bgy
Caburao	Bgy
Dan-ar	Bgy
Gabao	Bgy
Guinabang	Bgy
Imus	Bgy
Lang-ayan	Bgy
Mambug	Bgy
Nalasin	Bgy
Olo-olo Norte	Bgy
Olo-olo Sur	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Sabangan	Bgy
Salincub	Bgy
San Roque	Bgy
Ubbog	Bgy
Santo Domingo	Mun
Binalayangan	Bgy
Binongan	Bgy
Borobor	Bgy
Cabaritan	Bgy
Cabigbigaan	Bgy
Calautit	Bgy
Calay-ab	Bgy
Camestizoan	Bgy
Casili	Bgy
Flora	Bgy
Lagatit	Bgy
Laoingen	Bgy
Lussoc	Bgy
Nalasin	Bgy
Nagbettedan	Bgy
Naglaoa-an	Bgy
Nambaran	Bgy
Nanerman	Bgy
Napo	Bgy
Padu Chico	Bgy
Padu Grande	Bgy
Paguraper	Bgy
Panay	Bgy
Pangpangdan	Bgy
Parada	Bgy
Paras	Bgy
Poblacion	Bgy
Puerta Real	Bgy
Pussuac	Bgy
Quimmarayan	Bgy
San Pablo	Bgy
Santa Cruz	Bgy
Santo Tomas	Bgy
Sived	Bgy
Vacunero	Bgy
Suksukit	Bgy
Sigay	Mun
Abaccan	Bgy
Mabileg	Bgy
Matallucod	Bgy
Poblacion	Bgy
San Elias	Bgy
San Ramon	Bgy
Santo Rosario	Bgy
Sinait	Mun
Aguing	Bgy
Ballaigui (Pob.)	Bgy
Baliw	Bgy
Baracbac	Bgy
Barikir	Bgy
Battog	Bgy
Binacud	Bgy
Cabangtalan	Bgy
Cabarambanan	Bgy
Cabulalaan	Bgy
Cadanglaan	Bgy
Calingayan	Bgy
Curtin	Bgy
Dadalaquiten Norte	Bgy
Dadalaquiten Sur	Bgy
Duyayyat	Bgy
Jordan	Bgy
Calanutian	Bgy
Katipunan	Bgy
Macabiag (Pob.)	Bgy
Magsaysay	Bgy
Marnay	Bgy
Masadag	Bgy
Nagcullooban	Bgy
Nagbalioartian	Bgy
Nagongburan	Bgy
Namnama (Pob.)	Bgy
Pacis	Bgy
Paratong	Bgy
Dean Leopoldo Yabes	Bgy
Purag	Bgy
Quibit-quibit	Bgy
Quimmallogong	Bgy
Rang-ay (Pob.)	Bgy
Ricudo	Bgy
Sabañgan	Bgy
Sallacapo	Bgy
Santa Cruz	Bgy
Sapriana	Bgy
Tapao	Bgy
Teppeng	Bgy
Tubigay	Bgy
Ubbog	Bgy
Zapat	Bgy
Sugpon	Mun
Banga	Bgy
Caoayan	Bgy
Licungan	Bgy
Danac	Bgy
Pangotan	Bgy
Balbalayang (Pob.)	Bgy
Suyo	Mun
Baringcucurong	Bgy
Cabugao	Bgy
Man-atong	Bgy
Patoc-ao	Bgy
Poblacion	Bgy
Suyo Proper	Bgy
Urzadan	Bgy
Uso	Bgy
Tagudin	Mun
Ag-aguman	Bgy
Ambalayat	Bgy
Baracbac	Bgy
Bario-an	Bgy
Baritao	Bgy
Borono	Bgy
Becques	Bgy
Bimmanga	Bgy
Bio	Bgy
Bitalag	Bgy
Bucao East	Bgy
Bucao West	Bgy
Cabaroan	Bgy
Cabugbugan	Bgy
Cabulanglangan	Bgy
Dacutan	Bgy
Dardarat	Bgy
Del Pilar (Pob.)	Bgy
Farola	Bgy
Gabur	Bgy
Garitan	Bgy
Jardin	Bgy
Lacong	Bgy
Lantag	Bgy
Las-ud	Bgy
Libtong	Bgy
Lubnac	Bgy
Magsaysay (Pob.)	Bgy
Malacañang	Bgy
Pacac	Bgy
Pallogan	Bgy
Pudoc East	Bgy
Pudoc West	Bgy
Pula	Bgy
Quirino (Pob.)	Bgy
Ranget	Bgy
Rizal (Pob.)	Bgy
Salvacion	Bgy
San Miguel	Bgy
Sawat	Bgy
Tallaoen	Bgy
Tampugo	Bgy
Tarangotong	Bgy
City of Vigan (Capital)	City
Ayusan Norte	Bgy
Ayusan Sur	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Barraca	Bgy
Beddeng Laud	Bgy
Beddeng Daya	Bgy
Bongtolan	Bgy
Bulala	Bgy
Cabalangegan	Bgy
Cabaroan Daya	Bgy
Cabaroan Laud	Bgy
Camangaan	Bgy
Capangpangan	Bgy
Mindoro	Bgy
Nagsangalan	Bgy
Pantay Daya	Bgy
Pantay Fatima	Bgy
Pantay Laud	Bgy
Paoa	Bgy
Paratong	Bgy
Pong-ol	Bgy
Purok-a-bassit	Bgy
Purok-a-dackel	Bgy
Raois	Bgy
Rugsuanan	Bgy
Salindeg	Bgy
San Jose	Bgy
San Julian Norte	Bgy
San Julian Sur	Bgy
San Pedro	Bgy
Tamag	Bgy
Barangay VII	Bgy
Barangay VIII	Bgy
Barangay IX	Bgy
La Union	Prov
Agoo	Mun
Ambitacay	Bgy
Balawarte	Bgy
Capas	Bgy
Consolacion (Pob.)	Bgy
Macalva Central	Bgy
Macalva Norte	Bgy
Macalva Sur	Bgy
Nazareno	Bgy
Purok	Bgy
San Agustin East	Bgy
San Agustin Norte	Bgy
San Agustin Sur	Bgy
San Antonino	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Joaquin Norte	Bgy
San Joaquin Sur	Bgy
San Jose Norte	Bgy
San Jose Sur	Bgy
San Juan	Bgy
San Julian Central	Bgy
San Julian East	Bgy
San Julian Norte	Bgy
San Julian West	Bgy
San Manuel Norte	Bgy
San Manuel Sur	Bgy
San Marcos	Bgy
San Miguel	Bgy
San Nicolas Central (Pob.)	Bgy
San Nicolas East	Bgy
San Nicolas Norte (Pob.)	Bgy
San Nicolas West	Bgy
San Nicolas Sur (Pob.)	Bgy
San Pedro	Bgy
San Roque West	Bgy
San Roque East	Bgy
San Vicente Norte	Bgy
San Vicente Sur	Bgy
Santa Ana	Bgy
Santa Barbara (Pob.)	Bgy
Santa Fe	Bgy
Santa Maria	Bgy
Santa Monica	Bgy
Santa Rita	Bgy
Santa Rita East	Bgy
Santa Rita Norte	Bgy
Santa Rita Sur	Bgy
Santa Rita West	Bgy
Aringay	Mun
Alaska	Bgy
Basca	Bgy
Dulao	Bgy
Gallano	Bgy
Macabato	Bgy
Manga	Bgy
Pangao-aoan East	Bgy
Pangao-aoan West	Bgy
Poblacion	Bgy
Samara	Bgy
San Antonio	Bgy
San Benito Norte	Bgy
San Benito Sur	Bgy
San Eugenio	Bgy
San Juan East	Bgy
San Juan West	Bgy
San Simon East	Bgy
San Simon West	Bgy
Santa Cecilia	Bgy
Santa Lucia	Bgy
Santa Rita East	Bgy
Santa Rita West	Bgy
Santo Rosario East	Bgy
Santo Rosario West	Bgy
Bacnotan	Mun
Agtipal	Bgy
Arosip	Bgy
Bacqui	Bgy
Bacsil	Bgy
Bagutot	Bgy
Ballogo	Bgy
Baroro	Bgy
Bitalag	Bgy
Bulala	Bgy
Burayoc	Bgy
Bussaoit	Bgy
Cabaroan	Bgy
Cabarsican	Bgy
Cabugao	Bgy
Calautit	Bgy
Carcarmay	Bgy
Casiaman	Bgy
Galongen	Bgy
Guinabang	Bgy
Legleg	Bgy
Lisqueb	Bgy
Mabanengbeng 1st	Bgy
Mabanengbeng 2nd	Bgy
Maragayap	Bgy
Nangalisan	Bgy
Nagatiran	Bgy
Nagsaraboan	Bgy
Nagsimbaanan	Bgy
Narra	Bgy
Ortega	Bgy
Paagan	Bgy
Pandan	Bgy
Pang-pang	Bgy
Poblacion	Bgy
Quirino	Bgy
Raois	Bgy
Salincob	Bgy
San Martin	Bgy
Santa Cruz	Bgy
Santa Rita	Bgy
Sapilang	Bgy
Sayoan	Bgy
Sipulo	Bgy
Tammocalao	Bgy
Ubbog	Bgy
Oya-oy	Bgy
Zaragosa	Bgy
Bagulin	Mun
Alibangsay	Bgy
Baay	Bgy
Cambaly	Bgy
Cardiz	Bgy
Dagup	Bgy
Libbo	Bgy
Suyo (Pob.)	Bgy
Tagudtud	Bgy
Tio-angan	Bgy
Wallayan	Bgy
Balaoan	Mun
Apatut	Bgy
Ar-arampang	Bgy
Baracbac Este	Bgy
Baracbac Oeste	Bgy
Bet-ang	Bgy
Bulbulala	Bgy
Bungol	Bgy
Butubut Este	Bgy
Butubut Norte	Bgy
Butubut Oeste	Bgy
Butubut Sur	Bgy
Cabuaan Oeste (Pob.)	Bgy
Calliat	Bgy
Calungbuyan	Bgy
Camiling	Bgy
Guinaburan	Bgy
Masupe	Bgy
Nagsabaran Norte	Bgy
Nagsabaran Sur	Bgy
Nalasin	Bgy
Napaset	Bgy
Pagbennecan	Bgy
Pagleddegan	Bgy
Pantar Norte	Bgy
Pantar Sur	Bgy
Pa-o	Bgy
Almieda	Bgy
Paraoir	Bgy
Patpata	Bgy
Dr. Camilo Osias Pob.	Bgy
Sablut	Bgy
San Pablo	Bgy
Sinapangan Norte	Bgy
Sinapangan Sur	Bgy
Tallipugo	Bgy
Antonino	Bgy
Bangar	Mun
Agdeppa	Bgy
Alzate	Bgy
Bangaoilan East	Bgy
Bangaoilan West	Bgy
Barraca	Bgy
Cadapli	Bgy
Caggao	Bgy
Consuegra	Bgy
General Prim East	Bgy
General Prim West	Bgy
General Terrero	Bgy
Luzong Norte	Bgy
Luzong Sur	Bgy
Maria Cristina East	Bgy
Maria Cristina West	Bgy
Mindoro	Bgy
Nagsabaran	Bgy
Paratong Norte	Bgy
Paratong No. 3	Bgy
Paratong No. 4	Bgy
Central East No. 1 (Pob.)	Bgy
Central East No. 2 (Pob.)	Bgy
Central West No. 1 (Pob.)	Bgy
Central West No. 2 (Pob.)	Bgy
Central West No. 3 (Pob.)	Bgy
Quintarong	Bgy
Reyna Regente	Bgy
Rissing	Bgy
San Blas	Bgy
San Cristobal	Bgy
Sinapangan Norte	Bgy
Sinapangan Sur	Bgy
Ubbog	Bgy
Bauang	Mun
Acao	Bgy
Baccuit Norte	Bgy
Baccuit Sur	Bgy
Bagbag	Bgy
Ballay	Bgy
Bawanta	Bgy
Boy-utan	Bgy
Bucayab	Bgy
Cabalayangan	Bgy
Cabisilan	Bgy
Calumbaya	Bgy
Carmay	Bgy
Casilagan	Bgy
Central East (Pob.)	Bgy
Central West (Pob.)	Bgy
Dili	Bgy
Disso-or	Bgy
Guerrero	Bgy
Nagrebcan	Bgy
Pagdalagan Sur	Bgy
Palintucang	Bgy
Palugsi-Limmansangan	Bgy
Parian Oeste	Bgy
Parian Este	Bgy
Paringao	Bgy
Payocpoc Norte Este	Bgy
Payocpoc Norte Oeste	Bgy
Payocpoc Sur	Bgy
Pilar	Bgy
Pudoc	Bgy
Pottot	Bgy
Pugo	Bgy
Quinavite	Bgy
Lower San Agustin	Bgy
Santa Monica	Bgy
Santiago	Bgy
Taberna	Bgy
Upper San Agustin	Bgy
Urayong	Bgy
Burgos	Mun
Agpay	Bgy
Bilis	Bgy
Caoayan	Bgy
Dalacdac	Bgy
Delles	Bgy
Imelda	Bgy
Libtong	Bgy
Linuan	Bgy
New Poblacion	Bgy
Old Poblacion	Bgy
Lower Tumapoc	Bgy
Upper Tumapoc	Bgy
Caba	Mun
Bautista	Bgy
Gana	Bgy
Juan Cartas	Bgy
Las-ud	Bgy
Liquicia	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
San Carlos	Bgy
San Cornelio	Bgy
San Fermin	Bgy
San Gregorio	Bgy
San Jose	Bgy
Santiago Norte	Bgy
Santiago Sur	Bgy
Sobredillo	Bgy
Urayong	Bgy
Wenceslao	Bgy
Luna	Mun
Alcala (Pob.)	Bgy
Ayaoan	Bgy
Barangobong	Bgy
Barrientos	Bgy
Bungro	Bgy
Buselbusel	Bgy
Cabalitocan	Bgy
Cantoria No. 1	Bgy
Cantoria No. 2	Bgy
Cantoria No. 3	Bgy
Cantoria No. 4	Bgy
Carisquis	Bgy
Darigayos	Bgy
Magallanes (Pob.)	Bgy
Magsiping	Bgy
Mamay	Bgy
Nagrebcan	Bgy
Nalvo Norte	Bgy
Nalvo Sur	Bgy
Napaset	Bgy
Oaqui No. 1	Bgy
Oaqui No. 2	Bgy
Oaqui No. 3	Bgy
Oaqui No. 4	Bgy
Pila	Bgy
Pitpitac	Bgy
Rimos No. 1	Bgy
Rimos No. 2	Bgy
Rimos No. 3	Bgy
Rimos No. 4	Bgy
Rimos No. 5	Bgy
Rissing	Bgy
Salcedo (Pob.)	Bgy
Santo Domingo Norte	Bgy
Santo Domingo Sur	Bgy
Sucoc Norte	Bgy
Sucoc Sur	Bgy
Suyo	Bgy
Tallaoen	Bgy
Victoria (Pob.)	Bgy
Naguilian	Mun
Aguioas	Bgy
Al-alinao Norte	Bgy
Al-alinao Sur	Bgy
Ambaracao Norte	Bgy
Ambaracao Sur	Bgy
Angin	Bgy
Balecbec	Bgy
Bancagan	Bgy
Baraoas Norte	Bgy
Baraoas Sur	Bgy
Bariquir	Bgy
Bato	Bgy
Bimmotobot	Bgy
Cabaritan Norte	Bgy
Cabaritan Sur	Bgy
Casilagan	Bgy
Dal-lipaoen	Bgy
Daramuangan	Bgy
Guesset	Bgy
Gusing Norte	Bgy
Gusing Sur	Bgy
Imelda	Bgy
Lioac Norte	Bgy
Lioac Sur	Bgy
Magungunay	Bgy
Mamat-ing Norte	Bgy
Mamat-ing Sur	Bgy
Nagsidorisan	Bgy
Natividad (Pob.)	Bgy
Ortiz (Pob.)	Bgy
Ribsuan	Bgy
San Antonio	Bgy
San Isidro	Bgy
Sili	Bgy
Suguidan Norte	Bgy
Suguidan Sur	Bgy
Tuddingan	Bgy
Pugo	Mun
Ambalite	Bgy
Ambangonan	Bgy
Cares	Bgy
Cuenca	Bgy
Duplas	Bgy
Maoasoas Norte	Bgy
Maoasoas Sur	Bgy
Palina	Bgy
Poblacion East	Bgy
San Luis	Bgy
Saytan	Bgy
Tavora East	Bgy
Tavora Proper	Bgy
Poblacion West	Bgy
Rosario	Mun
Alipang	Bgy
Ambangonan	Bgy
Amlang	Bgy
Bacani	Bgy
Bangar	Bgy
Bani	Bgy
Benteng-Sapilang	Bgy
Cadumanian	Bgy
Camp One	Bgy
Carunuan East	Bgy
Carunuan West	Bgy
Casilagan	Bgy
Cataguingtingan	Bgy
Concepcion	Bgy
Damortis	Bgy
Gumot-Nagcolaran	Bgy
Inabaan Norte	Bgy
Inabaan Sur	Bgy
Nagtagaan	Bgy
Nangcamotian	Bgy
Parasapas	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Puzon	Bgy
Rabon	Bgy
San Jose	Bgy
Marcos	Bgy
Subusub	Bgy
Tabtabungao	Bgy
Tanglag	Bgy
Tay-ac	Bgy
Udiao	Bgy
Vila	Bgy
City of San Fernando (Capital)	City
Abut	Bgy
Apaleng	Bgy
Bacsil	Bgy
Bangbangolan	Bgy
Bangcusay	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Baraoas	Bgy
Bato	Bgy
Biday	Bgy
Birunget	Bgy
Bungro	Bgy
Cabaroan	Bgy
Cabarsican	Bgy
Cadaclan	Bgy
Calabugao	Bgy
Camansi	Bgy
Canaoay	Bgy
Carlatan	Bgy
Catbangen	Bgy
Dallangayan Este	Bgy
Dallangayan Oeste	Bgy
Dalumpinas Este	Bgy
Dalumpinas Oeste	Bgy
Ilocanos Norte	Bgy
Ilocanos Sur	Bgy
Langcuas	Bgy
Lingsat	Bgy
Madayegdeg	Bgy
Mameltac	Bgy
Masicong	Bgy
Nagyubuyuban	Bgy
Namtutan	Bgy
Narra Este	Bgy
Narra Oeste	Bgy
Pacpaco	Bgy
Pagdalagan	Bgy
Pagdaraoan	Bgy
Pagudpud	Bgy
Pao Norte	Bgy
Pao Sur	Bgy
Parian	Bgy
Pias	Bgy
Poro	Bgy
Puspus	Bgy
Sacyud	Bgy
Sagayad	Bgy
San Agustin	Bgy
San Francisco	Bgy
San Vicente	Bgy
Santiago Norte	Bgy
Santiago Sur	Bgy
Saoay	Bgy
Sevilla	Bgy
Siboan-Otong	Bgy
Tanqui	Bgy
Tanquigan	Bgy
San Gabriel	Mun
Amontoc	Bgy
Apayao	Bgy
Balbalayang	Bgy
Bayabas	Bgy
Bucao	Bgy
Bumbuneg	Bgy
Lacong	Bgy
Lipay Este	Bgy
Lipay Norte	Bgy
Lipay Proper	Bgy
Lipay Sur	Bgy
Lon-oy	Bgy
Poblacion	Bgy
Polipol	Bgy
Daking	Bgy
San Juan	Mun
Allangigan	Bgy
Aludaid	Bgy
Bacsayan	Bgy
Balballosa	Bgy
Bambanay	Bgy
Bugbugcao	Bgy
Caarusipan	Bgy
Cabaroan	Bgy
Cabugnayan	Bgy
Cacapian	Bgy
Caculangan	Bgy
Calincamasan	Bgy
Casilagan	Bgy
Catdongan	Bgy
Dangdangla	Bgy
Dasay	Bgy
Dinanum	Bgy
Duplas	Bgy
Guinguinabang	Bgy
Ili Norte (Pob.)	Bgy
Ili Sur (Pob.)	Bgy
Legleg	Bgy
Lubing	Bgy
Nadsaag	Bgy
Nagsabaran	Bgy
Naguirangan	Bgy
Naguituban	Bgy
Nagyubuyuban	Bgy
Oaquing	Bgy
Pacpacac	Bgy
Pagdildilan	Bgy
Panicsican	Bgy
Quidem	Bgy
San Felipe	Bgy
Santa Rosa	Bgy
Santo Rosario	Bgy
Saracat	Bgy
Sinapangan	Bgy
Taboc	Bgy
Talogtog	Bgy
Urbiztondo	Bgy
Santo Tomas	Mun
Ambitacay	Bgy
Bail	Bgy
Balaoc	Bgy
Balsaan	Bgy
Baybay	Bgy
Cabaruan	Bgy
Casantaan	Bgy
Casilagan	Bgy
Cupang	Bgy
Damortis	Bgy
Fernando	Bgy
Linong	Bgy
Lomboy	Bgy
Malabago	Bgy
Namboongan	Bgy
Namonitan	Bgy
Narvacan	Bgy
Patac	Bgy
Poblacion	Bgy
Pongpong	Bgy
Raois	Bgy
Tubod	Bgy
Tococ	Bgy
Ubagan	Bgy
Santol	Mun
Corrooy	Bgy
Lettac Norte	Bgy
Lettac Sur	Bgy
Mangaan	Bgy
Paagan	Bgy
Poblacion	Bgy
Puguil	Bgy
Ramot	Bgy
Sapdaan	Bgy
Sasaba	Bgy
Tubaday	Bgy
Sudipen	Mun
Bigbiga	Bgy
Castro	Bgy
Duplas	Bgy
Ipet	Bgy
Ilocano	Bgy
Maliclico	Bgy
Old Central	Bgy
Namaltugan	Bgy
Poblacion	Bgy
Porporiket	Bgy
San Francisco Norte	Bgy
San Francisco Sur	Bgy
San Jose	Bgy
Sengngat	Bgy
Turod	Bgy
Up-uplas	Bgy
Bulalaan	Bgy
Tubao	Mun
Amallapay	Bgy
Anduyan	Bgy
Caoigue	Bgy
Francia Sur	Bgy
Francia West	Bgy
Garcia	Bgy
Gonzales	Bgy
Halog East	Bgy
Halog West	Bgy
Leones East	Bgy
Leones West	Bgy
Linapew	Bgy
Magsaysay	Bgy
Pideg	Bgy
Poblacion	Bgy
Rizal	Bgy
Santa Teresa	Bgy
Lloren	Bgy
Pangasinan	Prov
Agno	Mun
Allabon	Bgy
Aloleng	Bgy
Bangan-Oda	Bgy
Baruan	Bgy
Boboy	Bgy
Cayungnan	Bgy
Dangley	Bgy
Gayusan	Bgy
Macaboboni	Bgy
Magsaysay	Bgy
Namatucan	Bgy
Patar	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
San Juan	Bgy
Tupa	Bgy
Viga	Bgy
Aguilar	Mun
Bayaoas	Bgy
Baybay	Bgy
Bocacliw	Bgy
Bocboc East	Bgy
Bocboc West	Bgy
Buer	Bgy
Calsib	Bgy
Ninoy	Bgy
Poblacion	Bgy
Pogomboa	Bgy
Pogonsili	Bgy
San Jose	Bgy
Tampac	Bgy
Laoag	Bgy
Manlocboc	Bgy
Panacol	Bgy
City of Alaminos	City
Alos	Bgy
Amandiego	Bgy
Amangbangan	Bgy
Balangobong	Bgy
Balayang	Bgy
Bisocol	Bgy
Bolaney	Bgy
Baleyadaan	Bgy
Bued	Bgy
Cabatuan	Bgy
Cayucay	Bgy
Dulacac	Bgy
Inerangan	Bgy
Linmansangan	Bgy
Lucap	Bgy
Macatiw	Bgy
Magsaysay	Bgy
Mona	Bgy
Palamis	Bgy
Pangapisan	Bgy
Poblacion	Bgy
Pocalpocal	Bgy
Pogo	Bgy
Polo	Bgy
Quibuar	Bgy
Sabangan	Bgy
San Jose	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Tanaytay	Bgy
Tangcarang	Bgy
Tawintawin	Bgy
Telbang	Bgy
Victoria	Bgy
Landoc	Bgy
Maawi	Bgy
Pandan	Bgy
San Antonio	Bgy
Alcala	Mun
Anulid	Bgy
Atainan	Bgy
Bersamin	Bgy
Canarvacanan	Bgy
Caranglaan	Bgy
Curareng	Bgy
Gualsic	Bgy
Kasikis	Bgy
Laoac	Bgy
Macayo	Bgy
Pindangan Centro	Bgy
Pindangan East	Bgy
Pindangan West	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
San Juan	Bgy
San Nicolas	Bgy
San Pedro Apartado	Bgy
San Pedro IlI	Bgy
San Vicente	Bgy
Vacante	Bgy
Anda	Mun
Awile	Bgy
Awag	Bgy
Batiarao	Bgy
Cabungan	Bgy
Carot	Bgy
Dolaoan	Bgy
Imbo	Bgy
Macaleeng	Bgy
Macandocandong	Bgy
Mal-ong	Bgy
Namagbagan	Bgy
Poblacion	Bgy
Roxas	Bgy
Sablig	Bgy
San Jose	Bgy
Siapar	Bgy
Tondol	Bgy
Toritori	Bgy
Asingan	Mun
Ariston Este	Bgy
Ariston Weste	Bgy
Bantog	Bgy
Baro	Bgy
Bobonan	Bgy
Cabalitian	Bgy
Calepaan	Bgy
Carosucan Norte	Bgy
Carosucan Sur	Bgy
Coldit	Bgy
Domanpot	Bgy
Dupac	Bgy
Macalong	Bgy
Palaris	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
San Vicente Este	Bgy
San Vicente Weste	Bgy
Sanchez	Bgy
Sobol	Bgy
Toboy	Bgy
Balungao	Mun
Angayan Norte	Bgy
Angayan Sur	Bgy
Capulaan	Bgy
Esmeralda	Bgy
Kita-kita	Bgy
Mabini	Bgy
Mauban	Bgy
Poblacion	Bgy
Pugaro	Bgy
Rajal	Bgy
San Andres	Bgy
San Aurelio 1st	Bgy
San Aurelio 2nd	Bgy
San Aurelio 3rd	Bgy
San Joaquin	Bgy
San Julian	Bgy
San Leon	Bgy
San Marcelino	Bgy
San Miguel	Bgy
San Raymundo	Bgy
Bani	Mun
Ambabaay	Bgy
Aporao	Bgy
Arwas	Bgy
Ballag	Bgy
Banog Norte	Bgy
Banog Sur	Bgy
Centro Toma	Bgy
Colayo	Bgy
Dacap Norte	Bgy
Dacap Sur	Bgy
Garrita	Bgy
Luac	Bgy
Macabit	Bgy
Masidem	Bgy
Poblacion	Bgy
Quinaoayanan	Bgy
Ranao	Bgy
Ranom Iloco	Bgy
San Jose	Bgy
San Miguel	Bgy
San Simon	Bgy
San Vicente	Bgy
Tiep	Bgy
Tipor	Bgy
Tugui Grande	Bgy
Tugui Norte	Bgy
Calabeng	Bgy
Basista	Mun
Anambongan	Bgy
Bayoyong	Bgy
Cabeldatan	Bgy
Dumpay	Bgy
Malimpec East	Bgy
Mapolopolo	Bgy
Nalneran	Bgy
Navatat	Bgy
Obong	Bgy
Osmena Sr.	Bgy
Palma	Bgy
Patacbo	Bgy
Poblacion	Bgy
Bautista	Mun
Artacho	Bgy
Cabuaan	Bgy
Cacandongan	Bgy
Diaz	Bgy
Nandacan	Bgy
Nibaliw Norte	Bgy
Nibaliw Sur	Bgy
Palisoc	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Pogo	Bgy
Poponto	Bgy
Primicias	Bgy
Ketegan	Bgy
Sinabaan	Bgy
Vacante	Bgy
Villanueva	Bgy
Baluyot	Bgy
Bayambang	Mun
Alinggan	Bgy
Amamperez	Bgy
Amancosiling Norte	Bgy
Amancosiling Sur	Bgy
Ambayat I	Bgy
Ambayat II	Bgy
Apalen	Bgy
Asin	Bgy
Ataynan	Bgy
Bacnono	Bgy
Balaybuaya	Bgy
Banaban	Bgy
Bani	Bgy
Batangcawa	Bgy
Beleng	Bgy
Bical Norte	Bgy
Bical Sur	Bgy
Bongato East	Bgy
Bongato West	Bgy
Buayaen	Bgy
Buenlag 1st	Bgy
Buenlag 2nd	Bgy
Cadre Site	Bgy
Carungay	Bgy
Caturay	Bgy
Duera	Bgy
Dusoc	Bgy
Hermoza	Bgy
Idong	Bgy
Inanlorenzana	Bgy
Inirangan	Bgy
Iton	Bgy
Langiran	Bgy
Ligue	Bgy
M. H. del Pilar	Bgy
Macayocayo	Bgy
Magsaysay	Bgy
Maigpa	Bgy
Malimpec	Bgy
Malioer	Bgy
Managos	Bgy
Manambong Norte	Bgy
Manambong Parte	Bgy
Manambong Sur	Bgy
Mangayao	Bgy
Nalsian Norte	Bgy
Nalsian Sur	Bgy
Pangdel	Bgy
Pantol	Bgy
Paragos	Bgy
Poblacion Sur	Bgy
Pugo	Bgy
Reynado	Bgy
San Gabriel 1st	Bgy
San Gabriel 2nd	Bgy
San Vicente	Bgy
Sangcagulis	Bgy
Sanlibo	Bgy
Sapang	Bgy
Tamaro	Bgy
Tambac	Bgy
Tampog	Bgy
Darawey	Bgy
Tanolong	Bgy
Tatarao	Bgy
Telbang	Bgy
Tococ East	Bgy
Tococ West	Bgy
Warding	Bgy
Wawa	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Zone V (Pob.)	Bgy
Zone VI (Pob.)	Bgy
Zone VII (Pob.)	Bgy
Binalonan	Mun
Balangobong	Bgy
Bued	Bgy
Bugayong	Bgy
Camangaan	Bgy
Canarvacanan	Bgy
Capas	Bgy
Cili	Bgy
Dumayat	Bgy
Linmansangan	Bgy
Mangcasuy	Bgy
Moreno	Bgy
Pasileng Norte	Bgy
Pasileng Sur	Bgy
Poblacion	Bgy
San Felipe Central	Bgy
San Felipe Sur	Bgy
San Pablo	Bgy
Santa Catalina	Bgy
Santa Maria Norte	Bgy
Santiago	Bgy
Santo Niño	Bgy
Sumabnit	Bgy
Tabuyoc	Bgy
Vacante	Bgy
Binmaley	Mun
Amancoro	Bgy
Balagan	Bgy
Balogo	Bgy
Basing	Bgy
Baybay Lopez	Bgy
Baybay Polong	Bgy
Biec	Bgy
Buenlag	Bgy
Calit	Bgy
Caloocan Norte	Bgy
Caloocan Sur	Bgy
Camaley	Bgy
Canaoalan	Bgy
Dulag	Bgy
Gayaman	Bgy
Linoc	Bgy
Lomboy	Bgy
Nagpalangan	Bgy
Malindong	Bgy
Manat	Bgy
Naguilayan	Bgy
Pallas	Bgy
Papagueyan	Bgy
Parayao	Bgy
Poblacion	Bgy
Pototan	Bgy
Sabangan	Bgy
Salapingao	Bgy
San Isidro Norte	Bgy
San Isidro Sur	Bgy
Santa Rosa	Bgy
Tombor	Bgy
Caloocan Dupo	Bgy
Bolinao	Mun
Arnedo	Bgy
Balingasay	Bgy
Binabalian	Bgy
Cabuyao	Bgy
Catuday	Bgy
Catungi	Bgy
Concordia (Pob.)	Bgy
Culang	Bgy
Dewey	Bgy
Estanza	Bgy
Germinal (Pob.)	Bgy
Goyoden	Bgy
Ilogmalino	Bgy
Lambes	Bgy
Liwa-liwa	Bgy
Lucero	Bgy
Luciente 1.0	Bgy
Luciente 2.0	Bgy
Luna	Bgy
Patar	Bgy
Pilar	Bgy
Salud	Bgy
Samang Norte	Bgy
Samang Sur	Bgy
Sampaloc	Bgy
San Roque	Bgy
Tara	Bgy
Tupa	Bgy
Victory	Bgy
Zaragoza	Bgy
Bugallon	Mun
Angarian	Bgy
Asinan	Bgy
Banaga	Bgy
Bacabac	Bgy
Bolaoen	Bgy
Buenlag	Bgy
Cabayaoasan	Bgy
Cayanga	Bgy
Gueset	Bgy
Hacienda	Bgy
Laguit Centro	Bgy
Laguit Padilla	Bgy
Magtaking	Bgy
Pangascasan	Bgy
Pantal	Bgy
Poblacion	Bgy
Polong	Bgy
Portic	Bgy
Salasa	Bgy
Salomague Norte	Bgy
Salomague Sur	Bgy
Samat	Bgy
San Francisco	Bgy
Umanday	Bgy
Burgos	Mun
Anapao	Bgy
Cacayasen	Bgy
Concordia	Bgy
Ilio-ilio	Bgy
Papallasen	Bgy
Poblacion	Bgy
Pogoruac	Bgy
Don Matias	Bgy
San Miguel	Bgy
San Pascual	Bgy
San Vicente	Bgy
Sapa Grande	Bgy
Sapa Pequeña	Bgy
Tambacan	Bgy
Calasiao	Mun
Ambonao	Bgy
Ambuetel	Bgy
Banaoang	Bgy
Bued	Bgy
Buenlag	Bgy
Cabilocaan	Bgy
Dinalaoan	Bgy
Doyong	Bgy
Gabon	Bgy
Lasip	Bgy
Longos	Bgy
Lumbang	Bgy
Macabito	Bgy
Malabago	Bgy
Mancup	Bgy
Nagsaing	Bgy
Nalsian	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Quesban	Bgy
San Miguel	Bgy
San Vicente	Bgy
Songkoy	Bgy
Talibaew	Bgy
City of Dagupan	City
Bacayao Norte	Bgy
Bacayao Sur	Bgy
Barangay II	Bgy
Barangay IV	Bgy
Bolosan	Bgy
Bonuan Binloc	Bgy
Bonuan Boquig	Bgy
Bonuan Gueset	Bgy
Calmay	Bgy
Carael	Bgy
Caranglaan	Bgy
Herrero	Bgy
Lasip Chico	Bgy
Lasip Grande	Bgy
Lomboy	Bgy
Lucao	Bgy
Malued	Bgy
Mamalingling	Bgy
Mangin	Bgy
Mayombo	Bgy
Pantal	Bgy
Poblacion Oeste	Bgy
Barangay I	Bgy
Pogo Chico	Bgy
Pogo Grande	Bgy
Pugaro Suit	Bgy
Salapingao	Bgy
Salisay	Bgy
Tambac	Bgy
Tapuac	Bgy
Tebeng	Bgy
Dasol	Mun
Alilao	Bgy
Amalbalan	Bgy
Bobonot	Bgy
Eguia	Bgy
Gais-Guipe	Bgy
Hermosa	Bgy
Macalang	Bgy
Magsaysay	Bgy
Malacapas	Bgy
Malimpin	Bgy
Osmeña	Bgy
Petal	Bgy
Poblacion	Bgy
San Vicente	Bgy
Tambac	Bgy
Tambobong	Bgy
Uli	Bgy
Viga	Bgy
Infanta	Mun
Bamban	Bgy
Batang	Bgy
Bayambang	Bgy
Cato	Bgy
Doliman	Bgy
Fatima	Bgy
Maya	Bgy
Nangalisan	Bgy
Nayom	Bgy
Pita	Bgy
Poblacion	Bgy
Potol	Bgy
Babuyan	Bgy
Labrador	Mun
Bolo	Bgy
Bongalon	Bgy
Dulig	Bgy
Laois	Bgy
Magsaysay	Bgy
Poblacion	Bgy
San Gonzalo	Bgy
San Jose	Bgy
Tobuan	Bgy
Uyong	Bgy
Lingayen (Capital)	Mun
Aliwekwek	Bgy
Baay	Bgy
Balangobong	Bgy
Balococ	Bgy
Bantayan	Bgy
Basing	Bgy
Capandanan	Bgy
Domalandan Center	Bgy
Domalandan East	Bgy
Domalandan West	Bgy
Dorongan	Bgy
Dulag	Bgy
Estanza	Bgy
Lasip	Bgy
Libsong East	Bgy
Libsong West	Bgy
Malawa	Bgy
Malimpuec	Bgy
Maniboc	Bgy
Matalava	Bgy
Naguelguel	Bgy
Namolan	Bgy
Pangapisan North	Bgy
Pangapisan Sur	Bgy
Poblacion	Bgy
Quibaol	Bgy
Rosario	Bgy
Sabangan	Bgy
Talogtog	Bgy
Tonton	Bgy
Tumbar	Bgy
Wawa	Bgy
Mabini	Mun
Bacnit	Bgy
Barlo	Bgy
Caabiangaan	Bgy
Cabanaetan	Bgy
Cabinuangan	Bgy
Calzada	Bgy
Caranglaan	Bgy
De Guzman	Bgy
Luna	Bgy
Magalong	Bgy
Nibaliw	Bgy
Patar	Bgy
Poblacion	Bgy
San Pedro	Bgy
Tagudin	Bgy
Villacorta	Bgy
Malasiqui	Mun
Abonagan	Bgy
Agdao	Bgy
Alacan	Bgy
Aliaga	Bgy
Amacalan	Bgy
Anolid	Bgy
Apaya	Bgy
Asin Este	Bgy
Asin Weste	Bgy
Bacundao Este	Bgy
Bacundao Weste	Bgy
Bakitiw	Bgy
Balite	Bgy
Banawang	Bgy
Barang	Bgy
Bawer	Bgy
Binalay	Bgy
Bobon	Bgy
Bolaoit	Bgy
Bongar	Bgy
Butao	Bgy
Cabatling	Bgy
Cabueldatan	Bgy
Calbueg	Bgy
Canan Norte	Bgy
Canan Sur	Bgy
Cawayan Bogtong	Bgy
Don Pedro	Bgy
Gatang	Bgy
Goliman	Bgy
Gomez	Bgy
Guilig	Bgy
Ican	Bgy
Ingalagala	Bgy
Lareg-lareg	Bgy
Lasip	Bgy
Lepa	Bgy
Loqueb Este	Bgy
Loqueb Norte	Bgy
Loqueb Sur	Bgy
Lunec	Bgy
Mabulitec	Bgy
Malimpec	Bgy
Manggan-Dampay	Bgy
Nancapian	Bgy
Nalsian Norte	Bgy
Nalsian Sur	Bgy
Nansangaan	Bgy
Olea	Bgy
Pacuan	Bgy
Palapar Norte	Bgy
Palapar Sur	Bgy
Palong	Bgy
Pamaranum	Bgy
Pasima	Bgy
Payar	Bgy
Poblacion	Bgy
Polong Norte	Bgy
Polong Sur	Bgy
Potiocan	Bgy
San Julian	Bgy
Tabo-Sili	Bgy
Tobor	Bgy
Talospatang	Bgy
Taloy	Bgy
Taloyan	Bgy
Tambac	Bgy
Tolonguat	Bgy
Tomling	Bgy
Umando	Bgy
Viado	Bgy
Waig	Bgy
Warey	Bgy
Manaoag	Mun
Babasit	Bgy
Baguinay	Bgy
Baritao	Bgy
Bisal	Bgy
Bucao	Bgy
Cabanbanan	Bgy
Calaocan	Bgy
Inamotan	Bgy
Lelemaan	Bgy
Licsi	Bgy
Lipit Norte	Bgy
Lipit Sur	Bgy
Parian	Bgy
Matolong	Bgy
Mermer	Bgy
Nalsian	Bgy
Oraan East	Bgy
Oraan West	Bgy
Pantal	Bgy
Pao	Bgy
Poblacion	Bgy
Pugaro	Bgy
San Ramon	Bgy
Santa Ines	Bgy
Sapang	Bgy
Tebuel	Bgy
Mangaldan	Mun
Alitaya	Bgy
Amansabina	Bgy
Anolid	Bgy
Banaoang	Bgy
Bantayan	Bgy
Bari	Bgy
Bateng	Bgy
Buenlag	Bgy
David	Bgy
Embarcadero	Bgy
Gueguesangen	Bgy
Guesang	Bgy
Guiguilonen	Bgy
Guilig	Bgy
Inlambo	Bgy
Lanas	Bgy
Landas	Bgy
Maasin	Bgy
Macayug	Bgy
Malabago	Bgy
Navaluan	Bgy
Nibaliw	Bgy
Osiem	Bgy
Palua	Bgy
Poblacion	Bgy
Pogo	Bgy
Salaan	Bgy
Salay	Bgy
Tebag	Bgy
Talogtog	Bgy
Mangatarem	Mun
Andangin	Bgy
Arellano Street (Pob.)	Bgy
Bantay	Bgy
Bantocaling	Bgy
Baracbac	Bgy
Peania Pedania	Bgy
Bogtong Bolo	Bgy
Bogtong Bunao	Bgy
Bogtong Centro	Bgy
Bogtong Niog	Bgy
Bogtong Silag	Bgy
Buaya	Bgy
Buenlag	Bgy
Bueno	Bgy
Bunagan	Bgy
Bunlalacao	Bgy
Burgos Street (Pob.)	Bgy
Cabaluyan 1st	Bgy
Cabaluyan 2nd	Bgy
Cabarabuan	Bgy
Cabaruan	Bgy
Cabayaoasan	Bgy
Cabayugan	Bgy
Cacaoiten	Bgy
Calumboyan Norte	Bgy
Calumboyan Sur	Bgy
Calvo (Pob.)	Bgy
Casilagan	Bgy
Catarataraan	Bgy
Caturay Norte	Bgy
Caturay Sur	Bgy
Caviernesan	Bgy
Dorongan Ketaket	Bgy
Dorongan Linmansangan	Bgy
Dorongan Punta	Bgy
Dorongan Sawat	Bgy
Dorongan Valerio	Bgy
General Luna (Pob.)	Bgy
Historia	Bgy
Lawak Langka	Bgy
Linmansangan	Bgy
Lopez (Pob.)	Bgy
Mabini (Pob.)	Bgy
Macarang	Bgy
Malabobo	Bgy
Malibong	Bgy
Malunec	Bgy
Maravilla (Pob.)	Bgy
Maravilla-Arellano Ext. (Pob.)	Bgy
Muelang	Bgy
Naguilayan East	Bgy
Naguilayan West	Bgy
Nancasalan	Bgy
Niog-Cabison-Bulaney	Bgy
Olegario-Caoile (Pob.)	Bgy
Olo Cacamposan	Bgy
Olo Cafabrosan	Bgy
Olo Cagarlitan	Bgy
Osmeña (Pob.)	Bgy
Pacalat	Bgy
Pampano	Bgy
Parian	Bgy
Paul	Bgy
Pogon-Aniat	Bgy
Pogon-Lomboy (Pob.)	Bgy
Ponglo-Baleg	Bgy
Ponglo-Muelag	Bgy
Quetegan	Bgy
Quezon (Pob.)	Bgy
Salavante	Bgy
Sapang	Bgy
Sonson Ongkit	Bgy
Suaco	Bgy
Tagac	Bgy
Takipan	Bgy
Talogtog	Bgy
Tococ Barikir	Bgy
Torre 1st	Bgy
Torre 2nd	Bgy
Torres Bugallon (Pob.)	Bgy
Umangan	Bgy
Zamora (Pob.)	Bgy
Mapandan	Mun
Amanoaoac	Bgy
Apaya	Bgy
Aserda	Bgy
Baloling	Bgy
Coral	Bgy
Golden	Bgy
Jimenez	Bgy
Lambayan	Bgy
Luyan	Bgy
Nilombot	Bgy
Pias	Bgy
Poblacion	Bgy
Primicias	Bgy
Santa Maria	Bgy
Torres	Bgy
Natividad	Mun
Barangobong	Bgy
Batchelor East	Bgy
Batchelor West	Bgy
Burgos	Bgy
Cacandungan	Bgy
Calapugan	Bgy
Canarem	Bgy
Luna	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Rizal	Bgy
Salud	Bgy
San Eugenio	Bgy
San Macario Norte	Bgy
San Macario Sur	Bgy
San Maximo	Bgy
San Miguel	Bgy
Silag	Bgy
Pozorrubio	Mun
Alipangpang	Bgy
Amagbagan	Bgy
Balacag	Bgy
Banding	Bgy
Bantugan	Bgy
Batakil	Bgy
Bobonan	Bgy
Buneg	Bgy
Cablong	Bgy
Castaño	Bgy
Dilan	Bgy
Don Benito	Bgy
Haway	Bgy
Imbalbalatong	Bgy
Inoman	Bgy
Laoac	Bgy
Maambal	Bgy
Malasin	Bgy
Malokiat	Bgy
Manaol	Bgy
Nama	Bgy
Nantangalan	Bgy
Palacpalac	Bgy
Palguyod	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Rosario	Bgy
Sugcong	Bgy
Talogtog	Bgy
Tulnac	Bgy
Villegas	Bgy
Casanfernandoan	Bgy
Rosales	Mun
Acop	Bgy
Bakitbakit	Bgy
Balingcanaway	Bgy
Cabalaoangan Norte	Bgy
Cabalaoangan Sur	Bgy
Camangaan	Bgy
Capitan Tomas	Bgy
Carmay West	Bgy
Carmen East	Bgy
Carmen West	Bgy
Casanicolasan	Bgy
Coliling	Bgy
Calanutan	Bgy
Guiling	Bgy
Palakipak	Bgy
Pangaoan	Bgy
Rabago	Bgy
Rizal	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Bartolome	Bgy
San Isidro	Bgy
San Luis	Bgy
San Pedro East	Bgy
San Pedro West	Bgy
San Vicente	Bgy
San Angel	Bgy
Station District	Bgy
Tomana East	Bgy
Tomana West	Bgy
Zone I (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Carmay East	Bgy
Don Antonio Village	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone V (Pob.)	Bgy
City of San Carlos	City
Abanon	Bgy
Agdao	Bgy
Anando	Bgy
Ano	Bgy
Antipangol	Bgy
Aponit	Bgy
Bacnar	Bgy
Balaya	Bgy
Balayong	Bgy
Baldog	Bgy
Balite Sur	Bgy
Balococ	Bgy
Bani	Bgy
Bega	Bgy
Bocboc	Bgy
Bugallon-Posadas Street (Pob.)	Bgy
Bogaoan	Bgy
Bolingit	Bgy
Bolosan	Bgy
Bonifacio (Pob.)	Bgy
Buenglat	Bgy
Burgos Padlan (Pob.)	Bgy
Cacaritan	Bgy
Caingal	Bgy
Calobaoan	Bgy
Calomboyan	Bgy
Capataan	Bgy
Caoayan-Kiling	Bgy
Cobol	Bgy
Coliling	Bgy
Cruz	Bgy
Doyong	Bgy
Gamata	Bgy
Guelew	Bgy
Ilang	Bgy
Inerangan	Bgy
Isla	Bgy
Libas	Bgy
Lilimasan	Bgy
Longos	Bgy
Lucban (Pob.)	Bgy
Mabalbalino	Bgy
Mabini (Pob.)	Bgy
Magtaking	Bgy
Malacañang	Bgy
Maliwara	Bgy
Mamarlao	Bgy
Manzon	Bgy
Matagdem	Bgy
Mestizo Norte	Bgy
Naguilayan	Bgy
Nilentap	Bgy
Padilla-Gomez	Bgy
Pagal	Bgy
Palaming	Bgy
Palaris (Pob.)	Bgy
Palospos	Bgy
Pangalangan	Bgy
Pangoloan	Bgy
Pangpang	Bgy
Paitan-Panoypoy	Bgy
Parayao	Bgy
Payapa	Bgy
Payar	Bgy
Perez Boulevard (Pob.)	Bgy
Polo	Bgy
Quezon Boulevard (Pob.)	Bgy
Quintong	Bgy
Rizal (Pob.)	Bgy
Roxas Boulevard (Pob.)	Bgy
Salinap	Bgy
San Juan	Bgy
San Pedro-Taloy	Bgy
Sapinit	Bgy
PNR Station Site	Bgy
Supo	Bgy
Talang	Bgy
Tamayo	Bgy
Tandoc	Bgy
Tarece	Bgy
Tarectec	Bgy
Tayambani	Bgy
Tebag	Bgy
Turac	Bgy
M. Soriano	Bgy
Tandang Sora	Bgy
San Fabian	Mun
Ambalangan-Dalin	Bgy
Angio	Bgy
Anonang	Bgy
Aramal	Bgy
Bigbiga	Bgy
Binday	Bgy
Bolaoen	Bgy
Bolasi	Bgy
Cayanga	Bgy
Gomot	Bgy
Inmalog	Bgy
Lekep-Butao	Bgy
Longos	Bgy
Mabilao	Bgy
Nibaliw Central	Bgy
Nibaliw East	Bgy
Nibaliw Magliba	Bgy
Palapad	Bgy
Poblacion	Bgy
Rabon	Bgy
Sagud-Bahley	Bgy
Sobol	Bgy
Tempra-Guilig	Bgy
Tocok	Bgy
Lipit-Tomeeng	Bgy
Colisao	Bgy
Nibaliw Narvarte	Bgy
Nibaliw Vidal	Bgy
Alacan	Bgy
Cabaruan	Bgy
Inmalog Norte	Bgy
Longos-Amangonan-Parac-Parac Fabrica	Bgy
Longos Proper	Bgy
Tiblong	Bgy
San Jacinto	Mun
Awai	Bgy
Bolo	Bgy
Capaoay (Pob.)	Bgy
Casibong	Bgy
Imelda	Bgy
Guibel	Bgy
Labney	Bgy
Magsaysay	Bgy
Lobong	Bgy
Macayug	Bgy
Bagong Pag-asa	Bgy
San Guillermo	Bgy
San Jose	Bgy
San Juan	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santo Tomas	Bgy
San Manuel	Mun
San Antonio-Arzadon	Bgy
Cabacaraan	Bgy
Cabaritan	Bgy
Flores	Bgy
Guiset Norte (Pob.)	Bgy
Guiset Sur (Pob.)	Bgy
Lapalo	Bgy
Nagsaag	Bgy
Narra	Bgy
San Bonifacio	Bgy
San Juan	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Domingo	Bgy
San Nicolas	Mun
Bensican	Bgy
Cabitnongan	Bgy
Caboloan	Bgy
Cacabugaoan	Bgy
Calanutian	Bgy
Calaocan	Bgy
Camanggaan	Bgy
Camindoroan	Bgy
Casaratan	Bgy
Dalumpinas	Bgy
Fianza	Bgy
Lungao	Bgy
Malico	Bgy
Malilion	Bgy
Nagkaysa	Bgy
Nining	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Salingcob	Bgy
Salpad	Bgy
San Felipe East	Bgy
San Felipe West	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael Centro	Bgy
San Rafael East	Bgy
San Rafael West	Bgy
San Roque	Bgy
Santa Maria East	Bgy
Santa Maria West	Bgy
Santo Tomas	Bgy
Siblot	Bgy
Sobol	Bgy
San Quintin	Mun
Alac	Bgy
Baligayan	Bgy
Bantog	Bgy
Bolintaguen	Bgy
Cabangaran	Bgy
Cabalaoangan	Bgy
Calomboyan	Bgy
Carayacan	Bgy
Casantamarian	Bgy
Gonzalo	Bgy
Labuan	Bgy
Lagasit	Bgy
Lumayao	Bgy
Mabini	Bgy
Mantacdang	Bgy
Nangapugan	Bgy
San Pedro	Bgy
Ungib	Bgy
Poblacion Zone I	Bgy
Poblacion Zone II	Bgy
Poblacion Zone III	Bgy
Santa Barbara	Mun
Alibago	Bgy
Balingueo	Bgy
Banaoang	Bgy
Banzal	Bgy
Botao	Bgy
Cablong	Bgy
Carusocan	Bgy
Dalongue	Bgy
Erfe	Bgy
Gueguesangen	Bgy
Leet	Bgy
Malanay	Bgy
Maningding	Bgy
Maronong	Bgy
Maticmatic	Bgy
Minien East	Bgy
Minien West	Bgy
Nilombot	Bgy
Patayac	Bgy
Payas	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Sapang	Bgy
Sonquil	Bgy
Tebag East	Bgy
Tebag West	Bgy
Tuliao	Bgy
Ventinilla	Bgy
Primicias	Bgy
Santa Maria	Mun
Bal-loy	Bgy
Bantog	Bgy
Caboluan	Bgy
Cal-litang	Bgy
Capandanan	Bgy
Cauplasan	Bgy
Dalayap	Bgy
Libsong	Bgy
Namagbagan	Bgy
Paitan	Bgy
Pataquid	Bgy
Pilar	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Pugot	Bgy
Samon	Bgy
San Alejandro	Bgy
San Mariano	Bgy
San Pablo	Bgy
San Patricio	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Rosa	Bgy
Santo Tomas	Mun
La Luna	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Salvacion	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Jose	Bgy
San Marcos	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Sison	Mun
Agat	Bgy
Alibeng	Bgy
Amagbagan	Bgy
Artacho	Bgy
Asan Norte	Bgy
Asan Sur	Bgy
Bantay Insik	Bgy
Bila	Bgy
Binmeckeg	Bgy
Bulaoen East	Bgy
Bulaoen West	Bgy
Cabaritan	Bgy
Calunetan	Bgy
Camangaan	Bgy
Cauringan	Bgy
Dungon	Bgy
Esperanza	Bgy
Killo	Bgy
Labayug	Bgy
Paldit	Bgy
Pindangan	Bgy
Pinmilapil	Bgy
Poblacion Central	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Sagunto	Bgy
Inmalog	Bgy
Tara-tara	Bgy
Sual	Mun
Baquioen	Bgy
Baybay Norte	Bgy
Baybay Sur	Bgy
Bolaoen	Bgy
Cabalitian	Bgy
Calumbuyan	Bgy
Camagsingalan	Bgy
Caoayan	Bgy
Capantolan	Bgy
Macaycayawan	Bgy
Paitan East	Bgy
Paitan West	Bgy
Pangascasan	Bgy
Poblacion	Bgy
Santo Domingo	Bgy
Seselangen	Bgy
Sioasio East	Bgy
Sioasio West	Bgy
Victoria	Bgy
Tayug	Mun
Agno	Bgy
Amistad	Bgy
Barangobong	Bgy
Carriedo	Bgy
C. Lichauco	Bgy
Evangelista	Bgy
Guzon	Bgy
Lawak	Bgy
Legaspi	Bgy
Libertad	Bgy
Magallanes	Bgy
Panganiban	Bgy
Barangay A (Pob.)	Bgy
Barangay B (Pob.)	Bgy
Barangay C (Pob.)	Bgy
Barangay D (Pob.)	Bgy
Saleng	Bgy
Santo Domingo	Bgy
Toketec	Bgy
Trenchera	Bgy
Zamora	Bgy
Umingan	Mun
Abot Molina	Bgy
Alo-o	Bgy
Amaronan	Bgy
Annam	Bgy
Bantug	Bgy
Baracbac	Bgy
Barat	Bgy
Buenavista	Bgy
Cabalitian	Bgy
Cabaruan	Bgy
Cabatuan	Bgy
Cadiz	Bgy
Calitlitan	Bgy
Capas	Bgy
Carosalesan	Bgy
Casilan	Bgy
Caurdanetaan	Bgy
Concepcion	Bgy
Decreto	Bgy
Diaz	Bgy
Diket	Bgy
Don Justo Abalos	Bgy
Don Montano	Bgy
Esperanza	Bgy
Evangelista	Bgy
Flores	Bgy
Fulgosino	Bgy
Gonzales	Bgy
La Paz	Bgy
Labuan	Bgy
Lauren	Bgy
Lubong	Bgy
Luna Weste	Bgy
Luna Este	Bgy
Mantacdang	Bgy
Maseil-seil	Bgy
Nampalcan	Bgy
Nancalabasaan	Bgy
Pangangaan	Bgy
Papallasen	Bgy
Pemienta	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Prado	Bgy
Resurreccion	Bgy
Ricos	Bgy
San Andres	Bgy
San Juan	Bgy
San Leon	Bgy
San Pablo	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Santa Rosa	Bgy
Sinabaan	Bgy
Tanggal Sawang	Bgy
Cabangaran	Bgy
Carayungan Sur	Bgy
Del Rosario	Bgy
Urbiztondo	Mun
Angatel	Bgy
Balangay	Bgy
Batangcaoa	Bgy
Baug	Bgy
Bayaoas	Bgy
Bituag	Bgy
Camambugan	Bgy
Dalangiring	Bgy
Duplac	Bgy
Galarin	Bgy
Gueteb	Bgy
Malaca	Bgy
Malayo	Bgy
Malibong	Bgy
Pasibi East	Bgy
Pasibi West	Bgy
Pisuac	Bgy
Poblacion	Bgy
Real	Bgy
Salavante	Bgy
Sawat	Bgy
City of Urdaneta	City
Anonas	Bgy
Bactad East	Bgy
Dr. Pedro T. Orata	Bgy
Bayaoas	Bgy
Bolaoen	Bgy
Cabaruan	Bgy
Cabuloan	Bgy
Camanang	Bgy
Camantiles	Bgy
Casantaan	Bgy
Catablan	Bgy
Cayambanan	Bgy
Consolacion	Bgy
Dilan Paurido	Bgy
Labit Proper	Bgy
Labit West	Bgy
Mabanogbog	Bgy
Macalong	Bgy
Nancalobasaan	Bgy
Nancamaliran East	Bgy
Nancamaliran West	Bgy
Nancayasan	Bgy
Oltama	Bgy
Palina East	Bgy
Palina West	Bgy
Pinmaludpod	Bgy
Poblacion	Bgy
San Jose	Bgy
San Vicente	Bgy
Santa Lucia	Bgy
Santo Domingo	Bgy
Sugcong	Bgy
Tipuso	Bgy
Tulong	Bgy
Villasis	Mun
Amamperez	Bgy
Bacag	Bgy
Barangobong	Bgy
Barraca	Bgy
Capulaan	Bgy
Caramutan	Bgy
La Paz	Bgy
Labit	Bgy
Lipay	Bgy
Lomboy	Bgy
Piaz	Bgy
Zone V (Pob.)	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Puelay	Bgy
San Blas	Bgy
San Nicolas	Bgy
Tombod	Bgy
Unzad	Bgy
Laoac	Mun
Anis	Bgy
Botique	Bgy
Caaringayan	Bgy
Domingo Alarcio	Bgy
Cabilaoan West	Bgy
Cabulalaan	Bgy
Calaoagan	Bgy
Calmay	Bgy
Casampagaan	Bgy
Casanestebanan	Bgy
Casantiagoan	Bgy
Inmanduyan	Bgy
Poblacion	Bgy
Lebueg	Bgy
Maraboc	Bgy
Nanbagatan	Bgy
Panaga	Bgy
Talogtog	Bgy
Turko	Bgy
Yatyat	Bgy
Balligi	Bgy
Banuar	Bgy
Region II (Cagayan Valley)	Reg
Batanes	Prov
Basco (Capital)	Mun
Ihubok II	Bgy
Ihubok I	Bgy
San Antonio	Bgy
San Joaquin	Bgy
Chanarian	Bgy
Kayhuvokan	Bgy
Itbayat	Mun
Raele	Bgy
San Rafael	Bgy
Santa Lucia	Bgy
Santa Maria	Bgy
Santa Rosa	Bgy
Ivana	Mun
Radiwan	Bgy
Salagao	Bgy
San Vicente	Bgy
Tuhel (Pob.)	Bgy
Mahatao	Mun
Hañib	Bgy
Kaumbakan	Bgy
Panatayan	Bgy
Uvoy (Pob.)	Bgy
Sabtang	Mun
Chavayan	Bgy
Malakdang (Pob.)	Bgy
Nakanmuan	Bgy
Savidug	Bgy
Sinakan (Pob.)	Bgy
Sumnanga	Bgy
Uyugan	Mun
Kayvaluganan (Pob.)	Bgy
Imnajbu	Bgy
Itbud	Bgy
Kayuganan (Pob.)	Bgy
Cagayan	Prov
Abulug	Mun
Alinunu	Bgy
Bagu	Bgy
Banguian	Bgy
Calog Norte	Bgy
Calog Sur	Bgy
Canayun	Bgy
Centro (Pob.)	Bgy
Dana-Ili	Bgy
Guiddam	Bgy
Libertad	Bgy
Lucban	Bgy
Pinili	Bgy
Santa Filomena	Bgy
Santo Tomas	Bgy
Siguiran	Bgy
Simayung	Bgy
Sirit	Bgy
San Agustin	Bgy
San Julian	Bgy
Santa Rosa	Bgy
Alcala	Mun
Abbeg	Bgy
Afusing Bato	Bgy
Afusing Daga	Bgy
Agani	Bgy
Baculod	Bgy
Baybayog	Bgy
Cabuluan	Bgy
Calantac	Bgy
Carallangan	Bgy
Centro Norte (Pob.)	Bgy
Centro Sur (Pob.)	Bgy
Dalaoig	Bgy
Damurog	Bgy
Jurisdiction	Bgy
Malalatan	Bgy
Maraburab	Bgy
Masin	Bgy
Pagbangkeruan	Bgy
Pared	Bgy
Piggatan	Bgy
Pinopoc	Bgy
Pussian	Bgy
San Esteban	Bgy
Tamban	Bgy
Tupang	Bgy
Allacapan	Mun
Bessang	Bgy
Binobongan	Bgy
Bulo	Bgy
Burot	Bgy
Capagaran	Bgy
Capalutan	Bgy
Capanickian Norte	Bgy
Capanickian Sur	Bgy
Cataratan	Bgy
Centro East (Pob.)	Bgy
Centro West (Pob.)	Bgy
Daan-Ili	Bgy
Dagupan	Bgy
Dalayap	Bgy
Gagaddangan	Bgy
Iringan	Bgy
Labben	Bgy
Maluyo	Bgy
Mapurao	Bgy
Matucay	Bgy
Nagattatan	Bgy
Pacac	Bgy
San Juan	Bgy
Silangan	Bgy
Tamboli	Bgy
Tubel	Bgy
Utan	Bgy
Amulung	Mun
Abolo	Bgy
Agguirit	Bgy
Alitungtung	Bgy
Annabuculan	Bgy
Annafatan	Bgy
Anquiray	Bgy
Babayuan	Bgy
Baccuit	Bgy
Bacring	Bgy
Baculud	Bgy
Balauini	Bgy
Bauan	Bgy
Bayabat	Bgy
Calamagui	Bgy
Calintaan	Bgy
Caratacat	Bgy
Casingsingan Norte	Bgy
Casingsingan Sur	Bgy
Catarauan	Bgy
Centro	Bgy
Concepcion	Bgy
Cordova	Bgy
Dadda	Bgy
Dafunganay	Bgy
Dugayung	Bgy
Estefania	Bgy
Gabut	Bgy
Gangauan	Bgy
Goran	Bgy
Jurisdiccion	Bgy
La Suerte	Bgy
Logung	Bgy
Magogod	Bgy
Manalo	Bgy
Marobbob	Bgy
Masical	Bgy
Monte Alegre	Bgy
Nabbialan	Bgy
Nagsabaran	Bgy
Nangalasauan	Bgy
Nanuccauan	Bgy
Pacac-Grande	Bgy
Pacac-Pequeño	Bgy
Palacu	Bgy
Palayag	Bgy
Tana	Bgy
Unag	Bgy
Aparri	Mun
Backiling	Bgy
Bangag	Bgy
Binalan	Bgy
Bisagu	Bgy
Centro 1 (Pob.)	Bgy
Centro 2 (Pob.)	Bgy
Centro 3 (Pob.)	Bgy
Centro 4 (Pob.)	Bgy
Centro 5 (Pob.)	Bgy
Centro 6 (Pob.)	Bgy
Centro 7 (Pob.)	Bgy
Centro 8 (Pob.)	Bgy
Centro 9 (Pob.)	Bgy
Centro 10 (Pob.)	Bgy
Centro 11 (Pob.)	Bgy
Centro 12 (Pob.)	Bgy
Centro 13 (Pob.)	Bgy
Centro 14 (Pob.)	Bgy
Bukig	Bgy
Bulala Norte	Bgy
Bulala Sur	Bgy
Caagaman	Bgy
Centro 15 (Pob.)	Bgy
Dodan	Bgy
Fuga Island	Bgy
Gaddang	Bgy
Linao	Bgy
Mabanguc	Bgy
Macanaya	Bgy
Maura	Bgy
Minanga	Bgy
Navagan	Bgy
Paddaya	Bgy
Paruddun Norte	Bgy
Paruddun Sur	Bgy
Plaza	Bgy
Punta	Bgy
San Antonio	Bgy
Tallungan	Bgy
Toran	Bgy
Sanja	Bgy
Zinarag	Bgy
Baggao	Mun
Adaoag	Bgy
Agaman	Bgy
Alba	Bgy
Annayatan	Bgy
Asassi	Bgy
Asinga-Via	Bgy
Awallan	Bgy
Bacagan	Bgy
Bagunot	Bgy
Barsat East	Bgy
Barsat West	Bgy
Bitag Grande	Bgy
Bitag Pequeño	Bgy
Bunugan	Bgy
Canagatan	Bgy
Carupian	Bgy
Catugay	Bgy
Poblacion	Bgy
Dabbac Grande	Bgy
Dalin	Bgy
Dalla	Bgy
Hacienda Intal	Bgy
Ibulo	Bgy
Imurong	Bgy
J. Pallagao	Bgy
Lasilat	Bgy
Masical	Bgy
Mocag	Bgy
Nangalinan	Bgy
Remus	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Vicente	Bgy
Santa Margarita	Bgy
Santor	Bgy
Taguing	Bgy
Taguntungan	Bgy
Tallang	Bgy
Temblique	Bgy
Taytay	Bgy
Tungel	Bgy
Mabini	Bgy
Agaman Norte	Bgy
Agaman Sur	Bgy
C. Verzosa	Bgy
Ballesteros	Mun
Ammubuan	Bgy
Baran	Bgy
Cabaritan East	Bgy
Cabaritan West	Bgy
Cabayu	Bgy
Cabuluan East	Bgy
Cabuluan West	Bgy
Centro East (Pob.)	Bgy
Centro West (Pob.)	Bgy
Fugu	Bgy
Mabuttal East	Bgy
Mabuttal West	Bgy
Nararagan	Bgy
Palloc	Bgy
Payagan East	Bgy
Payagan West	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Zitanga	Bgy
Buguey	Mun
Ballang	Bgy
Balza	Bgy
Cabaritan	Bgy
Calamegatan	Bgy
Centro (Pob.)	Bgy
Centro West	Bgy
Dalaya	Bgy
Fula	Bgy
Leron	Bgy
Antiporda	Bgy
Maddalero	Bgy
Mala Este	Bgy
Mala Weste	Bgy
Minanga Este	Bgy
Paddaya Este	Bgy
Pattao	Bgy
Quinawegan	Bgy
Remebella	Bgy
San Isidro	Bgy
Santa Isabel	Bgy
Santa Maria	Bgy
Tabbac	Bgy
Villa Cielo	Bgy
Alucao Weste	Bgy
Minanga Weste	Bgy
Paddaya Weste	Bgy
San Juan	Bgy
San Vicente	Bgy
Villa Gracia	Bgy
Villa Leonora	Bgy
Calayan	Mun
Cabudadan	Bgy
Balatubat	Bgy
Dadao	Bgy
Dibay	Bgy
Dilam	Bgy
Magsidel	Bgy
Naguilian	Bgy
Poblacion	Bgy
Babuyan Claro	Bgy
Centro II	Bgy
Dalupiri	Bgy
Minabel	Bgy
Camalaniugan	Mun
Abagao	Bgy
Afunan Cabayu	Bgy
Agusi	Bgy
Alilinu	Bgy
Baggao	Bgy
Bantay	Bgy
Bulala	Bgy
Casili Norte	Bgy
Catotoran Norte	Bgy
Centro Norte (Pob.)	Bgy
Centro Sur (Pob.)	Bgy
Cullit	Bgy
Dacal-Lafugu	Bgy
Dammang Norte	Bgy
Dugo	Bgy
Fusina	Bgy
Gang-ngo	Bgy
Jurisdiction	Bgy
Luec	Bgy
Minanga	Bgy
Paragat	Bgy
Tagum	Bgy
Tuluttuging	Bgy
Ziminila	Bgy
Casili Sur	Bgy
Catotoran Sur	Bgy
Dammang Sur	Bgy
Sapping	Bgy
Claveria	Mun
Alimoan	Bgy
Bacsay Cataraoan Norte	Bgy
Bacsay Mapulapula	Bgy
Bilibigao	Bgy
Buenavista	Bgy
Cadcadir East	Bgy
Capanikian	Bgy
Centro I (Pob.)	Bgy
Centro II (Pob.)	Bgy
Culao	Bgy
Dibalio	Bgy
Kilkiling	Bgy
Lablabig	Bgy
Luzon	Bgy
Mabnang	Bgy
Magdalena	Bgy
Centro VII	Bgy
Malilitao	Bgy
Centro VI	Bgy
Nagsabaran	Bgy
Centro IV	Bgy
Pata East	Bgy
Pinas	Bgy
Santiago	Bgy
Santo Tomas	Bgy
Santa Maria	Bgy
Tabbugan	Bgy
Taggat Norte	Bgy
Union	Bgy
Bacsay Cataraoan Sur	Bgy
Cadcadir West	Bgy
Camalaggoan/D Leaño	Bgy
Centro III	Bgy
Centro V	Bgy
Centro VIII	Bgy
Pata West	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Taggat Sur	Bgy
Enrile	Mun
Alibago	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III	Bgy
Divisoria	Bgy
Inga	Bgy
Lanna	Bgy
Lemu Norte	Bgy
Liwan Norte	Bgy
Liwan Sur	Bgy
Maddarulug Norte	Bgy
Magalalag East	Bgy
Maracuru	Bgy
Barangay IV (Pob.)	Bgy
Roma Norte	Bgy
Barangay III-A	Bgy
Batu	Bgy
Lemu Sur	Bgy
Maddarulug Sur	Bgy
Magalalag West	Bgy
Roma Sur	Bgy
San Antonio	Bgy
Gattaran	Mun
Abra	Bgy
Aguiguican	Bgy
Bangatan Ngagan	Bgy
Baracaoit	Bgy
Baraoidan	Bgy
Barbarit	Bgy
Basao	Bgy
Cabayu	Bgy
Calaoagan Bassit	Bgy
Calaoagan Dackel	Bgy
Capiddigan	Bgy
Capissayan Norte	Bgy
Capissayan Sur	Bgy
Casicallan Sur	Bgy
Casicallan Norte	Bgy
Centro Norte (Pob.)	Bgy
Centro Sur (Pob.)	Bgy
Cullit	Bgy
Cumao	Bgy
Cunig	Bgy
Dummun	Bgy
Fugu	Bgy
Ganzano	Bgy
Guising	Bgy
Langgan	Bgy
Lapogan	Bgy
L. Adviento	Bgy
Mabuno	Bgy
Nabaccayan	Bgy
Naddungan	Bgy
Nagatutuan	Bgy
Nassiping	Bgy
Newagac	Bgy
Palagao Norte	Bgy
Palagao Sur	Bgy
Piña Este	Bgy
Piña Weste	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Sidem	Bgy
Santa Ana	Bgy
Tagumay	Bgy
Takiki	Bgy
Taligan	Bgy
Tanglagan	Bgy
T. Elizaga	Bgy
Tubungan Este	Bgy
Tubungan Weste	Bgy
Bolos Point	Bgy
San Carlos	Bgy
Gonzaga	Mun
Amunitan	Bgy
Batangan	Bgy
Baua	Bgy
Cabanbanan Norte	Bgy
Cabanbanan Sur	Bgy
Cabiraoan	Bgy
Callao	Bgy
Calayan	Bgy
Caroan	Bgy
Casitan	Bgy
Flourishing (Pob.)	Bgy
Ipil	Bgy
Isca	Bgy
Magrafil	Bgy
Minanga	Bgy
Rebecca	Bgy
Paradise (Pob.)	Bgy
Pateng	Bgy
Progressive (Pob.)	Bgy
San Jose	Bgy
Santa Clara	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Smart (Pob.)	Bgy
Tapel	Bgy
Iguig	Mun
Ajat (Pob.)	Bgy
Atulu	Bgy
Baculud	Bgy
Bayo	Bgy
Campo	Bgy
San Esteban	Bgy
Dumpao	Bgy
Gammad	Bgy
Santa Teresa	Bgy
Garab	Bgy
Malabbac	Bgy
Manaoag	Bgy
Minanga Norte	Bgy
Minanga Sur	Bgy
Nattanzan (Pob.)	Bgy
Redondo	Bgy
Salamague	Bgy
San Isidro	Bgy
San Lorenzo	Bgy
Santa Barbara	Bgy
Santa Rosa	Bgy
Santiago	Bgy
San Vicente	Bgy
Lal-Lo	Mun
Abagao	Bgy
Alaguia	Bgy
Bagumbayan	Bgy
Bangag	Bgy
Bical	Bgy
Bicud	Bgy
Binag	Bgy
Cabayabasan	Bgy
Cagoran	Bgy
Cambong	Bgy
Catayauan	Bgy
Catugan	Bgy
Centro (Pob.)	Bgy
Cullit	Bgy
Dagupan	Bgy
Dalaya	Bgy
Fabrica	Bgy
Fusina	Bgy
Jurisdiction	Bgy
Lalafugan	Bgy
Logac	Bgy
Magallungon (Sta. Teresa)	Bgy
Magapit	Bgy
Malanao	Bgy
Maxingal	Bgy
Naguilian	Bgy
Paranum	Bgy
Rosario	Bgy
San Antonio	Bgy
San Jose	Bgy
San Juan	Bgy
San Lorenzo	Bgy
San Mariano	Bgy
Santa Maria	Bgy
Tucalana	Bgy
Lasam	Mun
Aggunetan	Bgy
Alannay	Bgy
Battalan	Bgy
Calapangan Norte	Bgy
Calapangan Sur	Bgy
Callao Norte	Bgy
Callao Sur	Bgy
Cataliganan	Bgy
Finugo Norte	Bgy
Gabun	Bgy
Ignacio Jurado	Bgy
Magsaysay	Bgy
Malinta	Bgy
Minanga Sur	Bgy
Minanga Norte	Bgy
Nicolas Agatep	Bgy
Peru	Bgy
Centro I (Pob.)	Bgy
San Pedro	Bgy
Sicalao	Bgy
Tagao	Bgy
Tucalan Passing	Bgy
Viga	Bgy
Cabatacan East	Bgy
Cabatacan West	Bgy
Nabannagan East	Bgy
Nabannagan West	Bgy
Centro II (Pob.)	Bgy
Centro III (Pob.)	Bgy
New Orlins	Bgy
Pamplona	Mun
Abanqueruan	Bgy
Allasitan	Bgy
Bagu	Bgy
Balingit	Bgy
Bidduang	Bgy
Cabaggan	Bgy
Capalalian	Bgy
Casitan	Bgy
Centro (Pob.)	Bgy
Curva	Bgy
Gattu	Bgy
Masi	Bgy
Nagattatan	Bgy
Nagtupacan	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Tabba	Bgy
Tupanna	Bgy
Peñablanca	Mun
Aggugaddan	Bgy
Alimanao	Bgy
Baliuag	Bgy
Bical	Bgy
Bugatay	Bgy
Buyun	Bgy
Cabasan	Bgy
Cabbo	Bgy
Callao	Bgy
Camasi	Bgy
Centro (Pob.)	Bgy
Dodan	Bgy
Lapi	Bgy
Malibabag	Bgy
Manga	Bgy
Minanga	Bgy
Nabbabalayan	Bgy
Nanguilattan	Bgy
Nannarian	Bgy
Parabba	Bgy
Patagueleg	Bgy
Quibal	Bgy
San Roque	Bgy
Sisim	Bgy
Piat	Mun
Apayao	Bgy
Aquib	Bgy
Dugayung	Bgy
Gumarueng	Bgy
Macapil	Bgy
Maguilling	Bgy
Minanga	Bgy
Poblacion I	Bgy
Santa Barbara	Bgy
Santo Domingo	Bgy
Sicatna	Bgy
Villa Rey	Bgy
Warat	Bgy
Baung	Bgy
Calaoagan	Bgy
Catarauan	Bgy
Poblacion II	Bgy
Villa Reyno	Bgy
Rizal	Mun
Anagguan	Bgy
Anurturu	Bgy
Anungu	Bgy
Balungcanag	Bgy
Batu	Bgy
Cambabangan	Bgy
Capacuan	Bgy
Dunggan	Bgy
Duyun	Bgy
Gaddangao	Bgy
Gaggabutan East	Bgy
Illuru Norte	Bgy
Lattut	Bgy
Linno	Bgy
Liwan	Bgy
Mabbang	Bgy
Mauanan	Bgy
Masi	Bgy
Minanga	Bgy
Nanauatan	Bgy
Nanungaran	Bgy
Pasingan	Bgy
Poblacion	Bgy
San Juan	Bgy
Sinicking	Bgy
Battut	Bgy
Bural	Bgy
Gaggabutan West	Bgy
Illuru Sur	Bgy
Sanchez-Mira	Mun
Bangan	Bgy
Callungan	Bgy
Centro I (Pob.)	Bgy
Centro II (Pob.)	Bgy
Dacal	Bgy
Dagueray	Bgy
Dammang	Bgy
Kittag	Bgy
Langagan	Bgy
Magacan	Bgy
Marzan	Bgy
Masisit	Bgy
Nagrangtayan	Bgy
Namuac	Bgy
San Andres	Bgy
Santiago	Bgy
Santor	Bgy
Tokitok	Bgy
Santa Ana	Mun
Casagan	Bgy
Casambalangan	Bgy
Centro (Pob.)	Bgy
Diora-Zinungan	Bgy
Dungeg	Bgy
Kapanikian	Bgy
Marede	Bgy
Palawig	Bgy
Batu-Parada	Bgy
Patunungan	Bgy
Rapuli	Bgy
San Vicente	Bgy
Santa Clara	Bgy
Santa Cruz	Bgy
Visitacion (Pob.)	Bgy
Tangatan	Bgy
Santa Praxedes	Mun
Cadongdongan	Bgy
Capacuan	Bgy
Centro I (Pob.)	Bgy
Centro II (Pob.)	Bgy
Macatel	Bgy
Portabaga	Bgy
San Juan	Bgy
San Miguel	Bgy
Salungsong	Bgy
Sicul	Bgy
Santa Teresita	Mun
Alucao	Bgy
Buyun	Bgy
Centro East (Pob.)	Bgy
Dungeg	Bgy
Luga	Bgy
Masi	Bgy
Mission	Bgy
Simpatuyo	Bgy
Villa	Bgy
Aridawen	Bgy
Caniugan	Bgy
Centro West	Bgy
Simbaluca	Bgy
Santo Niño	Mun
Abariongan Ruar	Bgy
Abariongan Uneg	Bgy
Balagan	Bgy
Balanni	Bgy
Cabayo	Bgy
Calapangan	Bgy
Calassitan	Bgy
Campo	Bgy
Centro Norte (Pob.)	Bgy
Centro Sur (Pob.)	Bgy
Dungao	Bgy
Lattac	Bgy
Lipatan	Bgy
Lubo	Bgy
Mabitbitnong	Bgy
Mapitac	Bgy
Masical	Bgy
Matalao	Bgy
Nag-uma	Bgy
Namuccayan	Bgy
Niug Norte	Bgy
Niug Sur	Bgy
Palusao	Bgy
San Manuel	Bgy
San Roque	Bgy
Santa Felicitas	Bgy
Santa Maria	Bgy
Sidiran	Bgy
Tabang	Bgy
Tamucco	Bgy
Virginia	Bgy
Solana	Mun
Andarayan North	Bgy
Lannig	Bgy
Bangag	Bgy
Bantay	Bgy
Basi East	Bgy
Bauan East	Bgy
Cadaanan	Bgy
Calamagui	Bgy
Carilucud	Bgy
Cattaran	Bgy
Centro Northeast (Pob.)	Bgy
Centro Northwest (Pob.)	Bgy
Centro Southeast (Pob.)	Bgy
Centro Southwest (Pob.)	Bgy
Lanna	Bgy
Lingu	Bgy
Maguirig	Bgy
Nabbotuan	Bgy
Nangalisan	Bgy
Natappian East	Bgy
Padul	Bgy
Palao	Bgy
Parug-parug	Bgy
Pataya	Bgy
Sampaguita	Bgy
Maddarulug	Bgy
Ubong	Bgy
Dassun	Bgy
Furagui	Bgy
Gadu	Bgy
Iraga	Bgy
Andarayan South	Bgy
Basi West	Bgy
Bauan West	Bgy
Calillauan	Bgy
Gen. Eulogio Balao	Bgy
Natappian West	Bgy
Malalam-Malacabibi	Bgy
Tuao	Mun
Accusilian	Bgy
Alabiao	Bgy
Alabug	Bgy
Angang	Bgy
Bagumbayan	Bgy
Barancuag	Bgy
Battung	Bgy
Bicok	Bgy
Bugnay	Bgy
Balagao	Bgy
Cagumitan	Bgy
Cato	Bgy
Culong	Bgy
Dagupan	Bgy
Fugu	Bgy
Lakambini	Bgy
Lallayug	Bgy
Malumin	Bgy
Mambacag	Bgy
San Vicente	Bgy
Mungo	Bgy
Naruangan	Bgy
Palca	Bgy
Pata	Bgy
San Juan	Bgy
San Luis	Bgy
Santo Tomas	Bgy
Taribubu	Bgy
Villa Laida	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Malalinta	Bgy
Tuguegarao City (Capital)	City
Annafunan East	Bgy
Atulayan Norte	Bgy
Bagay	Bgy
Centro 1 (Pob.)	Bgy
Centro 4 (Pob.)	Bgy
Centro 5 (Pob.)	Bgy
Centro 6 (Pob.)	Bgy
Centro 7 (Pob.)	Bgy
Centro 8 (Pob.)	Bgy
Centro 9 (Pob.)	Bgy
Centro 10 (Pob.)	Bgy
Centro 11 (Pob.)	Bgy
Buntun	Bgy
Caggay	Bgy
Capatan	Bgy
Carig	Bgy
Caritan Norte	Bgy
Caritan Sur	Bgy
Cataggaman Nuevo	Bgy
Cataggaman Viejo	Bgy
Gosi Norte	Bgy
Larion Alto	Bgy
Larion Bajo	Bgy
Libag Norte	Bgy
Linao East	Bgy
Nambbalan Norte	Bgy
Pallua Norte	Bgy
Pengue	Bgy
Tagga	Bgy
Tanza	Bgy
Ugac Norte	Bgy
Centro 2 (Pob.)	Bgy
Centro 3 (Pob.)	Bgy
Centro 12 (Pob.)	Bgy
Annafunan West	Bgy
Atulayan Sur	Bgy
Caritan Centro	Bgy
Cataggaman Pardo	Bgy
Dadda	Bgy
Gosi Sur	Bgy
Leonarda	Bgy
Libag Sur	Bgy
Linao Norte	Bgy
Linao West	Bgy
Nambbalan Sur	Bgy
Pallua Sur	Bgy
Reyes	Bgy
San Gabriel	Bgy
Ugac Sur	Bgy
Isabela	Prov
Alicia	Mun
Amistad	Bgy
Antonino (Pob.)	Bgy
Apanay	Bgy
Aurora	Bgy
Bagnos	Bgy
Bagong Sikat	Bgy
Bantug-Petines	Bgy
Bonifacio	Bgy
Burgos	Bgy
Calaocan (Pob.)	Bgy
Callao	Bgy
Dagupan	Bgy
Inanama	Bgy
Linglingay	Bgy
M.H. del Pilar	Bgy
Mabini	Bgy
Magsaysay (Pob.)	Bgy
Mataas na Kahoy	Bgy
Paddad	Bgy
Rizal	Bgy
Rizaluna	Bgy
Salvacion	Bgy
San Antonio (Pob.)	Bgy
San Fernando	Bgy
San Francisco	Bgy
San Juan	Bgy
San Pablo	Bgy
San Pedro	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santo Domingo	Bgy
Santo Tomas	Bgy
Victoria	Bgy
Zamora	Bgy
Angadanan	Mun
Allangigan	Bgy
Aniog	Bgy
Baniket	Bgy
Bannawag	Bgy
Bantug	Bgy
Barangcuag	Bgy
Baui	Bgy
Bonifacio	Bgy
Buenavista	Bgy
Bunnay	Bgy
Calabayan-Minanga	Bgy
Calaccab	Bgy
Calaocan	Bgy
Kalusutan	Bgy
Campanario	Bgy
Canangan	Bgy
Centro I (Pob.)	Bgy
Centro II (Pob.)	Bgy
Centro III (Pob.)	Bgy
Consular	Bgy
Cumu	Bgy
Dalakip	Bgy
Dalenat	Bgy
Dipaluda	Bgy
Duroc	Bgy
Lourdes	Bgy
Esperanza	Bgy
Fugaru	Bgy
Liwliwa	Bgy
Ingud Norte	Bgy
Ingud Sur	Bgy
La Suerte	Bgy
Lomboy	Bgy
Loria	Bgy
Mabuhay	Bgy
Macalauat	Bgy
Macaniao	Bgy
Malannao	Bgy
Malasin	Bgy
Mangandingay	Bgy
Minanga Proper	Bgy
Pappat	Bgy
Pissay	Bgy
Ramona	Bgy
Rancho Bassit	Bgy
Rang-ayan	Bgy
Salay	Bgy
San Ambrocio	Bgy
San Guillermo	Bgy
San Isidro	Bgy
San Marcelo	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Saranay	Bgy
Sinabbaran	Bgy
Victory	Bgy
Viga	Bgy
Villa Domingo	Bgy
Aurora	Mun
Apiat	Bgy
Bagnos	Bgy
Bagong Tanza	Bgy
Ballesteros	Bgy
Bannagao	Bgy
Bannawag	Bgy
Bolinao	Bgy
Caipilan	Bgy
Camarunggayan	Bgy
Dalig-Kalinga	Bgy
Diamantina	Bgy
Divisoria	Bgy
Esperanza East	Bgy
Esperanza West	Bgy
Kalabaza	Bgy
Rizaluna	Bgy
Macatal	Bgy
Malasin	Bgy
Nampicuan	Bgy
Villa Nuesa	Bgy
Panecien	Bgy
San Andres	Bgy
San Jose (Pob.)	Bgy
San Rafael	Bgy
San Ramon	Bgy
Santa Rita	Bgy
Santa Rosa	Bgy
Saranay	Bgy
Sili	Bgy
Victoria	Bgy
Villa Fugu	Bgy
San Juan (Pob.)	Bgy
San Pedro-San Pablo (Pob.)	Bgy
Benito Soliven	Mun
Andabuen	Bgy
Ara	Bgy
Binogtungan	Bgy
Capuseran	Bgy
Dagupan	Bgy
Danipa	Bgy
District II (Pob.)	Bgy
Gomez	Bgy
Guilingan	Bgy
La Salette	Bgy
Makindol	Bgy
Maluno Norte	Bgy
Maluno Sur	Bgy
Nacalma	Bgy
New Magsaysay	Bgy
District I (Pob.)	Bgy
Punit	Bgy
San Carlos	Bgy
San Francisco	Bgy
Santa Cruz	Bgy
Sevillana	Bgy
Sinipit	Bgy
Lucban	Bgy
Villaluz	Bgy
Yeban Norte	Bgy
Yeban Sur	Bgy
Santiago	Bgy
Placer	Bgy
Balliao	Bgy
Burgos	Mun
Bacnor East	Bgy
Bacnor West	Bgy
Caliguian (Pob.)	Bgy
Catabban	Bgy
Cullalabo Del Norte	Bgy
Cullalabo San Antonio	Bgy
Cullalabo Del Sur	Bgy
Dalig	Bgy
Malasin	Bgy
Masigun	Bgy
Raniag	Bgy
San Bonifacio	Bgy
San Miguel	Bgy
San Roque	Bgy
Cabagan	Mun
Aggub	Bgy
Anao	Bgy
Angancasilian	Bgy
Balasig	Bgy
Cansan	Bgy
Casibarag Norte	Bgy
Casibarag Sur	Bgy
Catabayungan	Bgy
Cubag	Bgy
Garita	Bgy
Luquilu	Bgy
Mabangug	Bgy
Magassi	Bgy
Ngarag	Bgy
Pilig Abajo	Bgy
Pilig Alto	Bgy
Centro (Pob.)	Bgy
San Bernardo	Bgy
San Juan	Bgy
Saui	Bgy
Tallag	Bgy
Ugad	Bgy
Union	Bgy
Masipi East	Bgy
Masipi West	Bgy
San Antonio	Bgy
Cabatuan	Mun
Rang-ay	Bgy
Calaocan	Bgy
Canan	Bgy
Centro (Pob.)	Bgy
Culing Centro	Bgy
Culing East	Bgy
Culing West	Bgy
Del Corpuz	Bgy
Del Pilar	Bgy
Diamantina	Bgy
La Paz	Bgy
Luzon	Bgy
Macalaoat	Bgy
Magdalena	Bgy
Magsaysay	Bgy
Namnama	Bgy
Nueva Era	Bgy
Paraiso	Bgy
Sampaloc	Bgy
San Andres	Bgy
Saranay	Bgy
Tandul	Bgy
City of Cauayan	City
Alicaocao	Bgy
Alinam	Bgy
Amobocan	Bgy
Andarayan	Bgy
Baculod	Bgy
Baringin Norte	Bgy
Baringin Sur	Bgy
Buena Suerte	Bgy
Bugallon	Bgy
Buyon	Bgy
Cabaruan	Bgy
Cabugao	Bgy
Carabatan Chica	Bgy
Carabatan Grande	Bgy
Carabatan Punta	Bgy
Carabatan Bacareno	Bgy
Casalatan	Bgy
San Pablo	Bgy
Cassap Fuera	Bgy
Catalina	Bgy
Culalabat	Bgy
Dabburab	Bgy
De Vera	Bgy
Dianao	Bgy
Disimuray	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
Duminit	Bgy
Faustino	Bgy
Gagabutan	Bgy
Gappal	Bgy
Guayabal	Bgy
Labinab	Bgy
Linglingay	Bgy
Mabantad	Bgy
Maligaya	Bgy
Manaoag	Bgy
Marabulig I	Bgy
Marabulig II	Bgy
Minante I	Bgy
Minante II	Bgy
Nagcampegan	Bgy
Naganacan	Bgy
Nagrumbuan	Bgy
Nungnungan I	Bgy
Nungnungan II	Bgy
Pinoma	Bgy
Rizal	Bgy
Rogus	Bgy
San Antonio	Bgy
San Fermin	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Luis	Bgy
Santa Luciana	Bgy
Santa Maria	Bgy
Sillawit	Bgy
Sinippil	Bgy
Tagaran	Bgy
Turayong	Bgy
Union	Bgy
Villa Concepcion	Bgy
Villa Luna	Bgy
Villaflor	Bgy
Cordon	Mun
Aguinaldo	Bgy
Calimaturod	Bgy
Capirpiriwan	Bgy
Caquilingan	Bgy
Dallao	Bgy
Gayong	Bgy
Laurel	Bgy
Magsaysay	Bgy
Malapat	Bgy
Osmena	Bgy
Quezon	Bgy
Quirino	Bgy
Rizaluna	Bgy
Roxas Pob.	Bgy
Sagat	Bgy
San Juan	Bgy
Taliktik	Bgy
Tanggal	Bgy
Tarinsing	Bgy
Turod Norte	Bgy
Turod Sur	Bgy
Villamiemban	Bgy
Villamarzo	Bgy
Anonang	Bgy
Camarao	Bgy
Wigan	Bgy
Dinapigue	Mun
Ayod	Bgy
Bucal Sur	Bgy
Bucal Norte	Bgy
Dibulo	Bgy
Digumased (Pob.)	Bgy
Dimaluade	Bgy
Divilacan	Mun
Dicambangan	Bgy
Dicaroyan	Bgy
Dicatian	Bgy
Bicobian	Bgy
Dilakit	Bgy
Dimapnat	Bgy
Dimapula (Pob.)	Bgy
Dimasalansan	Bgy
Dipudo	Bgy
Dibulos	Bgy
Ditarum	Bgy
Sapinit	Bgy
Echague	Mun
Angoluan	Bgy
Annafunan	Bgy
Arabiat	Bgy
Aromin	Bgy
Babaran	Bgy
Bacradal	Bgy
Benguet	Bgy
Buneg	Bgy
Busilelao	Bgy
Caniguing	Bgy
Carulay	Bgy
Castillo	Bgy
Dammang East	Bgy
Dammang West	Bgy
Dicaraoyan	Bgy
Dugayong	Bgy
Fugu	Bgy
Garit Norte	Bgy
Garit Sur	Bgy
Gucab	Bgy
Gumbauan	Bgy
Ipil	Bgy
Libertad	Bgy
Mabbayad	Bgy
Mabuhay	Bgy
Madadamian	Bgy
Magleticia	Bgy
Malibago	Bgy
Maligaya	Bgy
Malitao	Bgy
Narra	Bgy
Nilumisu	Bgy
Pag-asa	Bgy
Pangal Norte	Bgy
Pangal Sur	Bgy
Rumang-ay	Bgy
Salay	Bgy
Salvacion	Bgy
San Antonio Ugad	Bgy
San Antonio Minit	Bgy
San Carlos	Bgy
San Fabian	Bgy
San Felipe	Bgy
San Juan	Bgy
San Manuel	Bgy
San Miguel	Bgy
San Salvador	Bgy
Santa Ana	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santa Monica	Bgy
Santo Domingo	Bgy
Silauan Sur (Pob.)	Bgy
Silauan Norte (Pob.)	Bgy
Sinabbaran	Bgy
Soyung	Bgy
Taggappan	Bgy
Tuguegarao	Bgy
Villa Campo	Bgy
Villa Fermin	Bgy
Villa Rey	Bgy
Villa Victoria	Bgy
Cabugao (Pob.)	Bgy
Diasan	Bgy
Gamu	Mun
Barcolan	Bgy
Buenavista	Bgy
Dammao	Bgy
Furao	Bgy
Guibang	Bgy
Lenzon	Bgy
Linglingay	Bgy
Mabini	Bgy
Pintor	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
Rizal	Bgy
Songsong	Bgy
Union	Bgy
Upi	Bgy
City of Ilagan (Capital)	City
Cabeseria 27	Bgy
Aggasian	Bgy
Alibagu	Bgy
Allinguigan 1st	Bgy
Allinguigan 2nd	Bgy
Allinguigan 3rd	Bgy
Arusip	Bgy
Baculod (Pob.)	Bgy
Bagumbayan (Pob.)	Bgy
Baligatan	Bgy
Ballacong	Bgy
Bangag	Bgy
Cabeseria 5	Bgy
Batong-Labang	Bgy
Bigao	Bgy
Cabeseria 4	Bgy
Cabannungan 1st	Bgy
Cabannungan 2nd	Bgy
Cabeseria 6 & 24	Bgy
Cabeseria 19	Bgy
Cabeseria 25	Bgy
Cabeseria 3	Bgy
Cabeseria 23	Bgy
Cadu	Bgy
Calamagui 1st	Bgy
Calamagui 2nd	Bgy
Camunatan	Bgy
Capellan	Bgy
Capo	Bgy
Cabeseria 9 and 11	Bgy
Carikkikan Norte	Bgy
Carikkikan Sur	Bgy
Cabeseria 14 and 16	Bgy
Cabeseria 2	Bgy
Fugu	Bgy
Fuyo	Bgy
Gayong-Gayong Norte	Bgy
Gayong-Gayong Sur	Bgy
Guinatan	Bgy
Lullutan	Bgy
Cabeseria 10	Bgy
Malalam	Bgy
Malasin	Bgy
Manaring	Bgy
Mangcuram	Bgy
Villa Imelda	Bgy
Marana I	Bgy
Marana II	Bgy
Marana III	Bgy
Minabang	Bgy
Morado	Bgy
Naguilian Norte	Bgy
Naguilian Sur	Bgy
Namnama	Bgy
Nanaguan	Bgy
Cabeseria 7	Bgy
Osmeña	Bgy
Paliueg	Bgy
Pasa	Bgy
Pilar	Bgy
Quimalabasa	Bgy
Rang-ayan	Bgy
Rugao	Bgy
Cabeseria 22	Bgy
Salindingan	Bgy
San Andres	Bgy
Centro - San Antonio	Bgy
San Felipe	Bgy
San Ignacio	Bgy
San Isidro	Bgy
San Juan	Bgy
San Lorenzo	Bgy
San Pablo	Bgy
Cabeseria 17 and 21	Bgy
San Vicente (Pob.)	Bgy
Santa Barbara (Pob.)	Bgy
Santa Catalina	Bgy
Santa Isabel Norte	Bgy
Santa Isabel Sur	Bgy
Santa Victoria	Bgy
Santo Tomas	Bgy
Siffu	Bgy
Sindon Bayabo	Bgy
Sindon Maride	Bgy
Sipay	Bgy
Tangcul	Bgy
Centro Poblacion	Bgy
Bagong Silang	Bgy
Imelda Bliss Village	Bgy
San Rodrigo	Bgy
Santa Maria	Bgy
Jones	Mun
Abulan	Bgy
Addalam	Bgy
Arubub	Bgy
Bannawag	Bgy
Bantay	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangcuag	Bgy
Dalibubon	Bgy
Daligan	Bgy
Diarao	Bgy
Dibuluan	Bgy
Dicamay I	Bgy
Dicamay II	Bgy
Dipangit	Bgy
Disimpit	Bgy
Divinan	Bgy
Dumawing	Bgy
Fugu	Bgy
Lacab	Bgy
Linamanan	Bgy
Linomot	Bgy
Malannit	Bgy
Minuri	Bgy
Namnama	Bgy
Napaliong	Bgy
Palagao	Bgy
Papan Este	Bgy
Papan Weste	Bgy
Payac	Bgy
Pongpongan	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Roque	Bgy
San Sebastian	Bgy
San Vicente	Bgy
Santa Isabel	Bgy
Santo Domingo	Bgy
Tupax	Bgy
Usol	Bgy
Villa Bello	Bgy
Luna	Mun
Bustamante	Bgy
Centro 1 (Pob.)	Bgy
Centro 2 (Pob.)	Bgy
Centro 3 (Pob.)	Bgy
Concepcion	Bgy
Dadap	Bgy
Harana	Bgy
Lalog 1	Bgy
Lalog 2	Bgy
Luyao	Bgy
Macañao	Bgy
Macugay	Bgy
Mambabanga	Bgy
Pulay	Bgy
Puroc	Bgy
San Isidro	Bgy
San Miguel	Bgy
Santo Domingo	Bgy
Union Kalinga	Bgy
Maconacon	Mun
Diana	Bgy
Eleonor (Pob.)	Bgy
Fely (Pob.)	Bgy
Lita (Pob.)	Bgy
Reina Mercedes	Bgy
Minanga	Bgy
Malasin	Bgy
Canadam	Bgy
Aplaya	Bgy
Santa Marina	Bgy
Delfin Albano	Mun
Aga	Bgy
Andarayan	Bgy
Aneg	Bgy
Bayabo	Bgy
Calinaoan Sur	Bgy
Capitol	Bgy
Carmencita	Bgy
Concepcion	Bgy
Maui	Bgy
Quibal	Bgy
Ragan Almacen	Bgy
Ragan Norte	Bgy
Ragan Sur (Pob.)	Bgy
Rizal	Bgy
San Andres	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Macario	Bgy
San Nicolas	Bgy
San Patricio	Bgy
San Roque	Bgy
Santo Rosario	Bgy
Santor	Bgy
Villa Luz	Bgy
Villa Pereda	Bgy
Visitacion	Bgy
Caloocan	Bgy
Mallig	Mun
San Pedro	Bgy
Binmonton	Bgy
Casili	Bgy
Centro I (Pob.)	Bgy
Holy Friday	Bgy
Maligaya	Bgy
Manano	Bgy
Olango	Bgy
Centro II (Pob.)	Bgy
Rang-ayan	Bgy
San Jose Norte I	Bgy
San Jose Sur	Bgy
Siempre Viva Norte	Bgy
Trinidad	Bgy
Victoria	Bgy
San Jose Norte II	Bgy
San Ramon	Bgy
Siempre Viva Sur	Bgy
Naguilian	Mun
Aguinaldo	Bgy
Bagong Sikat	Bgy
Burgos	Bgy
Cabaruan	Bgy
Flores	Bgy
La Union	Bgy
Magsaysay (Pob.)	Bgy
Manaring	Bgy
Mansibang	Bgy
Minallo	Bgy
Minanga	Bgy
Palattao	Bgy
Quezon (Pob.)	Bgy
Quinalabasa	Bgy
Quirino (Pob.)	Bgy
Rangayan	Bgy
Rizal	Bgy
Roxas (Pob.)	Bgy
San Manuel	Bgy
Santo Tomas	Bgy
Sunlife	Bgy
Surcoc	Bgy
Tomines	Bgy
Villa Paz	Bgy
Santa Victoria	Bgy
Palanan	Mun
Bisag	Bgy
Dialaoyao	Bgy
Dicadyuan	Bgy
Didiyan	Bgy
Dimalicu-licu	Bgy
Dimasari	Bgy
Dimatican	Bgy
Maligaya	Bgy
Marikit	Bgy
Dicabisagan East (Pob.)	Bgy
Dicabisagan West (Pob.)	Bgy
Santa Jacinta	Bgy
Villa Robles	Bgy
Culasi	Bgy
Alomanay	Bgy
Diddadungan	Bgy
San Isidro	Bgy
Quezon	Mun
Abut	Bgy
Alunan (Pob.)	Bgy
Arellano (Pob.)	Bgy
Aurora	Bgy
Barucboc Norte	Bgy
Estrada	Bgy
Santos (Pob.)	Bgy
Lepanto	Bgy
Mangga	Bgy
Minagbag	Bgy
Samonte (Pob.)	Bgy
Turod	Bgy
Dunmon	Bgy
Calangigan	Bgy
San Juan	Bgy
Quirino	Mun
Binarzang	Bgy
Cabaruan	Bgy
Camaal	Bgy
Dolores	Bgy
Luna	Bgy
Manaoag	Bgy
Rizal	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Mateo	Bgy
San Vicente	Bgy
Santa Catalina	Bgy
Santa Lucia (Pob.)	Bgy
Santiago	Bgy
Santo Domingo	Bgy
Sinait	Bgy
Suerte	Bgy
Villa Bulusan	Bgy
Villa Miguel	Bgy
Vintar	Bgy
Ramon	Mun
Ambatali	Bgy
Bantug	Bgy
Bugallon Norte	Bgy
Burgos	Bgy
Nagbacalan	Bgy
Oscariz	Bgy
Pabil	Bgy
Pagrang-ayan	Bgy
Planas	Bgy
Purok ni Bulan	Bgy
Raniag	Bgy
San Antonio	Bgy
San Miguel	Bgy
San Sebastian	Bgy
Villa Beltran	Bgy
Villa Carmen	Bgy
Villa Marcos	Bgy
General Aguinaldo	Bgy
Bugallon Proper (Pob.)	Bgy
Reina Mercedes	Mun
Banquero	Bgy
Binarsang	Bgy
Cutog Grande	Bgy
Cutog Pequeño	Bgy
Dangan	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
Labinab Grande (Pob.)	Bgy
Labinab Pequeño (Pob.)	Bgy
Mallalatang Grande	Bgy
Mallalatang Tunggui	Bgy
Napaccu Grande	Bgy
Napaccu Pequeño	Bgy
Salucong	Bgy
Santor	Bgy
Sinippil	Bgy
Tallungan (Pob.)	Bgy
Turod	Bgy
Villador	Bgy
Santiago	Bgy
Roxas	Mun
Anao	Bgy
Imbiao	Bgy
Lanting	Bgy
Lucban	Bgy
Marcos	Bgy
Masigun	Bgy
Rizal (Pob.)	Bgy
Vira (Pob.)	Bgy
Bantug (Pob.)	Bgy
Luna (Pob.)	Bgy
Quiling	Bgy
Rang-ayan	Bgy
San Antonio	Bgy
San Jose	Bgy
San Pedro	Bgy
San Placido	Bgy
San Rafael	Bgy
Simimbaan	Bgy
Sinamar	Bgy
Sotero Nuesa	Bgy
Villa Concepcion	Bgy
Matusalem	Bgy
Muñoz East	Bgy
Muñoz West	Bgy
Doña Concha	Bgy
San Luis	Bgy
San Agustin	Mun
Bautista	Bgy
Calaocan	Bgy
Dabubu Grande	Bgy
Dabubu Pequeño	Bgy
Dappig	Bgy
Laoag	Bgy
Mapalad	Bgy
Masaya Centro (Pob.)	Bgy
Masaya Norte	Bgy
Masaya Sur	Bgy
Nemmatan	Bgy
Palacian	Bgy
Panang	Bgy
Quimalabasa Norte	Bgy
Quimalabasa Sur	Bgy
Rang-ay	Bgy
Salay	Bgy
San Antonio	Bgy
Santo Niño	Bgy
Santos	Bgy
Sinaoangan Norte	Bgy
Sinaoangan Sur	Bgy
Virgoneza	Bgy
San Guillermo	Mun
Anonang	Bgy
Aringay	Bgy
Centro 1 (Pob.)	Bgy
Centro 2 (Pob.)	Bgy
Colorado	Bgy
Dietban	Bgy
Dingading	Bgy
Dipacamo	Bgy
Estrella	Bgy
Guam	Bgy
Nakar	Bgy
Palawan	Bgy
Progreso	Bgy
Rizal	Bgy
San Francisco Sur	Bgy
San Mariano Norte	Bgy
San Mariano Sur	Bgy
Sinalugan	Bgy
Villa Remedios	Bgy
Villa Rose	Bgy
Villa Sanchez	Bgy
Villa Teresita	Bgy
Burgos	Bgy
San Francisco Norte	Bgy
Calaoagan	Bgy
San Rafael	Bgy
San Isidro	Mun
Camarag	Bgy
Cebu	Bgy
Gomez	Bgy
Gud	Bgy
Nagbukel	Bgy
Patanad	Bgy
Quezon	Bgy
Ramos East	Bgy
Ramos West	Bgy
Rizal East (Pob.)	Bgy
Rizal West (Pob.)	Bgy
Victoria	Bgy
Villaflor	Bgy
San Manuel	Mun
Agliam	Bgy
Babanuang	Bgy
Cabaritan	Bgy
Caraniogan	Bgy
Eden	Bgy
Malalinta	Bgy
Mararigue	Bgy
Nueva Era	Bgy
Pisang	Bgy
District 1 (Pob.)	Bgy
District 2 (Pob.)	Bgy
District 3 (Pob.)	Bgy
District 4 (Pob.)	Bgy
San Francisco	Bgy
Sandiat Centro	Bgy
Sandiat East	Bgy
Sandiat West	Bgy
Santa Cruz	Bgy
Villanueva	Bgy
San Mariano	Mun
Alibadabad	Bgy
Binatug	Bgy
Bitabian	Bgy
Buyasan	Bgy
Cadsalan	Bgy
Casala	Bgy
Cataguing	Bgy
Daragutan East	Bgy
Daragutan West	Bgy
Del Pilar	Bgy
Dibuluan	Bgy
Dicamay	Bgy
Dipusu	Bgy
Disulap	Bgy
Disusuan	Bgy
Gangalan	Bgy
Ibujan	Bgy
Libertad	Bgy
Macayucayu	Bgy
Mallabo	Bgy
Marannao	Bgy
Minanga	Bgy
Old San Mariano	Bgy
Palutan	Bgy
Panninan	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
San Jose	Bgy
San Pablo	Bgy
San Pedro	Bgy
Santa Filomina	Bgy
Tappa	Bgy
Ueg	Bgy
Zamora	Bgy
Balagan	Bgy
San Mateo	Mun
Bacareña	Bgy
Bagong Sikat	Bgy
Bella Luz	Bgy
Daramuangan Sur	Bgy
Estrella	Bgy
Gaddanan	Bgy
Malasin	Bgy
Mapuroc	Bgy
Marasat Grande	Bgy
Marasat Pequeño	Bgy
Old Centro I	Bgy
Old Centro II	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Salinungan East	Bgy
Salinungan West	Bgy
San Andres	Bgy
San Antonio	Bgy
San Ignacio	Bgy
San Manuel	Bgy
San Marcos	Bgy
San Roque	Bgy
Sinamar Norte	Bgy
Sinamar Sur	Bgy
Victoria	Bgy
Villafuerte	Bgy
Villa Cruz	Bgy
Villa Magat	Bgy
Villa Gamiao	Bgy
Dagupan	Bgy
Daramuangan Norte	Bgy
San Pablo	Mun
Annanuman	Bgy
Auitan	Bgy
Ballacayu	Bgy
Binguang	Bgy
Bungad	Bgy
Dalena	Bgy
Caddangan/Limbauan	Bgy
Calamagui	Bgy
Caralucud	Bgy
Guminga	Bgy
Minanga Norte	Bgy
Minanga Sur	Bgy
San Jose	Bgy
Poblacion	Bgy
Simanu Norte	Bgy
Simanu Sur	Bgy
Tupa	Bgy
Santa Maria	Mun
Bangad	Bgy
Buenavista	Bgy
Calamagui North	Bgy
Calamagui East	Bgy
Calamagui West	Bgy
Divisoria	Bgy
Lingaling	Bgy
Mozzozzin Sur	Bgy
Mozzozzin North	Bgy
Naganacan	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Quinagabian	Bgy
San Antonio	Bgy
San Isidro East	Bgy
San Isidro West	Bgy
San Rafael West	Bgy
San Rafael East	Bgy
Villabuena	Bgy
City of Santiago	City
Abra	Bgy
Ambalatungan	Bgy
Balintocatoc	Bgy
Baluarte	Bgy
Bannawag Norte	Bgy
Batal	Bgy
Buenavista	Bgy
Cabulay	Bgy
Calao East (Pob.)	Bgy
Calao West (Pob.)	Bgy
Calaocan	Bgy
Villa Gonzaga	Bgy
Centro East (Pob.)	Bgy
Centro West (Pob.)	Bgy
Divisoria	Bgy
Dubinan East	Bgy
Dubinan West	Bgy
Luna	Bgy
Mabini	Bgy
Malvar	Bgy
Nabbuan	Bgy
Naggasican	Bgy
Patul	Bgy
Plaridel	Bgy
Rizal	Bgy
Rosario	Bgy
Sagana	Bgy
Salvador	Bgy
San Andres	Bgy
San Isidro	Bgy
San Jose	Bgy
Sinili	Bgy
Sinsayon	Bgy
Santa Rosa	Bgy
Victory Norte	Bgy
Victory Sur	Bgy
Villasis	Bgy
Santo Tomas	Mun
Ammugauan	Bgy
Antagan	Bgy
Bagabag	Bgy
Bagutari	Bgy
Balelleng	Bgy
Barumbong	Bgy
Bubug	Bgy
Bolinao-Culalabo	Bgy
Cañogan Abajo Norte	Bgy
Calinaoan Centro	Bgy
Calinaoan Malasin	Bgy
Calinaoan Norte	Bgy
Cañogan Abajo Sur	Bgy
Cañogan Alto	Bgy
Centro	Bgy
Colunguan	Bgy
Malapagay	Bgy
San Rafael Abajo	Bgy
San Rafael Alto	Bgy
San Roque	Bgy
San Vicente	Bgy
Uauang-Tuliao	Bgy
Uauang-Galicia	Bgy
Biga Occidental	Bgy
Biga Oriental	Bgy
Calanigan Norte	Bgy
Calanigan Sur	Bgy
Tumauini	Mun
Annafunan	Bgy
Antagan I	Bgy
Antagan II	Bgy
Arcon	Bgy
Balug	Bgy
Banig	Bgy
Bantug	Bgy
Bayabo East	Bgy
Caligayan	Bgy
Camasi	Bgy
Carpentero	Bgy
Compania	Bgy
Cumabao	Bgy
Fugu Abajo	Bgy
Fugu Norte	Bgy
Fugu Sur	Bgy
Fermeldy	Bgy
Lalauanan	Bgy
Lanna	Bgy
Lapogan	Bgy
Lingaling	Bgy
Liwanag	Bgy
Santa Visitacion	Bgy
Malamag East	Bgy
Malamag West	Bgy
Maligaya	Bgy
Minanga	Bgy
Namnama	Bgy
Paragu	Bgy
Pilitan	Bgy
Barangay District 1 (Pob.)	Bgy
Barangay District 2 (Pob.)	Bgy
Barangay District 3 (Pob.)	Bgy
Barangay District 4 (Pob.)	Bgy
San Mateo	Bgy
San Pedro	Bgy
San Vicente	Bgy
Santa	Bgy
Santa Catalina	Bgy
Santo Niño	Bgy
Sinippil	Bgy
Sisim Abajo	Bgy
Sisim Alto	Bgy
Tunggui	Bgy
Ugad	Bgy
Moldero	Bgy
Nueva Vizcaya	Prov
Ambaguio	Mun
Ammueg	Bgy
Camandag	Bgy
Labang	Bgy
Napo	Bgy
Poblacion	Bgy
Salingsingan	Bgy
Tiblac	Bgy
Dulli	Bgy
Aritao	Mun
Banganan	Bgy
Beti	Bgy
Bone North	Bgy
Bone South	Bgy
Calitlitan	Bgy
Comon	Bgy
Cutar	Bgy
Darapidap	Bgy
Kirang	Bgy
Nagcuartelan	Bgy
Poblacion	Bgy
Santa Clara	Bgy
Tabueng	Bgy
Tucanon	Bgy
Anayo	Bgy
Baan	Bgy
Balite	Bgy
Canabuan	Bgy
Canarem	Bgy
Latar-Nocnoc-San Francisco	Bgy
Ocao-Capiniaan	Bgy
Yaway	Bgy
Bagabag	Mun
Bakir	Bgy
Baretbet	Bgy
Careb	Bgy
Lantap	Bgy
Murong	Bgy
Nangalisan	Bgy
Paniki	Bgy
Pogonsino	Bgy
San Geronimo (Pob.)	Bgy
San Pedro (Pob.)	Bgy
Santa Cruz	Bgy
Santa Lucia	Bgy
Tuao North	Bgy
Villa Coloma (Pob.)	Bgy
Quirino (Pob.)	Bgy
Villaros	Bgy
Tuao South	Bgy
Bambang	Mun
Abian	Bgy
Abinganan	Bgy
Aliaga	Bgy
Almaguer North	Bgy
Almaguer South	Bgy
Banggot (Pob.)	Bgy
Barat	Bgy
Buag (Pob.)	Bgy
Calaocan (Pob.)	Bgy
Dullao	Bgy
Homestead	Bgy
Indiana	Bgy
Mabuslo	Bgy
Macate	Bgy
Manamtam	Bgy
Mauan	Bgy
Salinas	Bgy
San Antonio North	Bgy
San Antonio South	Bgy
San Fernando	Bgy
San Leonardo	Bgy
Santo Domingo	Bgy
Pallas	Bgy
Magsaysay Hills	Bgy
Santo Domingo West	Bgy
Bayombong (Capital)	Mun
Bonfal East	Bgy
Bonfal Proper	Bgy
Bonfal West	Bgy
Buenavista	Bgy
Busilac	Bgy
Casat	Bgy
La Torre North	Bgy
Magapuy	Bgy
Magsaysay	Bgy
Masoc	Bgy
Paitan	Bgy
Don Domingo Maddela Pob. 	Bgy
Don Tomas Maddela Pob.	Bgy
District III Pob.	Bgy
District IV (Pob.)	Bgy
Bansing	Bgy
Cabuaan	Bgy
Don Mariano Marcos	Bgy
Ipil-Cuneg	Bgy
La Torre South	Bgy
Luyang	Bgy
Salvacion	Bgy
San Nicolas North	Bgy
Santa Rosa	Bgy
Vista Alegre	Bgy
Diadi	Mun
Arwas	Bgy
Balete	Bgy
Bugnay	Bgy
Decabacan	Bgy
Duruarog	Bgy
Escoting	Bgy
Nagsabaran	Bgy
Namamparan	Bgy
Pinya	Bgy
Poblacion	Bgy
Ampakling	Bgy
Butao	Bgy
Langca	Bgy
Lurad	Bgy
Rosario	Bgy
San Luis	Bgy
San Pablo	Bgy
Villa Aurora	Bgy
Villa Florentino	Bgy
Dupax del Norte	Mun
Belance	Bgy
Bulala	Bgy
Inaban	Bgy
Ineangan	Bgy
Lamo	Bgy
Mabasa	Bgy
Malasin (Pob.)	Bgy
Munguia	Bgy
Oyao	Bgy
New Gumiad	Bgy
Yabbi	Bgy
Binnuangan	Bgy
Bitnong	Bgy
Macabenga	Bgy
Parai	Bgy
Dupax del Sur	Mun
Abaca	Bgy
Banila	Bgy
Carolotan	Bgy
Gabut	Bgy
Ganao	Bgy
Lukidnon	Bgy
Mangayang	Bgy
Palabotan	Bgy
Biruk	Bgy
Bagumbayan	Bgy
Balsain	Bgy
Canabay	Bgy
Domang	Bgy
Dopaj	Bgy
Kimbutan	Bgy
Kinabuan	Bgy
Sanguit	Bgy
Santa Maria	Bgy
Talbek	Bgy
Kasibu	Mun
Antutot	Bgy
Alimit	Bgy
Poblacion	Bgy
Bilet	Bgy
Binogawan	Bgy
Bua	Bgy
Biyoy	Bgy
Capisaan	Bgy
Cordon	Bgy
Didipio	Bgy
Dine	Bgy
Kakiduguen	Bgy
Lupa	Bgy
Macalong	Bgy
Malabing	Bgy
Muta	Bgy
Pao	Bgy
Papaya	Bgy
Pudi	Bgy
Tokod	Bgy
Seguem	Bgy
Tadji	Bgy
Wangal	Bgy
Watwat	Bgy
Camamasi	Bgy
Catarawan	Bgy
Nantawacan	Bgy
Alloy	Bgy
Kongkong	Bgy
Pacquet	Bgy
Kayapa	Mun
Acacia	Bgy
Amilong Labeng	Bgy
Ansipsip	Bgy
Baan	Bgy
Babadi	Bgy
Balangabang	Bgy
Banao	Bgy
Binalian	Bgy
Besong	Bgy
Cabalatan-Alang	Bgy
Cabanglasan	Bgy
Kayapa Proper East	Bgy
Kayapa Proper West	Bgy
Mapayao	Bgy
Nansiakan	Bgy
Pampang (Pob.)	Bgy
Pangawan	Bgy
Pinayag	Bgy
Pingkian	Bgy
San Fabian	Bgy
Talecabcab	Bgy
Tubongan	Bgy
Alang-Salacsac	Bgy
Balete	Bgy
Buyasyas	Bgy
Cabayo	Bgy
Castillo Village	Bgy
Latbang	Bgy
Lawigan	Bgy
Tidang Village	Bgy
Quezon	Mun
Aurora	Bgy
Baresbes	Bgy
Buliwao	Bgy
Bonifacio	Bgy
Calaocan	Bgy
Caliat (Pob.)	Bgy
Darubba	Bgy
Maddiangat	Bgy
Nalubbunan	Bgy
Runruno	Bgy
Maasin	Bgy
Dagupan	Bgy
Santa Fe	Mun
Bacneng	Bgy
Baliling	Bgy
Bantinan	Bgy
Baracbac	Bgy
Buyasyas	Bgy
Imugan	Bgy
Poblacion	Bgy
Sinapaoan	Bgy
Tactac	Bgy
Villa Flores	Bgy
Atbu	Bgy
Balete	Bgy
Canabuan	Bgy
Malico	Bgy
Santa Rosa	Bgy
Unib	Bgy
Solano	Mun
Aggub	Bgy
Bangaan	Bgy
Bangar	Bgy
Bascaran	Bgy
Curifang	Bgy
Dadap	Bgy
Lactawan	Bgy
Osmeña	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
Quezon	Bgy
Quirino	Bgy
Roxas	Bgy
San Juan	Bgy
San Luis	Bgy
Tucal	Bgy
Uddiawan	Bgy
Wacal	Bgy
Bagahabag	Bgy
Communal	Bgy
Concepcion	Bgy
Pilar D. Galima	Bgy
Villaverde	Mun
Bintawan Sur	Bgy
Ibung	Bgy
Cabuluan	Bgy
Nagbitin	Bgy
Ocapon	Bgy
Pieza	Bgy
Sawmill	Bgy
Poblacion	Bgy
Bintawan Norte	Bgy
Alfonso Castaneda	Mun
Abuyo	Bgy
Galintuja	Bgy
Cauayan	Bgy
Lipuga	Bgy
Lublub (Pob.)	Bgy
Pelaway	Bgy
Quirino	Prov
Aglipay	Mun
Dagupan	Bgy
Dumabel	Bgy
Dungo	Bgy
Guinalbin	Bgy
Ligaya	Bgy
Palacian	Bgy
Pinaripad Sur	Bgy
Progreso (Pob.)	Bgy
Ramos	Bgy
Rang-ayan	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Leonardo	Bgy
San Ramon	Bgy
Victoria	Bgy
Villa Pagaduan	Bgy
Villa Santiago	Bgy
Alicia	Bgy
Cabugao	Bgy
Diodol	Bgy
Nagabgaban	Bgy
Pinaripad Norte	Bgy
San Benigno	Bgy
San Manuel	Bgy
Villa Ventura	Bgy
Cabarroguis (Capital)	Mun
Banuar	Bgy
Burgos	Bgy
Calaocan	Bgy
Del Pilar	Bgy
Dibibi	Bgy
Eden	Bgy
Gundaway (Pob.)	Bgy
Mangandingay (Pob.)	Bgy
San Marcos	Bgy
Villamor	Bgy
Zamora	Bgy
Villarose	Bgy
Villa Peña	Bgy
Dingasan	Bgy
Tucod	Bgy
Gomez	Bgy
Santo Domingo	Bgy
Diffun	Mun
Andres Bonifacio (Pob.)	Bgy
Aurora East (Pob.)	Bgy
Aurora West (Pob.)	Bgy
Baguio Village	Bgy
Balagbag	Bgy
Bannawag	Bgy
Cajel	Bgy
Campamento	Bgy
Diego Silang	Bgy
Don Mariano Perez, Sr.	Bgy
Doña Imelda	Bgy
Dumanisi	Bgy
Gabriela Silang	Bgy
Gulac	Bgy
Guribang	Bgy
Ifugao Village	Bgy
Isidro Paredes	Bgy
Rizal (Pob.)	Bgy
Liwayway	Bgy
Luttuad	Bgy
Magsaysay	Bgy
Makate	Bgy
Maria Clara	Bgy
Rafael Palma	Bgy
Ricarte Norte	Bgy
Ricarte Sur	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Pascual	Bgy
Villa Pascua	Bgy
Aklan Village	Bgy
Gregorio Pimentel	Bgy
Don Faustino Pagaduan	Bgy
Maddela	Mun
Abbag	Bgy
Balligui	Bgy
Divisoria Sur	Bgy
Buenavista	Bgy
Cabaruan	Bgy
Cabua-an	Bgy
Cofcaville	Bgy
Diduyon	Bgy
Dipintin	Bgy
Divisoria Norte	Bgy
Dumabato Norte	Bgy
Dumabato Sur	Bgy
Lusod	Bgy
Manglad	Bgy
Pedlisan	Bgy
Poblacion Norte	Bgy
San Bernabe	Bgy
San Dionisio I	Bgy
San Martin	Bgy
San Pedro	Bgy
San Salvador	Bgy
Santo Niño	Bgy
Santo Tomas	Bgy
Villa Gracia	Bgy
Villa Hermosa Sur	Bgy
Villa Hermosa Norte	Bgy
Ysmael	Bgy
Villa Agullana	Bgy
Poblacion Sur	Bgy
Villa Jose V Ylanan	Bgy
Jose Ancheta	Bgy
Santa Maria	Bgy
Saguday	Mun
Dibul	Bgy
La Paz	Bgy
Magsaysay (Pob.)	Bgy
Rizal (Pob.)	Bgy
Salvacion	Bgy
Santo Tomas	Bgy
Tres Reyes	Bgy
Cardenas	Bgy
Gamis	Bgy
Nagtipunan	Mun
Anak	Bgy
Dipantan	Bgy
Dissimungal	Bgy
Guino	Bgy
La Conwap	Bgy
Landingan	Bgy
Mataddi	Bgy
Matmad	Bgy
Old Gumiad	Bgy
Ponggo	Bgy
San Dionisio II	Bgy
San Pugo	Bgy
San Ramos	Bgy
Sangbay	Bgy
Wasid	Bgy
Asaklat	Bgy
Region III (Central Luzon)	Reg
Bataan	Prov
Abucay	Mun
Bangkal	Bgy
Calaylayan (Pob.)	Bgy
Capitangan	Bgy
Gabon	Bgy
Laon (Pob.)	Bgy
Mabatang	Bgy
Omboy	Bgy
Salian	Bgy
Wawa (Pob.)	Bgy
Bagac	Mun
Bagumbayan (Pob.)	Bgy
Banawang	Bgy
Binuangan	Bgy
Binukawan	Bgy
Ibaba	Bgy
Ibis	Bgy
Pag-asa	Bgy
Parang	Bgy
Paysawan	Bgy
Quinawan	Bgy
San Antonio	Bgy
Saysain	Bgy
Tabing-Ilog (Pob.)	Bgy
Atilano L. Ricardo	Bgy
City of Balanga (Capital)	City
Bagumbayan	Bgy
Cabog-Cabog	Bgy
Munting Batangas	Bgy
Cataning	Bgy
Central	Bgy
Cupang Proper	Bgy
Cupang West	Bgy
Dangcol	Bgy
Ibayo	Bgy
Malabia	Bgy
Poblacion	Bgy
Pto. Rivas Ibaba	Bgy
Pto. Rivas Itaas	Bgy
San Jose	Bgy
Sibacan	Bgy
Camacho	Bgy
Talisay	Bgy
Tanato	Bgy
Tenejero	Bgy
Tortugas	Bgy
Tuyo	Bgy
Bagong Silang	Bgy
Cupang North	Bgy
Doña Francisca	Bgy
Lote	Bgy
Dinalupihan	Mun
Bangal	Bgy
Bonifacio (Pob.)	Bgy
Burgos (Pob.)	Bgy
Colo	Bgy
Daang Bago	Bgy
Dalao	Bgy
Del Pilar (Pob.)	Bgy
Gen. Luna (Pob.)	Bgy
Gomez (Pob.)	Bgy
Happy Valley	Bgy
Kataasan	Bgy
Layac	Bgy
Luacan	Bgy
Mabini Proper (Pob.)	Bgy
Mabini Ext. (Pob.)	Bgy
Magsaysay	Bgy
Naparing	Bgy
New San Jose	Bgy
Old San Jose	Bgy
Padre Dandan (Pob.)	Bgy
Pag-asa	Bgy
Pagalanggang	Bgy
Pinulot	Bgy
Pita	Bgy
Rizal (Pob.)	Bgy
Roosevelt	Bgy
Roxas (Pob.)	Bgy
Saguing	Bgy
San Benito	Bgy
San Isidro (Pob.)	Bgy
San Pablo	Bgy
San Ramon	Bgy
San Simon	Bgy
Santo Niño	Bgy
Sapang Balas	Bgy
Santa Isabel	Bgy
Torres Bugauen (Pob.)	Bgy
Tucop	Bgy
Zamora (Pob.)	Bgy
Aquino	Bgy
Bayan-bayanan	Bgy
Maligaya	Bgy
Payangan	Bgy
Pentor	Bgy
Tubo-tubo	Bgy
Jose C. Payumo, Jr.	Bgy
Hermosa	Mun
A. Rivera (Pob.)	Bgy
Almacen	Bgy
Bacong	Bgy
Balsic	Bgy
Bamban	Bgy
Burgos-Soliman (Pob.)	Bgy
Cataning (Pob.)	Bgy
Culis	Bgy
Daungan (Pob.)	Bgy
Mabiga	Bgy
Mabuco	Bgy
Maite	Bgy
Mambog - Mandama	Bgy
Palihan	Bgy
Pandatung	Bgy
Pulo	Bgy
Saba	Bgy
San Pedro (Pob.)	Bgy
Santo Cristo (Pob.)	Bgy
Sumalo	Bgy
Tipo	Bgy
Judge Roman Cruz Sr.	Bgy
Sacrifice Valley	Bgy
Limay	Mun
Alangan	Bgy
Kitang I	Bgy
Kitang 2 & Luz	Bgy
Lamao	Bgy
Landing	Bgy
Poblacion	Bgy
Reformista	Bgy
Townsite	Bgy
Wawa	Bgy
Duale	Bgy
San Francisco de Asis	Bgy
St. Francis II	Bgy
Mariveles	Mun
Alas-asin	Bgy
Alion	Bgy
Batangas II	Bgy
Cabcaben	Bgy
Lucanin	Bgy
Baseco Country	Bgy
Poblacion	Bgy
San Carlos	Bgy
San Isidro	Bgy
Sisiman	Bgy
Balon-Anito	Bgy
Biaan	Bgy
Camaya	Bgy
Ipag	Bgy
Malaya	Bgy
Maligaya	Bgy
Mt. View	Bgy
Townsite	Bgy
Morong	Mun
Binaritan	Bgy
Mabayo	Bgy
Nagbalayong	Bgy
Poblacion	Bgy
Sabang	Bgy
Orani	Mun
Bagong Paraiso (Pob.)	Bgy
Balut (Pob.)	Bgy
Bayan (Pob.)	Bgy
Calero (Pob.)	Bgy
Paking-Carbonero (Pob.)	Bgy
Centro II (Pob.)	Bgy
Dona	Bgy
Kaparangan	Bgy
Masantol	Bgy
Mulawin	Bgy
Pag-asa	Bgy
Palihan (Pob.)	Bgy
Pantalan Bago (Pob.)	Bgy
Pantalan Luma (Pob.)	Bgy
Parang Parang (Pob.)	Bgy
Centro I (Pob.)	Bgy
Sibul	Bgy
Silahis	Bgy
Tala	Bgy
Talimundoc	Bgy
Tapulao	Bgy
Tenejero (Pob.)	Bgy
Tugatog	Bgy
Wawa (Pob.)	Bgy
Apollo	Bgy
Kabalutan	Bgy
Maria Fe	Bgy
Puksuan	Bgy
Tagumpay	Bgy
Orion	Mun
Arellano (Pob.)	Bgy
Bagumbayan (Pob.)	Bgy
Balagtas (Pob.)	Bgy
Balut (Pob.)	Bgy
Bantan	Bgy
Bilolo	Bgy
Calungusan	Bgy
Camachile	Bgy
Daang Bago (Pob.)	Bgy
Daang Bilolo (Pob.)	Bgy
Daang Pare	Bgy
General Lim	Bgy
Kapunitan	Bgy
Lati (Pob.)	Bgy
Lusungan (Pob.)	Bgy
Puting Buhangin	Bgy
Sabatan	Bgy
San Vicente (Pob.)	Bgy
Santo Domingo	Bgy
Villa Angeles (Pob.)	Bgy
Wakas (Pob.)	Bgy
Wawa (Pob.)	Bgy
Santa Elena	Bgy
Pilar	Mun
Ala-uli	Bgy
Bagumbayan	Bgy
Balut I	Bgy
Balut II	Bgy
Bantan Munti	Bgy
Burgos	Bgy
Del Rosario (Pob.)	Bgy
Diwa	Bgy
Landing	Bgy
Liyang	Bgy
Nagwaling	Bgy
Panilao	Bgy
Pantingan	Bgy
Poblacion	Bgy
Rizal (Pob.)	Bgy
Santa Rosa	Bgy
Wakas North	Bgy
Wakas South	Bgy
Wawa	Bgy
Samal	Mun
East Calaguiman (Pob.)	Bgy
East Daang Bago (Pob.)	Bgy
Ibaba (Pob.)	Bgy
Imelda	Bgy
Lalawigan	Bgy
Palili	Bgy
San Juan (Pob.)	Bgy
San Roque (Pob.)	Bgy
Santa Lucia	Bgy
Sapa	Bgy
Tabing Ilog	Bgy
Gugo	Bgy
West Calaguiman (Pob.)	Bgy
West Daang Bago (Pob.)	Bgy
Bulacan	Prov
Angat	Mun
Banaban	Bgy
Baybay	Bgy
Binagbag	Bgy
Donacion	Bgy
Encanto	Bgy
Laog	Bgy
Marungko	Bgy
Niugan	Bgy
Paltok	Bgy
Pulong Yantok	Bgy
San Roque (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
Santa Lucia	Bgy
Santo Cristo (Pob.)	Bgy
Sulucan	Bgy
Taboc	Bgy
Balagtas	Mun
Borol 2nd	Bgy
Borol 1st	Bgy
Dalig	Bgy
Longos	Bgy
Panginay	Bgy
Pulong Gubat	Bgy
San Juan	Bgy
Santol	Bgy
Wawa (Pob.)	Bgy
Baliuag	Mun
Bagong Nayon	Bgy
Barangca	Bgy
Calantipay	Bgy
Catulinan	Bgy
Concepcion	Bgy
Hinukay	Bgy
Makinabang	Bgy
Matangtubig	Bgy
Pagala	Bgy
Paitan	Bgy
Piel	Bgy
Pinagbarilan	Bgy
Poblacion	Bgy
Sabang	Bgy
San Jose	Bgy
San Roque	Bgy
Santa Barbara	Bgy
Santo Cristo	Bgy
Santo Niño	Bgy
Subic	Bgy
Sulivan	Bgy
Tangos	Bgy
Tarcan	Bgy
Tiaong	Bgy
Tibag	Bgy
Tilapayong	Bgy
Virgen delas Flores	Bgy
Bocaue	Mun
Antipona	Bgy
Bagumbayan	Bgy
Bambang	Bgy
Batia	Bgy
Biñang 1st	Bgy
Biñang 2nd	Bgy
Bolacan	Bgy
Bundukan	Bgy
Bunlo	Bgy
Caingin	Bgy
Duhat	Bgy
Igulot	Bgy
Lolomboy	Bgy
Poblacion	Bgy
Sulucan	Bgy
Taal	Bgy
Tambobong	Bgy
Turo	Bgy
Wakas	Bgy
Bulacan	Mun
Bagumbayan	Bgy
Balubad	Bgy
Bambang	Bgy
Matungao	Bgy
Maysantol	Bgy
Perez	Bgy
Pitpitan	Bgy
San Francisco	Bgy
San Jose (Pob.)	Bgy
San Nicolas	Bgy
Santa Ana	Bgy
Santa Ines	Bgy
Taliptip	Bgy
Tibig	Bgy
Bustos	Mun
Bonga Mayor	Bgy
Bonga Menor	Bgy
Buisan	Bgy
Camachilihan	Bgy
Cambaog	Bgy
Catacte	Bgy
Liciada	Bgy
Malamig	Bgy
Malawak	Bgy
Poblacion	Bgy
San Pedro	Bgy
Talampas	Bgy
Tanawan	Bgy
Tibagan	Bgy
Calumpit	Mun
Balite	Bgy
Balungao	Bgy
Buguion	Bgy
Bulusan	Bgy
Calizon	Bgy
Calumpang	Bgy
Caniogan	Bgy
Corazon	Bgy
Frances	Bgy
Gatbuca	Bgy
Gugo	Bgy
Iba Este	Bgy
Iba O'Este	Bgy
Longos	Bgy
Meysulao	Bgy
Meyto	Bgy
Palimbang	Bgy
Panducot	Bgy
Pio Cruzcosa	Bgy
Poblacion	Bgy
Pungo	Bgy
San Jose	Bgy
San Marcos	Bgy
San Miguel	Bgy
Santa Lucia	Bgy
Santo Niño	Bgy
Sapang Bayan	Bgy
Sergio Bayan	Bgy
Sucol	Bgy
Guiguinto	Mun
Cutcut	Bgy
Daungan	Bgy
Ilang-Ilang	Bgy
Malis	Bgy
Panginay	Bgy
Poblacion	Bgy
Pritil	Bgy
Pulong Gubat	Bgy
Santa Cruz	Bgy
Santa Rita	Bgy
Tabang	Bgy
Tabe	Bgy
Tiaong	Bgy
Tuktukan	Bgy
Hagonoy	Mun
Abulalas	Bgy
Carillo	Bgy
Iba	Bgy
Mercado	Bgy
Palapat	Bgy
Pugad	Bgy
Sagrada Familia	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pablo	Bgy
San Pascual	Bgy
San Pedro	Bgy
San Roque	Bgy
San Sebastian	Bgy
Santa Cruz	Bgy
Santa Elena	Bgy
Santa Monica	Bgy
Santo Niño (Pob.)	Bgy
Santo Rosario	Bgy
Tampok	Bgy
Tibaguin	Bgy
Iba-Ibayo	Bgy
City of Malolos (Capital)	City
Anilao	Bgy
Atlag	Bgy
Babatnin	Bgy
Bagna	Bgy
Bagong Bayan	Bgy
Balayong	Bgy
Balite	Bgy
Bangkal	Bgy
Barihan	Bgy
Bulihan	Bgy
Bungahan	Bgy
Dakila	Bgy
Guinhawa	Bgy
Caingin	Bgy
Calero	Bgy
Caliligawan	Bgy
Canalate	Bgy
Caniogan	Bgy
Catmon	Bgy
Ligas	Bgy
Liyang	Bgy
Longos	Bgy
Look 1st	Bgy
Look 2nd	Bgy
Lugam	Bgy
Mabolo	Bgy
Mambog	Bgy
Masile	Bgy
Matimbo	Bgy
Mojon	Bgy
Namayan	Bgy
Niugan	Bgy
Pamarawan	Bgy
Panasahan	Bgy
Pinagbakahan	Bgy
San Agustin	Bgy
San Gabriel	Bgy
San Juan	Bgy
San Pablo	Bgy
San Vicente (Pob.)	Bgy
Santiago	Bgy
Santisima Trinidad	Bgy
Santo Cristo	Bgy
Santo Niño (Pob.)	Bgy
Santo Rosario (Pob.)	Bgy
Santol	Bgy
Sumapang Bata	Bgy
Sumapang Matanda	Bgy
Taal	Bgy
Tikay	Bgy
Cofradia	Bgy
Marilao	Mun
Abangan Norte	Bgy
Abangan Sur	Bgy
Ibayo	Bgy
Lambakin	Bgy
Lias	Bgy
Loma de Gato	Bgy
Nagbalon	Bgy
Patubig	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Prenza I	Bgy
Prenza II	Bgy
Santa Rosa I	Bgy
Santa Rosa II	Bgy
Saog	Bgy
Tabing Ilog	Bgy
City of Meycauayan	City
Bagbaguin	Bgy
Bahay Pare	Bgy
Bancal	Bgy
Banga	Bgy
Bayugo	Bgy
Calvario	Bgy
Camalig	Bgy
Hulo	Bgy
Iba	Bgy
Langka	Bgy
Lawa	Bgy
Libtong	Bgy
Liputan	Bgy
Longos	Bgy
Malhacan	Bgy
Pajo	Bgy
Pandayan	Bgy
Pantoc	Bgy
Perez	Bgy
Poblacion	Bgy
Saluysoy	Bgy
Saint Francis (Gasak)	Bgy
Tugatog	Bgy
Ubihan	Bgy
Zamora	Bgy
Caingin	Bgy
Norzagaray	Mun
Bangkal	Bgy
Baraka	Bgy
Bigte	Bgy
Bitungol	Bgy
Matictic	Bgy
Minuyan	Bgy
Partida	Bgy
Pinagtulayan	Bgy
Poblacion	Bgy
San Mateo	Bgy
Tigbe	Bgy
San Lorenzo	Bgy
Friendship Village Resources	Bgy
Obando	Mun
Binuangan	Bgy
Catanghalan	Bgy
Hulo	Bgy
Lawa	Bgy
Salambao	Bgy
Paco	Bgy
Pag-asa (Pob.)	Bgy
Paliwas	Bgy
Panghulo	Bgy
San Pascual	Bgy
Tawiran	Bgy
Pandi	Mun
Bagbaguin	Bgy
Bagong Barrio	Bgy
Bunsuran III	Bgy
Bunsuran I	Bgy
Bunsuran II	Bgy
Cacarong Bata	Bgy
Cacarong Matanda	Bgy
Cupang	Bgy
Malibong Bata	Bgy
Malibong Matanda	Bgy
Manatal	Bgy
Mapulang Lupa	Bgy
Masagana	Bgy
Masuso	Bgy
Pinagkuartelan	Bgy
Poblacion	Bgy
Real de Cacarong	Bgy
San Roque	Bgy
Siling Bata	Bgy
Siling Matanda	Bgy
Baka-bakahan	Bgy
Santo Niño	Bgy
Paombong	Mun
Binakod	Bgy
Kapitangan	Bgy
Malumot	Bgy
Masukol	Bgy
Pinalagdan	Bgy
Poblacion	Bgy
San Isidro I	Bgy
San Isidro II	Bgy
San Jose	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Plaridel	Mun
Agnaya	Bgy
Bagong Silang	Bgy
Banga I	Bgy
Banga II	Bgy
Bintog	Bgy
Bulihan	Bgy
Culianin	Bgy
Dampol	Bgy
Lagundi	Bgy
Lalangan	Bgy
Lumang Bayan	Bgy
Parulan	Bgy
Poblacion	Bgy
Rueda	Bgy
San Jose	Bgy
Santa Ines	Bgy
Santo Niño	Bgy
Sipat	Bgy
Tabang	Bgy
Pulilan	Mun
Balatong A	Bgy
Balatong B	Bgy
Cutcot	Bgy
Dampol I	Bgy
Dampol II-A	Bgy
Dampol II-B	Bgy
Dulong Malabon	Bgy
Inaon	Bgy
Longos	Bgy
Lumbac	Bgy
Paltao	Bgy
Penabatan	Bgy
Poblacion	Bgy
Santa Peregrina	Bgy
Santo Cristo	Bgy
Taal	Bgy
Tabon	Bgy
Tibag	Bgy
Tinejero	Bgy
San Ildefonso	Mun
Akle	Bgy
Alagao	Bgy
Anyatam	Bgy
Bagong Barrio	Bgy
Basuit	Bgy
Bubulong Munti	Bgy
Bubulong Malaki	Bgy
Buhol na Mangga	Bgy
Bulusukan	Bgy
Calasag	Bgy
Calawitan	Bgy
Casalat	Bgy
Gabihan	Bgy
Garlang	Bgy
Lapnit	Bgy
Maasim	Bgy
Makapilapil	Bgy
Malipampang	Bgy
Matimbubong	Bgy
Nabaong Garlang	Bgy
Palapala	Bgy
Pasong Bangkal	Bgy
Pinaod	Bgy
Poblacion	Bgy
Pulong Tamo	Bgy
San Juan	Bgy
Santa Catalina Bata	Bgy
Santa Catalina Matanda	Bgy
Sapang Dayap	Bgy
Sapang Putik	Bgy
Sapang Putol	Bgy
Sumandig	Bgy
Telepatio	Bgy
Upig	Bgy
Umpucan	Bgy
Mataas na Parang	Bgy
City of San Jose Del Monte	City
Bagong Buhay	Bgy
Dulong Bayan	Bgy
Gaya-gaya	Bgy
Kaypian	Bgy
Kaybanban	Bgy
Minuyan	Bgy
Muzon	Bgy
Poblacion	Bgy
Santo Cristo	Bgy
Sapang Palay	Bgy
Tungkong Mangga	Bgy
Citrus	Bgy
San Martin	Bgy
Santa Cruz	Bgy
Fatima	Bgy
San Pedro	Bgy
San Rafael	Bgy
Santo Niño	Bgy
Assumption	Bgy
Bagong Buhay II	Bgy
Bagong Buhay III	Bgy
Ciudad Real	Bgy
Fatima II	Bgy
Fatima III	Bgy
Fatima IV	Bgy
Fatima V	Bgy
Francisco Homes-Guijo	Bgy
Francisco Homes-Mulawin	Bgy
Francisco Homes-Narra	Bgy
Francisco Homes-Yakal	Bgy
Graceville	Bgy
Gumaoc Central	Bgy
Gumaoc East	Bgy
Gumaoc West	Bgy
Lawang Pari	Bgy
Maharlika	Bgy
Minuyan II	Bgy
Minuyan III	Bgy
Minuyan IV	Bgy
Minuyan V	Bgy
Minuyan Proper	Bgy
Paradise III	Bgy
Poblacion I	Bgy
San Isidro	Bgy
San Manuel	Bgy
San Martin II	Bgy
San Martin III	Bgy
San Martin IV	Bgy
San Rafael I	Bgy
San Rafael III	Bgy
San Rafael IV	Bgy
San Rafael V	Bgy
San Roque	Bgy
Santa Cruz II	Bgy
Santa Cruz III	Bgy
Santa Cruz IV	Bgy
Santa Cruz V	Bgy
Santo Niño II	Bgy
St. Martin de Porres	Bgy
San Miguel	Mun
Bagong Silang	Bgy
Balaong	Bgy
Balite	Bgy
Bantog	Bgy
Bardias	Bgy
Baritan	Bgy
Batasan Bata	Bgy
Batasan Matanda	Bgy
Biak-na-Bato	Bgy
Biclat	Bgy
Buga	Bgy
Buliran	Bgy
Bulualto	Bgy
Calumpang	Bgy
Cambio	Bgy
Camias	Bgy
Ilog-Bulo	Bgy
King Kabayo	Bgy
Labne	Bgy
Lambakin	Bgy
Magmarale	Bgy
Malibay	Bgy
Mandile	Bgy
Masalipit	Bgy
Pacalag	Bgy
Paliwasan	Bgy
Partida	Bgy
Pinambaran	Bgy
Poblacion	Bgy
Pulong Bayabas	Bgy
Sacdalan	Bgy
Salacot	Bgy
Salangan	Bgy
San Agustin	Bgy
San Jose	Bgy
San Juan	Bgy
San Vicente	Bgy
Santa Ines	Bgy
Santa Lucia	Bgy
Santa Rita Bata	Bgy
Santa Rita Matanda	Bgy
Sapang	Bgy
Sibul	Bgy
Tartaro	Bgy
Tibagan	Bgy
Bagong Pag-asa	Bgy
Pulong Duhat	Bgy
Maligaya	Bgy
Tigpalas	Bgy
San Rafael	Mun
BMA-Balagtas	Bgy
Banca-banca	Bgy
Caingin	Bgy
Coral na Bato	Bgy
Cruz na Daan	Bgy
Dagat-dagatan	Bgy
Diliman I	Bgy
Diliman II	Bgy
Capihan	Bgy
Libis	Bgy
Lico	Bgy
Maasim	Bgy
Mabalas-balas	Bgy
Maguinao	Bgy
Maronguillo	Bgy
Paco	Bgy
Pansumaloc	Bgy
Pantubig	Bgy
Pasong Bangkal	Bgy
Pasong Callos	Bgy
Pasong Intsik	Bgy
Pinacpinacan	Bgy
Poblacion	Bgy
Pulo	Bgy
Pulong Bayabas	Bgy
Salapungan	Bgy
Sampaloc	Bgy
San Agustin	Bgy
San Roque	Bgy
Talacsan	Bgy
Tambubong	Bgy
Tukod	Bgy
Ulingao	Bgy
Sapang Pahalang	Bgy
Santa Maria	Mun
Bagbaguin	Bgy
Balasing	Bgy
Buenavista	Bgy
Bulac	Bgy
Camangyanan	Bgy
Catmon	Bgy
Cay Pombo	Bgy
Caysio	Bgy
Guyong	Bgy
Lalakhan	Bgy
Mag-asawang Sapa	Bgy
Mahabang Parang	Bgy
Manggahan	Bgy
Parada	Bgy
Poblacion	Bgy
Pulong Buhangin	Bgy
San Gabriel	Bgy
San Jose Patag	Bgy
San Vicente	Bgy
Santa Clara	Bgy
Santa Cruz	Bgy
Silangan	Bgy
Tabing Bakod	Bgy
Tumana	Bgy
Doña Remedios Trinidad	Mun
Bayabas	Bgy
Kabayunan	Bgy
Camachin	Bgy
Camachile	Bgy
Kalawakan	Bgy
Pulong Sampalok	Bgy
Talbak	Bgy
Sapang Bulak	Bgy
Nueva Ecija	Prov
Aliaga	Mun
Betes	Bgy
Bibiclat	Bgy
Bucot	Bgy
La Purisima	Bgy
Magsaysay	Bgy
Macabucod	Bgy
Pantoc	Bgy
Poblacion Centro	Bgy
Poblacion East I	Bgy
Poblacion East II	Bgy
Poblacion West III	Bgy
Poblacion West IV	Bgy
San Carlos	Bgy
San Emiliano	Bgy
San Eustacio	Bgy
San Felipe Bata	Bgy
San Felipe Matanda	Bgy
San Juan	Bgy
San Pablo Bata	Bgy
San Pablo Matanda	Bgy
Santa Monica	Bgy
Santiago	Bgy
Santo Rosario	Bgy
Santo Tomas	Bgy
Sunson	Bgy
Umangan	Bgy
Bongabon	Mun
Antipolo	Bgy
Ariendo	Bgy
Bantug	Bgy
Calaanan	Bgy
Commercial (Pob.)	Bgy
Cruz	Bgy
Digmala	Bgy
Curva	Bgy
Kaingin (Pob.)	Bgy
Labi	Bgy
Larcon	Bgy
Lusok	Bgy
Macabaclay	Bgy
Magtanggol (Pob.)	Bgy
Mantile (Pob.)	Bgy
Olivete	Bgy
Palo Maria (Pob.)	Bgy
Pesa	Bgy
Rizal (Pob.)	Bgy
Sampalucan (Pob.)	Bgy
San Roque (Pob.)	Bgy
Santor	Bgy
Sinipit (Pob.)	Bgy
Sisilang na Ligaya (Pob.)	Bgy
Social (Pob.)	Bgy
Tugatug	Bgy
Tulay na Bato (Pob.)	Bgy
Vega	Bgy
City of Cabanatuan	City
Aduas Centro	Bgy
Bagong Sikat	Bgy
Bagong Buhay	Bgy
Bakero	Bgy
Bakod Bayan	Bgy
Balite	Bgy
Bangad	Bgy
Bantug Bulalo	Bgy
Bantug Norte	Bgy
Barlis	Bgy
Barrera District (Pob.)	Bgy
Bernardo District (Pob.)	Bgy
Bitas	Bgy
Bonifacio District (Pob.)	Bgy
Buliran	Bgy
Caalibangbangan	Bgy
Cabu	Bgy
Campo Tinio	Bgy
Kapitan Pepe (Pob.)	Bgy
Cinco-Cinco	Bgy
City Supermarket (Pob.)	Bgy
Caudillo	Bgy
Communal	Bgy
Cruz Roja	Bgy
Daang Sarile	Bgy
Dalampang	Bgy
Dicarma (Pob.)	Bgy
Dimasalang (Pob.)	Bgy
Dionisio S. Garcia	Bgy
Fatima (Pob.)	Bgy
General Luna (Pob.)	Bgy
Ibabao Bana	Bgy
Imelda District	Bgy
Isla (Pob.)	Bgy
Calawagan	Bgy
Kalikid Norte	Bgy
Kalikid Sur	Bgy
Lagare	Bgy
M. S. Garcia	Bgy
Mabini Extension	Bgy
Mabini Homesite	Bgy
Macatbong	Bgy
Magsaysay District	Bgy
Matadero (Pob.)	Bgy
Lourdes	Bgy
Mayapyap Norte	Bgy
Mayapyap Sur	Bgy
Melojavilla (Pob.)	Bgy
Obrero	Bgy
Padre Crisostomo	Bgy
Pagas	Bgy
Palagay	Bgy
Pamaldan	Bgy
Pangatian	Bgy
Patalac	Bgy
Polilio	Bgy
Pula	Bgy
Quezon District (Pob.)	Bgy
Rizdelis (Pob.)	Bgy
Samon	Bgy
San Isidro	Bgy
San Josef Norte	Bgy
San Josef Sur	Bgy
San Juan Pob.	Bgy
San Roque Norte	Bgy
San Roque Sur	Bgy
Sanbermicristi (Pob.)	Bgy
Sangitan	Bgy
Santa Arcadia	Bgy
Sumacab Norte	Bgy
Valdefuente	Bgy
Valle Cruz	Bgy
Vijandre District (Pob.)	Bgy
Villa Ofelia-Caridad	Bgy
Zulueta District (Pob.)	Bgy
Nabao (Pob.)	Bgy
Padre Burgos (Pob.)	Bgy
Talipapa	Bgy
Aduas Norte	Bgy
Aduas Sur	Bgy
Hermogenes C. Concepcion, Sr.	Bgy
Sapang	Bgy
Sumacab Este	Bgy
Sumacab South	Bgy
Caridad	Bgy
Magsaysay South	Bgy
Maria Theresa	Bgy
Sangitan East	Bgy
Santo Niño	Bgy
Cabiao	Mun
Bagong Buhay	Bgy
Bagong Sikat	Bgy
Bagong Silang	Bgy
Concepcion	Bgy
Entablado	Bgy
Maligaya	Bgy
Natividad North (Pob.)	Bgy
Natividad South (Pob.)	Bgy
Palasinan	Bgy
San Antonio	Bgy
San Fernando Norte	Bgy
San Fernando Sur	Bgy
San Gregorio	Bgy
San Juan North (Pob.)	Bgy
San Juan South (Pob.)	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Rita	Bgy
Sinipit	Bgy
Polilio	Bgy
San Carlos	Bgy
Santa Isabel	Bgy
Santa Ines	Bgy
Carranglan	Mun
R.A.Padilla	Bgy
Bantug	Bgy
Bunga	Bgy
Burgos	Bgy
Capintalan	Bgy
Joson	Bgy
General Luna	Bgy
Minuli	Bgy
Piut	Bgy
Puncan	Bgy
Putlan	Bgy
Salazar	Bgy
San Agustin	Bgy
T. L. Padilla Pob.	Bgy
F. C. Otic Pob.	Bgy
D. L. Maglanoc Pob.	Bgy
G. S. Rosario Pob.	Bgy
Cuyapo	Mun
Baloy	Bgy
Bambanaba	Bgy
Bantug	Bgy
Bentigan	Bgy
Bibiclat	Bgy
Bonifacio	Bgy
Bued	Bgy
Bulala	Bgy
Burgos	Bgy
Cabileo	Bgy
Cabatuan	Bgy
Cacapasan	Bgy
Calancuasan Norte	Bgy
Calancuasan Sur	Bgy
Colosboa	Bgy
Columbitin	Bgy
Curva	Bgy
District I	Bgy
District II	Bgy
District IV	Bgy
District V	Bgy
District VI	Bgy
District VII	Bgy
District VIII	Bgy
Landig	Bgy
Latap	Bgy
Loob	Bgy
Luna	Bgy
Malbeg-Patalan	Bgy
Malineng	Bgy
Matindeg	Bgy
Maycaban	Bgy
Nacuralan	Bgy
Nagmisahan	Bgy
Paitan Norte	Bgy
Paitan Sur	Bgy
Piglisan	Bgy
Pugo	Bgy
Rizal	Bgy
Sabit	Bgy
Salagusog	Bgy
San Antonio	Bgy
San Jose	Bgy
San Juan	Bgy
Santa Clara	Bgy
Santa Cruz	Bgy
Sinimbaan	Bgy
Tagtagumbao	Bgy
Tutuloy	Bgy
Ungab	Bgy
Villaflores	Bgy
Gabaldon	Mun
Bagong Sikat	Bgy
Bagting	Bgy
Bantug	Bgy
Bitulok	Bgy
Bugnan	Bgy
Calabasa	Bgy
Camachile	Bgy
Cuyapa	Bgy
Ligaya	Bgy
Macasandal	Bgy
Malinao	Bgy
Pantoc	Bgy
Pinamalisan	Bgy
South Poblacion	Bgy
Sawmill	Bgy
Tagumpay	Bgy
City of Gapan	City
Bayanihan	Bgy
Bulak	Bgy
Kapalangan	Bgy
Mahipon	Bgy
Malimba	Bgy
Mangino	Bgy
Marelo	Bgy
Pambuan	Bgy
Parcutela	Bgy
San Lorenzo (Pob.)	Bgy
San Nicolas	Bgy
San Roque	Bgy
San Vicente (Pob.)	Bgy
Santa Cruz	Bgy
Santo Cristo Norte	Bgy
Santo Cristo Sur	Bgy
Santo Niño	Bgy
Makabaclay	Bgy
Balante	Bgy
Bungo	Bgy
Mabunga	Bgy
Maburak	Bgy
Puting Tubig	Bgy
General Mamerto Natividad	Mun
Balangkare Norte	Bgy
Balangkare Sur	Bgy
Balaring	Bgy
Belen	Bgy
Bravo	Bgy
Burol	Bgy
Kabulihan	Bgy
Mag-asawang Sampaloc	Bgy
Manarog	Bgy
Mataas na Kahoy	Bgy
Panacsac	Bgy
Picaleon	Bgy
Pinahan	Bgy
Platero	Bgy
Poblacion	Bgy
Pula	Bgy
Pulong Singkamas	Bgy
Sapang Bato	Bgy
Talabutab Norte	Bgy
Talabutab Sur	Bgy
General Tinio	Mun
Bago	Bgy
Concepcion	Bgy
Nazareth	Bgy
Padolina	Bgy
Pias	Bgy
San Pedro (Pob.)	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Rio Chico	Bgy
Poblacion Central	Bgy
Pulong Matong	Bgy
Sampaguita	Bgy
Palale	Bgy
Guimba	Mun
Agcano	Bgy
Ayos Lomboy	Bgy
Bacayao	Bgy
Bagong Barrio	Bgy
Balbalino	Bgy
Balingog East	Bgy
Balingog West	Bgy
Banitan	Bgy
Bantug	Bgy
Bulakid	Bgy
Caballero	Bgy
Cabaruan	Bgy
Caingin Tabing Ilog	Bgy
Calem	Bgy
Camiling	Bgy
Cardinal	Bgy
Casongsong	Bgy
Catimon	Bgy
Cavite	Bgy
Cawayan Bugtong	Bgy
Consuelo	Bgy
Culong	Bgy
Escano	Bgy
Faigal	Bgy
Galvan	Bgy
Guiset	Bgy
Lamorito	Bgy
Lennec	Bgy
Macamias	Bgy
Macapabellag	Bgy
Macatcatuit	Bgy
Manacsac	Bgy
Manggang Marikit	Bgy
Maturanoc	Bgy
Maybubon	Bgy
Naglabrahan	Bgy
Nagpandayan	Bgy
Narvacan I	Bgy
Narvacan II	Bgy
Pacac	Bgy
Partida I	Bgy
Partida II	Bgy
Pasong Inchic	Bgy
Saint John District (Pob.)	Bgy
San Agustin	Bgy
San Andres	Bgy
San Bernardino	Bgy
San Marcelino	Bgy
San Miguel	Bgy
San Rafael	Bgy
San Roque	Bgy
Santa Ana	Bgy
Santa Cruz	Bgy
Santa Lucia	Bgy
Santa Veronica District (Pob.)	Bgy
Santo Cristo District (Pob.)	Bgy
Saranay District (Pob.)	Bgy
Sinulatan	Bgy
Subol	Bgy
Tampac I	Bgy
Tampac II & III	Bgy
Triala	Bgy
Yuson	Bgy
Bunol	Bgy
Jaen	Mun
Calabasa	Bgy
Dampulan (Pob.)	Bgy
Hilera	Bgy
Imbunia	Bgy
Imelda Pob.	Bgy
Lambakin	Bgy
Langla	Bgy
Magsalisi	Bgy
Malabon-Kaingin	Bgy
Marawa	Bgy
Don Mariano Marcos (Pob.)	Bgy
San Josef	Bgy
Niyugan	Bgy
Pamacpacan	Bgy
Pakol	Bgy
Pinanggaan	Bgy
Ulanin-Pitak	Bgy
Putlod	Bgy
Ocampo-Rivera District (Pob.)	Bgy
San Jose	Bgy
San Pablo	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Rita	Bgy
Santo Tomas North	Bgy
Santo Tomas South	Bgy
Sapang	Bgy
Laur	Mun
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Betania	Bgy
Canantong	Bgy
Nauzon	Bgy
Pangarulong	Bgy
Pinagbayanan	Bgy
Sagana	Bgy
San Fernando	Bgy
San Isidro	Bgy
San Josef	Bgy
San Juan	Bgy
San Vicente	Bgy
Siclong	Bgy
San Felipe	Bgy
Licab	Mun
Linao	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
San Casimiro	Bgy
San Cristobal	Bgy
San Jose	Bgy
San Juan	Bgy
Santa Maria	Bgy
Tabing Ilog	Bgy
Villarosa	Bgy
Aquino	Bgy
Llanera	Mun
A. Bonifacio	Bgy
Caridad Norte	Bgy
Caridad Sur	Bgy
Casile	Bgy
Florida Blanca	Bgy
General Luna	Bgy
General Ricarte	Bgy
Gomez	Bgy
Inanama	Bgy
Ligaya	Bgy
Mabini	Bgy
Murcon	Bgy
Plaridel	Bgy
Bagumbayan (Pob.)	Bgy
San Felipe	Bgy
San Francisco	Bgy
San Nicolas	Bgy
San Vicente	Bgy
Santa Barbara	Bgy
Victoria	Bgy
Villa Viniegas	Bgy
Bosque	Bgy
Lupao	Mun
Agupalo Este	Bgy
Agupalo Weste	Bgy
Alalay Chica	Bgy
Alalay Grande	Bgy
J. U. Tienzo	Bgy
Bagong Flores	Bgy
Balbalungao	Bgy
Burgos	Bgy
Cordero	Bgy
Mapangpang	Bgy
Namulandayan	Bgy
Parista	Bgy
Poblacion East	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
Poblacion West	Bgy
Salvacion I	Bgy
Salvacion II	Bgy
San Antonio Este	Bgy
San Antonio Weste	Bgy
San Isidro	Bgy
San Pedro	Bgy
San Roque	Bgy
Santo Domingo	Bgy
Science City of Muñoz	City
Bagong Sikat	Bgy
Balante	Bgy
Bantug	Bgy
Bical	Bgy
Cabisuculan	Bgy
Calabalabaan	Bgy
Calisitan	Bgy
Catalanacan	Bgy
Curva	Bgy
Franza	Bgy
Gabaldon	Bgy
Labney	Bgy
Licaong	Bgy
Linglingay	Bgy
Mangandingay	Bgy
Magtanggol	Bgy
Maligaya	Bgy
Mapangpang	Bgy
Maragol	Bgy
Matingkis	Bgy
Naglabrahan	Bgy
Palusapis	Bgy
Pandalla	Bgy
Poblacion East	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
Poblacion West	Bgy
Rang-ayan	Bgy
Rizal	Bgy
San Andres	Bgy
San Antonio	Bgy
San Felipe	Bgy
Sapang Cawayan	Bgy
Villa Isla	Bgy
Villa Nati	Bgy
Villa Santos	Bgy
Villa Cuizon	Bgy
Nampicuan	Mun
Alemania	Bgy
Ambasador Alzate Village	Bgy
Cabaducan East (Pob.)	Bgy
Cabaducan West (Pob.)	Bgy
Cabawangan	Bgy
East Central Poblacion	Bgy
Edy	Bgy
Maeling	Bgy
Mayantoc	Bgy
Medico	Bgy
Monic	Bgy
North Poblacion	Bgy
Northwest Poblacion	Bgy
Estacion (Pob.)	Bgy
West Poblacion	Bgy
Recuerdo	Bgy
South Central Poblacion	Bgy
Southeast Poblacion	Bgy
Southwest Poblacion	Bgy
Tony	Bgy
West Central Poblacion	Bgy
City of Palayan (Capital)	City
Aulo	Bgy
Bo. Militar	Bgy
Ganaderia	Bgy
Maligaya	Bgy
Manacnac	Bgy
Mapait	Bgy
Marcos Village	Bgy
Malate (Pob.)	Bgy
Sapang Buho	Bgy
Singalat	Bgy
Atate	Bgy
Caballero	Bgy
Caimito	Bgy
Doña Josefa	Bgy
Imelda Valley	Bgy
Langka	Bgy
Santolan	Bgy
Popolon Pagas	Bgy
Bagong Buhay	Bgy
Pantabangan	Mun
Cadaclan	Bgy
Cambitala	Bgy
Conversion	Bgy
Ganduz	Bgy
Liberty	Bgy
Malbang	Bgy
Marikit	Bgy
Napon-Napon	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Sampaloc	Bgy
San Juan	Bgy
Villarica	Bgy
Fatima	Bgy
Peñaranda	Mun
Callos	Bgy
Las Piñas	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Santo Tomas	Bgy
Sinasajan	Bgy
San Josef	Bgy
San Mariano	Bgy
Quezon	Mun
Bertese	Bgy
Doña Lucia	Bgy
Dulong Bayan	Bgy
Ilog Baliwag	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Pulong Bahay	Bgy
San Alejandro	Bgy
San Andres I	Bgy
San Andres II	Bgy
San Manuel	Bgy
Santa Clara	Bgy
Santa Rita	Bgy
Santo Cristo	Bgy
Santo Tomas Feria	Bgy
San Miguel	Bgy
Rizal	Mun
Agbannawag	Bgy
Bicos	Bgy
Cabucbucan	Bgy
Calaocan District	Bgy
Canaan East	Bgy
Canaan West	Bgy
Casilagan	Bgy
Aglipay	Bgy
Del Pilar	Bgy
Estrella	Bgy
General Luna	Bgy
Macapsing	Bgy
Maligaya	Bgy
Paco Roman	Bgy
Pag-asa	Bgy
Poblacion Central	Bgy
Poblacion East	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Poblacion West	Bgy
Portal	Bgy
San Esteban	Bgy
Santa Monica	Bgy
Villa Labrador	Bgy
Villa Paraiso	Bgy
San Gregorio	Bgy
San Antonio	Mun
Buliran	Bgy
Cama Juan	Bgy
Julo	Bgy
Lawang Kupang	Bgy
Luyos	Bgy
Maugat	Bgy
Panabingan	Bgy
Papaya	Bgy
Poblacion	Bgy
San Francisco	Bgy
San Jose	Bgy
San Mariano	Bgy
Santa Cruz	Bgy
Santo Cristo	Bgy
Santa Barbara	Bgy
Tikiw	Bgy
San Isidro	Mun
Alua	Bgy
Calaba	Bgy
Malapit	Bgy
Mangga	Bgy
Poblacion	Bgy
Pulo	Bgy
San Roque	Bgy
Santo Cristo	Bgy
Tabon	Bgy
San Jose City	City
A. Pascual	Bgy
Abar Ist	Bgy
Abar 2nd	Bgy
Bagong Sikat	Bgy
Caanawan	Bgy
Calaocan	Bgy
Camanacsacan	Bgy
Culaylay	Bgy
Dizol	Bgy
Kaliwanagan	Bgy
Kita-Kita	Bgy
Malasin	Bgy
Manicla	Bgy
Palestina	Bgy
Parang Mangga	Bgy
Villa Joson	Bgy
Pinili	Bgy
Rafael Rueda, Sr. Pob.	Bgy
Ferdinand E. Marcos Pob.	Bgy
Canuto Ramos Pob.	Bgy
Raymundo Eugenio Pob.	Bgy
Crisanto Sanchez Pob.	Bgy
Porais	Bgy
San Agustin	Bgy
San Juan	Bgy
San Mauricio	Bgy
Santo Niño 1st	Bgy
Santo Niño 2nd	Bgy
Santo Tomas	Bgy
Sibut	Bgy
Sinipit Bubon	Bgy
Santo Niño 3rd	Bgy
Tabulac	Bgy
Tayabo	Bgy
Tondod	Bgy
Tulat	Bgy
Villa Floresca	Bgy
Villa Marina	Bgy
San Leonardo	Mun
Bonifacio District (Pob.)	Bgy
Burgos District (Pob.)	Bgy
Castellano	Bgy
Diversion	Bgy
Magpapalayoc	Bgy
Mallorca	Bgy
Mambangnan	Bgy
Nieves	Bgy
San Bartolome (Pob.)	Bgy
Rizal District (Pob.)	Bgy
San Anton	Bgy
San Roque	Bgy
Tabuating	Bgy
Tagumpay	Bgy
Tambo Adorable	Bgy
Santa Rosa	Mun
Cojuangco (Pob.)	Bgy
La Fuente	Bgy
Liwayway	Bgy
Malacañang	Bgy
Maliolio	Bgy
Mapalad	Bgy
Rizal (Pob.)	Bgy
Rajal Centro	Bgy
Rajal Norte	Bgy
Rajal Sur	Bgy
San Gregorio	Bgy
San Mariano	Bgy
San Pedro	Bgy
Santo Rosario	Bgy
Soledad	Bgy
Valenzuela (Pob.)	Bgy
Zamora (Pob.)	Bgy
Aguinaldo	Bgy
Berang	Bgy
Burgos	Bgy
Del Pilar	Bgy
Gomez	Bgy
Inspector	Bgy
Isla	Bgy
Lourdes	Bgy
Luna	Bgy
Mabini	Bgy
San Isidro	Bgy
San Josep	Bgy
Santa Teresita	Bgy
Sapsap	Bgy
Tagpos	Bgy
Tramo	Bgy
Santo Domingo	Mun
Baloc	Bgy
Buasao	Bgy
Burgos	Bgy
Cabugao	Bgy
Casulucan	Bgy
Comitang	Bgy
Concepcion	Bgy
Dolores	Bgy
General Luna	Bgy
Hulo	Bgy
Mabini	Bgy
Malasin	Bgy
Malayantoc	Bgy
Mambarao	Bgy
Poblacion	Bgy
Malaya	Bgy
Pulong Buli	Bgy
Sagaba	Bgy
San Agustin	Bgy
San Fabian	Bgy
San Francisco	Bgy
San Pascual	Bgy
Santa Rita	Bgy
Santo Rosario	Bgy
Talavera	Mun
Andal Alino (Pob.)	Bgy
Bagong Sikat	Bgy
Bagong Silang	Bgy
Bakal I	Bgy
Bakal II	Bgy
Bakal III	Bgy
Baluga	Bgy
Bantug	Bgy
Bantug Hacienda	Bgy
Bantug Hamog	Bgy
Bugtong na Buli	Bgy
Bulac	Bgy
Burnay	Bgy
Calipahan	Bgy
Campos	Bgy
Casulucan Este	Bgy
Collado	Bgy
Dimasalang Norte	Bgy
Dimasalang Sur	Bgy
Dinarayat	Bgy
Esguerra District (Pob.)	Bgy
Gulod	Bgy
Homestead I	Bgy
Homestead II	Bgy
Cabubulaonan	Bgy
Caaniplahan	Bgy
Caputican	Bgy
Kinalanguyan	Bgy
La Torre	Bgy
Lomboy	Bgy
Mabuhay	Bgy
Maestrang Kikay (Pob.)	Bgy
Mamandil	Bgy
Marcos District (Pob.)	Bgy
Purok Matias (Pob.)	Bgy
Matingkis	Bgy
Minabuyoc	Bgy
Pag-asa (Pob.)	Bgy
Paludpod	Bgy
Pantoc Bulac	Bgy
Pinagpanaan	Bgy
Poblacion Sur	Bgy
Pula	Bgy
Pulong San Miguel (Pob.)	Bgy
Sampaloc	Bgy
San Miguel na Munti	Bgy
San Pascual	Bgy
San Ricardo	Bgy
Sibul	Bgy
Sicsican Matanda	Bgy
Tabacao	Bgy
Tagaytay	Bgy
Valle	Bgy
Talugtug	Mun
Alula	Bgy
Baybayabas	Bgy
Buted	Bgy
Cabiangan	Bgy
Calisitan	Bgy
Cinense	Bgy
Culiat	Bgy
Maasin	Bgy
Magsaysay (Pob.)	Bgy
Mayamot I	Bgy
Mayamot II	Bgy
Nangabulan	Bgy
Osmeña (Pob.)	Bgy
Pangit	Bgy
Patola	Bgy
Quezon (Pob.)	Bgy
Quirino (Pob.)	Bgy
Roxas (Pob.)	Bgy
Saguing	Bgy
Sampaloc	Bgy
Santa Catalina	Bgy
Santo Domingo	Bgy
Saringaya	Bgy
Saverona	Bgy
Tandoc	Bgy
Tibag	Bgy
Villa Rosario	Bgy
Villa Boado	Bgy
Zaragoza	Mun
Batitang	Bgy
Carmen	Bgy
Concepcion	Bgy
Del Pilar	Bgy
General Luna	Bgy
H. Romero	Bgy
Macarse	Bgy
Manaul	Bgy
Mayamot	Bgy
Pantoc	Bgy
San Vicente (Pob.)	Bgy
San Isidro	Bgy
San Rafael	Bgy
Santa Cruz	Bgy
Santa Lucia Old	Bgy
Santa Lucia Young	Bgy
Santo Rosario Old	Bgy
Santo Rosario Young	Bgy
Valeriana	Bgy
Pampanga	Prov
City of Angeles	City
Agapito del Rosario	Bgy
Anunas	Bgy
Balibago	Bgy
Capaya	Bgy
Claro M. Recto	Bgy
Cuayan	Bgy
Cutcut	Bgy
Cutud	Bgy
Lourdes North West	Bgy
Lourdes Sur	Bgy
Lourdes Sur East	Bgy
Malabanias	Bgy
Margot	Bgy
Mining	Bgy
Pampang	Bgy
Pandan	Bgy
Pulung Maragul	Bgy
Pulungbulu	Bgy
Pulung Cacutud	Bgy
Salapungan	Bgy
San Jose	Bgy
San Nicolas	Bgy
Santa Teresita	Bgy
Santa Trinidad	Bgy
Santo Cristo	Bgy
Santo Domingo	Bgy
Santo Rosario (Pob.)	Bgy
Sapalibutad	Bgy
Sapangbato	Bgy
Tabun	Bgy
Virgen Delos Remedios	Bgy
Amsic	Bgy
Ninoy Aquino	Bgy
Apalit	Mun
Balucuc	Bgy
Calantipe	Bgy
Cansinala	Bgy
Capalangan	Bgy
Colgante	Bgy
Paligui	Bgy
Sampaloc	Bgy
San Juan (Pob.)	Bgy
San Vicente	Bgy
Sucad	Bgy
Sulipan	Bgy
Tabuyuc	Bgy
Arayat	Mun
Arenas	Bgy
Baliti	Bgy
Batasan	Bgy
Buensuceso	Bgy
Candating	Bgy
Gatiawin	Bgy
Guemasan	Bgy
La Paz	Bgy
Lacmit	Bgy
Lacquios	Bgy
Mangga-Cacutud	Bgy
Mapalad	Bgy
Panlinlang	Bgy
Paralaya	Bgy
Plazang Luma	Bgy
Poblacion	Bgy
San Agustin Norte	Bgy
San Agustin Sur	Bgy
San Antonio	Bgy
San Jose Mesulo	Bgy
San Juan Bano	Bgy
San Mateo	Bgy
San Nicolas	Bgy
San Roque Bitas	Bgy
Cupang	Bgy
Matamo	Bgy
Santo Niño Tabuan	Bgy
Suclayin	Bgy
Telapayong	Bgy
Kaledian	Bgy
Bacolor	Mun
Balas	Bgy
Cabalantian	Bgy
Cabambangan (Pob.)	Bgy
Cabetican	Bgy
Calibutbut	Bgy
Concepcion	Bgy
Dolores	Bgy
Duat	Bgy
Macabacle	Bgy
Magliman	Bgy
Maliwalu	Bgy
Mesalipit	Bgy
Parulog	Bgy
Potrero	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Vicente	Bgy
Santa Barbara	Bgy
Santa Ines	Bgy
Talba	Bgy
Tinajero	Bgy
Candaba	Mun
Bahay Pare	Bgy
Bambang	Bgy
Barangca	Bgy
Barit	Bgy
Buas (Pob.)	Bgy
Cuayang Bugtong	Bgy
Dalayap	Bgy
Dulong Ilog	Bgy
Gulap	Bgy
Lanang	Bgy
Lourdes	Bgy
Magumbali	Bgy
Mandasig	Bgy
Mandili	Bgy
Mangga	Bgy
Mapaniqui	Bgy
Paligui	Bgy
Pangclara	Bgy
Pansinao	Bgy
Paralaya (Pob.)	Bgy
Pasig	Bgy
Pescadores (Pob.)	Bgy
Pulong Gubat	Bgy
Pulong Palazan	Bgy
Salapungan	Bgy
San Agustin (Pob.)	Bgy
Santo Rosario	Bgy
Tagulod	Bgy
Talang	Bgy
Tenejero	Bgy
Vizal San Pablo	Bgy
Vizal Santo Cristo	Bgy
Vizal Santo Niño	Bgy
Floridablanca	Mun
Anon	Bgy
Apalit	Bgy
Basa Air Base	Bgy
Benedicto	Bgy
Bodega	Bgy
Cabangcalan	Bgy
Calantas	Bgy
Carmencita	Bgy
Consuelo	Bgy
Dampe	Bgy
Del Carmen	Bgy
Fortuna	Bgy
Gutad	Bgy
Mabical	Bgy
Santo Rosario	Bgy
Maligaya	Bgy
Nabuclod	Bgy
Pabanlag	Bgy
Paguiruan	Bgy
Palmayo	Bgy
Pandaguirig	Bgy
Poblacion	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Ramon	Bgy
San Roque	Bgy
Santa Monica	Bgy
Solib	Bgy
Valdez	Bgy
Mawacat	Bgy
Guagua	Mun
Bancal	Bgy
Jose Abad Santos	Bgy
Lambac	Bgy
Magsaysay	Bgy
Maquiapo	Bgy
Natividad	Bgy
Plaza Burgos (Pob.)	Bgy
Pulungmasle	Bgy
Rizal	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan Bautista	Bgy
San Juan Nepomuceno	Bgy
San Matias	Bgy
San Miguel	Bgy
San Nicolas 1st	Bgy
San Nicolas 2nd	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
San Juan	Bgy
Santa Filomena (Pob.)	Bgy
Santa Ines	Bgy
Santa Ursula	Bgy
Santo Cristo	Bgy
Santo Niño (Pob.)	Bgy
Ascomo	Bgy
Lubao	Mun
Balantacan	Bgy
Bancal Sinubli	Bgy
Bancal Pugad	Bgy
Baruya	Bgy
Calangain	Bgy
Concepcion	Bgy
Del Carmen	Bgy
De La Paz	Bgy
Don Ignacio Dimson	Bgy
Lourdes	Bgy
Prado Siongco	Bgy
Remedios	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose Apunan	Bgy
San Jose Gumi	Bgy
San Juan (Pob.)	Bgy
San Matias	Bgy
San Miguel	Bgy
San Nicolas 1st (Pob.)	Bgy
San Nicolas 2nd	Bgy
San Pablo 1st	Bgy
San Pablo 2nd	Bgy
San Pedro Palcarangan	Bgy
San Pedro Saug	Bgy
San Roque Arbol	Bgy
San Roque Dau	Bgy
San Vicente	Bgy
Santa Barbara	Bgy
Santa Catalina	Bgy
Santa Cruz	Bgy
Santa Lucia (Pob.)	Bgy
Santa Maria	Bgy
Santa Monica	Bgy
Santa Rita	Bgy
Santa Teresa 1st	Bgy
Santa Teresa 2nd	Bgy
Santiago	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Santo Tomas (Pob.)	Bgy
Santo Cristo	Bgy
Mabalacat City	City
Atlu-Bola	Bgy
Bical	Bgy
Bundagul	Bgy
Cacutud	Bgy
Calumpang	Bgy
Camachiles	Bgy
Dapdap	Bgy
Dau	Bgy
Dolores	Bgy
Duquit	Bgy
Lakandula	Bgy
Mabiga	Bgy
Macapagal Village	Bgy
Mamatitang	Bgy
Mangalit	Bgy
Marcos Village	Bgy
Mawaque	Bgy
Paralayunan	Bgy
Poblacion	Bgy
San Francisco	Bgy
San Joaquin	Bgy
Santa Ines	Bgy
Santa Maria	Bgy
Santo Rosario	Bgy
Sapang Balen	Bgy
Sapang Biabas	Bgy
Tabun	Bgy
Macabebe	Mun
Batasan	Bgy
Caduang Tete	Bgy
Candelaria	Bgy
Castuli	Bgy
Consuelo	Bgy
Dalayap	Bgy
Mataguiti	Bgy
San Esteban	Bgy
San Francisco	Bgy
San Gabriel (Pob.)	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santa Lutgarda	Bgy
Santa Maria	Bgy
Santa Rita (Pob.)	Bgy
Santo Niño	Bgy
Santo Rosario (Pob.)	Bgy
Saplad David	Bgy
Tacasan	Bgy
Telacsan	Bgy
Magalang	Mun
Camias	Bgy
Dolores	Bgy
Escaler	Bgy
La Paz	Bgy
Navaling	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Franciso	Bgy
San Ildefonso	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Nicolas 1st (Pob.)	Bgy
San Nicolas 2nd	Bgy
San Pablo (Pob.)	Bgy
San Pedro I	Bgy
San Pedro II	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santa Lucia	Bgy
Santa Maria	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Bucanan	Bgy
Turu	Bgy
Ayala	Bgy
Masantol	Mun
Alauli	Bgy
Bagang	Bgy
Balibago	Bgy
Bebe Anac	Bgy
Bebe Matua	Bgy
Bulacus	Bgy
San Agustin	Bgy
Santa Monica	Bgy
Cambasi	Bgy
Malauli	Bgy
Nigui	Bgy
Palimpe	Bgy
Puti	Bgy
Sagrada	Bgy
San Isidro Anac	Bgy
San Isidro Matua (Pob.)	Bgy
San Nicolas (Pob.)	Bgy
San Pedro	Bgy
Santa Cruz	Bgy
Santa Lucia Matua	Bgy
Santa Lucia Paguiba	Bgy
Santa Lucia Wakas	Bgy
Santa Lucia Anac (Pob.)	Bgy
Sapang Kawayan	Bgy
Sua	Bgy
Santo Niño	Bgy
Mexico	Mun
Acli	Bgy
Anao	Bgy
Balas	Bgy
Buenavista	Bgy
Camuning	Bgy
Cawayan	Bgy
Concepcion	Bgy
Culubasa	Bgy
Divisoria	Bgy
Dolores	Bgy
Eden	Bgy
Gandus	Bgy
Lagundi	Bgy
Laput	Bgy
Laug	Bgy
Masamat	Bgy
Masangsang	Bgy
Nueva Victoria	Bgy
Pandacaqui	Bgy
Pangatlan	Bgy
Panipuan	Bgy
Parian (Pob.)	Bgy
Sabanilla	Bgy
San Antonio	Bgy
San Carlos	Bgy
San Jose Malino	Bgy
San Jose Matulid	Bgy
San Juan	Bgy
San Lorenzo	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pablo	Bgy
San Patricio	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santo Domingo	Bgy
Santo Rosario	Bgy
Sapang Maisac	Bgy
Suclaban	Bgy
Tangle	Bgy
Minalin	Mun
Bulac	Bgy
Dawe	Bgy
Lourdes	Bgy
Maniango	Bgy
San Francisco 1st	Bgy
San Francisco 2nd	Bgy
San Isidro	Bgy
San Nicolas (Pob.)	Bgy
San Pedro	Bgy
Santa Catalina	Bgy
Santa Maria	Bgy
Santa Rita	Bgy
Santo Domingo	Bgy
Santo Rosario (Pob.)	Bgy
Saplad	Bgy
Porac	Mun
Babo Pangulo	Bgy
Babo Sacan	Bgy
Balubad	Bgy
Calzadang Bayu	Bgy
Camias	Bgy
Cangatba	Bgy
Diaz	Bgy
Dolores	Bgy
Jalung	Bgy
Mancatian	Bgy
Manibaug Libutad	Bgy
Manibaug Paralaya	Bgy
Manibaug Pasig	Bgy
Manuali	Bgy
Mitla Proper	Bgy
Palat	Bgy
Pias	Bgy
Pio	Bgy
Planas	Bgy
Poblacion	Bgy
Pulong Santol	Bgy
Salu	Bgy
San Jose Mitla	Bgy
Santa Cruz	Bgy
Sepung Bulaun	Bgy
Sinura	Bgy
Villa Maria	Bgy
Inararo	Bgy
Sapang Uwak	Bgy
City of San Fernando (Capital)	City
Alasas	Bgy
Baliti	Bgy
Bulaon	Bgy
Calulut	Bgy
Dela Paz Norte	Bgy
Dela Paz Sur	Bgy
Del Carmen	Bgy
Del Pilar	Bgy
Del Rosario	Bgy
Dolores	Bgy
Juliana	Bgy
Lara	Bgy
Lourdes	Bgy
Magliman	Bgy
Maimpis	Bgy
Malino	Bgy
Malpitic	Bgy
Pandaras	Bgy
Panipuan	Bgy
Santo Rosario (Pob.)	Bgy
Quebiauan	Bgy
Saguin	Bgy
San Agustin	Bgy
San Felipe	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Nicolas	Bgy
San Pedro	Bgy
Santa Lucia	Bgy
Santa Teresita	Bgy
Santo Niño	Bgy
Sindalan	Bgy
Telabastagan	Bgy
Pulung Bulu	Bgy
San Luis	Mun
San Agustin	Bgy
San Carlos	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Nicolas	Bgy
San Roque	Bgy
San Sebastian	Bgy
Santa Catalina	Bgy
Santa Cruz Pambilog	Bgy
Santa Cruz Poblacion	Bgy
Santa Lucia	Bgy
Santa Monica	Bgy
Santa Rita	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Santo Tomas	Bgy
San Simon	Mun
Concepcion	Bgy
De La Paz	Bgy
San Juan (Pob.)	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pablo Libutad	Bgy
San Pablo Proper	Bgy
San Pedro	Bgy
Santa Cruz	Bgy
Santa Monica	Bgy
Santo Niño	Bgy
Santa Ana	Mun
San Agustin	Bgy
San Bartolome	Bgy
San Isidro	Bgy
San Joaquin (Pob.)	Bgy
San Jose	Bgy
San Juan	Bgy
San Nicolas	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Roque	Bgy
Santa Lucia	Bgy
Santa Maria	Bgy
Santiago	Bgy
Santo Rosario	Bgy
Santa Rita	Mun
Becuran	Bgy
Dila-dila	Bgy
San Agustin	Bgy
San Basilio	Bgy
San Isidro	Bgy
San Jose (Pob.)	Bgy
San Juan	Bgy
San Matias	Bgy
San Vicente	Bgy
Santa Monica	Bgy
Santo Tomas	Mun
Moras De La Paz	Bgy
Poblacion	Bgy
San Bartolome	Bgy
San Matias	Bgy
San Vicente	Bgy
Santo Rosario	Bgy
Sapa	Bgy
Sasmuan	Mun
Batang 1st	Bgy
Batang 2nd	Bgy
Mabuanbuan	Bgy
Malusac	Bgy
Santa Lucia (Pob.)	Bgy
San Antonio	Bgy
San Nicolas 1st	Bgy
San Nicolas 2nd	Bgy
San Pedro	Bgy
Santa Monica	Bgy
Santo Tomas	Bgy
Sabitanan	Bgy
Tarlac	Prov
Anao	Mun
Baguindoc	Bgy
Bantog	Bgy
Campos	Bgy
Carmen	Bgy
Casili	Bgy
Don Ramon	Bgy
Hernando	Bgy
Poblacion	Bgy
Rizal	Bgy
San Francisco East	Bgy
San Francisco West	Bgy
San Jose North	Bgy
San Jose South	Bgy
San Juan	Bgy
San Roque	Bgy
Santo Domingo	Bgy
Sinense	Bgy
Suaverdez	Bgy
Bamban	Mun
Anupul	Bgy
Banaba	Bgy
Bangcu	Bgy
Culubasa	Bgy
Dela Cruz	Bgy
La Paz	Bgy
Lourdes	Bgy
Malonzo	Bgy
Virgen de los Remedios	Bgy
San Nicolas (Pob.)	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Camiling	Mun
Anoling 1st	Bgy
Anoling 2nd	Bgy
Anoling 3rd	Bgy
Bacabac	Bgy
Bacsay	Bgy
Bancay 1st	Bgy
San Isidro	Bgy
Bilad	Bgy
Birbira	Bgy
Bobon Caarosipan	Bgy
Bobon 1st	Bgy
Bobon 2nd	Bgy
Cabanabaan	Bgy
Cacamilingan Norte	Bgy
Cacamilingan Sur	Bgy
Caniag	Bgy
Carael	Bgy
Cayaoan	Bgy
Cayasan	Bgy
Florida	Bgy
Lasong	Bgy
Libueg	Bgy
Malacampa	Bgy
Manakem	Bgy
Manupeg	Bgy
Marawi	Bgy
Matubog	Bgy
Nagrambacan	Bgy
Nagserialan	Bgy
Palimbo Proper	Bgy
Palimbo-Caarosipan	Bgy
Pao 1st	Bgy
Pao 2nd	Bgy
Pao 3rd	Bgy
Papaac	Bgy
Pindangan 1st	Bgy
Pindangan 2nd	Bgy
Poblacion A	Bgy
Poblacion B	Bgy
Poblacion C	Bgy
Poblacion D	Bgy
Poblacion E	Bgy
Poblacion F	Bgy
Poblacion G	Bgy
Poblacion H	Bgy
Poblacion I	Bgy
Poblacion J	Bgy
Santa Maria	Bgy
Sawat	Bgy
Sinilian 1st	Bgy
Sinilian 2nd	Bgy
Sinilian 3rd	Bgy
Sinilian Cacalibosoan	Bgy
Sinulatan 1st	Bgy
Sinulatan 2nd	Bgy
Surgui 1st	Bgy
Surgui 2nd	Bgy
Surgui 3rd	Bgy
Tambugan	Bgy
Telbang	Bgy
Tuec	Bgy
Capas	Mun
Aranguren	Bgy
Bueno	Bgy
Cubcub (Pob.)	Bgy
Cutcut 1st	Bgy
Cutcut 2nd	Bgy
Dolores	Bgy
Estrada	Bgy
Lawy	Bgy
Manga	Bgy
Manlapig	Bgy
O'Donnell	Bgy
Santa Lucia	Bgy
Santa Rita	Bgy
Santo Domingo 1st	Bgy
Santo Domingo 2nd	Bgy
Santo Rosario	Bgy
Talaga	Bgy
Maruglu	Bgy
Santa Juliana	Bgy
Cristo Rey	Bgy
Concepcion	Mun
Alfonso	Bgy
Balutu	Bgy
Cafe	Bgy
Calius Gueco	Bgy
Caluluan	Bgy
Castillo	Bgy
Corazon de Jesus	Bgy
Culatingan	Bgy
Dungan	Bgy
Dutung-A-Matas	Bgy
Lilibangan	Bgy
Mabilog	Bgy
Magao	Bgy
Malupa	Bgy
Minane	Bgy
Panalicsian	Bgy
Pando	Bgy
Parang	Bgy
Parulung	Bgy
Pitabunan	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Bartolome	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose (Pob.)	Bgy
San Juan	Bgy
San Martin	Bgy
San Nicolas (Pob.)	Bgy
San Nicolas Balas	Bgy
Santo Niño	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santa Monica	Bgy
Santa Rita	Bgy
Santa Rosa	Bgy
Santiago	Bgy
Santo Cristo	Bgy
Santo Rosario	Bgy
Talimunduc Marimla	Bgy
Talimunduc San Miguel	Bgy
Telabanca	Bgy
Tinang	Bgy
San Vicente	Bgy
Green Village	Bgy
Gerona	Mun
Abagon	Bgy
Amacalan	Bgy
Apsayan	Bgy
Ayson	Bgy
Bawa	Bgy
Buenlag	Bgy
Bularit	Bgy
Calayaan	Bgy
Carbonel	Bgy
Cardona	Bgy
Caturay	Bgy
Danzo	Bgy
Dicolor	Bgy
Don Basilio	Bgy
Luna	Bgy
Mabini	Bgy
Magaspac	Bgy
Malayep	Bgy
Matapitap	Bgy
Matayumcab	Bgy
New Salem	Bgy
Oloybuaya	Bgy
Padapada	Bgy
Parsolingan	Bgy
Pinasling	Bgy
Plastado	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Quezon	Bgy
Rizal	Bgy
Salapungan	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Bartolome	Bgy
San Jose	Bgy
Santa Lucia	Bgy
Santiago	Bgy
Sembrano	Bgy
Singat	Bgy
Sulipa	Bgy
Tagumbao	Bgy
Tangcaran	Bgy
Villa Paz	Bgy
La Paz	Mun
Balanoy	Bgy
Bantog-Caricutan	Bgy
Caramutan	Bgy
Caut	Bgy
Comillas	Bgy
Dumarais	Bgy
Guevarra	Bgy
Kapanikian	Bgy
La Purisima	Bgy
Lara	Bgy
Laungcupang	Bgy
Lomboy	Bgy
Macalong	Bgy
Matayumtayum	Bgy
Mayang	Bgy
Motrico	Bgy
Paludpud	Bgy
Rizal	Bgy
San Isidro (Pob.)	Bgy
San Roque (Pob.)	Bgy
Sierra	Bgy
Mayantoc	Mun
Ambalingit	Bgy
Baybayaoas	Bgy
Bigbiga	Bgy
Binbinaca	Bgy
Calabtangan	Bgy
Caocaoayan	Bgy
Carabaoan	Bgy
Cubcub	Bgy
Gayonggayong	Bgy
Gossood	Bgy
Labney	Bgy
Mamonit	Bgy
Maniniog	Bgy
Mapandan	Bgy
Nambalan	Bgy
Pedro L. Quines	Bgy
Pitombayog	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Rotrottooc	Bgy
San Bartolome	Bgy
San Jose	Bgy
Taldiapan	Bgy
Tangcarang	Bgy
Moncada	Mun
Ablang-Sapang	Bgy
Aringin	Bgy
Atencio	Bgy
Banaoang East	Bgy
Banaoang West	Bgy
Baquero Norte	Bgy
Baquero Sur	Bgy
Burgos	Bgy
Calamay	Bgy
Calapan	Bgy
Camangaan East	Bgy
Camangaan West	Bgy
Camposanto 1 - Norte	Bgy
Camposanto 1 - Sur	Bgy
Camposanto 2	Bgy
Capaoayan	Bgy
Lapsing	Bgy
Mabini	Bgy
Maluac	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Rizal	Bgy
San Juan	Bgy
San Julian	Bgy
San Leon	Bgy
San Pedro	Bgy
San Roque	Bgy
Santa Lucia East	Bgy
Santa Lucia West	Bgy
Santa Maria	Bgy
Santa Monica	Bgy
Tubectubang	Bgy
Tolega Norte	Bgy
Tolega Sur	Bgy
Villa	Bgy
Paniqui	Mun
Abogado	Bgy
Acocolao	Bgy
Aduas	Bgy
Apulid	Bgy
Balaoang	Bgy
Barang	Bgy
Brillante	Bgy
Burgos	Bgy
Cabayaoasan	Bgy
Canan	Bgy
Carino	Bgy
Cayanga	Bgy
Colibangbang	Bgy
Coral	Bgy
Dapdap	Bgy
Estacion	Bgy
Mabilang	Bgy
Manaois	Bgy
Matalapitap	Bgy
Nagmisaan	Bgy
Nancamarinan	Bgy
Nipaco	Bgy
Patalan	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Rang-ayan	Bgy
Salumague	Bgy
Samput	Bgy
San Carlos	Bgy
San Isidro	Bgy
San Juan de Milla	Bgy
Santa Ines	Bgy
Sinigpit	Bgy
Tablang	Bgy
Ventenilla	Bgy
Pura	Mun
Balite	Bgy
Buenavista	Bgy
Cadanglaan	Bgy
Estipona	Bgy
Linao	Bgy
Maasin	Bgy
Matindeg	Bgy
Maungib	Bgy
Naya	Bgy
Nilasin 1st	Bgy
Nilasin 2nd	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poroc	Bgy
Singat	Bgy
Ramos	Mun
Coral-Iloco	Bgy
Guiteb	Bgy
Pance	Bgy
Poblacion Center	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
San Juan	Bgy
San Raymundo	Bgy
Toledo	Bgy
San Clemente	Mun
Balloc	Bgy
Bamban	Bgy
Casipo	Bgy
Catagudingan	Bgy
Daldalayap	Bgy
Doclong 1	Bgy
Doclong 2	Bgy
Maasin	Bgy
Nagsabaran	Bgy
Pit-ao	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
San Manuel	Mun
Colubot	Bgy
Lanat	Bgy
Legaspi	Bgy
Mangandingay	Bgy
Matarannoc	Bgy
Pacpaco	Bgy
Poblacion	Bgy
Salcedo	Bgy
San Agustin	Bgy
San Felipe	Bgy
San Jacinto	Bgy
San Miguel	Bgy
San Narciso	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Santa Ignacia	Mun
Baldios	Bgy
Botbotones	Bgy
Caanamongan	Bgy
Cabaruan	Bgy
Cabugbugan	Bgy
Caduldulaoan	Bgy
Calipayan	Bgy
Macaguing	Bgy
Nambalan	Bgy
Padapada	Bgy
Pilpila	Bgy
Pinpinas	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Pugo-Cecilio	Bgy
San Francisco	Bgy
San Sotero	Bgy
San Vicente	Bgy
Santa Ines Centro	Bgy
Santa Ines East	Bgy
Santa Ines West	Bgy
Taguiporo	Bgy
Timmaguab	Bgy
Vargas	Bgy
City of Tarlac (Capital)	City
Aguso	Bgy
Alvindia Segundo	Bgy
Amucao	Bgy
Armenia	Bgy
Asturias	Bgy
Atioc	Bgy
Balanti	Bgy
Balete	Bgy
Balibago I	Bgy
Balibago II	Bgy
Balingcanaway	Bgy
Banaba	Bgy
Bantog	Bgy
Baras-baras	Bgy
Batang-batang	Bgy
Binauganan	Bgy
Bora	Bgy
Buenavista	Bgy
Buhilit	Bgy
Burot	Bgy
Calingcuan	Bgy
Capehan	Bgy
Carangian	Bgy
Central	Bgy
Culipat	Bgy
Cut-cut I	Bgy
Cut-cut II	Bgy
Dalayap	Bgy
Dela Paz	Bgy
Dolores	Bgy
Laoang	Bgy
Ligtasan	Bgy
Lourdes	Bgy
Mabini	Bgy
Maligaya	Bgy
Maliwalo	Bgy
Mapalacsiao	Bgy
Mapalad	Bgy
Matatalaib	Bgy
Paraiso	Bgy
Poblacion	Bgy
San Carlos	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Jose de Urquico	Bgy
San Juan de Mata	Bgy
San Luis	Bgy
San Manuel	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pablo	Bgy
San Pascual	Bgy
San Rafael	Bgy
San Roque	Bgy
San Sebastian	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santo Cristo	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Sapang Maragul	Bgy
Sapang Tagalog	Bgy
Sepung Calzada	Bgy
Sinait	Bgy
Suizo	Bgy
Tariji	Bgy
Tibag	Bgy
Tibagan	Bgy
Trinidad	Bgy
Ungot	Bgy
Matadero	Bgy
Salapungan	Bgy
Villa Bacolor	Bgy
Care	Bgy
Victoria	Mun
Baculong	Bgy
Balayang	Bgy
Balbaloto	Bgy
Bangar	Bgy
Bantog	Bgy
Batangbatang	Bgy
Bulo	Bgy
Cabuluan	Bgy
Calibungan	Bgy
Canarem	Bgy
Cruz	Bgy
Lalapac	Bgy
Maluid	Bgy
Mangolago	Bgy
Masalasa	Bgy
Palacpalac	Bgy
San Agustin	Bgy
San Andres	Bgy
San Fernando (Pob.)	Bgy
San Francisco	Bgy
San Gavino (Pob.)	Bgy
San Jacinto	Bgy
San Nicolas (Pob.)	Bgy
San Vicente	Bgy
Santa Barbara	Bgy
Santa Lucia (Pob.)	Bgy
San Jose	Mun
Burgos	Bgy
David	Bgy
Iba	Bgy
Labney	Bgy
Lawacamulag	Bgy
Lubigan	Bgy
Maamot	Bgy
Mababanaba	Bgy
Moriones	Bgy
Pao	Bgy
San Juan de Valdez	Bgy
Sula	Bgy
Villa Aglipay	Bgy
Zambales	Prov
Botolan	Mun
Bangan	Bgy
Batonlapoc	Bgy
Beneg	Bgy
Capayawan	Bgy
Carael	Bgy
Danacbunga	Bgy
Maguisguis	Bgy
Mambog	Bgy
Moraza	Bgy
Paco (Pob.)	Bgy
Panan	Bgy
Parel	Bgy
Paudpod	Bgy
Poonbato	Bgy
Porac	Bgy
San Isidro	Bgy
San Juan	Bgy
San Miguel	Bgy
Santiago	Bgy
Tampo (Pob.)	Bgy
Taugtog	Bgy
Villar	Bgy
Bancal	Bgy
Belbel	Bgy
Binuclutan	Bgy
Burgos	Bgy
Cabatuan	Bgy
Malomboy	Bgy
Nacolcol	Bgy
Owaog-Nibloc	Bgy
Palis	Bgy
Cabangan	Mun
Anonang	Bgy
Apo-apo	Bgy
Arew	Bgy
Banuambayo (Pob.)	Bgy
Cadmang-Reserva	Bgy
Camiling	Bgy
Casabaan	Bgy
Dolores (Pob.)	Bgy
Del Carmen (Pob.)	Bgy
Laoag	Bgy
Lomboy	Bgy
Longos	Bgy
Mabanglit	Bgy
New San Juan	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Juan (Pob.)	Bgy
San Rafael	Bgy
Santa Rita	Bgy
Santo Niño	Bgy
Tondo	Bgy
Felmida-Diaz	Bgy
Candelaria	Mun
Babancal	Bgy
Binabalian	Bgy
Catol	Bgy
Dampay	Bgy
Lauis	Bgy
Libertador	Bgy
Malabon	Bgy
Malimanga	Bgy
Pamibian	Bgy
Panayonan	Bgy
Pinagrealan	Bgy
Poblacion	Bgy
Sinabacan	Bgy
Taposo	Bgy
Uacon	Bgy
Yamot	Bgy
Castillejos	Mun
Balaybay	Bgy
Buenavista	Bgy
Del Pilar	Bgy
Looc	Bgy
Magsaysay	Bgy
Nagbayan	Bgy
Nagbunga	Bgy
San Agustin	Bgy
San Jose (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Nicolas	Bgy
San Pablo (Pob.)	Bgy
San Roque	Bgy
Santa Maria	Bgy
Iba (Capital)	Mun
Amungan	Bgy
Zone 2 Pob.	Bgy
Zone 5 Pob.	Bgy
Bangantalinga	Bgy
Zone 6 Pob.	Bgy
Zone 3 Pob.	Bgy
Dirita-Baloguen	Bgy
Zone 1 Pob.	Bgy
Lipay-Dingin-Panibuatan	Bgy
Palanginan	Bgy
Zone 4 Pob.	Bgy
San Agustin	Bgy
Santa Barbara	Bgy
Santo Rosario	Bgy
Masinloc	Mun
Baloganon	Bgy
Bamban	Bgy
Bani	Bgy
Collat	Bgy
Inhobol	Bgy
North Poblacion	Bgy
San Lorenzo	Bgy
San Salvador	Bgy
Santa Rita	Bgy
Santo Rosario	Bgy
South Poblacion	Bgy
Taltal	Bgy
Tapuac	Bgy
City of Olongapo	City
Asinan	Bgy
Banicain	Bgy
Barreto	Bgy
East Bajac-bajac	Bgy
East Tapinac	Bgy
Gordon Heights	Bgy
Kalaklan	Bgy
New Kalalake	Bgy
Mabayuan	Bgy
New Cabalan	Bgy
New Ilalim	Bgy
New Kababae	Bgy
Pag-asa	Bgy
Santa Rita	Bgy
West Bajac-bajac	Bgy
West Tapinac	Bgy
Old Cabalan	Bgy
Palauig	Mun
Alwa	Bgy
Bato	Bgy
Bulawen	Bgy
Cauyan	Bgy
Garreta	Bgy
Libaba	Bgy
Liozon	Bgy
Lipay	Bgy
Locloc	Bgy
Macarang	Bgy
Magalawa	Bgy
Pangolingan	Bgy
East Poblacion	Bgy
West Poblacion	Bgy
Salaza	Bgy
San Juan	Bgy
Santo Niño	Bgy
Santo Tomas	Bgy
Tition	Bgy
San Antonio	Mun
Angeles	Bgy
Antipolo (Pob.)	Bgy
Burgos (Pob.)	Bgy
West Dirita	Bgy
East Dirita	Bgy
Luna (Pob.)	Bgy
Pundaquit	Bgy
San Esteban	Bgy
San Gregorio (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Miguel	Bgy
San Nicolas (Pob.)	Bgy
Santiago	Bgy
Rizal	Bgy
San Felipe	Mun
Amagna (Pob.)	Bgy
Apostol (Pob.)	Bgy
Balincaguing	Bgy
Farañal (Pob.)	Bgy
Feria (Pob.)	Bgy
Maloma	Bgy
Manglicmot (Pob.)	Bgy
Rosete (Pob.)	Bgy
San Rafael	Bgy
Santo Niño	Bgy
Sindol	Bgy
San Marcelino	Mun
Aglao	Bgy
Buhawen	Bgy
Burgos (Pob.)	Bgy
Consuelo Norte	Bgy
Consuelo Sur (Pob.)	Bgy
La Paz (Pob.)	Bgy
Laoag	Bgy
Linasin	Bgy
Linusungan	Bgy
Lucero (Pob.)	Bgy
Nagbunga	Bgy
Rizal (Pob.)	Bgy
San Guillermo (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Rafael	Bgy
Santa Fe	Bgy
Central (Pob.)	Bgy
Rabanes	Bgy
San Narciso	Mun
Alusiis	Bgy
Beddeng	Bgy
Candelaria (Pob.)	Bgy
Dallipawen	Bgy
Grullo	Bgy
La Paz	Bgy
Libertad (Pob.)	Bgy
Namatacan	Bgy
Natividad (Pob.)	Bgy
Paite	Bgy
Patrocinio (Pob.)	Bgy
San Jose (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Pascual (Pob.)	Bgy
San Rafael (Pob.)	Bgy
Siminublan	Bgy
Omaya	Bgy
Santa Cruz	Mun
Babuyan	Bgy
Bolitoc	Bgy
Bangcol	Bgy
Bayto	Bgy
Biay	Bgy
Canaynayan	Bgy
Gama	Bgy
Guinabon	Bgy
Guisguis	Bgy
Lipay	Bgy
Lomboy	Bgy
Lucapon North	Bgy
Lucapon South	Bgy
Malabago	Bgy
Naulo	Bgy
Poblacion North	Bgy
Pagatpat	Bgy
Pamonoran	Bgy
Sabang	Bgy
San Fernando	Bgy
Poblacion South	Bgy
Tabalong	Bgy
Tubotubo North	Bgy
Tubotubo South	Bgy
Bulawon	Bgy
Subic	Mun
Aningway Sacatihan	Bgy
Asinan Poblacion	Bgy
Asinan Proper	Bgy
Baraca-Camachile (Pob.)	Bgy
Batiawan	Bgy
Calapacuan	Bgy
Calapandayan (Pob.)	Bgy
Cawag	Bgy
Ilwas (Pob.)	Bgy
Mangan-Vaca	Bgy
Matain	Bgy
Naugsol	Bgy
Pamatawan	Bgy
San Isidro	Bgy
Santo Tomas	Bgy
Wawandue (Pob.)	Bgy
Aurora	Prov
Baler (Capital)	Mun
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Buhangin	Bgy
Calabuanan	Bgy
Obligacion	Bgy
Pingit	Bgy
Reserva	Bgy
Sabang	Bgy
Suclayin	Bgy
Zabali	Bgy
Casiguran	Mun
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Calabgan	Bgy
Calangcuasan	Bgy
Calantas	Bgy
Culat	Bgy
Dibet	Bgy
Esperanza	Bgy
Lual	Bgy
Marikit	Bgy
Tabas	Bgy
Tinib	Bgy
Bianuan	Bgy
Cozo	Bgy
Dibacong	Bgy
Ditinagyan	Bgy
Esteves	Bgy
San Ildefonso	Bgy
Dilasag	Mun
Diagyan	Bgy
Dicabasan	Bgy
Dilaguidi	Bgy
Dimaseset	Bgy
Diniog	Bgy
Lawang	Bgy
Maligaya (Pob.)	Bgy
Manggitahan	Bgy
Masagana (Pob.)	Bgy
Ura	Bgy
Esperanza	Bgy
Dinalungan	Mun
Abuleg	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Nipoo	Bgy
Dibaraybay	Bgy
Ditawini	Bgy
Mapalad	Bgy
Paleg	Bgy
Simbahan	Bgy
Dingalan	Mun
Aplaya	Bgy
Butas Na Bato	Bgy
Cabog	Bgy
Caragsacan	Bgy
Davildavilan	Bgy
Dikapanikian	Bgy
Ibona	Bgy
Paltic	Bgy
Poblacion	Bgy
Tanawan	Bgy
Umiray	Bgy
Dipaculao	Mun
Bayabas	Bgy
Buenavista	Bgy
Borlongan	Bgy
Calaocan	Bgy
Dianed	Bgy
Diarabasin	Bgy
Dibutunan	Bgy
Dimabuno	Bgy
Dinadiawan	Bgy
Ditale	Bgy
Gupa	Bgy
Ipil	Bgy
Laboy	Bgy
Lipit	Bgy
Lobbot	Bgy
Maligaya	Bgy
Mijares	Bgy
Mucdol	Bgy
North Poblacion	Bgy
Puangi	Bgy
Salay	Bgy
Sapangkawayan	Bgy
South Poblacion	Bgy
Toytoyan	Bgy
Diamanen	Bgy
Maria Aurora	Mun
Alcala	Bgy
Bagtu	Bgy
Bangco	Bgy
Bannawag	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Baubo	Bgy
Bayanihan	Bgy
Bazal	Bgy
Cabituculan East	Bgy
Cabituculan West	Bgy
Debucao	Bgy
Decoliat	Bgy
Detailen	Bgy
Diaat	Bgy
Dialatman	Bgy
Diaman	Bgy
Dianawan	Bgy
Dikildit	Bgy
Dimanpudso	Bgy
Diome	Bgy
Estonilo	Bgy
Florida	Bgy
Galintuja	Bgy
Cadayacan	Bgy
Malasin	Bgy
Ponglo	Bgy
Quirino	Bgy
Ramada	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Leonardo	Bgy
Santa Lucia	Bgy
Santo Tomas	Bgy
Suguit	Bgy
Villa Aurora	Bgy
Wenceslao	Bgy
San Juan	Bgy
San Luis	Mun
Bacong	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Dibalo	Bgy
Dibayabay	Bgy
Dibut	Bgy
Dikapinisan	Bgy
Dimanayat	Bgy
Diteki	Bgy
Ditumabo	Bgy
L. Pimentel	Bgy
Nonong Senior	Bgy
Real	Bgy
San Isidro	Bgy
San Jose	Bgy
Zarah	Bgy
Region IV-A (CALABARZON)	Reg
Batangas	Prov
Agoncillo	Mun
Adia	Bgy
Bagong Sikat	Bgy
Balangon	Bgy
Bilibinwang	Bgy
Bangin	Bgy
Barigon	Bgy
Coral Na Munti	Bgy
Guitna	Bgy
Mabini	Bgy
Pamiga	Bgy
Panhulan	Bgy
Pansipit	Bgy
Poblacion	Bgy
Pook	Bgy
San Jacinto	Bgy
San Teodoro	Bgy
Santa Cruz	Bgy
Santo Tomas	Bgy
Subic Ibaba	Bgy
Subic Ilaya	Bgy
Banyaga	Bgy
Alitagtag	Mun
Balagbag	Bgy
Concepcion	Bgy
Concordia	Bgy
Dalipit East	Bgy
Dalipit West	Bgy
Dominador East	Bgy
Dominador West	Bgy
Munlawin Sur	Bgy
Munlawin Norte	Bgy
Muzon Primero	Bgy
Muzon Segundo	Bgy
Pinagkurusan	Bgy
Ping-As	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
San Jose	Bgy
Santa Cruz	Bgy
Tadlac	Bgy
San Juan	Bgy
Balayan	Mun
Baclaran	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Calan	Bgy
Caloocan	Bgy
Calzada	Bgy
Canda	Bgy
Carenahan	Bgy
Caybunga	Bgy
Cayponce	Bgy
Dalig	Bgy
Dao	Bgy
Dilao	Bgy
Duhatan	Bgy
Durungao	Bgy
Gimalas	Bgy
Gumamela	Bgy
Lagnas	Bgy
Lanatan	Bgy
Langgangan	Bgy
Lucban Putol	Bgy
Lucban Pook	Bgy
Magabe	Bgy
Malalay	Bgy
Munting Tubig	Bgy
Navotas	Bgy
Patugo	Bgy
Palikpikan	Bgy
Pooc	Bgy
Sambat	Bgy
Sampaga	Bgy
San Juan	Bgy
San Piro	Bgy
Santol	Bgy
Sukol	Bgy
Tactac	Bgy
Taludtud	Bgy
Tanggoy	Bgy
Balete	Mun
Alangilan	Bgy
Calawit	Bgy
Looc	Bgy
Magapi	Bgy
Makina	Bgy
Malabanan	Bgy
Paligawan	Bgy
Palsara	Bgy
Poblacion	Bgy
Sala	Bgy
Sampalocan	Bgy
Solis	Bgy
San Sebastian	Bgy
Batangas City (Capital)	City
Alangilan	Bgy
Balagtas	Bgy
Balete	Bgy
Banaba Center	Bgy
Banaba Kanluran	Bgy
Banaba Silangan	Bgy
Banaba Ibaba	Bgy
Bilogo	Bgy
Maapas	Bgy
Bolbok	Bgy
Bukal	Bgy
Calicanto	Bgy
Catandala	Bgy
Concepcion	Bgy
Conde Itaas	Bgy
Conde Labak	Bgy
Cuta	Bgy
Dalig	Bgy
Dela Paz	Bgy
Dela Paz Pulot Aplaya	Bgy
Dela Paz Pulot Itaas	Bgy
Domoclay	Bgy
Dumantay	Bgy
Gulod Itaas	Bgy
Gulod Labak	Bgy
Haligue Kanluran	Bgy
Haligue Silangan	Bgy
Ilihan	Bgy
Kumba	Bgy
Kumintang Ibaba	Bgy
Kumintang Ilaya	Bgy
Libjo	Bgy
Liponpon, Isla Verde	Bgy
Mahabang Dahilig	Bgy
Mahabang Parang	Bgy
Mahacot Silangan	Bgy
Mahacot Kanluran	Bgy
Malalim	Bgy
Malibayo	Bgy
Malitam	Bgy
Maruclap	Bgy
Mabacong	Bgy
Pagkilatan	Bgy
Paharang Kanluran	Bgy
Paharang Silangan	Bgy
Pallocan Silangan	Bgy
Pallocan Kanluran	Bgy
Pinamucan	Bgy
Pinamucan Ibaba	Bgy
Pinamucan Silangan	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 15 (Pob.)	Bgy
Barangay 16 (Pob.)	Bgy
Barangay 17 (Pob.)	Bgy
Barangay 18 (Pob.)	Bgy
Barangay 19 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 20 (Pob.)	Bgy
Barangay 21 (Pob.)	Bgy
Barangay 22 (Pob.)	Bgy
Barangay 23 (Pob.)	Bgy
Barangay 24 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Sampaga	Bgy
San Agapito, Isla Verde	Bgy
San Agustin Kanluran, Isla Verde	Bgy
San Agustin Silangan, Isla Verde	Bgy
San Andres, Isla Verde	Bgy
San Antonio, Isla Verde	Bgy
San Isidro	Bgy
San Jose Sico	Bgy
San Miguel	Bgy
San Pedro	Bgy
Santa Clara	Bgy
Santa Rita Aplaya	Bgy
Santa Rita Karsada	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Simlong	Bgy
Sirang Lupa	Bgy
Sorosoro Ibaba	Bgy
Sorosoro Ilaya	Bgy
Sorosoro Karsada	Bgy
Tabangao Aplaya	Bgy
Tabangao Ambulong	Bgy
Tabangao Dao	Bgy
Talahib Pandayan	Bgy
Talahib Payapa	Bgy
Talumpok Kanluran	Bgy
Talumpok Silangan	Bgy
Tinga Itaas	Bgy
Tinga Labak	Bgy
Tulo	Bgy
Wawa	Bgy
Bauan	Mun
Alagao	Bgy
Aplaya	Bgy
As-Is	Bgy
Bagong Silang	Bgy
Baguilawa	Bgy
Balayong	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Bolo	Bgy
Colvo	Bgy
Cupang	Bgy
Durungao	Bgy
Gulibay	Bgy
Inicbulan	Bgy
Locloc	Bgy
Magalang-Galang	Bgy
Malindig	Bgy
Manalupong	Bgy
Manghinao Proper	Bgy
Manghinao Uno	Bgy
New Danglayan	Bgy
Orense	Bgy
Pitugo	Bgy
Rizal	Bgy
Sampaguita	Bgy
San Agustin	Bgy
San Andres Proper	Bgy
San Andres Uno	Bgy
San Diego	Bgy
San Miguel	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Roque	Bgy
San Teodoro	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Santo Domingo	Bgy
Sinala	Bgy
Calaca	Mun
Bagong Tubig	Bgy
Baclas	Bgy
Balimbing	Bgy
Bambang	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Bisaya	Bgy
Cahil	Bgy
Caluangan	Bgy
Calantas	Bgy
Camastilisan	Bgy
Coral Ni Lopez	Bgy
Coral Ni Bacal	Bgy
Dacanlao	Bgy
Dila	Bgy
Loma	Bgy
Lumbang Calzada	Bgy
Lumbang Na Bata	Bgy
Lumbang Na Matanda	Bgy
Madalunot	Bgy
Makina	Bgy
Matipok	Bgy
Munting Coral	Bgy
Niyugan	Bgy
Pantay	Bgy
Puting Bato West	Bgy
Puting Kahoy	Bgy
Puting Bato East	Bgy
Quisumbing	Bgy
Salong	Bgy
San Rafael	Bgy
Sinisian	Bgy
Taklang Anak	Bgy
Talisay	Bgy
Tamayo	Bgy
Timbain	Bgy
Calatagan	Mun
Bagong Silang	Bgy
Baha	Bgy
Balibago	Bgy
Balitoc	Bgy
Biga	Bgy
Bucal	Bgy
Carlosa	Bgy
Carretunan	Bgy
Encarnacion	Bgy
Gulod	Bgy
Hukay	Bgy
Lucsuhin	Bgy
Luya	Bgy
Paraiso	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Quilitisan	Bgy
Real	Bgy
Sambungan	Bgy
Santa Ana	Bgy
Talibayog	Bgy
Talisay	Bgy
Tanagan	Bgy
Cuenca	Mun
Balagbag	Bgy
Bungahan	Bgy
Calumayin	Bgy
Dalipit East	Bgy
Dalipit West	Bgy
Dita	Bgy
Don Juan	Bgy
Emmanuel	Bgy
Ibabao	Bgy
Labac	Bgy
Pinagkaisahan	Bgy
San Felipe	Bgy
San Isidro	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Ibaan	Mun
Bago	Bgy
Balanga	Bgy
Bungahan	Bgy
Calamias	Bgy
Catandala	Bgy
Coliat	Bgy
Dayapan	Bgy
Lapu-lapu	Bgy
Lucsuhin	Bgy
Mabalor	Bgy
Malainin	Bgy
Matala	Bgy
Munting-Tubig	Bgy
Palindan	Bgy
Pangao	Bgy
Panghayaan	Bgy
Poblacion	Bgy
Quilo	Bgy
Sabang	Bgy
Salaban I	Bgy
San Agustin	Bgy
Sandalan	Bgy
Santo Niño	Bgy
Talaibon	Bgy
Tulay Na Patpat	Bgy
Salaban II	Bgy
Laurel	Mun
As-Is	Bgy
Balakilong	Bgy
Berinayan	Bgy
Bugaan East	Bgy
Bugaan West	Bgy
Buso-buso	Bgy
Dayap Itaas	Bgy
Gulod	Bgy
J. Leviste	Bgy
Molinete	Bgy
Niyugan	Bgy
Paliparan	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
San Gabriel	Bgy
San Gregorio	Bgy
Santa Maria	Bgy
Ticub	Bgy
Lemery	Mun
Anak-Dagat	Bgy
Arumahan	Bgy
Ayao-iyao	Bgy
Bagong Pook	Bgy
Bagong Sikat	Bgy
Balanga	Bgy
Bukal	Bgy
Cahilan I	Bgy
Cahilan II	Bgy
Dayapan	Bgy
Dita	Bgy
Gulod	Bgy
Lucky	Bgy
Maguihan	Bgy
Mahabang Dahilig	Bgy
Mahayahay	Bgy
Maigsing Dahilig	Bgy
Maligaya	Bgy
Malinis	Bgy
Masalisi	Bgy
Mataas Na Bayan	Bgy
Matingain I	Bgy
Matingain II	Bgy
Mayasang	Bgy
Niugan	Bgy
Nonong Casto	Bgy
Palanas	Bgy
Payapa Ibaba	Bgy
Payapa Ilaya	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
District IV (Pob.)	Bgy
Rizal	Bgy
Sambal Ibaba	Bgy
Sambal Ilaya	Bgy
San Isidro Ibaba	Bgy
San Isidro Itaas	Bgy
Sangalang	Bgy
Talaga	Bgy
Tubigan	Bgy
Tubuan	Bgy
Wawa Ibaba	Bgy
Wawa Ilaya	Bgy
Sinisian East	Bgy
Sinisian West	Bgy
Lian	Mun
Bagong Pook	Bgy
Balibago	Bgy
Binubusan	Bgy
Bungahan	Bgy
Cumba	Bgy
Humayingan	Bgy
Kapito	Bgy
Lumaniag	Bgy
Luyahan	Bgy
Malaruhatan	Bgy
Matabungkay	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Prenza	Bgy
Puting-Kahoy	Bgy
San Diego	Bgy
City of Lipa	City
Adya	Bgy
Anilao	Bgy
Anilao-Labac	Bgy
Antipolo Del Norte	Bgy
Antipolo Del Sur	Bgy
Bagong Pook	Bgy
San Sebastian	Bgy
Balintawak	Bgy
Banaybanay	Bgy
Bolbok	Bgy
Bugtong na Pulo	Bgy
Bulacnin	Bgy
Bulaklakan	Bgy
Calamias	Bgy
Cumba	Bgy
Dagatan	Bgy
Duhatan	Bgy
Halang	Bgy
Inosloban	Bgy
Kayumanggi	Bgy
Latag	Bgy
Lodlod	Bgy
Lumbang	Bgy
Mabini	Bgy
Malagonlong	Bgy
Malitlit	Bgy
Marauoy	Bgy
Mataas Na Lupa	Bgy
Munting Pulo	Bgy
Pagolingin Bata	Bgy
Pagolingin East	Bgy
Pagolingin West	Bgy
Pangao	Bgy
Pinagkawitan	Bgy
Pinagtongulan	Bgy
Plaridel	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 10	Bgy
Poblacion Barangay 11	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Poblacion Barangay 5	Bgy
Poblacion Barangay 6	Bgy
Poblacion Barangay 7	Bgy
Poblacion Barangay 8	Bgy
Poblacion Barangay 9	Bgy
Pusil	Bgy
Quezon	Bgy
Rizal	Bgy
Sabang	Bgy
Sampaguita	Bgy
San Benito	Bgy
San Carlos	Bgy
San Celestino	Bgy
San Francisco	Bgy
San Guillermo	Bgy
San Jose	Bgy
San Lucas	Bgy
San Salvador	Bgy
Sapac	Bgy
Sico	Bgy
Santo Niño	Bgy
Santo Toribio	Bgy
Talisay	Bgy
Tambo	Bgy
Tangob	Bgy
Tanguay	Bgy
Tibig	Bgy
Tipacan	Bgy
Poblacion Barangay 9-A	Bgy
Barangay 12 (Pob.)	Bgy
Lobo	Mun
Apar	Bgy
Balatbat	Bgy
Balibago	Bgy
Banalo	Bgy
Biga	Bgy
Bignay	Bgy
Calo	Bgy
Calumpit	Bgy
Fabrica	Bgy
Jaybanga	Bgy
Lagadlarin	Bgy
Mabilog Na Bundok	Bgy
Malabrigo	Bgy
Malalim Na Sanog	Bgy
Malapad Na Parang	Bgy
Masaguitsit	Bgy
Nagtalongtong	Bgy
Nagtoctoc	Bgy
Olo-olo	Bgy
Pinaghawanan	Bgy
San Miguel	Bgy
San Nicolas	Bgy
Sawang	Bgy
Soloc	Bgy
Tayuman	Bgy
Poblacion	Bgy
Mabini	Mun
Anilao Proper	Bgy
Anilao East	Bgy
Bagalangit	Bgy
Bulacan	Bgy
Calamias	Bgy
Estrella	Bgy
Gasang	Bgy
Laurel	Bgy
Ligaya	Bgy
Mainaga	Bgy
Mainit	Bgy
Majuben	Bgy
Malimatoc I	Bgy
Malimatoc II	Bgy
Nag-Iba	Bgy
Pilahan	Bgy
Poblacion	Bgy
Pulang Lupa	Bgy
Pulong Anahao	Bgy
Pulong Balibaguhan	Bgy
Pulong Niogan	Bgy
Saguing	Bgy
Sampaguita	Bgy
San Francisco	Bgy
San Jose	Bgy
San Juan	Bgy
San Teodoro	Bgy
Santa Ana	Bgy
Santa Mesa	Bgy
Santo Niño	Bgy
Santo Tomas	Bgy
Solo	Bgy
Talaga Proper	Bgy
Talaga East	Bgy
Malvar	Mun
Bagong Pook	Bgy
Bilucao	Bgy
Bulihan	Bgy
San Gregorio	Bgy
Luta Del Norte	Bgy
Luta Del Sur	Bgy
Poblacion	Bgy
San Andres	Bgy
San Fernando	Bgy
San Isidro East	Bgy
San Juan	Bgy
San Pedro II	Bgy
San Pedro I	Bgy
San Pioquinto	Bgy
Santiago	Bgy
Mataasnakahoy	Mun
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
District IV (Pob.)	Bgy
Bayorbor	Bgy
Bubuyan	Bgy
Calingatan	Bgy
Kinalaglagan	Bgy
Loob	Bgy
Lumang Lipa	Bgy
Manggahan	Bgy
Nangkaan	Bgy
San Sebastian	Bgy
Santol	Bgy
Upa	Bgy
Barangay II-A (Pob.)	Bgy
Nasugbu	Mun
Aga	Bgy
Balaytigui	Bgy
Banilad	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Bilaran	Bgy
Bucana	Bgy
Bulihan	Bgy
Bunducan	Bgy
Butucan	Bgy
Calayo	Bgy
Catandaan	Bgy
Kaylaway	Bgy
Kayrilaw	Bgy
Cogunan	Bgy
Dayap	Bgy
Latag	Bgy
Looc	Bgy
Lumbangan	Bgy
Malapad Na Bato	Bgy
Mataas Na Pulo	Bgy
Maugat	Bgy
Munting Indan	Bgy
Natipuan	Bgy
Pantalan	Bgy
Papaya	Bgy
Putat	Bgy
Reparo	Bgy
Talangan	Bgy
Tumalim	Bgy
Utod	Bgy
Wawa	Bgy
Barangay 1 (Pob.)	Bgy
Padre Garcia	Mun
Banaba	Bgy
Banaybanay	Bgy
Bawi	Bgy
Bukal	Bgy
Castillo	Bgy
Cawongan	Bgy
Manggas	Bgy
Maugat East	Bgy
Maugat West	Bgy
Pansol	Bgy
Payapa	Bgy
Poblacion	Bgy
Quilo-quilo North	Bgy
Quilo-quilo South	Bgy
San Felipe	Bgy
San Miguel	Bgy
Tamak	Bgy
Tangob	Bgy
Rosario	Mun
Alupay	Bgy
Antipolo	Bgy
Bagong Pook	Bgy
Balibago	Bgy
Bayawang	Bgy
Baybayin	Bgy
Bulihan	Bgy
Cahigam	Bgy
Calantas	Bgy
Colongan	Bgy
Itlugan	Bgy
Lumbangan	Bgy
Maalas-As	Bgy
Mabato	Bgy
Mabunga	Bgy
Macalamcam A	Bgy
Macalamcam B	Bgy
Malaya	Bgy
Maligaya	Bgy
Marilag	Bgy
Masaya	Bgy
Matamis	Bgy
Mavalor	Bgy
Mayuro	Bgy
Namuco	Bgy
Namunga	Bgy
Natu	Bgy
Nasi	Bgy
Palakpak	Bgy
Pinagsibaan	Bgy
Barangay A (Pob.)	Bgy
Barangay B (Pob.)	Bgy
Barangay C (Pob.)	Bgy
Barangay D (Pob.)	Bgy
Barangay E (Pob.)	Bgy
Putingkahoy	Bgy
Quilib	Bgy
Salao	Bgy
San Carlos	Bgy
San Ignacio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Timbugan	Bgy
Tiquiwan	Bgy
Leviste	Bgy
Tulos	Bgy
San Jose	Mun
Aguila	Bgy
Anus	Bgy
Aya	Bgy
Bagong Pook	Bgy
Balagtasin	Bgy
Balagtasin I	Bgy
Banaybanay I	Bgy
Banaybanay II	Bgy
Bigain I	Bgy
Bigain II	Bgy
Calansayan	Bgy
Dagatan	Bgy
Don Luis	Bgy
Galamay-Amo	Bgy
Lalayat	Bgy
Lapolapo I	Bgy
Lapolapo II	Bgy
Lepute	Bgy
Lumil	Bgy
Natunuan	Bgy
Palanca	Bgy
Pinagtung-Ulan	Bgy
Poblacion Barangay I	Bgy
Poblacion Barangay II	Bgy
Poblacion Barangay III	Bgy
Poblacion Barangay IV	Bgy
Sabang	Bgy
Salaban	Bgy
Santo Cristo	Bgy
Mojon-Tampoy	Bgy
Taysan	Bgy
Tugtug	Bgy
Bigain South	Bgy
San Juan	Mun
Abung	Bgy
Balagbag	Bgy
Barualte	Bgy
Bataan	Bgy
Buhay Na Sapa	Bgy
Bulsa	Bgy
Calicanto	Bgy
Calitcalit	Bgy
Calubcub I	Bgy
Calubcub II	Bgy
Catmon	Bgy
Coloconto	Bgy
Escribano	Bgy
Hugom	Bgy
Imelda	Bgy
Janaojanao	Bgy
Laiya-Ibabao	Bgy
Laiya-Aplaya	Bgy
Libato	Bgy
Lipahan	Bgy
Mabalanoy	Bgy
Nagsaulay	Bgy
Maraykit	Bgy
Muzon	Bgy
Palahanan I	Bgy
Palahanan II	Bgy
Palingowak	Bgy
Pinagbayanan	Bgy
Poblacion	Bgy
Poctol	Bgy
Pulangbato	Bgy
Putingbuhangin	Bgy
Quipot	Bgy
Sampiro	Bgy
Sapangan	Bgy
Sico I	Bgy
Sico II	Bgy
Subukin	Bgy
Talahiban I	Bgy
Talahiban II	Bgy
Ticalan	Bgy
Tipaz	Bgy
San Luis	Mun
Abiacao	Bgy
Bagong Tubig	Bgy
Balagtasin	Bgy
Balite	Bgy
Banoyo	Bgy
Boboy	Bgy
Bonliw	Bgy
Calumpang West	Bgy
Calumpang East	Bgy
Dulangan	Bgy
Durungao	Bgy
Locloc	Bgy
Luya	Bgy
Mahabang Parang	Bgy
Manggahan	Bgy
Muzon	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Martin	Bgy
Santa Monica	Bgy
Taliba	Bgy
Talon	Bgy
Tejero	Bgy
Tungal	Bgy
Poblacion	Bgy
San Nicolas	Mun
Abelo	Bgy
Balete	Bgy
Baluk-baluk	Bgy
Bancoro	Bgy
Bangin	Bgy
Calangay	Bgy
Hipit	Bgy
Maabud North	Bgy
Maabud South	Bgy
Munlawin	Bgy
Pansipit	Bgy
Poblacion	Bgy
Santo Niño	Bgy
Sinturisan	Bgy
Tagudtod	Bgy
Talang	Bgy
Alas-as	Bgy
Pulang-Bato	Bgy
San Pascual	Mun
Alalum	Bgy
Antipolo	Bgy
Balimbing	Bgy
Banaba	Bgy
Bayanan	Bgy
Danglayan	Bgy
Del Pilar	Bgy
Gelerang Kawayan	Bgy
Ilat North	Bgy
Ilat South	Bgy
Kaingin	Bgy
Laurel	Bgy
Malaking Pook	Bgy
Mataas Na Lupa	Bgy
Natunuan North	Bgy
Natunuan South	Bgy
Padre Castillo	Bgy
Palsahingin	Bgy
Pila	Bgy
Poblacion	Bgy
Pook Ni Banal	Bgy
Pook Ni Kapitan	Bgy
Resplandor	Bgy
Sambat	Bgy
San Antonio	Bgy
San Mariano	Bgy
San Mateo	Bgy
Santa Elena	Bgy
Santo Niño	Bgy
Santa Teresita	Mun
Antipolo	Bgy
Bihis	Bgy
Burol	Bgy
Calayaan	Bgy
Calumala	Bgy
Cuta East	Bgy
Cutang Cawayan	Bgy
Irukan	Bgy
Pacifico	Bgy
Poblacion I	Bgy
Saimsim	Bgy
Sampa	Bgy
Sinipian	Bgy
Tambo Ibaba	Bgy
Tambo Ilaya	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
City of Sto. Tomas	City
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Bartolome	Bgy
San Felix	Bgy
San Fernando	Bgy
San Francisco	Bgy
San Isidro Norte	Bgy
San Isidro Sur	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Juan	Bgy
San Luis	Bgy
San Miguel	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Ana	Bgy
Santa Anastacia	Bgy
Santa Clara	Bgy
Santa Cruz	Bgy
Santa Elena	Bgy
Santa Maria	Bgy
Santiago	Bgy
Santa Teresita	Bgy
Taal	Mun
Apacay	Bgy
Balisong	Bgy
Bihis	Bgy
Bolbok	Bgy
Buli	Bgy
Butong	Bgy
Carasuche	Bgy
Cawit	Bgy
Caysasay	Bgy
Cubamba	Bgy
Cultihan	Bgy
Gahol	Bgy
Halang	Bgy
Iba	Bgy
Ilog	Bgy
Imamawo	Bgy
Ipil	Bgy
Luntal	Bgy
Mahabang Lodlod	Bgy
Niogan	Bgy
Pansol	Bgy
Poblacion 11	Bgy
Poblacion 1	Bgy
Poblacion 10	Bgy
Poblacion 12	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Poblacion 6	Bgy
Poblacion 7	Bgy
Poblacion 8	Bgy
Poblacion 9	Bgy
Pook	Bgy
Seiran	Bgy
Laguile	Bgy
Latag	Bgy
Tierra Alta	Bgy
Tulo	Bgy
Tatlong Maria	Bgy
Poblacion 13	Bgy
Poblacion 14	Bgy
Talisay	Mun
Aya	Bgy
Balas	Bgy
Banga	Bgy
Buco	Bgy
Caloocan	Bgy
Leynes	Bgy
Miranda	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Poblacion Barangay 5	Bgy
Poblacion Barangay 6	Bgy
Poblacion Barangay 7	Bgy
Poblacion Barangay 8	Bgy
Quiling	Bgy
Sampaloc	Bgy
San Guillermo	Bgy
Santa Maria	Bgy
Tranca	Bgy
Tumaway	Bgy
City of Tanauan	City
Altura Bata	Bgy
Altura Matanda	Bgy
Altura-South	Bgy
Ambulong	Bgy
Banadero	Bgy
Bagbag	Bgy
Bagumbayan	Bgy
Balele	Bgy
Banjo East	Bgy
Banjo Laurel	Bgy
Bilog-bilog	Bgy
Boot	Bgy
Cale	Bgy
Darasa	Bgy
Pagaspas	Bgy
Gonzales	Bgy
Hidalgo	Bgy
Janopol	Bgy
Janopol Oriental	Bgy
Laurel	Bgy
Luyos	Bgy
Mabini	Bgy
Malaking Pulo	Bgy
Maria Paz	Bgy
Maugat	Bgy
Montaña	Bgy
Natatas	Bgy
Pantay Matanda	Bgy
Pantay Bata	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Poblacion Barangay 5	Bgy
Poblacion Barangay 6	Bgy
Poblacion Barangay 7	Bgy
Sala	Bgy
Sambat	Bgy
San Jose	Bgy
Santol	Bgy
Santor	Bgy
Sulpoc	Bgy
Suplang	Bgy
Talaga	Bgy
Tinurik	Bgy
Trapiche	Bgy
Ulango	Bgy
Wawa	Bgy
Taysan	Mun
Bacao	Bgy
Bilogo	Bgy
Bukal	Bgy
Dagatan	Bgy
Guinhawa	Bgy
Laurel	Bgy
Mabayabas	Bgy
Mahanadiong	Bgy
Mapulo	Bgy
Mataas Na Lupa	Bgy
Pag-Asa	Bgy
Panghayaan	Bgy
Piña	Bgy
Pinagbayanan	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
San Isidro	Bgy
San Marcelino	Bgy
Santo Niño	Bgy
Tilambo	Bgy
Tingloy	Mun
Corona	Bgy
Gamao	Bgy
Makawayan	Bgy
Marikaban	Bgy
Papaya	Bgy
Pisa	Bgy
Barangay 13	Bgy
Barangay 14	Bgy
Barangay 15	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Pedro	Bgy
Santo Tomas	Bgy
Talahib	Bgy
Tuy	Mun
Acle	Bgy
Bayudbud	Bgy
Bolboc	Bgy
Dalima	Bgy
Dao	Bgy
Guinhawa	Bgy
Lumbangan	Bgy
Luntal	Bgy
Magahis	Bgy
Malibu	Bgy
Mataywanac	Bgy
Palincaro	Bgy
Luna (Pob.)	Bgy
Burgos (Pob.)	Bgy
Rizal (Pob.)	Bgy
Rillo (Pob.)	Bgy
Putol	Bgy
Sabang	Bgy
San Jose	Bgy
Talon	Bgy
Toong	Bgy
Tuyon-tuyon	Bgy
Cavite	Prov
Alfonso	Mun
Amuyong	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Buck Estate	Bgy
Esperanza Ibaba	Bgy
Kaytitinga I	Bgy
Luksuhin	Bgy
Mangas I	Bgy
Marahan I	Bgy
Matagbak I	Bgy
Pajo	Bgy
Sikat	Bgy
Sinaliw Malaki	Bgy
Sinaliw na Munti	Bgy
Sulsugin	Bgy
Taywanak Ibaba	Bgy
Taywanak Ilaya	Bgy
Upli	Bgy
Kaysuyo	Bgy
Luksuhin Ilaya	Bgy
Palumlum	Bgy
Bilog	Bgy
Esperanza Ilaya	Bgy
Kaytitinga II	Bgy
Kaytitinga III	Bgy
Mangas II	Bgy
Marahan II	Bgy
Matagbak II	Bgy
Santa Teresa	Bgy
Amadeo	Mun
Banaybanay	Bgy
Bucal	Bgy
Dagatan	Bgy
Halang	Bgy
Loma	Bgy
Maitim I	Bgy
Maymangga	Bgy
Minantok Kanluran	Bgy
Pangil	Bgy
Barangay I (Pob.)	Bgy
Barangay X (Pob.)	Bgy
Barangay XI (Pob.)	Bgy
Barangay XII (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Barangay VII (Pob.)	Bgy
Barangay VIII (Pob.)	Bgy
Barangay IX (Pob.)	Bgy
Salaban	Bgy
Talon	Bgy
Tamacan	Bgy
Buho	Bgy
Minantok Silangan	Bgy
City of Bacoor	City
Alima	Bgy
Aniban I	Bgy
Banalo	Bgy
Bayanan	Bgy
Daang Bukid	Bgy
Digman	Bgy
Dulong Bayan	Bgy
Habay I	Bgy
Kaingin (Pob.)	Bgy
Ligas I	Bgy
Mabolo I	Bgy
Maliksi I	Bgy
Mambog I	Bgy
Molino I	Bgy
Niog I	Bgy
P.F. Espiritu I	Bgy
Real I	Bgy
Salinas I	Bgy
San Nicolas I	Bgy
Sineguelasan	Bgy
Tabing Dagat	Bgy
Talaba I	Bgy
Zapote I	Bgy
Queens Row Central	Bgy
Queens Row East	Bgy
Queens Row West	Bgy
Aniban II	Bgy
Aniban III	Bgy
Aniban IV	Bgy
Aniban V	Bgy
Campo Santo	Bgy
Habay II	Bgy
Ligas II	Bgy
Ligas III	Bgy
Mabolo II	Bgy
Mabolo III	Bgy
Maliksi II	Bgy
Maliksi III	Bgy
Mambog II	Bgy
Mambog III	Bgy
Mambog IV	Bgy
Mambog V	Bgy
Molino II	Bgy
Molino III	Bgy
Molino IV	Bgy
Molino V	Bgy
Molino VI	Bgy
Molino VII	Bgy
Niog II	Bgy
Niog III	Bgy
P.F. Espiritu II	Bgy
P.F. Espiritu III	Bgy
P.F. Espiritu IV	Bgy
P.F. Espiritu V	Bgy
P.F. Espiritu VI	Bgy
P.F. Espiritu VII	Bgy
P.F. Espiritu VIII	Bgy
Real II	Bgy
Salinas II	Bgy
Salinas III	Bgy
Salinas IV	Bgy
San Nicolas II	Bgy
San Nicolas III	Bgy
Talaba II	Bgy
Talaba III	Bgy
Talaba IV	Bgy
Talaba V	Bgy
Talaba VI	Bgy
Talaba VII	Bgy
Zapote II	Bgy
Zapote III	Bgy
Zapote IV	Bgy
Zapote V	Bgy
Carmona	Mun
Bancal	Bgy
Cabilang Baybay	Bgy
Lantic	Bgy
Mabuhay	Bgy
Maduya	Bgy
Milagrosa	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
City of Cavite	City
Barangay 1	Bgy
Barangay 10	Bgy
Barangay 2	Bgy
Barangay 11	Bgy
Barangay 12	Bgy
Barangay 13	Bgy
Barangay 14	Bgy
Barangay 15	Bgy
Barangay 16	Bgy
Barangay 17	Bgy
Barangay 18	Bgy
Barangay 19	Bgy
Barangay 20	Bgy
Barangay 21	Bgy
Barangay 22	Bgy
Barangay 23	Bgy
Barangay 24	Bgy
Barangay 25	Bgy
Barangay 26	Bgy
Barangay 27	Bgy
Barangay 28	Bgy
Barangay 29	Bgy
Barangay 3	Bgy
Barangay 30	Bgy
Barangay 31	Bgy
Barangay 32	Bgy
Barangay 33	Bgy
Barangay 34	Bgy
Barangay 35	Bgy
Barangay 36	Bgy
Barangay 37	Bgy
Barangay 38	Bgy
Barangay 39	Bgy
Barangay 4	Bgy
Barangay 40	Bgy
Barangay 41	Bgy
Barangay 42	Bgy
Barangay 43	Bgy
Barangay 44	Bgy
Barangay 45	Bgy
Barangay 46	Bgy
Barangay 47	Bgy
Barangay 48	Bgy
Barangay 49	Bgy
Barangay 5	Bgy
Barangay 50	Bgy
Barangay 51	Bgy
Barangay 52	Bgy
Barangay 53	Bgy
Barangay 54	Bgy
Barangay 55	Bgy
Barangay 56	Bgy
Barangay 57	Bgy
Barangay 58	Bgy
Barangay 59	Bgy
Barangay 6	Bgy
Barangay 60	Bgy
Barangay 61	Bgy
Barangay 62	Bgy
Barangay 7	Bgy
Barangay 8	Bgy
Barangay 9	Bgy
Barangay 10-A	Bgy
Barangay 10-B	Bgy
Barangay 22-A	Bgy
Barangay 29-A	Bgy
Barangay 36-A	Bgy
Barangay 37-A	Bgy
Barangay 38-A	Bgy
Barangay 42-A	Bgy
Barangay 42-B	Bgy
Barangay 42-C	Bgy
Barangay 45-A	Bgy
Barangay 47-A	Bgy
Barangay 47-B	Bgy
Barangay 48-A	Bgy
Barangay 49-A	Bgy
Barangay 53-A	Bgy
Barangay 53-B	Bgy
Barangay 54-A	Bgy
Barangay 58-A	Bgy
Barangay 61-A	Bgy
Barangay 62-A	Bgy
Barangay 62-B	Bgy
City of Dasmariñas	City
Burol	Bgy
Langkaan I	Bgy
Paliparan I	Bgy
Sabang	Bgy
Salawag	Bgy
Salitran I	Bgy
Sampaloc I	Bgy
San Agustin I	Bgy
San Jose	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Datu Esmael	Bgy
Emmanuel Bergado I	Bgy
Fatima I	Bgy
Luzviminda I	Bgy
Saint Peter I	Bgy
San Andres I	Bgy
San Antonio De Padua I	Bgy
San Dionisio	Bgy
San Esteban	Bgy
San Francisco I	Bgy
San Isidro Labrador I	Bgy
San Juan	Bgy
San Lorenzo Ruiz I	Bgy
San Luis I	Bgy
San Manuel I	Bgy
San Mateo	Bgy
San Miguel	Bgy
San Nicolas I	Bgy
San Roque	Bgy
San Simon	Bgy
Santa Cristina I	Bgy
Santa Cruz I	Bgy
Santa Fe	Bgy
Santa Lucia	Bgy
Santa Maria	Bgy
Santo Cristo	Bgy
Santo Niño I	Bgy
Burol I	Bgy
Burol II	Bgy
Burol III	Bgy
Emmanuel Bergado II	Bgy
Fatima II	Bgy
Fatima III	Bgy
Langkaan II	Bgy
Luzviminda II	Bgy
Paliparan II	Bgy
Paliparan III	Bgy
Saint Peter II	Bgy
Salitran II	Bgy
Salitran III	Bgy
Salitran IV	Bgy
Sampaloc II	Bgy
Sampaloc III	Bgy
Sampaloc IV	Bgy
Sampaloc V	Bgy
San Agustin II	Bgy
San Agustin III	Bgy
San Andres II	Bgy
San Antonio De Padua II	Bgy
San Francisco II	Bgy
San Isidro Labrador II	Bgy
San Lorenzo Ruiz II	Bgy
San Luis II	Bgy
San Manuel II	Bgy
San Miguel II	Bgy
San Nicolas II	Bgy
Santa Cristina II	Bgy
Santa Cruz II	Bgy
Santo Niño II	Bgy
Zone I-B	Bgy
H-2	Bgy
Victoria Reyes	Bgy
General Emilio Aguinaldo	Mun
A. Dalusag	Bgy
Batas Dao	Bgy
Castaños Cerca	Bgy
Castaños Lejos	Bgy
Kabulusan	Bgy
Kaymisas	Bgy
Kaypaaba	Bgy
Lumipa	Bgy
Narvaez	Bgy
Poblacion I	Bgy
Tabora	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
City of General Trias	City
Alingaro	Bgy
Bacao I	Bgy
Gov. Ferrer Pob.	Bgy
Sampalucan Pob.	Bgy
Dulong Bayan Pob.	Bgy
San Gabriel Pob.	Bgy
Bagumbayan Pob.	Bgy
Vibora Pob.	Bgy
Ninety Sixth Pob.	Bgy
Prinza Pob.	Bgy
Biclatan	Bgy
Buenavista I	Bgy
Corregidor Pob.	Bgy
Javalera	Bgy
Manggahan	Bgy
Navarro	Bgy
Panungyanan	Bgy
Pasong Camachile I	Bgy
Pasong Kawayan I	Bgy
Pasong Kawayan II	Bgy
Pinagtipunan	Bgy
San Francisco	Bgy
San Juan I	Bgy
Santa Clara	Bgy
Santiago	Bgy
Tapia	Bgy
Tejero	Bgy
Arnaldo Pob.	Bgy
Bacao II	Bgy
Buenavista II	Bgy
Buenavista III	Bgy
Pasong Camachile II	Bgy
San Juan II	Bgy
City of Imus	City
Alapan I-A	Bgy
Alapan II-A	Bgy
Anabu I-A	Bgy
Anabu II-A	Bgy
Poblacion I-A (Pob.)	Bgy
Poblacion II-A (Pob.)	Bgy
Poblacion III-A (Pob.)	Bgy
Poblacion IV-A (Pob.)	Bgy
Bayan Luma I	Bgy
Bucandala I	Bgy
Carsadang Bago I	Bgy
Malagasang I-A	Bgy
Malagasang II-A	Bgy
Medicion I-A	Bgy
Medicion II-A	Bgy
Pag-Asa I	Bgy
Palico I	Bgy
Pasong Buaya I	Bgy
Tanzang Luma I	Bgy
Toclong I-A	Bgy
Toclong II-A	Bgy
Alapan I-B	Bgy
Alapan I-C	Bgy
Alapan II-B	Bgy
Anabu I-B	Bgy
Anabu I-C	Bgy
Anabu I-D	Bgy
Anabu I-E	Bgy
Anabu I-F	Bgy
Anabu I-G	Bgy
Anabu II-B	Bgy
Anabu II-C	Bgy
Anabu II-D	Bgy
Anabu II-E	Bgy
Anabu II-F	Bgy
Bagong Silang	Bgy
Bayan Luma II	Bgy
Bayan Luma III	Bgy
Bayan Luma IV	Bgy
Bayan Luma V	Bgy
Bayan Luma VI	Bgy
Bayan Luma VII	Bgy
Bayan Luma VIII	Bgy
Bayan Luma IX	Bgy
Bucandala II	Bgy
Bucandala III	Bgy
Bucandala IV	Bgy
Bucandala V	Bgy
Buhay na Tubig	Bgy
Carsadang Bago II	Bgy
Magdalo	Bgy
Maharlika	Bgy
Malagasang I-B	Bgy
Malagasang I-C	Bgy
Malagasang I-D	Bgy
Malagasang I-E	Bgy
Malagasang I-F	Bgy
Malagasang I-G	Bgy
Malagasang II-B	Bgy
Malagasang II-C	Bgy
Malagasang II-D	Bgy
Malagasang II-E	Bgy
Malagasang II-F	Bgy
Malagasang II-G	Bgy
Mariano Espeleta I	Bgy
Mariano Espeleta II	Bgy
Mariano Espeleta III	Bgy
Medicion I-B	Bgy
Medicion I-C	Bgy
Medicion I-D	Bgy
Medicion II-B	Bgy
Medicion II-C	Bgy
Medicion II-D	Bgy
Medicion II-E	Bgy
Medicion II-F	Bgy
Pag-Asa II	Bgy
Pag-Asa III	Bgy
Palico II	Bgy
Palico III	Bgy
Palico IV	Bgy
Pasong Buaya II	Bgy
Pinagbuklod	Bgy
Poblacion I-B	Bgy
Poblacion I-C	Bgy
Poblacion II-B	Bgy
Poblacion III-B	Bgy
Poblacion IV-B	Bgy
Poblacion IV-C	Bgy
Poblacion IV-D	Bgy
Tanzang Luma II	Bgy
Tanzang Luma III	Bgy
Tanzang Luma IV	Bgy
Tanzang Luma V	Bgy
Tanzang Luma VI	Bgy
Toclong I-B	Bgy
Toclong I-C	Bgy
Toclong II-B	Bgy
Indang	Mun
Agus-us	Bgy
Alulod	Bgy
Banaba Cerca	Bgy
Banaba Lejos	Bgy
Bancod	Bgy
Buna Cerca	Bgy
Buna Lejos I	Bgy
Calumpang Cerca	Bgy
Calumpang Lejos I	Bgy
Carasuchi	Bgy
Kayquit I	Bgy
Daine I	Bgy
Guyam Malaki	Bgy
Guyam Munti	Bgy
Harasan	Bgy
Kaytambog	Bgy
Limbon	Bgy
Lumampong Balagbag	Bgy
Lumampong Halayhay	Bgy
Mahabangkahoy Lejos	Bgy
Mahabangkahoy Cerca	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Pulo	Bgy
Tambo Balagbag	Bgy
Tambo Ilaya	Bgy
Tambo Malaki	Bgy
Tambo Kulit	Bgy
Buna Lejos II	Bgy
Daine II	Bgy
Kayquit II	Bgy
Kayquit III	Bgy
Kaytapos	Bgy
Mataas na Lupa	Bgy
Kawit	Mun
Binakayan-Kanluran	Bgy
Gahak	Bgy
Kaingen	Bgy
Marulas	Bgy
Panamitan	Bgy
Poblacion	Bgy
Magdalo	Bgy
San Sebastian	Bgy
Santa Isabel	Bgy
Tabon I	Bgy
Toclong	Bgy
Wakas I	Bgy
Batong Dalig	Bgy
Balsahan-Bisita	Bgy
Binakayan-Aplaya	Bgy
Congbalay-Legaspi	Bgy
Manggahan-Lawin	Bgy
Pulvorista	Bgy
Samala-Marquez	Bgy
Tabon II	Bgy
Tabon III	Bgy
Tramo-Bantayan	Bgy
Wakas II	Bgy
Magallanes	Mun
Baliwag	Bgy
Bendita I	Bgy
Caluangan	Bgy
Medina	Bgy
Pacheco	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Ramirez	Bgy
Tua	Bgy
Urdaneta	Bgy
Kabulusan	Bgy
Bendita II	Bgy
San Agustin	Bgy
Maragondon	Mun
Caingin Pob.	Bgy
Bucal I	Bgy
Bucal II	Bgy
Bucal III A	Bgy
Bucal IV A	Bgy
San Miguel I A	Bgy
Garita I A	Bgy
Mabato	Bgy
Talipusngo	Bgy
Pantihan I	Bgy
Pantihan II	Bgy
Pantihan III	Bgy
Pantihan IV	Bgy
Patungan	Bgy
Pinagsanhan I A	Bgy
Poblacion I A	Bgy
Poblacion II A	Bgy
Tulay Silangan	Bgy
Layong Mabilog	Bgy
Bucal III B	Bgy
Bucal IV B	Bgy
Garita I B	Bgy
Pinagsanhan I B	Bgy
Poblacion I B	Bgy
Poblacion II B	Bgy
San Miguel I B	Bgy
Tulay Kanluran	Bgy
Mendez	Mun
Anuling Lejos I	Bgy
Asis I	Bgy
Galicia I	Bgy
Palocpoc I	Bgy
Panungyan I	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
Poblacion VI	Bgy
Poblacion VII	Bgy
Anuling Cerca I	Bgy
Anuling Cerca II	Bgy
Anuling Lejos II	Bgy
Asis II	Bgy
Asis III	Bgy
Banayad	Bgy
Bukal	Bgy
Galicia II	Bgy
Galicia III	Bgy
Miguel Mojica	Bgy
Palocpoc II	Bgy
Panungyan II	Bgy
Naic	Mun
Bagong Karsada	Bgy
Balsahan	Bgy
Bancaan	Bgy
Bucana Malaki	Bgy
Bucana Sasahan	Bgy
Capt. C. Nazareno (Pob.)	Bgy
Calubcob	Bgy
Palangue 2 & 3	Bgy
Gomez-Zamora (Pob.)	Bgy
Halang	Bgy
Humbac	Bgy
Ibayo Estacion	Bgy
Ibayo Silangan	Bgy
Kanluran	Bgy
Labac	Bgy
Latoria	Bgy
Mabolo	Bgy
Makina	Bgy
Malainen Bago	Bgy
Malainen Luma	Bgy
Molino	Bgy
Munting Mapino	Bgy
Muzon	Bgy
Palangue 1	Bgy
Sabang	Bgy
San Roque	Bgy
Santulan	Bgy
Sapa	Bgy
Timalan Balsahan	Bgy
Timalan Concepcion	Bgy
Noveleta	Mun
Magdiwang	Bgy
Poblacion	Bgy
Salcedo I	Bgy
San Antonio I	Bgy
San Juan I	Bgy
San Rafael I	Bgy
San Rafael II	Bgy
San Jose I	Bgy
Santa Rosa I	Bgy
Salcedo II	Bgy
San Antonio II	Bgy
San Jose II	Bgy
San Juan II	Bgy
San Rafael III	Bgy
San Rafael IV	Bgy
Santa Rosa II	Bgy
Rosario	Mun
Silangan I	Bgy
Bagbag I	Bgy
Kanluran	Bgy
Ligtong I	Bgy
Ligtong II	Bgy
Muzon I	Bgy
Poblacion	Bgy
Sapa I	Bgy
Tejeros Convention	Bgy
Wawa I	Bgy
Ligtong III	Bgy
Bagbag II	Bgy
Ligtong IV	Bgy
Muzon II	Bgy
Sapa II	Bgy
Sapa III	Bgy
Sapa IV	Bgy
Silangan II	Bgy
Wawa II	Bgy
Wawa III	Bgy
Silang	Mun
Adlas	Bgy
Balite I	Bgy
Balite II	Bgy
Balubad	Bgy
Batas	Bgy
Biga I	Bgy
Biluso	Bgy
Buho	Bgy
Bucal	Bgy
Bulihan	Bgy
Cabangaan	Bgy
Carmen	Bgy
Hukay	Bgy
Iba	Bgy
Inchican	Bgy
Kalubkob	Bgy
Kaong	Bgy
Lalaan I	Bgy
Lalaan II	Bgy
Litlit	Bgy
Lucsuhin	Bgy
Lumil	Bgy
Maguyam	Bgy
Malabag	Bgy
Mataas Na Burol	Bgy
Munting Ilog	Bgy
Paligawan	Bgy
Pasong Langka	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Pooc I	Bgy
Pulong Bunga	Bgy
Pulong Saging	Bgy
Puting Kahoy	Bgy
Sabutan	Bgy
San Miguel I	Bgy
San Vicente I	Bgy
Santol	Bgy
Tartaria	Bgy
Tibig	Bgy
Tubuan I	Bgy
Ulat	Bgy
Acacia	Bgy
Anahaw I	Bgy
Ipil I	Bgy
Narra I	Bgy
Yakal	Bgy
Anahaw II	Bgy
Banaba	Bgy
Biga II	Bgy
Hoyo	Bgy
Ipil II	Bgy
Malaking Tatyao	Bgy
Narra II	Bgy
Narra III	Bgy
Pooc II	Bgy
San Miguel II	Bgy
San Vicente II	Bgy
Toledo	Bgy
Tubuan II	Bgy
Tubuan III	Bgy
City of Tagaytay	City
Asisan	Bgy
Bagong Tubig	Bgy
Dapdap West	Bgy
Francisco	Bgy
Guinhawa South	Bgy
Iruhin West	Bgy
Calabuso	Bgy
Kaybagal South (Pob.)	Bgy
Mag-Asawang Ilat	Bgy
Maharlika West	Bgy
Maitim 2nd East	Bgy
Mendez Crossing West	Bgy
Neogan	Bgy
Patutong Malaki South	Bgy
Sambong	Bgy
San Jose	Bgy
Silang Junction South	Bgy
Sungay South	Bgy
Tolentino West	Bgy
Zambal	Bgy
Iruhin East	Bgy
Kaybagal North	Bgy
Maitim 2nd West	Bgy
Dapdap East	Bgy
Guinhawa North	Bgy
Iruhin South	Bgy
Kaybagal East	Bgy
Maharlika East	Bgy
Maitim 2nd Central	Bgy
Mendez Crossing East	Bgy
Patutong Malaki North	Bgy
Silang Junction North	Bgy
Sungay North	Bgy
Tolentino East	Bgy
Tanza	Mun
Amaya I	Bgy
Bagtas	Bgy
Biga	Bgy
Biwas	Bgy
Bucal	Bgy
Bunga	Bgy
Calibuyo	Bgy
Capipisa	Bgy
Daang Amaya I	Bgy
Halayhay	Bgy
Julugan I	Bgy
Mulawin	Bgy
Paradahan I	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Punta I	Bgy
Sahud Ulan	Bgy
Sanja Mayor	Bgy
Santol	Bgy
Tanauan	Bgy
Tres Cruses	Bgy
Lambingan	Bgy
Amaya II	Bgy
Amaya III	Bgy
Amaya IV	Bgy
Amaya V	Bgy
Amaya VI	Bgy
Amaya VII	Bgy
Daang Amaya II	Bgy
Daang Amaya III	Bgy
Julugan II	Bgy
Julugan III	Bgy
Julugan IV	Bgy
Julugan V	Bgy
Julugan VI	Bgy
Julugan VII	Bgy
Julugan VIII	Bgy
Paradahan II	Bgy
Punta II	Bgy
Ternate	Mun
Poblacion I	Bgy
Poblacion II	Bgy
Bucana	Bgy
Poblacion III	Bgy
San Jose	Bgy
San Juan I	Bgy
Sapang I	Bgy
Poblacion I A	Bgy
San Juan II	Bgy
Sapang II	Bgy
City of Trece Martires (Capital)	City
Cabezas	Bgy
Cabuco	Bgy
De Ocampo	Bgy
Lallana	Bgy
San Agustin (Pob.)	Bgy
Osorio	Bgy
Conchu	Bgy
Perez	Bgy
Aguado	Bgy
Gregorio	Bgy
Inocencio	Bgy
Lapidario	Bgy
Luciano	Bgy
Gen. Mariano Alvarez	Mun
Aldiano Olaes	Bgy
Barangay 1 Poblacion	Bgy
Barangay 2 Poblacion	Bgy
Barangay 3 Poblacion	Bgy
Barangay 4 Poblacion	Bgy
Barangay 5 Poblacion	Bgy
Benjamin Tirona	Bgy
Bernardo Pulido	Bgy
Epifanio Malia	Bgy
Francisco De Castro	Bgy
Francisco Reyes	Bgy
Fiorello Calimag	Bgy
Gavino Maderan	Bgy
Gregoria De Jesus	Bgy
Inocencio Salud	Bgy
Jacinto Lumbreras	Bgy
Kapitan Kua	Bgy
Koronel Jose P. Elises	Bgy
Macario Dacon	Bgy
Marcelino Memije	Bgy
Nicolasa Virata	Bgy
Pantaleon Granados	Bgy
Ramon Cruz	Bgy
San Gabriel	Bgy
San Jose	Bgy
Severino De Las Alas	Bgy
Tiniente Tiago	Bgy
Laguna	Prov
Alaminos	Mun
Del Carmen	Bgy
Palma	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
San Agustin	Bgy
San Andres	Bgy
San Benito	Bgy
San Gregorio	Bgy
San Ildefonso	Bgy
San Juan	Bgy
San Miguel	Bgy
San Roque	Bgy
Santa Rosa	Bgy
Bay	Mun
Bitin	Bgy
Calo	Bgy
Dila	Bgy
Maitim	Bgy
Masaya	Bgy
Paciano Rizal	Bgy
Puypuy	Bgy
San Antonio	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Santo Domingo	Bgy
Tagumpay	Bgy
Tranca	Bgy
San Agustin (Pob.)	Bgy
San Nicolas (Pob.)	Bgy
City of Biñan	City
Biñan	Bgy
Bungahan	Bgy
Santo Tomas	Bgy
Canlalay	Bgy
Casile	Bgy
De La Paz	Bgy
Ganado	Bgy
San Francisco	Bgy
Langkiwa	Bgy
Loma	Bgy
Malaban	Bgy
Malamig	Bgy
Mampalasan	Bgy
Platero	Bgy
Poblacion	Bgy
Santo Niño	Bgy
San Antonio	Bgy
San Jose	Bgy
San Vicente	Bgy
Soro-soro	Bgy
Santo Domingo	Bgy
Timbao	Bgy
Tubigan	Bgy
Zapote	Bgy
City of Cabuyao	City
Baclaran	Bgy
Banaybanay	Bgy
Banlic	Bgy
Butong	Bgy
Bigaa	Bgy
Casile	Bgy
Gulod	Bgy
Mamatid	Bgy
Marinig	Bgy
Niugan	Bgy
Pittland	Bgy
Pulo	Bgy
Sala	Bgy
San Isidro	Bgy
Diezmo	Bgy
Barangay Uno (Pob.)	Bgy
Barangay Dos (Pob.)	Bgy
Barangay Tres (Pob.)	Bgy
City of Calamba	City
Bagong Kalsada	Bgy
Banadero	Bgy
Banlic	Bgy
Barandal	Bgy
Bubuyan	Bgy
Bucal	Bgy
Bunggo	Bgy
Burol	Bgy
Camaligan	Bgy
Canlubang	Bgy
Halang	Bgy
Hornalan	Bgy
Kay-Anlog	Bgy
Laguerta	Bgy
La Mesa	Bgy
Lawa	Bgy
Lecheria	Bgy
Lingga	Bgy
Looc	Bgy
Mabato	Bgy
Makiling	Bgy
Mapagong	Bgy
Masili	Bgy
Maunong	Bgy
Mayapa	Bgy
Paciano Rizal	Bgy
Palingon	Bgy
Palo-Alto	Bgy
Pansol	Bgy
Parian	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Prinza	Bgy
Punta	Bgy
Puting Lupa	Bgy
Real	Bgy
Sucol	Bgy
Saimsim	Bgy
Sampiruhan	Bgy
San Cristobal	Bgy
San Jose	Bgy
San Juan	Bgy
Sirang Lupa	Bgy
Milagrosa	Bgy
Turbina	Bgy
Ulango	Bgy
Uwisan	Bgy
Batino	Bgy
Majada Labas	Bgy
Calauan	Mun
Balayhangin	Bgy
Bangyas	Bgy
Dayap	Bgy
Hanggan	Bgy
Imok	Bgy
Lamot 1	Bgy
Lamot 2	Bgy
Limao	Bgy
Mabacan	Bgy
Masiit	Bgy
Paliparan	Bgy
Perez	Bgy
Kanluran (Pob.)	Bgy
Silangan (Pob.)	Bgy
Prinza	Bgy
San Isidro	Bgy
Santo Tomas	Bgy
Cavinti	Mun
Anglas	Bgy
Bangco	Bgy
Bukal	Bgy
Bulajo	Bgy
Cansuso	Bgy
Duhat	Bgy
Inao-Awan	Bgy
Kanluran Talaongan	Bgy
Labayo	Bgy
Layasin	Bgy
Layug	Bgy
Mahipon	Bgy
Paowin	Bgy
Poblacion	Bgy
Sisilmin	Bgy
Silangan Talaongan	Bgy
Sumucab	Bgy
Tibatib	Bgy
Udia	Bgy
Famy	Mun
Asana (Pob.)	Bgy
Bacong-Sigsigan	Bgy
Bagong Pag-Asa (Pob.)	Bgy
Balitoc	Bgy
Banaba (Pob.)	Bgy
Batuhan	Bgy
Bulihan	Bgy
Caballero (Pob.)	Bgy
Calumpang (Pob.)	Bgy
Kapatalan	Bgy
Cuebang Bato	Bgy
Damayan (Pob.)	Bgy
Kataypuanan	Bgy
Liyang	Bgy
Maate	Bgy
Magdalo (Pob.)	Bgy
Mayatba	Bgy
Minayutan	Bgy
Salangbato	Bgy
Tunhac	Bgy
Kalayaan	Mun
Longos	Bgy
San Antonio	Bgy
San Juan (Pob.)	Bgy
Liliw	Mun
Bagong Anyo (Pob.)	Bgy
Bayate	Bgy
Bubukal	Bgy
Bongkol	Bgy
Cabuyao	Bgy
Calumpang	Bgy
Culoy	Bgy
Dagatan	Bgy
Daniw	Bgy
Dita	Bgy
Ibabang Palina	Bgy
Ibabang San Roque	Bgy
Ibabang Sungi	Bgy
Ibabang Taykin	Bgy
Ilayang Palina	Bgy
Ilayang San Roque	Bgy
Ilayang Sungi	Bgy
Ilayang Taykin	Bgy
Kanlurang Bukal	Bgy
Laguan	Bgy
Rizal (Pob.)	Bgy
Luquin	Bgy
Malabo-Kalantukan	Bgy
Masikap (Pob.)	Bgy
Maslun (Pob.)	Bgy
Mojon	Bgy
Novaliches	Bgy
Oples	Bgy
Pag-Asa (Pob.)	Bgy
Palayan	Bgy
San Isidro	Bgy
Silangang Bukal	Bgy
Tuy-Baanan	Bgy
Los Baños	Mun
Anos	Bgy
Bagong Silang	Bgy
Bambang	Bgy
Batong Malake	Bgy
Baybayin (Pob.)	Bgy
Bayog	Bgy
Lalakay	Bgy
Maahas	Bgy
Mayondon	Bgy
Putho Tuntungin	Bgy
San Antonio	Bgy
Tadlak	Bgy
Timugan (Pob.)	Bgy
Malinta	Bgy
Luisiana	Mun
De La Paz	Bgy
Barangay Zone I (Pob.)	Bgy
Barangay Zone II (Pob.)	Bgy
Barangay Zone III (Pob.)	Bgy
Barangay Zone IV (Pob.)	Bgy
Barangay Zone V (Pob.)	Bgy
Barangay Zone VI (Pob.)	Bgy
Barangay Zone VII (Pob.)	Bgy
Barangay Zone VIII (Pob.)	Bgy
San Antonio	Bgy
San Buenaventura	Bgy
San Diego	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Luis	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Salvador	Bgy
Santo Domingo	Bgy
Santo Tomas	Bgy
Lumban	Mun
Bagong Silang	Bgy
Balimbingan (Pob.)	Bgy
Balubad	Bgy
Caliraya	Bgy
Concepcion	Bgy
Lewin	Bgy
Maracta (Pob.)	Bgy
Maytalang I	Bgy
Maytalang II	Bgy
Primera Parang (Pob.)	Bgy
Primera Pulo (Pob.)	Bgy
Salac (Pob.)	Bgy
Segunda Parang (Pob.)	Bgy
Segunda Pulo (Pob.)	Bgy
Santo Niño (Pob.)	Bgy
Wawa	Bgy
Mabitac	Mun
Amuyong	Bgy
Lambac (Pob.)	Bgy
Lucong	Bgy
Matalatala	Bgy
Nanguma	Bgy
Numero	Bgy
Paagahan	Bgy
Bayanihan (Pob.)	Bgy
Libis ng Nayon (Pob.)	Bgy
Maligaya (Pob.)	Bgy
Masikap (Pob.)	Bgy
Pag-Asa (Pob.)	Bgy
Sinagtala (Pob.)	Bgy
San Antonio	Bgy
San Miguel	Bgy
Magdalena	Mun
Alipit	Bgy
Malaking Ambling	Bgy
Munting Ambling	Bgy
Baanan	Bgy
Balanac	Bgy
Bucal	Bgy
Buenavista	Bgy
Bungkol	Bgy
Buo	Bgy
Burlungan	Bgy
Cigaras	Bgy
Ibabang Atingay	Bgy
Ibabang Butnong	Bgy
Ilayang Atingay	Bgy
Ilayang Butnong	Bgy
Ilog	Bgy
Malinao	Bgy
Maravilla	Bgy
Poblacion	Bgy
Sabang	Bgy
Salasad	Bgy
Tanawan	Bgy
Tipunan	Bgy
Halayhayin	Bgy
Majayjay	Mun
Amonoy	Bgy
Bakia	Bgy
Bukal	Bgy
Balanac	Bgy
Balayong	Bgy
Banilad	Bgy
Banti	Bgy
Bitaoy	Bgy
Botocan	Bgy
Burgos	Bgy
Burol	Bgy
Coralao	Bgy
Gagalot	Bgy
Ibabang Banga	Bgy
Ibabang Bayucain	Bgy
Ilayang Banga	Bgy
Ilayang Bayucain	Bgy
Isabang	Bgy
Malinao	Bgy
May-It	Bgy
Munting Kawayan	Bgy
Oobi	Bgy
Olla	Bgy
Origuel (Pob.)	Bgy
Panalaban	Bgy
Panglan	Bgy
Pangil	Bgy
Piit	Bgy
Pook	Bgy
Rizal	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
San Miguel (Pob.)	Bgy
San Roque	Bgy
Santa Catalina (Pob.)	Bgy
Suba	Bgy
Tanawan	Bgy
Taytay	Bgy
Talortor	Bgy
Villa Nogales	Bgy
Nagcarlan	Mun
Abo	Bgy
Alibungbungan	Bgy
Alumbrado	Bgy
Balayong	Bgy
Balimbing	Bgy
Balinacon	Bgy
Bambang	Bgy
Banago	Bgy
Banca-banca	Bgy
Bangcuro	Bgy
Banilad	Bgy
Bayaquitos	Bgy
Buboy	Bgy
Buenavista	Bgy
Buhanginan	Bgy
Bukal	Bgy
Bunga	Bgy
Cabuyew	Bgy
Calumpang	Bgy
Kanluran Kabubuhayan	Bgy
Silangan Kabubuhayan	Bgy
Labangan	Bgy
Lawaguin	Bgy
Kanluran Lazaan	Bgy
Silangan Lazaan	Bgy
Lagulo	Bgy
Maiit	Bgy
Malaya	Bgy
Malinao	Bgy
Manaol	Bgy
Maravilla	Bgy
Nagcalbang	Bgy
Poblacion I (Pob.)	Bgy
Poblacion II (Pob.)	Bgy
Poblacion III (Pob.)	Bgy
Oples	Bgy
Palayan	Bgy
Palina	Bgy
Sabang	Bgy
San Francisco	Bgy
Sibulan	Bgy
Silangan Napapatid	Bgy
Silangan Ilaya	Bgy
Sinipian	Bgy
Santa Lucia	Bgy
Sulsuguin	Bgy
Talahib	Bgy
Talangan	Bgy
Taytay	Bgy
Tipacan	Bgy
Wakat	Bgy
Yukos	Bgy
Paete	Mun
Bagumbayan (Pob.)	Bgy
Bangkusay (Pob.)	Bgy
Ermita (Pob.)	Bgy
Ibaba del Norte (Pob.)	Bgy
Ibaba del Sur (Pob.)	Bgy
Ilaya del Norte (Pob.)	Bgy
Ilaya del Sur (Pob.)	Bgy
Maytoong (Pob.)	Bgy
Quinale (Pob.)	Bgy
Pagsanjan	Mun
Anibong	Bgy
Biñan	Bgy
Buboy	Bgy
Cabanbanan	Bgy
Calusiche	Bgy
Dingin	Bgy
Lambac	Bgy
Layugan	Bgy
Magdapio	Bgy
Maulawin	Bgy
Pinagsanjan	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Sabang	Bgy
Sampaloc	Bgy
San Isidro	Bgy
Pakil	Mun
Baño (Pob.)	Bgy
Banilan	Bgy
Burgos (Pob.)	Bgy
Casa Real	Bgy
Casinsin	Bgy
Dorado	Bgy
Gonzales (Pob.)	Bgy
Kabulusan	Bgy
Matikiw	Bgy
Rizal (Pob.)	Bgy
Saray	Bgy
Taft (Pob.)	Bgy
Tavera (Pob.)	Bgy
Pangil	Mun
Balian	Bgy
Dambo	Bgy
Galalan	Bgy
Isla (Pob.)	Bgy
Mabato-Azufre	Bgy
Natividad (Pob.)	Bgy
San Jose (Pob.)	Bgy
Sulib	Bgy
Pila	Mun
Aplaya	Bgy
Bagong Pook	Bgy
Bukal	Bgy
Bulilan Norte (Pob.)	Bgy
Bulilan Sur (Pob.)	Bgy
Concepcion	Bgy
Labuin	Bgy
Linga	Bgy
Masico	Bgy
Mojon	Bgy
Pansol	Bgy
Pinagbayanan	Bgy
San Antonio	Bgy
San Miguel	Bgy
Santa Clara Norte (Pob.)	Bgy
Santa Clara Sur (Pob.)	Bgy
Tubuan	Bgy
Rizal	Mun
Antipolo	Bgy
Entablado	Bgy
Laguan	Bgy
Paule 1	Bgy
Paule 2	Bgy
East Poblacion	Bgy
West Poblacion	Bgy
Pook	Bgy
Tala	Bgy
Talaga	Bgy
Tuy	Bgy
City of San Pablo	City
Bagong Bayan II-A (Pob.)	Bgy
Bagong Pook VI-C (Pob.)	Bgy
Barangay I-A (Pob.)	Bgy
Barangay I-B (Pob.)	Bgy
Barangay II-A (Pob.)	Bgy
Barangay II-B (Pob.)	Bgy
Barangay II-C (Pob.)	Bgy
Barangay II-D (Pob.)	Bgy
Barangay II-E (Pob.)	Bgy
Barangay II-F (Pob.)	Bgy
Barangay III-A (Pob.)	Bgy
Barangay III-B (Pob.)	Bgy
Barangay III-C (Pob.)	Bgy
Barangay III-D (Pob.)	Bgy
Barangay III-E (Pob.)	Bgy
Barangay III-F (Pob.)	Bgy
Barangay IV-A (Pob.)	Bgy
Barangay IV-B (Pob.)	Bgy
Barangay IV-C (Pob.)	Bgy
Barangay V-A (Pob.)	Bgy
Barangay V-B (Pob.)	Bgy
Barangay V-C (Pob.)	Bgy
Barangay V-D (Pob.)	Bgy
Barangay VI-A (Pob.)	Bgy
Barangay VI-B (Pob.)	Bgy
Barangay VI-D (Pob.)	Bgy
Barangay VI-E (Pob.)	Bgy
Barangay VII-A (Pob.)	Bgy
Barangay VII-B (Pob.)	Bgy
Barangay VII-C (Pob.)	Bgy
Barangay VII-D (Pob.)	Bgy
Barangay VII-E (Pob.)	Bgy
Bautista	Bgy
Concepcion	Bgy
Del Remedio	Bgy
Dolores	Bgy
San Antonio 1	Bgy
San Antonio 2	Bgy
San Bartolome	Bgy
San Buenaventura	Bgy
San Crispin	Bgy
San Cristobal	Bgy
San Diego	Bgy
San Francisco	Bgy
San Gabriel	Bgy
San Gregorio	Bgy
San Ignacio	Bgy
San Isidro	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Juan	Bgy
San Lorenzo	Bgy
San Lucas 1	Bgy
San Lucas 2	Bgy
San Marcos	Bgy
San Mateo	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Ana	Bgy
Santa Catalina	Bgy
Santa Cruz	Bgy
Santa Felomina	Bgy
Santa Isabel	Bgy
Santa Maria Magdalena	Bgy
Santa Veronica	Bgy
Santiago I	Bgy
Santiago II	Bgy
Santisimo Rosario	Bgy
Santo Angel	Bgy
Santo Cristo	Bgy
Santo Niño	Bgy
Soledad	Bgy
Atisan	Bgy
Santa Elena	Bgy
Santa Maria	Bgy
Santa Monica	Bgy
City of San Pedro	City
Bagong Silang	Bgy
Cuyab	Bgy
Estrella	Bgy
G.S.I.S.	Bgy
Landayan	Bgy
Langgam	Bgy
Laram	Bgy
Magsaysay	Bgy
Nueva	Bgy
Poblacion	Bgy
Riverside	Bgy
San Antonio	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Niño	Bgy
United Bayanihan	Bgy
United Better Living	Bgy
Sampaguita Village	Bgy
Calendola	Bgy
Narra	Bgy
Chrysanthemum	Bgy
Fatima	Bgy
Maharlika	Bgy
Pacita 1	Bgy
Pacita 2	Bgy
Rosario	Bgy
San Lorenzo Ruiz	Bgy
Santa Cruz (Capital)	Mun
Alipit	Bgy
Bagumbayan	Bgy
Bubukal	Bgy
Calios	Bgy
Duhat	Bgy
Gatid	Bgy
Jasaan	Bgy
Labuin	Bgy
Malinao	Bgy
Oogong	Bgy
Pagsawitan	Bgy
Palasan	Bgy
Patimbao	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
San Jose	Bgy
San Juan	Bgy
San Pablo Norte	Bgy
San Pablo Sur	Bgy
Santisima Cruz	Bgy
Santo Angel Central	Bgy
Santo Angel Norte	Bgy
Santo Angel Sur	Bgy
Santa Maria	Mun
Adia	Bgy
Bagong Pook	Bgy
Bagumbayan	Bgy
Bubukal	Bgy
Cabooan	Bgy
Calangay	Bgy
Cambuja	Bgy
Coralan	Bgy
Cueva	Bgy
Inayapan	Bgy
Jose Laurel, Sr.	Bgy
Kayhakat	Bgy
Macasipac	Bgy
Masinao	Bgy
Mataling-Ting	Bgy
Pao-o	Bgy
Parang Ng Buho	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Jose Rizal	Bgy
Santiago	Bgy
Talangka	Bgy
Tungkod	Bgy
City of Santa Rosa	City
Aplaya	Bgy
Balibago	Bgy
Caingin	Bgy
Dila	Bgy
Dita	Bgy
Don Jose	Bgy
Ibaba	Bgy
Labas	Bgy
Macabling	Bgy
Malitlit	Bgy
Malusak (Pob.)	Bgy
Market Area (Pob.)	Bgy
Kanluran (Pob.)	Bgy
Pook	Bgy
Pulong Santa Cruz	Bgy
Santo Domingo	Bgy
Sinalhan	Bgy
Tagapo	Bgy
Siniloan	Mun
Acevida	Bgy
Bagong Pag-Asa (Pob.)	Bgy
Bagumbarangay (Pob.)	Bgy
Buhay	Bgy
Gen. Luna	Bgy
Halayhayin	Bgy
Mendiola	Bgy
Kapatalan	Bgy
Laguio	Bgy
Liyang	Bgy
Llavac	Bgy
Pandeno	Bgy
Magsaysay	Bgy
Macatad	Bgy
Mayatba	Bgy
P. Burgos	Bgy
G. Redor (Pob.)	Bgy
Salubungan	Bgy
Wawa	Bgy
J. Rizal (Pob.)	Bgy
Victoria	Mun
Banca-banca	Bgy
Daniw	Bgy
Masapang	Bgy
Nanhaya (Pob.)	Bgy
Pagalangan	Bgy
San Benito	Bgy
San Felix	Bgy
San Francisco	Bgy
San Roque (Pob.)	Bgy
Quezon	Prov
Agdangan	Mun
Binagbag	Bgy
Dayap	Bgy
Ibabang Kinagunan	Bgy
Ilayang Kinagunan	Bgy
Kanlurang Calutan	Bgy
Kanlurang Maligaya	Bgy
Salvacion	Bgy
Silangang Calutan	Bgy
Silangang Maligaya	Bgy
Sildora	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Alabat	Mun
Angeles	Bgy
Bacong	Bgy
Balungay	Bgy
Buenavista	Bgy
Caglate	Bgy
Camagong	Bgy
Gordon	Bgy
Pambilan Norte	Bgy
Pambilan Sur	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Villa Esperanza	Bgy
Villa Jesus Este	Bgy
Villa Jesus Weste	Bgy
Villa Norte	Bgy
Villa Victoria	Bgy
Atimonan	Mun
Angeles	Bgy
Balubad	Bgy
Balugohin	Bgy
Barangay Zone 1 (Pob.)	Bgy
Barangay Zone 2 (Pob.)	Bgy
Barangay Zone 3 (Pob.)	Bgy
Barangay Zone 4 (Pob.)	Bgy
Buhangin	Bgy
Caridad Ibaba	Bgy
Caridad Ilaya	Bgy
Habingan	Bgy
Inaclagan	Bgy
Inalig	Bgy
Kilait	Bgy
Kulawit	Bgy
Lakip	Bgy
Lubi	Bgy
Lumutan	Bgy
Magsaysay	Bgy
Malinao Ibaba	Bgy
Malinao Ilaya	Bgy
Malusak	Bgy
Manggalayan Bundok	Bgy
Manggalayan Labak	Bgy
Matanag	Bgy
Montes Balaon	Bgy
Montes Kallagan	Bgy
Ponon	Bgy
Rizal	Bgy
San Andres Bundok	Bgy
San Andres Labak	Bgy
San Isidro	Bgy
San Jose Balatok	Bgy
San Rafael	Bgy
Santa Catalina	Bgy
Sapaan	Bgy
Sokol	Bgy
Tagbakin	Bgy
Talaba	Bgy
Tinandog	Bgy
Villa Ibaba	Bgy
Villa Ilaya	Bgy
Buenavista	Mun
Bagong Silang	Bgy
Batabat Norte	Bgy
Batabat Sur	Bgy
Buenavista	Bgy
Bukal	Bgy
Bulo	Bgy
Cabong	Bgy
Cadlit	Bgy
Catulin	Bgy
Cawa	Bgy
De La Paz	Bgy
Del Rosario	Bgy
Hagonghong	Bgy
Ibabang Wasay	Bgy
Ilayang Wasay	Bgy
Lilukin	Bgy
Mabini	Bgy
Mabutag	Bgy
Magallanes	Bgy
Maligaya	Bgy
Manlana	Bgy
Masaya	Bgy
Poblacion	Bgy
Rizal	Bgy
Sabang Pinamasagan	Bgy
Sabang Piris	Bgy
San Diego	Bgy
San Isidro Ibaba	Bgy
San Isidro Ilaya	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Vicente	Bgy
Siain	Bgy
Villa Aurora	Bgy
Villa Batabat	Bgy
Villa Magsaysay	Bgy
Villa Veronica	Bgy
Burdeos	Mun
Aluyon	Bgy
Amot	Bgy
Anibawan	Bgy
Bonifacio	Bgy
Cabugao	Bgy
Cabungalunan	Bgy
Calutcot	Bgy
Caniwan	Bgy
Carlagan	Bgy
Mabini	Bgy
Palasan	Bgy
Poblacion	Bgy
San Rafael	Bgy
Calauag	Mun
Agoho	Bgy
Anahawan	Bgy
Anas	Bgy
Apad Lutao	Bgy
Apad Quezon	Bgy
Apad Taisan	Bgy
Atulayan	Bgy
Baclaran	Bgy
Bagong Silang	Bgy
Balibago	Bgy
Bangkuruhan	Bgy
Bantolinao	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Bigaan	Bgy
Binutas	Bgy
Biyan	Bgy
Bukal	Bgy
Buli	Bgy
Dapdap	Bgy
Dominlog	Bgy
Doña Aurora	Bgy
Guinosayan	Bgy
Ipil	Bgy
Kalibo	Bgy
Kapaluhan	Bgy
Katangtang	Bgy
Kigtan	Bgy
Kinamaligan	Bgy
Kinalin Ibaba	Bgy
Kinalin Ilaya	Bgy
Kumaludkud	Bgy
Kunalum	Bgy
Kuyaoyao	Bgy
Lagay	Bgy
Lainglaingan	Bgy
Lungib	Bgy
Mabini	Bgy
Madlangdungan	Bgy
Maglipad	Bgy
Maligaya	Bgy
Mambaling	Bgy
Manhulugin	Bgy
Marilag	Bgy
Mulay	Bgy
Pandanan	Bgy
Pansol	Bgy
Patihan	Bgy
Pinagbayanan	Bgy
Pinagkamaligan	Bgy
Pinagsakahan	Bgy
Pinagtalleran	Bgy
Rizal Ibaba	Bgy
Rizal Ilaya	Bgy
Sabang I	Bgy
Sabang II	Bgy
Salvacion	Bgy
San Quintin	Bgy
San Roque Ibaba	Bgy
San Roque Ilaya	Bgy
Santa Cecilia	Bgy
Santa Maria	Bgy
Santa Milagrosa	Bgy
Santa Rosa	Bgy
Santo Angel	Bgy
Santo Domingo	Bgy
Sinag	Bgy
Sumilang	Bgy
Sumulong	Bgy
Tabansak	Bgy
Talingting	Bgy
Tamis	Bgy
Tikiwan	Bgy
Tiniguiban	Bgy
Villa Magsino	Bgy
Villa San Isidro	Bgy
Viñas	Bgy
Yaganak	Bgy
Candelaria	Mun
Poblacion	Bgy
Buenavista East	Bgy
Buenavista West	Bgy
Bukal Norte	Bgy
Bukal Sur	Bgy
Kinatihan I	Bgy
Kinatihan II	Bgy
Malabanban Norte	Bgy
Malabanban Sur	Bgy
Mangilag Norte	Bgy
Mangilag Sur	Bgy
Masalukot I	Bgy
Masalukot II	Bgy
Masalukot III	Bgy
Masalukot IV	Bgy
Masin Norte	Bgy
Masin Sur	Bgy
Mayabobo	Bgy
Pahinga Norte	Bgy
Pahinga Sur	Bgy
San Andres	Bgy
San Isidro	Bgy
Santa Catalina Norte	Bgy
Santa Catalina Sur	Bgy
Masalukot V	Bgy
Catanauan	Mun
Ajos	Bgy
Anusan	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Bolo	Bgy
Bulagsong	Bgy
Camandiison	Bgy
Canculajao	Bgy
Catumbo	Bgy
Cawayanin Ibaba	Bgy
Cawayanin Ilaya	Bgy
Cutcutan	Bgy
Dahican	Bgy
Doongan Ibaba	Bgy
Doongan Ilaya	Bgy
Gatasan	Bgy
Macpac	Bgy
Madulao	Bgy
Matandang Sabang Kanluran	Bgy
Matandang Sabang Silangan	Bgy
Milagrosa	Bgy
Navitas	Bgy
Pacabit	Bgy
San Antonio Magkupa	Bgy
San Antonio Pala	Bgy
San Isidro	Bgy
San Jose	Bgy
San Pablo	Bgy
San Roque	Bgy
San Vicente Kanluran	Bgy
San Vicente Silangan	Bgy
Santa Maria	Bgy
Tagabas Ibaba	Bgy
Tagabas Ilaya	Bgy
Tagbacan Ibaba	Bgy
Tagbacan Ilaya	Bgy
Tagbacan Silangan	Bgy
Tuhian	Bgy
Barangay 10 (Pob.)	Bgy
Dolores	Mun
Antonino	Bgy
Bagong Anyo (Pob.)	Bgy
Bayanihan (Pob.)	Bgy
Bulakin I	Bgy
Bungoy	Bgy
Cabatang	Bgy
Dagatan	Bgy
Kinabuhayan	Bgy
Maligaya (Pob.)	Bgy
Manggahan	Bgy
Pinagdanlayan	Bgy
Putol	Bgy
San Mateo	Bgy
Santa Lucia	Bgy
Silanganan (Pob.)	Bgy
Bulakin II	Bgy
General Luna	Mun
Bacong Ibaba	Bgy
Bacong Ilaya	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Lavides	Bgy
Magsaysay	Bgy
Malaya	Bgy
Nieva	Bgy
Recto	Bgy
San Ignacio Ibaba	Bgy
San Ignacio Ilaya	Bgy
San Isidro Ibaba	Bgy
San Isidro Ilaya	Bgy
San Jose	Bgy
San Nicolas	Bgy
San Vicente	Bgy
Santa Maria Ibaba	Bgy
Santa Maria Ilaya	Bgy
Sumilang	Bgy
Villarica	Bgy
General Nakar	Mun
Anoling	Bgy
Banglos	Bgy
Batangan	Bgy
Catablingan	Bgy
Canaway	Bgy
Lumutan	Bgy
Mahabang Lalim	Bgy
Maigang	Bgy
Maligaya	Bgy
Magsikap	Bgy
Minahan Norte	Bgy
Minahan Sur	Bgy
Pagsangahan	Bgy
Pamplona	Bgy
Pisa	Bgy
Poblacion	Bgy
Sablang	Bgy
San Marcelino	Bgy
Umiray	Bgy
Guinayangan	Mun
A. Mabini	Bgy
Aloneros	Bgy
Arbismen	Bgy
Bagong Silang	Bgy
Balinarin	Bgy
Bukal Maligaya	Bgy
Cabibihan	Bgy
Cabong Norte	Bgy
Cabong Sur	Bgy
Calimpak	Bgy
Capuluan Central	Bgy
Capuluan Tulon	Bgy
Dancalan Caimawan	Bgy
Dancalan Central	Bgy
Danlagan Batis	Bgy
Danlagan Cabayao	Bgy
Danlagan Central	Bgy
Danlagan Reserva	Bgy
Del Rosario	Bgy
Dungawan Central	Bgy
Dungawan Paalyunan	Bgy
Dungawan Pantay	Bgy
Ermita	Bgy
Gapas	Bgy
Himbubulo Este	Bgy
Himbubulo Weste	Bgy
Hinabaan	Bgy
Ligpit Bantayan	Bgy
Lubigan	Bgy
Magallanes	Bgy
Magsaysay	Bgy
Manggagawa	Bgy
Manggalang	Bgy
Manlayo	Bgy
Poblacion	Bgy
Salakan	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Lorenzo	Bgy
San Luis I	Bgy
San Luis II	Bgy
San Miguel	Bgy
San Pedro I	Bgy
San Pedro II	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Santa Teresita	Bgy
Sintones	Bgy
Sisi	Bgy
Tikay	Bgy
Triumpo	Bgy
Villa Hiwasayan	Bgy
Gumaca	Mun
Adia Bitaog	Bgy
Anonangin	Bgy
Bagong Buhay (Pob.)	Bgy
Bamban	Bgy
Bantad	Bgy
Batong Dalig	Bgy
Biga	Bgy
Binambang	Bgy
Buensuceso	Bgy
Bungahan	Bgy
Butaguin	Bgy
Calumangin	Bgy
Camohaguin	Bgy
Casasahan Ibaba	Bgy
Casasahan Ilaya	Bgy
Cawayan	Bgy
Gayagayaan	Bgy
Gitnang Barrio	Bgy
Hardinan	Bgy
Inaclagan	Bgy
Inagbuhan Ilaya	Bgy
Hagakhakin	Bgy
Labnig	Bgy
Laguna	Bgy
Mabini (Pob.)	Bgy
Mabunga	Bgy
Malabtog	Bgy
Manlayaan	Bgy
Marcelo H. Del Pilar	Bgy
Mataas Na Bundok	Bgy
Maunlad (Pob.)	Bgy
Pagsabangan	Bgy
Panikihan	Bgy
Peñafrancia (Pob.)	Bgy
Pipisik (Pob.)	Bgy
Progreso	Bgy
Rizal (Pob.)	Bgy
Rosario	Bgy
San Agustin	Bgy
San Diego Poblacion	Bgy
San Isidro Kanluran	Bgy
San Isidro Silangan	Bgy
San Juan De Jesus	Bgy
San Vicente	Bgy
Sastre	Bgy
Tabing Dagat (Pob.)	Bgy
Tumayan	Bgy
Villa Arcaya	Bgy
Villa Bota	Bgy
Villa Fuerte	Bgy
Villa Mendoza	Bgy
Villa Padua	Bgy
Villa Perez	Bgy
Villa M. Principe	Bgy
Villa Tañada	Bgy
Villa Victoria	Bgy
San Diego	Bgy
Villa Nava	Bgy
Lagyo	Bgy
Infanta	Mun
Abiawin	Bgy
Agos-agos	Bgy
Alitas	Bgy
Amolongin	Bgy
Anibong	Bgy
Antikin	Bgy
Bacong	Bgy
Balobo	Bgy
Bantilan	Bgy
Banugao	Bgy
Poblacion 1	Bgy
Poblacion 38	Bgy
Poblacion 39	Bgy
Batican	Bgy
Binonoan	Bgy
Binulasan	Bgy
Boboin	Bgy
Comon	Bgy
Dinahican	Bgy
Gumian	Bgy
Ilog	Bgy
Ingas	Bgy
Catambungan	Bgy
Cawaynin	Bgy
Langgas	Bgy
Libjo	Bgy
Lual	Bgy
Magsaysay	Bgy
Maypulot	Bgy
Miswa	Bgy
Pilaway	Bgy
Pinaglapatan	Bgy
Pulo	Bgy
Silangan	Bgy
Tongohin	Bgy
Tudturan	Bgy
Jomalig	Mun
Bukal	Bgy
Casuguran	Bgy
Gango	Bgy
Talisoy (Pob.)	Bgy
Apad	Bgy
Lopez	Mun
Bacungan	Bgy
Bagacay	Bgy
Banabahin Ibaba	Bgy
Banabahin Ilaya	Bgy
Burgos (Pob.)	Bgy
Gomez (Pob.)	Bgy
Magsaysay (Pob.)	Bgy
Talolong (Pob.)	Bgy
Bayabas	Bgy
Bebito	Bgy
Bigajo	Bgy
Binahian A	Bgy
Binahian B	Bgy
Binahian C	Bgy
Bocboc	Bgy
Buenavista	Bgy
Buyacanin	Bgy
Cagacag	Bgy
Calantipayan	Bgy
Canda Ibaba	Bgy
Canda Ilaya	Bgy
Cawayan	Bgy
Cawayanin	Bgy
Cogorin Ibaba	Bgy
Cogorin Ilaya	Bgy
Concepcion	Bgy
Danlagan	Bgy
De La Paz	Bgy
Del Pilar	Bgy
Del Rosario	Bgy
Esperanza Ibaba	Bgy
Esperanza Ilaya	Bgy
Guihay	Bgy
Guinuangan	Bgy
Guites	Bgy
Hondagua	Bgy
Ilayang Ilog A	Bgy
Ilayang Ilog B	Bgy
Inalusan	Bgy
Jongo	Bgy
Lalaguna	Bgy
Lourdes	Bgy
Mabanban	Bgy
Mabini	Bgy
Magallanes	Bgy
Maguilayan	Bgy
Mahayod-Hayod	Bgy
Mal-ay	Bgy
Mandoog	Bgy
Manguisian	Bgy
Matinik	Bgy
Monteclaro	Bgy
Pamampangin	Bgy
Pansol	Bgy
Peñafrancia	Bgy
Pisipis	Bgy
Rizal	Bgy
Roma	Bgy
Rosario	Bgy
Samat	Bgy
San Andres	Bgy
San Antonio	Bgy
San Francisco A	Bgy
San Francisco B	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
Santa Catalina	Bgy
Santa Elena	Bgy
Santa Jacobe	Bgy
Santa Lucia	Bgy
Santa Maria	Bgy
Santa Rosa	Bgy
Santo Niño Ibaba	Bgy
Santo Niño Ilaya	Bgy
Silang	Bgy
Sugod	Bgy
Sumalang	Bgy
Tan-ag Ibaba	Bgy
Tan-ag Ilaya	Bgy
Tocalin	Bgy
Vegaflor	Bgy
Vergaña	Bgy
Veronica	Bgy
Villa Aurora	Bgy
Villa Espina	Bgy
Villa Hermosa	Bgy
Villa Geda	Bgy
Villamonte	Bgy
Villanacaob	Bgy
Rizal	Bgy
Santa Teresa	Bgy
Lucban	Mun
Abang	Bgy
Aliliw	Bgy
Atulinao	Bgy
Ayuti	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Igang	Bgy
Kabatete	Bgy
Kakawit	Bgy
Kalangay	Bgy
Kalyaat	Bgy
Kilib	Bgy
Kulapi	Bgy
Mahabang Parang	Bgy
Malupak	Bgy
Manasa	Bgy
May-It	Bgy
Nagsinamo	Bgy
Nalunao	Bgy
Palola	Bgy
Piis	Bgy
Barangay 1 (Pob.)	Bgy
Samil	Bgy
Tiawe	Bgy
Tinamnan	Bgy
City of Lucena (Capital)	City
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barra	Bgy
Bocohan	Bgy
Mayao Castillo	Bgy
Cotta	Bgy
Gulang-gulang	Bgy
Dalahican	Bgy
Domoit	Bgy
Ibabang Dupay	Bgy
Ibabang Iyam	Bgy
Ibabang Talim	Bgy
Ilayang Dupay	Bgy
Ilayang Iyam	Bgy
Ilayang Talim	Bgy
Isabang	Bgy
Mayao Crossing	Bgy
Mayao Kanluran	Bgy
Mayao Parada	Bgy
Mayao Silangan	Bgy
Ransohan	Bgy
Salinas	Bgy
Talao-talao	Bgy
Market View	Bgy
Macalelon	Mun
Amontay	Bgy
Anos	Bgy
Buyao	Bgy
Candangal	Bgy
Calantas	Bgy
Lahing	Bgy
Luctob	Bgy
Mabini Ibaba	Bgy
Mabini Ilaya	Bgy
Malabahay	Bgy
Mambog	Bgy
Olongtao Ibaba	Bgy
Olongtao Ilaya	Bgy
Padre Herrera	Bgy
Pajarillo	Bgy
Pinagbayanan	Bgy
Rodriquez (Pob.)	Bgy
Rizal (Pob.)	Bgy
Castillo (Pob.)	Bgy
Pag-Asa (Pob.)	Bgy
Masipag (Pob.)	Bgy
Damayan (Pob.)	Bgy
San Isidro	Bgy
San Jose	Bgy
San Nicolas	Bgy
San Vicente	Bgy
Taguin	Bgy
Tubigan Ibaba	Bgy
Tubigan Ilaya	Bgy
Vista Hermosa	Bgy
Mauban	Mun
Abo-abo	Bgy
Alitap	Bgy
Baao	Bgy
Balaybalay	Bgy
Bato	Bgy
Cagbalete I	Bgy
Cagbalete II	Bgy
Cagsiay I	Bgy
Cagsiay II	Bgy
Cagsiay III	Bgy
Concepcion	Bgy
Liwayway	Bgy
Lucutan	Bgy
Luya-luya	Bgy
Macasin	Bgy
Lual (Pob.)	Bgy
Mabato (Pob.)	Bgy
Daungan (Pob.)	Bgy
Bagong Bayan (Pob.)	Bgy
Sadsaran (Pob.)	Bgy
Rizaliana (Pob.)	Bgy
Polo	Bgy
Remedios I	Bgy
Remedios II	Bgy
Rosario	Bgy
San Gabriel	Bgy
San Isidro	Bgy
San Jose	Bgy
San Lorenzo	Bgy
San Miguel	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Lucia	Bgy
Santo Angel	Bgy
Santo Niño	Bgy
Santol	Bgy
Soledad	Bgy
Tapucan	Bgy
Lual Rural	Bgy
Mulanay	Mun
Ajos	Bgy
Amuguis	Bgy
Anonang	Bgy
Bagong Silang	Bgy
Bagupaye	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Bolo	Bgy
Buenavista	Bgy
Burgos	Bgy
Butanyog	Bgy
Canuyep	Bgy
F. Nanadiego	Bgy
Ibabang Cambuga	Bgy
Ibabang Yuni	Bgy
Ilayang Cambuga	Bgy
Ilayang Yuni	Bgy
Latangan	Bgy
Magsaysay	Bgy
Matataja	Bgy
Pakiing	Bgy
Patabog	Bgy
Sagongon	Bgy
San Isidro	Bgy
San Pedro	Bgy
Santa Rosa	Bgy
Padre Burgos	Mun
Cabuyao Norte	Bgy
Cabuyao Sur	Bgy
Danlagan	Bgy
Duhat	Bgy
Hinguiwin	Bgy
Kinagunan Ibaba	Bgy
Kinagunan Ilaya	Bgy
Lipata	Bgy
Marao	Bgy
Marquez	Bgy
Burgos (Pob.)	Bgy
Campo (Pob.)	Bgy
Basiao (Pob.)	Bgy
Punta (Pob.)	Bgy
Rizal	Bgy
San Isidro	Bgy
San Vicente	Bgy
Sipa	Bgy
Tulay Buhangin	Bgy
Villapaz	Bgy
Walay	Bgy
Yawe	Bgy
Pagbilao	Mun
Alupaye	Bgy
Añato	Bgy
Antipolo	Bgy
Bantigue	Bgy
Bigo	Bgy
Binahaan	Bgy
Bukal	Bgy
Ibabang Bagumbungan	Bgy
Ibabang Palsabangon	Bgy
Ibabang Polo	Bgy
Ikirin	Bgy
Ilayang Bagumbungan	Bgy
Ilayang Palsabangon	Bgy
Ilayang Polo	Bgy
Kanluran Malicboy	Bgy
Mapagong	Bgy
Mayhay	Bgy
Pinagbayanan	Bgy
Barangay 1 Castillo (Pob.)	Bgy
Barangay 2 Daungan (Pob.)	Bgy
Barangay 3 Del Carmen (Pob.)	Bgy
Barangay 4 Parang (Pob.)	Bgy
Barangay 5 Santa Catalina (Pob.)	Bgy
Barangay 6 Tambak (Pob.)	Bgy
Silangan Malicboy	Bgy
Talipan	Bgy
Tukalan	Bgy
Panukulan	Mun
Balungay	Bgy
Bato	Bgy
Bonbon	Bgy
Calasumanga	Bgy
Kinalagti	Bgy
Libo	Bgy
Lipata	Bgy
Matangkap	Bgy
Milawid	Bgy
Pagitan	Bgy
Pandan	Bgy
San Juan (Pob.)	Bgy
Rizal	Bgy
Patnanungan	Mun
Amaga	Bgy
Busdak	Bgy
Kilogan	Bgy
Luod	Bgy
Patnanungan Norte	Bgy
Patnanungan Sur (Pob.)	Bgy
Perez	Mun
Maabot	Bgy
Mainit Norte	Bgy
Mainit Sur	Bgy
Pambuhan	Bgy
Pinagtubigan Este	Bgy
Pinagtubigan Weste	Bgy
Pagkakaisa Pob.	Bgy
Mapagmahal Pob.	Bgy
Bagong Pag-Asa Pob.	Bgy
Bagong Silang Pob.	Bgy
Rizal	Bgy
Sangirin	Bgy
Villamanzano Norte	Bgy
Villamanzano Sur	Bgy
Pitogo	Mun
Amontay	Bgy
Cometa	Bgy
Biga	Bgy
Bilucao	Bgy
Cabulihan	Bgy
Cawayanin	Bgy
Gangahin	Bgy
Ibabang Burgos	Bgy
Ibabang Pacatin	Bgy
Ibabang Piña	Bgy
Ibabang Soliyao	Bgy
Ilayang Burgos	Bgy
Ilayang Pacatin	Bgy
Ilayang Piña	Bgy
Ilayang Soliyao	Bgy
Nag-Cruz	Bgy
Osmeña	Bgy
Payte	Bgy
Pinagbayanan	Bgy
Masaya (Pob.)	Bgy
Manggahan (Pob.)	Bgy
Dulong Bayan (Pob.)	Bgy
Pag-Asa (Pob.)	Bgy
Castillo (Pob.)	Bgy
Maaliw (Pob.)	Bgy
Mayubok (Pob.)	Bgy
Pamilihan (Pob.)	Bgy
Dalampasigan (Pob.)	Bgy
Poctol	Bgy
Quezon	Bgy
Quinagasan	Bgy
Rizalino	Bgy
Saguinsinan	Bgy
Sampaloc	Bgy
San Roque	Bgy
Sisirin	Bgy
Sumag Este	Bgy
Sumag Norte	Bgy
Sumag Weste	Bgy
Plaridel	Mun
Concepcion	Bgy
Duhat	Bgy
Ilaya	Bgy
Ilosong	Bgy
Tanauan	Bgy
Central (Pob.)	Bgy
Paang Bundok (Pob.)	Bgy
Pampaaralan (Pob.)	Bgy
M. L. Tumagay Pob.	Bgy
Polillo	Mun
Anawan	Bgy
Atulayan	Bgy
Balesin	Bgy
Bañadero	Bgy
Binibitinan	Bgy
Bislian	Bgy
Bucao	Bgy
Canicanian	Bgy
Kalubakis	Bgy
Languyin	Bgy
Libjo	Bgy
Pamatdan	Bgy
Pilion	Bgy
Pinaglubayan	Bgy
Poblacion	Bgy
Sabang	Bgy
Salipsip	Bgy
Sibulan	Bgy
Taluong	Bgy
Tamulaya-Anitong	Bgy
Quezon	Mun
Apad	Bgy
Argosino	Bgy
Cagbalogo	Bgy
Caridad	Bgy
Cometa	Bgy
Del Pilar	Bgy
Guinhawa	Bgy
Gumubat	Bgy
Magsino	Bgy
Mascariña	Bgy
Montaña	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Sabang	Bgy
Silangan	Bgy
Tagkawa	Bgy
Villa Belen	Bgy
Villa Francia	Bgy
Villa Gomez	Bgy
Villa Mercedes	Bgy
Real	Mun
Poblacion I	Bgy
Capalong	Bgy
Cawayan	Bgy
Kiloloran	Bgy
Llavac	Bgy
Lubayat	Bgy
Malapad	Bgy
Maragondon	Bgy
Pandan	Bgy
Tanauan	Bgy
Tignoan	Bgy
Ungos	Bgy
Poblacion 61	Bgy
Maunlad	Bgy
Bagong Silang	Bgy
Masikap	Bgy
Tagumpay	Bgy
Sampaloc	Mun
Alupay	Bgy
Apasan	Bgy
Banot	Bgy
Bataan	Bgy
Bayongon	Bgy
Bilucao	Bgy
Caldong	Bgy
Ibabang Owain	Bgy
Ilayang Owain	Bgy
Mamala	Bgy
San Bueno	Bgy
San Isidro (Pob.)	Bgy
San Roque (Pob.)	Bgy
Taquico	Bgy
San Andres	Mun
Alibihaban	Bgy
Camflora	Bgy
Mangero	Bgy
Pansoy	Bgy
Tala	Bgy
Talisay	Bgy
Poblacion	Bgy
San Antonio	Mun
Arawan	Bgy
Bagong Niing	Bgy
Balat Atis	Bgy
Briones	Bgy
Bulihan	Bgy
Buliran	Bgy
Callejon	Bgy
Corazon	Bgy
Manuel del Valle, Sr.	Bgy
Loob	Bgy
Magsaysay	Bgy
Matipunso	Bgy
Niing	Bgy
Poblacion	Bgy
Pulo	Bgy
Pury	Bgy
Sampaga	Bgy
Sampaguita	Bgy
San Jose	Bgy
Sinturisan	Bgy
San Francisco	Mun
Butanguiad	Bgy
Casay	Bgy
Cawayan I	Bgy
Cawayan II	Bgy
Huyon-Uyon	Bgy
Ibabang Tayuman	Bgy
Ilayang Tayuman	Bgy
Inabuan	Bgy
Nasalaan	Bgy
Pagsangahan	Bgy
Poblacion	Bgy
Pugon	Bgy
Silongin	Bgy
Don Juan Vercelos	Bgy
Mabuñga	Bgy
Santo Niño	Bgy
San Narciso	Mun
Abuyon	Bgy
Andres Bonifacio	Bgy
Bani	Bgy
Binay	Bgy
Buenavista	Bgy
Busokbusokan	Bgy
Calwit	Bgy
Guinhalinan	Bgy
Lacdayan	Bgy
Maguiting	Bgy
Manlampong	Bgy
Pagkakaisa (Pob.)	Bgy
Maligaya (Pob.)	Bgy
Bayanihan (Pob.)	Bgy
Pagdadamayan (Pob.)	Bgy
Punta	Bgy
Rizal	Bgy
San Isidro	Bgy
San Juan	Bgy
San Vicente	Bgy
Vigo Central	Bgy
Villa Aurin	Bgy
Villa Reyes	Bgy
White Cliff	Bgy
Sariaya	Mun
Antipolo	Bgy
Balubal	Bgy
Concepcion Pinagbakuran	Bgy
Bignay 1	Bgy
Bignay 2	Bgy
Bucal	Bgy
Canda	Bgy
Castañas	Bgy
Concepcion Banahaw	Bgy
Concepcion No. 1	Bgy
Concepcion Palasan	Bgy
Gibanga	Bgy
Guisguis-San Roque	Bgy
Guisguis-Talon	Bgy
Janagdong 1	Bgy
Janagdong 2	Bgy
Limbon	Bgy
Lutucan Bata	Bgy
Lutucan Malabag	Bgy
Lutucan 1	Bgy
Mamala I	Bgy
Manggalang 1	Bgy
Manggalang Tulo-tulo	Bgy
Manggalang-Bantilan	Bgy
Manggalang-Kiling	Bgy
Montecillo	Bgy
Morong	Bgy
Pili	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Sampaloc Bogon	Bgy
Sampaloc 1	Bgy
Sampaloc 2	Bgy
Sampaloc Santo Cristo	Bgy
Talaan Aplaya	Bgy
Talaanpantoc	Bgy
Tumbaga 1	Bgy
Tumbaga 2	Bgy
Barangay 1 (Pob.)	Bgy
Mamala II	Bgy
Tagkawayan	Mun
Aldavoc	Bgy
Aliji	Bgy
Bagong Silang	Bgy
Bamban	Bgy
Bosigon	Bgy
Bukal	Bgy
Cabugwang	Bgy
Cagascas	Bgy
Casispalan	Bgy
Colong-colong	Bgy
Del Rosario	Bgy
Cabibihan	Bgy
Candalapdap	Bgy
Katimo	Bgy
Kinatakutan	Bgy
Landing	Bgy
Laurel	Bgy
Magsaysay	Bgy
Maguibuay	Bgy
Mahinta	Bgy
Malbog	Bgy
Manato Central	Bgy
Manato Station	Bgy
Mangayao	Bgy
Mansilay	Bgy
Mapulot	Bgy
Munting Parang	Bgy
Payapa	Bgy
Poblacion	Bgy
Rizal	Bgy
Sabang	Bgy
San Diego	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cecilia	Bgy
Santa Monica	Bgy
Santo Niño I	Bgy
Santo Niño II	Bgy
Santo Tomas	Bgy
Seguiwan	Bgy
Tabason	Bgy
Tunton	Bgy
Victoria	Bgy
City of Tayabas	City
Alitao	Bgy
Alupay	Bgy
Angeles Zone I (Pob.)	Bgy
Angeles Zone II	Bgy
Angeles Zone III	Bgy
Angeles Zone IV	Bgy
Angustias Zone I (Pob.)	Bgy
Angustias Zone II	Bgy
Angustias Zone III	Bgy
Angustias Zone IV	Bgy
Anos	Bgy
Ayaas	Bgy
Baguio	Bgy
Banilad	Bgy
Calantas	Bgy
Camaysa	Bgy
Dapdap	Bgy
Gibanga	Bgy
Alsam Ibaba	Bgy
Bukal Ibaba	Bgy
Ilasan Ibaba	Bgy
Nangka Ibaba	Bgy
Palale Ibaba	Bgy
Ibas	Bgy
Alsam Ilaya	Bgy
Bukal Ilaya	Bgy
Ilasan Ilaya	Bgy
Nangka Ilaya	Bgy
Palale Ilaya	Bgy
Ipilan	Bgy
Isabang	Bgy
Calumpang	Bgy
Domoit Kanluran	Bgy
Katigan Kanluran	Bgy
Palale Kanluran	Bgy
Lakawan	Bgy
Lalo	Bgy
Lawigue	Bgy
Lita (Pob.)	Bgy
Malaoa	Bgy
Masin	Bgy
Mate	Bgy
Mateuna	Bgy
Mayowe	Bgy
Opias	Bgy
Pandakaki	Bgy
Pook	Bgy
Potol	Bgy
San Diego Zone I (Pob.)	Bgy
San Diego Zone II	Bgy
San Diego Zone III	Bgy
San Diego Zone IV	Bgy
San Isidro Zone I (Pob.)	Bgy
San Isidro Zone II	Bgy
San Isidro Zone III	Bgy
San Isidro Zone IV	Bgy
San Roque Zone I (Pob.)	Bgy
San Roque Zone II	Bgy
Domoit Silangan	Bgy
Katigan Silangan	Bgy
Palale Silangan	Bgy
Talolong	Bgy
Tamlong	Bgy
Tongko	Bgy
Valencia	Bgy
Wakas	Bgy
Tiaong	Mun
Anastacia	Bgy
Ayusan I	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Behia	Bgy
Bukal	Bgy
Bula	Bgy
Bulakin	Bgy
Cabatang	Bgy
Cabay	Bgy
Del Rosario	Bgy
Lagalag	Bgy
Lalig	Bgy
Lumingon	Bgy
Lusacan	Bgy
Paiisa	Bgy
Palagaran	Bgy
Quipot	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Pedro	Bgy
Tagbakin	Bgy
Talisay	Bgy
Tamisian	Bgy
San Francisco	Bgy
Aquino	Bgy
Ayusan II	Bgy
Unisan	Mun
Almacen	Bgy
Balagtas	Bgy
Balanacan	Bgy
Bulo Ibaba	Bgy
Bulo Ilaya	Bgy
Bonifacio	Bgy
Burgos	Bgy
Caigdal	Bgy
General Luna	Bgy
Kalilayan Ibaba	Bgy
Cabulihan Ibaba	Bgy
Mairok Ibaba	Bgy
Kalilayan Ilaya	Bgy
Cabulihan Ilaya	Bgy
Mabini	Bgy
Mairok Ilaya	Bgy
Malvar	Bgy
Maputat	Bgy
Muliguin	Bgy
Pagaguasan	Bgy
Panaon Ibaba	Bgy
Panaon Ilaya	Bgy
Plaridel	Bgy
F. De Jesus (Pob.)	Bgy
R. Lapu-lapu (Pob.)	Bgy
Raja Soliman (Pob.)	Bgy
R. Magsaysay (Pob.)	Bgy
Poctol	Bgy
Punta	Bgy
Rizal Ibaba	Bgy
Rizal Ilaya	Bgy
San Roque	Bgy
Socorro	Bgy
Tagumpay	Bgy
Tubas	Bgy
Tubigan	Bgy
Rizal	Prov
Angono	Mun
Bagumbayan	Bgy
Kalayaan	Bgy
Poblacion Ibaba	Bgy
Poblacion Itaas	Bgy
San Isidro	Bgy
San Pedro	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Mahabang Parang	Bgy
City of Antipolo (Capital)	City
Calawis	Bgy
Cupang	Bgy
Dela Paz (Pob.)	Bgy
Mayamot	Bgy
San Isidro (Pob.)	Bgy
San Jose (Pob.)	Bgy
San Roque (Pob.)	Bgy
Mambugan	Bgy
Bagong Nayon	Bgy
Beverly Hills	Bgy
Dalig	Bgy
Inarawan	Bgy
San Juan	Bgy
San Luis	Bgy
Santa Cruz	Bgy
Muntingdilaw	Bgy
Baras	Mun
Evangelista	Bgy
Rizal (Pob.)	Bgy
San Jose	Bgy
San Salvador	Bgy
Santiago	Bgy
Concepcion	Bgy
San Juan	Bgy
San Miguel	Bgy
Mabini	Bgy
Pinugay	Bgy
Binangonan	Mun
Bangad	Bgy
Batingan	Bgy
Bilibiran	Bgy
Binitagan	Bgy
Bombong	Bgy
Buhangin	Bgy
Calumpang	Bgy
Ginoong Sanay	Bgy
Gulod	Bgy
Habagatan	Bgy
Ithan	Bgy
Janosa	Bgy
Kalawaan	Bgy
Kalinawan	Bgy
Kasile	Bgy
Kaytome	Bgy
Kinaboogan	Bgy
Kinagatan	Bgy
Libis (Pob.)	Bgy
Limbon-limbon	Bgy
Lunsad	Bgy
Mahabang Parang	Bgy
Macamot	Bgy
Mambog	Bgy
Palangoy	Bgy
Pantok	Bgy
Pila Pila	Bgy
Pinagdilawan	Bgy
Pipindan	Bgy
Rayap	Bgy
Sapang	Bgy
Tabon	Bgy
Tagpos	Bgy
Tatala	Bgy
Tayuman	Bgy
Layunan (Pob.)	Bgy
Libid (Pob.)	Bgy
Malakaban	Bgy
Pag-Asa	Bgy
San Carlos	Bgy
Cainta	Mun
San Andres (Pob.)	Bgy
San Isidro	Bgy
San Juan	Bgy
San Roque	Bgy
Santa Rosa	Bgy
Santo Niño	Bgy
Santo Domingo	Bgy
Cardona	Mun
Balibago	Bgy
Boor	Bgy
Calahan	Bgy
Dalig	Bgy
Del Remedio (Pob.)	Bgy
Iglesia (Pob.)	Bgy
Lambac	Bgy
Looc	Bgy
Malanggam-Calubacan	Bgy
Nagsulo	Bgy
Navotas	Bgy
Patunhay	Bgy
Real (Pob.)	Bgy
Sampad	Bgy
San Roque (Pob.)	Bgy
Subay	Bgy
Ticulio	Bgy
Tuna	Bgy
Jala-Jala	Mun
Bagumbong	Bgy
Bayugo	Bgy
Second District (Pob.)	Bgy
Third District (Pob.)	Bgy
Lubo	Bgy
Pagkalinawan	Bgy
Palaypalay	Bgy
Punta	Bgy
Sipsipin	Bgy
Special District (Pob.)	Bgy
Paalaman	Bgy
Rodriguez	Mun
Balite (Pob.)	Bgy
Burgos	Bgy
Geronimo	Bgy
Macabud	Bgy
Manggahan	Bgy
Mascap	Bgy
Puray	Bgy
Rosario	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael	Bgy
Morong	Mun
Bombongan	Bgy
Can-Cal-Lan	Bgy
Lagundi	Bgy
Maybancal	Bgy
San Guillermo	Bgy
San Jose (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Pedro (Pob.)	Bgy
Pililla	Mun
Bagumbayan (Pob.)	Bgy
Halayhayin	Bgy
Hulo (Pob.)	Bgy
Imatong (Pob.)	Bgy
Malaya	Bgy
Niogan	Bgy
Quisao	Bgy
Wawa (Pob.)	Bgy
Takungan (Pob.)	Bgy
San Mateo	Mun
Ampid I	Bgy
Dulong Bayan 1	Bgy
Dulong Bayan 2	Bgy
Guinayang	Bgy
Guitnang Bayan I (Pob.)	Bgy
Guitnang Bayan II (Pob.)	Bgy
Malanday	Bgy
Maly	Bgy
Santa Ana	Bgy
Ampid II	Bgy
Banaba	Bgy
Gulod Malaya	Bgy
Pintong Bocawe	Bgy
Santo Niño	Bgy
Silangan	Bgy
Tanay	Mun
Cayabu	Bgy
Cuyambay	Bgy
Daraitan	Bgy
Katipunan-Bayan (Pob.)	Bgy
Kaybuto (Pob.)	Bgy
Laiban	Bgy
Mag-Ampon (Pob.)	Bgy
Mamuyao	Bgy
Pinagkamaligan (Pob.)	Bgy
Plaza Aldea (Pob.)	Bgy
Sampaloc	Bgy
San Andres	Bgy
San Isidro (Pob.)	Bgy
Santa Inez	Bgy
Santo Niño	Bgy
Tabing Ilog (Pob.)	Bgy
Tandang Kutyo (Pob.)	Bgy
Tinucan	Bgy
Wawa (Pob.)	Bgy
Madilay-dilay	Bgy
Taytay	Mun
Dolores (Pob.)	Bgy
Muzon	Bgy
San Isidro	Bgy
San Juan	Bgy
Santa Ana	Bgy
Teresa	Mun
Bagumbayan	Bgy
Dalig	Bgy
Dulumbayan	Bgy
May-Iba	Bgy
Poblacion	Bgy
Prinza	Bgy
San Gabriel	Bgy
San Roque	Bgy
Calumpang Santo Cristo	Bgy
MIMAROPA Region	Reg
Marinduque	Prov
Boac (Capital)	Mun
Agot	Bgy
Agumaymayan	Bgy
Amoingon	Bgy
Apitong	Bgy
Balagasan	Bgy
Balaring	Bgy
Balimbing	Bgy
Balogo	Bgy
Bangbangalon	Bgy
Bamban	Bgy
Bantad	Bgy
Bantay	Bgy
Bayuti	Bgy
Binunga	Bgy
Boi	Bgy
Boton	Bgy
Buliasnin	Bgy
Bunganay	Bgy
Maligaya	Bgy
Caganhao	Bgy
Canat	Bgy
Catubugan	Bgy
Cawit	Bgy
Daig	Bgy
Daypay	Bgy
Duyay	Bgy
Ihatub	Bgy
Isok II Pob.	Bgy
Hinapulan	Bgy
Laylay	Bgy
Lupac	Bgy
Mahinhin	Bgy
Mainit	Bgy
Malbog	Bgy
Malusak (Pob.)	Bgy
Mansiwat	Bgy
Mataas Na Bayan (Pob.)	Bgy
Maybo	Bgy
Mercado (Pob.)	Bgy
Murallon (Pob.)	Bgy
Ogbac	Bgy
Pawa	Bgy
Pili	Bgy
Poctoy	Bgy
Poras	Bgy
Puting Buhangin	Bgy
Puyog	Bgy
Sabong	Bgy
San Miguel (Pob.)	Bgy
Santol	Bgy
Sawi	Bgy
Tabi	Bgy
Tabigue	Bgy
Tagwak	Bgy
Tambunan	Bgy
Tampus (Pob.)	Bgy
Tanza	Bgy
Tugos	Bgy
Tumagabok	Bgy
Tumapon	Bgy
Isok I (Pob.)	Bgy
Buenavista	Mun
Bagacay	Bgy
Bagtingon	Bgy
Bicas-bicas	Bgy
Caigangan	Bgy
Daykitin	Bgy
Libas	Bgy
Malbog	Bgy
Sihi	Bgy
Timbo	Bgy
Tungib-Lipata	Bgy
Yook	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Gasan	Mun
Antipolo	Bgy
Bachao Ibaba	Bgy
Bachao Ilaya	Bgy
Bacongbacong	Bgy
Bahi	Bgy
Bangbang	Bgy
Banot	Bgy
Banuyo	Bgy
Bognuyan	Bgy
Cabugao	Bgy
Dawis	Bgy
Dili	Bgy
Libtangin	Bgy
Mahunig	Bgy
Mangiliol	Bgy
Masiga	Bgy
Matandang Gasan	Bgy
Pangi	Bgy
Pingan	Bgy
Tabionan	Bgy
Tapuyan	Bgy
Tiguion	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Mogpog	Mun
Anapog-Sibucao	Bgy
Argao	Bgy
Balanacan	Bgy
Banto	Bgy
Bintakay	Bgy
Bocboc	Bgy
Butansapa	Bgy
Candahon	Bgy
Capayang	Bgy
Danao	Bgy
Dulong Bayan (Pob.)	Bgy
Gitnang Bayan (Pob.)	Bgy
Guisian	Bgy
Hinadharan	Bgy
Hinanggayon	Bgy
Ino	Bgy
Janagdong	Bgy
Lamesa	Bgy
Laon	Bgy
Magapua	Bgy
Malayak	Bgy
Malusak	Bgy
Mampaitan	Bgy
Mangyan-Mababad	Bgy
Market Site (Pob.)	Bgy
Mataas Na Bayan	Bgy
Mendez	Bgy
Nangka I	Bgy
Nangka II	Bgy
Paye	Bgy
Pili	Bgy
Puting Buhangin	Bgy
Sayao	Bgy
Silangan	Bgy
Sumangga	Bgy
Tarug	Bgy
Villa Mendez (Pob.)	Bgy
Santa Cruz	Mun
Alobo	Bgy
Angas	Bgy
Aturan	Bgy
Bagong Silang Pob.	Bgy
Baguidbirin	Bgy
Baliis	Bgy
Balogo	Bgy
Banahaw Pob.	Bgy
Bangcuangan	Bgy
Banogbog	Bgy
Biga	Bgy
Botilao	Bgy
Buyabod	Bgy
Dating Bayan	Bgy
Devilla	Bgy
Dolores	Bgy
Haguimit	Bgy
Hupi	Bgy
Ipil	Bgy
Jolo	Bgy
Kaganhao	Bgy
Kalangkang	Bgy
Kamandugan	Bgy
Kasily	Bgy
Kilo-kilo	Bgy
Kiñaman	Bgy
Labo	Bgy
Lamesa	Bgy
Landy	Bgy
Lapu-lapu Pob.	Bgy
Libjo	Bgy
Lipa	Bgy
Lusok	Bgy
Maharlika Pob.	Bgy
Makulapnit	Bgy
Maniwaya	Bgy
Manlibunan	Bgy
Masaguisi	Bgy
Masalukot	Bgy
Matalaba	Bgy
Mongpong	Bgy
Morales	Bgy
Napo	Bgy
Pag-Asa Pob.	Bgy
Pantayin	Bgy
Polo	Bgy
Pulong-Parang	Bgy
Punong	Bgy
San Antonio	Bgy
San Isidro	Bgy
Tagum	Bgy
Tamayo	Bgy
Tambangan	Bgy
Tawiran	Bgy
Taytay	Bgy
Torrijos	Mun
Bangwayin	Bgy
Bayakbakin	Bgy
Bolo	Bgy
Bonliw	Bgy
Buangan	Bgy
Cabuyo	Bgy
Cagpo	Bgy
Dampulan	Bgy
Kay Duke	Bgy
Mabuhay	Bgy
Makawayan	Bgy
Malibago	Bgy
Malinao	Bgy
Maranlig	Bgy
Marlangga	Bgy
Matuyatuya	Bgy
Nangka	Bgy
Pakaskasan	Bgy
Payanas	Bgy
Poblacion	Bgy
Poctoy	Bgy
Sibuyao	Bgy
Suha	Bgy
Talawan	Bgy
Tigwi	Bgy
Occidental Mindoro	Prov
Abra De Ilog	Mun
Balao	Bgy
Cabacao	Bgy
Lumangbayan	Bgy
Poblacion	Bgy
San Vicente	Bgy
Tibag	Bgy
Udalo	Bgy
Wawa	Bgy
Armado	Bgy
Santa Maria	Bgy
Calintaan	Mun
Concepcion	Bgy
Iriron	Bgy
Malpalon	Bgy
New Dagupan	Bgy
Poblacion	Bgy
Poypoy	Bgy
Tanyag	Bgy
Looc	Mun
Agkawayan	Bgy
Ambil	Bgy
Balikyas	Bgy
Bonbon (Pob.)	Bgy
Bulacan	Bgy
Burol	Bgy
Guitna (Pob.)	Bgy
Kanluran (Pob.)	Bgy
Talaotao	Bgy
Lubang	Mun
Binakas	Bgy
Cabra	Bgy
Maligaya	Bgy
Maliig	Bgy
Tagbac	Bgy
Tangal	Bgy
Tilik	Bgy
Vigo	Bgy
Surville (Pob.)	Bgy
Araw At Bituin (Pob.)	Bgy
Bagong Sikat (Pob.)	Bgy
Banaag At Pag-Asa (Pob.)	Bgy
Likas Ng Silangan (Pob.)	Bgy
Maginhawa (Pob.)	Bgy
Ninikat Ng Pag-Asa (Pob.)	Bgy
Paraiso (Pob.)	Bgy
Magsaysay	Mun
Alibog	Bgy
Caguray	Bgy
Calawag	Bgy
Gapasan	Bgy
Laste	Bgy
Lourdes	Bgy
Nicolas	Bgy
Paclolo	Bgy
Poblacion	Bgy
Purnaga	Bgy
Santa Teresa	Bgy
Sibalat	Bgy
Mamburao (Capital)	Mun
Balansay	Bgy
Fatima	Bgy
Payompon	Bgy
San Luis	Bgy
Talabaan	Bgy
Tangkalan	Bgy
Tayamaan	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Poblacion 6	Bgy
Poblacion 7	Bgy
Poblacion 8	Bgy
Paluan	Mun
Alipaoy	Bgy
Harrison	Bgy
Lumangbayan	Bgy
Mananao	Bgy
Marikit	Bgy
Mapalad Pob.	Bgy
Handang Tumulong Pob.	Bgy
Silahis Ng Pag-Asa Pob.	Bgy
Pag-Asa Ng Bayan Pob.	Bgy
Bagong Silang Pob.	Bgy
San Jose Pob.	Bgy
Tubili	Bgy
Rizal	Mun
Adela	Bgy
Aguas	Bgy
Magsikap	Bgy
Malawaan	Bgy
Pitogo	Bgy
Rizal	Bgy
Rumbang	Bgy
Salvacion	Bgy
San Pedro	Bgy
Santo Niño	Bgy
Manoot	Bgy
Sablayan	Mun
Batong Buhay	Bgy
Buenavista	Bgy
Burgos	Bgy
Claudio Salgado	Bgy
General Emilio Aguinaldo	Bgy
Ibud	Bgy
Ilvita	Bgy
Ligaya	Bgy
Poblacion	Bgy
Paetan	Bgy
Pag-Asa	Bgy
San Agustin	Bgy
San Francisco	Bgy
San Nicolas	Bgy
San Vicente	Bgy
Santa Lucia	Bgy
Santo Niño	Bgy
Tagumpay	Bgy
Victoria	Bgy
Lagnas	Bgy
Malisbong	Bgy
Tuban	Bgy
San Jose	Mun
Ambulong	Bgy
Ansiray	Bgy
Bagong Sikat	Bgy
Bangkal	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Batasan	Bgy
Bayotbot	Bgy
Bubog	Bgy
Buri	Bgy
Camburay	Bgy
Caminawit	Bgy
Catayungan	Bgy
Central	Bgy
Iling Proper	Bgy
Inasakan	Bgy
Ipil	Bgy
La Curva	Bgy
Labangan Iling	Bgy
Labangan Poblacion	Bgy
Mabini	Bgy
Magbay	Bgy
Mangarin	Bgy
Mapaya	Bgy
Murtha	Bgy
Monte Claro	Bgy
Natandol	Bgy
Pag-Asa	Bgy
Pawican	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Roque	Bgy
Naibuan	Bgy
Santa Cruz	Mun
Alacaak	Bgy
Barahan	Bgy
Casague	Bgy
Dayap	Bgy
Lumangbayan	Bgy
Mulawin	Bgy
Pinagturilan	Bgy
Poblacion I	Bgy
San Vicente	Bgy
Poblacion II	Bgy
Kurtinganan	Bgy
Oriental Mindoro	Prov
Baco	Mun
Alag	Bgy
Bangkatan	Bgy
Burbuli	Bgy
Catwiran I	Bgy
Catwiran II	Bgy
Dulangan I	Bgy
Dulangan II	Bgy
Lumang Bayan	Bgy
Malapad	Bgy
Mangangan I	Bgy
Mangangan II	Bgy
Mayabig	Bgy
Pambisan	Bgy
Pulang-Tubig	Bgy
Putican-Cabulo	Bgy
San Andres	Bgy
San Ignacio	Bgy
Santa Cruz	Bgy
Santa Rosa I	Bgy
Santa Rosa II	Bgy
Tabon-tabon	Bgy
Tagumpay	Bgy
Water	Bgy
Baras	Bgy
Bayanan	Bgy
Lantuyang	Bgy
Poblacion	Bgy
Bansud	Mun
Alcadesma	Bgy
Bato	Bgy
Conrazon	Bgy
Malo	Bgy
Manihala	Bgy
Pag-Asa	Bgy
Poblacion	Bgy
Proper Bansud	Bgy
Rosacara	Bgy
Salcedo	Bgy
Sumagui	Bgy
Proper Tiguisan	Bgy
Villa Pag-Asa	Bgy
Bongabong	Mun
Anilao	Bgy
Batangan	Bgy
Bukal	Bgy
Camantigue	Bgy
Carmundo	Bgy
Cawayan	Bgy
Dayhagan	Bgy
Formon	Bgy
Hagan	Bgy
Hagupit	Bgy
Kaligtasan	Bgy
Labasan	Bgy
Labonan	Bgy
Libertad	Bgy
Lisap	Bgy
Luna	Bgy
Malitbog	Bgy
Mapang	Bgy
Masaguisi	Bgy
Morente	Bgy
Ogbot	Bgy
Orconuma	Bgy
Polusahi	Bgy
Sagana	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Sigange	Bgy
Tawas	Bgy
Poblacion	Bgy
Aplaya	Bgy
Bagumbayan I	Bgy
Bagumbayan II	Bgy
Ipil	Bgy
Mina de Oro	Bgy
Bulalacao	Mun
Bagong Sikat	Bgy
Balatasan	Bgy
Benli	Bgy
Cabugao	Bgy
Cambunang (Pob.)	Bgy
Campaasan (Pob.)	Bgy
Maasin	Bgy
Maujao	Bgy
Milagrosa	Bgy
Nasukob (Pob.)	Bgy
Poblacion	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Juan	Bgy
San Roque 	Bgy
City of Calapan (Capital)	City
Balingayan	Bgy
Balite	Bgy
Baruyan	Bgy
Batino	Bgy
Bayanan I	Bgy
Bayanan II	Bgy
Biga	Bgy
Bondoc	Bgy
Bucayao	Bgy
Buhuan	Bgy
Bulusan	Bgy
Santa Rita	Bgy
Calero (Pob.)	Bgy
Camansihan	Bgy
Camilmil	Bgy
Canubing I	Bgy
Canubing II	Bgy
Comunal	Bgy
Guinobatan	Bgy
Gulod	Bgy
Gutad	Bgy
Ibaba East (Pob.)	Bgy
Ibaba West (Pob.)	Bgy
Ilaya (Pob.)	Bgy
Lalud	Bgy
Lazareto	Bgy
Libis (Pob.)	Bgy
Lumang Bayan	Bgy
Mahal Na Pangalan	Bgy
Maidlang	Bgy
Malad	Bgy
Malamig	Bgy
Managpi	Bgy
Masipit	Bgy
Nag-Iba I	Bgy
Navotas	Bgy
Pachoca	Bgy
Palhi	Bgy
Panggalaan	Bgy
Parang	Bgy
Patas	Bgy
Personas	Bgy
Putingtubig	Bgy
Salong	Bgy
San Antonio	Bgy
San Vicente Central (Pob.)	Bgy
San Vicente East (Pob.)	Bgy
San Vicente North (Pob.)	Bgy
San Vicente South (Pob.)	Bgy
San Vicente West (Pob.)	Bgy
Santa Cruz	Bgy
Santa Isabel	Bgy
Santo Niño	Bgy
Sapul	Bgy
Silonay	Bgy
Santa Maria Village	Bgy
Suqui	Bgy
Tawagan	Bgy
Tawiran	Bgy
Tibag	Bgy
Wawa	Bgy
Nag-Iba II	Bgy
Gloria	Mun
Agsalin	Bgy
Agos	Bgy
Andres Bonifacio	Bgy
Balete	Bgy
Banus	Bgy
Banutan	Bgy
Buong Lupa	Bgy
Bulaklakan	Bgy
Gaudencio Antonino	Bgy
Guimbonan	Bgy
Kawit	Bgy
Lucio Laurel	Bgy
Macario Adriatico	Bgy
Malamig	Bgy
Malayong	Bgy
Maligaya (Pob.)	Bgy
Malubay	Bgy
Manguyang	Bgy
Maragooc	Bgy
Mirayan	Bgy
Narra	Bgy
Papandungin	Bgy
San Antonio	Bgy
Santa Maria	Bgy
Santa Theresa	Bgy
Tambong	Bgy
Alma Villa	Bgy
Mansalay	Mun
B. Del Mundo	Bgy
Balugo	Bgy
Bonbon	Bgy
Budburan	Bgy
Cabalwa	Bgy
Don Pedro	Bgy
Maliwanag	Bgy
Manaul	Bgy
Panaytayan	Bgy
Poblacion	Bgy
Roma	Bgy
Santa Brigida	Bgy
Santa Maria	Bgy
Villa Celestial	Bgy
Wasig	Bgy
Santa Teresita	Bgy
Waygan	Bgy
Naujan	Mun
Adrialuna	Bgy
Antipolo	Bgy
Apitong	Bgy
Arangin	Bgy
Aurora	Bgy
Bacungan	Bgy
Bagong Buhay	Bgy
Bancuro	Bgy
Barcenaga	Bgy
Bayani	Bgy
Buhangin	Bgy
Concepcion	Bgy
Dao	Bgy
Del Pilar	Bgy
Estrella	Bgy
Evangelista	Bgy
Gamao	Bgy
General Esco	Bgy
Herrera	Bgy
Inarawan	Bgy
Kalinisan	Bgy
Laguna	Bgy
Mabini	Bgy
Andres Ilagan	Bgy
Mahabang Parang	Bgy
Malaya	Bgy
Malinao	Bgy
Malvar	Bgy
Masagana	Bgy
Masaguing	Bgy
Melgar A	Bgy
Metolza	Bgy
Montelago	Bgy
Montemayor	Bgy
Motoderazo	Bgy
Mulawin	Bgy
Nag-Iba I	Bgy
Nag-Iba II	Bgy
Pagkakaisa	Bgy
Paniquian	Bgy
Pinagsabangan I	Bgy
Pinagsabangan II	Bgy
Piñahan	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Sampaguita	Bgy
San Agustin I	Bgy
San Agustin II	Bgy
San Andres	Bgy
San Antonio	Bgy
San Carlos	Bgy
San Isidro	Bgy
San Jose	Bgy
San Luis	Bgy
San Nicolas	Bgy
San Pedro	Bgy
Santa Isabel	Bgy
Santa Maria	Bgy
Santiago	Bgy
Santo Niño	Bgy
Tagumpay	Bgy
Tigkan	Bgy
Melgar B	Bgy
Santa Cruz	Bgy
Balite	Bgy
Banuton	Bgy
Caburo	Bgy
Magtibay	Bgy
Paitan	Bgy
Pinamalayan	Mun
Anoling	Bgy
Bacungan	Bgy
Bangbang	Bgy
Banilad	Bgy
Buli	Bgy
Cacawan	Bgy
Calingag	Bgy
Del Razon	Bgy
Inclanay	Bgy
Lumangbayan	Bgy
Malaya	Bgy
Maliangcog	Bgy
Maningcol	Bgy
Marayos	Bgy
Marfrancisco	Bgy
Nabuslot	Bgy
Pagalagala	Bgy
Palayan	Bgy
Pambisan Malaki	Bgy
Pambisan Munti	Bgy
Panggulayan	Bgy
Papandayan	Bgy
Pili	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Quinabigan	Bgy
Ranzo	Bgy
Rosario	Bgy
Sabang	Bgy
Santa Isabel	Bgy
Santa Maria	Bgy
Santa Rita	Bgy
Wawa	Bgy
Zone I (Pob.)	Bgy
Santo Niño	Bgy
Guinhawa	Bgy
Pola	Mun
Bacawan	Bgy
Bacungan	Bgy
Batuhan	Bgy
Bayanan	Bgy
Biga	Bgy
Buhay Na Tubig	Bgy
Calubasanhon	Bgy
Calima	Bgy
Casiligan	Bgy
Malibago	Bgy
Maluanluan	Bgy
Matulatula	Bgy
Pahilahan	Bgy
Panikihan	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Pula	Bgy
Puting Cacao	Bgy
Tagbakin	Bgy
Tagumpay	Bgy
Tiguihan	Bgy
Campamento	Bgy
Misong	Bgy
Puerto Galera	Mun
Aninuan	Bgy
Balatero	Bgy
Dulangan	Bgy
Palangan	Bgy
Sabang	Bgy
San Antonio	Bgy
San Isidro	Bgy
Santo Niño	Bgy
Sinandigan	Bgy
Tabinay	Bgy
Villaflor	Bgy
Poblacion	Bgy
Baclayan	Bgy
Roxas	Mun
Bagumbayan (Pob.)	Bgy
Cantil	Bgy
Dangay	Bgy
Happy Valley	Bgy
Libertad	Bgy
Libtong	Bgy
Mabuhay	Bgy
Maraska	Bgy
Odiong	Bgy
Paclasan (Pob.)	Bgy
San Aquilino	Bgy
San Isidro	Bgy
San Jose	Bgy
San Mariano	Bgy
San Miguel	Bgy
San Rafael	Bgy
San Vicente	Bgy
Uyao	Bgy
Victoria	Bgy
Little Tanauan	Bgy
San Teodoro	Mun
Bigaan	Bgy
Calangatan	Bgy
Calsapa	Bgy
Ilag	Bgy
Lumangbayan	Bgy
Tacligan	Bgy
Poblacion	Bgy
Caagutayan	Bgy
Socorro	Mun
Bagsok	Bgy
Batong Dalig	Bgy
Bayuin	Bgy
Calocmoy	Bgy
Catiningan	Bgy
Villareal	Bgy
Fortuna	Bgy
Happy Valley	Bgy
Calubayan	Bgy
Leuteboro I	Bgy
Leuteboro II	Bgy
Mabuhay I	Bgy
Malugay	Bgy
Matungao	Bgy
Monteverde	Bgy
Pasi I	Bgy
Pasi II	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Santo Domingo	Bgy
Subaan	Bgy
Bugtong Na Tuog	Bgy
Mabuhay II	Bgy
Ma. Concepcion	Bgy
Victoria	Mun
Alcate	Bgy
Babangonan	Bgy
Bagong Silang	Bgy
Bagong Buhay	Bgy
Bambanin	Bgy
Bethel	Bgy
Canaan	Bgy
Concepcion	Bgy
Duongan	Bgy
Loyal	Bgy
Mabini	Bgy
Macatoc	Bgy
Malabo	Bgy
Merit	Bgy
Ordovilla	Bgy
Pakyas	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Sampaguita	Bgy
San Antonio	Bgy
San Gabriel	Bgy
San Gelacio	Bgy
San Isidro	Bgy
San Juan	Bgy
San Narciso	Bgy
Urdaneta	Bgy
Villa Cerveza	Bgy
Jose Leido Jr.	Bgy
San Cristobal	Bgy
Antonino	Bgy
Palawan	Prov
Aborlan	Mun
Apo-Aporawan	Bgy
Apoc-apoc	Bgy
Aporawan	Bgy
Barake	Bgy
Cabigaan	Bgy
Gogognan	Bgy
Iraan	Bgy
Isaub	Bgy
Jose Rizal	Bgy
Mabini	Bgy
Magbabadil	Bgy
Plaridel	Bgy
Ramon Magsaysay	Bgy
Sagpangan	Bgy
San Juan	Bgy
Tagpait	Bgy
Tigman	Bgy
Poblacion	Bgy
Culandanum	Bgy
Agutaya	Mun
Algeciras	Bgy
Concepcion	Bgy
Diit	Bgy
Maracanao	Bgy
Matarawis	Bgy
Abagat (Pob.)	Bgy
Bangcal (Pob.)	Bgy
Cambian (Pob.)	Bgy
Villafria	Bgy
Villasol	Bgy
Araceli	Mun
Balogo	Bgy
Dagman	Bgy
Dalayawon	Bgy
Lumacad	Bgy
Madoldolon	Bgy
Mauringuen	Bgy
Osmeña	Bgy
San Jose De Oro	Bgy
Santo Niño	Bgy
Taloto	Bgy
Tinintinan	Bgy
Tudela	Bgy
Poblacion	Bgy
Balabac	Mun
Agutayan	Bgy
Bugsuk	Bgy
Bancalaan	Bgy
Indalawan	Bgy
Catagupan	Bgy
Malaking Ilog	Bgy
Mangsee	Bgy
Melville	Bgy
Pandanan	Bgy
Pasig	Bgy
Rabor	Bgy
Ramos	Bgy
Salang	Bgy
Sebaring	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
Poblacion VI	Bgy
Bataraza	Mun
Bono-bono	Bgy
Bulalacao	Bgy
Buliluyan	Bgy
Culandanum	Bgy
Igang-igang	Bgy
Inogbong	Bgy
Iwahig	Bgy
Malihud	Bgy
Malitub	Bgy
Marangas (Pob.)	Bgy
Ocayan	Bgy
Puring	Bgy
Rio Tuba	Bgy
Sandoval	Bgy
Sapa	Bgy
Sarong	Bgy
Sumbiling	Bgy
Tabud	Bgy
Tagnato	Bgy
Tagolango	Bgy
Taratak	Bgy
Tarusan	Bgy
Brooke'S Point	Mun
Amas	Bgy
Aribungos	Bgy
Barong-barong	Bgy
Calasaguen	Bgy
Imulnod	Bgy
Ipilan	Bgy
Maasin	Bgy
Mainit	Bgy
Malis	Bgy
Mambalot	Bgy
Oring-oring	Bgy
Pangobilian	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Salogon	Bgy
Samareñana	Bgy
Saraza	Bgy
Tubtub	Bgy
Busuanga	Mun
Bogtong	Bgy
Buluang	Bgy
Cheey	Bgy
Concepcion	Bgy
Maglalambay	Bgy
New Busuanga (Pob.)	Bgy
Old Busuanga	Bgy
Panlaitan	Bgy
Quezon	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Rafael	Bgy
Santo Niño	Bgy
Cagayancillo	Mun
Bantayan (Pob.)	Bgy
Calsada (Pob.)	Bgy
Convento (Pob.)	Bgy
Lipot North (Pob.)	Bgy
Lipot South (Pob.)	Bgy
Magsaysay	Bgy
Mampio	Bgy
Nusa	Bgy
Santa Cruz	Bgy
Tacas (Pob.)	Bgy
Talaga	Bgy
Wahig (Pob.)	Bgy
Coron	Mun
Banuang Daan	Bgy
Bintuan	Bgy
Borac	Bgy
Buenavista	Bgy
Bulalacao	Bgy
Cabugao	Bgy
Decabobo	Bgy
Decalachao	Bgy
Guadalupe	Bgy
Lajala	Bgy
Malawig	Bgy
Marcilla	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
San Jose	Bgy
San Nicolas	Bgy
Tagumpay	Bgy
Tara	Bgy
Turda	Bgy
Barangay VI (Pob.)	Bgy
Cuyo	Mun
Balading	Bgy
Bangcal (Pob.)	Bgy
Cabigsing (Pob.)	Bgy
Caburian	Bgy
Caponayan	Bgy
Catadman (Pob.)	Bgy
Funda	Bgy
Lagaoriao (Pob.)	Bgy
Lubid	Bgy
Manamoc	Bgy
Maringian	Bgy
Lungsod (Pob.)	Bgy
Pawa	Bgy
San Carlos	Bgy
Suba	Bgy
Tenga-tenga (Pob.)	Bgy
Tocadan (Pob.)	Bgy
Dumaran	Mun
Bacao	Bgy
Bohol	Bgy
Calasag	Bgy
Capayas	Bgy
Catep	Bgy
Culasian	Bgy
Danleg	Bgy
Dumaran (Pob.)	Bgy
Itangil	Bgy
Ilian	Bgy
Magsaysay	Bgy
San Juan	Bgy
Santa Teresita	Bgy
Santo Tomas	Bgy
Tanatanaon	Bgy
Santa Maria	Bgy
El Nido	Mun
Bagong Bayan	Bgy
Buena Suerte Pob.	Bgy
Barotuan	Bgy
Bebeladan	Bgy
Corong-corong Pob.	Bgy
Mabini	Bgy
Manlag	Bgy
Masagana Pob.	Bgy
New Ibajay	Bgy
Pasadeña	Bgy
Maligaya Pob.	Bgy
San Fernando	Bgy
Sibaltan	Bgy
Teneguiban	Bgy
Villa Libertad	Bgy
Villa Paz	Bgy
Bucana	Bgy
Aberawan	Bgy
Linapacan	Mun
Barangonan	Bgy
Cabunlawan	Bgy
Calibangbangan	Bgy
Decabaitot	Bgy
Maroyogroyog	Bgy
Nangalao	Bgy
New Culaylayan	Bgy
Pical	Bgy
San Miguel (Pob.)	Bgy
San Nicolas	Bgy
Magsaysay	Mun
Alcoba	Bgy
Balaguen	Bgy
Canipo	Bgy
Cocoro	Bgy
Danawan (Pob.)	Bgy
Emilod	Bgy
Igabas	Bgy
Lacaren	Bgy
Los Angeles	Bgy
Lucbuan	Bgy
Rizal	Bgy
Narra	Mun
Antipuluan	Bgy
Aramaywan	Bgy
Batang-batang	Bgy
Bato-bato	Bgy
Burirao	Bgy
Caguisan	Bgy
Calategas	Bgy
Dumagueña	Bgy
Elvita	Bgy
Estrella Village	Bgy
Ipilan	Bgy
Malatgao	Bgy
Malinao	Bgy
Narra (Pob.)	Bgy
Panacan	Bgy
Princess Urduja	Bgy
Sandoval	Bgy
Tacras	Bgy
Taritien	Bgy
Teresa	Bgy
Tinagong Dagat	Bgy
Bagong Sikat	Bgy
Panacan 2	Bgy
City of Puerto Princesa (Capital)	City
Babuyan	Bgy
Bacungan	Bgy
Bagong Bayan	Bgy
Bagong Pag-Asa (Pob.)	Bgy
Bagong Sikat (Pob.)	Bgy
Bagong Silang (Pob.)	Bgy
Bahile	Bgy
Bancao-bancao	Bgy
Binduyan	Bgy
Buenavista	Bgy
Cabayugan	Bgy
Concepcion	Bgy
Inagawan	Bgy
Irawan	Bgy
Iwahig (Pob.)	Bgy
Kalipay (Pob.)	Bgy
Kamuning	Bgy
Langogan	Bgy
Liwanag (Pob.)	Bgy
Lucbuan	Bgy
Mabuhay (Pob.)	Bgy
Macarascas	Bgy
Magkakaibigan (Pob.)	Bgy
Maligaya (Pob.)	Bgy
Manalo	Bgy
Manggahan (Pob.)	Bgy
Maningning (Pob.)	Bgy
Maoyon	Bgy
Marufinas	Bgy
Maruyogon	Bgy
Masigla (Pob.)	Bgy
Masikap (Pob.)	Bgy
Masipag (Pob.)	Bgy
Matahimik (Pob.)	Bgy
Matiyaga (Pob.)	Bgy
Maunlad (Pob.)	Bgy
Milagrosa (Pob.)	Bgy
Model (Pob.)	Bgy
Montible (Pob.)	Bgy
Napsan	Bgy
New Panggangan	Bgy
Pagkakaisa (Pob.)	Bgy
Princesa (Pob.)	Bgy
Salvacion	Bgy
San Jose	Bgy
San Miguel	Bgy
San Pedro	Bgy
San Rafael	Bgy
Santa Cruz	Bgy
Santa Lourdes	Bgy
Santa Lucia (Pob.)	Bgy
Santa Monica	Bgy
Seaside (Pob.)	Bgy
Sicsican	Bgy
Simpocan	Bgy
Tagabinit	Bgy
Tagburos	Bgy
Tagumpay (Pob.)	Bgy
Tanabag	Bgy
Tanglaw (Pob.)	Bgy
Barangay ng mga Mangingisda	Bgy
Inagawan Sub-Colony	Bgy
Luzviminda	Bgy
Mandaragat	Bgy
San Manuel	Bgy
Tiniguiban	Bgy
Quezon	Mun
Alfonso XIII (Pob.)	Bgy
Aramaywan	Bgy
Berong	Bgy
Calumpang	Bgy
Isugod	Bgy
Quinlogan	Bgy
Maasin	Bgy
Panitian	Bgy
Pinaglabanan	Bgy
Sowangan	Bgy
Tabon	Bgy
Calatagbak	Bgy
Malatgao	Bgy
Tagusao	Bgy
Roxas	Mun
Abaroan	Bgy
Antonino	Bgy
Bagong Bayan	Bgy
Caramay	Bgy
Dumarao	Bgy
Iraan	Bgy
Jolo	Bgy
Magara	Bgy
Malcampo	Bgy
Mendoza	Bgy
Narra	Bgy
New Barbacan	Bgy
New Cuyo	Bgy
Barangay 1 (Pob.)	Bgy
Rizal	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Nicolas	Bgy
Sandoval	Bgy
Tagumpay	Bgy
Taradungan	Bgy
Tinitian	Bgy
Tumarbong	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V Pob.	Bgy
Barangay VI Pob.	Bgy
Nicanor Zabala	Bgy
San Vicente	Mun
Alimanguan	Bgy
Binga	Bgy
Caruray	Bgy
Kemdeng	Bgy
New Agutaya	Bgy
New Canipo	Bgy
Port Barton	Bgy
Poblacion	Bgy
San Isidro	Bgy
Santo Niño	Bgy
Taytay	Mun
Abongan	Bgy
Banbanan	Bgy
Bantulan	Bgy
Batas	Bgy
Bato	Bgy
Beton	Bgy
Busy Bees	Bgy
Calawag	Bgy
Casian	Bgy
Cataban	Bgy
Debangan	Bgy
Depla	Bgy
Liminangcong	Bgy
Meytegued	Bgy
New Guinlo	Bgy
Old Guinlo	Bgy
Pamantolon	Bgy
Pancol	Bgy
Paly	Bgy
Poblacion	Bgy
Pularaquen	Bgy
San Jose	Bgy
Sandoval	Bgy
Silanga	Bgy
Alacalian	Bgy
Baras	Bgy
Libertad	Bgy
Minapla	Bgy
Talog	Bgy
Tumbod	Bgy
Paglaum	Bgy
Kalayaan	Mun
Pag-Asa (Pob.)	Bgy
Culion	Mun
Balala	Bgy
Baldat	Bgy
Binudac	Bgy
Culango	Bgy
Galoc	Bgy
Jardin	Bgy
Libis	Bgy
Luac	Bgy
Malaking Patag	Bgy
Osmeña	Bgy
Tiza	Bgy
Burabod	Bgy
Halsey	Bgy
De Carabao	Bgy
Rizal	Mun
Bunog	Bgy
Campong Ulay	Bgy
Candawaga	Bgy
Canipaan	Bgy
Culasian	Bgy
Iraan	Bgy
Latud	Bgy
Panalingaan	Bgy
Punta Baja	Bgy
Ransang	Bgy
Taburi	Bgy
Sofronio Española	Mun
Abo-abo	Bgy
Iraray	Bgy
Isumbo	Bgy
Labog	Bgy
Panitian	Bgy
Pulot Center	Bgy
Pulot Interior	Bgy
Pulot Shore	Bgy
Punang	Bgy
Romblon	Prov
Alcantara	Mun
Bonlao	Bgy
Calagonsao	Bgy
Camili	Bgy
Camod-Om	Bgy
Madalag	Bgy
Poblacion	Bgy
San Isidro	Bgy
Tugdan	Bgy
Bagsik	Bgy
Gui-ob	Bgy
Lawan	Bgy
San Roque	Bgy
Banton	Mun
Balogo	Bgy
Banice	Bgy
Hambi-an	Bgy
Lagang	Bgy
Libtong	Bgy
Mainit	Bgy
Nabalay	Bgy
Nasunogan	Bgy
Poblacion	Bgy
Sibay	Bgy
Tan-Ag	Bgy
Toctoc	Bgy
Togbongan	Bgy
Togong	Bgy
Tungonan	Bgy
Tumalum	Bgy
Yabawon	Bgy
Cajidiocan	Mun
Alibagon	Bgy
Cambajao	Bgy
Cambalo	Bgy
Cambijang	Bgy
Cantagda	Bgy
Danao	Bgy
Gutivan	Bgy
Lico	Bgy
Lumbang Este	Bgy
Lumbang Weste	Bgy
Marigondon	Bgy
Poblacion	Bgy
Sugod	Bgy
Taguilos	Bgy
Calatrava	Mun
Balogo	Bgy
Linao	Bgy
Poblacion	Bgy
Pagsangahan	Bgy
Pangulo	Bgy
San Roque	Bgy
Talisay	Bgy
Concepcion	Mun
Bachawan	Bgy
Calabasahan	Bgy
Dalajican	Bgy
Masudsud	Bgy
Poblacion	Bgy
Sampong	Bgy
San Pedro	Bgy
San Vicente	Bgy
Masadya	Bgy
Corcuera	Mun
Alegria	Bgy
Ambulong	Bgy
Colongcolong	Bgy
Gobon	Bgy
Guintiguiban	Bgy
Ilijan	Bgy
Labnig	Bgy
Mabini	Bgy
Mahaba	Bgy
Mangansag	Bgy
Poblacion	Bgy
San Agustin	Bgy
San Roque	Bgy
San Vicente	Bgy
Tacasan	Bgy
Looc	Mun
Agojo	Bgy
Balatucan	Bgy
Buenavista	Bgy
Camandao	Bgy
Guinhayaan	Bgy
Limon Norte	Bgy
Limon Sur	Bgy
Manhac	Bgy
Pili	Bgy
Poblacion	Bgy
Punta	Bgy
Tuguis	Bgy
Magdiwang	Mun
Agsao	Bgy
Agutay	Bgy
Ambulong	Bgy
Dulangan	Bgy
Ipil	Bgy
Jao-asan	Bgy
Poblacion	Bgy
Silum	Bgy
Tampayan	Bgy
Odiongan	Mun
Amatong	Bgy
Anahao	Bgy
Bangon	Bgy
Batiano	Bgy
Budiong	Bgy
Canduyong	Bgy
Dapawan	Bgy
Gabawan	Bgy
Libertad	Bgy
Malilico	Bgy
Mayha	Bgy
Panique	Bgy
Pato-o	Bgy
Ligaya (Pob.)	Bgy
Liwanag (Pob.)	Bgy
Poctoy	Bgy
Progreso Este	Bgy
Progreso Weste	Bgy
Rizal	Bgy
Tabing Dagat (Pob.)	Bgy
Tabobo-an	Bgy
Tulay	Bgy
Tumingad	Bgy
Liwayway (Pob.)	Bgy
Tuburan	Bgy
Romblon (Capital)	Mun
Agbaluto	Bgy
Agpanabat	Bgy
Agbudia	Bgy
Agnaga	Bgy
Agnay	Bgy
Agnipa	Bgy
Agtongo	Bgy
Alad	Bgy
Bagacay	Bgy
Cajimos	Bgy
Calabogo	Bgy
Capaclan	Bgy
Ginablan	Bgy
Guimpingan	Bgy
Ilauran	Bgy
Lamao	Bgy
Li-o	Bgy
Logbon	Bgy
Lunas	Bgy
Lonos	Bgy
Macalas	Bgy
Mapula	Bgy
Cobrador	Bgy
Palje	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Sablayan	Bgy
Sawang	Bgy
Tambac	Bgy
San Agustin	Mun
Bachawan	Bgy
Binongahan	Bgy
Buli	Bgy
Cabolutan	Bgy
Cagbuaya	Bgy
Carmen	Bgy
Cawayan	Bgy
Doña Juana	Bgy
Dubduban	Bgy
Binugusan	Bgy
Lusong	Bgy
Mahabang Baybay	Bgy
Poblacion	Bgy
Sugod	Bgy
Camantaya	Bgy
San Andres	Mun
Agpudlos	Bgy
Calunacon	Bgy
Doña Trinidad	Bgy
Linawan	Bgy
Mabini	Bgy
Marigondon Norte	Bgy
Marigondon Sur	Bgy
Matutuna	Bgy
Pag-Alad	Bgy
Poblacion	Bgy
Tan-Agan	Bgy
Victoria	Bgy
Juncarlo	Bgy
San Fernando	Mun
Agtiwa	Bgy
Azarga	Bgy
Campalingo	Bgy
Canjalon	Bgy
España	Bgy
Mabini	Bgy
Mabulo	Bgy
Otod	Bgy
Panangcalan	Bgy
Pili	Bgy
Poblacion	Bgy
Taclobo	Bgy
San Jose	Mun
Busay	Bgy
Combot	Bgy
Lanas	Bgy
Pinamihagan	Bgy
Poblacion	Bgy
Santa Fe	Mun
Agmanic	Bgy
Canyayo	Bgy
Danao Norte	Bgy
Danao Sur	Bgy
Guinbirayan	Bgy
Guintigbasan	Bgy
Magsaysay	Bgy
Mat-i	Bgy
Pandan	Bgy
Poblacion	Bgy
Tabugon	Bgy
Ferrol	Mun
Agnonoc	Bgy
Bunsoran	Bgy
Claro M. Recto	Bgy
Poblacion	Bgy
Hinaguman	Bgy
Tubigon	Bgy
Santa Maria	Mun
Bonga	Bgy
Concepcion Norte (Pob.)	Bgy
Concepcion Sur	Bgy
Paroyhog	Bgy
Santo Niño	Bgy
San Isidro	Bgy
Region V (Bicol Region)	Reg
Albay	Prov
Bacacay	Mun
Baclayon	Bgy
Banao	Bgy
Bariw	Bgy
Basud	Bgy
Bayandong	Bgy
Bonga	Bgy
Buang	Bgy
Cabasan	Bgy
Cagbulacao	Bgy
Cagraray	Bgy
Cajogutan	Bgy
Cawayan	Bgy
Damacan	Bgy
Gubat Ilawod	Bgy
Gubat Iraya	Bgy
Hindi	Bgy
Igang	Bgy
Langaton	Bgy
Manaet	Bgy
Mapulang Daga	Bgy
Mataas	Bgy
Misibis	Bgy
Nahapunan	Bgy
Namanday	Bgy
Namantao	Bgy
Napao	Bgy
Panarayon	Bgy
Pigcobohan	Bgy
Pili Ilawod	Bgy
Pili Iraya	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Pongco	Bgy
Busdac	Bgy
San Pablo	Bgy
San Pedro	Bgy
Sogod	Bgy
Sula	Bgy
Tambilagao	Bgy
Tambongon	Bgy
Tanagan	Bgy
Uson	Bgy
Vinisitahan-Basud	Bgy
Vinisitahan-Napao	Bgy
Camalig	Mun
Anoling	Bgy
Baligang	Bgy
Bantonan	Bgy
Bariw	Bgy
Binanderahan	Bgy
Binitayan	Bgy
Bongabong	Bgy
Cabagñan	Bgy
Cabraran Pequeño	Bgy
Calabidongan	Bgy
Comun	Bgy
Cotmon	Bgy
Del Rosario	Bgy
Gapo	Bgy
Gotob	Bgy
Ilawod	Bgy
Iluluan	Bgy
Libod	Bgy
Ligban	Bgy
Mabunga	Bgy
Magogon	Bgy
Manawan	Bgy
Maninila	Bgy
Mina	Bgy
Miti	Bgy
Palanog	Bgy
Panoypoy	Bgy
Pariaan	Bgy
Quinartilan	Bgy
Quirangay	Bgy
Quitinday	Bgy
Salugan	Bgy
Solong	Bgy
Sua	Bgy
Sumlang	Bgy
Tagaytay	Bgy
Tagoytoy	Bgy
Taladong	Bgy
Taloto	Bgy
Taplacon	Bgy
Tinago	Bgy
Tumpa	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Caguiba	Bgy
Daraga	Mun
Alcala	Bgy
Alobo	Bgy
Anislag	Bgy
Bagumbayan	Bgy
Balinad	Bgy
Bañadero	Bgy
Bañag	Bgy
Bascaran	Bgy
Bigao	Bgy
Binitayan	Bgy
Bongalon	Bgy
Budiao	Bgy
Burgos	Bgy
Busay	Bgy
Cullat	Bgy
Canarom	Bgy
Dela Paz	Bgy
Dinoronan	Bgy
Gabawan	Bgy
Gapo	Bgy
Ibaugan	Bgy
Ilawod Area Pob.	Bgy
Inarado	Bgy
Kidaco	Bgy
Kilicao	Bgy
Kimantong	Bgy
Kinawitan	Bgy
Kiwalo	Bgy
Lacag	Bgy
Mabini	Bgy
Malabog	Bgy
Malobago	Bgy
Maopi	Bgy
Market Area Pob. 	Bgy
Maroroy	Bgy
Matnog	Bgy
Mayon	Bgy
Mi-isi	Bgy
Nabasan	Bgy
Namantao	Bgy
Pandan	Bgy
Peñafrancia	Bgy
Sagpon	Bgy
Salvacion	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Roque	Bgy
San Vicente Grande	Bgy
San Vicente Pequeño	Bgy
Sipi	Bgy
Tabon-tabon	Bgy
Tagas	Bgy
Talahib	Bgy
Villahermosa	Bgy
Guinobatan	Mun
Agpay	Bgy
Balite	Bgy
Banao	Bgy
Batbat	Bgy
Binogsacan Lower	Bgy
Bololo	Bgy
Bubulusan	Bgy
Marcial O. Rañola	Bgy
Calzada	Bgy
Catomag	Bgy
Doña Mercedes	Bgy
Doña Tomasa	Bgy
Ilawod	Bgy
Inamnan Pequeño	Bgy
Inamnan Grande	Bgy
Inascan	Bgy
Iraya	Bgy
Lomacao	Bgy
Maguiron	Bgy
Maipon	Bgy
Malabnig	Bgy
Malipo	Bgy
Malobago	Bgy
Maninila	Bgy
Mapaco	Bgy
Masarawag	Bgy
Mauraro	Bgy
Minto	Bgy
Morera	Bgy
Muladbucad Pequeño	Bgy
Muladbucad Grande	Bgy
Ongo	Bgy
Palanas	Bgy
Poblacion	Bgy
Pood	Bgy
Quitago	Bgy
Quibongbongan	Bgy
San Francisco	Bgy
San Jose	Bgy
San Rafael	Bgy
Sinungtan	Bgy
Tandarora	Bgy
Travesia	Bgy
Binogsacan Upper	Bgy
Jovellar	Mun
Bagacay	Bgy
Rizal Pob.	Bgy
Mabini Pob.	Bgy
Plaza Pob.	Bgy
Magsaysay Pob	Bgy
Calzada Pob.	Bgy
Quitinday Pob.	Bgy
White Deer Pob.	Bgy
Bautista	Bgy
Cabraran	Bgy
Del Rosario	Bgy
Estrella	Bgy
Florista	Bgy
Mamlad	Bgy
Maogog	Bgy
Mercado Pob.	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Roque	Bgy
San Vicente	Bgy
Sinagaran	Bgy
Villa Paz	Bgy
Aurora Pob.	Bgy
City of Legazpi (Capital)	City
Bgy. 47 - Arimbay	Bgy
Bgy. 64 - Bagacay	Bgy
Bgy. 48 - Bagong Abre	Bgy
Bgy. 66 - Banquerohan	Bgy
Bgy. 1 - Em's Barrio (Pob.)	Bgy
Bgy. 11 - Maoyod Pob.	Bgy
Bgy. 12 - Tula-tula (Pob.)	Bgy
Bgy. 13 - Ilawod West Pob.	Bgy
Bgy. 14 - Ilawod Pob.	Bgy
Bgy. 15 - Ilawod East Pob.	Bgy
Bgy. 16 - Kawit-East Washington Drive (Pob.)	Bgy
Bgy. 17 - Rizal Street., Ilawod (Pob.)	Bgy
Bgy. 19 - Cabagñan	Bgy
Bgy. 2 - Em's Barrio South (Pob.)	Bgy
Bgy. 18 - Cabagñan West (Pob.)	Bgy
Bgy. 21 - Binanuahan West (Pob.)	Bgy
Bgy. 22 - Binanuahan East (Pob.)	Bgy
Bgy. 23 - Imperial Court Subd. (Pob.)	Bgy
Bgy. 20 - Cabagñan East (Pob.)	Bgy
Bgy. 25 - Lapu-lapu (Pob.)	Bgy
Bgy. 26 - Dinagaan (Pob.)	Bgy
Bgy. 27 - Victory Village South (Pob.)	Bgy
Bgy. 28 - Victory Village North (Pob.)	Bgy
Bgy. 29 - Sabang (Pob.)	Bgy
Bgy. 3 - Em's Barrio East (Pob.)	Bgy
Bgy. 36 - Kapantawan (Pob.)	Bgy
Bgy. 30 - Pigcale (Pob.)	Bgy
Bgy. 31 - Centro-Baybay (Pob.)	Bgy
Bgy. 33 - PNR-Peñaranda St.-Iraya (Pob.)	Bgy
Bgy. 34 - Oro Site-Magallanes St. (Pob.)	Bgy
Bgy. 35 - Tinago (Pob.)	Bgy
Bgy. 37 - Bitano (Pob.)	Bgy
Bgy. 39 - Bonot (Pob.)	Bgy
Bgy. 4 - Sagpon Pob.	Bgy
Bgy. 5 - Sagmin Pob.	Bgy
Bgy. 6 - Bañadero Pob.	Bgy
Bgy. 7 - Baño (Pob.)	Bgy
Bgy. 8 - Bagumbayan (Pob.)	Bgy
Bgy. 9 - Pinaric (Pob.)	Bgy
Bgy. 67 - Bariis	Bgy
Bgy. 49 - Bigaa	Bgy
Bgy. 41 - Bogtong	Bgy
Bgy. 53 - Bonga	Bgy
Bgy. 69 - Buenavista	Bgy
Bgy. 51 - Buyuan	Bgy
Bgy. 70 - Cagbacong	Bgy
Bgy. 40 - Cruzada	Bgy
Bgy. 57 - Dap-dap	Bgy
Bgy. 45 - Dita	Bgy
Bgy. 55 - Estanza	Bgy
Bgy. 38 - Gogon	Bgy
Bgy. 62 - Homapon	Bgy
Bgy. 65 - Imalnod	Bgy
Bgy. 54 - Mabinit	Bgy
Bgy. 63 - Mariawa	Bgy
Bgy. 61 - Maslog	Bgy
Bgy. 50 - Padang	Bgy
Bgy. 44 - Pawa	Bgy
Bgy. 59 - Puro	Bgy
Bgy. 42 - Rawis	Bgy
Bgy. 68 - San Francisco	Bgy
Bgy. 46 - San Joaquin	Bgy
Bgy. 32 - San Roque	Bgy
Bgy. 43 - Tamaoyan	Bgy
Bgy. 56 - Taysan	Bgy
Bgy. 52 - Matanag	Bgy
Bgy. 10 - Cabugao	Bgy
Bgy. 24 - Rizal Street	Bgy
Bgy. 58 - Buragwis	Bgy
Bgy. 60 - Lamba	Bgy
Libon	Mun
Alongong	Bgy
Apud	Bgy
Bacolod	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Zone V (Pob.)	Bgy
Zone VI (Pob.)	Bgy
Zone VII (Pob.)	Bgy
Bariw	Bgy
Bonbon	Bgy
Buga	Bgy
Bulusan	Bgy
Burabod	Bgy
Caguscos	Bgy
East Carisac	Bgy
West Carisac	Bgy
Harigue	Bgy
Libtong	Bgy
Linao	Bgy
Mabayawas	Bgy
Macabugos	Bgy
Magallang	Bgy
Malabiga	Bgy
Marayag	Bgy
Matara	Bgy
Molosbolos	Bgy
Natasan	Bgy
Nogpo	Bgy
Pantao	Bgy
Rawis	Bgy
Sagrada Familia	Bgy
Salvacion	Bgy
Sampongan	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Pascual	Bgy
San Ramon	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Niño Jesus	Bgy
Talin-talin	Bgy
Tambo	Bgy
Villa Petrona	Bgy
City of Ligao	City
Abella	Bgy
Allang	Bgy
Amtic	Bgy
Bacong	Bgy
Bagumbayan	Bgy
Balanac	Bgy
Baligang	Bgy
Barayong	Bgy
Basag	Bgy
Batang	Bgy
Bay	Bgy
Binanowan	Bgy
Binatagan (Pob.)	Bgy
Bobonsuran	Bgy
Bonga	Bgy
Busac	Bgy
Busay	Bgy
Cabarian	Bgy
Calzada (Pob.)	Bgy
Catburawan	Bgy
Cavasi	Bgy
Culliat	Bgy
Dunao	Bgy
Francia	Bgy
Guilid	Bgy
Herrera	Bgy
Layon	Bgy
Macalidong	Bgy
Mahaba	Bgy
Malama	Bgy
Maonon	Bgy
Nasisi	Bgy
Nabonton	Bgy
Oma-oma	Bgy
Palapas	Bgy
Pandan	Bgy
Paulba	Bgy
Paulog	Bgy
Pinamaniquian	Bgy
Pinit	Bgy
Ranao-ranao	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Tagpo	Bgy
Tambo	Bgy
Tandarura	Bgy
Tastas	Bgy
Tinago	Bgy
Tinampo	Bgy
Tiongson	Bgy
Tomolin	Bgy
Tuburan	Bgy
Tula-tula Grande	Bgy
Tula-tula Pequeño	Bgy
Tupas	Bgy
Malilipot	Mun
Binitayan	Bgy
Calbayog	Bgy
Canaway	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Salvacion	Bgy
San Antonio Santicon (Pob.)	Bgy
San Antonio Sulong	Bgy
San Francisco	Bgy
San Isidro Ilawod	Bgy
San Isidro Iraya	Bgy
San Jose	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santa Teresa	Bgy
Malinao	Mun
Awang	Bgy
Bagatangki	Bgy
Balading	Bgy
Balza	Bgy
Bariw	Bgy
Baybay	Bgy
Bulang	Bgy
Burabod	Bgy
Cabunturan	Bgy
Comun	Bgy
Diaro	Bgy
Estancia	Bgy
Jonop	Bgy
Labnig	Bgy
Libod	Bgy
Malolos	Bgy
Matalipni	Bgy
Ogob	Bgy
Pawa	Bgy
Payahan	Bgy
Poblacion	Bgy
Bagumbayan	Bgy
Quinarabasahan	Bgy
Santa Elena	Bgy
Soa	Bgy
Sugcad	Bgy
Tagoytoy	Bgy
Tanawan	Bgy
Tuliw	Bgy
Manito	Mun
Balabagon	Bgy
Balasbas	Bgy
Bamban	Bgy
Buyo	Bgy
Cabacongan	Bgy
Cabit	Bgy
Cawayan	Bgy
Cawit	Bgy
Holugan	Bgy
It-Ba (Pob.)	Bgy
Malobago	Bgy
Manumbalay	Bgy
Nagotgot	Bgy
Pawa	Bgy
Tinapian	Bgy
Oas	Mun
Badbad	Bgy
Badian	Bgy
Bagsa	Bgy
Bagumbayan	Bgy
Balogo	Bgy
Banao	Bgy
Bangiawon	Bgy
Bongoran	Bgy
Bogtong	Bgy
Busac	Bgy
Cadawag	Bgy
Cagmanaba	Bgy
Calaguimit	Bgy
Calpi	Bgy
Calzada	Bgy
Camagong	Bgy
Casinagan	Bgy
Centro Poblacion	Bgy
Coliat	Bgy
Del Rosario	Bgy
Gumabao	Bgy
Ilaor Norte	Bgy
Ilaor Sur	Bgy
Iraya Norte	Bgy
Iraya Sur	Bgy
Manga	Bgy
Maporong	Bgy
Maramba	Bgy
Moroponros	Bgy
Matambo	Bgy
Mayag	Bgy
Mayao	Bgy
Nagas	Bgy
San Pascual	Bgy
Obaliw-Rinas	Bgy
Pistola	Bgy
Ramay	Bgy
Rizal	Bgy
Saban	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Miguel	Bgy
San Ramon	Bgy
San Vicente	Bgy
Talisay	Bgy
Talongog	Bgy
Tapel	Bgy
Tobgon	Bgy
Tobog	Bgy
Tablon	Bgy
Pio Duran	Mun
Agol	Bgy
Alabangpuro	Bgy
Basicao Coastal	Bgy
Basicao Interior	Bgy
Banawan	Bgy
Binodegahan	Bgy
Buenavista	Bgy
Buyo	Bgy
Caratagan	Bgy
Cuyaoyao	Bgy
Flores	Bgy
Lawinon	Bgy
Macasitas	Bgy
Malapay	Bgy
Malidong	Bgy
Mamlad	Bgy
Marigondon	Bgy
Matanglad	Bgy
Nablangbulod	Bgy
Oringon	Bgy
Palapas	Bgy
Panganiran	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Rawis	Bgy
Salvacion	Bgy
Santo Cristo	Bgy
Sukip	Bgy
Tibabo	Bgy
La Medalla	Bgy
Polangui	Mun
Agos	Bgy
Alnay	Bgy
Alomon	Bgy
Amoguis	Bgy
Anopol	Bgy
Apad	Bgy
Balaba	Bgy
Balangibang	Bgy
Balinad	Bgy
Basud	Bgy
Binagbangan	Bgy
Buyo	Bgy
Centro Occidental (Pob.)	Bgy
Centro Oriental (Pob.)	Bgy
Cepres	Bgy
Cotmon	Bgy
Cotnogan	Bgy
Danao	Bgy
Gabon	Bgy
Gamot	Bgy
Itaran	Bgy
Kinale	Bgy
Kinuartilan	Bgy
La Medalla	Bgy
La Purisima	Bgy
Lanigay	Bgy
Lidong	Bgy
Lourdes	Bgy
Magpanambo	Bgy
Magurang	Bgy
Matacon	Bgy
Maynaga	Bgy
Maysua	Bgy
Mendez	Bgy
Napo	Bgy
Pinagdapugan	Bgy
Ponso	Bgy
Salvacion	Bgy
San Roque	Bgy
Santicon	Bgy
Santa Cruz	Bgy
Santa Teresita	Bgy
Sugcad	Bgy
Ubaliw	Bgy
Rapu-Rapu	Mun
Bagaobawan	Bgy
Batan	Bgy
Bilbao	Bgy
Binosawan	Bgy
Bogtong	Bgy
Buenavista	Bgy
Buhatan	Bgy
Calanaga	Bgy
Caracaran	Bgy
Carogcog	Bgy
Dap-dap	Bgy
Gaba	Bgy
Galicia	Bgy
Guadalupe	Bgy
Hamorawon	Bgy
Lagundi	Bgy
Liguan	Bgy
Linao	Bgy
Malobago	Bgy
Mananao	Bgy
Mancao	Bgy
Manila	Bgy
Masaga	Bgy
Morocborocan	Bgy
Nagcalsot	Bgy
Pagcolbon	Bgy
Poblacion	Bgy
Sagrada	Bgy
San Ramon	Bgy
Santa Barbara	Bgy
Tinocawan	Bgy
Tinopan	Bgy
Viga	Bgy
Villahermosa	Bgy
Santo Domingo	Mun
Alimsog	Bgy
Buhatan	Bgy
Calayucay	Bgy
Fidel Surtida	Bgy
Lidong	Bgy
Bagong San Roque	Bgy
San Juan Pob.	Bgy
Santo Domingo Pob.	Bgy
San Pedro Pob.	Bgy
San Vicente Pob.	Bgy
San Rafael Pob.	Bgy
Del Rosario Pob.	Bgy
San Francisco Pob.	Bgy
Nagsiya Pob.	Bgy
Salvacion	Bgy
San Andres	Bgy
San Fernando	Bgy
San Isidro	Bgy
San Roque	Bgy
Santa Misericordia	Bgy
Santo Niño	Bgy
Market Site Pob.	Bgy
Pandayan Pob.	Bgy
City of Tabaco	City
Agnas	Bgy
Bacolod	Bgy
Bangkilingan	Bgy
Bantayan	Bgy
Baranghawon	Bgy
Basagan	Bgy
Basud (Pob.)	Bgy
Bogñabong	Bgy
Bombon (Pob.)	Bgy
Bonot	Bgy
San Isidro	Bgy
Buang	Bgy
Buhian	Bgy
Cabagñan	Bgy
Cobo	Bgy
Comon	Bgy
Cormidal	Bgy
Divino Rostro (Pob.)	Bgy
Fatima	Bgy
Guinobat	Bgy
Hacienda	Bgy
Magapo	Bgy
Mariroc	Bgy
Matagbac	Bgy
Oras	Bgy
Oson	Bgy
Panal	Bgy
Pawa	Bgy
Pinagbobong	Bgy
Quinale Cabasan (Pob.)	Bgy
Quinastillojan	Bgy
Rawis	Bgy
Sagurong	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Carlos	Bgy
San Juan (Pob.)	Bgy
San Lorenzo	Bgy
San Ramon	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Cristo (Pob.)	Bgy
Sua-Igot	Bgy
Tabiguian	Bgy
Tagas	Bgy
Tayhi (Pob.)	Bgy
Visita	Bgy
Tiwi	Mun
Bagumbayan	Bgy
Bariis	Bgy
Baybay	Bgy
Belen	Bgy
Biyong	Bgy
Bolo	Bgy
Cale	Bgy
Cararayan	Bgy
Coro-coro	Bgy
Dap-dap	Bgy
Gajo	Bgy
Joroan	Bgy
Libjo	Bgy
Libtong	Bgy
Matalibong	Bgy
Maynonong	Bgy
Mayong	Bgy
Misibis	Bgy
Naga	Bgy
Nagas	Bgy
Oyama	Bgy
Putsan	Bgy
San Bernardo	Bgy
Sogod	Bgy
Tigbi (Pob.)	Bgy
Camarines Norte	Prov
Basud	Mun
Angas	Bgy
Bactas	Bgy
Binatagan	Bgy
Caayunan	Bgy
Guinatungan	Bgy
Hinampacan	Bgy
Langa	Bgy
Laniton	Bgy
Lidong	Bgy
Mampili	Bgy
Mandazo	Bgy
Mangcamagong	Bgy
Manmuntay	Bgy
Mantugawe	Bgy
Matnog	Bgy
Mocong	Bgy
Oliva	Bgy
Pagsangahan	Bgy
Pinagwarasan	Bgy
Plaridel	Bgy
Poblacion 1	Bgy
San Felipe	Bgy
San Jose	Bgy
San Pascual	Bgy
Taba-taba	Bgy
Tacad	Bgy
Taisan	Bgy
Tuaca	Bgy
Poblacion 2	Bgy
Capalonga	Mun
Alayao	Bgy
Binawangan	Bgy
Calabaca	Bgy
Camagsaan	Bgy
Catabaguangan	Bgy
Catioan	Bgy
Del Pilar	Bgy
Itok	Bgy
Lucbanan	Bgy
Mabini	Bgy
Mactang	Bgy
Mataque	Bgy
Old Camp	Bgy
Poblacion	Bgy
Magsaysay	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Roque	Bgy
Tanawan	Bgy
Ubang	Bgy
Villa Aurora	Bgy
Villa Belen	Bgy
Daet (Capital)	Mun
Alawihao	Bgy
Awitan	Bgy
Bagasbas	Bgy
Bibirao	Bgy
Borabod	Bgy
Calasgasan	Bgy
Camambugan	Bgy
Cobangbang	Bgy
Dogongan	Bgy
Gahonon	Bgy
Gubat	Bgy
Lag-On	Bgy
Magang	Bgy
Mambalite	Bgy
Mancruz	Bgy
Pamorangon	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Barangay VII (Pob.)	Bgy
Barangay VIII (Pob.)	Bgy
San Isidro	Bgy
San Lorenzo Ruiz	Mun
Daculang Bolo	Bgy
Dagotdotan	Bgy
Langga	Bgy
Laniton	Bgy
Maisog	Bgy
Mampurog	Bgy
Manlimonsito	Bgy
Matacong (Pob.)	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Ramon	Bgy
Jose Panganiban	Mun
Bagong Bayan	Bgy
Calero	Bgy
Dahican	Bgy
Dayhagan	Bgy
Larap	Bgy
Luklukan Norte	Bgy
Luklukan Sur	Bgy
Motherlode	Bgy
Nakalaya	Bgy
Osmeña	Bgy
Pag-Asa	Bgy
Parang	Bgy
Plaridel	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Jose	Bgy
San Martin	Bgy
San Pedro	Bgy
San Rafael	Bgy
Santa Cruz	Bgy
Santa Elena	Bgy
Santa Milagrosa	Bgy
Santa Rosa Norte	Bgy
Santa Rosa Sur	Bgy
Tamisan	Bgy
Labo	Mun
Anameam	Bgy
Awitan	Bgy
Baay	Bgy
Bagacay	Bgy
Bagong Silang I	Bgy
Bagong Silang II	Bgy
Bakiad	Bgy
Bautista	Bgy
Bayabas	Bgy
Bayan-bayan	Bgy
Benit	Bgy
Anahaw (Pob.)	Bgy
Gumamela (Pob.)	Bgy
San Francisco (Pob.)	Bgy
Kalamunding (Pob.)	Bgy
Bulhao	Bgy
Cabatuhan	Bgy
Cabusay	Bgy
Calabasa	Bgy
Canapawan	Bgy
Daguit	Bgy
Dalas	Bgy
Dumagmang	Bgy
Exciban	Bgy
Fundado	Bgy
Guinacutan	Bgy
Guisican	Bgy
Iberica	Bgy
Lugui	Bgy
Mabilo I	Bgy
Mabilo II	Bgy
Macogon	Bgy
Mahawan-hawan	Bgy
Malangcao-Basud	Bgy
Malasugui	Bgy
Malatap	Bgy
Malaya	Bgy
Malibago	Bgy
Maot	Bgy
Masalong	Bgy
Matanlang	Bgy
Napaod	Bgy
Pag-Asa	Bgy
Pangpang	Bgy
Pinya (Pob.)	Bgy
San Antonio	Bgy
Santa Cruz	Bgy
Bagong Silang III	Bgy
Submakin	Bgy
Talobatib	Bgy
Tigbinan	Bgy
Tulay Na Lupa	Bgy
Mercedes	Mun
Apuao	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Barangay VII (Pob.)	Bgy
Caringo	Bgy
Catandunganon	Bgy
Cayucyucan	Bgy
Colasi	Bgy
Del Rosario	Bgy
Gaboc	Bgy
Hamoraon	Bgy
Hinipaan	Bgy
Lalawigan	Bgy
Lanot	Bgy
Mambungalon	Bgy
Manguisoc	Bgy
Masalongsalong	Bgy
Matoogtoog	Bgy
Pambuhan	Bgy
Quinapaguian	Bgy
San Roque	Bgy
Tarum	Bgy
Paracale	Mun
Awitan	Bgy
Bagumbayan	Bgy
Bakal	Bgy
Batobalani	Bgy
Calaburnay	Bgy
Capacuan	Bgy
Casalugan	Bgy
Dagang	Bgy
Dalnac	Bgy
Dancalan	Bgy
Gumaus	Bgy
Labnig	Bgy
Macolabo Island	Bgy
Malacbang	Bgy
Malaguit	Bgy
Mampungo	Bgy
Mangkasay	Bgy
Maybato	Bgy
Palanas	Bgy
Pinagbirayan Malaki	Bgy
Pinagbirayan Munti	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Tabas	Bgy
Talusan	Bgy
Tawig	Bgy
Tugos	Bgy
San Vicente	Mun
Asdum	Bgy
Cabanbanan	Bgy
Calabagas	Bgy
Fabrica	Bgy
Iraya Sur	Bgy
Man-Ogob	Bgy
Poblacion District I	Bgy
Poblacion District II	Bgy
San Jose	Bgy
Santa Elena	Mun
Basiad	Bgy
Bulala	Bgy
Polungguitguit	Bgy
Rizal	Bgy
Salvacion	Bgy
San Lorenzo	Bgy
San Pedro	Bgy
San Vicente	Bgy
Santa Elena (Pob.)	Bgy
Villa San Isidro	Bgy
Don Tomas	Bgy
Guitol	Bgy
Kabuluan	Bgy
Kagtalaba	Bgy
Maulawin	Bgy
Patag Ibaba	Bgy
Patag Iraya	Bgy
Plaridel	Bgy
Tabugon	Bgy
Talisay	Mun
Binanuaan	Bgy
Caawigan	Bgy
Cahabaan	Bgy
Calintaan	Bgy
Del Carmen	Bgy
Gabon	Bgy
Itomang	Bgy
Poblacion	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Nicolas	Bgy
Santa Cruz	Bgy
Santa Elena	Bgy
Santo Niño	Bgy
Vinzons	Mun
Aguit-It	Bgy
Banocboc	Bgy
Cagbalogo	Bgy
Calangcawan Norte	Bgy
Calangcawan Sur	Bgy
Guinacutan	Bgy
Mangcayo	Bgy
Mangcawayan	Bgy
Manlucugan	Bgy
Matango	Bgy
Napilihan	Bgy
Pinagtigasan	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Sabang	Bgy
Santo Domingo	Bgy
Singi	Bgy
Sula	Bgy
Camarines Sur	Prov
Baao	Mun
Agdangan Pob.	Bgy
Antipolo	Bgy
Bagumbayan	Bgy
Caranday	Bgy
Cristo Rey	Bgy
Del Pilar	Bgy
Del Rosario (Pob.)	Bgy
Iyagan	Bgy
La Medalla	Bgy
Lourdes	Bgy
Nababarera	Bgy
Sagrada	Bgy
Salvacion	Bgy
Buluang	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
San Jose (Pob.)	Bgy
San Juan	Bgy
San Nicolas (Pob.)	Bgy
San Rafael	Bgy
Pugay	Bgy
San Ramon (Pob.)	Bgy
San Roque (Pob.)	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santa Eulalia	Bgy
Santa Isabel	Bgy
Santa Teresa	Bgy
Santa Teresita	Bgy
Tapol	Bgy
Balatan	Mun
Cabanbanan	Bgy
Cabungan	Bgy
Camangahan	Bgy
Cayogcog	Bgy
Coguit	Bgy
Duran	Bgy
Laganac	Bgy
Luluasan	Bgy
Montenegro	Bgy
Pararao	Bgy
Siramag (Pob.)	Bgy
Pulang Daga	Bgy
Sagrada Nacacale	Bgy
San Francisco	Bgy
Santiago Nacacale	Bgy
Tapayas	Bgy
Tomatarayo	Bgy
Bato	Mun
Agos	Bgy
Bacolod	Bgy
Buluang	Bgy
Caricot	Bgy
Cawacagan	Bgy
Cotmon	Bgy
Cristo Rey	Bgy
Del Rosario	Bgy
Divina Pastora (Pob.)	Bgy
Goyudan	Bgy
Lobong	Bgy
Lubigan	Bgy
Mainit	Bgy
Manga	Bgy
Masoli	Bgy
Neighborhood	Bgy
Niño Jesus	Bgy
Pagatpatan	Bgy
Palo	Bgy
Payak	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Isidro (Pob.)	Bgy
San Juan	Bgy
San Miguel	Bgy
San Rafael (Pob.)	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santiago (Pob.)	Bgy
Sooc	Bgy
Tagpolo	Bgy
Tres Reyes (Pob.)	Bgy
Bombon	Mun
San Antonio	Bgy
San Francisco	Bgy
San Isidro (Pob.)	Bgy
San Jose (Pob.)	Bgy
Pagao	Bgy
San Roque (Pob.)	Bgy
Siembre	Bgy
Santo Domingo	Bgy
Buhi	Mun
Antipolo	Bgy
Burocbusoc	Bgy
Cabatuan	Bgy
Cagmaslog	Bgy
Amlongan	Bgy
De La Fe	Bgy
Divino Rostro	Bgy
Ipil	Bgy
Gabas	Bgy
Ibayugan	Bgy
Igbac	Bgy
Iraya	Bgy
Labawon	Bgy
Delos Angeles	Bgy
Monte Calvario	Bgy
Namurabod	Bgy
Sagrada Familia	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Buenaventura (Pob.)	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose Baybayon	Bgy
San Jose Salay	Bgy
Macaangay	Bgy
San Pascual (Pob.)	Bgy
San Pedro (Pob.)	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Roque (Pob.)	Bgy
San Vicente	Bgy
Santa Clara (Pob.)	Bgy
Santa Cruz	Bgy
Santa Elena (Pob.)	Bgy
Santa Isabel	Bgy
Santa Justina	Bgy
Lourdes	Bgy
Tambo	Bgy
Bula	Mun
Bagoladio	Bgy
Bagumbayan	Bgy
Balaogan	Bgy
Caorasan	Bgy
Casugad	Bgy
Causip	Bgy
Fabrica	Bgy
Inoyonan	Bgy
Itangon	Bgy
Kinalabasahan	Bgy
La Purisima	Bgy
La Victoria	Bgy
Lanipga	Bgy
Lubgan	Bgy
Ombao Heights	Bgy
Ombao Polpog	Bgy
Palsong	Bgy
Panoypoyan	Bgy
Pawili	Bgy
Sagrada	Bgy
Salvacion (Pob.)	Bgy
San Agustin	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Ramon	Bgy
San Roque (Pob.)	Bgy
San Roque Heights	Bgy
Santa Elena	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Taisan	Bgy
Cabusao	Mun
Barcelonita	Bgy
Biong	Bgy
Camagong	Bgy
Castillo	Bgy
New Poblacion	Bgy
Pandan	Bgy
San Pedro	Bgy
Santa Cruz	Bgy
Santa Lutgarda (Pob.)	Bgy
Calabanga	Mun
Balatasan	Bgy
Balombon	Bgy
Balongay	Bgy
Belen	Bgy
Bigaas	Bgy
Binanuaanan Grande	Bgy
Binanuaanan Pequeño	Bgy
Binaliw	Bgy
Bonot-Santa Rosa	Bgy
Burabod	Bgy
Cabanbanan	Bgy
Cagsao	Bgy
Camuning	Bgy
Comaguingking	Bgy
Del Carmen (Pob.)	Bgy
Dominorog	Bgy
Fabrica	Bgy
Harobay	Bgy
La Purisima	Bgy
Lugsad	Bgy
Manguiring	Bgy
Pagatpat	Bgy
Paolbo	Bgy
Pinada	Bgy
Punta Tarawal	Bgy
Quinale	Bgy
Sabang	Bgy
Salvacion-Baybay	Bgy
San Antonio Poblacion	Bgy
San Antonio	Bgy
San Bernardino	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
San Lucas	Bgy
San Miguel (Pob.)	Bgy
San Pablo (Pob.)	Bgy
San Roque	Bgy
San Vicente (Pob.)	Bgy
Santa Cruz Ratay	Bgy
Santa Cruz Poblacion	Bgy
Santa Isabel (Pob.)	Bgy
Santa Salud (Pob.)	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Siba-o	Bgy
Sibobo	Bgy
Sogod	Bgy
Tomagodtod	Bgy
Camaligan	Mun
Dugcal	Bgy
Marupit	Bgy
San Francisco	Bgy
San Juan-San Ramon (Pob.)	Bgy
San Lucas (Pob.)	Bgy
San Marcos (Pob.)	Bgy
San Mateo (Pob.)	Bgy
San Jose-San Pablo (Pob.)	Bgy
San Roque	Bgy
Santo Domingo (Pob.)	Bgy
Santo Tomas (Pob.)	Bgy
Sua	Bgy
Tarosanan	Bgy
Canaman	Mun
Baras (Pob.)	Bgy
Del Rosario	Bgy
Dinaga (Pob.)	Bgy
Fundado	Bgy
Haring	Bgy
Iquin	Bgy
Linaga	Bgy
Mangayawan	Bgy
Palo	Bgy
Pangpang (Pob.)	Bgy
Poro	Bgy
San Agustin	Bgy
San Francisco	Bgy
San Jose East	Bgy
San Jose West	Bgy
San Juan	Bgy
San Nicolas	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santa Teresita	Bgy
Sua	Bgy
Talidtid	Bgy
Tibgao (Pob.)	Bgy
Caramoan	Mun
Agaas	Bgy
Antolon	Bgy
Bacgong	Bgy
Bahay	Bgy
Bikal	Bgy
Binanuahan (Pob.)	Bgy
Cabacongan	Bgy
Cadong	Bgy
Colongcogong	Bgy
Canatuan	Bgy
Caputatan	Bgy
Gogon	Bgy
Daraga	Bgy
Gata	Bgy
Gibgos	Bgy
Guijalo	Bgy
Hanopol	Bgy
Hanoy	Bgy
Haponan	Bgy
Ilawod	Bgy
Ili-Centro (Pob.)	Bgy
Lidong	Bgy
Lubas	Bgy
Malabog	Bgy
Maligaya	Bgy
Mampirao	Bgy
Mandiclum	Bgy
Maqueda	Bgy
Minalaba	Bgy
Oring	Bgy
Oroc-Osoc	Bgy
Pagolinan	Bgy
Pandanan	Bgy
Paniman	Bgy
Patag-Belen	Bgy
Pili-Centro	Bgy
Pili-Tabiguian	Bgy
Poloan	Bgy
Salvacion	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Solnopan	Bgy
Tabgon	Bgy
Tabiguian	Bgy
Tabog	Bgy
Tawog (Pob.)	Bgy
Toboan	Bgy
Terogo	Bgy
Del Gallego	Mun
Bagong Silang	Bgy
Bucal	Bgy
Cabasag	Bgy
Comadaycaday	Bgy
Domagondong	Bgy
Kinalangan	Bgy
Comadogcadog	Bgy
Mabini	Bgy
Magais I	Bgy
Magais II	Bgy
Mansalaya	Bgy
Nagkalit	Bgy
Palaspas	Bgy
Pamplona	Bgy
Pasay	Bgy
Pinagdapian	Bgy
Pinugusan	Bgy
Zone I Fatima (Pob.)	Bgy
Zone II San Antonio (Pob.)	Bgy
Poblacion Zone III	Bgy
Sabang	Bgy
Salvacion	Bgy
San Juan	Bgy
San Pablo	Bgy
Santa Rita I	Bgy
Santa Rita II	Bgy
Sinagawsawan	Bgy
Sinuknipan I	Bgy
Sinuknipan II	Bgy
Sugsugin	Bgy
Tabion	Bgy
Tomagoktok	Bgy
Gainza	Mun
Cagbunga	Bgy
Dahilig	Bgy
Loob	Bgy
Malbong	Bgy
Namuat	Bgy
Sampaloc	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
Garchitorena	Mun
Ason	Bgy
Bahi	Bgy
Binagasbasan	Bgy
Burabod	Bgy
Cagamutan	Bgy
Cagnipa	Bgy
Canlong	Bgy
Dangla	Bgy
Del Pilar	Bgy
Denrica	Bgy
Harrison	Bgy
Mansangat	Bgy
Pambuhan	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Vicente	Bgy
Sumaoy	Bgy
Tamiawon	Bgy
Toytoy	Bgy
Goa	Mun
Abucayan	Bgy
Bagumbayan Grande (Pob.)	Bgy
Bagumbayan Pequeño (Pob.)	Bgy
Balaynan	Bgy
Belen (Pob.)	Bgy
Buyo	Bgy
Cagaycay	Bgy
Catagbacan	Bgy
Digdigon	Bgy
Gimaga	Bgy
Halawig-Gogon	Bgy
Hiwacloy	Bgy
La Purisima (Pob.)	Bgy
Lamon	Bgy
Matacla	Bgy
Maymatan	Bgy
Maysalay	Bgy
Napawon	Bgy
Panday (Pob.)	Bgy
Payatan	Bgy
Pinaglabanan	Bgy
Salog	Bgy
San Benito (Pob.)	Bgy
San Isidro West	Bgy
San Isidro (Pob.)	Bgy
San Jose (Pob.)	Bgy
San Juan Bautista (Pob.)	Bgy
San Juan Evangelista (Pob.)	Bgy
San Pedro	Bgy
Scout Fuentebella	Bgy
Tabgon	Bgy
Tagongtong	Bgy
Tamban	Bgy
Taytay	Bgy
City of Iriga	City
Antipolo	Bgy
Cristo Rey	Bgy
Del Rosario	Bgy
Francia	Bgy
La Anunciacion	Bgy
La Medalla	Bgy
La Purisima	Bgy
La Trinidad	Bgy
Niño Jesus	Bgy
Perpetual Help	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Agustin	Bgy
San Andres	Bgy
San Antonio	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Roque (Pob.)	Bgy
San Vicente Norte	Bgy
San Vicente Sur	Bgy
Santa Cruz Norte	Bgy
Santa Cruz Sur	Bgy
Santa Elena	Bgy
Santa Isabel	Bgy
Santa Maria	Bgy
Santa Teresita	Bgy
Santiago	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Lagonoy	Mun
Agosais	Bgy
Agpo-Camagong-Tabog	Bgy
Amoguis	Bgy
Bocogan	Bgy
Balaton	Bgy
Binanuahan	Bgy
Burabod	Bgy
Cabotonan	Bgy
Dahat	Bgy
Del Carmen	Bgy
Ginorangan	Bgy
Gimagtocon	Bgy
Gubat	Bgy
Guibahoy	Bgy
Himanag	Bgy
Kinahologan	Bgy
Loho	Bgy
Manamoc	Bgy
Mangogon	Bgy
Mapid	Bgy
Olas	Bgy
Omalo	Bgy
Panagan	Bgy
Panicuan	Bgy
Pinamihagan	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
San Isidro Sur (Pob.)	Bgy
San Isidro Norte (Pob.)	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Roque	Bgy
San Sebastian	Bgy
San Vicente (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
Santa Maria (Pob.)	Bgy
Saripongpong (Pob.)	Bgy
Sipaco	Bgy
Libmanan	Mun
Aslong	Bgy
Awayan	Bgy
Bagacay	Bgy
Bagadion	Bgy
Bagamelon	Bgy
Bagumbayan	Bgy
Bahao	Bgy
Bahay	Bgy
Beguito Nuevo	Bgy
Beguito Viejo	Bgy
Begajo Norte	Bgy
Begajo Sur	Bgy
Bikal	Bgy
Busak	Bgy
Caima	Bgy
Calabnigan	Bgy
Camambugan	Bgy
Cambalidio	Bgy
Candami	Bgy
Candato	Bgy
Cawayan	Bgy
Concepcion	Bgy
Cuyapi	Bgy
Danawan	Bgy
Duang Niog	Bgy
Handong	Bgy
Ibid	Bgy
Inalahan	Bgy
Labao	Bgy
Libod I	Bgy
Libod II	Bgy
Loba-loba	Bgy
Mabini	Bgy
Malansad Nuevo	Bgy
Malansad Viejo	Bgy
Malbogon	Bgy
Malinao	Bgy
Mambalite	Bgy
Mambayawas	Bgy
Mambulo Nuevo	Bgy
Mambulo Viejo	Bgy
Mancawayan	Bgy
Mandacanan	Bgy
Mantalisay	Bgy
Padlos	Bgy
Pag-Oring Nuevo	Bgy
Pag-Oring Viejo	Bgy
Palangon	Bgy
Palong	Bgy
Patag	Bgy
Planza	Bgy
Poblacion	Bgy
Potot	Bgy
Puro-Batia	Bgy
Rongos	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Juan	Bgy
San Pablo	Bgy
San Vicente	Bgy
Sibujo	Bgy
Sigamot	Bgy
Station-Church Site	Bgy
Taban-Fundado	Bgy
Tampuhan	Bgy
Tanag	Bgy
Tarum	Bgy
Tinalmud Nuevo	Bgy
Tinalmud Viejo	Bgy
Tinangkihan	Bgy
Udoc	Bgy
Umalo	Bgy
Uson	Bgy
Villasocorro	Bgy
Villadima	Bgy
Lupi	Mun
Alleomar	Bgy
Bagangan Sr.	Bgy
Bagong Sikat	Bgy
Bel-Cruz	Bgy
Bangon	Bgy
Barrera Jr.	Bgy
Barrera Sr.	Bgy
Belwang	Bgy
Buenasuerte	Bgy
Bulawan Jr.	Bgy
Bulawan Sr.	Bgy
Cabutagan	Bgy
Kaibigan	Bgy
Casay	Bgy
Colacling	Bgy
Cristo Rey	Bgy
Del Carmen	Bgy
Haguimit	Bgy
Haluban	Bgy
La Purisima	Bgy
Lourdes	Bgy
Mangcawayan	Bgy
Napolidan	Bgy
Poblacion	Bgy
Polantuna	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Jose	Bgy
San Pedro	Bgy
San Rafael Norte	Bgy
San Rafael Sur	Bgy
San Ramon	Bgy
San Vicente	Bgy
Sooc	Bgy
Tanawan	Bgy
Tible	Bgy
Tapi	Bgy
Magarao	Mun
Barobaybay	Bgy
Bell (Pob.)	Bgy
Carangcang	Bgy
Carigsa	Bgy
Casuray	Bgy
Monserrat (Pob.)	Bgy
Ponong	Bgy
San Francisco (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Miguel	Bgy
San Pantaleon (Pob.)	Bgy
Santa Lucia (Pob.)	Bgy
Santa Rosa	Bgy
Santo Tomas (Pob.)	Bgy
Milaor	Mun
Alimbuyog	Bgy
Amparado (Pob.)	Bgy
Balagbag	Bgy
Borongborongan	Bgy
Cabugao	Bgy
Capucnasan	Bgy
Dalipay	Bgy
Del Rosario (Pob.)	Bgy
Flordeliz	Bgy
Lipot	Bgy
Mayaopayawan	Bgy
Maycatmon	Bgy
Maydaso	Bgy
San Antonio	Bgy
San Jose (Pob.)	Bgy
San Miguel (Pob.)	Bgy
San Roque (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santo Domingo (Pob.)	Bgy
Tarusanan	Bgy
Minalabac	Mun
Antipolo	Bgy
Bagolatao	Bgy
Bagongbong	Bgy
Baliuag Nuevo	Bgy
Baliuag Viejo	Bgy
Catanusan	Bgy
Del Carmen-Del Rosario (Pob.)	Bgy
Del Socorro	Bgy
Hamoraon	Bgy
Hobo	Bgy
Irayang Solong	Bgy
Magadap	Bgy
Malitbog	Bgy
Manapao	Bgy
Mataoroc	Bgy
Sagrada	Bgy
Salingogon	Bgy
San Antonio	Bgy
San Felipe-Santiago (Pob.)	Bgy
San Francisco (Pob.)	Bgy
San Jose	Bgy
San Juan-San Lorenzo (Pob.)	Bgy
Taban	Bgy
Tariric	Bgy
Timbang	Bgy
Nabua	Mun
Angustia	Bgy
Antipolo Old	Bgy
Antipolo Young	Bgy
Aro-aldao	Bgy
Bustrac	Bgy
Inapatan	Bgy
Dolorosa	Bgy
Duran	Bgy
La Purisima	Bgy
Lourdes Old	Bgy
Lourdes Young	Bgy
La Opinion	Bgy
Paloyon Proper	Bgy
Salvacion Que Gatos	Bgy
San Antonio (Pob.)	Bgy
San Antonio Ogbon	Bgy
San Esteban (Pob.)	Bgy
San Francisco (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Isidro Inapatan	Bgy
Malawag	Bgy
San Jose	Bgy
San Juan (Pob.)	Bgy
San Luis (Pob.)	Bgy
San Miguel (Pob.)	Bgy
San Nicolas (Pob.)	Bgy
San Roque (Pob.)	Bgy
San Roque Madawon	Bgy
San Roque Sagumay	Bgy
San Vicente Gorong-Gorong	Bgy
San Vicente Ogbon	Bgy
Santa Barbara	Bgy
Santa Cruz	Bgy
Santa Elena Baras	Bgy
Santa Lucia Baras	Bgy
Santiago Old	Bgy
Santiago Young	Bgy
Santo Domingo	Bgy
Tandaay	Bgy
Topas Proper	Bgy
Topas Sogod	Bgy
Paloyon Oriental	Bgy
City of Naga	City
Abella	Bgy
Bagumbayan Norte	Bgy
Bagumbayan Sur	Bgy
Balatas	Bgy
Calauag	Bgy
Cararayan	Bgy
Carolina	Bgy
Concepcion Grande	Bgy
Concepcion Pequeña	Bgy
Dayangdang	Bgy
Del Rosario	Bgy
Dinaga	Bgy
Igualdad Interior	Bgy
Lerma	Bgy
Liboton	Bgy
Mabolo	Bgy
Pacol	Bgy
Panicuason	Bgy
Peñafrancia	Bgy
Sabang	Bgy
San Felipe	Bgy
San Francisco (Pob.)	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Tabuco	Bgy
Tinago	Bgy
Triangulo	Bgy
Ocampo	Mun
Ayugan	Bgy
Cabariwan	Bgy
Cagmanaba	Bgy
Del Rosario	Bgy
Gatbo	Bgy
Guinaban	Bgy
Hanawan	Bgy
Hibago	Bgy
La Purisima Nuevo	Bgy
May-Ogob	Bgy
New Moriones	Bgy
Old Moriones	Bgy
Pinit	Bgy
Poblacion Central	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Jose Oras	Bgy
San Roque Commonal	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Villaflorida	Bgy
Pamplona	Mun
Batang	Bgy
Burabod	Bgy
Cagbibi	Bgy
Cagbunga	Bgy
Calawat	Bgy
Del Rosario	Bgy
Patong	Bgy
Poblacion	Bgy
Salvacion	Bgy
San Gabriel	Bgy
San Isidro	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Vicente	Bgy
Veneracion	Bgy
Tambo	Bgy
Tampadong	Bgy
Pasacao	Mun
Antipolo	Bgy
Bagong Silang	Bgy
Bahay	Bgy
Balogo	Bgy
Caranan	Bgy
Cuco	Bgy
Dalupaon	Bgy
Macad	Bgy
Hubo	Bgy
Itulan	Bgy
Odicon	Bgy
Quitang	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Cirilo (Pob.)	Bgy
Santa Rosa Del Norte (Pob.)	Bgy
Santa Rosa Del Sur (Pob.)	Bgy
Tilnac	Bgy
Tinalmud	Bgy
Pili (Capital)	Mun
Anayan	Bgy
Bagong Sirang	Bgy
Binanwaanan	Bgy
Binobong	Bgy
Cadlan	Bgy
Caroyroyan	Bgy
Curry	Bgy
Del Rosario	Bgy
Himaao	Bgy
La Purisima	Bgy
New San Roque	Bgy
Old San Roque (Pob.)	Bgy
Palestina	Bgy
Pawili	Bgy
Sagurong	Bgy
Sagrada	Bgy
San Agustin	Bgy
San Antonio (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Jose	Bgy
San Juan (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santiago (Pob.)	Bgy
Santo Niño	Bgy
Tagbong	Bgy
Tinangis	Bgy
Presentacion	Mun
Ayugao	Bgy
Bagong Sirang	Bgy
Baliguian	Bgy
Bantugan	Bgy
Bicalen	Bgy
Bitaogan	Bgy
Buenavista	Bgy
Bulalacao	Bgy
Cagnipa	Bgy
Lagha	Bgy
Lidong	Bgy
Liwacsa	Bgy
Maangas	Bgy
Pagsangahan	Bgy
Patrocinio	Bgy
Pili	Bgy
Santa Maria (Pob.)	Bgy
Tanawan	Bgy
Ragay	Mun
Agao-ao	Bgy
Agrupacion	Bgy
Amomokpok	Bgy
Apad	Bgy
Apale	Bgy
Banga Caves	Bgy
Baya	Bgy
Binahan Proper	Bgy
Binahan Upper	Bgy
Buenasuerte	Bgy
Cabadisan	Bgy
Cabinitan	Bgy
Cabugao	Bgy
Caditaan	Bgy
Cale	Bgy
Godofredo Reyes Sr.	Bgy
Catabangan Proper	Bgy
Inandawa	Bgy
Laguio	Bgy
Lanipga-Cawayan	Bgy
Liboro	Bgy
Lohong	Bgy
Lower Omon	Bgy
Lower Santa Cruz	Bgy
Panaytayan	Bgy
Panaytayan Nuevo	Bgy
Patalunan	Bgy
Poblacion Ilaod	Bgy
Poblacion Iraya	Bgy
Port Junction Norte	Bgy
Port Junction Sur	Bgy
Salvacion	Bgy
Samay	Bgy
San Rafael	Bgy
F. Simeon	Bgy
Tagbac	Bgy
Upper Omon	Bgy
Upper Santa Cruz	Bgy
Sagñay	Mun
Aniog	Bgy
Atulayan	Bgy
Bongalon	Bgy
Buracan	Bgy
Catalotoan	Bgy
Del Carmen (Pob.)	Bgy
Kilantaao	Bgy
Kilomaon	Bgy
Mabca	Bgy
Minadongjol	Bgy
Nato	Bgy
Patitinan	Bgy
San Antonio (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Roque (Pob.)	Bgy
Santo Niño	Bgy
Sibaguan	Bgy
Tinorongan	Bgy
Turague	Bgy
San Fernando	Mun
Alianza	Bgy
Beberon	Bgy
Bical	Bgy
Bocal	Bgy
Bonifacio (Pob.)	Bgy
Buenavista (Pob.)	Bgy
Calascagas	Bgy
Cotmo	Bgy
Daculang Tubig	Bgy
Del Pilar (Pob.)	Bgy
Gñaran	Bgy
Grijalvo	Bgy
Lupi	Bgy
Maragñi	Bgy
Pamukid	Bgy
Pinamasagan	Bgy
Pipian	Bgy
Planza	Bgy
Rizal (Pob.)	Bgy
San Joaquin	Bgy
Santa Cruz	Bgy
Tagpocol	Bgy
San Jose	Mun
Adiangao	Bgy
Bagacay	Bgy
Bahay	Bgy
Boclod	Bgy
Calalahan	Bgy
Calawit	Bgy
Camagong	Bgy
Catalotoan	Bgy
Danlog	Bgy
Del Carmen (Pob.)	Bgy
Dolo	Bgy
Kinalansan	Bgy
Mampirao	Bgy
Manzana	Bgy
Minoro	Bgy
Palale	Bgy
Ponglon	Bgy
Pugay	Bgy
Sabang	Bgy
Salogon	Bgy
San Antonio (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
Soledad (Pob.)	Bgy
Tagas	Bgy
Tambangan	Bgy
Telegrafo	Bgy
Tominawog	Bgy
Sipocot	Mun
Aldezar	Bgy
Alteza	Bgy
Anib	Bgy
Awayan	Bgy
Azucena	Bgy
Bagong Sirang	Bgy
Binahian	Bgy
Bolo Sur	Bgy
Bolo Norte	Bgy
Bulan	Bgy
Bulawan	Bgy
Cabuyao	Bgy
Caima	Bgy
Calagbangan	Bgy
Calampinay	Bgy
Carayrayan	Bgy
Cotmo	Bgy
Gabi	Bgy
Gaongan	Bgy
Impig	Bgy
Lipilip	Bgy
Lubigan Jr.	Bgy
Lubigan Sr.	Bgy
Malaguico	Bgy
Malubago	Bgy
Manangle	Bgy
Mangga	Bgy
Mangapo	Bgy
Manlubang	Bgy
Mantila	Bgy
North Centro (Pob.)	Bgy
North Villazar	Bgy
Sagrada Familia	Bgy
Salanda	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Vicente	Bgy
Serranzana	Bgy
South Centro (Pob.)	Bgy
South Villazar	Bgy
Taisan	Bgy
Tara	Bgy
Tible	Bgy
Tula-tula	Bgy
Vigaan	Bgy
Yabo	Bgy
Siruma	Mun
Bagong Sirang	Bgy
Bahao	Bgy
Boboan	Bgy
Butawanan	Bgy
Cabugao	Bgy
Fundado	Bgy
Homestead	Bgy
La Purisima	Bgy
Mabuhay	Bgy
Malaconini	Bgy
Nalayahan	Bgy
Matandang Siruma	Bgy
Pinitan	Bgy
Poblacion	Bgy
Pamintan-Bantilan	Bgy
Salvacion	Bgy
San Andres	Bgy
San Ramon	Bgy
Sulpa	Bgy
Tandoc	Bgy
Tongo-Bantigue	Bgy
Vito	Bgy
Tigaon	Mun
Abo	Bgy
Cabalinadan	Bgy
Caraycayon	Bgy
Casuna	Bgy
Consocep	Bgy
Coyaoyao	Bgy
Gaao	Bgy
Gingaroy	Bgy
Gubat	Bgy
Huyonhuyon	Bgy
Libod	Bgy
Mabalodbalod	Bgy
May-Anao	Bgy
Panagan	Bgy
Poblacion	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Miguel	Bgy
San Rafael	Bgy
Talojongon	Bgy
Tinawagan	Bgy
Vinagre	Bgy
Tinambac	Mun
Agay-Ayan	Bgy
Antipolo	Bgy
Bagacay	Bgy
Banga	Bgy
Bolaobalite	Bgy
Bani	Bgy
Bataan	Bgy
Binalay	Bgy
Buenavista	Bgy
Buyo	Bgy
Cagliliog	Bgy
Caloco	Bgy
Camagong	Bgy
Canayonan	Bgy
Cawaynan	Bgy
Daligan	Bgy
Filarca (Pob.)	Bgy
La Purisima (Pob.)	Bgy
Lupi	Bgy
Magsaysay	Bgy
Magtang	Bgy
Mananao	Bgy
La Medalla	Bgy
New Caaluan	Bgy
Olag Grande	Bgy
Olag Pequeño	Bgy
Old Caaluan	Bgy
Pag-Asa	Bgy
Pantat	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Isidro (Pob.)	Bgy
San Jose	Bgy
San Pascual (Pob.)	Bgy
San Ramon	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Sogod	Bgy
Tambang	Bgy
Tierra Nevada	Bgy
Union	Bgy
Salvacion Poblacion	Bgy
Catanduanes	Prov
Bagamanoc	Mun
Antipolo	Bgy
Bacak	Bgy
Bagatabao	Bgy
Bugao	Bgy
Cahan	Bgy
Hinipaan	Bgy
Magsaysay	Bgy
Poblacion	Bgy
Quigaray	Bgy
Quezon	Bgy
Sagrada	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Rafael	Bgy
San Vicente	Bgy
Santa Mesa	Bgy
Santa Teresa	Bgy
Suchan	Bgy
Baras	Mun
Abihao	Bgy
Agban	Bgy
Bagong Sirang	Bgy
Benticayan	Bgy
Buenavista	Bgy
Caragumihan	Bgy
Batolinao	Bgy
Danao	Bgy
Sagrada	Bgy
Ginitligan	Bgy
Guinsaanan	Bgy
J. M. Alberto	Bgy
Macutal	Bgy
Moning	Bgy
Nagbarorong	Bgy
Osmeña	Bgy
P. Teston	Bgy
Paniquihan	Bgy
Eastern Poblacion	Bgy
Puraran	Bgy
Putsan	Bgy
Quezon	Bgy
Rizal	Bgy
Salvacion	Bgy
San Lorenzo	Bgy
San Miguel	Bgy
Santa Maria	Bgy
Tilod	Bgy
Western Poblacion	Bgy
Bato	Mun
Aroyao Pequeño	Bgy
Bagumbayan	Bgy
Banawang	Bgy
Batalay	Bgy
Binanuahan	Bgy
Bote	Bgy
Buenavista	Bgy
Cabugao	Bgy
Cagraray	Bgy
Carorian	Bgy
Guinobatan	Bgy
Libjo	Bgy
Marinawa	Bgy
Mintay	Bgy
Oguis	Bgy
Pananaogan	Bgy
Libod Poblacion	Bgy
San Andres	Bgy
San Pedro	Bgy
San Roque	Bgy
Santa Isabel	Bgy
Sibacungan	Bgy
Sipi	Bgy
Talisay	Bgy
Tamburan	Bgy
Tilis	Bgy
Ilawod (Pob.)	Bgy
Caramoran	Mun
Baybay (Pob.)	Bgy
Bocon	Bgy
Bothoan (Pob.)	Bgy
Buenavista	Bgy
Bulalacao	Bgy
Camburo	Bgy
Dariao	Bgy
Datag East	Bgy
Datag West	Bgy
Guiamlong	Bgy
Hitoma	Bgy
Icanbato (Pob.)	Bgy
Inalmasinan	Bgy
Iyao	Bgy
Mabini	Bgy
Maui	Bgy
Maysuran	Bgy
Milaviga	Bgy
Panique	Bgy
Sabangan	Bgy
Sabloyon	Bgy
Salvacion	Bgy
Supang	Bgy
Toytoy (Pob.)	Bgy
Tubli	Bgy
Tucao	Bgy
Obi	Bgy
Gigmoto	Mun
Biong	Bgy
Dororian	Bgy
Poblacion District I	Bgy
Poblacion District II	Bgy
Poblacion District III	Bgy
San Pedro	Bgy
San Vicente	Bgy
Sicmil	Bgy
Sioron	Bgy
Pandan	Mun
Bagawang	Bgy
Balagñonan	Bgy
Baldoc	Bgy
Canlubi	Bgy
Santa Cruz	Bgy
Catamban	Bgy
Cobo	Bgy
Hiyop	Bgy
Libod (Pob.)	Bgy
Lourdes	Bgy
Lumabao	Bgy
Marambong	Bgy
Napo (Pob.)	Bgy
Oga	Bgy
Pandan Del Norte (Pob.)	Bgy
Pandan Del Sur (Pob.)	Bgy
Panuto	Bgy
Salvacion	Bgy
San Andres	Bgy
San Isidro	Bgy
Porot	Bgy
San Rafael	Bgy
San Roque	Bgy
Tabugoc	Bgy
Tokio	Bgy
Wagdas	Bgy
Panganiban	Mun
Alinawan	Bgy
Babaguan	Bgy
Bagong Bayan	Bgy
Burabod	Bgy
Cabuyoan	Bgy
Cagdarao	Bgy
Mabini	Bgy
Maculiw	Bgy
Panay	Bgy
Taopon	Bgy
Salvacion (Pob.)	Bgy
San Antonio	Bgy
San Joaquin (Pob.)	Bgy
San Jose (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Miguel	Bgy
San Nicolas (Pob.)	Bgy
San Pedro (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santa Ana (Pob.)	Bgy
Santa Maria (Pob.)	Bgy
Santo Santiago (Pob.)	Bgy
Tibo	Bgy
San Andres	Mun
Agojo	Bgy
Alibuag	Bgy
Asgad	Bgy
Bagong Sirang	Bgy
Barihay	Bgy
Batong Paloway	Bgy
Belmonte (Pob.)	Bgy
Bislig	Bgy
Bon-ot	Bgy
Cabungahan	Bgy
Cabcab	Bgy
Carangag	Bgy
Catagbacan	Bgy
Codon	Bgy
Comagaycay	Bgy
Datag	Bgy
Divino Rostro (Pob.)	Bgy
Esperanza (Pob.)	Bgy
Hilawan	Bgy
Lictin	Bgy
Lubas	Bgy
Manambrag	Bgy
Mayngaway	Bgy
Palawig	Bgy
Puting Baybay	Bgy
Rizal	Bgy
Salvacion (Pob.)	Bgy
San Isidro	Bgy
San Jose	Bgy
San Roque (Pob.)	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Sapang Palay (Pob.)	Bgy
Tibang	Bgy
Timbaan	Bgy
Tominawog	Bgy
Wagdas (Pob.)	Bgy
Yocti	Bgy
San Miguel	Mun
Balatohan	Bgy
Salvacion	Bgy
Boton	Bgy
Buhi	Bgy
Dayawa	Bgy
Atsan	Bgy
Poblacion District II	Bgy
Poblacion District III	Bgy
J. M. Alberto	Bgy
Katipunan	Bgy
Kilikilihan	Bgy
Mabato	Bgy
Obo	Bgy
Pacogon	Bgy
Pagsangahan	Bgy
Pangilao	Bgy
Paraiso	Bgy
Santa Elena	Bgy
Progreso	Bgy
San Juan	Bgy
San Marcos	Bgy
Siay	Bgy
Solong	Bgy
Tobrehon	Bgy
Viga	Mun
Almojuela	Bgy
Ananong	Bgy
Asuncion (Pob.)	Bgy
Batohonan	Bgy
Begonia	Bgy
Botinagan	Bgy
Buenavista	Bgy
Burgos	Bgy
Del Pilar	Bgy
Mabini	Bgy
Magsaysay	Bgy
Ogbong	Bgy
Osmeña	Bgy
Pedro Vera	Bgy
Peñafrancia (Pob.)	Bgy
Quezon	Bgy
Quirino	Bgy
Rizal	Bgy
Roxas	Bgy
Sagrada	Bgy
San Isidro (Pob.)	Bgy
San Jose Poblacion	Bgy
San Jose Oco	Bgy
San Pedro (Pob.)	Bgy
San Roque (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santa Rosa	Bgy
Soboc	Bgy
Tambongon	Bgy
Tinago	Bgy
Villa Aurora	Bgy
Virac (Capital)	Mun
Antipolo Del Norte	Bgy
Antipolo Del Sur	Bgy
Balite	Bgy
Batag	Bgy
Bigaa	Bgy
Buenavista	Bgy
Buyo	Bgy
Cabihian	Bgy
Calabnigan	Bgy
Calampong	Bgy
Calatagan Proper	Bgy
Calatagan Tibang	Bgy
Capilihan	Bgy
Casoocan	Bgy
Cavinitan	Bgy
Gogon Sirangan	Bgy
Concepcion (Pob.)	Bgy
Constantino (Pob.)	Bgy
Danicop	Bgy
Dugui San Vicente	Bgy
Dugui San Isidro	Bgy
Dugui Too	Bgy
F. Tacorda Village	Bgy
Francia (Pob.)	Bgy
Gogon Centro	Bgy
Hawan Grande	Bgy
Hawan Ilaya	Bgy
Hicming	Bgy
Igang	Bgy
Juan M. Alberto	Bgy
Lanao (Pob.)	Bgy
Magnesia Del Norte	Bgy
Magnesia Del Sur	Bgy
Marcelo Alberto (Pob.)	Bgy
Marilima	Bgy
Pajo Baguio	Bgy
Pajo San Isidro	Bgy
Palnab Del Norte	Bgy
Palnab Del Sur	Bgy
Palta Big	Bgy
Palta Salvacion	Bgy
Palta Small	Bgy
Rawis (Pob.)	Bgy
Salvacion	Bgy
San Isidro Village	Bgy
San Jose (Pob.)	Bgy
San Juan (Pob.)	Bgy
San Pablo (Pob.)	Bgy
San Pedro (Pob.)	Bgy
San Roque (Pob.)	Bgy
San Vicente	Bgy
Ibong Sapa	Bgy
Santa Cruz	Bgy
Santa Elena (Pob.)	Bgy
Santo Cristo	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Simamla	Bgy
Sogod-Simamla	Bgy
Talisoy	Bgy
Sogod-Tibgao	Bgy
Tubaon	Bgy
Valencia	Bgy
Masbate	Prov
Aroroy	Mun
Ambolong	Bgy
Amoroy	Bgy
Amotag	Bgy
Bagauma	Bgy
Balawing	Bgy
Balete	Bgy
Bangon	Bgy
Cabangcalan	Bgy
Cabas-An	Bgy
Calanay	Bgy
Capsay	Bgy
Concepcion	Bgy
Dayhagan	Bgy
Don Pablo Dela Rosa	Bgy
Gumahang	Bgy
Jaboyoan	Bgy
Lanang	Bgy
Luy-a	Bgy
Macabug	Bgy
Malubi	Bgy
Managanaga	Bgy
Manamoc	Bgy
Mariposa	Bgy
Mataba	Bgy
Matalangtalang	Bgy
Matongog	Bgy
Nabongsoran	Bgy
Pangle	Bgy
Panique	Bgy
Pinanaan	Bgy
Poblacion	Bgy
Puro	Bgy
San Agustin	Bgy
San Isidro	Bgy
Sawang	Bgy
Syndicate	Bgy
Talabaan	Bgy
Talib	Bgy
Tigbao	Bgy
Tinago	Bgy
Tinigban	Bgy
Baleno	Mun
Baao	Bgy
Banase	Bgy
Batuila	Bgy
Cagara	Bgy
Cagpandan	Bgy
Cancahorao	Bgy
Canjunday	Bgy
Docol	Bgy
Eastern Capsay	Bgy
Gabi	Bgy
Gangao	Bgy
Lagta	Bgy
Lahong Proper	Bgy
Lahong Interior	Bgy
Lipata	Bgy
Madangcalan	Bgy
Magdalena	Bgy
Manoboc	Bgy
Obongon Diot	Bgy
Poblacion	Bgy
Polot	Bgy
Potoson	Bgy
Sog-Ong	Bgy
Tinapian	Bgy
Balud	Mun
Baybay	Bgy
Bongcanaway	Bgy
Mabuhay	Bgy
Calumpang	Bgy
Cantil	Bgy
Casamongan	Bgy
Dao	Bgy
Danao	Bgy
Guinbanwahan	Bgy
Ilaya	Bgy
Jangan	Bgy
Jintotolo	Bgy
Mapili	Bgy
Mapitogo	Bgy
Pajo	Bgy
Palane	Bgy
Panguiranan	Bgy
Panubigan	Bgy
Poblacion	Bgy
Pulanduta	Bgy
Quinayangan Diotay	Bgy
Quinayangan Tonga	Bgy
Salvacion	Bgy
Sampad	Bgy
San Andres	Bgy
San Antonio	Bgy
Sapatos	Bgy
Talisay	Bgy
Tonga	Bgy
Ubo	Bgy
Victory	Bgy
Villa Alvarez	Bgy
Batuan	Mun
Burgos	Bgy
Canares	Bgy
Cambañez	Bgy
Costa Rica	Bgy
Danao	Bgy
Gibraltar	Bgy
Mabuhay	Bgy
Matabao	Bgy
Nasandig	Bgy
Panisihan	Bgy
Poblacion	Bgy
Rizal	Bgy
Royroy	Bgy
Sawang	Bgy
Cataingan	Mun
Abaca	Bgy
Aguada	Bgy
Badiang	Bgy
Bagumbayan	Bgy
Cadulawan	Bgy
Cagbatang	Bgy
Chimenea	Bgy
Concepcion	Bgy
Curvada	Bgy
Divisoria	Bgy
Domorog	Bgy
Estampar	Bgy
Gahit	Bgy
Libtong	Bgy
Liong	Bgy
Maanahao	Bgy
Madamba	Bgy
Malobago	Bgy
Matayum	Bgy
Matubinao	Bgy
Mintac	Bgy
Nadawisan	Bgy
Osmeña	Bgy
Pawican	Bgy
Pitogo	Bgy
Poblacion	Bgy
Quezon	Bgy
San Isidro	Bgy
San Jose	Bgy
San Pedro	Bgy
San Rafael	Bgy
Santa Teresita	Bgy
Santo Niño	Bgy
Tagboan	Bgy
Tuybo	Bgy
Villa Pogado	Bgy
Cawayan	Mun
Begia	Bgy
Cabayugan	Bgy
Cabungahan	Bgy
Calapayan	Bgy
Calumpang	Bgy
Dalipe	Bgy
Divisoria	Bgy
Guiom	Bgy
Gilotongan	Bgy
Itombato	Bgy
Libertad	Bgy
Looc	Bgy
Mactan	Bgy
Madbad	Bgy
R.M. Magbalon	Bgy
Mahayahay	Bgy
Maihao	Bgy
Malbug	Bgy
Naro	Bgy
Pananawan	Bgy
Poblacion	Bgy
Pulot	Bgy
Recodo	Bgy
San Jose	Bgy
San Vicente	Bgy
Taberna	Bgy
Talisay	Bgy
Tuburan	Bgy
Villahermosa	Bgy
Chico Island	Bgy
Lague-lague	Bgy
Palobandera	Bgy
Peña Island	Bgy
Pin-As	Bgy
Iraya	Bgy
Punta Batsan	Bgy
Tubog	Bgy
Claveria	Mun
Albasan	Bgy
Boca Engaño	Bgy
Buyo	Bgy
Calpi	Bgy
Canomay	Bgy
Cawayan	Bgy
Poblacion District I	Bgy
Poblacion District II	Bgy
Mababang Baybay	Bgy
Mabiton	Bgy
Manapao	Bgy
Nabasagan	Bgy
Nonoc	Bgy
Osmeña	Bgy
Pasig	Bgy
Peñafrancia	Bgy
Quezon	Bgy
San Isidro	Bgy
San Ramon	Bgy
San Vicente	Bgy
Taguilid	Bgy
Imelda	Bgy
Dimasalang	Mun
Balantay	Bgy
Balocawe	Bgy
Banahao	Bgy
Buenaflor	Bgy
Buracan	Bgy
Cabanoyoan	Bgy
Cabrera	Bgy
Cadulan	Bgy
Calabad	Bgy
Canomay	Bgy
Divisoria	Bgy
Gaid	Bgy
Gregorio Alino	Bgy
Magcaraguit	Bgy
Mambog	Bgy
Poblacion	Bgy
Rizal	Bgy
San Vicente	Bgy
Suba	Bgy
T.R. Yangco	Bgy
Esperanza	Mun
Agoho	Bgy
Almero	Bgy
Baras	Bgy
Domorog	Bgy
Guadalupe	Bgy
Iligan	Bgy
Labangtaytay	Bgy
Labrador	Bgy
Libertad	Bgy
Magsaysay	Bgy
Masbaranon	Bgy
Poblacion	Bgy
Potingbato	Bgy
Rizal	Bgy
San Roque	Bgy
Santiago	Bgy
Sorosimbajan	Bgy
Tawad	Bgy
Tunga	Bgy
Villa	Bgy
Mandaon	Mun
Alas	Bgy
Ayat	Bgy
Bat-Ongan	Bgy
Bugtong	Bgy
Buri	Bgy
Cabitan	Bgy
Cagmasoso	Bgy
Canomoy	Bgy
Centro	Bgy
Dayao	Bgy
Guincaiptan	Bgy
Lantangan	Bgy
Looc	Bgy
Mabatobato	Bgy
Maolingon	Bgy
Nailaban	Bgy
Nanipsan	Bgy
Pinamangcaan	Bgy
Poblacion	Bgy
Polo Dacu	Bgy
San Juan	Bgy
San Pablo	Bgy
Santa Fe	Bgy
Tagpu	Bgy
Tumalaytay	Bgy
Laguinbanwa	Bgy
City of Masbate (Capital)	City
Anas	Bgy
Asid	Bgy
B. Titong	Bgy
Bagumbayan	Bgy
Bantigue	Bgy
Bapor (Pob.)	Bgy
Batuhan	Bgy
Bayombon	Bgy
Biyong	Bgy
Bolo	Bgy
Cagay	Bgy
Cawayan Exterior	Bgy
Cawayan Interior	Bgy
Centro (Pob.)	Bgy
Espinosa	Bgy
F. Magallanes	Bgy
Ibingay	Bgy
Igang	Bgy
Kalipay (Pob.)	Bgy
Kinamaligan	Bgy
Malinta	Bgy
Mapiña	Bgy
Mayngaran	Bgy
Nursery	Bgy
Pating (Pob.)	Bgy
Pawa	Bgy
Sinalongan	Bgy
Tugbo	Bgy
Ubongan Dacu	Bgy
Usab	Bgy
Milagros	Mun
Bacolod	Bgy
Bangad	Bgy
Bara	Bgy
Bonbon	Bgy
Calasuche	Bgy
Calumpang	Bgy
Capaculan	Bgy
Cayabon	Bgy
Guinluthangan	Bgy
Jamorawon	Bgy
Magsalangi	Bgy
Matagbac	Bgy
Matanglad	Bgy
Matiporon	Bgy
Moises R. Espinosa	Bgy
Narangasan	Bgy
Pamangpangon	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Paraiso	Bgy
San Antonio	Bgy
San Carlos	Bgy
Sawmill	Bgy
Tagbon	Bgy
Tawad	Bgy
Tigbao	Bgy
Tinaclipan	Bgy
Mobo	Mun
Baang	Bgy
Bagacay	Bgy
Balatucan	Bgy
Barag	Bgy
Dacu	Bgy
Fabrica	Bgy
Guintorelan	Bgy
Holjogon	Bgy
Lalaguna	Bgy
Lomocloc	Bgy
Luyong Catungan	Bgy
Mabuhay	Bgy
Mandali	Bgy
Mapuyo	Bgy
Marintoc	Bgy
Nasunduan	Bgy
Pinamalatican	Bgy
Pinamarbuhan	Bgy
Poblacion Dist. I	Bgy
Poblacion Dist. II	Bgy
Polot	Bgy
Sambulawan	Bgy
Santa Maria	Bgy
Sawmill	Bgy
Tabuc	Bgy
Tugawe	Bgy
Tugbo	Bgy
Umabay Exterior	Bgy
Umabay Interior	Bgy
Monreal	Mun
Cantorna	Bgy
Famosa	Bgy
Macarthur	Bgy
Maglambong	Bgy
Morocborocan	Bgy
Poblacion	Bgy
Guinhadap	Bgy
Real	Bgy
Rizal	Bgy
Santo Niño	Bgy
Togoron	Bgy
Palanas	Mun
Antipolo	Bgy
Banco	Bgy
Biga-a	Bgy
Bontod	Bgy
Buenasuerte	Bgy
Intusan	Bgy
Jose A. Abenir Sr.	Bgy
Maanahao	Bgy
Mabini	Bgy
Malibas	Bgy
Maravilla	Bgy
Matugnao	Bgy
Miabas	Bgy
Nabangig	Bgy
Nipa	Bgy
Parina	Bgy
Piña	Bgy
Poblacion	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Carlos	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Malatawan	Bgy
Pio V. Corpuz	Mun
Alegria	Bgy
Buenasuerte	Bgy
Bugang	Bgy
Bugtong	Bgy
Bunducan	Bgy
Cabangrayan	Bgy
Calongongan	Bgy
Casabangan	Bgy
Guindawahan	Bgy
Labigan	Bgy
Lampuyang	Bgy
Mabuhay	Bgy
Palho	Bgy
Poblacion	Bgy
Salvacion	Bgy
Tanque	Bgy
Tubigan	Bgy
Tubog	Bgy
Placer	Mun
Aguada	Bgy
Ban-Ao	Bgy
Burabod	Bgy
Cabangcalan	Bgy
Calumpang	Bgy
Camayabsan	Bgy
Daanlungsod	Bgy
Dangpanan	Bgy
Daraga	Bgy
Guin-Awayan	Bgy
Guinhan-Ayan	Bgy
Katipunan	Bgy
Libas	Bgy
Locso-An	Bgy
Luna	Bgy
Mahayag	Bgy
Mahayahay	Bgy
Manlut-Od	Bgy
Matagantang	Bgy
Naboctot	Bgy
Nagarao	Bgy
Nainday	Bgy
Naocondiot	Bgy
Pasiagon	Bgy
Pili	Bgy
Poblacion	Bgy
Puro	Bgy
Quibrada	Bgy
San Marcos	Bgy
Santa Cruz	Bgy
Taboc	Bgy
Tan-Awan	Bgy
Taverna	Bgy
Tubod	Bgy
Villa Inocencio	Bgy
San Fernando	Mun
Altavista	Bgy
Benitinan	Bgy
Buenasuerte	Bgy
Buenavista	Bgy
Buenos Aires	Bgy
Buyo	Bgy
Cañelas	Bgy
Corbada	Bgy
Daplian	Bgy
Del Rosario	Bgy
Ipil	Bgy
Lahong	Bgy
Lumbia	Bgy
Magkaipit	Bgy
Minio	Bgy
Pinamoghaan	Bgy
Baybaydagat Pob.	Bgy
Silangan Pob.	Bgy
Magsasaka Pob.	Bgy
Bayanihan Pob.	Bgy
Progreso	Bgy
Resurreccion	Bgy
Salvacion	Bgy
Sowa	Bgy
Talisay	Bgy
Valparaiso	Bgy
San Jacinto	Mun
Almiñe	Bgy
Bagacay	Bgy
Bagahanglad	Bgy
Bartolabac	Bgy
Burgos	Bgy
Calipat-An	Bgy
Danao	Bgy
Dorong-an Daplian	Bgy
Interior	Bgy
Jagna-an	Bgy
Luna	Bgy
Mabini	Bgy
Piña	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
District IV (Pob.)	Bgy
Roosevelt	Bgy
San Isidro	Bgy
Santa Rosa	Bgy
Washington	Bgy
San Pascual	Mun
Boca Chica	Bgy
Bolod (Pob.)	Bgy
Busing	Bgy
Dangcalan	Bgy
Halabangbaybay	Bgy
Iniwaran	Bgy
Ki-Buaya	Bgy
Ki-Romero	Bgy
Laurente	Bgy
Mabini	Bgy
Mabuhay	Bgy
Mapanique	Bgy
Nazareno	Bgy
Pinamasingan	Bgy
Quintina	Bgy
San Jose	Bgy
San Pedro	Bgy
San Rafael	Bgy
Santa Cruz	Bgy
Terraplin (Pob.)	Bgy
Cueva	Bgy
Malaking Ilog	Bgy
Uson	Mun
Arado	Bgy
Armenia	Bgy
Aurora	Bgy
Badling	Bgy
Bonifacio	Bgy
Buenasuerte	Bgy
Buenavista	Bgy
Campana	Bgy
Candelaria	Bgy
Centro	Bgy
Crossing	Bgy
Dapdap	Bgy
Del Carmen	Bgy
Del Rosario	Bgy
Libertad	Bgy
Madao	Bgy
Mabini	Bgy
Magsaysay	Bgy
Marcella	Bgy
Miaga	Bgy
Mongahay	Bgy
Morocborocan	Bgy
Mabuhay	Bgy
Paguihaman	Bgy
Panicijan	Bgy
Poblacion	Bgy
Quezon	Bgy
San Isidro	Bgy
San Jose	Bgy
San Mateo	Bgy
San Ramon	Bgy
San Vicente	Bgy
Santo Cristo	Bgy
Sawang	Bgy
Simawa	Bgy
Sorsogon	Prov
Barcelona	Mun
Alegria	Bgy
Bagacay	Bgy
Bangate	Bgy
Bugtong	Bgy
Cagang	Bgy
Fabrica	Bgy
Jibong	Bgy
Lago	Bgy
Layog	Bgy
Luneta	Bgy
Macabari	Bgy
Mapapac	Bgy
Olandia	Bgy
Paghaluban	Bgy
Poblacion Central	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Putiao	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Ramon	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Lourdes	Bgy
Tagdon	Bgy
Bulan	Mun
A. Bonifacio	Bgy
Abad Santos	Bgy
Aguinaldo	Bgy
Antipolo	Bgy
Zone I Pob.	Bgy
Zone II Pob.	Bgy
Zone III Pob.	Bgy
Zone IV Pob.	Bgy
Zone V Pob.	Bgy
Zone VI Pob.	Bgy
Bical	Bgy
Beguin	Bgy
Bonga	Bgy
Butag	Bgy
Cadandanan	Bgy
Calomagon	Bgy
Calpi	Bgy
Cocok-Cabitan	Bgy
Daganas	Bgy
Danao	Bgy
Dolos	Bgy
E. Quirino	Bgy
Fabrica	Bgy
Gate	Bgy
Benigno S. Aquino	Bgy
Inararan	Bgy
J. Gerona	Bgy
Jamorawon	Bgy
Libertad	Bgy
Lajong	Bgy
Magsaysay	Bgy
Managanaga	Bgy
Marinab	Bgy
Nasuje	Bgy
Montecalvario	Bgy
N. Roque	Bgy
Namo	Bgy
Obrero	Bgy
Osmeña	Bgy
Otavi	Bgy
Padre Diaz	Bgy
Palale	Bgy
J.P. Laurel	Bgy
Quezon	Bgy
R. Gerona	Bgy
Recto	Bgy
M. Roxas	Bgy
Sagrada	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Juan Bag-o	Bgy
San Juan Daan	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Vicente	Bgy
Santa Remedios	Bgy
Santa Teresita	Bgy
Sigad	Bgy
Somagongsong	Bgy
G. Del Pilar	Bgy
Taromata	Bgy
Zone VII Pob.	Bgy
Zone VIII Pob.	Bgy
Bulusan	Mun
Bagacay	Bgy
Central (Pob.)	Bgy
Cogon	Bgy
Dancalan	Bgy
Dapdap (Pob.)	Bgy
Lalud	Bgy
Looban (Pob.)	Bgy
Mabuhay (Pob.)	Bgy
Madlawon (Pob.)	Bgy
Poctol (Pob.)	Bgy
Porog	Bgy
Sabang (Pob.)	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Bernardo	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Barbara	Bgy
Sapngan (Pob.)	Bgy
Tinampo	Bgy
Casiguran	Mun
Adovis (Pob.)	Bgy
Boton	Bgy
Burgos	Bgy
Casay	Bgy
Cawit	Bgy
Central (Pob.)	Bgy
Colambis	Bgy
Escuala	Bgy
Cogon	Bgy
Inlagadian	Bgy
Lungib	Bgy
Mabini	Bgy
Ponong	Bgy
Rizal	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Juan	Bgy
San Pascual	Bgy
Santa Cruz	Bgy
Somal-ot (Pob.)	Bgy
Tigbao	Bgy
Timbayog (Pob.)	Bgy
Tiris	Bgy
Trece Martirez	Bgy
Tulay	Bgy
Castilla	Mun
Amomonting	Bgy
Bagalayag	Bgy
Bagong Sirang	Bgy
Bonga	Bgy
Buenavista	Bgy
Burabod	Bgy
Caburacan	Bgy
Canjela	Bgy
Cogon	Bgy
Cumadcad	Bgy
Dangcalan	Bgy
Dinapa	Bgy
La Union	Bgy
Libtong	Bgy
Loreto	Bgy
Macalaya	Bgy
Maracabac	Bgy
Mayon	Bgy
Milagrosa	Bgy
Miluya	Bgy
Maypangi	Bgy
Monte Carmelo	Bgy
Oras	Bgy
Pandan	Bgy
Poblacion	Bgy
Quirapi	Bgy
Saclayan	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Sogoy	Bgy
Tomalaytay	Bgy
Donsol	Mun
Alin	Bgy
Awai	Bgy
Banban	Bgy
Bandi	Bgy
Banuang Gurang	Bgy
Baras	Bgy
Bayawas	Bgy
Bororan Barangay 1 (Pob.)	Bgy
Cabugao	Bgy
Central Barangay 2 (Pob.)	Bgy
Cristo	Bgy
Dancalan	Bgy
De Vera	Bgy
Gimagaan	Bgy
Girawan	Bgy
Gogon	Bgy
Gura	Bgy
Juan Adre	Bgy
Lourdes	Bgy
Mabini	Bgy
Malapoc	Bgy
Malinao	Bgy
Market Site Barangay 3 (Pob.)	Bgy
New Maguisa	Bgy
Ogod	Bgy
Old Maguisa	Bgy
Orange	Bgy
Pangpang	Bgy
Parina	Bgy
Pawala	Bgy
Pinamanaan	Bgy
Poso Pob.	Bgy
Punta Waling-Waling Pob.	Bgy
Rawis	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael	Bgy
San Ramon	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Sevilla	Bgy
Sibago	Bgy
Suguian	Bgy
Tagbac	Bgy
Tinanogan	Bgy
Tongdol	Bgy
Tres Marias	Bgy
Tuba	Bgy
Tupas	Bgy
Vinisitahan	Bgy
Gubat	Mun
Ariman	Bgy
Bagacay	Bgy
Balud Del Norte (Pob.)	Bgy
Balud Del Sur (Pob.)	Bgy
Benguet	Bgy
Bentuco	Bgy
Beriran	Bgy
Buenavista	Bgy
Bulacao	Bgy
Cabigaan	Bgy
Cabiguhan	Bgy
Carriedo	Bgy
Casili	Bgy
Cogon	Bgy
Cota Na Daco (Pob.)	Bgy
Dita	Bgy
Jupi	Bgy
Lapinig	Bgy
Luna-Candol (Pob.)	Bgy
Manapao	Bgy
Manook (Pob.)	Bgy
Naagtan	Bgy
Nato	Bgy
Nazareno	Bgy
Ogao	Bgy
Paco	Bgy
Panganiban (Pob.)	Bgy
Paradijon (Pob.)	Bgy
Patag	Bgy
Payawin	Bgy
Pinontingan (Pob.)	Bgy
Rizal	Bgy
San Ignacio	Bgy
Sangat	Bgy
Santa Ana	Bgy
Tabi	Bgy
Tagaytay	Bgy
Tigkiw	Bgy
Tiris	Bgy
Togawe	Bgy
Union	Bgy
Villareal	Bgy
Irosin	Mun
Bagsangan	Bgy
Bacolod (Pob.)	Bgy
Batang	Bgy
Bolos	Bgy
Buenavista	Bgy
Bulawan	Bgy
Carriedo	Bgy
Casini	Bgy
Cawayan	Bgy
Cogon	Bgy
Gabao	Bgy
Gulang-Gulang	Bgy
Gumapia	Bgy
Santo Domingo	Bgy
Liang	Bgy
Macawayan	Bgy
Mapaso	Bgy
Monbon	Bgy
Patag	Bgy
Salvacion	Bgy
San Agustin (Pob.)	Bgy
San Isidro	Bgy
San Juan (Pob.)	Bgy
San Julian (Pob.)	Bgy
San Pedro	Bgy
Tabon-Tabon	Bgy
Tinampo	Bgy
Tongdol	Bgy
Juban	Mun
Anog	Bgy
Aroroy	Bgy
Bacolod	Bgy
Binanuahan	Bgy
Biriran	Bgy
Buraburan	Bgy
Calateo	Bgy
Calmayon	Bgy
Carohayon	Bgy
Catanagan	Bgy
Catanusan	Bgy
Cogon	Bgy
Embarcadero	Bgy
Guruyan	Bgy
Lajong	Bgy
Maalo	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Puting Sapa	Bgy
Rangas	Bgy
Sablayan	Bgy
Sipaya	Bgy
Taboc	Bgy
Tinago	Bgy
Tughan	Bgy
Magallanes	Mun
Aguada Norte	Bgy
Aguada Sur	Bgy
Anibong	Bgy
Bacalon	Bgy
Bacolod	Bgy
Banacud	Bgy
Biga	Bgy
Behia	Bgy
Binisitahan del Norte	Bgy
Binisitahan del Sur	Bgy
Biton	Bgy
Bulala	Bgy
Busay	Bgy
Caditaan	Bgy
Cagbolo	Bgy
Cagtalaba	Bgy
Cawit Extension	Bgy
Cawit Proper	Bgy
Ginangra	Bgy
Hubo	Bgy
Incarizan	Bgy
Lapinig	Bgy
Magsaysay	Bgy
Malbog	Bgy
Pantalan	Bgy
Pawik	Bgy
Pili	Bgy
Poblacion	Bgy
Salvacion	Bgy
Santa Elena	Bgy
Siuton	Bgy
Tagas	Bgy
Tulatula Norte	Bgy
Tulatula Sur	Bgy
Matnog	Mun
Balocawe	Bgy
Banogao	Bgy
Banuangdaan	Bgy
Bariis	Bgy
Bolo	Bgy
Bon-Ot Big	Bgy
Bon-Ot Small	Bgy
Cabagahan	Bgy
Calayuan	Bgy
Calintaan	Bgy
Caloocan (Pob.)	Bgy
Calpi	Bgy
Camachiles (Pob.)	Bgy
Camcaman (Pob.)	Bgy
Coron-coron	Bgy
Culasi	Bgy
Gadgaron	Bgy
Genablan Occidental	Bgy
Genablan Oriental	Bgy
Hidhid	Bgy
Laboy	Bgy
Lajong	Bgy
Mambajog	Bgy
Manjunlad	Bgy
Manurabi	Bgy
Naburacan	Bgy
Paghuliran	Bgy
Pangi	Bgy
Pawa	Bgy
Poropandan	Bgy
Santa Isabel	Bgy
Sinalmacan	Bgy
Sinang-Atan	Bgy
Sinibaran	Bgy
Sisigon	Bgy
Sua	Bgy
Sulangan	Bgy
Tablac (Pob.)	Bgy
Tabunan (Pob.)	Bgy
Tugas	Bgy
Pilar	Mun
Abas	Bgy
Abucay	Bgy
Bantayan	Bgy
Banuyo (Pob.)	Bgy
Bayasong	Bgy
Bayawas	Bgy
Binanuahan (Pob.)	Bgy
Cabiguan	Bgy
Cagdongon	Bgy
Calongay	Bgy
Calpi	Bgy
Catamlangan	Bgy
Comapo-capo	Bgy
Danlog	Bgy
Dao (Pob.)	Bgy
Dapdap	Bgy
Del Rosario	Bgy
Esmerada	Bgy
Esperanza	Bgy
Guiron	Bgy
Ginablan	Bgy
Inang	Bgy
Inapugan	Bgy
Lubiano	Bgy
Leona	Bgy
Lipason	Bgy
Lourdes	Bgy
Lungib	Bgy
Lumbang	Bgy
Mabanate	Bgy
Malbog	Bgy
Marifosque (Pob.)	Bgy
Mercedes	Bgy
Migabod	Bgy
Naspi	Bgy
Palanas	Bgy
Pangpang	Bgy
Pinagsalog	Bgy
Pineda	Bgy
Poctol	Bgy
Pudo	Bgy
Putiao	Bgy
Sacnangan	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Antonio	Bgy
San Jose	Bgy
San Rafael	Bgy
Santa Fe	Bgy
Prieto Diaz	Mun
Brillante (Pob.)	Bgy
Bulawan	Bgy
Calao	Bgy
Carayat	Bgy
Diamante	Bgy
Gogon	Bgy
Lupi	Bgy
Manlabong	Bgy
Maningcay De Oro	Bgy
Perlas	Bgy
Quidolog	Bgy
Rizal	Bgy
San Antonio	Bgy
San Fernando	Bgy
San Isidro	Bgy
San Juan	Bgy
San Rafael	Bgy
San Ramon	Bgy
Santa Lourdes	Bgy
Santo Domingo	Bgy
Talisayan	Bgy
Tupaz	Bgy
Ulag	Bgy
Santa Magdalena	Mun
La Esperanza	Bgy
Peñafrancia	Bgy
Barangay Poblacion I	Bgy
Barangay Poblacion II	Bgy
Barangay Poblacion III	Bgy
Barangay Poblacion IV	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Bartolome	Bgy
San Eugenio	Bgy
San Isidro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Sebastian	Bgy
City of Sorsogon (Capital)	City
Abuyog	Bgy
Almendras-Cogon (Pob.)	Bgy
Balogo	Bgy
Barayong	Bgy
Basud	Bgy
Bibincahan	Bgy
Bitan-o/Dalipay (Pob.)	Bgy
Bucalbucalan	Bgy
Buenavista	Bgy
Buhatan	Bgy
Bulabog	Bgy
Burabod (Pob.)	Bgy
Cabid-An	Bgy
Cambulaga	Bgy
Capuy	Bgy
Gimaloto	Bgy
Guinlajon	Bgy
Macabog	Bgy
Marinas	Bgy
Pamurayan	Bgy
Pangpang	Bgy
Panlayaan	Bgy
Peñafrancia	Bgy
Piot (Pob.)	Bgy
Polvorista (Pob.)	Bgy
Rizal	Bgy
Salog (Pob.)	Bgy
Salvacion	Bgy
Sampaloc (Pob.)	Bgy
San Isidro	Bgy
San Juan	Bgy
Sirangan (Pob.)	Bgy
Sulucan (Pob.)	Bgy
Talisay (Pob.)	Bgy
Ticol	Bgy
Tugos	Bgy
Balete	Bgy
Balogo	Bgy
Bato	Bgy
Bon-Ot	Bgy
Bogña	Bgy
Buenavista	Bgy
Cabarbuhan	Bgy
Caricaran	Bgy
Del Rosario	Bgy
Gatbo	Bgy
Jamislagan	Bgy
Maricrum	Bgy
Osiao	Bgy
Poblacion	Bgy
Rawis	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Juan	Bgy
San Pascual	Bgy
San Ramon	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Lucia	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Sawanga	Bgy
Sugod	Bgy
Region VI (Western Visayas)	Reg
Aklan	Prov
Altavas	Mun
Cabangila	Bgy
Cabugao	Bgy
Catmon	Bgy
Dalipdip	Bgy
Ginictan	Bgy
Linayasan	Bgy
Lumaynay	Bgy
Lupo	Bgy
Man-up	Bgy
Odiong	Bgy
Poblacion	Bgy
Quinasay-an	Bgy
Talon	Bgy
Tibiao	Bgy
Balete	Mun
Aranas	Bgy
Arcangel	Bgy
Calizo	Bgy
Cortes	Bgy
Feliciano	Bgy
Fulgencio	Bgy
Guanko	Bgy
Morales	Bgy
Oquendo	Bgy
Poblacion	Bgy
Banga	Mun
Agbanawan	Bgy
Bacan	Bgy
Badiangan	Bgy
Cerrudo	Bgy
Cupang	Bgy
Daguitan	Bgy
Daja Norte	Bgy
Daja Sur	Bgy
Dingle	Bgy
Jumarap	Bgy
Lapnag	Bgy
Libas	Bgy
Linabuan Sur	Bgy
Mambog	Bgy
Mangan	Bgy
Muguing	Bgy
Pagsanghan	Bgy
Palale	Bgy
Poblacion	Bgy
Polo	Bgy
Polocate	Bgy
San Isidro	Bgy
Sibalew	Bgy
Sigcay	Bgy
Taba-ao	Bgy
Tabayon	Bgy
Tinapuay	Bgy
Torralba	Bgy
Ugsod	Bgy
Venturanza	Bgy
Batan	Mun
Ambolong	Bgy
Angas	Bgy
Bay-ang	Bgy
Caiyang	Bgy
Cabugao	Bgy
Camaligan	Bgy
Camanci	Bgy
Ipil	Bgy
Lalab	Bgy
Lupit	Bgy
Magpag-ong	Bgy
Magubahay	Bgy
Mambuquiao	Bgy
Man-up	Bgy
Mandong	Bgy
Napti	Bgy
Palay	Bgy
Poblacion	Bgy
Songcolan	Bgy
Tabon	Bgy
Buruanga	Mun
Alegria	Bgy
Bagongbayan	Bgy
Balusbos	Bgy
Bel-is	Bgy
Cabugan	Bgy
El Progreso	Bgy
Habana	Bgy
Katipunan	Bgy
Mayapay	Bgy
Nazareth	Bgy
Panilongan	Bgy
Poblacion	Bgy
Santander	Bgy
Tag-osip	Bgy
Tigum	Bgy
Ibajay	Mun
Agbago	Bgy
Agdugayan	Bgy
Antipolo	Bgy
Aparicio	Bgy
Aquino	Bgy
Aslum	Bgy
Bagacay	Bgy
Batuan	Bgy
Buenavista	Bgy
Bugtongbato	Bgy
Cabugao	Bgy
Capilijan	Bgy
Colongcolong	Bgy
Laguinbanua	Bgy
Mabusao	Bgy
Malindog	Bgy
Maloco	Bgy
Mina-a	Bgy
Monlaque	Bgy
Naile	Bgy
Naisud	Bgy
Naligusan	Bgy
Ondoy	Bgy
Poblacion	Bgy
Polo	Bgy
Regador	Bgy
Rivera	Bgy
Rizal	Bgy
San Isidro	Bgy
San Jose	Bgy
Santa Cruz	Bgy
Tagbaya	Bgy
Tul-ang	Bgy
Unat	Bgy
Yawan	Bgy
Kalibo (Capital)	Mun
Andagaw	Bgy
Bachaw Norte	Bgy
Bachaw Sur	Bgy
Briones	Bgy
Buswang New	Bgy
Buswang Old	Bgy
Caano	Bgy
Estancia	Bgy
Linabuan Norte	Bgy
Mabilo	Bgy
Mobo	Bgy
Nalook	Bgy
Poblacion	Bgy
Pook	Bgy
Tigayon	Bgy
Tinigaw	Bgy
Lezo	Mun
Agcawilan	Bgy
Bagto	Bgy
Bugasongan	Bgy
Carugdog	Bgy
Cogon	Bgy
Ibao	Bgy
Mina	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Santa Cruz Bigaa	Bgy
Silakat-Nonok	Bgy
Tayhawan	Bgy
Libacao	Mun
Agmailig	Bgy
Alfonso XII	Bgy
Batobato	Bgy
Bonza	Bgy
Calacabian	Bgy
Calamcan	Bgy
Can-Awan	Bgy
Casit-an	Bgy
Dalagsa-an	Bgy
Guadalupe	Bgy
Janlud	Bgy
Julita	Bgy
Luctoga	Bgy
Magugba	Bgy
Manika	Bgy
Ogsip	Bgy
Ortega	Bgy
Oyang	Bgy
Pampango	Bgy
Pinonoy	Bgy
Poblacion	Bgy
Rivera	Bgy
Rosal	Bgy
Sibalew	Bgy
Madalag	Mun
Alaminos	Bgy
Alas-as	Bgy
Bacyang	Bgy
Balactasan	Bgy
Cabangahan	Bgy
Cabilawan	Bgy
Catabana	Bgy
Dit-Ana	Bgy
Galicia	Bgy
Guinatu-an	Bgy
Logohon	Bgy
Mamba	Bgy
Maria Cristina	Bgy
Medina	Bgy
Mercedes	Bgy
Napnot	Bgy
Pang-Itan	Bgy
Paningayan	Bgy
Panipiason	Bgy
Poblacion	Bgy
San Jose	Bgy
Singay	Bgy
Talangban	Bgy
Talimagao	Bgy
Tigbawan	Bgy
Makato	Mun
Agbalogo	Bgy
Aglucay	Bgy
Alibagon	Bgy
Bagong Barrio	Bgy
Baybay	Bgy
Cabatanga	Bgy
Cajilo	Bgy
Calangcang	Bgy
Calimbajan	Bgy
Castillo	Bgy
Cayangwan	Bgy
Dumga	Bgy
Libang	Bgy
Mantiguib	Bgy
Poblacion	Bgy
Tibiawan	Bgy
Tina	Bgy
Tugas	Bgy
Malay	Mun
Argao	Bgy
Balabag	Bgy
Balusbus	Bgy
Cabulihan	Bgy
Caticlan	Bgy
Cogon	Bgy
Cubay Norte	Bgy
Cubay Sur	Bgy
Dumlog	Bgy
Manoc-Manoc	Bgy
Naasug	Bgy
Nabaoy	Bgy
Napaan	Bgy
Poblacion	Bgy
San Viray	Bgy
Yapak	Bgy
Motag	Bgy
Malinao	Mun
Banaybanay	Bgy
Biga-a	Bgy
Bulabud	Bgy
Cabayugan	Bgy
Capataga	Bgy
Cogon	Bgy
Dangcalan	Bgy
Kinalangay Nuevo	Bgy
Kinalangay Viejo	Bgy
Lilo-an	Bgy
Malandayon	Bgy
Manhanip	Bgy
Navitas	Bgy
Osman	Bgy
Poblacion	Bgy
Rosario	Bgy
San Dimas	Bgy
San Ramon	Bgy
San Roque	Bgy
Sipac	Bgy
Sugnod	Bgy
Tambuan	Bgy
Tigpalas	Bgy
Nabas	Mun
Alimbo-Baybay	Bgy
Buenasuerte	Bgy
Buenafortuna	Bgy
Buenavista	Bgy
Gibon	Bgy
Habana	Bgy
Laserna	Bgy
Libertad	Bgy
Magallanes	Bgy
Matabana	Bgy
Nagustan	Bgy
Pawa	Bgy
Pinatuad	Bgy
Poblacion	Bgy
Rizal	Bgy
Solido	Bgy
Tagororoc	Bgy
Toledo	Bgy
Unidos	Bgy
Union	Bgy
New Washington	Mun
Candelaria	Bgy
Cawayan	Bgy
Dumaguit	Bgy
Fatima	Bgy
Guinbaliwan	Bgy
Jalas	Bgy
Jugas	Bgy
Lawa-an	Bgy
Mabilo	Bgy
Mataphao	Bgy
Ochando	Bgy
Pinamuk-an	Bgy
Poblacion	Bgy
Polo	Bgy
Puis	Bgy
Tambak	Bgy
Numancia	Mun
Albasan	Bgy
Aliputos	Bgy
Badio	Bgy
Bubog	Bgy
Bulwang	Bgy
Camanci Norte	Bgy
Camanci Sur	Bgy
Dongon East	Bgy
Dongon West	Bgy
Joyao-joyao	Bgy
Laguinbanua East	Bgy
Laguinbanua West	Bgy
Marianos	Bgy
Navitas	Bgy
Poblacion	Bgy
Pusiw	Bgy
Tabangka	Bgy
Tangalan	Mun
Afga	Bgy
Baybay	Bgy
Dapdap	Bgy
Dumatad	Bgy
Jawili	Bgy
Lanipga	Bgy
Napatag	Bgy
Panayakan	Bgy
Poblacion	Bgy
Pudiot	Bgy
Tagas	Bgy
Tamalagon	Bgy
Tamokoe	Bgy
Tondog	Bgy
Vivo	Bgy
Antique	Prov
Anini-Y	Mun
Bayo Grande	Bgy
Bayo Pequeño	Bgy
Butuan	Bgy
Casay	Bgy
Casay Viejo	Bgy
Iba	Bgy
Igbarabatuan	Bgy
Igpalge	Bgy
Igtumarom	Bgy
Lisub A	Bgy
Lisub B	Bgy
Mabuyong	Bgy
Magdalena	Bgy
Nasuli C	Bgy
Nato	Bgy
Poblacion	Bgy
Sagua	Bgy
Salvacion	Bgy
San Francisco	Bgy
San Ramon	Bgy
San Roque	Bgy
Tagaytay	Bgy
Talisayan	Bgy
Barbaza	Mun
Baghari	Bgy
Bahuyan	Bgy
Beri	Bgy
Biga-a	Bgy
Binangbang	Bgy
Binangbang Centro	Bgy
Binanu-an	Bgy
Cadiao	Bgy
Calapadan	Bgy
Capoyuan	Bgy
Cubay	Bgy
Esparar	Bgy
Gua	Bgy
Idao	Bgy
Igpalge	Bgy
Igtunarum	Bgy
Embrangga-an	Bgy
Integasan	Bgy
Ipil	Bgy
Jinalinan	Bgy
Lanas	Bgy
Langcaon	Bgy
Lisub	Bgy
Lombuyan	Bgy
Mablad	Bgy
Magtulis	Bgy
Marigne	Bgy
Mayabay	Bgy
Mayos	Bgy
Nalusdan	Bgy
Narirong	Bgy
Palma	Bgy
Poblacion	Bgy
San Antonio	Bgy
San Ramon	Bgy
Soligao	Bgy
Tabongtabong	Bgy
Tig-Alaran	Bgy
Yapo	Bgy
Belison	Mun
Borocboroc	Bgy
Buenavista	Bgy
Concepcion	Bgy
Delima	Bgy
Ipil	Bgy
Maradiona	Bgy
Mojon	Bgy
Poblacion	Bgy
Rombang	Bgy
Salvacion	Bgy
Sinaja	Bgy
Bugasong	Mun
Anilawan	Bgy
Arangote	Bgy
Bagtason	Bgy
Camangahan	Bgy
Cubay North	Bgy
Cubay South	Bgy
Guija	Bgy
Igbalangao	Bgy
Igsoro	Bgy
Ilaures	Bgy
Jinalinan	Bgy
Lacayon	Bgy
Maray	Bgy
Paliwan	Bgy
Pangalcagan	Bgy
Centro Ilawod (Pob.)	Bgy
Centro Ilaya (Pob.)	Bgy
Centro Pojo (Pob.)	Bgy
Sabang East	Bgy
Sabang West	Bgy
Tagudtud North	Bgy
Tagudtud South	Bgy
Talisay	Bgy
Tica	Bgy
Tono-an	Bgy
Yapu	Bgy
Zaragoza	Bgy
Caluya	Mun
Alegria	Bgy
Bacong	Bgy
Banago	Bgy
Bonbon	Bgy
Dawis	Bgy
Dionela	Bgy
Harigue	Bgy
Hininga-an	Bgy
Imba	Bgy
Masanag	Bgy
Poblacion	Bgy
Sabang	Bgy
Salamento	Bgy
Semirara	Bgy
Sibato	Bgy
Sibay	Bgy
Sibolo	Bgy
Tinogboc	Bgy
Culasi	Mun
Alojipan	Bgy
Bagacay	Bgy
Balac-balac	Bgy
Magsaysay	Bgy
Batbatan Island	Bgy
Batonan Norte	Bgy
Batonan Sur	Bgy
Bita	Bgy
Bitadton Norte	Bgy
Bitadton Sur	Bgy
Buenavista	Bgy
Buhi	Bgy
Camancijan	Bgy
Caridad	Bgy
Carit-an	Bgy
Condes	Bgy
Esperanza	Bgy
Fe	Bgy
Flores	Bgy
Jalandoni	Bgy
Janlagasi	Bgy
Lamputong	Bgy
Lipata	Bgy
Malacañang	Bgy
Malalison Island	Bgy
Maniguin	Bgy
Naba	Bgy
Osorio	Bgy
Paningayan	Bgy
Centro Poblacion	Bgy
Centro Norte (Pob.)	Bgy
Centro Sur (Pob.)	Bgy
Salde	Bgy
San Antonio	Bgy
San Gregorio	Bgy
San Juan	Bgy
San Luis	Bgy
San Pascual	Bgy
San Vicente	Bgy
Simbola	Bgy
Tigbobolo	Bgy
Tinabusan	Bgy
Tomao	Bgy
Valderama	Bgy
Tobias Fornier	Mun
Abaca	Bgy
Aras-Asan	Bgy
Arobo	Bgy
Atabay	Bgy
Atiotes	Bgy
Bagumbayan	Bgy
Balloscas	Bgy
Balud	Bgy
Barasanan A	Bgy
Barasanan B	Bgy
Barasanan C	Bgy
Bariri	Bgy
Camandagan	Bgy
Cato-ogan	Bgy
Danawan	Bgy
Diclum	Bgy
Fatima	Bgy
Gamad	Bgy
Igbalogo	Bgy
Igbangcal-A	Bgy
Igbangcal-B	Bgy
Igbangcal-C	Bgy
Igcabuad	Bgy
Igcado	Bgy
Igcalawagan	Bgy
Igcapuyas	Bgy
Igcasicad	Bgy
Igdalaguit	Bgy
Igdanlog	Bgy
Igdurarog	Bgy
Igtugas	Bgy
Lawigan	Bgy
Manaling	Bgy
Masayo	Bgy
Nagsubuan	Bgy
Paciencia	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Portillo	Bgy
Quezon	Bgy
Opsan	Bgy
Nasuli-A	Bgy
Salamague	Bgy
Santo Tomas	Bgy
Tacbuyan	Bgy
Tene	Bgy
Villaflor	Bgy
Ysulat	Bgy
Igcadac	Bgy
Lindero	Bgy
Hamtic	Mun
Apdo	Bgy
Asluman	Bgy
Banawon	Bgy
Bia-an	Bgy
Bongbongan I-II	Bgy
Bongbongan III	Bgy
Botbot	Bgy
Budbudan	Bgy
Buhang	Bgy
Calacja I	Bgy
Calacja II	Bgy
Calala	Bgy
Cantulan	Bgy
Caridad	Bgy
Caromangay	Bgy
Casalngan	Bgy
Dangcalan	Bgy
Del Pilar	Bgy
Fabrica	Bgy
Funda	Bgy
General Fullon	Bgy
Guintas	Bgy
Igbical	Bgy
Igbucagay	Bgy
Inabasan	Bgy
Ingwan-Batangan	Bgy
La Paz	Bgy
Gov. Evelio B. Javier	Bgy
Linaban	Bgy
Malandog	Bgy
Mapatag	Bgy
Masanag	Bgy
Nalihawan	Bgy
Pamandayan	Bgy
Pasu-Jungao	Bgy
Piape I	Bgy
Piape II	Bgy
Piape III	Bgy
Pili 1, 2, 3	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Pu-ao	Bgy
Suloc	Bgy
Villavert-Jimenez	Bgy
Laua-An	Mun
Banban	Bgy
Bongbongan	Bgy
Cabariwan	Bgy
Cadajug	Bgy
Canituan	Bgy
Capnayan	Bgy
Casit-an	Bgy
Guinbanga-an	Bgy
Guiamon	Bgy
Guisijan	Bgy
Igtadiao	Bgy
Intao	Bgy
Jaguikican	Bgy
Jinalinan	Bgy
Lactudan	Bgy
Latazon	Bgy
Laua-an	Bgy
Loon	Bgy
Liberato	Bgy
Lindero	Bgy
Liya-liya	Bgy
Lugta	Bgy
Lupa-an	Bgy
Magyapo	Bgy
Maria	Bgy
Mauno	Bgy
Maybunga	Bgy
Necesito	Bgy
Oloc	Bgy
Omlot	Bgy
Pandanan	Bgy
Paningayan	Bgy
Pascuala	Bgy
Poblacion	Bgy
San Ramon	Bgy
Santiago	Bgy
Tibacan	Bgy
Tigunhao	Bgy
Virginia	Bgy
Bagongbayan	Bgy
Libertad	Mun
Barusbus	Bgy
Bulanao	Bgy
Cubay	Bgy
Codiong	Bgy
Igcagay	Bgy
Inyawan	Bgy
Lindero	Bgy
Maramig	Bgy
Pucio	Bgy
Pajo	Bgy
Panangkilon	Bgy
Paz	Bgy
Centro Este (Pob.)	Bgy
Centro Weste (Pob.)	Bgy
San Roque	Bgy
Tinigbas	Bgy
Tinindugan	Bgy
Taboc	Bgy
Union	Bgy
Pandan	Mun
Aracay	Bgy
Badiangan	Bgy
Bagumbayan	Bgy
Baybay	Bgy
Botbot	Bgy
Buang	Bgy
Cabugao	Bgy
Candari	Bgy
Carmen	Bgy
Centro Sur (Pob.)	Bgy
Dionela	Bgy
Dumrog	Bgy
Duyong	Bgy
Fragante	Bgy
Guia	Bgy
Idiacacan	Bgy
Jinalinan	Bgy
Luhod-Bayang	Bgy
Maadios	Bgy
Mag-aba	Bgy
Napuid	Bgy
Nauring	Bgy
Patria	Bgy
Perfecta	Bgy
Centro Norte (Pob.)	Bgy
San Andres	Bgy
San Joaquin	Bgy
Santa Ana	Bgy
Santa Cruz	Bgy
Santa Fe	Bgy
Santo Rosario	Bgy
Talisay	Bgy
Tingib	Bgy
Zaldivar	Bgy
Patnongon	Mun
Alvañiz	Bgy
Amparo	Bgy
Apgahan	Bgy
Aureliana	Bgy
Badiangan	Bgy
Bernaldo A. Julagting	Bgy
Carit-an	Bgy
Cuyapiao	Bgy
Villa Elio	Bgy
Gella	Bgy
Igbarawan	Bgy
Igbobon	Bgy
Igburi	Bgy
La Rioja	Bgy
Mabasa	Bgy
Macarina	Bgy
Magarang	Bgy
Magsaysay	Bgy
Padang	Bgy
Pandanan	Bgy
Patlabawon	Bgy
Poblacion	Bgy
Quezon	Bgy
Salaguiawan	Bgy
Samalague	Bgy
San Rafael	Bgy
Tobias Fornier	Bgy
Tamayoc	Bgy
Tigbalogo	Bgy
Villa Crespo	Bgy
Villa Cruz	Bgy
Villa Flores	Bgy
Villa Laua-an	Bgy
Villa Sal	Bgy
Villa Salomon	Bgy
Vista Alegre	Bgy
San Jose (Capital)	Mun
Atabay	Bgy
Badiang	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Bariri	Bgy
Bugarot	Bgy
Cansadan	Bgy
Durog	Bgy
Funda-Dalipe	Bgy
Igbonglo	Bgy
Inabasan	Bgy
Madrangca	Bgy
Magcalon	Bgy
Malaiba	Bgy
Maybato Norte	Bgy
Maybato Sur	Bgy
Mojon	Bgy
Pantao	Bgy
San Angel	Bgy
San Fernando	Bgy
San Pedro	Bgy
Supa	Bgy
San Remigio	Mun
Agricula	Bgy
Alegria	Bgy
Aningalan	Bgy
Atabay	Bgy
Bagumbayan	Bgy
Baladjay	Bgy
Banbanan	Bgy
Barangbang	Bgy
Bawang	Bgy
Bugo	Bgy
Bulan-bulan	Bgy
Cabiawan	Bgy
Cabunga-an	Bgy
Cadolonan	Bgy
Poblacion	Bgy
Carawisan I	Bgy
Carawisan II	Bgy
Carmelo I	Bgy
Carmelo II	Bgy
General Fullon	Bgy
General Luna	Bgy
Orquia	Bgy
Iguirindon	Bgy
Insubuan	Bgy
La Union	Bgy
Lapak	Bgy
Lumpatan	Bgy
Magdalena	Bgy
Maragubdub	Bgy
Nagbangi I	Bgy
Nagbangi II	Bgy
Nasuli	Bgy
Osorio I	Bgy
Osorio II	Bgy
Panpanan I	Bgy
Panpanan II	Bgy
Ramon Magsaysay	Bgy
Rizal	Bgy
San Rafael	Bgy
Sinundolan	Bgy
Sumaray	Bgy
Trinidad	Bgy
Tubudan	Bgy
Vilvar	Bgy
Walker	Bgy
Sebaste	Mun
Abiera	Bgy
Aguila	Bgy
Alegre	Bgy
Aras-Asan	Bgy
Bacalan	Bgy
Callan	Bgy
Nauhon	Bgy
P. Javier	Bgy
Poblacion	Bgy
Idio	Bgy
Sibalom	Mun
Alangan	Bgy
Valentin Grasparil	Bgy
Bari	Bgy
Biga-a	Bgy
Bongbongan I	Bgy
Bongbongan II	Bgy
Bongsod	Bgy
Bontol	Bgy
Bugnay	Bgy
Bulalacao	Bgy
Cabanbanan	Bgy
Cabariuan	Bgy
Cabladan	Bgy
Cadoldolan	Bgy
Calo-oy	Bgy
Calog	Bgy
Catmon	Bgy
Catungan I	Bgy
Catungan II	Bgy
Catungan III	Bgy
Catungan IV	Bgy
Cubay-Sermon	Bgy
Egaña	Bgy
Esperanza I	Bgy
Esperanza II	Bgy
Esperanza III	Bgy
Igcococ	Bgy
Igdalaquit	Bgy
Igdagmay	Bgy
Iglanot	Bgy
Igpanolong	Bgy
Igparas	Bgy
Igsuming	Bgy
Ilabas	Bgy
Imparayan	Bgy
Inabasan	Bgy
Indag-an	Bgy
Initan	Bgy
Insarayan	Bgy
Lacaron	Bgy
Lagdo	Bgy
Lambayagan	Bgy
Luna	Bgy
Luyang	Bgy
Maasin	Bgy
Mabini	Bgy
Millamena	Bgy
Mojon	Bgy
Nagdayao	Bgy
Cubay-Napultan	Bgy
Nazareth	Bgy
Odiong	Bgy
Olaga	Bgy
Pangpang	Bgy
Panlagangan	Bgy
Pantao	Bgy
Pasong	Bgy
Pis-Anan	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
District IV (Pob.)	Bgy
Rombang	Bgy
Salvacion	Bgy
San Juan	Bgy
Sido	Bgy
Solong	Bgy
Tabongtabong	Bgy
Tig-Ohot	Bgy
Tigbalua I	Bgy
Tordesillas	Bgy
Tulatula	Bgy
Villafont	Bgy
Villahermosa	Bgy
Villar	Bgy
Tigbalua II	Bgy
Tibiao	Mun
Alegre	Bgy
Amar	Bgy
Bandoja	Bgy
Castillo	Bgy
Esparagoza	Bgy
Importante	Bgy
La Paz	Bgy
Malabor	Bgy
Martinez	Bgy
Natividad	Bgy
Pitac	Bgy
Poblacion	Bgy
Salazar	Bgy
San Francisco Norte	Bgy
San Francisco Sur	Bgy
San Isidro	Bgy
Santa Ana	Bgy
Santa Justa	Bgy
Santo Rosario	Bgy
Tigbaboy	Bgy
Tuno	Bgy
Valderrama	Mun
Bakiang	Bgy
Binanogan	Bgy
Borocboroc	Bgy
Bugnay	Bgy
Buluangan I	Bgy
Buluangan II	Bgy
Bunsod	Bgy
Busog	Bgy
Cananghan	Bgy
Canipayan	Bgy
Cansilayan	Bgy
Culyat	Bgy
Iglinab	Bgy
Igmasandig	Bgy
Lublub	Bgy
Manlacbo	Bgy
Pandanan	Bgy
San Agustin	Bgy
Takas (Pob.)	Bgy
Tigmamale	Bgy
Ubos (Pob.)	Bgy
Alon	Bgy
Capiz	Prov
Cuartero	Mun
Agcabugao	Bgy
Agdahon	Bgy
Agnaga	Bgy
Angub	Bgy
Balingasag	Bgy
Bito-on Ilawod	Bgy
Bito-on Ilaya	Bgy
Bun-od	Bgy
Carataya	Bgy
Lunayan	Bgy
Mahunodhunod	Bgy
Maindang	Bgy
Mainit	Bgy
Malagab-i	Bgy
Nagba	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Poblacion Takas	Bgy
Puti-an	Bgy
San Antonio	Bgy
Sinabsaban	Bgy
Mahabang Sapa	Bgy
Dao	Mun
Aganan	Bgy
Agtambi	Bgy
Agtanguay	Bgy
Balucuan	Bgy
Bita	Bgy
Centro	Bgy
Daplas	Bgy
Duyoc	Bgy
Ilas Sur	Bgy
Lacaron	Bgy
Malonoy	Bgy
Manhoy	Bgy
Mapulang Bato	Bgy
Matagnop	Bgy
Nasunogan	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Quinabcaban	Bgy
Quinayuya	Bgy
San Agustin	Bgy
Dumalag	Mun
Concepcion	Bgy
Consolacion	Bgy
Dolores	Bgy
Duran	Bgy
San Agustin	Bgy
San Jose	Bgy
San Martin	Bgy
San Miguel	Bgy
San Rafael	Bgy
San Roque	Bgy
Santa Carmen	Bgy
Santa Cruz	Bgy
Santa Monica	Bgy
Santa Rita	Bgy
Santa Teresa	Bgy
Santo Angel	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Poblacion	Bgy
Dumarao	Mun
Agbatuan	Bgy
Aglalana	Bgy
Aglanot	Bgy
Agsirab	Bgy
Alipasiawan	Bgy
Astorga	Bgy
Bayog	Bgy
Bungsuan	Bgy
Calapawan	Bgy
Cubi	Bgy
Dacuton	Bgy
Dangula	Bgy
Gibato	Bgy
Codingle	Bgy
Guinotos	Bgy
Jambad	Bgy
Janguslob	Bgy
Lawaan	Bgy
Malonoy	Bgy
Nagsulang	Bgy
Ongol Ilawod	Bgy
Ongol Ilaya	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Sagrada Familia	Bgy
Salcedo	Bgy
San Juan	Bgy
Sibariwan	Bgy
Tamulalod	Bgy
Taslan	Bgy
Tina	Bgy
Tinaytayan	Bgy
Traciano	Bgy
Ivisan	Mun
Agmalobo	Bgy
Agustin Navarra	Bgy
Balaring	Bgy
Basiao	Bgy
Cabugao	Bgy
Cudian	Bgy
Ilaya-Ivisan	Bgy
Malocloc Norte	Bgy
Malocloc Sur	Bgy
Matnog	Bgy
Mianay	Bgy
Ondoy	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Santa Cruz	Bgy
Jamindan	Mun
Agambulong	Bgy
Agbun-od	Bgy
Agcagay	Bgy
Aglibacao	Bgy
Agloloway	Bgy
Bayebaye	Bgy
Caridad	Bgy
Esperanza	Bgy
Fe	Bgy
Ganzon	Bgy
Guintas	Bgy
Igang	Bgy
Jaena Norte	Bgy
Jaena Sur	Bgy
Jagnaya	Bgy
Lapaz	Bgy
Linambasan	Bgy
Lucero	Bgy
Maantol	Bgy
Masgrau	Bgy
Milan	Bgy
Molet	Bgy
Pangabat	Bgy
Pangabuan	Bgy
Pasol-o	Bgy
Poblacion	Bgy
San Jose	Bgy
San Juan	Bgy
San Vicente	Bgy
Santo Rosario	Bgy
Ma-Ayon	Mun
Aglimocon	Bgy
Alasaging	Bgy
Alayunan	Bgy
Balighot	Bgy
Batabat	Bgy
Bongbongan	Bgy
Cabungahan	Bgy
Canapian	Bgy
Carataya	Bgy
Duluan	Bgy
East Villaflores	Bgy
Fernandez	Bgy
Guinbi-alan	Bgy
Indayagan	Bgy
Jebaca	Bgy
Maalan	Bgy
Manayupit	Bgy
New Guia	Bgy
Quevedo	Bgy
Old Guia	Bgy
Palaguian	Bgy
Parallan	Bgy
Piña	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Poblacion Tabuc	Bgy
Quinabonglan	Bgy
Quinat-uyan	Bgy
Salgan	Bgy
Tapulang	Bgy
Tuburan	Bgy
West Villaflores	Bgy
Mambusao	Mun
Atiplo	Bgy
Balat-an	Bgy
Balit	Bgy
Batiano	Bgy
Bating	Bgy
Bato Bato	Bgy
Baye	Bgy
Bergante	Bgy
Bunga	Bgy
Bula	Bgy
Bungsi	Bgy
Burias	Bgy
Caidquid	Bgy
Cala-agus	Bgy
Libo-o	Bgy
Manibad	Bgy
Maralag	Bgy
Najus-an	Bgy
Pangpang Norte	Bgy
Pangpang Sur	Bgy
Pinay	Bgy
Poblacion Proper	Bgy
Poblacion Tabuc	Bgy
Sinondojan	Bgy
Tugas	Bgy
Tumalalud	Bgy
Panay	Mun
Agbalo	Bgy
Agbanban	Bgy
Agojo	Bgy
Anhawon	Bgy
Bagacay	Bgy
Bago Chiquito	Bgy
Bago Grande	Bgy
Bahit	Bgy
Bantique	Bgy
Bato	Bgy
Binangig	Bgy
Binantuan	Bgy
Bonga	Bgy
Buntod	Bgy
Butacal	Bgy
Cabugao Este	Bgy
Cabugao Oeste	Bgy
Calapawan	Bgy
Calitan	Bgy
Candual	Bgy
Cogon	Bgy
Daga	Bgy
Ilamnay	Bgy
Jamul-awon	Bgy
Lanipga	Bgy
Lat-Asan	Bgy
Libon	Bgy
Linao	Bgy
Linateran	Bgy
Lomboy	Bgy
Lus-Onan	Bgy
Magubilan	Bgy
Navitas	Bgy
Pawa	Bgy
Pili	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Poblacion Tabuc	Bgy
Talasa	Bgy
Tanza Norte	Bgy
Tanza Sur	Bgy
Tico	Bgy
Panitan	Mun
Agbabadiang	Bgy
Agkilo	Bgy
Agloway	Bgy
Ambilay	Bgy
Bahit	Bgy
Balatucan	Bgy
Banga-an	Bgy
Cabugao	Bgy
Cabangahan	Bgy
Cadio	Bgy
Cala-an	Bgy
Capagao	Bgy
Cogon	Bgy
Conciencia	Bgy
Ensenagan	Bgy
Intampilan	Bgy
Pasugue	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Quios	Bgy
Salocon	Bgy
Tabuc Norte	Bgy
Tabuc Sur	Bgy
Timpas	Bgy
Tincupon	Bgy
Tinigban	Bgy
Pilar	Mun
Balogo	Bgy
Binaobawan	Bgy
Blasco	Bgy
Casanayan	Bgy
Cayus	Bgy
Dayhagan	Bgy
Dulangan	Bgy
Monteflor	Bgy
Natividad	Bgy
Olalo	Bgy
Poblacion	Bgy
Rosario	Bgy
San Antonio	Bgy
San Blas	Bgy
San Esteban	Bgy
San Fernando	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Ramon	Bgy
San Silvestre	Bgy
Sinamongan	Bgy
Santa Fe	Bgy
Tabun-acan	Bgy
Yating	Bgy
Pontevedra	Mun
Agbanog	Bgy
Agdalipe	Bgy
Ameligan	Bgy
Bailan	Bgy
Banate	Bgy
Bantigue	Bgy
Binuntucan	Bgy
Cabugao	Bgy
Gabuc	Bgy
Guba	Bgy
Hipona	Bgy
Intungcan	Bgy
Jolongajog	Bgy
Lantangan	Bgy
Linampongan	Bgy
Malag-it	Bgy
Manapao	Bgy
Ilawod (Pob.)	Bgy
Ilaya (Pob.)	Bgy
Rizal	Bgy
San Pedro	Bgy
Solo	Bgy
Sublangon	Bgy
Tabuc	Bgy
Tacas	Bgy
Yatingan	Bgy
President Roxas	Mun
Aranguel	Bgy
Badiangon	Bgy
Bayuyan	Bgy
Cabugcabug	Bgy
Carmencita	Bgy
Cubay	Bgy
Culilang	Bgy
Goce	Bgy
Hanglid	Bgy
Ibaca	Bgy
Madulano	Bgy
Manoling	Bgy
Marita	Bgy
Pandan	Bgy
Pantalan Cabugcabug	Bgy
Pinamihagan	Bgy
Poblacion	Bgy
Pondol	Bgy
Quiajo	Bgy
Sangkal	Bgy
Santo Niño	Bgy
Vizcaya	Bgy
City of Roxas (Capital)	City
Adlawan	Bgy
Bago	Bgy
Balijuagan	Bgy
Banica	Bgy
Poblacion I	Bgy
Poblacion X	Bgy
Poblacion XI	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
Poblacion VI	Bgy
Poblacion VII	Bgy
Poblacion VIII	Bgy
Poblacion IX	Bgy
Barra	Bgy
Bato	Bgy
Baybay	Bgy
Bolo	Bgy
Cabugao	Bgy
Cagay	Bgy
Cogon	Bgy
Culajao	Bgy
Culasi	Bgy
Dumolog	Bgy
Dayao	Bgy
Dinginan	Bgy
Gabu-an	Bgy
Inzo Arnaldo Village	Bgy
Jumaguicjic	Bgy
Lanot	Bgy
Lawa-an	Bgy
Liong	Bgy
Libas	Bgy
Loctugan	Bgy
Lonoy	Bgy
Milibili	Bgy
Mongpong	Bgy
Olotayan	Bgy
Punta Cogon	Bgy
Punta Tabuc	Bgy
San Jose	Bgy
Sibaguan	Bgy
Talon	Bgy
Tanque	Bgy
Tanza	Bgy
Tiza	Bgy
Sapi-An	Mun
Agsilab	Bgy
Agtatacay Norte	Bgy
Agtatacay Sur	Bgy
Bilao	Bgy
Damayan	Bgy
Dapdapan	Bgy
Lonoy	Bgy
Majanlud	Bgy
Maninang	Bgy
Poblacion	Bgy
Sigma	Mun
Acbo	Bgy
Amaga	Bgy
Balucuan	Bgy
Bangonbangon	Bgy
Capuyhan	Bgy
Cogon	Bgy
Dayhagon	Bgy
Guintas	Bgy
Malapad Cogon	Bgy
Mangoso	Bgy
Mansacul	Bgy
Matangcong	Bgy
Matinabus	Bgy
Mianay	Bgy
Oyong	Bgy
Pagbunitan	Bgy
Parian	Bgy
Pinamalatican	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Tawog	Bgy
Tapaz	Mun
Abangay	Bgy
Acuña	Bgy
Agcococ	Bgy
Aglinab	Bgy
Aglupacan	Bgy
Agpalali	Bgy
Apero	Bgy
Artuz	Bgy
Bag-Ong Barrio	Bgy
Bato-bato	Bgy
Buri	Bgy
Camburanan	Bgy
Candelaria	Bgy
Carida	Bgy
Cristina	Bgy
Da-an Banwa	Bgy
Da-an Norte	Bgy
Da-an Sur	Bgy
Garcia	Bgy
Gebio-an	Bgy
Hilwan	Bgy
Initan	Bgy
Katipunan	Bgy
Lagdungan	Bgy
Lahug	Bgy
Libertad	Bgy
Mabini	Bgy
Maliao	Bgy
Malitbog	Bgy
Minan	Bgy
Nayawan	Bgy
Poblacion	Bgy
Rizal Norte	Bgy
Rizal Sur	Bgy
Roosevelt	Bgy
Roxas	Bgy
Salong	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Jose	Bgy
San Julian	Bgy
San Miguel Ilawod	Bgy
San Miguel Ilaya	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Ana	Bgy
Santa Petronila	Bgy
Senonod	Bgy
Siya	Bgy
Switch	Bgy
Tabon	Bgy
Tacayan	Bgy
Taft	Bgy
Taganghin	Bgy
Taslan	Bgy
Wright	Bgy
Iloilo	Prov
Ajuy	Mun
Adcadarao	Bgy
Agbobolo	Bgy
Badiangan	Bgy
Barrido	Bgy
Bato Biasong	Bgy
Bay-ang	Bgy
Bucana Bunglas	Bgy
Central	Bgy
Culasi	Bgy
Lanjagan	Bgy
Luca	Bgy
Malayu-an	Bgy
Mangorocoro	Bgy
Nasidman	Bgy
Pantalan Nabaye	Bgy
Pantalan Navarro	Bgy
Pedada	Bgy
Pili	Bgy
Pinantan Diel	Bgy
Pinantan Elizalde	Bgy
Pinay Espinosa	Bgy
Poblacion	Bgy
Progreso	Bgy
Puente Bunglas	Bgy
Punta Buri	Bgy
Rojas	Bgy
San Antonio	Bgy
Silagon	Bgy
Santo Rosario	Bgy
Tagubanhan	Bgy
Taguhangin	Bgy
Tanduyan	Bgy
Tipacla	Bgy
Tubogan	Bgy
Alimodian	Mun
Abang-abang	Bgy
Agsing	Bgy
Atabay	Bgy
Ba-ong	Bgy
Baguingin-Lanot	Bgy
Bagsakan	Bgy
Bagumbayan-Ilajas	Bgy
Balabago	Bgy
Ban-ag	Bgy
Bancal	Bgy
Binalud	Bgy
Bugang	Bgy
Buhay	Bgy
Bulod	Bgy
Cabacanan Proper	Bgy
Cabacanan Rizal	Bgy
Cagay	Bgy
Coline	Bgy
Coline-Dalag	Bgy
Cunsad	Bgy
Cuyad	Bgy
Dalid	Bgy
Dao	Bgy
Gines	Bgy
Ginomoy	Bgy
Ingwan	Bgy
Laylayan	Bgy
Lico	Bgy
Luan-luan	Bgy
Malamhay	Bgy
Malamboy-Bondolan	Bgy
Mambawi	Bgy
Manasa	Bgy
Manduyog	Bgy
Pajo	Bgy
Pianda-an Norte	Bgy
Pianda-an Sur	Bgy
Punong	Bgy
Quinaspan	Bgy
Sinamay	Bgy
Sulong	Bgy
Taban-Manguining	Bgy
Tabug	Bgy
Tarug	Bgy
Tugaslon	Bgy
Ubodan	Bgy
Ugbo	Bgy
Ulay-Bugang	Bgy
Ulay-Hinablan	Bgy
Umingan	Bgy
Poblacion	Bgy
Anilao	Mun
Agbatuan	Bgy
Badiang	Bgy
Balabag	Bgy
Balunos	Bgy
Cag-an	Bgy
Camiros	Bgy
Sambag Culob	Bgy
Dangula-an	Bgy
Guipis	Bgy
Manganese	Bgy
Medina	Bgy
Mostro	Bgy
Palaypay	Bgy
Pantalan	Bgy
Poblacion	Bgy
San Carlos	Bgy
San Juan Crisostomo	Bgy
Santa Rita	Bgy
Santo Rosario	Bgy
Serallo	Bgy
Vista Alegre	Bgy
Badiangan	Mun
Agusipan	Bgy
Astorga	Bgy
Bita-oyan	Bgy
Botong	Bgy
Budiawe	Bgy
Cabanga-an	Bgy
Cabayogan	Bgy
Calansanan	Bgy
Catubig	Bgy
Guinawahan	Bgy
Ilongbukid	Bgy
Indorohan	Bgy
Iniligan	Bgy
Latawan	Bgy
Linayuan	Bgy
Mainguit	Bgy
Malublub	Bgy
Manaolan	Bgy
Mapili Grande	Bgy
Mapili Sanjo	Bgy
Odiongan	Bgy
Poblacion	Bgy
San Julian	Bgy
Sariri	Bgy
Sianon	Bgy
Sinuagan	Bgy
Talaba	Bgy
Tamocol	Bgy
Teneclan	Bgy
Tina	Bgy
Bingauan	Bgy
Balasan	Mun
Aranjuez	Bgy
Bacolod	Bgy
Balanti-an	Bgy
Batuan	Bgy
Cabalic	Bgy
Camambugan	Bgy
Dolores	Bgy
Gimamanay	Bgy
Ipil	Bgy
Kinalkalan	Bgy
Lawis	Bgy
Malapoc	Bgy
Mamhut Norte	Bgy
Mamhut Sur	Bgy
Maya	Bgy
Pani-an	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Quiasan	Bgy
Salong	Bgy
Salvacion	Bgy
Tingui-an	Bgy
Zaragosa	Bgy
Banate	Mun
Alacaygan	Bgy
Bariga	Bgy
Belen	Bgy
Bobon	Bgy
Bularan	Bgy
Carmelo	Bgy
De La Paz	Bgy
Dugwakan	Bgy
Juanico	Bgy
Libertad	Bgy
Magdalo	Bgy
Managopaya	Bgy
Merced	Bgy
Poblacion	Bgy
San Salvador	Bgy
Talokgangan	Bgy
Zona Sur	Bgy
Fuentes*	Bgy
Barotac Nuevo	Mun
Acuit	Bgy
Agcuyawan Calsada	Bgy
Agcuyawan Pulo	Bgy
Bagongbong	Bgy
Baras	Bgy
Bungca	Bgy
Cabilauan	Bgy
Cruz	Bgy
Guintas	Bgy
Igbong	Bgy
Ilaud Poblacion	Bgy
Ilaya Poblacion	Bgy
Jalaud	Bgy
Lagubang	Bgy
Lanas	Bgy
Lico-an	Bgy
Linao	Bgy
Monpon	Bgy
Palaciawan	Bgy
Patag	Bgy
Salihid	Bgy
So-ol	Bgy
Sohoton	Bgy
Tabuc-Suba	Bgy
Tabucan	Bgy
Talisay	Bgy
Tinorian	Bgy
Tiwi	Bgy
Tubungan	Bgy
Barotac Viejo	Mun
Bugnay	Bgy
California	Bgy
Del Pilar	Bgy
De la Peña	Bgy
General Luna	Bgy
La Fortuna	Bgy
Lipata	Bgy
Natividad	Bgy
Nueva Invencion	Bgy
Nueva Sevilla	Bgy
Poblacion	Bgy
Puerto Princesa	Bgy
Rizal	Bgy
San Antonio	Bgy
San Fernando	Bgy
San Francisco	Bgy
San Geronimo	Bgy
San Juan	Bgy
San Lucas	Bgy
San Miguel	Bgy
San Roque	Bgy
Santiago	Bgy
Santo Domingo	Bgy
Santo Tomas	Bgy
Ugasan	Bgy
Vista Alegre	Bgy
Batad	Mun
Alapasco	Bgy
Alinsolong	Bgy
Banban	Bgy
Batad Viejo	Bgy
Binon-an	Bgy
Bolhog	Bgy
Bulak Norte	Bgy
Bulak Sur	Bgy
Cabagohan	Bgy
Calangag	Bgy
Caw-i	Bgy
Drancalan	Bgy
Embarcadero	Bgy
Hamod	Bgy
Malico	Bgy
Nangka	Bgy
Pasayan	Bgy
Poblacion	Bgy
Quiazan Florete	Bgy
Quiazan Lopez	Bgy
Salong	Bgy
Santa Ana	Bgy
Tanao	Bgy
Tapi-an	Bgy
Bingawan	Mun
Agba-o	Bgy
Alabidhan	Bgy
Bulabog	Bgy
Cairohan	Bgy
Guinhulacan	Bgy
Inamyungan	Bgy
Malitbog Ilawod	Bgy
Malitbog Ilaya	Bgy
Ngingi-an	Bgy
Poblacion	Bgy
Quinangyana	Bgy
Quinar-Upan	Bgy
Tapacon	Bgy
Tubod	Bgy
Cabatuan	Mun
Acao	Bgy
Amerang	Bgy
Amurao	Bgy
Anuang	Bgy
Ayaman	Bgy
Ayong	Bgy
Bacan	Bgy
Balabag	Bgy
Baluyan	Bgy
Banguit	Bgy
Bulay	Bgy
Cadoldolan	Bgy
Cagban	Bgy
Calawagan	Bgy
Calayo	Bgy
Duyanduyan	Bgy
Gaub	Bgy
Gines Interior	Bgy
Gines Patag	Bgy
Guibuangan Tigbauan	Bgy
Inabasan	Bgy
Inaca	Bgy
Inaladan	Bgy
Ingas	Bgy
Ito Norte	Bgy
Ito Sur	Bgy
Janipaan Central	Bgy
Janipaan Este	Bgy
Janipaan Oeste	Bgy
Janipaan Olo	Bgy
Jelicuon Lusaya	Bgy
Jelicuon Montinola	Bgy
Lag-an	Bgy
Leong	Bgy
Lutac	Bgy
Manguna	Bgy
Maraguit	Bgy
Morubuan	Bgy
Pacatin	Bgy
Pagotpot	Bgy
Pamul-Ogan	Bgy
Pamuringao Proper	Bgy
Pamuringao Garrido	Bgy
Zone I Pob.	Bgy
Zone X Pob.	Bgy
Zone XI Pob.	Bgy
Zone II Pob.	Bgy
Zone III Pob.	Bgy
Zone IV Pob.	Bgy
Zone V Pob.	Bgy
Zone VI Pob.	Bgy
Zone VII Pob.	Bgy
Zone VIII Pob.	Bgy
Zone IX Pob.	Bgy
Pungtod	Bgy
Puyas	Bgy
Salacay	Bgy
Sulanga	Bgy
Tabucan	Bgy
Tacdangan	Bgy
Talanghauan	Bgy
Tigbauan Road	Bgy
Tinio-an	Bgy
Tiring	Bgy
Tupol Central	Bgy
Tupol Este	Bgy
Tupol Oeste	Bgy
Tuy-an	Bgy
Calinog	Mun
Agcalaga	Bgy
Aglibacao	Bgy
Aglonok	Bgy
Alibunan	Bgy
Badlan Grande	Bgy
Badlan Pequeño	Bgy
Badu	Bgy
Balaticon	Bgy
Banban Grande	Bgy
Banban Pequeño	Bgy
Binolosan Grande	Bgy
Binolosan Pequeño	Bgy
Cabagiao	Bgy
Cabugao	Bgy
Cahigon	Bgy
Barrio Calinog	Bgy
Camalongo	Bgy
Canabajan	Bgy
Caratagan	Bgy
Carvasana	Bgy
Dalid	Bgy
Datagan	Bgy
Gama Grande	Bgy
Gama Pequeño	Bgy
Garangan	Bgy
Guinbonyugan	Bgy
Guiso	Bgy
Hilwan	Bgy
Impalidan	Bgy
Ipil	Bgy
Jamin-ay	Bgy
Lampaya	Bgy
Libot	Bgy
Lonoy	Bgy
Malaguinabot	Bgy
Malapawe	Bgy
Malitbog Centro	Bgy
Mambiranan	Bgy
Manaripay	Bgy
Marandig	Bgy
Masaroy	Bgy
Maspasan	Bgy
Nalbugan	Bgy
Owak	Bgy
Poblacion Centro	Bgy
Poblacion Delgado	Bgy
Poblacion Rizal Ilaud	Bgy
Poblacion Ilaya	Bgy
Baje San Julian	Bgy
San Nicolas	Bgy
Simsiman	Bgy
Tabucan	Bgy
Tahing	Bgy
Tibiao	Bgy
Tigbayog	Bgy
Toyungan	Bgy
Ulayan	Bgy
Malag-it	Bgy
Supanga	Bgy
Carles	Mun
Abong	Bgy
Alipata	Bgy
Asluman	Bgy
Bancal	Bgy
Barangcalan	Bgy
Barosbos	Bgy
Punta Batuanan	Bgy
Binuluangan	Bgy
Bito-on	Bgy
Bolo	Bgy
Buaya	Bgy
Buenavista	Bgy
Isla De Cana	Bgy
Cabilao Grande	Bgy
Cabilao Pequeño	Bgy
Cabuguana	Bgy
Cawayan	Bgy
Dayhagan	Bgy
Gabi	Bgy
Granada	Bgy
Guinticgan	Bgy
Lantangan	Bgy
Manlot	Bgy
Nalumsan	Bgy
Pantalan	Bgy
Poblacion	Bgy
Punta	Bgy
San Fernando	Bgy
Tabugon	Bgy
Talingting	Bgy
Tarong	Bgy
Tinigban	Bgy
Tupaz	Bgy
Concepcion	Mun
Aglosong	Bgy
Agnaga	Bgy
Bacjawan Norte	Bgy
Bacjawan Sur	Bgy
Bagongon	Bgy
Batiti	Bgy
Botlog	Bgy
Calamigan	Bgy
Dungon	Bgy
Igbon	Bgy
Jamul-Awon	Bgy
Lo-ong	Bgy
Macalbang	Bgy
Macatunao	Bgy
Malangabang	Bgy
Maliogliog	Bgy
Niño	Bgy
Nipa	Bgy
Plandico	Bgy
Poblacion	Bgy
Polopina	Bgy
Salvacion	Bgy
Talotu-an	Bgy
Tambaliza	Bgy
Tamis-ac	Bgy
Dingle	Mun
Abangay	Bgy
Agsalanan	Bgy
Agtatacay	Bgy
Alegria	Bgy
Bongloy	Bgy
Buenavista	Bgy
Caguyuman	Bgy
Calicuang	Bgy
Camambugan	Bgy
Dawis	Bgy
Ginalinan Nuevo	Bgy
Ginalinan Viejo	Bgy
Gutao	Bgy
Ilajas	Bgy
Libo-o	Bgy
Licu-an	Bgy
Lincud	Bgy
Matangharon	Bgy
Moroboro	Bgy
Namatay	Bgy
Nazuni	Bgy
Pandan	Bgy
Poblacion	Bgy
Potolan	Bgy
San Jose	Bgy
San Matias	Bgy
Siniba-an	Bgy
Tabugon	Bgy
Tambunac	Bgy
Tanghawan	Bgy
Tiguib	Bgy
Tinocuan	Bgy
Tulatula-an	Bgy
Dueñas	Mun
Agutayan	Bgy
Angare	Bgy
Anjawan	Bgy
Baac	Bgy
Bagongbong	Bgy
Balangigan	Bgy
Balingasag	Bgy
Banugan	Bgy
Batuan	Bgy
Bita	Bgy
Buenavista	Bgy
Bugtongan	Bgy
Cabudian	Bgy
Calaca-an	Bgy
Calang	Bgy
Calawinan	Bgy
Capaycapay	Bgy
Capuling	Bgy
Catig	Bgy
Dila-an	Bgy
Fundacion	Bgy
Inadlawan	Bgy
Jagdong	Bgy
Jaguimit	Bgy
Lacadon	Bgy
Luag	Bgy
Malusgod	Bgy
Maribuyong	Bgy
Minanga	Bgy
Monpon	Bgy
Navalas	Bgy
Pader	Bgy
Pandan	Bgy
Ponong Grande	Bgy
Ponong Pequeño	Bgy
Purog	Bgy
Romblon	Bgy
San Isidro	Bgy
Santo Niño	Bgy
Sawe	Bgy
Taminla	Bgy
Tinocuan	Bgy
Tipolo	Bgy
Poblacion A	Bgy
Poblacion B	Bgy
Poblacion C	Bgy
Poblacion D	Bgy
Dumangas	Mun
Bacay	Bgy
Bacong	Bgy
Balabag	Bgy
Balud	Bgy
Bantud	Bgy
Bantud Fabrica	Bgy
Baras	Bgy
Barasan	Bgy
Bolilao	Bgy
Calao	Bgy
Cali	Bgy
Cansilayan	Bgy
Capaliz	Bgy
Cayos	Bgy
Compayan	Bgy
Dacutan	Bgy
Ermita	Bgy
Pd Monfort South	Bgy
Ilaya 1st	Bgy
Ilaya 2nd	Bgy
Ilaya 3rd	Bgy
Jardin	Bgy
Lacturan	Bgy
Pd Monfort North	Bgy
Managuit	Bgy
Maquina	Bgy
Nanding Lopez	Bgy
Pagdugue	Bgy
Paloc Bigque	Bgy
Paloc Sool	Bgy
Patlad	Bgy
Pulao	Bgy
Rosario	Bgy
Sapao	Bgy
Sulangan	Bgy
Tabucan	Bgy
Talusan	Bgy
Tambobo	Bgy
Tamboilan	Bgy
Victorias	Bgy
Burgos-Regidor (Pob.)	Bgy
Aurora-del Pilar (Pob.)	Bgy
Buenaflor Embarkadero (Pob.)	Bgy
Lopez Jaena - Rizal (Pob.)	Bgy
Basa-Mabini Bonifacio (Pob.)	Bgy
Estancia	Mun
Lumbia	Bgy
Bayas	Bgy
Bayuyan	Bgy
Botongon	Bgy
Bulaqueña	Bgy
Calapdan	Bgy
Cano-an	Bgy
Daan Banua	Bgy
Daculan	Bgy
Gogo	Bgy
Jolog	Bgy
Loguingot	Bgy
Malbog	Bgy
Manipulon	Bgy
Pa-on	Bgy
Villa Pani-an	Bgy
Poblacion Zone 1	Bgy
Lonoy	Bgy
San Roque	Bgy
Santa Ana	Bgy
Tabu-an	Bgy
Tacbuyan	Bgy
Tanza	Bgy
Poblacion Zone II	Bgy
Poblacion Zone III	Bgy
Guimbal	Mun
Anono-o	Bgy
Bacong	Bgy
Bagumbayan Pob.	Bgy
Balantad-Carlos Fruto (Pob.)	Bgy
Baras	Bgy
Binanua-an	Bgy
Torreblanca-Blumentritt (Pob.)	Bgy
Bongol San Miguel	Bgy
Bongol San Vicente	Bgy
Bulad	Bgy
Buluangan	Bgy
Burgos-Gengos (Pob.)	Bgy
Cabasi	Bgy
Cabubugan	Bgy
Calampitao	Bgy
Camangahan	Bgy
Generosa-Cristobal Colon (Pob.)	Bgy
Gerona-Gimeno (Pob.)	Bgy
Girado-Magsaysay (Pob.)	Bgy
Gotera (Pob.)	Bgy
Igcocolo	Bgy
Libo-on Gonzales (Pob.)	Bgy
Lubacan	Bgy
Nahapay	Bgy
Nalundan	Bgy
Nanga	Bgy
Nito-an Lupsag	Bgy
Particion	Bgy
Pescadores (Pob.)	Bgy
Rizal-Tuguisan (Pob.)	Bgy
Sipitan-Badiang	Bgy
Iyasan	Bgy
Santa Rosa-Laguna	Bgy
Igbaras	Mun
Alameda	Bgy
Amorogtong	Bgy
Anilawan	Bgy
Bagacay	Bgy
Bagacayan	Bgy
Bagay	Bgy
Balibagan	Bgy
Barasan	Bgy
Binanua-an	Bgy
Boclod	Bgy
Buenavista	Bgy
Buga	Bgy
Bugnay	Bgy
Calampitao	Bgy
Cale	Bgy
Corucuan	Bgy
Catiringan	Bgy
Igcabugao	Bgy
Igpigus	Bgy
Igtalongon	Bgy
Indaluyon	Bgy
Jovellar	Bgy
Kinagdan	Bgy
Lab-on	Bgy
Lacay Dol-Dol	Bgy
Lumangan	Bgy
Lutungan	Bgy
Mantangon	Bgy
Mulangan	Bgy
Pasong	Bgy
Passi	Bgy
Pinaopawan	Bgy
Barangay 1 Poblacion	Bgy
Barangay 2 Poblacion	Bgy
Barangay 3 Poblacion	Bgy
Barangay 4 Poblacion	Bgy
Barangay 5 Poblacion	Bgy
Barangay 6 Poblacion	Bgy
Riro-an	Bgy
San Ambrosio	Bgy
Santa Barbara	Bgy
Signe	Bgy
Tabiac	Bgy
Talayatay	Bgy
Taytay	Bgy
Tigbanaba	Bgy
City of Iloilo (Capital)	City
Santa Cruz	Bgy
Aguinaldo	Bgy
Airport	Bgy
Alalasan Lapuz	Bgy
Arguelles	Bgy
Arsenal Aduana	Bgy
North Avanceña	Bgy
Bakhaw	Bgy
Balabago	Bgy
Balantang	Bgy
Baldoza	Bgy
Sinikway	Bgy
Bantud	Bgy
Banuyao	Bgy
Baybay Tanza	Bgy
Benedicto	Bgy
Bito-on	Bgy
Monica Blumentritt	Bgy
Bolilao	Bgy
Bonifacio Tanza	Bgy
Bonifacio	Bgy
Buhang	Bgy
Buhang Taft North	Bgy
Buntatala	Bgy
Seminario	Bgy
Caingin	Bgy
Calahunan	Bgy
Calaparan	Bgy
Calumpang	Bgy
Camalig	Bgy
El 98 Castilla	Bgy
Cochero	Bgy
Compania	Bgy
Concepcion-Montes	Bgy
Cuartero	Bgy
Cubay	Bgy
Danao	Bgy
Mabolo-Delgado	Bgy
Democracia	Bgy
Desamparados	Bgy
Divinagracia	Bgy
Don Esteban-Lapuz	Bgy
Dulonan	Bgy
Dungon	Bgy
Dungon A	Bgy
Dungon B	Bgy
East Baluarte	Bgy
East Timawa	Bgy
Edganzon	Bgy
Tanza-Esperanza	Bgy
Fajardo	Bgy
Flores	Bgy
South Fundidor	Bgy
General Hughes-Montes	Bgy
Gloria	Bgy
Gustilo	Bgy
Guzman-Jesena	Bgy
Habog-habog Salvacion	Bgy
Hibao-an Sur	Bgy
Hinactacan	Bgy
Hipodromo	Bgy
Inday	Bgy
Infante	Bgy
Ingore	Bgy
Jalandoni Estate-Lapuz	Bgy
Jalandoni-Wilson	Bgy
Delgado-Jalandoni-Bagumbayan	Bgy
Javellana	Bgy
Jereos	Bgy
Calubihan	Bgy
Kasingkasing	Bgy
Katilingban	Bgy
Kauswagan	Bgy
Our Lady Of Fatima	Bgy
Laguda	Bgy
Lanit	Bgy
Lapuz Norte	Bgy
Lapuz Sur	Bgy
Legaspi dela Rama	Bgy
Liberation	Bgy
Libertad, Santa Isabel	Bgy
Libertad-Lapuz	Bgy
Lopez Jaena	Bgy
Loboc-Lapuz	Bgy
Lopez Jaena Norte	Bgy
Lopez Jaena Sur	Bgy
Luna	Bgy
M. V. Hechanova	Bgy
Burgos-Mabini-Plaza	Bgy
Macarthur	Bgy
Magdalo	Bgy
Magsaysay	Bgy
Magsaysay Village	Bgy
Malipayon-Delgado	Bgy
Mansaya-Lapuz	Bgy
Marcelo H. del Pilar	Bgy
Maria Clara	Bgy
Maria Cristina	Bgy
Mohon	Bgy
Molo Boulevard	Bgy
Montinola	Bgy
Muelle Loney-Montes	Bgy
Nabitasan	Bgy
Navais	Bgy
Nonoy	Bgy
North Fundidor	Bgy
North Baluarte	Bgy
North San Jose	Bgy
Oñate de Leon	Bgy
Obrero-Lapuz	Bgy
Ortiz	Bgy
Osmeña	Bgy
Our Lady Of Lourdes	Bgy
Rizal Palapala I	Bgy
Rizal Palapala II	Bgy
PHHC Block 17	Bgy
PHHC Block 22 NHA	Bgy
Poblacion Molo	Bgy
President Roxas	Bgy
Progreso-Lapuz	Bgy
Punong-Lapuz	Bgy
Quezon	Bgy
Quintin Salas	Bgy
Rima-Rizal	Bgy
Rizal Estanzuela	Bgy
Rizal Ibarra	Bgy
Railway	Bgy
Roxas Village	Bgy
Sambag	Bgy
Sampaguita	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Felix	Bgy
San Isidro	Bgy
Hibao-an Norte	Bgy
San Jose	Bgy
San Jose	Bgy
San Juan	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Filomena	Bgy
Santa Rosa	Bgy
Santo Domingo	Bgy
Santo Niño Norte	Bgy
Santo Niño Sur	Bgy
Santo Rosario-Duran	Bgy
Simon Ledesma	Bgy
So-oc	Bgy
South Baluarte	Bgy
South San Jose	Bgy
Taal	Bgy
Tabuc Suba	Bgy
Tabucan	Bgy
Tacas	Bgy
Abeto Mirasol Taft South	Bgy
Tagbac	Bgy
Tap-oc	Bgy
Taytay Zone II	Bgy
Ticud	Bgy
Timawa Tanza I	Bgy
Timawa Tanza II	Bgy
Ungka	Bgy
Veterans Village	Bgy
Villa Anita	Bgy
West Habog-habog	Bgy
West Timawa	Bgy
Yulo-Arroyo	Bgy
Yulo Drive	Bgy
Zamora-Melliza	Bgy
Pale Benedicto Rizal	Bgy
Kahirupan	Bgy
Luna	Bgy
San Isidro	Bgy
San Jose	Bgy
Tabuc Suba	Bgy
Rizal	Bgy
Janiuay	Mun
Abangay	Bgy
Agcarope	Bgy
Aglobong	Bgy
Aguingay	Bgy
Anhawan	Bgy
Atimonan	Bgy
Balanac	Bgy
Barasalon	Bgy
Bongol	Bgy
Cabantog	Bgy
Calmay	Bgy
Canawili	Bgy
Canawillian	Bgy
Caranas	Bgy
Caraudan	Bgy
Carigangan	Bgy
Cunsad	Bgy
Dabong	Bgy
Damires	Bgy
Damo-ong	Bgy
Danao	Bgy
Gines	Bgy
Guadalupe	Bgy
Jibolo	Bgy
Kuyot	Bgy
Madong	Bgy
Manacabac	Bgy
Mangil	Bgy
Matag-ub	Bgy
Monte-Magapa	Bgy
Pangilihan	Bgy
Panuran	Bgy
Pararinga	Bgy
Patong-patong	Bgy
Quipot	Bgy
Santo Tomas	Bgy
Sarawag	Bgy
Tambal	Bgy
Tamu-an	Bgy
Tiringanan	Bgy
Tolarucan	Bgy
Tuburan	Bgy
Ubian	Bgy
Yabon	Bgy
Aquino Nobleza East (Pob.)	Bgy
Aquino Nobleza West (Pob.)	Bgy
R. Armada (Pob.)	Bgy
Concepcion Pob.	Bgy
Golgota (Pob.)	Bgy
Locsin (Pob.)	Bgy
Don T. Lutero Center (Pob.)	Bgy
Don T. Lutero East (Pob.)	Bgy
Don T. Lutero West Pob.	Bgy
Crispin Salazar North (Pob.)	Bgy
Crispin Salazar South (Pob.)	Bgy
San Julian (Pob.)	Bgy
San Pedro (Pob.)	Bgy
Santa Rita (Pob.)	Bgy
Capt. A. Tirador (Pob.)	Bgy
S. M. Villa (Pob.)	Bgy
Lambunao	Mun
Agsirab	Bgy
Agtuman	Bgy
Alugmawa	Bgy
Badiangan	Bgy
Bogongbong	Bgy
Balagiao	Bgy
Banban	Bgy
Bansag	Bgy
Bayuco	Bgy
Binaba-an Armada	Bgy
Binaba-an Labayno	Bgy
Binaba-an Limoso	Bgy
Binaba-an Portigo	Bgy
Binaba-an Tirador	Bgy
Bonbon	Bgy
Bontoc	Bgy
Buri	Bgy
Burirao	Bgy
Buwang	Bgy
Cabatangan	Bgy
Cabugao	Bgy
Cabunlawan	Bgy
Caguisanan	Bgy
Caloy-Ahan	Bgy
Caninguan	Bgy
Capangyan	Bgy
Cayan Este	Bgy
Cayan Oeste	Bgy
Corot-on	Bgy
Coto	Bgy
Cubay	Bgy
Cunarum	Bgy
Daanbanwa	Bgy
Gines	Bgy
Hipgos	Bgy
Jayubo	Bgy
Jorog	Bgy
Lanot Grande	Bgy
Lanot Pequeño	Bgy
Legayada	Bgy
Lumanay	Bgy
Madarag	Bgy
Magbato	Bgy
Maite Grande	Bgy
Maite Pequeño	Bgy
Malag-it	Bgy
Manaulan	Bgy
Maribong	Bgy
Marong	Bgy
Misi	Bgy
Natividad	Bgy
Pajo	Bgy
Pandan	Bgy
Panuran	Bgy
Pasig	Bgy
Patag	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Poong	Bgy
Pughanan	Bgy
Pungsod	Bgy
Quiling	Bgy
Sagcup	Bgy
San Gregorio	Bgy
Sibacungan	Bgy
Sibaguan	Bgy
Simsiman	Bgy
Supoc	Bgy
Tampucao	Bgy
Tranghawan	Bgy
Tubungan	Bgy
Tuburan	Bgy
Walang	Bgy
Leganes	Mun
M.V. Hechanova	Bgy
Bigke	Bgy
Buntatala	Bgy
Cagamutan Norte	Bgy
Cagamutan Sur	Bgy
Calaboa	Bgy
Camangay	Bgy
Cari Mayor	Bgy
Cari Minor	Bgy
Gua-an	Bgy
Guihaman	Bgy
Guinobatan	Bgy
Guintas	Bgy
Lapayon	Bgy
Nabitasan	Bgy
Napnud	Bgy
Poblacion	Bgy
San Vicente	Bgy
Lemery	Mun
Agpipili	Bgy
Alcantara	Bgy
Almeñana	Bgy
Anabo	Bgy
Bankal	Bgy
Buenavista	Bgy
Cabantohan	Bgy
Capiñahan	Bgy
Dalipe	Bgy
Dapdapan	Bgy
Gerongan	Bgy
Imbaulan	Bgy
Layogbato	Bgy
Marapal	Bgy
Milan	Bgy
Nagsulang	Bgy
Nasapahan	Bgy
Omio	Bgy
Pacuan	Bgy
Poblacion NW Zone	Bgy
Poblacion SE Zone	Bgy
Pontoc	Bgy
San Antonio	Bgy
San Diego	Bgy
San Jose Moto	Bgy
Sepanton	Bgy
Sincua	Bgy
Tabunan	Bgy
Tugas	Bgy
Velasco	Bgy
Yawyawan	Bgy
Leon	Mun
Agboy Norte	Bgy
Agboy Sur	Bgy
Agta	Bgy
Ambulong	Bgy
Anonang	Bgy
Apian	Bgy
Avanzada	Bgy
Awis	Bgy
Ayabang	Bgy
Ayubo	Bgy
Bacolod	Bgy
Baje	Bgy
Banagan	Bgy
Barangbang	Bgy
Barasan	Bgy
Bayag Norte	Bgy
Bayag Sur	Bgy
Binolbog	Bgy
Biri Norte	Bgy
Biri Sur	Bgy
Bobon	Bgy
Bucari	Bgy
Buenavista	Bgy
Buga	Bgy
Bulad	Bgy
Bulwang	Bgy
Cabolo-an	Bgy
Cabunga-an	Bgy
Cabutongan	Bgy
Cagay	Bgy
Camandag	Bgy
Camando	Bgy
Cananaman	Bgy
Capt. Fernando	Bgy
Carara-an	Bgy
Carolina	Bgy
Cawilihan	Bgy
Coyugan Norte	Bgy
Coyugan Sur	Bgy
Danao	Bgy
Dorog	Bgy
Dusacan	Bgy
Gines	Bgy
Gumboc	Bgy
Igcadios	Bgy
Ingay	Bgy
Isian Norte	Bgy
Isian Victoria	Bgy
Jamog Gines	Bgy
Lanag	Bgy
Lang-og	Bgy
Ligtos	Bgy
Lonoc	Bgy
Lampaya	Bgy
Magcapay	Bgy
Maliao	Bgy
Malublub	Bgy
Manampunay	Bgy
Marirong	Bgy
Mina	Bgy
Mocol	Bgy
Nagbangi	Bgy
Nalbang	Bgy
Odong-odong	Bgy
Oluangan	Bgy
Omambong	Bgy
Paoy	Bgy
Pandan	Bgy
Panginman	Bgy
Pepe	Bgy
Poblacion	Bgy
Paga	Bgy
Salngan	Bgy
Samlague	Bgy
Siol Norte	Bgy
Siol Sur	Bgy
Tacuyong Norte	Bgy
Tacuyong Sur	Bgy
Tagsing	Bgy
Talacuan	Bgy
Ticuan	Bgy
Tina-an Norte	Bgy
Tina-an Sur	Bgy
Tunguan	Bgy
Tu-og	Bgy
Maasin	Mun
Abay	Bgy
Abilay	Bgy
AGROCEL Pob.	Bgy
Amerang	Bgy
Bagacay East	Bgy
Bagacay West	Bgy
Bug-ot	Bgy
Bolo	Bgy
Bulay	Bgy
Buntalan	Bgy
Burak	Bgy
Cabangcalan	Bgy
Cabatac	Bgy
Caigon	Bgy
Cananghan	Bgy
Canawili	Bgy
Dagami	Bgy
Daja	Bgy
Dalusan	Bgy
DELCAR Pob.	Bgy
Inabasan	Bgy
Layog	Bgy
Liñagan Calsada	Bgy
Liñagan Tacas	Bgy
Linab	Bgy
MARI Pob.	Bgy
Magsaysay	Bgy
Mandog	Bgy
Miapa	Bgy
Nagba	Bgy
Nasaka	Bgy
Naslo-Bucao	Bgy
Nasuli	Bgy
Panalian	Bgy
Piandaan East	Bgy
Piandaan West	Bgy
Pispis	Bgy
Punong	Bgy
Sinubsuban	Bgy
Siwalo	Bgy
Santa Rita	Bgy
Subog	Bgy
THTP Pob.	Bgy
Tigbauan	Bgy
Trangka	Bgy
Tubang	Bgy
Tulahong	Bgy
Tuy-an East	Bgy
Tuy-an West	Bgy
Ubian	Bgy
Miagao	Mun
Agdum	Bgy
Aguiauan	Bgy
Alimodias	Bgy
Awang	Bgy
Oya-oy	Bgy
Bacauan	Bgy
Bacolod	Bgy
Bagumbayan	Bgy
Banbanan	Bgy
Banga	Bgy
Bangladan	Bgy
Banuyao	Bgy
Baraclayan	Bgy
Bariri	Bgy
Baybay Norte (Pob.)	Bgy
Baybay Sur (Pob.)	Bgy
Belen	Bgy
Bolho (Pob.)	Bgy
Bolocaue	Bgy
Buenavista Norte	Bgy
Buenavista Sur	Bgy
Bugtong Lumangan	Bgy
Bugtong Naulid	Bgy
Cabalaunan	Bgy
Cabangcalan	Bgy
Cabunotan	Bgy
Cadoldolan	Bgy
Cagbang	Bgy
Caitib	Bgy
Calagtangan	Bgy
Calampitao	Bgy
Cavite	Bgy
Cawayanan	Bgy
Cubay	Bgy
Cubay Ubos	Bgy
Dalije	Bgy
Damilisan	Bgy
Dawog	Bgy
Diday	Bgy
Dingle	Bgy
Durog	Bgy
Frantilla	Bgy
Fundacion	Bgy
Gines	Bgy
Guibongan	Bgy
Igbita	Bgy
Igbugo	Bgy
Igcabidio	Bgy
Igcabito-on	Bgy
Igcatambor	Bgy
Igdalaquit	Bgy
Igdulaca	Bgy
Igpajo	Bgy
Igpandan	Bgy
Igpuro	Bgy
Igpuro-Bariri	Bgy
Igsoligue	Bgy
Igtuba	Bgy
Ilog-ilog	Bgy
Indag-an	Bgy
Kirayan Norte	Bgy
Kirayan Sur	Bgy
Kirayan Tacas	Bgy
La Consolacion	Bgy
Lacadon	Bgy
Lanutan	Bgy
Lumangan	Bgy
Mabayan	Bgy
Maduyo	Bgy
Malagyan	Bgy
Mambatad	Bgy
Maninila	Bgy
Maricolcol	Bgy
Maringyan	Bgy
Mat-y (Pob.)	Bgy
Matalngon	Bgy
Naclub	Bgy
Nam-o Sur	Bgy
Nam-o Norte	Bgy
Narat-an	Bgy
Narorogan	Bgy
Naulid	Bgy
Olango	Bgy
Ongyod	Bgy
Onop	Bgy
Oyungan	Bgy
Palaca	Bgy
Paro-on	Bgy
Potrido	Bgy
Pudpud	Bgy
Pungtod Monteclaro	Bgy
Pungtod Naulid	Bgy
Sag-on	Bgy
San Fernando	Bgy
San Jose	Bgy
San Rafael	Bgy
Sapa	Bgy
Saring	Bgy
Sibucao	Bgy
Taal	Bgy
Tabunacan	Bgy
Tacas (Pob.)	Bgy
Tambong	Bgy
Tan-agan	Bgy
Tatoy	Bgy
Ticdalan	Bgy
Tig-amaga	Bgy
Tig-Apog-Apog	Bgy
Tigbagacay	Bgy
Tiglawa	Bgy
Tigmalapad	Bgy
Tigmarabo	Bgy
To-og	Bgy
Tugura-ao	Bgy
Tumagboc	Bgy
Ubos Ilawod (Pob.)	Bgy
Ubos Ilaya (Pob.)	Bgy
Valencia	Bgy
Wayang	Bgy
Mina	Mun
Abat	Bgy
Agmanaphao	Bgy
Amiroy	Bgy
Badiangan	Bgy
Bangac	Bgy
Cabalabaguan	Bgy
Capul-an	Bgy
Dala	Bgy
Guibuangan	Bgy
Janipa-an West	Bgy
Janipa-an East	Bgy
Mina East (Pob.)	Bgy
Mina West (Pob.)	Bgy
Nasirum	Bgy
Naumuan	Bgy
Singay	Bgy
Talibong Grande	Bgy
Talibong Pequeño	Bgy
Tipolo	Bgy
Tolarucan	Bgy
Tumay	Bgy
Yugot	Bgy
New Lucena	Mun
Baclayan	Bgy
Badiang	Bgy
Balabag	Bgy
Bilidan	Bgy
Bita-og Gaja	Bgy
Bololacao	Bgy
Burot	Bgy
Cabilauan	Bgy
Cabugao	Bgy
Cagban	Bgy
Calumbuyan	Bgy
Damires	Bgy
Dawis	Bgy
General Delgado	Bgy
Guinobatan	Bgy
Janipa-an Oeste	Bgy
Jelicuon Este	Bgy
Jelicuon Oeste	Bgy
Pasil	Bgy
Poblacion	Bgy
Wari-wari	Bgy
Oton	Mun
Abilay Norte	Bgy
Abilay Sur	Bgy
Alegre	Bgy
Batuan Ilaud	Bgy
Batuan Ilaya	Bgy
Bita Norte	Bgy
Bita Sur	Bgy
Botong	Bgy
Buray	Bgy
Cabanbanan	Bgy
Caboloan Norte	Bgy
Caboloan Sur	Bgy
Cadinglian	Bgy
Cagbang	Bgy
Calam-isan	Bgy
Galang	Bgy
Lambuyao	Bgy
Mambog	Bgy
Pakiad	Bgy
Poblacion East	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
Poblacion West	Bgy
Polo Maestra Bita	Bgy
Rizal	Bgy
Salngan	Bgy
Sambaludan	Bgy
San Antonio	Bgy
San Nicolas	Bgy
Santa Clara	Bgy
Santa Monica	Bgy
Santa Rita	Bgy
Tagbac Norte	Bgy
Tagbac Sur	Bgy
Trapiche	Bgy
Tuburan	Bgy
Turog-Turog	Bgy
City of Passi	City
Agdahon	Bgy
Agdayao	Bgy
Aglalana	Bgy
Agtabo	Bgy
Agtambo	Bgy
Alimono	Bgy
Arac	Bgy
Ayuyan	Bgy
Bacuranan	Bgy
Bagacay	Bgy
Batu	Bgy
Bayan	Bgy
Bitaogan	Bgy
Buenavista	Bgy
Buyo	Bgy
Cabunga	Bgy
Cadilang	Bgy
Cairohan	Bgy
Dalicanan	Bgy
Gemat-y	Bgy
Gemumua-agahon	Bgy
Gegachac	Bgy
Gines Viejo	Bgy
Imbang Grande	Bgy
Jaguimitan	Bgy
Libo-o	Bgy
Maasin	Bgy
Magdungao	Bgy
Malag-it Grande	Bgy
Malag-it Pequeño	Bgy
Mambiranan Grande	Bgy
Mambiranan Pequeño	Bgy
Man-it	Bgy
Mantulang	Bgy
Mulapula	Bgy
Nueva Union	Bgy
Pangi	Bgy
Pagaypay	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Punong	Bgy
Quinagaringan Grande	Bgy
Quinagaringan Pequeño	Bgy
Sablogon	Bgy
Salngan	Bgy
Santo Tomas	Bgy
Sarapan	Bgy
Tagubong	Bgy
Talongonan	Bgy
Tubod	Bgy
Tuburan	Bgy
Pavia	Mun
Aganan	Bgy
Amparo	Bgy
Anilao	Bgy
Balabag	Bgy
Purok I (Pob.)	Bgy
Purok II (Pob.)	Bgy
Purok III (Pob.)	Bgy
Purok IV (Pob.)	Bgy
Cabugao Norte	Bgy
Cabugao Sur	Bgy
Jibao-an	Bgy
Mali-ao	Bgy
Pagsanga-an	Bgy
Pandac	Bgy
Tigum	Bgy
Ungka I	Bgy
Ungka II	Bgy
Pal-agon	Bgy
Pototan	Mun
Abangay	Bgy
Amamaros	Bgy
Bagacay	Bgy
Barasan	Bgy
Batuan	Bgy
Bongco	Bgy
Cahaguichican	Bgy
Callan	Bgy
Cansilayan	Bgy
Casalsagan	Bgy
Cato-ogan	Bgy
Cau-ayan	Bgy
Culob	Bgy
Danao	Bgy
Dapitan	Bgy
Dawis	Bgy
Dongsol	Bgy
Fundacion	Bgy
Guinacas	Bgy
Guibuangan	Bgy
Igang	Bgy
Intaluan	Bgy
Iwa Ilaud	Bgy
Iwa Ilaya	Bgy
Jamabalud	Bgy
Jebioc	Bgy
Lay-Ahan	Bgy
Primitivo Ledesma Ward (Pob.)	Bgy
Lopez Jaena Ward (Pob.)	Bgy
Lumbo	Bgy
Macatol	Bgy
Malusgod	Bgy
Naslo	Bgy
Nabitasan	Bgy
Naga	Bgy
Nanga	Bgy
Pajo	Bgy
Palanguia	Bgy
Fernando Parcon Ward (Pob.)	Bgy
Pitogo	Bgy
Polot-an	Bgy
Purog	Bgy
Rumbang	Bgy
San Jose Ward (Pob.)	Bgy
Sinuagan	Bgy
Tuburan	Bgy
Tumcon Ilaya	Bgy
Tumcon Ilaud	Bgy
Ubang	Bgy
Zarrague	Bgy
San Dionisio	Mun
Agdaliran	Bgy
Amayong	Bgy
Bagacay	Bgy
Batuan	Bgy
Bondulan	Bgy
Boroñgon	Bgy
Canas	Bgy
Capinang	Bgy
Cubay	Bgy
Cudionan	Bgy
Dugman	Bgy
Hacienda Conchita	Bgy
Madanlog	Bgy
Mandu-awak	Bgy
Moto	Bgy
Naborot	Bgy
Nipa	Bgy
Odiongan	Bgy
Pangi	Bgy
Pase	Bgy
Poblacion	Bgy
San Nicolas	Bgy
Santol	Bgy
Siempreviva	Bgy
Sua	Bgy
Talo-ato	Bgy
Tamangi	Bgy
Tiabas	Bgy
Tuble	Bgy
San Enrique	Mun
Abaca	Bgy
Asisig	Bgy
Bantayan	Bgy
Braulan	Bgy
Cabugao Nuevo	Bgy
Cabugao Viejo	Bgy
Camiri	Bgy
Compo	Bgy
Catan-Agan	Bgy
Cubay	Bgy
Dacal	Bgy
Dumiles	Bgy
Garita	Bgy
Gines Nuevo	Bgy
Imbang Pequeño	Bgy
Imbesad-an	Bgy
Iprog	Bgy
Lip-ac	Bgy
Madarag	Bgy
Mapili	Bgy
Paga	Bgy
Palje	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Quinolpan	Bgy
Rumagayray	Bgy
San Antonio	Bgy
Tambunac	Bgy
San Joaquin	Mun
Amboyu-an	Bgy
Andres Bonifacio	Bgy
Antalon	Bgy
Bad-as	Bgy
Bagumbayan	Bgy
Balabago	Bgy
Baybay	Bgy
Bayunan	Bgy
Bolbogan	Bgy
Bulho	Bgy
Bucaya	Bgy
Cadluman	Bgy
Cadoldolan	Bgy
Camia	Bgy
Camaba-an	Bgy
Cata-an	Bgy
Crossing Dapuyan	Bgy
Cubay	Bgy
Cumarascas	Bgy
Dacdacanan	Bgy
Danawan	Bgy
Doldol	Bgy
Dongoc	Bgy
Escalantera	Bgy
Ginot-an	Bgy
Huna	Bgy
Igbaje	Bgy
Igbangcal	Bgy
Igbinangon	Bgy
Igburi	Bgy
Igcabutong	Bgy
Igcadlum	Bgy
Igcaphang	Bgy
Igcaratong	Bgy
Igcondao	Bgy
Igcores	Bgy
Igdagmay	Bgy
Igdomingding	Bgy
Iglilico	Bgy
Igpayong	Bgy
Jawod	Bgy
Langca	Bgy
Languanan	Bgy
Lawigan	Bgy
Lomboy	Bgy
Lopez Vito	Bgy
Mabini Norte	Bgy
Mabini Sur	Bgy
Manhara	Bgy
Maninila	Bgy
Masagud	Bgy
Matambog	Bgy
Mayunoc	Bgy
Montinola	Bgy
Nagquirisan	Bgy
Nadsadan	Bgy
Nagsipit	Bgy
New Gumawan	Bgy
Panatan	Bgy
Pitogo	Bgy
Purok 1 (Pob.)	Bgy
Purok 2 (Pob.)	Bgy
Purok 3 (Pob.)	Bgy
Purok 4 (Pob.)	Bgy
Purok 5 (Pob.)	Bgy
Qui-anan	Bgy
Roma	Bgy
San Luis	Bgy
San Mateo Norte	Bgy
San Mateo Sur	Bgy
Santiago	Bgy
Sinogbuhan	Bgy
Siwaragan	Bgy
Lomboyan	Bgy
Santa Rita	Bgy
Talagutac	Bgy
Tapikan	Bgy
Taslan	Bgy
Tiglawa	Bgy
Tiolas	Bgy
To-og	Bgy
Torocadan	Bgy
Ulay	Bgy
Bonga	Bgy
Guibongan Bayunan	Bgy
San Miguel	Mun
Bgy. 1 Pob.	Bgy
Bgy. 10	Bgy
Bgy. 11 Pob.	Bgy
Bgy. 12 Pob.	Bgy
Bgy. 13 Pob.	Bgy
Bgy. 14 Pob.	Bgy
Bgy. 15 Pob.	Bgy
Bgy. 16 Pob.	Bgy
Bgy. 2 Pob.	Bgy
Bgy. 3 Pob.	Bgy
Bgy. 4 Pob.	Bgy
Bgy. 5 Pob.	Bgy
Bgy. 6 Pob.	Bgy
Bgy. 7 Pob.	Bgy
Bgy. 8 Pob.	Bgy
Bgy. 9 Pob.	Bgy
Consolacion	Bgy
Igtambo	Bgy
San Antonio	Bgy
San Jose	Bgy
Santa Cruz	Bgy
Santa Teresa	Bgy
Santo Angel	Bgy
Santo Niño	Bgy
San Rafael	Mun
Aripdip	Bgy
Bagacay	Bgy
Calaigang	Bgy
Ilongbukid	Bgy
Poscolon	Bgy
San Andres	Bgy
San Dionisio	Bgy
San Florentino	Bgy
Poblacion	Bgy
Santa Barbara	Mun
Agusipan	Bgy
Agutayan	Bgy
Bagumbayan	Bgy
Balabag	Bgy
Balibagan Este	Bgy
Balibagan Oeste	Bgy
Ban-ag	Bgy
Bantay	Bgy
Barangay Zone I (Pob.)	Bgy
Barangay Zone II (Pob.)	Bgy
Barangay Zone III (Pob.)	Bgy
Barangay Zone IV (Pob.)	Bgy
Barangay Zone V (Pob.)	Bgy
Barasan Este	Bgy
Barasan Oeste	Bgy
Binangkilan	Bgy
Bitaog-Taytay	Bgy
Bolong Este	Bgy
Bolong Oeste	Bgy
Buayahon	Bgy
Buyo	Bgy
Cabugao Norte	Bgy
Cabugao Sur	Bgy
Cadagmayan Norte	Bgy
Cadagmayan Sur	Bgy
Cafe	Bgy
Calaboa Este	Bgy
Calaboa Oeste	Bgy
Camambugan	Bgy
Canipayan	Bgy
Conaynay	Bgy
Daga	Bgy
Dalid	Bgy
Duyanduyan	Bgy
Gen. Martin T. Delgado	Bgy
Guno	Bgy
Inangayan	Bgy
Jibao-an	Bgy
Lacadon	Bgy
Lanag	Bgy
Lupa	Bgy
Magancina	Bgy
Malawog	Bgy
Mambuyo	Bgy
Manhayang	Bgy
Miraga-Guibuangan	Bgy
Nasugban	Bgy
Omambog	Bgy
Pal-Agon	Bgy
Pungsod	Bgy
San Sebastian	Bgy
Sangcate	Bgy
Tagsing	Bgy
Talanghauan	Bgy
Talongadian	Bgy
Tigtig	Bgy
Tungay	Bgy
Tuburan	Bgy
Tugas	Bgy
Barangay Zone VI (Pob.)	Bgy
Sara	Mun
Aguirre	Bgy
Aldeguer	Bgy
Alibayog	Bgy
Anoring	Bgy
Apelo	Bgy
Apologista	Bgy
Aposaga	Bgy
Arante	Bgy
Ardemil	Bgy
Aspera	Bgy
Aswe-Pabriaga	Bgy
Bagaygay	Bgy
Bakabak	Bgy
Batitao	Bgy
Bato	Bgy
Del Castillo	Bgy
Castor	Bgy
Crespo	Bgy
Devera	Bgy
Domingo	Bgy
Ferraris	Bgy
Gildore	Bgy
Improgo	Bgy
Juaneza	Bgy
Labigan	Bgy
Lanciola	Bgy
Latawan	Bgy
Malapaya	Bgy
Muyco	Bgy
Padios	Bgy
Pasig	Bgy
Poblacion Ilawod	Bgy
Poblacion Ilaya	Bgy
Poblacion Market	Bgy
Posadas	Bgy
Preciosa	Bgy
Salcedo	Bgy
San Luis	Bgy
Tady	Bgy
Tentay	Bgy
Villahermosa	Bgy
Zerrudo	Bgy
Tigbauan	Mun
Alupidian	Bgy
Atabayan	Bgy
Bagacay	Bgy
Baguingin	Bgy
Bagumbayan	Bgy
Bangkal	Bgy
Bantud	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barosong	Bgy
Barroc	Bgy
Bayuco	Bgy
Binaliuan Mayor	Bgy
Binaliuan Menor	Bgy
Bitas	Bgy
Buenavista	Bgy
Bugasongan	Bgy
Buyu-an	Bgy
Canabuan	Bgy
Cansilayan	Bgy
Cordova Norte	Bgy
Cordova Sur	Bgy
Danao	Bgy
Dapdap	Bgy
Dorong-an	Bgy
Guisian	Bgy
Isauan	Bgy
Isian	Bgy
Jamog	Bgy
Lanag	Bgy
Linobayan	Bgy
Lubog	Bgy
Nagba	Bgy
Namocon	Bgy
Napnapan Norte	Bgy
Napnapan Sur	Bgy
Olo Barroc	Bgy
Parara Norte	Bgy
Parara Sur	Bgy
San Rafael	Bgy
Sermon	Bgy
Sipitan	Bgy
Supa	Bgy
Tan Pael	Bgy
Taro	Bgy
Tubungan	Mun
Adgao	Bgy
Ago	Bgy
Ambarihon	Bgy
Ayubo	Bgy
Bacan	Bgy
Bagunanay	Bgy
Badiang	Bgy
Balicua	Bgy
Bantayanan	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Batga	Bgy
Bato	Bgy
Bikil	Bgy
Boloc	Bgy
Bondoc	Bgy
Borong	Bgy
Buenavista	Bgy
Cadabdab	Bgy
Daga-ay	Bgy
Desposorio	Bgy
Igdampog Norte	Bgy
Igdampog Sur	Bgy
Igpaho	Bgy
Igtuble	Bgy
Ingay	Bgy
Isauan	Bgy
Jolason	Bgy
Jona	Bgy
La-ag	Bgy
Lanag Norte	Bgy
Lanag Sur	Bgy
Male	Bgy
Mayang	Bgy
Molina	Bgy
Morcillas	Bgy
Nagba	Bgy
Navillan	Bgy
Pinamacalan	Bgy
San Jose	Bgy
Sibucauan	Bgy
Singon	Bgy
Tabat	Bgy
Tagpu-an	Bgy
Talento	Bgy
Teniente Benito	Bgy
Victoria	Bgy
Zarraga	Mun
Balud Lilo-an	Bgy
Balud I	Bgy
Balud II	Bgy
Dawis Centro	Bgy
Dawis Norte	Bgy
Dawis Sur	Bgy
Gines	Bgy
Inagdangan Centro	Bgy
Inagdangan Norte	Bgy
Inagdangan Sur	Bgy
Jalaud Norte	Bgy
Jalaud Sur	Bgy
Libongcogon	Bgy
Malunang	Bgy
Pajo	Bgy
Ilawod Poblacion	Bgy
Ilaya Poblacion	Bgy
Sambag	Bgy
Sigangao	Bgy
Talauguis	Bgy
Talibong	Bgy
Tubigan	Bgy
Tuburan	Bgy
Tuburan Sulbod	Bgy
Negros Occidental	Prov
City of Bacolod (Capital)	City
Alangilan	Bgy
Alijis	Bgy
Banago	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 15 (Pob.)	Bgy
Barangay 16 (Pob.)	Bgy
Barangay 17 (Pob.)	Bgy
Barangay 18 (Pob.)	Bgy
Barangay 19 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 20 (Pob.)	Bgy
Barangay 21 (Pob.)	Bgy
Barangay 22 (Pob.)	Bgy
Barangay 23 (Pob.)	Bgy
Barangay 24 (Pob.)	Bgy
Barangay 25 (Pob.)	Bgy
Barangay 26 (Pob.)	Bgy
Barangay 27 (Pob.)	Bgy
Barangay 28 (Pob.)	Bgy
Barangay 29 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 30 (Pob.)	Bgy
Barangay 31 (Pob.)	Bgy
Barangay 32 (Pob.)	Bgy
Barangay 33 (Pob.)	Bgy
Barangay 34 (Pob.)	Bgy
Barangay 35 (Pob.)	Bgy
Barangay 36 (Pob.)	Bgy
Barangay 37 (Pob.)	Bgy
Barangay 38 (Pob.)	Bgy
Barangay 39 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 40 (Pob.)	Bgy
Barangay 41 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Bata	Bgy
Cabug	Bgy
Estefania	Bgy
Felisa	Bgy
Granada	Bgy
Mandalagan	Bgy
Mansilingan	Bgy
Montevista	Bgy
Pahanocoy	Bgy
Punta Taytay	Bgy
Singcang-Airport	Bgy
Sum-ag	Bgy
Taculing	Bgy
Tangub	Bgy
Villamonte	Bgy
Vista Alegre	Bgy
Handumanan	Bgy
City of Bago	City
Abuanan	Bgy
Alianza	Bgy
Atipuluan	Bgy
Bacong-Montilla	Bgy
Bagroy	Bgy
Balingasag	Bgy
Binubuhan	Bgy
Busay	Bgy
Calumangan	Bgy
Caridad	Bgy
Dulao	Bgy
Ilijan	Bgy
Lag-Asan	Bgy
Ma-ao Barrio	Bgy
Jorge L. Araneta	Bgy
Mailum	Bgy
Malingin	Bgy
Napoles	Bgy
Pacol	Bgy
Poblacion	Bgy
Sagasa	Bgy
Tabunan	Bgy
Taloc	Bgy
Sampinit	Bgy
Binalbagan	Mun
Amontay	Bgy
Bagroy	Bgy
Bi-ao	Bgy
Canmoros (Pob.)	Bgy
Enclaro	Bgy
Marina (Pob.)	Bgy
Paglaum (Pob.)	Bgy
Payao	Bgy
Progreso (Pob.)	Bgy
San Jose	Bgy
San Juan (Pob.)	Bgy
San Pedro (Pob.)	Bgy
San Teodoro (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santo Rosario (Pob.)	Bgy
Santol	Bgy
City of Cadiz	City
Andres Bonifacio	Bgy
Banquerohan	Bgy
Barangay 1 Pob.	Bgy
Barangay 2 Pob.	Bgy
Barangay 3 Pob.	Bgy
Barangay 4 Pob.	Bgy
Barangay 5 Pob.	Bgy
Barangay 6 Pob.	Bgy
Burgos	Bgy
Cabahug	Bgy
Cadiz Viejo	Bgy
Caduha-an	Bgy
Celestino Villacin	Bgy
Daga	Bgy
V. F. Gustilo	Bgy
Jerusalem	Bgy
Luna	Bgy
Mabini	Bgy
Magsaysay	Bgy
Sicaba	Bgy
Tiglawigan	Bgy
Tinampa-an	Bgy
Calatrava	Mun
Agpangi	Bgy
Ani-e	Bgy
Bagacay	Bgy
Bantayanon	Bgy
Buenavista	Bgy
Cabungahan	Bgy
Calampisawan	Bgy
Cambayobo	Bgy
Castellano	Bgy
Cruz	Bgy
Dolis	Bgy
Hilub-Ang	Bgy
Hinab-Ongan	Bgy
Ilaya	Bgy
Laga-an	Bgy
Lalong	Bgy
Lemery	Bgy
Lipat-on	Bgy
Lo-ok (Pob.)	Bgy
Ma-aslob	Bgy
Macasilao	Bgy
Malanog	Bgy
Malatas	Bgy
Marcelo	Bgy
Mina-utok	Bgy
Menchaca	Bgy
Minapasuk	Bgy
Mahilum	Bgy
Paghumayan	Bgy
Pantao	Bgy
Patun-an	Bgy
Pinocutan	Bgy
Refugio	Bgy
San Benito	Bgy
San Isidro	Bgy
Suba (Pob.)	Bgy
Telim	Bgy
Tigbao	Bgy
Tigbon	Bgy
Winaswasan	Bgy
Candoni	Mun
Agboy	Bgy
Banga	Bgy
Cabia-an	Bgy
Caningay	Bgy
Gatuslao	Bgy
Haba	Bgy
Payauan	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Cauayan	Mun
Abaca	Bgy
Baclao	Bgy
Poblacion	Bgy
Basak	Bgy
Bulata	Bgy
Caliling	Bgy
Camalanda-an	Bgy
Camindangan	Bgy
Elihan	Bgy
Guiljungan	Bgy
Inayawan	Bgy
Isio	Bgy
Linaon	Bgy
Lumbia	Bgy
Mambugsay	Bgy
Man-Uling	Bgy
Masaling	Bgy
Molobolo	Bgy
Sura	Bgy
Talacdan	Bgy
Tambad	Bgy
Tiling	Bgy
Tomina	Bgy
Tuyom	Bgy
Yao-yao	Bgy
Enrique B. Magalona	Mun
Alacaygan	Bgy
Alicante	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Batea	Bgy
Consing	Bgy
Cudangdang	Bgy
Damgo	Bgy
Gahit	Bgy
Canlusong	Bgy
Latasan	Bgy
Madalag	Bgy
Manta-angan	Bgy
Nanca	Bgy
Pasil	Bgy
San Isidro	Bgy
San Jose	Bgy
Santo Niño	Bgy
Tabigue	Bgy
Tanza	Bgy
Tuburan	Bgy
Tomongtong	Bgy
City of Escalante	City
Alimango	Bgy
Balintawak (Pob.)	Bgy
Magsaysay	Bgy
Binaguiohan	Bgy
Buenavista	Bgy
Cervantes	Bgy
Dian-ay	Bgy
Hacienda Fe	Bgy
Jonobjonob	Bgy
Japitan	Bgy
Langub	Bgy
Libertad	Bgy
Mabini	Bgy
Malasibog	Bgy
Paitan	Bgy
Pinapugasan	Bgy
Old Poblacion	Bgy
Rizal	Bgy
Tamlang	Bgy
Udtongan	Bgy
Washington	Bgy
City of Himamaylan	City
Aguisan	Bgy
Buenavista	Bgy
Cabadiangan	Bgy
Cabanbanan	Bgy
Carabalan	Bgy
Caradio-an	Bgy
Libacao	Bgy
Mambagaton	Bgy
Nabali-an	Bgy
Mahalang	Bgy
San Antonio	Bgy
Sara-et	Bgy
Su-ay	Bgy
Talaban	Bgy
To-oy	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Hinigaran	Mun
Anahaw	Bgy
Aranda	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Bato	Bgy
Calapi	Bgy
Camalobalo	Bgy
Camba-og	Bgy
Cambugsa	Bgy
Candumarao	Bgy
Gargato	Bgy
Himaya	Bgy
Miranda	Bgy
Nanunga	Bgy
Narauis	Bgy
Palayog	Bgy
Paticui	Bgy
Pilar	Bgy
Quiwi	Bgy
Tagda	Bgy
Tuguis	Bgy
Baga-as	Bgy
Hinoba-an	Mun
Alim	Bgy
Asia	Bgy
Bacuyangan	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Bulwangan	Bgy
Culipapa	Bgy
Damutan	Bgy
Daug	Bgy
Po-ok	Bgy
San Rafael	Bgy
Sangke	Bgy
Talacagay	Bgy
Ilog	Mun
Andulauan	Bgy
Balicotoc	Bgy
Bocana	Bgy
Calubang	Bgy
Canlamay	Bgy
Consuelo	Bgy
Dancalan	Bgy
Delicioso	Bgy
Galicia	Bgy
Manalad	Bgy
Pinggot	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Tabu	Bgy
Vista Alegre	Bgy
Isabela	Mun
Amin	Bgy
Banogbanog	Bgy
Bulad	Bgy
Bungahin	Bgy
Cabcab	Bgy
Camangcamang	Bgy
Camp Clark	Bgy
Cansalongon	Bgy
Guintubhan	Bgy
Libas	Bgy
Limalima	Bgy
Makilignit	Bgy
Mansablay	Bgy
Maytubig	Bgy
Panaquiao	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Riverside	Bgy
Rumirang	Bgy
San Agustin	Bgy
Sebucawan	Bgy
Sikatuna	Bgy
Tinongan	Bgy
City of Kabankalan	City
Bantayan	Bgy
Binicuil	Bgy
Camansi	Bgy
Camingawan	Bgy
Camugao	Bgy
Carol-an	Bgy
Daan Banua	Bgy
Hilamonan	Bgy
Inapoy	Bgy
Linao	Bgy
Locotan	Bgy
Magballo	Bgy
Oringao	Bgy
Orong	Bgy
Pinaguinpinan	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Salong	Bgy
Tabugon	Bgy
Tagoc	Bgy
Talubangi	Bgy
Tampalon	Bgy
Tan-Awan	Bgy
Tapi	Bgy
Tagukon	Bgy
City of La Carlota	City
Ara-al	Bgy
Ayungon	Bgy
Balabag	Bgy
Batuan	Bgy
Roberto S. Benedicto	Bgy
Cubay	Bgy
Haguimit	Bgy
La Granja	Bgy
Nagasi	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
San Miguel	Bgy
Yubo	Bgy
La Castellana	Mun
Biaknabato	Bgy
Cabacungan	Bgy
Cabagnaan	Bgy
Camandag	Bgy
Lalagsan	Bgy
Manghanoy	Bgy
Mansalanao	Bgy
Masulog	Bgy
Nato	Bgy
Puso	Bgy
Robles (Pob.)	Bgy
Sag-Ang	Bgy
Talaptap	Bgy
Manapla	Mun
Chambery	Bgy
Barangay I (Pob.)	Bgy
Barangay I-A (Pob.)	Bgy
Barangay I-B (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay II-A (Pob.)	Bgy
Punta Mesa	Bgy
Punta Salong	Bgy
Purisima	Bgy
San Pablo	Bgy
Santa Teresa	Bgy
Tortosa	Bgy
Moises Padilla	Mun
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Crossing Magallon	Bgy
Guinpana-an	Bgy
Inolingan	Bgy
Macagahay	Bgy
Magallon Cadre	Bgy
Montilla	Bgy
Odiong	Bgy
Quintin Remo	Bgy
Murcia	Mun
Abo-abo	Bgy
Alegria	Bgy
Amayco	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Zone V (Pob.)	Bgy
Blumentritt	Bgy
Buenavista	Bgy
Caliban	Bgy
Canlandog	Bgy
Cansilayan	Bgy
Damsite	Bgy
Iglau-an	Bgy
Lopez Jaena	Bgy
Minoyan	Bgy
Pandanon	Bgy
San Miguel	Bgy
Santa Cruz	Bgy
Santa Rosa	Bgy
Salvacion	Bgy
Talotog	Bgy
Pontevedra	Mun
Antipolo	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Buenavista Gibong	Bgy
Buenavista Rizal	Bgy
Burgos	Bgy
Cambarus	Bgy
Canroma	Bgy
Don Salvador Benedicto	Bgy
General Malvar	Bgy
Gomez	Bgy
M. H. Del Pilar	Bgy
Mabini	Bgy
Miranda	Bgy
Pandan	Bgy
Recreo	Bgy
San Isidro	Bgy
San Juan	Bgy
Zamora	Bgy
Pulupandan	Mun
Barangay Zone 1-A (Pob.)	Bgy
Barangay Zone 4-A (Pob.)	Bgy
Barangay Zone 1 (Pob.)	Bgy
Barangay Zone 2 (Pob.)	Bgy
Barangay Zone 3 (Pob.)	Bgy
Barangay Zone 4 (Pob.)	Bgy
Barangay Zone 5 (Pob.)	Bgy
Barangay Zone 6 (Pob.)	Bgy
Barangay Zone 7 (Pob.)	Bgy
Canjusa	Bgy
Crossing Pulupandan	Bgy
Culo	Bgy
Mabini	Bgy
Pag-ayon	Bgy
Palaka Norte	Bgy
Palaka Sur	Bgy
Patic	Bgy
Tapong	Bgy
Ubay	Bgy
Utod	Bgy
City of Sagay	City
Andres Bonifacio	Bgy
Bato	Bgy
Baviera	Bgy
Bulanon	Bgy
Campo Himoga-an	Bgy
Campo Santiago	Bgy
Colonia Divina	Bgy
Fabrica	Bgy
General Luna	Bgy
Himoga-an Baybay	Bgy
Lopez Jaena	Bgy
Malubon	Bgy
Makiling	Bgy
Molocaboc	Bgy
Old Sagay	Bgy
Paraiso	Bgy
Plaridel	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Puey	Bgy
Rizal	Bgy
Taba-ao	Bgy
Tadlong	Bgy
Vito	Bgy
Rafaela Barrera	Bgy
City of San Carlos	City
Bagonbon	Bgy
Buluangan	Bgy
Codcod	Bgy
Ermita	Bgy
Guadalupe	Bgy
Nataban	Bgy
Palampas	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Prosperidad	Bgy
Punao	Bgy
Quezon	Bgy
Rizal	Bgy
San Juan	Bgy
San Enrique	Mun
Bagonawa	Bgy
Baliwagan	Bgy
Batuan	Bgy
Guintorilan	Bgy
Nayon	Bgy
Poblacion	Bgy
Sibucao	Bgy
Tabao Baybay	Bgy
Tabao Rizal	Bgy
Tibsoc	Bgy
City of Silay	City
Balaring	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI Pob.	Bgy
Eustaquio Lopez	Bgy
Guimbala-on	Bgy
Guinhalaran	Bgy
Kapitan Ramon	Bgy
Lantad	Bgy
Mambulac	Bgy
Rizal	Bgy
Bagtic	Bgy
Patag	Bgy
City of Sipalay	City
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Cabadiangan	Bgy
Camindangan	Bgy
Canturay	Bgy
Cartagena	Bgy
Cayhagan	Bgy
Gil Montilla	Bgy
Mambaroto	Bgy
Manlucahoc	Bgy
Maricalum	Bgy
Nabulao	Bgy
Nauhang	Bgy
San Jose	Bgy
City of Talisay	City
Bubog	Bgy
Cabatangan	Bgy
Zone 4-A (Pob.)	Bgy
Zone 4 (Pob.)	Bgy
Concepcion	Bgy
Dos Hermanas	Bgy
Efigenio Lizares	Bgy
Zone 7 (Pob.)	Bgy
Zone 14-B (Pob.)	Bgy
Zone 12-A (Pob.)	Bgy
Zone 10 (Pob.)	Bgy
Zone 5 (Pob.)	Bgy
Zone 16 (Pob.)	Bgy
Matab-ang	Bgy
Zone 9 (Pob.)	Bgy
Zone 6 (Pob.)	Bgy
Zone 14 (Pob.)	Bgy
San Fernando	Bgy
Zone 15 (Pob.)	Bgy
Zone 14-A (Pob.)	Bgy
Zone 11 (Pob.)	Bgy
Zone 8 (Pob.)	Bgy
Zone 12 (Pob.)	Bgy
Zone 1 (Pob.)	Bgy
Zone 2 (Pob.)	Bgy
Zone 3 (Pob.)	Bgy
Katilingban	Bgy
Toboso	Mun
Bandila	Bgy
Bug-ang	Bgy
General Luna	Bgy
Magticol	Bgy
Poblacion	Bgy
Salamanca	Bgy
San Isidro	Bgy
San Jose	Bgy
Tabun-ac	Bgy
Valladolid	Mun
Alijis	Bgy
Ayungon	Bgy
Bagumbayan	Bgy
Batuan	Bgy
Bayabas	Bgy
Central Tabao	Bgy
Doldol	Bgy
Guintorilan	Bgy
Lacaron	Bgy
Mabini	Bgy
Pacol	Bgy
Palaka	Bgy
Paloma	Bgy
Poblacion	Bgy
Sagua Banua	Bgy
Tabao Proper	Bgy
City of Victorias	City
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Barangay VII (Pob.)	Bgy
Barangay VIII (Pob.)	Bgy
Barangay IX	Bgy
Barangay X	Bgy
Barangay XI	Bgy
Barangay XII	Bgy
Barangay XIII	Bgy
Barangay XIV	Bgy
Barangay XV	Bgy
Barangay XV-A	Bgy
Barangay XVI	Bgy
Barangay XVI-A	Bgy
Barangay XVII	Bgy
Barangay XVIII	Bgy
Barangay XVIII-A	Bgy
Barangay XIX	Bgy
Barangay XIX-A	Bgy
Barangay XX	Bgy
Barangay XXI	Bgy
Barangay VI-A	Bgy
Salvador Benedicto	Mun
Bago	Bgy
Bagong Silang	Bgy
Bunga	Bgy
Igmaya-an	Bgy
Kumaliskis	Bgy
Pandanon	Bgy
Pinowayan	Bgy
Guimaras	Prov
Buenavista	Mun
Agsanayan	Bgy
Avila	Bgy
Banban	Bgy
Bacjao 	Bgy
Cansilayan	Bgy
Dagsa-an	Bgy
Daragan	Bgy
East Valencia	Bgy
Getulio	Bgy
Mabini	Bgy
Magsaysay	Bgy
Mclain	Bgy
Montpiller	Bgy
Navalas	Bgy
Nazaret	Bgy
New Poblacion 	Bgy
Old Poblacion	Bgy
Piña	Bgy
Rizal	Bgy
Salvacion	Bgy
San Fernando	Bgy
San Isidro	Bgy
San Miguel	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Roque	Bgy
Santo Rosario	Bgy
Sawang	Bgy
Supang	Bgy
Tacay	Bgy
Taminla	Bgy
Tanag	Bgy
Tastasan	Bgy
Tinadtaran	Bgy
Umilig	Bgy
Zaldivar	Bgy
Jordan (Capital)	Mun
Alaguisoc	Bgy
Balcon Maravilla	Bgy
Balcon Melliza	Bgy
Bugnay	Bgy
Buluangan	Bgy
Espinosa	Bgy
Hoskyn	Bgy
Lawi	Bgy
Morobuan	Bgy
Poblacion	Bgy
Rizal	Bgy
San Miguel	Bgy
Sinapsapan	Bgy
Santa Teresa	Bgy
Nueva Valencia	Mun
Cabalagnan	Bgy
Calaya	Bgy
Canhawan	Bgy
Concordia Sur	Bgy
Dolores	Bgy
Guiwanon	Bgy
Igang	Bgy
Igdarapdap	Bgy
La Paz	Bgy
Lanipe	Bgy
Lucmayan	Bgy
Magamay	Bgy
Napandong	Bgy
Oracon Sur	Bgy
Pandaraonan	Bgy
Panobolon	Bgy
Poblacion	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Roque	Bgy
Santo Domingo	Bgy
Tando	Bgy
San Lorenzo	Mun
Aguilar	Bgy
Cabano	Bgy
Cabungahan	Bgy
Constancia	Bgy
Gaban	Bgy
Igcawayan	Bgy
M. Chavez	Bgy
San Enrique	Bgy
Sapal	Bgy
Sebario	Bgy
Suclaran	Bgy
Tamborong	Bgy
Sibunag	Mun
Alegria	Bgy
Ayangan	Bgy
Bubog	Bgy
Concordia Norte	Bgy
Dasal	Bgy
Inampologan	Bgy
Maabay	Bgy
Millan	Bgy
Oracon Norte	Bgy
Ravina	Bgy
Sabang	Bgy
San Isidro	Bgy
Sebaste	Bgy
Tanglad	Bgy
Region VII (Central Visayas)	Reg
Bohol	Prov
Alburquerque	Mun
Bahi	Bgy
Basacdacu	Bgy
Cantiguib	Bgy
Dangay	Bgy
East Poblacion	Bgy
Ponong	Bgy
San Agustin	Bgy
Santa Filomena	Bgy
Tagbuane	Bgy
Toril	Bgy
West Poblacion	Bgy
Alicia	Mun
Cabatang	Bgy
Cagongcagong	Bgy
Cambaol	Bgy
Cayacay	Bgy
Del Monte	Bgy
Katipunan	Bgy
La Hacienda	Bgy
Mahayag	Bgy
Napo	Bgy
Pagahat	Bgy
Poblacion	Bgy
Progreso	Bgy
Putlongcam	Bgy
Sudlon	Bgy
Untaga	Bgy
Anda	Mun
Almaria	Bgy
Bacong	Bgy
Badiang	Bgy
Buenasuerte	Bgy
Candabong	Bgy
Casica	Bgy
Katipunan	Bgy
Linawan	Bgy
Lundag	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Suba	Bgy
Talisay	Bgy
Tanod	Bgy
Tawid	Bgy
Virgen	Bgy
Antequera	Mun
Angilan	Bgy
Bantolinao	Bgy
Bicahan	Bgy
Bitaugan	Bgy
Bungahan	Bgy
Canlaas	Bgy
Cansibuan	Bgy
Can-omay	Bgy
Celing	Bgy
Danao	Bgy
Danicop	Bgy
Mag-aso	Bgy
Poblacion	Bgy
Quinapon-an	Bgy
Santo Rosario	Bgy
Tabuan	Bgy
Tagubaas	Bgy
Tupas	Bgy
Ubojan	Bgy
Viga	Bgy
Villa Aurora	Bgy
Baclayon	Mun
Cambanac	Bgy
Dasitam	Bgy
Buenaventura	Bgy
Guiwanon	Bgy
Landican	Bgy
Laya	Bgy
Libertad	Bgy
Montana	Bgy
Pamilacan	Bgy
Payahan	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Taguihon	Bgy
Tanday	Bgy
Balilihan	Mun
Baucan Norte	Bgy
Baucan Sur	Bgy
Boctol	Bgy
Boyog Norte	Bgy
Boyog Proper	Bgy
Boyog Sur	Bgy
Cabad	Bgy
Candasig	Bgy
Cantalid	Bgy
Cantomimbo	Bgy
Cogon	Bgy
Datag Norte	Bgy
Datag Sur	Bgy
Del Carmen Este (Pob.)	Bgy
Del Carmen Norte (Pob.)	Bgy
Del Carmen Weste (Pob.)	Bgy
Del Carmen Sur (Pob.)	Bgy
Del Rosario	Bgy
Dorol	Bgy
Haguilanan Grande	Bgy
Hanopol Este	Bgy
Hanopol Norte	Bgy
Hanopol Weste	Bgy
Magsija	Bgy
Maslog	Bgy
Sagasa	Bgy
Sal-ing	Bgy
San Isidro	Bgy
San Roque	Bgy
Santo Niño	Bgy
Tagustusan	Bgy
Batuan	Mun
Aloja	Bgy
Cabacnitan	Bgy
Cambacay	Bgy
Cantigdas	Bgy
Garcia	Bgy
Janlud	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Poblacion Vieja	Bgy
Quezon	Bgy
Quirino	Bgy
Rizal	Bgy
Rosariohan	Bgy
Behind The Clouds	Bgy
Santa Cruz	Bgy
Bilar	Mun
Bonifacio	Bgy
Bugang Norte	Bgy
Bugang Sur	Bgy
Cabacnitan	Bgy
Cambigsi	Bgy
Campagao	Bgy
Cansumbol	Bgy
Dagohoy	Bgy
Owac	Bgy
Poblacion	Bgy
Quezon	Bgy
Riverside	Bgy
Rizal	Bgy
Roxas	Bgy
Subayon	Bgy
Villa Aurora	Bgy
Villa Suerte	Bgy
Yanaya	Bgy
Zamora	Bgy
Buenavista	Mun
Anonang	Bgy
Asinan	Bgy
Bago	Bgy
Baluarte	Bgy
Bantuan	Bgy
Bato	Bgy
Bonotbonot	Bgy
Bugaong	Bgy
Cambuhat	Bgy
Cambus-oc	Bgy
Cangawa	Bgy
Cantomugcad	Bgy
Cantores	Bgy
Cantuba	Bgy
Catigbian	Bgy
Cawag	Bgy
Cruz	Bgy
Dait	Bgy
Eastern Cabul-an	Bgy
Hunan	Bgy
Lapacan Norte	Bgy
Lapacan Sur	Bgy
Lubang	Bgy
Lusong	Bgy
Magkaya	Bgy
Merryland	Bgy
Nueva Granada	Bgy
Nueva Montana	Bgy
Overland	Bgy
Panghagban	Bgy
Poblacion	Bgy
Puting Bato	Bgy
Rufo Hill	Bgy
Sweetland	Bgy
Western Cabul-an	Bgy
Calape	Mun
Abucayan Norte	Bgy
Abucayan Sur	Bgy
Banlasan	Bgy
Bentig	Bgy
Binogawan	Bgy
Bonbon	Bgy
Cabayugan	Bgy
Cabudburan	Bgy
Calunasan	Bgy
Camias	Bgy
Canguha	Bgy
Catmonan	Bgy
Desamparados (Pob.)	Bgy
Kahayag	Bgy
Kinabag-an	Bgy
Labuon	Bgy
Lawis	Bgy
Liboron	Bgy
Lo-oc	Bgy
Lomboy	Bgy
Lucob	Bgy
Madangog	Bgy
Magtongtong	Bgy
Mandaug	Bgy
Mantatao	Bgy
Sampoangon	Bgy
San Isidro	Bgy
Santa Cruz (Pob.)	Bgy
Sojoton	Bgy
Talisay	Bgy
Tinibgan	Bgy
Tultugan	Bgy
Ulbujan	Bgy
Candijay	Mun
Abihilan	Bgy
Anoling	Bgy
Boyo-an	Bgy
Cadapdapan	Bgy
Cambane	Bgy
Can-olin	Bgy
Canawa	Bgy
Cogtong	Bgy
La Union	Bgy
Luan	Bgy
Lungsoda-an	Bgy
Mahangin	Bgy
Pagahat	Bgy
Panadtaran	Bgy
Panas	Bgy
Poblacion	Bgy
San Isidro	Bgy
Tambongan	Bgy
Tawid	Bgy
Tugas	Bgy
Tubod	Bgy
Carmen	Mun
Alegria	Bgy
Bicao	Bgy
Buenavista	Bgy
Buenos Aires	Bgy
Calatrava	Bgy
El Progreso	Bgy
El Salvador	Bgy
Guadalupe	Bgy
Katipunan	Bgy
La Libertad	Bgy
La Paz	Bgy
La Salvacion	Bgy
La Victoria	Bgy
Matin-ao	Bgy
Montehermoso	Bgy
Montesuerte	Bgy
Montesunting	Bgy
Montevideo	Bgy
Nueva Fuerza	Bgy
Nueva Vida Este	Bgy
Nueva Vida Sur	Bgy
Nueva Vida Norte	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Tambo-an	Bgy
Vallehermoso	Bgy
Villaflor	Bgy
Villafuerte	Bgy
Villarcayo	Bgy
Catigbian	Mun
Alegria	Bgy
Ambuan	Bgy
Baang	Bgy
Bagtic	Bgy
Bongbong	Bgy
Cambailan	Bgy
Candumayao	Bgy
Kang-iras	Bgy
Causwagan Norte	Bgy
Hagbuaya	Bgy
Haguilanan	Bgy
Libertad Sur	Bgy
Liboron	Bgy
Mahayag Norte	Bgy
Mahayag Sur	Bgy
Maitum	Bgy
Mantasida	Bgy
Poblacion	Bgy
Rizal	Bgy
Sinakayanan	Bgy
Triple Union	Bgy
Poblacion Weste	Bgy
Clarin	Mun
Bacani	Bgy
Bogtongbod	Bgy
Bonbon	Bgy
Bontud	Bgy
Buacao	Bgy
Buangan	Bgy
Cabog	Bgy
Caboy	Bgy
Caluwasan	Bgy
Candajec	Bgy
Cantoyoc	Bgy
Comaang	Bgy
Danahao	Bgy
Katipunan	Bgy
Lajog	Bgy
Mataub	Bgy
Nahawan	Bgy
Poblacion Centro	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Tangaran	Bgy
Tontunan	Bgy
Tubod	Bgy
Villaflor	Bgy
Corella	Mun
Anislag	Bgy
Canangca-an	Bgy
Canapnapan	Bgy
Cancatac	Bgy
Pandol	Bgy
Poblacion	Bgy
Sambog	Bgy
Tanday	Bgy
Cortes	Mun
De la Paz	Bgy
Fatima	Bgy
Loreto	Bgy
Lourdes	Bgy
Malayo Norte	Bgy
Malayo Sur	Bgy
Monserrat	Bgy
New Lourdes	Bgy
Patrocinio	Bgy
Poblacion	Bgy
Rosario	Bgy
Salvador	Bgy
San Roque	Bgy
Upper de la Paz	Bgy
Dagohoy	Mun
Babag	Bgy
Can-oling	Bgy
Candelaria	Bgy
Estaca	Bgy
Cagawasan	Bgy
Cagawitan	Bgy
Caluasan	Bgy
La Esperanza	Bgy
Mahayag	Bgy
Malitbog	Bgy
Poblacion	Bgy
San Miguel	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Villa Aurora	Bgy
Danao	Mun
Cabatuan	Bgy
Cantubod	Bgy
Carbon	Bgy
Concepcion	Bgy
Dagohoy	Bgy
Hibale	Bgy
Magtangtang	Bgy
Nahud	Bgy
Poblacion	Bgy
Remedios	Bgy
San Carlos	Bgy
San Miguel	Bgy
Santa Fe	Bgy
Santo Niño	Bgy
Tabok	Bgy
Taming	Bgy
Villa Anunciado	Bgy
Dauis	Mun
Biking	Bgy
Bingag	Bgy
San Isidro	Bgy
Catarman	Bgy
Dao	Bgy
Mayacabac	Bgy
Poblacion	Bgy
Songculan	Bgy
Tabalong	Bgy
Tinago	Bgy
Totolan	Bgy
Mariveles	Bgy
Dimiao	Mun
Abihid	Bgy
Alemania	Bgy
Baguhan	Bgy
Bakilid	Bgy
Balbalan	Bgy
Banban	Bgy
Bauhugan	Bgy
Bilisan	Bgy
Cabagakian	Bgy
Cabanbanan	Bgy
Cadap-agan	Bgy
Cambacol	Bgy
Cambayaon	Bgy
Canhayupon	Bgy
Canlambong	Bgy
Casingan	Bgy
Catugasan	Bgy
Datag	Bgy
Guindaguitan	Bgy
Guingoyuran	Bgy
Ile	Bgy
Lapsaon	Bgy
Limokon Ilaod	Bgy
Limokon Ilaya	Bgy
Luyo	Bgy
Malijao	Bgy
Oac	Bgy
Pagsa	Bgy
Pangihawan	Bgy
Sawang	Bgy
Puangyuta	Bgy
Tangohay	Bgy
Taongon Cabatuan	Bgy
Tawid Bitaog	Bgy
Taongon Can-andam	Bgy
Duero	Mun
Alejawan	Bgy
Angilan	Bgy
Anibongan	Bgy
Bangwalog	Bgy
Cansuhay	Bgy
Danao	Bgy
Duay	Bgy
Guinsularan	Bgy
Itum	Bgy
Langkis	Bgy
Lobogon	Bgy
Madua Norte	Bgy
Madua Sur	Bgy
Mambool	Bgy
Mawi	Bgy
Payao	Bgy
San Antonio (Pob.)	Bgy
San Isidro	Bgy
San Pedro	Bgy
Imelda	Bgy
Taytay	Bgy
Garcia Hernandez	Mun
Abijilan	Bgy
Antipolo	Bgy
Basiao	Bgy
Cagwang	Bgy
Calma	Bgy
Cambuyo	Bgy
Canayaon East	Bgy
Canayaon West	Bgy
Candanas	Bgy
Candulao	Bgy
Catmon	Bgy
Cayam	Bgy
Cupa	Bgy
Datag	Bgy
Estaca	Bgy
Libertad	Bgy
Lungsodaan East	Bgy
Lungsodaan West	Bgy
Malinao	Bgy
Manaba	Bgy
Pasong	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Sacaon	Bgy
Sampong	Bgy
Tabuan	Bgy
Togbongon	Bgy
Ulbujan East	Bgy
Ulbujan West	Bgy
Victoria	Bgy
Guindulman	Mun
Basdio	Bgy
Bato	Bgy
Bayong	Bgy
Biabas	Bgy
Bulawan	Bgy
Cabantian	Bgy
Canhaway	Bgy
Cansiwang	Bgy
Casbu	Bgy
Catungawan Sur	Bgy
Catungawan Norte	Bgy
Guinacot	Bgy
Guio-ang	Bgy
Lombog	Bgy
Mayuga	Bgy
Sawang (Pob.)	Bgy
Tabajan (Pob.)	Bgy
Tabunok	Bgy
Trinidad	Bgy
Inabanga	Mun
Anonang	Bgy
Bahan	Bgy
Badiang	Bgy
Baguhan	Bgy
Banahao	Bgy
Baogo	Bgy
Bugang	Bgy
Cagawasan	Bgy
Cagayan	Bgy
Cambitoon	Bgy
Canlinte	Bgy
Cawayan	Bgy
Cogon	Bgy
Cuaming	Bgy
Dagnawan	Bgy
Dagohoy	Bgy
Dait Sur	Bgy
Datag	Bgy
Fatima	Bgy
Hambongan	Bgy
Ilaud (Pob.)	Bgy
Ilaya	Bgy
Ilihan	Bgy
Lapacan Norte	Bgy
Lapacan Sur	Bgy
Lawis	Bgy
Liloan Norte	Bgy
Liloan Sur	Bgy
Lomboy	Bgy
Lonoy Cainsican	Bgy
Lonoy Roma	Bgy
Lutao	Bgy
Luyo	Bgy
Mabuhay	Bgy
Maria Rosario	Bgy
Nabuad	Bgy
Napo	Bgy
Ondol	Bgy
Poblacion	Bgy
Riverside	Bgy
Saa	Bgy
San Isidro	Bgy
San Jose	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Sua	Bgy
Tambook	Bgy
Tungod	Bgy
U-og	Bgy
Ubujan	Bgy
Jagna	Mun
Alejawan	Bgy
Balili	Bgy
Boctol	Bgy
Buyog	Bgy
Bunga Ilaya	Bgy
Bunga Mar	Bgy
Cabunga-an	Bgy
Calabacita	Bgy
Cambugason	Bgy
Can-ipol	Bgy
Canjulao	Bgy
Cantagay	Bgy
Cantuyoc	Bgy
Can-uba	Bgy
Can-upao	Bgy
Faraon	Bgy
Ipil	Bgy
Kinagbaan	Bgy
Laca	Bgy
Larapan	Bgy
Lonoy	Bgy
Looc	Bgy
Malbog	Bgy
Mayana	Bgy
Naatang	Bgy
Nausok	Bgy
Odiong	Bgy
Pagina	Bgy
Pangdan	Bgy
Poblacion	Bgy
Tejero	Bgy
Tubod Mar	Bgy
Tubod Monte	Bgy
Getafe	Mun
Alumar	Bgy
Banacon	Bgy
Buyog	Bgy
Cabasakan	Bgy
Campao Occidental	Bgy
Campao Oriental	Bgy
Cangmundo	Bgy
Carlos P. Garcia	Bgy
Corte Baud	Bgy
Handumon	Bgy
Jagoliao	Bgy
Jandayan Norte	Bgy
Jandayan Sur	Bgy
Mahanay	Bgy
Nasingin	Bgy
Pandanon	Bgy
Poblacion	Bgy
Saguise	Bgy
Salog	Bgy
San Jose	Bgy
Santo Niño	Bgy
Taytay	Bgy
Tugas	Bgy
Tulang	Bgy
Lila	Mun
Banban	Bgy
Bonkokan Ilaya	Bgy
Bonkokan Ubos	Bgy
Calvario	Bgy
Candulang	Bgy
Catugasan	Bgy
Cayupo	Bgy
Cogon	Bgy
Jambawan	Bgy
La Fortuna	Bgy
Lomanoy	Bgy
Macalingan	Bgy
Malinao East	Bgy
Malinao West	Bgy
Nagsulay	Bgy
Poblacion	Bgy
Taug	Bgy
Tiguis	Bgy
Loay	Mun
Agape	Bgy
Alegria Norte	Bgy
Alegria Sur	Bgy
Bonbon	Bgy
Botoc Occidental	Bgy
Botoc Oriental	Bgy
Calvario	Bgy
Concepcion	Bgy
Hinawanan	Bgy
Las Salinas Norte	Bgy
Las Salinas Sur	Bgy
Palo	Bgy
Poblacion Ibabao	Bgy
Poblacion Ubos	Bgy
Sagnap	Bgy
Tambangan	Bgy
Tangcasan Norte	Bgy
Tangcasan Sur	Bgy
Tayong Occidental	Bgy
Tayong Oriental	Bgy
Tocdog Dacu	Bgy
Tocdog Ilaya	Bgy
Villalimpia	Bgy
Yanangan	Bgy
Loboc	Mun
Agape	Bgy
Alegria	Bgy
Bagumbayan	Bgy
Bahian	Bgy
Bonbon Lower	Bgy
Bonbon Upper	Bgy
Buenavista	Bgy
Bugho	Bgy
Cabadiangan	Bgy
Calunasan Norte	Bgy
Calunasan Sur	Bgy
Camayaan	Bgy
Cambance	Bgy
Candabong	Bgy
Candasag	Bgy
Canlasid	Bgy
Gon-ob	Bgy
Gotozon	Bgy
Jimilian	Bgy
Oy	Bgy
Poblacion Sawang	Bgy
Poblacion Ondol	Bgy
Quinoguitan	Bgy
Taytay	Bgy
Tigbao	Bgy
Ugpong	Bgy
Valladolid	Bgy
Villaflor	Bgy
Loon	Mun
Agsoso	Bgy
Badbad Occidental	Bgy
Badbad Oriental	Bgy
Bagacay Katipunan	Bgy
Bagacay Kawayan	Bgy
Bagacay Saong	Bgy
Bahi	Bgy
Basac	Bgy
Basdacu	Bgy
Basdio	Bgy
Biasong	Bgy
Bongco	Bgy
Bugho	Bgy
Cabacongan	Bgy
Cabadug	Bgy
Cabug	Bgy
Calayugan Norte	Bgy
Calayugan Sur	Bgy
Canmaag	Bgy
Cambaquiz	Bgy
Campatud	Bgy
Candaigan	Bgy
Canhangdon Occidental	Bgy
Canhangdon Oriental	Bgy
Canigaan	Bgy
Canmanoc	Bgy
Cansuagwit	Bgy
Cansubayon	Bgy
Catagbacan Handig	Bgy
Catagbacan Norte	Bgy
Catagbacan Sur	Bgy
Cantam-is Bago	Bgy
Cantaongon	Bgy
Cantumocad	Bgy
Cantam-is Baslay	Bgy
Cogon Norte (Pob.)	Bgy
Cogon Sur	Bgy
Cuasi	Bgy
Genomoan	Bgy
Lintuan	Bgy
Looc	Bgy
Mocpoc Norte	Bgy
Mocpoc Sur	Bgy
Nagtuang	Bgy
Napo (Pob.)	Bgy
Nueva Vida	Bgy
Panangquilon	Bgy
Pantudlan	Bgy
Pig-ot	Bgy
Moto Norte (Pob.)	Bgy
Moto Sur (Pob.)	Bgy
Pondol	Bgy
Quinobcoban	Bgy
Sondol	Bgy
Song-on	Bgy
Talisay	Bgy
Tan-awan	Bgy
Tangnan	Bgy
Taytay	Bgy
Ticugan	Bgy
Tiwi	Bgy
Tontonan	Bgy
Tubodacu	Bgy
Tubodio	Bgy
Tubuan	Bgy
Ubayon	Bgy
Ubojan	Bgy
Mabini	Mun
Abaca	Bgy
Abad Santos	Bgy
Aguipo	Bgy
Concepcion	Bgy
Baybayon	Bgy
Bulawan	Bgy
Cabidian	Bgy
Cawayanan	Bgy
Del Mar	Bgy
Lungsoda-an	Bgy
Marcelo	Bgy
Minol	Bgy
Paraiso	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael	Bgy
San Roque	Bgy
Tambo	Bgy
Tangkigan	Bgy
Valaga	Bgy
Maribojoc	Mun
San Roque	Bgy
Agahay	Bgy
Aliguay	Bgy
Anislag	Bgy
Bayacabac	Bgy
Bood	Bgy
Busao	Bgy
Cabawan	Bgy
Candavid	Bgy
Dipatlong	Bgy
Guiwanon	Bgy
Jandig	Bgy
Lagtangon	Bgy
Lincod	Bgy
Pagnitoan	Bgy
Poblacion	Bgy
Punsod	Bgy
Punta Cruz	Bgy
San Isidro	Bgy
San Vicente	Bgy
Tinibgan	Bgy
Toril	Bgy
Panglao	Mun
Bil-isan	Bgy
Bolod	Bgy
Danao	Bgy
Doljo	Bgy
Libaong	Bgy
Looc	Bgy
Lourdes	Bgy
Poblacion	Bgy
Tangnan	Bgy
Tawala	Bgy
Pilar	Mun
Aurora	Bgy
Bagacay	Bgy
Bagumbayan	Bgy
Bayong	Bgy
Buenasuerte	Bgy
Cagawasan	Bgy
Cansungay	Bgy
Catagda-an	Bgy
Del Pilar	Bgy
Estaca	Bgy
Ilaud	Bgy
Inaghuban	Bgy
La Suerte	Bgy
Lumbay	Bgy
Lundag	Bgy
Pamacsalan	Bgy
Poblacion	Bgy
Rizal	Bgy
San Carlos	Bgy
San Isidro	Bgy
San Vicente	Bgy
President Carlos P. Garcia	Mun
Aguining	Bgy
Basiao	Bgy
Baud	Bgy
Bayog	Bgy
Bogo	Bgy
Bonbonon	Bgy
Canmangao	Bgy
Campamanog	Bgy
Gaus	Bgy
Kabangkalan	Bgy
Lapinig	Bgy
Lipata	Bgy
Poblacion	Bgy
Popoo	Bgy
Saguise	Bgy
San Jose	Bgy
Santo Rosario	Bgy
Tilmobo	Bgy
Tugas	Bgy
Tugnao	Bgy
Villa Milagrosa	Bgy
Butan	Bgy
San Vicente	Bgy
Sagbayan	Mun
Calangahan	Bgy
Canmano	Bgy
Canmaya Centro	Bgy
Canmaya Diot	Bgy
Dagnawan	Bgy
Kabasacan	Bgy
Kagawasan	Bgy
Katipunan	Bgy
Langtad	Bgy
Libertad Norte	Bgy
Libertad Sur	Bgy
Mantalongon	Bgy
Poblacion	Bgy
Sagbayan Sur	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Ramon	Bgy
San Roque	Bgy
San Vicente Norte	Bgy
San Vicente Sur	Bgy
Santa Catalina	Bgy
Santa Cruz	Bgy
Ubojan	Bgy
San Isidro	Mun
Abehilan	Bgy
Baunos	Bgy
Cabanugan	Bgy
Caimbang	Bgy
Cambansag	Bgy
Candungao	Bgy
Cansague Norte	Bgy
Cansague Sur	Bgy
Causwagan Sur	Bgy
Masonoy	Bgy
Poblacion	Bgy
Baryong Daan	Bgy
San Miguel	Mun
Bayongan	Bgy
Bugang	Bgy
Cabangahan	Bgy
Kagawasan	Bgy
Camanaga	Bgy
Cambangay Norte	Bgy
Capayas	Bgy
Corazon	Bgy
Garcia	Bgy
Hagbuyo	Bgy
Caluasan	Bgy
Mahayag	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Jose	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Tomoc	Bgy
Sevilla	Mun
Bayawahan	Bgy
Cabancalan	Bgy
Calinga-an	Bgy
Calinginan Norte	Bgy
Calinginan Sur	Bgy
Cambagui	Bgy
Ewon	Bgy
Guinob-an	Bgy
Lagtangan	Bgy
Licolico	Bgy
Lobgob	Bgy
Magsaysay	Bgy
Poblacion	Bgy
Sierra Bullones	Mun
Abachanan	Bgy
Anibongan	Bgy
Bugsoc	Bgy
Canlangit	Bgy
Canta-ub	Bgy
Casilay	Bgy
Danicop	Bgy
Dusita	Bgy
Cahayag	Bgy
La Union	Bgy
Lataban	Bgy
Magsaysay	Bgy
Matin-ao	Bgy
Man-od	Bgy
Poblacion	Bgy
Salvador	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Villa Garcia	Bgy
Sikatuna	Mun
Abucay Norte	Bgy
Abucay Sur	Bgy
Badiang	Bgy
Bahaybahay	Bgy
Cambuac Norte	Bgy
Cambuac Sur	Bgy
Canagong	Bgy
Libjo	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
City of Tagbilaran (Capital)	City
Bool	Bgy
Booy	Bgy
Cabawan	Bgy
Cogon	Bgy
Dao	Bgy
Dampas	Bgy
Manga	Bgy
Mansasa	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
San Isidro	Bgy
Taloto	Bgy
Tiptip	Bgy
Ubujan	Bgy
Talibon	Mun
Bagacay	Bgy
Balintawak	Bgy
Burgos	Bgy
Busalian	Bgy
Calituban	Bgy
Cataban	Bgy
Guindacpan	Bgy
Magsaysay	Bgy
Mahanay	Bgy
Nocnocan	Bgy
Poblacion	Bgy
Rizal	Bgy
Sag	Bgy
San Agustin	Bgy
San Carlos	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Pedro	Bgy
San Roque	Bgy
Santo Niño	Bgy
Sikatuna	Bgy
Suba	Bgy
Tanghaligue	Bgy
Zamora	Bgy
Trinidad	Mun
Banlasan	Bgy
Bongbong	Bgy
Catoogan	Bgy
Guinobatan	Bgy
Hinlayagan Ilaud	Bgy
Hinlayagan Ilaya	Bgy
Kauswagan	Bgy
Kinan-oan	Bgy
La Union	Bgy
La Victoria	Bgy
Mabuhay Cabigohan	Bgy
Mahagbu	Bgy
Manuel M. Roxas	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Vicente	Bgy
Santo Tomas	Bgy
Soom	Bgy
Tagum Norte	Bgy
Tagum Sur	Bgy
Tubigon	Mun
Bagongbanwa	Bgy
Bunacan	Bgy
Banlasan	Bgy
Batasan	Bgy
Bilangbilangan	Bgy
Bosongon	Bgy
Buenos Aires	Bgy
Cabulihan	Bgy
Cahayag	Bgy
Cawayanan	Bgy
Centro (Pob.)	Bgy
Genonocan	Bgy
Guiwanon	Bgy
Ilihan Norte	Bgy
Ilihan Sur	Bgy
Libertad	Bgy
Macaas	Bgy
Mocaboc Island	Bgy
Matabao	Bgy
Panadtaran	Bgy
Panaytayon	Bgy
Pandan	Bgy
Pangapasan	Bgy
Pinayagan Norte	Bgy
Pinayagan Sur	Bgy
Pooc Occidental (Pob.)	Bgy
Pooc Oriental (Pob.)	Bgy
Potohan	Bgy
Talenceras	Bgy
Tan-awan	Bgy
Tinangnan	Bgy
Ubay Island	Bgy
Ubojan	Bgy
Villanueva	Bgy
Ubay	Mun
Achila	Bgy
Bay-ang	Bgy
Biabas	Bgy
Benliw	Bgy
Bongbong	Bgy
Bood	Bgy
Buenavista	Bgy
Cagting	Bgy
Camali-an	Bgy
Camambugan	Bgy
Casate	Bgy
Katarungan	Bgy
Cuya	Bgy
Fatima	Bgy
Gabi	Bgy
Governor Boyles	Bgy
Guintabo-an	Bgy
Hambabauran	Bgy
Humayhumay	Bgy
Ilihan	Bgy
Imelda	Bgy
Juagdan	Bgy
Calanggaman	Bgy
Los Angeles	Bgy
Lomangog	Bgy
Pag-asa	Bgy
Pangpang	Bgy
Poblacion	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Pascual	Bgy
San Vicente	Bgy
Sentinila	Bgy
Sinandigan	Bgy
Tapal	Bgy
Tapon	Bgy
Tintinan	Bgy
Tipolo	Bgy
Tubog	Bgy
Tuboran	Bgy
Union	Bgy
Villa Teresita	Bgy
Bulilis	Bgy
California	Bgy
Valencia	Mun
Adlawan	Bgy
Anas	Bgy
Anonang	Bgy
Anoyon	Bgy
Balingasao	Bgy
Botong	Bgy
Buyog	Bgy
Canduao Occidental	Bgy
Canduao Oriental	Bgy
Canlusong	Bgy
Canmanico	Bgy
Cansibao	Bgy
Catug-a	Bgy
Cutcutan	Bgy
Danao	Bgy
Genoveva	Bgy
Ginopolan	Bgy
La Victoria	Bgy
Lantang	Bgy
Limocon	Bgy
Loctob	Bgy
Magsaysay	Bgy
Marawis	Bgy
Maubo	Bgy
Nailo	Bgy
Omjon	Bgy
Pangi-an	Bgy
Poblacion Occidental	Bgy
Poblacion Oriental	Bgy
Simang	Bgy
Taug	Bgy
Tausion	Bgy
Taytay	Bgy
Ticum	Bgy
Banderahan	Bgy
Bien Unido	Mun
Bilangbilangan Dako	Bgy
Bilangbilangan Diot	Bgy
Hingotanan East	Bgy
Hingotanan West	Bgy
Liberty	Bgy
Malingin	Bgy
Mandawa	Bgy
Maomawan	Bgy
Nueva Esperanza	Bgy
Nueva Estrella	Bgy
Pinamgo	Bgy
Poblacion	Bgy
Puerto San Pedro	Bgy
Sagasa	Bgy
Tuboran	Bgy
Cebu	Prov
Alcantara	Mun
Cabadiangan	Bgy
Cabil-isan	Bgy
Candabong	Bgy
Lawaan	Bgy
Manga	Bgy
Palanas	Bgy
Poblacion	Bgy
Polo	Bgy
Salagmaya	Bgy
Alcoy	Mun
Atabay	Bgy
Daan-Lungsod	Bgy
Guiwang	Bgy
Nug-as	Bgy
Pasol	Bgy
Poblacion	Bgy
Pugalo	Bgy
San Agustin	Bgy
Alegria	Mun
Compostela	Bgy
Guadalupe	Bgy
Legaspi	Bgy
Lepanto	Bgy
Madridejos	Bgy
Montpeller	Bgy
Poblacion	Bgy
Santa Filomena	Bgy
Valencia	Bgy
Aloguinsan	Mun
Angilan	Bgy
Bojo	Bgy
Bonbon	Bgy
Esperanza	Bgy
Kandingan	Bgy
Kantabogon	Bgy
Kawasan	Bgy
Olango	Bgy
Poblacion	Bgy
Punay	Bgy
Rosario	Bgy
Saksak	Bgy
Tampa-an	Bgy
Toyokon	Bgy
Zaragosa	Bgy
Argao	Mun
Alambijud	Bgy
Anajao	Bgy
Apo	Bgy
Balaas	Bgy
Balisong	Bgy
Binlod	Bgy
Bogo	Bgy
Butong	Bgy
Bug-ot	Bgy
Bulasa	Bgy
Calagasan	Bgy
Canbantug	Bgy
Canbanua	Bgy
Cansuje	Bgy
Capio-an	Bgy
Casay	Bgy
Catang	Bgy
Colawin	Bgy
Conalum	Bgy
Guiwanon	Bgy
Gutlang	Bgy
Jampang	Bgy
Jomgao	Bgy
Lamacan	Bgy
Langtad	Bgy
Langub	Bgy
Lapay	Bgy
Lengigon	Bgy
Linut-od	Bgy
Mabasa	Bgy
Mandilikit	Bgy
Mompeller	Bgy
Panadtaran	Bgy
Poblacion	Bgy
Sua	Bgy
Sumaguan	Bgy
Tabayag	Bgy
Talaga	Bgy
Talaytay	Bgy
Talo-ot	Bgy
Tiguib	Bgy
Tulang	Bgy
Tulic	Bgy
Ubaub	Bgy
Usmad	Bgy
Asturias	Mun
Agbanga	Bgy
Agtugop	Bgy
Bago	Bgy
Bairan	Bgy
Banban	Bgy
Baye	Bgy
Bog-o	Bgy
Kaluangan	Bgy
Lanao	Bgy
Langub	Bgy
Looc Norte	Bgy
Lunas	Bgy
Magcalape	Bgy
Manguiao	Bgy
New Bago	Bgy
Owak	Bgy
Poblacion	Bgy
Saksak	Bgy
San Isidro	Bgy
San Roque	Bgy
Santa Lucia	Bgy
Santa Rita	Bgy
Tag-amakan	Bgy
Tagbubonga	Bgy
Tubigagmanok	Bgy
Tubod	Bgy
Ubogon	Bgy
Badian	Mun
Alawijao	Bgy
Balhaan	Bgy
Banhigan	Bgy
Basak	Bgy
Basiao	Bgy
Bato	Bgy
Bugas	Bgy
Calangcang	Bgy
Candiis	Bgy
Dagatan	Bgy
Dobdob	Bgy
Ginablan	Bgy
Lambug	Bgy
Malabago	Bgy
Malhiao	Bgy
Manduyong	Bgy
Matutinao	Bgy
Patong	Bgy
Poblacion	Bgy
Sanlagan	Bgy
Santicon	Bgy
Sohoton	Bgy
Sulsugan	Bgy
Talayong	Bgy
Taytay	Bgy
Tigbao	Bgy
Tiguib	Bgy
Tubod	Bgy
Zaragosa	Bgy
Balamban	Mun
Abucayan	Bgy
Aliwanay	Bgy
Arpili	Bgy
Bayong	Bgy
Biasong	Bgy
Buanoy	Bgy
Cabagdalan	Bgy
Cabasiangan	Bgy
Cambuhawe	Bgy
Cansomoroy	Bgy
Cantibas	Bgy
Cantuod	Bgy
Duangan	Bgy
Gaas	Bgy
Ginatilan	Bgy
Hingatmonan	Bgy
Lamesa	Bgy
Liki	Bgy
Luca	Bgy
Matun-og	Bgy
Nangka	Bgy
Pondol	Bgy
Prenza	Bgy
Singsing	Bgy
Sunog	Bgy
Vito	Bgy
Baliwagan (Pob.)	Bgy
Santa Cruz-Santo Niño (Pob.)	Bgy
Bantayan	Mun
Atop-atop	Bgy
Baigad	Bgy
Baod	Bgy
Binaobao (Pob.)	Bgy
Botigues	Bgy
Kabac	Bgy
Doong	Bgy
Hilotongan	Bgy
Guiwanon	Bgy
Kabangbang	Bgy
Kampingganon	Bgy
Kangkaibe	Bgy
Lipayran	Bgy
Luyongbaybay	Bgy
Mojon	Bgy
Obo-ob	Bgy
Patao	Bgy
Putian	Bgy
Sillon	Bgy
Sungko	Bgy
Suba (Pob.)	Bgy
Sulangan	Bgy
Tamiao	Bgy
Bantigue (Pob.)	Bgy
Ticad (Pob.)	Bgy
Barili	Mun
Azucena	Bgy
Bagakay	Bgy
Balao	Bgy
Bolocboloc	Bgy
Budbud	Bgy
Bugtong Kawayan	Bgy
Cabcaban	Bgy
Campangga	Bgy
Dakit	Bgy
Giloctog	Bgy
Guibuangan	Bgy
Giwanon	Bgy
Gunting	Bgy
Hilasgasan	Bgy
Japitan	Bgy
Cagay	Bgy
Kalubihan	Bgy
Kangdampas	Bgy
Candugay	Bgy
Luhod	Bgy
Lupo	Bgy
Luyo	Bgy
Maghanoy	Bgy
Maigang	Bgy
Malolos	Bgy
Mantalongon	Bgy
Mantayupan	Bgy
Mayana	Bgy
Minolos	Bgy
Nabunturan	Bgy
Nasipit	Bgy
Pancil	Bgy
Pangpang	Bgy
Paril	Bgy
Patupat	Bgy
Poblacion	Bgy
San Rafael	Bgy
Santa Ana	Bgy
Sayaw	Bgy
Tal-ot	Bgy
Tubod	Bgy
Vito	Bgy
City of Bogo	City
Cogon (Pob.)	Bgy
Anonang Norte	Bgy
Anonang Sur	Bgy
Banban	Bgy
Binabag	Bgy
Bungtod (Pob.)	Bgy
Carbon (Pob.)	Bgy
Cayang	Bgy
Dakit	Bgy
Don Pedro Rodriguez	Bgy
Gairan	Bgy
Guadalupe	Bgy
La Paz	Bgy
La Purisima Concepcion (Pob.)	Bgy
Libertad	Bgy
Lourdes (Pob.)	Bgy
Malingin	Bgy
Marangog	Bgy
Nailon	Bgy
Odlot	Bgy
Pandan	Bgy
Polambato	Bgy
Sambag (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santo Niño	Bgy
Santo Rosario (Pob.)	Bgy
Siocon	Bgy
Taytayan	Bgy
Sudlonon	Bgy
Boljoon	Mun
Baclayan	Bgy
El Pardo	Bgy
Granada	Bgy
Lower Becerril	Bgy
Poblacion	Bgy
San Antonio	Bgy
Upper Becerril	Bgy
Arbor	Bgy
Lunop	Bgy
Nangka	Bgy
South Granada	Bgy
Borbon	Mun
Bagacay	Bgy
Bili	Bgy
Bingay	Bgy
Bongdo	Bgy
Bongdo Gua	Bgy
Bongoyan	Bgy
Cadaruhan	Bgy
Cajel	Bgy
Campusong	Bgy
Clavera	Bgy
Don Gregorio Antigua	Bgy
Laaw	Bgy
Lugo	Bgy
Managase	Bgy
Poblacion	Bgy
Sagay	Bgy
San Jose	Bgy
Tabunan	Bgy
Tagnucan	Bgy
City of Carcar	City
Bolinawan	Bgy
Buenavista	Bgy
Calidngan	Bgy
Can-asujan	Bgy
Guadalupe	Bgy
Liburon	Bgy
Napo	Bgy
Ocana	Bgy
Perrelos	Bgy
Valencia	Bgy
Valladolid	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Tuyom	Bgy
Carmen	Mun
Baring	Bgy
Cantipay	Bgy
Cantumog	Bgy
Cantukong	Bgy
Caurasan	Bgy
Corte	Bgy
Dawis Norte	Bgy
Dawis Sur	Bgy
Cogon East	Bgy
Hagnaya	Bgy
Ipil	Bgy
Lanipga	Bgy
Liboron	Bgy
Lower Natimao-an	Bgy
Luyang	Bgy
Poblacion	Bgy
Puente	Bgy
Sac-on	Bgy
Triumfo	Bgy
Upper Natimao-an	Bgy
Cogon West	Bgy
Catmon	Mun
Agsuwao	Bgy
Amancion	Bgy
Anapog	Bgy
Bactas	Bgy
Bongyas	Bgy
Basak	Bgy
Binongkalan	Bgy
Cabungaan	Bgy
Cambangkaya	Bgy
Can-ibuang	Bgy
Catmondaan	Bgy
Duyan	Bgy
Ginabucan	Bgy
Macaas	Bgy
Panalipan	Bgy
Tabili	Bgy
Tinabyonan	Bgy
San Jose Pob.	Bgy
Corazon (Pob.)	Bgy
Flores (Pob.)	Bgy
City of Cebu (Capital)	City
Adlaon	Bgy
Agsungot	Bgy
Apas	Bgy
Babag	Bgy
Basak Pardo	Bgy
Bacayan	Bgy
Banilad	Bgy
Basak San Nicolas	Bgy
Binaliw	Bgy
Bonbon	Bgy
Budla-an (Pob.)	Bgy
Buhisan	Bgy
Bulacao	Bgy
Buot-Taup Pardo	Bgy
Busay (Pob.)	Bgy
Calamba	Bgy
Cambinocot	Bgy
Capitol Site (Pob.)	Bgy
Carreta	Bgy
Central (Pob.)	Bgy
Cogon Ramos (Pob.)	Bgy
Cogon Pardo	Bgy
Day-as	Bgy
Duljo (Pob.)	Bgy
Ermita (Pob.)	Bgy
Guadalupe	Bgy
Guba	Bgy
Hippodromo	Bgy
Inayawan	Bgy
Kalubihan (Pob.)	Bgy
Kalunasan	Bgy
Kamagayan (Pob.)	Bgy
Camputhaw (Pob.)	Bgy
Kasambagan	Bgy
Kinasang-an Pardo	Bgy
Labangon	Bgy
Lahug (Pob.)	Bgy
Lorega	Bgy
Lusaran	Bgy
Luz	Bgy
Mabini	Bgy
Mabolo	Bgy
Malubog	Bgy
Mambaling	Bgy
Pahina Central (Pob.)	Bgy
Pahina San Nicolas	Bgy
Pamutan	Bgy
Pardo (Pob.)	Bgy
Pari-an	Bgy
Paril	Bgy
Pasil	Bgy
Pit-os	Bgy
Pulangbato	Bgy
Pung-ol-Sibugay	Bgy
Punta Princesa	Bgy
Quiot Pardo	Bgy
Sambag I (Pob.)	Bgy
Sambag II (Pob.)	Bgy
San Antonio (Pob.)	Bgy
San Jose	Bgy
San Nicolas Central	Bgy
San Roque	Bgy
Santa Cruz (Pob.)	Bgy
Sawang Calero (Pob.)	Bgy
Sinsin	Bgy
Sirao	Bgy
Suba Pob.	Bgy
Sudlon I	Bgy
Sapangdaku	Bgy
T. Padilla	Bgy
Tabunan	Bgy
Tagbao	Bgy
Talamban	Bgy
Taptap	Bgy
Tejero	Bgy
Tinago	Bgy
Tisa	Bgy
To-ong Pardo	Bgy
Zapatera	Bgy
Sudlon II	Bgy
Compostela	Mun
Bagalnga	Bgy
Basak	Bgy
Buluang	Bgy
Cabadiangan	Bgy
Cambayog	Bgy
Canamucan	Bgy
Cogon	Bgy
Dapdap	Bgy
Estaca	Bgy
Lupa	Bgy
Magay	Bgy
Mulao	Bgy
Panangban	Bgy
Poblacion	Bgy
Tag-ube	Bgy
Tamiao	Bgy
Tubigan	Bgy
Consolacion	Mun
Cabangahan	Bgy
Cansaga	Bgy
Casili	Bgy
Danglag	Bgy
Garing	Bgy
Jugan	Bgy
Lamac	Bgy
Lanipga	Bgy
Nangka	Bgy
Panas	Bgy
Panoypoy	Bgy
Pitogo	Bgy
Poblacion Occidental	Bgy
Poblacion Oriental	Bgy
Polog	Bgy
Pulpogan	Bgy
Sacsac	Bgy
Tayud	Bgy
Tilhaong	Bgy
Tolotolo	Bgy
Tugbongan	Bgy
Cordova	Mun
Alegria	Bgy
Bangbang	Bgy
Buagsong	Bgy
Catarman	Bgy
Cogon	Bgy
Dapitan	Bgy
Day-as	Bgy
Gabi	Bgy
Gilutongan	Bgy
Ibabao	Bgy
Pilipog	Bgy
Poblacion	Bgy
San Miguel	Bgy
Daanbantayan	Mun
Aguho	Bgy
Bagay	Bgy
Bakhawan	Bgy
Bateria	Bgy
Bitoon	Bgy
Calape	Bgy
Carnaza	Bgy
Dalingding	Bgy
Lanao	Bgy
Logon	Bgy
Malbago	Bgy
Malingin	Bgy
Maya	Bgy
Pajo	Bgy
Paypay	Bgy
Poblacion	Bgy
Talisay	Bgy
Tapilon	Bgy
Tinubdan	Bgy
Tominjao	Bgy
Dalaguete	Mun
Ablayan	Bgy
Babayongan	Bgy
Balud	Bgy
Banhigan	Bgy
Bulak	Bgy
Caliongan	Bgy
Caleriohan	Bgy
Casay	Bgy
Catolohan	Bgy
Cawayan	Bgy
Consolacion	Bgy
Coro	Bgy
Dugyan	Bgy
Dumalan	Bgy
Jolomaynon	Bgy
Lanao	Bgy
Langkas	Bgy
Lumbang	Bgy
Malones	Bgy
Maloray	Bgy
Mananggal	Bgy
Manlapay	Bgy
Mantalongon	Bgy
Nalhub	Bgy
Obo	Bgy
Obong	Bgy
Panas	Bgy
Poblacion	Bgy
Sacsac	Bgy
Tapun	Bgy
Tuba	Bgy
Salug	Bgy
Tabon	Bgy
Danao City	City
Baliang	Bgy
Bayabas	Bgy
Binaliw	Bgy
Cabungahan	Bgy
Cagat-Lamac	Bgy
Cahumayan	Bgy
Cambanay	Bgy
Cambubho	Bgy
Cogon-Cruz	Bgy
Danasan	Bgy
Dungga	Bgy
Dunggoan	Bgy
Guinacot	Bgy
Guinsay	Bgy
Ibo	Bgy
Langosig	Bgy
Lawaan	Bgy
Licos	Bgy
Looc	Bgy
Magtagobtob	Bgy
Malapoc	Bgy
Manlayag	Bgy
Mantija	Bgy
Masaba	Bgy
Maslog	Bgy
Nangka	Bgy
Oguis	Bgy
Pili	Bgy
Poblacion	Bgy
Quisol	Bgy
Sabang	Bgy
Sacsac	Bgy
Sandayong Norte	Bgy
Sandayong Sur	Bgy
Santa Rosa	Bgy
Santican	Bgy
Sibacan	Bgy
Suba	Bgy
Taboc	Bgy
Taytay	Bgy
Togonon	Bgy
Tuburan Sur	Bgy
Dumanjug	Mun
Balaygtiki	Bgy
Bitoon	Bgy
Bulak	Bgy
Bullogan	Bgy
Doldol	Bgy
Kabalaasnan	Bgy
Kabatbatan	Bgy
Calaboon	Bgy
Kambanog	Bgy
Camboang	Bgy
Candabong	Bgy
Kang-actol	Bgy
Kanghalo	Bgy
Kanghumaod	Bgy
Kanguha	Bgy
Kantangkas	Bgy
Kanyuko	Bgy
Cogon	Bgy
Kolabtingon	Bgy
Cotcoton	Bgy
Lamak	Bgy
Lawaan	Bgy
Liong	Bgy
Manlapay	Bgy
Masa	Bgy
Matalao	Bgy
Paculob	Bgy
Panlaan	Bgy
Pawa	Bgy
Ilaya (Pob.)	Bgy
Poblacion Looc	Bgy
Poblacion Sima	Bgy
Tangil	Bgy
Tapon	Bgy
Tubod-Bitoon	Bgy
Tubod-Dugoan	Bgy
Poblacion Central	Bgy
Ginatilan	Mun
Anao	Bgy
Cagsing	Bgy
Calabawan	Bgy
Cambagte	Bgy
Campisong	Bgy
Canorong	Bgy
Guiwanon	Bgy
Looc	Bgy
Malatbo	Bgy
Mangaco	Bgy
Palanas	Bgy
Poblacion	Bgy
Salamanca	Bgy
San Roque	Bgy
City of Lapu-Lapu	City
Agus	Bgy
Babag	Bgy
Bankal	Bgy
Baring	Bgy
Basak	Bgy
Buaya	Bgy
Calawisan	Bgy
Canjulao	Bgy
Caw-oy	Bgy
Cawhagan	Bgy
Caubian	Bgy
Gun-ob	Bgy
Ibo	Bgy
Looc	Bgy
Mactan	Bgy
Maribago	Bgy
Marigondon	Bgy
Pajac	Bgy
Pajo	Bgy
Pangan-an	Bgy
Poblacion	Bgy
Punta Engaño	Bgy
Pusok	Bgy
Sabang	Bgy
Santa Rosa	Bgy
Subabasbas	Bgy
Talima	Bgy
Tingo	Bgy
Tungasan	Bgy
San Vicente	Bgy
Liloan	Mun
Cabadiangan	Bgy
Calero	Bgy
Catarman	Bgy
Cotcot	Bgy
Jubay	Bgy
Lataban	Bgy
Mulao	Bgy
Poblacion	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Tabla	Bgy
Tayud	Bgy
Yati	Bgy
Madridejos	Mun
Bunakan	Bgy
Kangwayan	Bgy
Kaongkod	Bgy
Kodia	Bgy
Maalat	Bgy
Malbago	Bgy
Mancilang	Bgy
Pili	Bgy
Poblacion	Bgy
San Agustin	Bgy
Tabagak	Bgy
Talangnan	Bgy
Tarong	Bgy
Tugas	Bgy
Malabuyoc	Mun
Armeña	Bgy
Tolosa	Bgy
Cerdeña	Bgy
Labrador	Bgy
Looc	Bgy
Lombo	Bgy
Mahanlud	Bgy
Mindanao	Bgy
Montañeza	Bgy
Salmeron	Bgy
Santo Niño	Bgy
Sorsogon	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
City of Mandaue	City
Alang-alang	Bgy
Bakilid	Bgy
Banilad	Bgy
Basak	Bgy
Cabancalan	Bgy
Cambaro	Bgy
Canduman	Bgy
Casili	Bgy
Casuntingan	Bgy
Centro (Pob.)	Bgy
Cubacub	Bgy
Guizo	Bgy
Ibabao-Estancia	Bgy
Jagobiao	Bgy
Labogon	Bgy
Looc	Bgy
Maguikay	Bgy
Mantuyong	Bgy
Opao	Bgy
Pakna-an	Bgy
Pagsabungan	Bgy
Subangdaku	Bgy
Tabok	Bgy
Tawason	Bgy
Tingub	Bgy
Tipolo	Bgy
Umapad	Bgy
Medellin	Mun
Antipolo	Bgy
Curva	Bgy
Daanlungsod	Bgy
Dalingding Sur	Bgy
Dayhagon	Bgy
Gibitngil	Bgy
Canhabagat	Bgy
Caputatan Norte	Bgy
Caputatan Sur	Bgy
Kawit	Bgy
Lamintak Norte	Bgy
Luy-a	Bgy
Panugnawan	Bgy
Poblacion	Bgy
Tindog	Bgy
Don Virgilio Gonzales	Bgy
Lamintak Sur	Bgy
Maharuhay	Bgy
Mahawak	Bgy
Minglanilla	Mun
Cadulawan	Bgy
Calajo-an	Bgy
Camp 7	Bgy
Camp 8	Bgy
Cuanos	Bgy
Guindaruhan	Bgy
Linao	Bgy
Manduang	Bgy
Pakigne	Bgy
Poblacion Ward I	Bgy
Poblacion Ward II	Bgy
Poblacion Ward III	Bgy
Poblacion Ward IV	Bgy
Tubod	Bgy
Tulay	Bgy
Tunghaan	Bgy
Tungkop	Bgy
Vito	Bgy
Tungkil	Bgy
Moalboal	Mun
Agbalanga	Bgy
Bala	Bgy
Balabagon	Bgy
Basdiot	Bgy
Batadbatad	Bgy
Bugho	Bgy
Buguil	Bgy
Busay	Bgy
Lanao	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Saavedra	Bgy
Tomonoy	Bgy
Tuble	Bgy
Tunga	Bgy
City of Naga	City
Alfaco	Bgy
Bairan	Bgy
Balirong	Bgy
Cabungahan	Bgy
Cantao-an	Bgy
Central Poblacion	Bgy
Cogon	Bgy
Colon	Bgy
East Poblacion	Bgy
Inoburan	Bgy
Inayagan	Bgy
Jaguimit	Bgy
Lanas	Bgy
Langtad	Bgy
Lutac	Bgy
Mainit	Bgy
Mayana	Bgy
Naalad	Bgy
North Poblacion	Bgy
Pangdan	Bgy
Patag	Bgy
South Poblacion	Bgy
Tagjaguimit	Bgy
Tangke	Bgy
Tinaan	Bgy
Tuyan	Bgy
Uling	Bgy
West Poblacion	Bgy
Oslob	Mun
Alo	Bgy
Bangcogon	Bgy
Bonbon	Bgy
Calumpang	Bgy
Canangca-an	Bgy
Cañang	Bgy
Can-ukban	Bgy
Cansalo-ay	Bgy
Daanlungsod	Bgy
Gawi	Bgy
Hagdan	Bgy
Lagunde	Bgy
Looc	Bgy
Luka	Bgy
Mainit	Bgy
Manlum	Bgy
Nueva Caceres	Bgy
Poblacion	Bgy
Pungtod	Bgy
Tan-awan	Bgy
Tumalog	Bgy
Pilar	Mun
Biasong	Bgy
Cawit	Bgy
Dapdap	Bgy
Esperanza	Bgy
Lanao	Bgy
Lower Poblacion	Bgy
Moabog	Bgy
Montserrat	Bgy
San Isidro	Bgy
San Juan	Bgy
Upper Poblacion	Bgy
Villahermosa	Bgy
Imelda	Bgy
Pinamungajan	Mun
Anislag	Bgy
Anopog	Bgy
Binabag	Bgy
Buhingtubig	Bgy
Busay	Bgy
Butong	Bgy
Cabiangon	Bgy
Camugao	Bgy
Duangan	Bgy
Guimbawian	Bgy
Lamac	Bgy
Lut-od	Bgy
Mangoto	Bgy
Opao	Bgy
Pandacan	Bgy
Poblacion	Bgy
Punod	Bgy
Rizal	Bgy
Sacsac	Bgy
Sambagon	Bgy
Sibago	Bgy
Tajao	Bgy
Tangub	Bgy
Tanibag	Bgy
Tupas	Bgy
Tutay	Bgy
Poro	Mun
Adela	Bgy
Altavista	Bgy
Cagcagan	Bgy
Cansabusab	Bgy
Daan Paz	Bgy
Eastern Poblacion	Bgy
Esperanza	Bgy
Libertad	Bgy
Mabini	Bgy
Mercedes	Bgy
Pagsa	Bgy
Paz	Bgy
Rizal	Bgy
San Jose	Bgy
Santa Rita	Bgy
Teguis	Bgy
Western Poblacion	Bgy
Ronda	Mun
Butong	Bgy
Can-abuhon	Bgy
Canduling	Bgy
Cansalonoy	Bgy
Cansayahon	Bgy
Ilaya	Bgy
Langin	Bgy
Libo-o	Bgy
Malalay	Bgy
Palanas	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Tupas	Bgy
Vive	Bgy
Samboan	Mun
Basak	Bgy
Bonbon	Bgy
Bulangsuran	Bgy
Calatagan	Bgy
Cambigong	Bgy
Canorong	Bgy
Colase	Bgy
Dalahikan	Bgy
Jumangpas	Bgy
Camburoy	Bgy
Poblacion	Bgy
San Sebastian	Bgy
Suba	Bgy
Tangbo	Bgy
Monteverde	Bgy
San Fernando	Mun
Balud	Bgy
Balungag	Bgy
Basak	Bgy
Bugho	Bgy
Cabatbatan	Bgy
Greenhills	Bgy
Lantawan	Bgy
Liburon	Bgy
Magsico	Bgy
Poblacion North	Bgy
Panadtaran	Bgy
Pitalo	Bgy
San Isidro	Bgy
Sangat	Bgy
Poblacion South	Bgy
Tabionan	Bgy
Tananas	Bgy
Tinubdan	Bgy
Tonggo	Bgy
Tubod	Bgy
Ilaya	Bgy
San Francisco	Mun
Montealegre	Bgy
Cabunga-an	Bgy
Campo	Bgy
Consuelo	Bgy
Esperanza	Bgy
Himensulan	Bgy
Northern Poblacion	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Santiago	Bgy
Sonog	Bgy
Southern Poblacion	Bgy
Unidos	Bgy
Union	Bgy
Western Poblacion	Bgy
San Remigio	Mun
Anapog	Bgy
Argawanon	Bgy
Bagtic	Bgy
Bancasan	Bgy
Batad	Bgy
Busogon	Bgy
Calambua	Bgy
Canagahan	Bgy
Dapdap	Bgy
Gawaygaway	Bgy
Hagnaya	Bgy
Kayam	Bgy
Kinawahan	Bgy
Lambusan	Bgy
Lawis	Bgy
Libaong	Bgy
Looc	Bgy
Luyang	Bgy
Mano	Bgy
Poblacion	Bgy
Punta	Bgy
Sab-a	Bgy
San Miguel	Bgy
Tacup	Bgy
Tambongon	Bgy
To-ong	Bgy
Victoria	Bgy
Santa Fe	Mun
Hagdan	Bgy
Hilantagaan	Bgy
Kinatarkan	Bgy
Langub	Bgy
Maricaban	Bgy
Okoy	Bgy
Poblacion	Bgy
Balidbid	Bgy
Pooc	Bgy
Talisay	Bgy
Santander	Mun
Bunlan	Bgy
Cabutongan	Bgy
Candamiang	Bgy
Liloan	Bgy
Lip-tong	Bgy
Looc	Bgy
Pasil	Bgy
Poblacion	Bgy
Talisay	Bgy
Canlumacad	Bgy
Sibonga	Mun
Abugon	Bgy
Bae	Bgy
Bagacay	Bgy
Bahay	Bgy
Banlot	Bgy
Basak	Bgy
Bato	Bgy
Cagay	Bgy
Can-aga	Bgy
Candaguit	Bgy
Cantolaroy	Bgy
Dugoan	Bgy
Guimbangco-an	Bgy
Lamacan	Bgy
Libo	Bgy
Lindogon	Bgy
Magcagong	Bgy
Manatad	Bgy
Mangyan	Bgy
Papan	Bgy
Poblacion	Bgy
Sabang	Bgy
Sayao	Bgy
Simala	Bgy
Tubod	Bgy
Sogod	Mun
Ampongol	Bgy
Bagakay	Bgy
Bagatayam	Bgy
Bawo	Bgy
Cabalawan	Bgy
Cabangahan	Bgy
Calumboyan	Bgy
Dakit	Bgy
Damolog	Bgy
Ibabao	Bgy
Liki	Bgy
Lubo	Bgy
Mohon	Bgy
Nahus-an	Bgy
Poblacion	Bgy
Tabunok	Bgy
Takay	Bgy
Pansoy	Bgy
Tabogon	Mun
Alang-alang	Bgy
Caduawan	Bgy
Kal-anan	Bgy
Camoboan	Bgy
Canaocanao	Bgy
Combado	Bgy
Daantabogon	Bgy
Ilihan	Bgy
Labangon	Bgy
Libjo	Bgy
Loong	Bgy
Mabuli	Bgy
Managase	Bgy
Manlagtang	Bgy
Maslog	Bgy
Muabog	Bgy
Pio	Bgy
Poblacion	Bgy
Salag	Bgy
Sambag	Bgy
San Isidro	Bgy
San Vicente	Bgy
Somosa	Bgy
Taba-ao	Bgy
Tapul	Bgy
Tabuelan	Mun
Bongon	Bgy
Kanlim-ao	Bgy
Kanluhangon	Bgy
Kantubaon	Bgy
Dalid	Bgy
Mabunao	Bgy
Maravilla	Bgy
Olivo	Bgy
Poblacion	Bgy
Tabunok	Bgy
Tigbawan	Bgy
Villahermosa	Bgy
City of Talisay	City
Bulacao	Bgy
Cadulawan	Bgy
Cansojong	Bgy
Dumlog	Bgy
Jaclupan	Bgy
Lagtang	Bgy
Lawaan I	Bgy
Linao	Bgy
Maghaway	Bgy
Manipis	Bgy
Mohon	Bgy
Poblacion	Bgy
Pooc	Bgy
San Isidro	Bgy
San Roque	Bgy
Tabunoc	Bgy
Tangke	Bgy
Tapul	Bgy
Biasong	Bgy
Camp IV	Bgy
Lawaan II	Bgy
Lawaan III	Bgy
City of Toledo	City
Awihao	Bgy
Bagakay	Bgy
Bato	Bgy
Biga	Bgy
Bulongan	Bgy
Bunga	Bgy
Cabitoonan	Bgy
Calongcalong	Bgy
Cambang-ug	Bgy
Camp 8	Bgy
Canlumampao	Bgy
Cantabaco	Bgy
Capitan Claudio	Bgy
Carmen	Bgy
Daanglungsod	Bgy
Don Andres Soriano	Bgy
Dumlog	Bgy
Ibo	Bgy
Ilihan	Bgy
Landahan	Bgy
Loay	Bgy
Luray II	Bgy
Juan Climaco, Sr.	Bgy
Gen. Climaco	Bgy
Matab-ang	Bgy
Media Once	Bgy
Pangamihan	Bgy
Poblacion	Bgy
Poog	Bgy
Putingbato	Bgy
Sagay	Bgy
Sam-ang	Bgy
Sangi	Bgy
Santo Niño	Bgy
Subayon	Bgy
Talavera	Bgy
Tungkay	Bgy
Tubod	Bgy
Tuburan	Mun
Alegria	Bgy
Amatugan	Bgy
Antipolo	Bgy
Apalan	Bgy
Bagasawe	Bgy
Bakyawan	Bgy
Bangkito	Bgy
Bulwang	Bgy
Kabangkalan	Bgy
Kalangahan	Bgy
Kamansi	Bgy
Kan-an	Bgy
Kanlunsing	Bgy
Kansi	Bgy
Caridad	Bgy
Carmelo	Bgy
Cogon	Bgy
Colonia	Bgy
Daan Lungsod	Bgy
Fortaliza	Bgy
Ga-ang	Bgy
Gimama-a	Bgy
Jagbuaya	Bgy
Kabkaban	Bgy
Kagba-o	Bgy
Kampoot	Bgy
Kaorasan	Bgy
Libo	Bgy
Lusong	Bgy
Macupa	Bgy
Mag-alwa	Bgy
Mag-antoy	Bgy
Mag-atubang	Bgy
Maghan-ay	Bgy
Mangga	Bgy
Marmol	Bgy
Molobolo	Bgy
Montealegre	Bgy
Putat	Bgy
San Juan	Bgy
Sandayong	Bgy
Santo Niño	Bgy
Siotes	Bgy
Sumon	Bgy
Tominjao	Bgy
Tomugpa	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Barangay III (Pob.)	Bgy
Barangay IV (Pob.)	Bgy
Barangay V (Pob.)	Bgy
Barangay VI (Pob.)	Bgy
Barangay VII (Pob.)	Bgy
Barangay VIII (Pob.)	Bgy
Tudela	Mun
Buenavista	Bgy
Calmante	Bgy
Daan Secante	Bgy
General	Bgy
McArthur	Bgy
Northern Poblacion	Bgy
Puertobello	Bgy
Santander	Bgy
Secante Bag-o	Bgy
Southern Poblacion	Bgy
Villahermosa	Bgy
Negros Oriental	Prov
Amlan	Mun
Bio-os	Bgy
Jantianon	Bgy
Jugno	Bgy
Mag-abo	Bgy
Poblacion	Bgy
Silab	Bgy
Tambojangin	Bgy
Tandayag	Bgy
Ayungon	Mun
Amdus	Bgy
Jandalamanon	Bgy
Anibong	Bgy
Atabay	Bgy
Awa-an	Bgy
Ban-ban	Bgy
Calagcalag	Bgy
Candana-ay	Bgy
Carol-an	Bgy
Gomentoc	Bgy
Inacban	Bgy
Iniban	Bgy
Kilaban	Bgy
Lamigan	Bgy
Maaslum	Bgy
Mabato	Bgy
Manogtong	Bgy
Nabhang	Bgy
Tambo	Bgy
Tampocon I	Bgy
Tampocon II	Bgy
Tibyawan	Bgy
Tiguib	Bgy
Poblacion	Bgy
Bacong	Mun
Balayagmanok	Bgy
Banilad	Bgy
Buntis	Bgy
Buntod	Bgy
Calangag	Bgy
Combado	Bgy
Doldol	Bgy
Isugan	Bgy
Liptong	Bgy
Lutao	Bgy
Magsuhot	Bgy
Malabago	Bgy
Mampas	Bgy
North Poblacion	Bgy
Sacsac	Bgy
San Miguel	Bgy
South Poblacion	Bgy
Sulodpan	Bgy
Timbanga	Bgy
Timbao	Bgy
Tubod	Bgy
West Poblacion	Bgy
City of Bais	City
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Basak	Bgy
Biñohon	Bgy
Cabanlutan	Bgy
Calasga-an	Bgy
Cambagahan	Bgy
Cambaguio	Bgy
Cambanjao	Bgy
Cambuilao	Bgy
Canlargo	Bgy
Capiñahan	Bgy
Consolacion	Bgy
Dansulan	Bgy
Hangyad	Bgy
La Paz	Bgy
Lo-oc	Bgy
Lonoy	Bgy
Mabunao	Bgy
Manlipac	Bgy
Mansangaban	Bgy
Okiot	Bgy
Olympia	Bgy
Panala-an	Bgy
Panam-angan	Bgy
Rosario	Bgy
Sab-ahan	Bgy
San Isidro	Bgy
Katacgahan	Bgy
Tagpo	Bgy
Talungon	Bgy
Tamisu	Bgy
Tamogong	Bgy
Tangculogan	Bgy
Valencia	Bgy
Basay	Mun
Actin	Bgy
Bal-os	Bgy
Bongalonan	Bgy
Cabalayongan	Bgy
Cabatuanan	Bgy
Linantayan	Bgy
Maglinao	Bgy
Nagbo-alao	Bgy
Olandao	Bgy
Poblacion	Bgy
City of Bayawan	City
Ali-is	Bgy
Banaybanay	Bgy
Banga	Bgy
Villasol	Bgy
Boyco	Bgy
Bugay	Bgy
Cansumalig	Bgy
Dawis	Bgy
Kalamtukan	Bgy
Kalumboyan	Bgy
Malabugas	Bgy
Mandu-ao	Bgy
Maninihon	Bgy
Minaba	Bgy
Nangka	Bgy
Narra	Bgy
Pagatban	Bgy
Poblacion	Bgy
San Jose	Bgy
San Miguel	Bgy
San Roque	Bgy
Suba (Pob.)	Bgy
Tabuan	Bgy
Tayawan	Bgy
Tinago (Pob.)	Bgy
Ubos (Pob.)	Bgy
Villareal	Bgy
San Isidro	Bgy
Bindoy	Mun
Atotes	Bgy
Batangan	Bgy
Bulod	Bgy
Cabcaban	Bgy
Cabugan	Bgy
Camudlas	Bgy
Canluto	Bgy
Danao	Bgy
Danawan	Bgy
Domolog	Bgy
Malaga	Bgy
Manseje	Bgy
Matobato	Bgy
Nagcasunog	Bgy
Nalundan	Bgy
Pangalaycayan	Bgy
Peñahan	Bgy
Poblacion	Bgy
Salong	Bgy
Tagaytay	Bgy
Tinaogan	Bgy
Tubod	Bgy
City of Canlaon	City
Bayog	Bgy
Binalbagan	Bgy
Bucalan	Bgy
Linothangan	Bgy
Lumapao	Bgy
Malaiba	Bgy
Masulog	Bgy
Panubigan	Bgy
Mabigo (Pob.)	Bgy
Pula	Bgy
Budlasan	Bgy
Ninoy Aquino	Bgy
Dauin	Mun
Anahawan	Bgy
Apo Island	Bgy
Bagacay	Bgy
Baslay	Bgy
Batuhon Dacu	Bgy
Boloc-boloc	Bgy
Bulak	Bgy
Bunga	Bgy
Casile	Bgy
Libjo	Bgy
Lipayo	Bgy
Maayongtubig	Bgy
Mag-aso	Bgy
Magsaysay	Bgy
Malongcay Dacu	Bgy
Masaplod Norte	Bgy
Masaplod Sur	Bgy
Panubtuban	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Tugawe	Bgy
Tunga-tunga	Bgy
City of Dumaguete (Capital)	City
Bagacay	Bgy
Bajumpandan	Bgy
Balugo	Bgy
Banilad	Bgy
Bantayan	Bgy
Batinguel	Bgy
Bunao	Bgy
Cadawinonan	Bgy
Calindagan	Bgy
Camanjac	Bgy
Candau-ay	Bgy
Cantil-e	Bgy
Daro	Bgy
Junob	Bgy
Looc	Bgy
Mangnao-Canal	Bgy
Motong	Bgy
Piapi	Bgy
Poblacion No. 1	Bgy
Poblacion No. 2 	Bgy
Poblacion No. 3	Bgy
Poblacion No. 4	Bgy
Poblacion No. 5	Bgy
Poblacion No. 6	Bgy
Poblacion No. 7	Bgy
Poblacion No. 8	Bgy
Pulantubig	Bgy
Tabuctubig	Bgy
Taclobo	Bgy
Talay	Bgy
City of Guihulngan	City
Bakid	Bgy
Balogo	Bgy
Banwaque	Bgy
Basak	Bgy
Binobohan	Bgy
Buenavista	Bgy
Bulado	Bgy
Calamba	Bgy
Calupa-an	Bgy
Hibaiyo	Bgy
Hilaitan	Bgy
Hinakpan	Bgy
Humayhumay	Bgy
Imelda	Bgy
Kagawasan	Bgy
Linantuyan	Bgy
Luz	Bgy
Mabunga	Bgy
Mckinley	Bgy
Nagsaha	Bgy
Magsaysay	Bgy
Malusay	Bgy
Maniak	Bgy
Padre Zamora	Bgy
Plagatasanon	Bgy
Planas	Bgy
Poblacion	Bgy
Sandayao	Bgy
Tacpao	Bgy
Tinayunan Beach	Bgy
Tinayunan Hill	Bgy
Trinidad	Bgy
Villegas	Bgy
Jimalalud	Mun
Aglahug	Bgy
Agutayon	Bgy
Apanangon	Bgy
Bae	Bgy
Bala-as	Bgy
Bangcal	Bgy
Banog	Bgy
Buto	Bgy
Cabang	Bgy
Camandayon	Bgy
Cangharay	Bgy
Canlahao	Bgy
Dayoyo	Bgy
Eli	Bgy
Lacaon	Bgy
Mahanlud	Bgy
Malabago	Bgy
Mambaid	Bgy
Mongpong	Bgy
Owacan	Bgy
Pacuan	Bgy
Panglaya-an	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Polopantao	Bgy
Sampiniton	Bgy
Talamban	Bgy
Tamao	Bgy
La Libertad	Mun
Aniniaw	Bgy
Aya	Bgy
Bagtic	Bgy
Biga-a	Bgy
Busilak	Bgy
Cangabo	Bgy
Cantupa	Bgy
Eli	Bgy
Guihob	Bgy
Kansumandig	Bgy
Mambulod	Bgy
Mandapaton	Bgy
Manghulyawon	Bgy
Manluminsag	Bgy
Mapalasan	Bgy
Maragondong	Bgy
Martilo	Bgy
Nasungan	Bgy
Pacuan	Bgy
Pangca	Bgy
Pisong	Bgy
Pitogo	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
San Jose	Bgy
Solongon	Bgy
Tala-on	Bgy
Talayong	Bgy
Elecia	Bgy
Mabinay	Mun
Abis	Bgy
Arebasore	Bgy
Bagtic	Bgy
Banban	Bgy
Barras	Bgy
Bato	Bgy
Bugnay	Bgy
Bulibulihan	Bgy
Bulwang	Bgy
Campanun-an	Bgy
Canggohob	Bgy
Cansal-ing	Bgy
Dagbasan	Bgy
Dahile	Bgy
Himocdongon	Bgy
Hagtu	Bgy
Inapoy	Bgy
Lamdas	Bgy
Lumbangan	Bgy
Luyang	Bgy
Manlingay	Bgy
Mayaposi	Bgy
Napasu-an	Bgy
New Namangka	Bgy
Old Namangka	Bgy
Pandanon	Bgy
Paniabonan	Bgy
Pantao	Bgy
Poblacion	Bgy
Samac	Bgy
Tadlong	Bgy
Tara	Bgy
Manjuyod	Mun
Alangilanan	Bgy
Bagtic	Bgy
Balaas	Bgy
Bantolinao	Bgy
Bolisong	Bgy
Butong	Bgy
Campuyo	Bgy
Candabong	Bgy
Concepcion	Bgy
Dungo-an	Bgy
Kauswagan	Bgy
Libjo	Bgy
Lamogong	Bgy
Maaslum	Bgy
Mandalupang	Bgy
Panciao	Bgy
Poblacion	Bgy
Sac-sac	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Jose	Bgy
Santa Monica	Bgy
Suba	Bgy
Sundo-an	Bgy
Tanglad	Bgy
Tubod	Bgy
Tupas	Bgy
Pamplona	Mun
Abante	Bgy
Balayong	Bgy
Banawe	Bgy
Datagon	Bgy
Fatima	Bgy
Inawasan	Bgy
Magsusunog	Bgy
Malalangsi	Bgy
Mamburao	Bgy
Mangoto	Bgy
Poblacion	Bgy
San Isidro	Bgy
Santa Agueda	Bgy
Simborio	Bgy
Yupisan	Bgy
Calicanan	Bgy
San Jose	Mun
Basak	Bgy
Basiao	Bgy
Cambaloctot	Bgy
Cancawas	Bgy
Janayjanay	Bgy
Jilocon	Bgy
Naiba	Bgy
Poblacion	Bgy
San Roque	Bgy
Santo Niño	Bgy
Señora Ascion	Bgy
Siapo	Bgy
Tampi	Bgy
Tapon Norte	Bgy
Santa Catalina	Mun
Alangilan	Bgy
Amio	Bgy
Buenavista	Bgy
Kabulacan	Bgy
Caigangan	Bgy
Caranoche	Bgy
Cawitan	Bgy
Fatima	Bgy
Mabuhay	Bgy
Manalongon	Bgy
Mansagomayon	Bgy
Milagrosa	Bgy
Nagbinlod	Bgy
Nagbalaye	Bgy
Obat	Bgy
Poblacion	Bgy
San Francisco	Bgy
San Jose	Bgy
San Miguel	Bgy
San Pedro	Bgy
Santo Rosario	Bgy
Talalak	Bgy
Siaton	Mun
Albiga	Bgy
Apoloy	Bgy
Bonawon	Bgy
Bonbonon	Bgy
Cabangahan	Bgy
Canaway	Bgy
Casala-an	Bgy
Caticugan	Bgy
Datag	Bgy
Giliga-on	Bgy
Inalad	Bgy
Malabuhan	Bgy
Maloh	Bgy
Mantiquil	Bgy
Mantuyop	Bgy
Napacao	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Salag	Bgy
San Jose	Bgy
Sandulot	Bgy
Si-it	Bgy
Sumaliring	Bgy
Tayak	Bgy
Sibulan	Mun
Agan-an	Bgy
Ajong	Bgy
Balugo	Bgy
Bolocboloc	Bgy
Calabnugan	Bgy
Cangmating	Bgy
Enrique Villanueva	Bgy
Looc	Bgy
Magatas	Bgy
Maningcao	Bgy
Maslog	Bgy
Poblacion	Bgy
San Antonio	Bgy
Tubigon	Bgy
Tubtubon	Bgy
City of Tanjay	City
Azagra	Bgy
Bahi-an	Bgy
Luca	Bgy
Manipis	Bgy
Novallas	Bgy
Obogon	Bgy
Pal-ew	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
Poblacion VI	Bgy
Poblacion VII	Bgy
Poblacion VIII	Bgy
Poblacion IX	Bgy
Polo	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
Santa Cruz Nuevo	Bgy
Santa Cruz Viejo	Bgy
Santo Niño	Bgy
Tugas	Bgy
Tayasan	Mun
Bacong	Bgy
Bago	Bgy
Banga	Bgy
Cabulotan	Bgy
Cambaye	Bgy
Dalaupon	Bgy
Guincalaban	Bgy
Ilaya-Tayasan	Bgy
Jilabangan	Bgy
Lag-it	Bgy
Linao	Bgy
Lutay	Bgy
Maglihe	Bgy
Matauta	Bgy
Magtuhao	Bgy
Matuog	Bgy
Numnum	Bgy
Palaslan	Bgy
Pindahan	Bgy
Pinalubngan	Bgy
Pinocawan	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Saying	Bgy
Suquib	Bgy
Tamao	Bgy
Tambulan	Bgy
Tanlad	Bgy
Valencia	Mun
Apolong	Bgy
Balabag East	Bgy
Balabag West	Bgy
Balayagmanok	Bgy
Balili	Bgy
Balugo	Bgy
Bongbong	Bgy
Bong-ao	Bgy
Calayugan	Bgy
Cambucad	Bgy
Dobdob	Bgy
Jawa	Bgy
Caidiocan	Bgy
Liptong	Bgy
Lunga	Bgy
Malabo	Bgy
Malaunay	Bgy
Mampas	Bgy
Palinpinon	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Puhagan	Bgy
Pulangbato	Bgy
Sagbang	Bgy
Vallehermoso	Mun
Bagawines	Bgy
Bairan	Bgy
Don Espiridion Villegas	Bgy
Guba	Bgy
Cabulihan	Bgy
Macapso	Bgy
Malangsa	Bgy
Molobolo	Bgy
Maglahos	Bgy
Pinocawan	Bgy
Poblacion	Bgy
Puan	Bgy
Tabon	Bgy
Tagbino	Bgy
Ulay	Bgy
Zamboanguita	Mun
Basac	Bgy
Calango	Bgy
Lotuban	Bgy
Malongcay Diot	Bgy
Maluay	Bgy
Mayabon	Bgy
Nabago	Bgy
Nasig-id	Bgy
Najandig	Bgy
Poblacion	Bgy
Siquijor	Prov
Enrique Villanueva	Mun
Balolong	Bgy
Bino-ongan	Bgy
Bitaug	Bgy
Bolot	Bgy
Camogao	Bgy
Cangmangki	Bgy
Libo	Bgy
Lomangcapan	Bgy
Lotloton	Bgy
Manan-ao	Bgy
Olave	Bgy
Parian	Bgy
Poblacion	Bgy
Tulapos	Bgy
Larena	Mun
Bagacay	Bgy
Balolang	Bgy
Basac	Bgy
Bintangan	Bgy
Bontod	Bgy
Cabulihan	Bgy
Calunasan	Bgy
Candigum	Bgy
Cang-allas	Bgy
Cang-apa	Bgy
Cangbagsa	Bgy
Cangmalalag	Bgy
Canlambo	Bgy
Canlasog	Bgy
Catamboan	Bgy
Helen	Bgy
Nonoc	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Ponong	Bgy
Sabang	Bgy
Sandugan	Bgy
Taculing	Bgy
Lazi	Mun
Campalanas	Bgy
Cangclaran	Bgy
Cangomantong	Bgy
Capalasanan	Bgy
Catamboan (Pob.)	Bgy
Gabayan	Bgy
Kimba	Bgy
Kinamandagan	Bgy
Lower Cabangcalan	Bgy
Nagerong	Bgy
Po-o	Bgy
Simacolong	Bgy
Tagmanocan	Bgy
Talayong	Bgy
Tigbawan (Pob.)	Bgy
Tignao	Bgy
Upper Cabangcalan	Bgy
Ytaya	Bgy
Maria	Mun
Bogo	Bgy
Bonga	Bgy
Cabal-asan	Bgy
Calunasan	Bgy
Candaping A	Bgy
Candaping B	Bgy
Cantaroc A	Bgy
Cantaroc B	Bgy
Cantugbas	Bgy
Lico-an	Bgy
Lilo-an	Bgy
Looc	Bgy
Logucan	Bgy
Minalulan	Bgy
Nabutay	Bgy
Olang	Bgy
Pisong A	Bgy
Pisong B	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Saguing	Bgy
Sawang	Bgy
San Juan	Mun
Canasagan	Bgy
Candura	Bgy
Cangmunag	Bgy
Cansayang	Bgy
Catulayan	Bgy
Lala-o	Bgy
Maite	Bgy
Napo	Bgy
Paliton	Bgy
Poblacion	Bgy
Solangon	Bgy
Tag-ibo	Bgy
Tambisan	Bgy
Timbaon	Bgy
Tubod	Bgy
Siquijor (Capital)	Mun
Banban	Bgy
Bolos	Bgy
Caipilan	Bgy
Caitican	Bgy
Calalinan	Bgy
Cang-atuyom	Bgy
Canal	Bgy
Candanay Norte	Bgy
Candanay Sur	Bgy
Cang-adieng	Bgy
Cang-agong	Bgy
Cang-alwang	Bgy
Cang-asa	Bgy
Cang-inte	Bgy
Cang-isad	Bgy
Canghunoghunog	Bgy
Cangmatnog	Bgy
Cangmohao	Bgy
Cantabon	Bgy
Caticugan	Bgy
Dumanhog	Bgy
Ibabao	Bgy
Lambojon	Bgy
Luyang	Bgy
Luzong	Bgy
Olo	Bgy
Pangi	Bgy
Panlautan	Bgy
Pasihagon	Bgy
Pili	Bgy
Poblacion	Bgy
Polangyuta	Bgy
Ponong	Bgy
Sabang	Bgy
San Antonio	Bgy
Songculan	Bgy
Tacdog	Bgy
Tacloban	Bgy
Tambisan	Bgy
Tebjong	Bgy
Tinago	Bgy
Tongo	Bgy
Region VIII (Eastern Visayas)	Reg
Eastern Samar	Prov
Arteche	Mun
Aguinaldo	Bgy
Balud (Pob.)	Bgy
Bato	Bgy
Beri	Bgy
Bigo	Bgy
Buenavista	Bgy
Cagsalay	Bgy
Campacion	Bgy
Carapdapan	Bgy
Casidman	Bgy
Catumsan	Bgy
Central (Pob.)	Bgy
Concepcion	Bgy
Garden (Pob.)	Bgy
Inayawan	Bgy
Macarthur	Bgy
Rawis (Pob.)	Bgy
Tangbo	Bgy
Tawagan	Bgy
Tebalawon	Bgy
Balangiga	Mun
Bacjao	Bgy
Cag-olango	Bgy
Cansumangcay	Bgy
Guinmaayohan	Bgy
Barangay Poblacion I	Bgy
Barangay Poblacion II	Bgy
Barangay Poblacion III	Bgy
Barangay Poblacion IV	Bgy
Barangay Poblacion V	Bgy
Barangay Poblacion VI	Bgy
San Miguel	Bgy
Santa Rosa	Bgy
Maybunga	Bgy
Balangkayan	Mun
Balogo	Bgy
Bangon	Bgy
Cabay	Bgy
Caisawan	Bgy
Cantubi	Bgy
General Malvar	Bgy
Guinpoliran	Bgy
Julag	Bgy
Magsaysay	Bgy
Maramag	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
City of Borongan (Capital)	City
Alang-alang	Bgy
Amantacop	Bgy
Ando	Bgy
Balacdas	Bgy
Balud	Bgy
Banuyo	Bgy
Baras	Bgy
Bato	Bgy
Bayobay	Bgy
Benowangan	Bgy
Bugas	Bgy
Cabalagnan	Bgy
Cabong	Bgy
Cagbonga	Bgy
Calico-an	Bgy
Calingatngan	Bgy
Campesao	Bgy
Can-abong	Bgy
Can-aga	Bgy
Camada	Bgy
Canjaway	Bgy
Canlaray	Bgy
Canyopay	Bgy
Divinubo	Bgy
Hebacong	Bgy
Hindang	Bgy
Lalawigan	Bgy
Libuton	Bgy
Locso-on	Bgy
Maybacong	Bgy
Maypangdan	Bgy
Pepelitan	Bgy
Pinanag-an	Bgy
Purok D1 (Pob.)	Bgy
Purok A (Pob.)	Bgy
Purok B (Pob.)	Bgy
Purok C (Pob.)	Bgy
Purok D2 (Pob.)	Bgy
Purok E (Pob.)	Bgy
Purok F (Pob.)	Bgy
Purok G (Pob.)	Bgy
Purok H (Pob.)	Bgy
Punta Maria	Bgy
Sabang North	Bgy
Sabang South	Bgy
San Andres	Bgy
San Gabriel	Bgy
San Gregorio	Bgy
San Jose	Bgy
San Mateo	Bgy
San Pablo	Bgy
San Saturnino	Bgy
Santa Fe	Bgy
Siha	Bgy
Songco	Bgy
Sohutan	Bgy
Suribao	Bgy
Surok	Bgy
Taboc	Bgy
Tabunan	Bgy
Tamoso	Bgy
Can-Avid	Mun
Balagon	Bgy
Baruk	Bgy
Boco	Bgy
Caghalong	Bgy
Camantang	Bgy
Can-ilay	Bgy
Cansangaya	Bgy
Canteros	Bgy
Carolina	Bgy
Guibuangan	Bgy
Jepaco	Bgy
Mabuhay	Bgy
Malogo	Bgy
Obong	Bgy
Pandol	Bgy
Barangay 1 Poblacion	Bgy
Barangay 10 Poblacion	Bgy
Barangay 2 Poblacion	Bgy
Barangay 3 Poblacion	Bgy
Barangay 4 Poblacion	Bgy
Barangay 5 Poblacion	Bgy
Barangay 6 Poblacion	Bgy
Barangay 7 Poblacion	Bgy
Barangay 8 Poblacion	Bgy
Barangay 9 Poblacion	Bgy
Salvacion	Bgy
Solong	Bgy
Rawis	Bgy
Dolores	Mun
Aroganga	Bgy
Magongbong	Bgy
Buenavista	Bgy
Cabago-an	Bgy
Caglao-an	Bgy
Cagtabon	Bgy
Dampigan	Bgy
Dapdap	Bgy
Del Pilar	Bgy
Denigpian	Bgy
Gap-ang	Bgy
Japitan	Bgy
Jicontol	Bgy
Hilabaan	Bgy
Hinolaso	Bgy
Libertad	Bgy
Magsaysay	Bgy
Malobago	Bgy
Osmeña	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 15 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Rizal	Bgy
San Isidro	Bgy
San Pascual	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Tanauan	Bgy
Villahermosa	Bgy
Bonghon	Bgy
Malaintos	Bgy
Tikling	Bgy
General Macarthur	Mun
Alang-alang	Bgy
Binalay	Bgy
Calutan	Bgy
Camcuevas	Bgy
Domrog	Bgy
Limbujan	Bgy
Magsaysay	Bgy
Osmeña	Bgy
Pingan	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Poblacion Barangay 5	Bgy
Poblacion Barangay 6	Bgy
Poblacion Barangay 7	Bgy
Poblacion Barangay 8	Bgy
Laurel	Bgy
Roxas	Bgy
Quirino	Bgy
San Isidro	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santa Fe	Bgy
Tugop	Bgy
Vigan	Bgy
Macapagal	Bgy
Aguinaldo	Bgy
Quezon	Bgy
Tandang Sora	Bgy
Giporlos	Mun
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Biga	Bgy
Coticot	Bgy
Gigoso	Bgy
Huknan	Bgy
Parina	Bgy
Paya	Bgy
President Roxas	Bgy
San Miguel	Bgy
Santa Cruz	Bgy
Barangay 8 (Pob.)	Bgy
San Isidro	Bgy
Guiuan	Mun
Alingarog	Bgy
Bagua	Bgy
Banaag	Bgy
Banahao	Bgy
Baras	Bgy
Barbo	Bgy
Bitaugan	Bgy
Bungtod	Bgy
Bucao	Bgy
Buenavista	Bgy
Cagdara-o	Bgy
Cagusu-an	Bgy
Camparang	Bgy
Campoyong	Bgy
Cantahay	Bgy
Casuguran	Bgy
Cogon	Bgy
Culasi	Bgy
Poblacion Ward 10	Bgy
Poblacion Ward 9-A	Bgy
Gahoy	Bgy
Habag	Bgy
Hamorawon	Bgy
Inapulangan	Bgy
Poblacion Ward 4-A	Bgy
Lupok (Pob.)	Bgy
Mayana	Bgy
Ngolos	Bgy
Pagbabangnan	Bgy
Pagnamitan	Bgy
Poblacion Ward 1	Bgy
Poblacion Ward 2	Bgy
Poblacion Ward 11	Bgy
Poblacion Ward 12	Bgy
Poblacion Ward 3	Bgy
Poblacion Ward 4	Bgy
Poblacion Ward 5	Bgy
Poblacion Ward 6	Bgy
Poblacion Ward 7	Bgy
Poblacion Ward 8	Bgy
Poblacion Ward 9	Bgy
Salug	Bgy
San Antonio	Bgy
San Jose	Bgy
San Pedro	Bgy
Sapao	Bgy
Sulangan	Bgy
Suluan	Bgy
Surok	Bgy
Taytay	Bgy
Timala	Bgy
Trinidad	Bgy
Victory Island	Bgy
Canawayon	Bgy
Dalaragan	Bgy
Hagna	Bgy
Hollywood	Bgy
San Juan	Bgy
Santo Niño	Bgy
Tagporo	Bgy
Hernani	Mun
Batang	Bgy
Cacatmonan	Bgy
Garawon	Bgy
Canciledes	Bgy
Carmen	Bgy
Nagaja	Bgy
Padang	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
San Isidro	Bgy
San Miguel	Bgy
Jipapad	Mun
Agsaman	Bgy
Cagmanaba	Bgy
Dorillo	Bgy
Jewaran	Bgy
Mabuhay	Bgy
Magsaysay	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Recare	Bgy
Roxas	Bgy
San Roque	Bgy
Lawaan	Mun
Betaog	Bgy
Bolusao	Bgy
Guinob-an	Bgy
Maslog	Bgy
Barangay Poblacion 1	Bgy
Barangay Poblacion 2	Bgy
Barangay Poblacion 3	Bgy
Barangay Poblacion 4	Bgy
Barangay Poblacion 5	Bgy
Barangay Poblacion 6	Bgy
Barangay Poblacion 7	Bgy
Barangay Poblacion 8	Bgy
Barangay Poblacion 9	Bgy
Barangay Poblacion 10	Bgy
San Isidro	Bgy
Taguite	Bgy
Llorente	Mun
Antipolo	Bgy
Babanikhon	Bgy
Bacayawan	Bgy
Barobo	Bgy
Burak	Bgy
Can-ato	Bgy
Candoros	Bgy
Canliwag	Bgy
Cantomco	Bgy
Hugpa	Bgy
Maca-anga	Bgy
Magtino	Bgy
Mina-anod	Bgy
Naubay	Bgy
Piliw	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
San Jose	Bgy
San Miguel	Bgy
San Roque	Bgy
So-ong	Bgy
Tabok	Bgy
Waso	Bgy
Maslog	Mun
Bulawan	Bgy
Carayacay	Bgy
Libertad	Bgy
Malobago	Bgy
Maputi	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
San Miguel	Bgy
San Roque	Bgy
Tangbo	Bgy
Taytay	Bgy
Tugas	Bgy
Maydolong	Mun
Camada	Bgy
Campakerit	Bgy
Canloterio	Bgy
Del Pilar	Bgy
Guindalitan	Bgy
Lapgap	Bgy
Malobago	Bgy
Maybocog	Bgy
Maytigbao	Bgy
Omawas	Bgy
Patag	Bgy
Barangay Poblacion 1	Bgy
Barangay Poblacion 2	Bgy
Barangay Poblacion 3	Bgy
Barangay Poblacion 4	Bgy
Barangay Poblacion 5	Bgy
Barangay Poblacion 6	Bgy
Barangay Poblacion 7	Bgy
San Gabriel	Bgy
Tagaslian	Bgy
Mercedes	Mun
Anuron	Bgy
Banuyo	Bgy
Bobon	Bgy
Busay	Bgy
Buyayawon	Bgy
Cabunga-an	Bgy
Cambante	Bgy
Barangay 1 Poblacion	Bgy
Barangay 2 Poblacion	Bgy
Barangay 3 Poblacion	Bgy
San Jose	Bgy
Sung-an	Bgy
Palamrag	Bgy
Barangay 4 Poblacion	Bgy
Port Kennedy	Bgy
San Roque	Bgy
Oras	Mun
Agsam	Bgy
Bagacay	Bgy
Balingasag	Bgy
Balocawe (Pob.)	Bgy
Bantayan	Bgy
Batang	Bgy
Bato	Bgy
Binalayan	Bgy
Buntay	Bgy
Burak	Bgy
Butnga (Pob.)	Bgy
Cadian	Bgy
Cagdine	Bgy
Cagpile	Bgy
Cagtoog	Bgy
Camanga (Pob.)	Bgy
Dalid	Bgy
Dao	Bgy
Factoria	Bgy
Gamot	Bgy
Iwayan	Bgy
Japay	Bgy
Kalaw	Bgy
Mabuhay	Bgy
Malingon	Bgy
Minap-os	Bgy
Nadacpan	Bgy
Naga	Bgy
Pangudtan	Bgy
Paypayon (Pob.)	Bgy
Riverside (Pob.)	Bgy
Rizal	Bgy
Sabang	Bgy
San Eduardo	Bgy
Santa Monica	Bgy
Saugan	Bgy
Saurong	Bgy
Tawagan (Pob.)	Bgy
Tiguib (Pob.)	Bgy
Trinidad	Bgy
Alang-alang	Bgy
San Roque (Pob.)	Bgy
Quinapondan	Mun
Anislag	Bgy
Bagte	Bgy
Barangay No. 1 (Pob.)	Bgy
Barangay No. 2 (Pob.)	Bgy
Barangay No. 3 (Pob.)	Bgy
Barangay No. 4 (Pob.)	Bgy
Barangay No. 5 (Pob.)	Bgy
Buenavista	Bgy
Caculangan	Bgy
Cagdaja	Bgy
Cambilla	Bgy
Cantenio	Bgy
Naga	Bgy
Paco	Bgy
Rizal	Bgy
San Pedro	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Margarita	Bgy
Santo Niño	Bgy
Palactad	Bgy
Alang-alang	Bgy
Barangay No. 6 (Pob.)	Bgy
Barangay No. 7 (Pob.)	Bgy
San Isidro	Bgy
Salcedo	Mun
Abejao	Bgy
Alog	Bgy
Asgad	Bgy
Bagtong	Bgy
Balud	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Buabua	Bgy
Burak	Bgy
Butig	Bgy
Cagaut	Bgy
Camanga	Bgy
Cantomoja	Bgy
Carapdapan	Bgy
Caridad	Bgy
Casili-on	Bgy
Iberan	Bgy
Jagnaya	Bgy
Lusod	Bgy
Malbog	Bgy
Maliwaliw	Bgy
Matarinao	Bgy
Naparaan	Bgy
Seguinon	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Tacla-on	Bgy
Tagbacan	Bgy
Palanas	Bgy
Talangdawan	Bgy
San Julian	Mun
Bunacan	Bgy
Campidhan	Bgy
Casoroy	Bgy
Libas	Bgy
Nena	Bgy
Pagbabangnan	Bgy
Barangay No. 1 Poblacion	Bgy
Barangay No. 2 Poblacion	Bgy
Barangay No. 3 Poblacion	Bgy
Barangay No. 4 Poblacion	Bgy
Barangay No. 5 Poblacion	Bgy
Barangay No. 6 Poblacion	Bgy
Putong	Bgy
San Isidro	Bgy
San Miguel	Bgy
Lunang	Bgy
San Policarpo	Mun
Alugan	Bgy
Bahay	Bgy
Bangon	Bgy
Baras	Bgy
Binogawan	Bgy
Cajagwayan	Bgy
Japunan	Bgy
Natividad	Bgy
Pangpang	Bgy
Barangay No. 1 (Pob.)	Bgy
Barangay No. 2 (Pob.)	Bgy
Barangay No. 3 (Pob.)	Bgy
Barangay No. 4 (Pob.)	Bgy
Barangay No. 5 (Pob.)	Bgy
Santa Cruz	Bgy
Tabo	Bgy
Tan-awan	Bgy
Sulat	Mun
A-et	Bgy
Baybay (Pob.)	Bgy
Kandalakit	Bgy
Del Remedio	Bgy
Loyola Heights (Pob.)	Bgy
Mabini	Bgy
Maglipay (Pob.)	Bgy
Maramara (Pob.)	Bgy
Riverside (Pob.)	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Juan	Bgy
San Mateo	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Santo Tomas	Bgy
Tabi (Pob.)	Bgy
Abucay (Pob.)	Bgy
Taft	Mun
Batiawan	Bgy
Beto	Bgy
Binaloan	Bgy
Bongdo	Bgy
Dacul	Bgy
Danao	Bgy
Del Remedios	Bgy
Gayam	Bgy
Lomatud	Bgy
Mabuhay	Bgy
Malinao	Bgy
Mantang	Bgy
Nato	Bgy
Pangabutan	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Poblacion Barangay 5	Bgy
Poblacion Barangay 6	Bgy
Polangi	Bgy
San Luis	Bgy
San Pablo	Bgy
San Rafael	Bgy
Leyte	Prov
Abuyog	Mun
Alangilan	Bgy
Anibongan	Bgy
Buaya	Bgy
Bagacay	Bgy
Bahay	Bgy
Balinsasayao	Bgy
Balocawe	Bgy
Balocawehay	Bgy
Barayong	Bgy
Bayabas	Bgy
Bito (Pob.)	Bgy
Buenavista	Bgy
Bulak	Bgy
Bunga	Bgy
Buntay (Pob.)	Bgy
Burubud-an	Bgy
Cagbolo	Bgy
Can-uguib (Pob.)	Bgy
Can-aporong	Bgy
Canmarating	Bgy
Capilian	Bgy
Cadac-an	Bgy
Combis	Bgy
Dingle	Bgy
Guintagbucan (Pob.)	Bgy
Hampipila	Bgy
Katipunan	Bgy
Kikilo	Bgy
Laray	Bgy
Lawa-an	Bgy
Libertad	Bgy
Loyonsawang (Pob.)	Bgy
Mahagna	Bgy
Mag-atubang	Bgy
Mahayahay	Bgy
Maitum	Bgy
Malaguicay	Bgy
Matagnao	Bgy
Nalibunan (Pob.)	Bgy
Nebga	Bgy
Odiongan	Bgy
Pagsang-an	Bgy
Paguite	Bgy
Parasanon	Bgy
Picas Sur	Bgy
Pilar	Bgy
Pinamanagan	Bgy
Salvacion	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Roque	Bgy
Santa Fe (Pob.)	Bgy
Santa Lucia (Pob.)	Bgy
Santo Niño (Pob.)	Bgy
Tabigue	Bgy
Tadoc	Bgy
New Taligue	Bgy
Old Taligue	Bgy
Tib-o	Bgy
Tinalian	Bgy
Tinocolan	Bgy
Tuy-a	Bgy
Victory (Pob.)	Bgy
Alangalang	Mun
Aslum	Bgy
Astorga	Bgy
Bato	Bgy
Binongto-an	Bgy
Binotong	Bgy
Bobonon	Bgy
Borseth	Bgy
Buenavista	Bgy
Bugho	Bgy
Buri	Bgy
Cabadsan	Bgy
Calaasan	Bgy
Cambahanon	Bgy
Cambolao	Bgy
Canvertudes	Bgy
Capiz	Bgy
Cavite	Bgy
Cogon	Bgy
Dapdap	Bgy
Divisoria	Bgy
Ekiran	Bgy
Hinapolan	Bgy
Hubang	Bgy
Hupit	Bgy
Langit	Bgy
Lingayon	Bgy
Lourdes	Bgy
Lukay	Bgy
Magsaysay	Bgy
Mudboron	Bgy
P. Barrantes	Bgy
Peñalosa	Bgy
Pepita	Bgy
Salvacion Poblacion	Bgy
San Antonio	Bgy
San Diego	Bgy
San Francisco East	Bgy
San Francisco West	Bgy
San Isidro	Bgy
San Pedro	Bgy
San Vicente	Bgy
Santiago	Bgy
Santo Niño (Pob.)	Bgy
Santol	Bgy
Tabangohay	Bgy
Tombo	Bgy
Veteranos	Bgy
Blumentritt (Pob.)	Bgy
Holy Child I (Pob.)	Bgy
Holy Child II (Pob.)	Bgy
Milagrosa (Pob.)	Bgy
San Antonio Pob.	Bgy
San Roque (Pob.)	Bgy
Salvacion	Bgy
Albuera	Mun
Antipolo	Bgy
Balugo	Bgy
Benolho	Bgy
Cambalading	Bgy
Damula-an	Bgy
Doña Maria	Bgy
Mahayag	Bgy
Mahayahay	Bgy
Poblacion	Bgy
Salvacion	Bgy
San Pedro	Bgy
Seguinon	Bgy
Sherwood	Bgy
Tabgas	Bgy
Talisayan	Bgy
Tinag-an	Bgy
Babatngon	Mun
Biasong	Bgy
Gov. E. Jaro	Bgy
Malibago	Bgy
Magcasuang	Bgy
Planza	Bgy
Poblacion District I	Bgy
Poblacion District II	Bgy
Poblacion District III	Bgy
Poblacion District IV	Bgy
Rizal I	Bgy
San Ricardo	Bgy
Sangputan	Bgy
Pagsulhugon	Bgy
Taguite	Bgy
Uban	Bgy
Victory	Bgy
Villa Magsaysay	Bgy
Bacong	Bgy
Bagong Silang	Bgy
Lukay	Bgy
Rizal II	Bgy
San Agustin	Bgy
Guintigui-an	Bgy
Naga-asan	Bgy
San Isidro	Bgy
Barugo	Mun
Abango	Bgy
Amahit	Bgy
Balire	Bgy
Balud	Bgy
Bukid	Bgy
Bulod	Bgy
Busay	Bgy
Cabolo-an	Bgy
Calingcaguing	Bgy
Can-isak	Bgy
Canomantag	Bgy
Duka	Bgy
Guindaohan	Bgy
Hiagsam	Bgy
Hilaba	Bgy
Hinugayan	Bgy
Ibag	Bgy
Minuhang	Bgy
Minuswang	Bgy
Pikas	Bgy
Pitogo	Bgy
Poblacion Dist. I	Bgy
Poblacion Dist. II	Bgy
Poblacion Dist. III	Bgy
Poblacion Dist. IV	Bgy
Poblacion Dist. V	Bgy
Poblacion Dist. VI	Bgy
Pongso	Bgy
Roosevelt	Bgy
San Isidro	Bgy
Santa Rosa	Bgy
Santarin	Bgy
Tutug-an	Bgy
Cabarasan	Bgy
Cuta	Bgy
Domogdog	Bgy
San Roque	Bgy
Bato	Mun
Alegria	Bgy
Alejos	Bgy
Amagos	Bgy
Anahawan	Bgy
Bago	Bgy
Bagong Bayan District (Pob.)	Bgy
Buli	Bgy
Cebuana	Bgy
Daan Lungsod	Bgy
Dawahon	Bgy
Himamaa	Bgy
Dolho	Bgy
Domagocdoc	Bgy
Guerrero District (Pob.)	Bgy
Iniguihan District (Pob.)	Bgy
Katipunan	Bgy
Liberty	Bgy
Mabini	Bgy
Naga	Bgy
Osmeña	Bgy
Plaridel	Bgy
Kalanggaman District (Pob.)	Bgy
Ponong	Bgy
San Agustin	Bgy
Santo Niño	Bgy
Tabunok	Bgy
Tagaytay	Bgy
Tinago District (Pob.)	Bgy
Tugas	Bgy
Imelda	Bgy
Marcelo	Bgy
Rivilla	Bgy
City of Baybay	City
Altavista	Bgy
Ambacan	Bgy
Amguhan	Bgy
Ampihanon	Bgy
Balao	Bgy
Banahao	Bgy
Biasong	Bgy
Bidlinan	Bgy
Bitanhuan	Bgy
Bubon	Bgy
Buenavista	Bgy
Bunga	Bgy
Butigan	Bgy
Kabatuan	Bgy
Caridad	Bgy
Ga-as	Bgy
Gabas	Bgy
Gakat	Bgy
Guadalupe	Bgy
Gubang	Bgy
Hibunawan	Bgy
Higuloan	Bgy
Hilapnitan	Bgy
Hipusngo	Bgy
Igang	Bgy
Imelda	Bgy
Jaena	Bgy
Kabalasan	Bgy
Kabungaan	Bgy
Kagumay	Bgy
Kambonggan	Bgy
Candadam	Bgy
Kan-ipa	Bgy
Kansungka	Bgy
Kantagnos	Bgy
Kilim	Bgy
Lintaon	Bgy
Maganhan	Bgy
Mahayahay	Bgy
Mailhi	Bgy
Maitum	Bgy
Makinhas	Bgy
Mapgap	Bgy
Marcos	Bgy
Maslug	Bgy
Matam-is	Bgy
Maybog	Bgy
Maypatag	Bgy
Monterico	Bgy
Palhi	Bgy
Pangasungan	Bgy
Pansagan	Bgy
Patag	Bgy
Plaridel	Bgy
Poblacion Zone 2	Bgy
Poblacion Zone 3	Bgy
Poblacion Zone 4	Bgy
Poblacion Zone 5	Bgy
Poblacion Zone 6	Bgy
Poblacion Zone 7	Bgy
Poblacion Zone 8	Bgy
Poblacion Zone 9	Bgy
Poblacion Zone 10	Bgy
Poblacion Zone 11	Bgy
Poblacion Zone 12	Bgy
Poblacion Zone 13	Bgy
Poblacion Zone 14	Bgy
Poblacion Zone 15	Bgy
Poblacion Zone 16	Bgy
Poblacion Zone 17	Bgy
Poblacion Zone 18	Bgy
Poblacion Zone 19	Bgy
Poblacion Zone 20	Bgy
Poblacion Zone 21	Bgy
Poblacion Zone 22	Bgy
Poblacion Zone 23	Bgy
Pomponan	Bgy
Punta	Bgy
Sabang	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Santo Rosario	Bgy
Sapa	Bgy
Ciabo	Bgy
Villa Solidaridad	Bgy
Zacarito	Bgy
Poblacion Zone 1	Bgy
Cogon	Bgy
Monte Verde	Bgy
Villa Mag-aso	Bgy
Burauen	Mun
Abuyogon	Bgy
Anonang	Bgy
Arado	Bgy
Balao	Bgy
Balatson	Bgy
Balorinay	Bgy
Bobon	Bgy
Buenavista	Bgy
Buri	Bgy
Caanislagan	Bgy
Cadahunan	Bgy
Cagangon	Bgy
Cali	Bgy
Calsadahay	Bgy
Candag-on	Bgy
Cansiboy	Bgy
Catagbacan	Bgy
Poblacion District I	Bgy
Poblacion District II	Bgy
Poblacion District III	Bgy
Poblacion District IV	Bgy
Poblacion District V	Bgy
Poblacion District VI	Bgy
Poblacion District VII	Bgy
Poblacion District VIII	Bgy
Poblacion District IX	Bgy
Dumalag	Bgy
Ilihan	Bgy
Esperanza	Bgy
Gitablan	Bgy
Hapunan	Bgy
Hibonawan	Bgy
Hugpa East	Bgy
Hugpa West	Bgy
Kalao	Bgy
Kaparasanan	Bgy
Laguiwan	Bgy
Libas	Bgy
Limburan	Bgy
Logsongan	Bgy
Maabab	Bgy
Maghubas	Bgy
Mahagnao	Bgy
Malabca	Bgy
Malaguinabot	Bgy
Malaihao	Bgy
Matin-ao	Bgy
Moguing	Bgy
Paghudlan	Bgy
Paitan	Bgy
Pangdan	Bgy
Patag	Bgy
Patong	Bgy
Pawa	Bgy
Roxas	Bgy
Sambel	Bgy
San Esteban	Bgy
San Fernando	Bgy
San Jose East	Bgy
San Jose West	Bgy
San Pablo	Bgy
Tabuanon	Bgy
Tagadtaran	Bgy
Taghuyan	Bgy
Takin	Bgy
Tambis	Bgy
Toloyao	Bgy
Villa Aurora	Bgy
Villa Corazon	Bgy
Villa Patria	Bgy
Villa Rosas	Bgy
Kagbana	Bgy
Damulo-an	Bgy
Dina-ayan	Bgy
Gamay	Bgy
Kalipayan	Bgy
Tambuko	Bgy
Calubian	Mun
Abanilla	Bgy
Agas	Bgy
Anislagan	Bgy
Bunacan	Bgy
Cabalquinto	Bgy
Cabalhin	Bgy
Cabradilla	Bgy
Caneja	Bgy
Cantonghao	Bgy
Caroyocan	Bgy
Casiongan	Bgy
Cristina	Bgy
Dalumpines	Bgy
Don Luis	Bgy
Dulao	Bgy
Efe	Bgy
Enage	Bgy
Espinosa	Bgy
Garganera	Bgy
Ferdinand E. Marcos	Bgy
Garrido	Bgy
Guadalupe	Bgy
Gutosan	Bgy
Igang	Bgy
Inalad	Bgy
Jubay	Bgy
Juson	Bgy
Kawayan Bogtong	Bgy
Kawayanan	Bgy
Kokoy Romualdez	Bgy
Labtic	Bgy
Laray	Bgy
M. Veloso	Bgy
Mahait	Bgy
Malobago	Bgy
Matagok	Bgy
Nipa	Bgy
Obispo	Bgy
Pagatpat	Bgy
Pangpang	Bgy
Patag	Bgy
Pates	Bgy
Paula	Bgy
Padoga	Bgy
Petrolio	Bgy
Poblacion	Bgy
Railes	Bgy
Tabla	Bgy
Tagharigue	Bgy
Tuburan	Bgy
Villahermosa	Bgy
Villalon	Bgy
Villanueva	Bgy
Capoocan	Mun
Balucanad	Bgy
Balud	Bgy
Balugo	Bgy
Cabul-an	Bgy
Gayad	Bgy
Culasian	Bgy
Guinadiongan	Bgy
Lemon	Bgy
Libertad	Bgy
Manloy	Bgy
Nauguisan	Bgy
Pinamopoan	Bgy
Poblacion Zone I	Bgy
Poblacion Zone II	Bgy
Potot	Bgy
San Joaquin	Bgy
Santo Niño	Bgy
Talairan	Bgy
Talisay	Bgy
Tolibao	Bgy
Visares	Bgy
Carigara	Mun
Balilit	Bgy
Barayong	Bgy
Barugohay Central	Bgy
Barugohay Norte	Bgy
Barugohay Sur	Bgy
Baybay (Pob.)	Bgy
Binibihan	Bgy
Bislig	Bgy
Caghalo	Bgy
Camansi	Bgy
Canal	Bgy
Candigahub	Bgy
Canlampay	Bgy
Cogon	Bgy
Cutay	Bgy
East Visoria	Bgy
Guindapunan East	Bgy
Guindapunan West	Bgy
Hiluctogan	Bgy
Jugaban (Pob.)	Bgy
Libo	Bgy
Lower Hiraan	Bgy
Lower Sogod	Bgy
Macalpi	Bgy
Manloy	Bgy
Nauguisan	Bgy
Pangna	Bgy
Parag-um	Bgy
Parina	Bgy
Piloro	Bgy
Ponong (Pob.)	Bgy
Sagkahan	Bgy
San Mateo (Pob.)	Bgy
Santa Fe	Bgy
Sawang (Pob.)	Bgy
Tagak	Bgy
Tangnan	Bgy
Tigbao	Bgy
Tinaguban	Bgy
Upper Hiraan	Bgy
Upper Sogod	Bgy
Uyawan	Bgy
West Visoria	Bgy
Paglaum	Bgy
San Juan	Bgy
Bagong Lipunan	Bgy
Canfabi	Bgy
Rizal	Bgy
San Isidro	Bgy
Dagami	Mun
Abaca	Bgy
Abre	Bgy
Balilit	Bgy
Banayon	Bgy
Bayabas	Bgy
Bolirao	Bgy
Buenavista	Bgy
Buntay	Bgy
Caanislagan	Bgy
Cabariwan	Bgy
Cabuloran	Bgy
Cabunga-an	Bgy
Calipayan	Bgy
Calsadahay	Bgy
Caluctogan	Bgy
Calutan	Bgy
Camono-an	Bgy
Candagara	Bgy
Canlingga	Bgy
Cansamada East	Bgy
Digahongan	Bgy
Guinarona	Bgy
Hiabangan	Bgy
Hilabago	Bgy
Hinabuyan	Bgy
Hinologan	Bgy
Hitumnog	Bgy
Katipunan	Bgy
Los Martires	Bgy
Lobe-lobe	Bgy
Macaalang	Bgy
Maliwaliw	Bgy
Maragondong	Bgy
Ormocay	Bgy
Palacio	Bgy
Panda	Bgy
Patoc	Bgy
Plaridel	Bgy
Sampao West Pob.	Bgy
Lapu-lapu Pob.	Bgy
Lusad Pob.	Bgy
Sampao East Pob.	Bgy
San Antonio Pob.	Bgy
San Jose Pob.	Bgy
Sta. Mesa Pob.	Bgy
Tunga Pob.	Bgy
San Roque Pob.	Bgy
Poponton	Bgy
Rizal	Bgy
Salvacion	Bgy
San Benito	Bgy
Santo Domingo	Bgy
Sirab	Bgy
Tagkip	Bgy
Tin-ao	Bgy
Victoria	Bgy
Balugo	Bgy
Cansamada West	Bgy
Capulhan	Bgy
Lobe-lobe East	Bgy
Paraiso	Bgy
Sampaguita	Bgy
Sawahon	Bgy
Talinhugon	Bgy
Tuya	Bgy
Dulag	Mun
Alegre	Bgy
Arado	Bgy
Bulod	Bgy
Batug	Bgy
Bolongtohan	Bgy
Cabacungan	Bgy
Cabarasan	Bgy
Cabato-an	Bgy
Calipayan	Bgy
Calubian	Bgy
Camitoc	Bgy
Camote	Bgy
Dacay	Bgy
Del Carmen	Bgy
Del Pilar	Bgy
Fatima	Bgy
General Roxas	Bgy
Luan	Bgy
Magsaysay	Bgy
Maricum	Bgy
Barbo (Pob.)	Bgy
Buntay (Pob.)	Bgy
Cambula District (Pob.)	Bgy
Candao (Pob.)	Bgy
Catmonan (Pob.)	Bgy
Combis (Pob.)	Bgy
Highway (Pob.)	Bgy
Market Site (Pob.)	Bgy
San Miguel (Pob.)	Bgy
Serrano (Pob.)	Bgy
Sungi (Pob.)	Bgy
Rawis	Bgy
Rizal	Bgy
Romualdez	Bgy
Sabang Daguitan	Bgy
Salvacion	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael	Bgy
San Vicente	Bgy
Tabu	Bgy
Tigbao	Bgy
Victory	Bgy
Hilongos	Mun
Agutayan	Bgy
Atabay	Bgy
Baas	Bgy
Bagumbayan	Bgy
Baliw	Bgy
Bantigue	Bgy
Bung-aw	Bgy
Cacao	Bgy
Campina	Bgy
Catandog 1	Bgy
Catandog 2	Bgy
Concepcion	Bgy
Himo-aw	Bgy
Hitudpan	Bgy
Imelda Marcos	Bgy
Kang-iras	Bgy
Lamak	Bgy
Libertad	Bgy
Liberty	Bgy
Lunang	Bgy
Magnangoy	Bgy
Marangog	Bgy
Matapay	Bgy
Naval	Bgy
Owak	Bgy
Pa-a	Bgy
Central Barangay (Pob.)	Bgy
Eastern Barangay (Pob.)	Bgy
Western Barangay (Pob.)	Bgy
Pontod	Bgy
Proteccion	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Juan	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santa Margarita	Bgy
Santo Niño	Bgy
Tabunok	Bgy
Tagnate	Bgy
Talisay	Bgy
Tambis	Bgy
Tejero	Bgy
Tuguipa	Bgy
Utanan	Bgy
Bagong Lipunan	Bgy
Bon-ot	Bgy
Hampangan	Bgy
Kangha-as	Bgy
Manaul	Bgy
Hindang	Mun
Anahaw	Bgy
Anolon	Bgy
Baldoza	Bgy
Bontoc	Bgy
Bulacan	Bgy
Canha-ayon	Bgy
Capudlosan	Bgy
Himacugo	Bgy
Doos Del Norte	Bgy
Doos Del Sur	Bgy
Himokilan Island	Bgy
Katipunan	Bgy
Maasin	Bgy
Mabagon	Bgy
Mahilum	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
San Vicente	Bgy
Tabok	Bgy
Tagbibi	Bgy
Inopacan	Mun
Apid	Bgy
Cabulisan	Bgy
Caminto	Bgy
Can-angay	Bgy
Caulisihan	Bgy
Conalum	Bgy
De los Santos	Bgy
Esperanza	Bgy
Guadalupe	Bgy
Guinsanga-an	Bgy
Hinabay	Bgy
Jubasan	Bgy
Linao	Bgy
Macagoco	Bgy
Maljo	Bgy
Marao	Bgy
Poblacion	Bgy
Tahud	Bgy
Taotaon	Bgy
Tinago	Bgy
Isabel	Mun
Anislag	Bgy
Antipolo	Bgy
Apale	Bgy
Bantigue	Bgy
Benog	Bgy
Bilwang	Bgy
Can-andan	Bgy
Cangag	Bgy
Consolacion	Bgy
Honan	Bgy
Libertad	Bgy
Mahayag	Bgy
Marvel (Pob.)	Bgy
Matlang	Bgy
Monte Alegre	Bgy
Puting Bato	Bgy
San Francisco	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santo Niño (Pob.)	Bgy
Santo Rosario	Bgy
Tabunok	Bgy
Tolingon	Bgy
Tubod	Bgy
Jaro	Mun
Alahag	Bgy
Anibongan	Bgy
Badiang	Bgy
Batug	Bgy
Buenavista	Bgy
Bukid	Bgy
Burabod	Bgy
Buri	Bgy
Kaglawaan	Bgy
Canhandugan	Bgy
Crossing Rubas	Bgy
Daro	Bgy
Hiagsam	Bgy
Hibunawon	Bgy
Hibucawan	Bgy
Kalinawan	Bgy
Likod	Bgy
Macanip	Bgy
Macopa	Bgy
Mag-aso	Bgy
Malobago	Bgy
Olotan	Bgy
Pange	Bgy
Parasan	Bgy
Pitogo	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
District IV (Pob.)	Bgy
Sagkahan	Bgy
San Agustin	Bgy
San Pedro	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Sari-sari	Bgy
Tinambacan	Bgy
Tuba	Bgy
Uguiao	Bgy
Villagonzoilo	Bgy
Villa Paz	Bgy
Bias Zabala	Bgy
Atipolo	Bgy
Canapuan	Bgy
La Paz	Bgy
Palanog	Bgy
Javier	Mun
Abuyogay	Bgy
Andres Bonifacio	Bgy
Batug	Bgy
Binulho	Bgy
Calzada	Bgy
Cancayang	Bgy
Caranhug	Bgy
Caraye	Bgy
Casalungan	Bgy
Comatin	Bgy
Guindapunan	Bgy
Inayupan	Bgy
Laray	Bgy
Magsaysay	Bgy
Malitbogay	Bgy
Manarug	Bgy
Manlilisid	Bgy
Naliwatan	Bgy
Odiong	Bgy
Picas Norte	Bgy
Pinocawan	Bgy
Poblacion Zone 1	Bgy
Poblacion Zone 2	Bgy
Rizal	Bgy
Santa Cruz	Bgy
Talisayan	Bgy
San Sotero	Bgy
Ulhay	Bgy
Julita	Mun
Alegria	Bgy
Anibong	Bgy
Aslum	Bgy
Balante	Bgy
Bongdo	Bgy
Bonifacio	Bgy
Bugho	Bgy
Calbasag	Bgy
Caridad	Bgy
Cuya-e	Bgy
Dita	Bgy
Gitabla	Bgy
Hindang	Bgy
Inawangan	Bgy
Jurao	Bgy
Poblacion District I	Bgy
Poblacion District II	Bgy
Poblacion District III	Bgy
Poblacion District IV	Bgy
San Andres	Bgy
San Pablo	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Tagkip	Bgy
Tolosahay	Bgy
Villa Hermosa	Bgy
Kananga	Mun
Aguiting	Bgy
Cacao	Bgy
Kawayan	Bgy
Hiluctogan	Bgy
Libertad	Bgy
Libongao	Bgy
Lim-ao	Bgy
Lonoy	Bgy
Mahawan	Bgy
Masarayao	Bgy
Monte Alegre	Bgy
Monte Bello	Bgy
Naghalin	Bgy
Natubgan	Bgy
Poblacion	Bgy
Rizal	Bgy
San Ignacio	Bgy
San Isidro	Bgy
Santo Domingo	Bgy
Santo Niño	Bgy
Tagaytay	Bgy
Tongonan	Bgy
Tugbong	Bgy
La Paz	Mun
Bagacay East	Bgy
Bagacay West	Bgy
Bongtod	Bgy
Bocawon	Bgy
Buracan	Bgy
Caabangan	Bgy
Cacao	Bgy
Cagngaran	Bgy
Calabnian	Bgy
Calaghusan	Bgy
Caltayan	Bgy
Canbañez	Bgy
Cogon	Bgy
Duyog	Bgy
Gimenarat East	Bgy
Gimenarat West	Bgy
Limba	Bgy
Lubi-lubi	Bgy
Luneta	Bgy
Mag-aso	Bgy
Moroboro	Bgy
Pansud	Bgy
Pawa	Bgy
Piliway	Bgy
Poblacion District 1	Bgy
Poblacion District 2	Bgy
Poblacion District 3	Bgy
Poblacion District 4	Bgy
Quiong	Bgy
Rizal	Bgy
San Victoray	Bgy
Santa Ana	Bgy
Santa Elena	Bgy
Tabang	Bgy
Tarugan	Bgy
Leyte	Mun
Bachao	Bgy
Baco	Bgy
Bagaba-o	Bgy
Basud	Bgy
Belen	Bgy
Burabod	Bgy
Calaguise	Bgy
Consuegra	Bgy
Culasi	Bgy
Danus	Bgy
Elizabeth	Bgy
Kawayan	Bgy
Libas	Bgy
Maanda	Bgy
Macupa	Bgy
Mataloto	Bgy
Palarao	Bgy
Palid I	Bgy
Palid II	Bgy
Parasan	Bgy
Poblacion	Bgy
Salog	Bgy
Sambulawan	Bgy
Tag-abaca	Bgy
Tapol	Bgy
Tigbawan	Bgy
Tinocdugan	Bgy
Toctoc	Bgy
Ugbon	Bgy
Wague	Bgy
Macarthur	Mun
Batug	Bgy
Burabod	Bgy
Capudlosan	Bgy
Casuntingan	Bgy
Causwagan	Bgy
Danao	Bgy
General Luna	Bgy
Kiling	Bgy
Lanawan	Bgy
Liwayway	Bgy
Maya	Bgy
Oguisan	Bgy
Osmeña	Bgy
Palale 1	Bgy
Palale 2	Bgy
Poblacion District 1	Bgy
Poblacion District 2	Bgy
Poblacion District 3	Bgy
Pongon	Bgy
Quezon	Bgy
Romualdez	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Pedro	Bgy
San Vicente	Bgy
Santa Isabel	Bgy
Tinawan	Bgy
Tuyo	Bgy
Doña Josefa	Bgy
Villa Imelda	Bgy
Mahaplag	Mun
Campin	Bgy
Cuatro De Agosto	Bgy
Hilusig	Bgy
Himamara	Bgy
Hinaguimitan	Bgy
Liberacion	Bgy
Mabuhay	Bgy
Mabunga	Bgy
Magsuganao	Bgy
Mahayag	Bgy
Mahayahay	Bgy
Malinao	Bgy
Malipoon	Bgy
Palañogan	Bgy
Paril	Bgy
Pinamonoan	Bgy
Poblacion	Bgy
Polahongon	Bgy
San Isidro	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Tagaytay	Bgy
Uguis	Bgy
Union	Bgy
Upper Mahaplag	Bgy
Hiluctogan	Bgy
Maligaya	Bgy
Santo Niño	Bgy
Matag-Ob	Mun
Balagtas	Bgy
Bulak	Bgy
Cambadbad	Bgy
Candelaria	Bgy
Cansoso	Bgy
Mansahaon	Bgy
Masaba	Bgy
Naulayan	Bgy
Bonoy (Pob.)	Bgy
Mansalip (Pob.)	Bgy
Riverside (Pob.)	Bgy
Talisay (Pob.)	Bgy
San Dionisio	Bgy
San Marcelino	Bgy
San Sebastian	Bgy
San Vicente	Bgy
Santa Rosa	Bgy
Santo Rosario	Bgy
Imelda	Bgy
Malazarte	Bgy
San Guillermo	Bgy
Matalom	Mun
Agbanga	Bgy
Altavista	Bgy
Cahagnaan	Bgy
Calumpang	Bgy
Caningag	Bgy
Caridad Norte	Bgy
Caridad Sur	Bgy
Elevado	Bgy
Esperanza	Bgy
Hitoog	Bgy
Itum	Bgy
Lowan	Bgy
Monte Alegre	Bgy
San Isidro (Pob.)	Bgy
San Pedro (Pob.)	Bgy
Santo Niño (Pob.)	Bgy
President Garcia	Bgy
Punong	Bgy
San Juan	Bgy
San Salvador	Bgy
San Vicente	Bgy
Santa Fe	Bgy
Santa Paz	Bgy
Tag-os	Bgy
Templanza	Bgy
Tigbao	Bgy
Waterloo	Bgy
Zaragoza	Bgy
Bagong Lipunan	Bgy
Taglibas Imelda	Bgy
Mayorga	Mun
A. Bonifacio	Bgy
Mabini	Bgy
Burgos	Bgy
Calipayan	Bgy
Camansi	Bgy
General Antonio Luna	Bgy
Liberty	Bgy
Ormocay	Bgy
Poblacion Zone 1	Bgy
Poblacion Zone 2	Bgy
Poblacion Zone 3	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Talisay	Bgy
Union	Bgy
Wilson	Bgy
Merida	Mun
Binabaye	Bgy
Cabaliwan	Bgy
Calunangan	Bgy
Calunasan	Bgy
Cambalong	Bgy
Can-unzo	Bgy
Canbantug	Bgy
Casilda	Bgy
Lamanoc	Bgy
Libas	Bgy
Libjo	Bgy
Lundag	Bgy
Mahalit	Bgy
Mahayag	Bgy
Masumbang	Bgy
Poblacion	Bgy
Puerto Bello	Bgy
San Isidro	Bgy
San Jose	Bgy
Macario	Bgy
Tubod	Bgy
Mat-e	Bgy
Ormoc City	City
Alegria	Bgy
Bagong	Bgy
Labrador	Bgy
Bantigue	Bgy
Batuan	Bgy
Biliboy	Bgy
Borok	Bgy
Cabaon-an	Bgy
Cabulihan	Bgy
Cagbuhangin	Bgy
Can-adieng	Bgy
Can-untog	Bgy
Catmon	Bgy
Cogon Combado	Bgy
Concepcion	Bgy
Curva	Bgy
Danao	Bgy
Dayhagan	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 15 (Pob.)	Bgy
Barangay 16 (Pob.)	Bgy
Barangay 17 (Pob.)	Bgy
Barangay 18 (Pob.)	Bgy
Barangay 19 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 20 (Pob.)	Bgy
Barangay 21 (Pob.)	Bgy
Barangay 22 (Pob.)	Bgy
Barangay 23 (Pob.)	Bgy
Barangay 24 (Pob.)	Bgy
Barangay 25 (Pob.)	Bgy
Barangay 26 (Pob.)	Bgy
Barangay 27 (Pob.)	Bgy
Barangay 28 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Dolores	Bgy
Domonar	Bgy
Don Felipe Larrazabal	Bgy
Donghol	Bgy
Esperanza	Bgy
Hibunawon	Bgy
Hugpa	Bgy
Ipil	Bgy
Lao	Bgy
Libertad	Bgy
Liloan	Bgy
Linao	Bgy
Mabini	Bgy
Macabug	Bgy
Magaswi	Bgy
Mahayag	Bgy
Mahayahay	Bgy
Manlilinao	Bgy
Margen	Bgy
Mas-in	Bgy
Matica-a	Bgy
Milagro	Bgy
Monterico	Bgy
Nasunogan	Bgy
Naungan	Bgy
Nueva Vista	Bgy
Patag	Bgy
Punta	Bgy
Quezon, Jr.	Bgy
Rufina M. Tan	Bgy
Sabang Bao	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Vicente	Bgy
Santo Niño	Bgy
San Pablo	Bgy
Sumangga	Bgy
Valencia	Bgy
Barangay 29 (Pob.)	Bgy
Airport	Bgy
Cabintan	Bgy
Camp Downes	Bgy
Gaas	Bgy
Green Valley	Bgy
Licuma	Bgy
Liberty	Bgy
Leondoni	Bgy
Nueva Sociedad	Bgy
Tambulilid	Bgy
Tongonan	Bgy
Don Potenciano Larrazabal	Bgy
Kadaohan	Bgy
Guintigui-an	Bgy
Danhug	Bgy
Alta Vista	Bgy
Bagong Buhay	Bgy
Bayog	Bgy
Doña Feliza Z. Mejia	Bgy
Juaton	Bgy
Luna	Bgy
Mabato	Bgy
Palo	Mun
Anahaway	Bgy
Arado	Bgy
Baras	Bgy
Barayong	Bgy
Cabarasan Daku	Bgy
Cabarasan Guti	Bgy
Campetik	Bgy
Candahug	Bgy
Cangumbang	Bgy
Canhidoc	Bgy
Capirawan	Bgy
Castilla	Bgy
Cogon	Bgy
Gacao	Bgy
Guindapunan	Bgy
Libertad	Bgy
Naga-naga	Bgy
Pawing	Bgy
Buri (Pob.)	Bgy
Cavite East (Pob.)	Bgy
Cavite West (Pob.)	Bgy
Luntad (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
Salvacion	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Miguel (Pob.)	Bgy
Tacuranga	Bgy
Teraza	Bgy
San Fernando	Bgy
Palompon	Mun
Baguinbin	Bgy
Belen	Bgy
Buenavista	Bgy
Caduhaan	Bgy
Cambakbak	Bgy
Cambinoy	Bgy
Cangcosme	Bgy
Cangmuya	Bgy
Canipaan	Bgy
Cantandoy	Bgy
Cantuhaon	Bgy
Catigahan	Bgy
Cruz	Bgy
Duljugan	Bgy
Guiwan 1 (Pob.)	Bgy
Guiwan 2 (Pob.)	Bgy
Himarco	Bgy
Hinagbuan	Bgy
Lat-osan	Bgy
Liberty	Bgy
Mazawalo Pob.	Bgy
Lomonon	Bgy
Mabini	Bgy
Magsaysay	Bgy
Masaba	Bgy
Parilla	Bgy
Plaridel	Bgy
Central 1 (Pob.)	Bgy
Central 2 (Pob.)	Bgy
Hinablayan Pob.	Bgy
Rizal	Bgy
Sabang	Bgy
San Guillermo	Bgy
San Isidro	Bgy
San Joaquin	Bgy
San Juan	Bgy
San Miguel	Bgy
San Pablo	Bgy
San Pedro	Bgy
San Roque	Bgy
Santiago	Bgy
Taberna	Bgy
Tabunok	Bgy
Tambis	Bgy
Tinabilan	Bgy
Tinago	Bgy
Tinubdan	Bgy
Pinagdait Pob.	Bgy
Pinaghi-usa Pob.	Bgy
Bitaog Pob.	Bgy
Pastrana	Mun
Arabunog	Bgy
Aringit	Bgy
Bahay	Bgy
Cabaohan	Bgy
Calsadahay	Bgy
Cancaraja	Bgy
Caninoan	Bgy
Capilla	Bgy
Colawen	Bgy
Dumarag	Bgy
Guindapunan	Bgy
Halaba	Bgy
Jones	Bgy
Lanawan	Bgy
Lima	Bgy
Macalpiay	Bgy
Malitbogay	Bgy
Manaybanay	Bgy
Maricum	Bgy
Patong	Bgy
District 1 (Pob.)	Bgy
District 2 (Pob.)	Bgy
District 3 (Pob.)	Bgy
District 4 (Pob.)	Bgy
Sapsap	Bgy
Socsocon	Bgy
Tingib	Bgy
Yapad	Bgy
Lourdes	Bgy
San Isidro	Mun
Banat-e	Bgy
Basud	Bgy
Bawod (Pob.)	Bgy
Biasong	Bgy
Bunacan	Bgy
Cabungaan	Bgy
Capiñahan (Pob.)	Bgy
Crossing (Pob.)	Bgy
Daja-daku	Bgy
Daja-diot	Bgy
Hacienda Maria	Bgy
Linao	Bgy
Matungao	Bgy
Paril	Bgy
San Jose	Bgy
Taglawigan	Bgy
Tinago	Bgy
Busay	Bgy
San Miguel	Bgy
San Miguel	Mun
Bagacay	Bgy
Bahay	Bgy
Bairan	Bgy
Cabatianuhan	Bgy
Canap	Bgy
Capilihan	Bgy
Caraycaray	Bgy
Libtong	Bgy
Guinciaman	Bgy
Impo	Bgy
Kinalumsan	Bgy
Lukay	Bgy
Malaguinabot	Bgy
Malpag	Bgy
Mawodpawod	Bgy
Patong	Bgy
Pinarigusan	Bgy
San Andres	Bgy
Santa Cruz	Bgy
Santol	Bgy
Cayare	Bgy
Santa Fe	Mun
Baculanad	Bgy
Badiangay	Bgy
Bulod	Bgy
Catoogan	Bgy
Katipunan	Bgy
Milagrosa	Bgy
Pilit	Bgy
Pitogo	Bgy
Zone 1 (Pob.)	Bgy
Zone 2 (Pob.)	Bgy
Zone 3 (Pob.)	Bgy
San Isidro	Bgy
San Juan	Bgy
San Miguelay	Bgy
San Roque	Bgy
Tibak	Bgy
Victoria	Bgy
Cutay	Bgy
Gapas	Bgy
Zone 4 Pob.	Bgy
Tabango	Mun
Butason I	Bgy
Butason II	Bgy
Campokpok	Bgy
Catmon	Bgy
Gimarco	Bgy
Gibacungan	Bgy
Inangatan	Bgy
Manlawaan	Bgy
Omaganhan	Bgy
Poblacion	Bgy
Santa Rosa	Bgy
Tabing	Bgy
Tugas	Bgy
Tabontabon	Mun
Amandangay	Bgy
Aslum	Bgy
Balingasag	Bgy
Belisong	Bgy
Cambucao	Bgy
Capahuan	Bgy
Guingawan	Bgy
Jabong	Bgy
Mercadohay	Bgy
Mering	Bgy
Mohon	Bgy
District I Pob.	Bgy
District II Pob.	Bgy
District III Pob.	Bgy
District IV Pob.	Bgy
San Pablo	Bgy
City of Tacloban (Capital)	City
Barangay 2	Bgy
Nula-tula	Bgy
Libertad	Bgy
Barangay 5	Bgy
Barangay 6	Bgy
Barangay 6-A	Bgy
Barangay 7	Bgy
Barangay 8	Bgy
Barangay 100	Bgy
Barangay 101	Bgy
Barangay 102	Bgy
Barangay 103	Bgy
Barangay 103-A	Bgy
Barangay 104	Bgy
Barangay 105	Bgy
Barangay 106	Bgy
Barangay 107	Bgy
Barangay 108	Bgy
Barangay 12	Bgy
Barangay 13	Bgy
Barangay 14	Bgy
Barangay 15	Bgy
Barangay 16	Bgy
Barangay 17	Bgy
Barangay 18	Bgy
Barangay 19	Bgy
Barangay 20	Bgy
Barangay 21	Bgy
Barangay 21-A	Bgy
Barangay 22	Bgy
Barangay 23	Bgy
Barangay 24	Bgy
Barangay 25	Bgy
Barangay 26	Bgy
Barangay 27	Bgy
Barangay 28	Bgy
Barangay 29	Bgy
Barangay 30	Bgy
Barangay 31	Bgy
Barangay 32	Bgy
Barangay 33	Bgy
Barangay 34	Bgy
Barangay 35	Bgy
Barangay 35-A	Bgy
Barangay 36	Bgy
Barangay 37	Bgy
Barangay 37-A	Bgy
Barangay 38	Bgy
Barangay 39	Bgy
Barangay 40	Bgy
Barangay 41	Bgy
Barangay 42	Bgy
Barangay 43	Bgy
Barangay 43-A	Bgy
Barangay 43-B	Bgy
Barangay 44	Bgy
Barangay 44-A	Bgy
Barangay 45	Bgy
Barangay 46	Bgy
Barangay 47	Bgy
Barangay 48	Bgy
Barangay 49	Bgy
Barangay 50	Bgy
Barangay 50-A	Bgy
Barangay 50-B	Bgy
Barangay 51	Bgy
Barangay 52	Bgy
Barangay 53	Bgy
Barangay 54	Bgy
El Reposo	Bgy
Barangay 56	Bgy
Barangay 57	Bgy
Barangay 58	Bgy
Barangay 59	Bgy
Barangay 60	Bgy
Barangay 60-A	Bgy
Barangay 61	Bgy
Barangay 62	Bgy
Barangay 63	Bgy
Barangay 64	Bgy
Barangay 65	Bgy
Barangay 66	Bgy
Barangay 66-A	Bgy
Barangay 67	Bgy
Barangay 68	Bgy
Barangay 69	Bgy
Barangay 70	Bgy
Barangay 71	Bgy
Barangay 72	Bgy
Barangay 73	Bgy
Barangay 74	Bgy
Barangay 75	Bgy
Barangay 76	Bgy
Barangay 77	Bgy
Barangay 78	Bgy
Barangay 79	Bgy
Barangay 80	Bgy
Barangay 81	Bgy
Barangay 82	Bgy
Barangay 83	Bgy
Barangay 83-A	Bgy
Barangay 84	Bgy
Barangay 85	Bgy
Barangay 86	Bgy
Barangay 87	Bgy
Barangay 88	Bgy
Barangay 89	Bgy
Barangay 90	Bgy
Barangay 91	Bgy
Barangay 92	Bgy
Barangay 93	Bgy
Barangay 94	Bgy
Barangay 95	Bgy
Barangay 96	Bgy
Barangay 97	Bgy
Barangay 98	Bgy
Barangay 99	Bgy
Barangay 109	Bgy
Barangay 109-A	Bgy
Barangay 110	Bgy
Barangay 5-A	Bgy
Barangay 36-A	Bgy
Barangay 42-A	Bgy
Barangay 48-A	Bgy
Barangay 48-B	Bgy
Barangay 51-A	Bgy
Barangay 54-A	Bgy
Barangay 56-A	Bgy
Barangay 59-A	Bgy
Barangay 59-B	Bgy
Barangay 62-A	Bgy
Barangay 62-B	Bgy
Barangay 83-B	Bgy
Barangay 83-C	Bgy
Barangay 95-A	Bgy
Barangay 8-A	Bgy
Barangay 23-A	Bgy
Barangay 94-A	Bgy
Tanauan	Mun
Ada	Bgy
Amanluran	Bgy
Arado	Bgy
Atipolo	Bgy
Balud	Bgy
Bangon	Bgy
Bantagan	Bgy
Baras	Bgy
Binolo	Bgy
Binongto-an	Bgy
Bislig	Bgy
Cabalagnan	Bgy
Cabarasan Guti	Bgy
Cabonga-an	Bgy
Cabuynan	Bgy
Cahumayhumayan	Bgy
Calogcog	Bgy
Calsadahay	Bgy
Camire	Bgy
Canbalisara	Bgy
Catigbian	Bgy
Catmon	Bgy
Cogon	Bgy
Guindag-an	Bgy
Guingawan	Bgy
Hilagpad	Bgy
Lapay	Bgy
Limbuhan Daku	Bgy
Limbuhan Guti	Bgy
Linao	Bgy
Magay	Bgy
Maghulod	Bgy
Malaguicay	Bgy
Maribi	Bgy
Mohon	Bgy
Pago	Bgy
Pasil	Bgy
Pikas	Bgy
Buntay (Pob.)	Bgy
Canramos (Pob.)	Bgy
Licod (Pob.)	Bgy
San Miguel (Pob.)	Bgy
Salvador	Bgy
San Isidro	Bgy
San Roque (Pob.)	Bgy
San Victor	Bgy
Santa Cruz	Bgy
Santa Elena	Bgy
Santo Niño Pob.	Bgy
Solano	Bgy
Talolora	Bgy
Tugop	Bgy
Kiling	Bgy
Sacme	Bgy
Tolosa	Mun
Burak	Bgy
Canmogsay	Bgy
Cantariwis	Bgy
Capangihan	Bgy
Malbog	Bgy
Olot	Bgy
Opong	Bgy
Poblacion	Bgy
Quilao	Bgy
San Roque	Bgy
San Vicente	Bgy
Tanghas	Bgy
Telegrafo	Bgy
Doña Brigida	Bgy
Imelda	Bgy
Tunga	Mun
Astorga	Bgy
Balire	Bgy
Banawang	Bgy
San Antonio (Pob.)	Bgy
San Pedro	Bgy
San Roque	Bgy
San Vicente (Pob.)	Bgy
Santo Niño (Pob.)	Bgy
Villaba	Mun
Abijao	Bgy
Balite	Bgy
Bugabuga	Bgy
Cabungahan	Bgy
Cabunga-an	Bgy
Cagnocot	Bgy
Cahigan	Bgy
Calbugos	Bgy
Camporog	Bgy
Capinyahan	Bgy
Casili-on	Bgy
Catagbacan	Bgy
Fatima (Pob.)	Bgy
Hibulangan	Bgy
Hinabuyan	Bgy
Iligay	Bgy
Jalas	Bgy
Jordan	Bgy
Libagong	Bgy
New Balanac	Bgy
Payao	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Sambulawan	Bgy
San Francisco	Bgy
Silad	Bgy
Sulpa	Bgy
Tabunok	Bgy
Tagbubunga	Bgy
Tinghub	Bgy
Bangcal	Bgy
Canquiason	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Suba	Bgy
Northern Samar	Prov
Allen	Mun
Alejandro Village	Bgy
Bonifacio	Bgy
Cabacungan	Bgy
Calarayan	Bgy
Guin-arawayan	Bgy
Jubasan	Bgy
Kinabranan Zone I (Pob.)	Bgy
Kinaguitman	Bgy
Lagundi	Bgy
Lipata	Bgy
Londres	Bgy
Sabang Zone I (Pob.)	Bgy
Santa Rita	Bgy
Tasvilla	Bgy
Frederic	Bgy
Imelda	Bgy
Lo-oc	Bgy
Kinabranan Zone II (Pob.)	Bgy
Sabang Zone II (Pob.)	Bgy
Victoria	Bgy
Biri	Mun
Poblacion	Bgy
MacArthur	Bgy
Kauswagan	Bgy
Pio Del Pilar	Bgy
Progresso	Bgy
San Antonio	Bgy
San Pedro	Bgy
Santo Niño	Bgy
Bobon	Mun
Acerida	Bgy
Arellano	Bgy
Balat-balud	Bgy
Dancalan	Bgy
E. Duran	Bgy
Gen. Lucban (Pob.)	Bgy
Jose Abad Santos	Bgy
Jose P. Laurel	Bgy
Magsaysay	Bgy
Calantiao	Bgy
Quezon	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Juan (Pob.)	Bgy
Santa Clara (Pob.)	Bgy
Santander	Bgy
Somoroy	Bgy
Trojello	Bgy
Capul	Mun
Aguin	Bgy
Jubang	Bgy
Landusan	Bgy
Oson	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Poblacion Barangay 5	Bgy
Sagaosawan	Bgy
San Luis	Bgy
Sawang	Bgy
Catarman (Capital)	Mun
Aguinaldo	Bgy
Airport Village	Bgy
Baybay	Bgy
Bocsol	Bgy
Cabayhan	Bgy
Cag-abaca	Bgy
Cal-igang	Bgy
Cawayan	Bgy
Cervantes	Bgy
Cularima	Bgy
Daganas	Bgy
Galutan	Bgy
General Malvar	Bgy
Guba	Bgy
Gebalagnan	Bgy
Gebulwangan	Bgy
Doña Pulqueria	Bgy
Hinatad	Bgy
Imelda	Bgy
Liberty	Bgy
Libjo	Bgy
Mabini	Bgy
Macagtas	Bgy
Mckinley	Bgy
New Rizal	Bgy
Old Rizal	Bgy
Paticua	Bgy
Polangi	Bgy
Quezon	Bgy
Salvacion	Bgy
San Julian	Bgy
Somoge	Bgy
Tinowaran	Bgy
Trangue	Bgy
Washington	Bgy
UEP I	Bgy
UEP II	Bgy
UEP III	Bgy
Acacia (Pob.)	Bgy
Talisay (Pob.)	Bgy
Molave (Pob.)	Bgy
Yakal (Pob.)	Bgy
Ipil-ipil (Pob.)	Bgy
Jose Abad Santos (Pob.)	Bgy
Kasoy (Pob.)	Bgy
Lapu-lapu (Pob.)	Bgy
Santol (Pob.)	Bgy
Narra (Pob.)	Bgy
Calachuchi (Pob.)	Bgy
Sampaguita (Pob.)	Bgy
Mabolo (Pob.)	Bgy
Jose P. Rizal (Pob.)	Bgy
Bangkerohan	Bgy
Dalakit (Pob.)	Bgy
San Pascual	Bgy
Catubig	Mun
Anongo	Bgy
D. Mercader	Bgy
Bonifacio	Bgy
Boring	Bgy
Cagbugna	Bgy
Cagmanaba	Bgy
Cagogobngan	Bgy
Calingnan	Bgy
Canuctan	Bgy
Guibwangan	Bgy
Hinagonoyan	Bgy
Hiparayan	Bgy
Hitapi-an	Bgy
Inoburan	Bgy
Irawahan	Bgy
Libon	Bgy
Claro M. Recto	Bgy
Lenoyahan	Bgy
Magongon	Bgy
Magtuad	Bgy
Manering	Bgy
Nabulo	Bgy
Nagoocan	Bgy
Nahulid	Bgy
Opong	Bgy
Osang	Bgy
Osmeña	Bgy
P. Rebadulla	Bgy
Roxas	Bgy
Sagudsuron	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Jose	Bgy
San Vicente	Bgy
Santa Fe	Bgy
Sulitan	Bgy
Tangbo	Bgy
Tungodnon	Bgy
Vienna Maria	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Gamay	Mun
Anito	Bgy
Bangon	Bgy
Bato	Bgy
Bonifacio	Bgy
Cabarasan	Bgy
Cadac-an	Bgy
Cade-an	Bgy
Cagamutan del Norte	Bgy
Dao	Bgy
G. M. Osias	Bgy
Guibuangan	Bgy
Henogawe	Bgy
Lonoy	Bgy
Luneta	Bgy
Malidong	Bgy
Gamay Central (Pob.)	Bgy
Gamay Occidental I (Pob.)	Bgy
Gamay Oriental I (Pob.)	Bgy
Rizal	Bgy
San Antonio	Bgy
Baybay District (Pob.)	Bgy
Burabod (Pob.)	Bgy
Cagamutan del Sur	Bgy
Libertad (Pob.)	Bgy
Occidental II (Pob.)	Bgy
Oriental II (Pob.)	Bgy
Laoang	Mun
Abaton	Bgy
Aguadahan	Bgy
Aroganga	Bgy
Atipolo	Bgy
Bawang	Bgy
Baybay (Pob.)	Bgy
Binatiklan	Bgy
Bobolosan	Bgy
Bongliw	Bgy
Burabud	Bgy
Cabadiangan	Bgy
Cabagngan	Bgy
Cabago-an	Bgy
Cabulaloan	Bgy
Cagaasan	Bgy
Cagdara-o	Bgy
Cahayagan	Bgy
Calintaan Pob.	Bgy
Calomotan	Bgy
Candawid	Bgy
Cangcahipos	Bgy
Canyomanao	Bgy
Catigbian	Bgy
E. J. Dulay	Bgy
G. B. Tan	Bgy
Gibatangan	Bgy
Guilaoangi (Pob.)	Bgy
Inamlan	Bgy
La Perla	Bgy
Langob	Bgy
Lawaan	Bgy
Little Venice (Pob.)	Bgy
Magsaysay	Bgy
Marubay	Bgy
Mualbual	Bgy
Napotiocan	Bgy
Oleras	Bgy
Onay	Bgy
Palmera	Bgy
Pangdan	Bgy
Rawis	Bgy
Rombang	Bgy
San Antonio	Bgy
San Miguel Heights (Pob.)	Bgy
Sangcol	Bgy
Sibunot	Bgy
Simora	Bgy
Suba	Bgy
Tan-awan	Bgy
Tarusan	Bgy
Tinoblan	Bgy
Tumaguingting (Pob.)	Bgy
Vigo	Bgy
Yabyaban	Bgy
Yapas	Bgy
Talisay	Bgy
Lapinig	Mun
Alang-alang	Bgy
Bagacay	Bgy
Cahagwayan	Bgy
Can Maria	Bgy
Can Omanio	Bgy
Imelda	Bgy
Lapinig Del Sur (Pob.)	Bgy
Lapinig Del Norte (Pob.)	Bgy
Lo-ok	Bgy
Mabini	Bgy
May-igot	Bgy
Palanas	Bgy
Pio Del Pilar	Bgy
Potong	Bgy
Potong Del Sur	Bgy
Las Navas	Mun
Balugo	Bgy
Bugay	Bgy
Bugtosan	Bgy
Bukid	Bgy
Bulao	Bgy
Caputoan	Bgy
Catoto-ogan	Bgy
Cuenco	Bgy
Dapdap	Bgy
Del Pilar	Bgy
Dolores	Bgy
Epaw	Bgy
Geguinta	Bgy
Geracdo	Bgy
Guyo	Bgy
H. Jolejole District (Pob.)	Bgy
Hangi	Bgy
Imelda	Bgy
L. Empon	Bgy
Lakandula	Bgy
Lumala-og	Bgy
Lourdes	Bgy
Mabini	Bgy
Macarthur	Bgy
Magsaysay	Bgy
Matelarag	Bgy
Osmeña	Bgy
Paco	Bgy
Palanas	Bgy
Perez	Bgy
Poponton	Bgy
Quezon	Bgy
Quirino	Bgy
Quirino District (Pob.)	Bgy
Rebong	Bgy
Rizal	Bgy
Roxas	Bgy
Rufino	Bgy
Sag-od	Bgy
San Andres	Bgy
San Antonio	Bgy
San Fernando	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jorge	Bgy
San Jose	Bgy
San Miguel	Bgy
Santo Tomas	Bgy
Tagab-iran	Bgy
Tagan-ayan	Bgy
Taylor	Bgy
Victory	Bgy
H. Jolejole	Bgy
Lavezares	Mun
Balicuatro	Bgy
Bani	Bgy
Barobaybay	Bgy
Caburihan (Pob.)	Bgy
Caragas (Pob.)	Bgy
Cataogan (Pob.)	Bgy
Chansvilla	Bgy
Datag	Bgy
Enriqueta	Bgy
Libas	Bgy
Libertad	Bgy
Macarthur	Bgy
Magsaysay	Bgy
Maravilla	Bgy
Ocad (Pob.)	Bgy
Salvacion	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Miguel	Bgy
To-og	Bgy
Urdaneta	Bgy
Villa	Bgy
Villahermosa	Bgy
Sabong-Tabok	Bgy
Mapanas	Mun
Burgos	Bgy
Jubasan	Bgy
Magsaysay	Bgy
Magtaon	Bgy
Del Norte (Pob.)	Bgy
Del Sur (Pob.)	Bgy
Quezon	Bgy
San Jose	Bgy
Siljagon	Bgy
Naparasan	Bgy
E. Laodenio	Bgy
Manaybanay	Bgy
Santa Potenciana (Pob.)	Bgy
Mondragon	Mun
Bagasbas	Bgy
Bugko	Bgy
Cablangan	Bgy
Cagmanaba	Bgy
Cahicsan	Bgy
Chitongco (Pob.)	Bgy
De Maria	Bgy
Doña Lucia	Bgy
Eco (Pob.)	Bgy
Flormina	Bgy
Hinabangan	Bgy
Imelda	Bgy
La Trinidad	Bgy
Makiwalo	Bgy
Mirador	Bgy
Nenita	Bgy
Roxas	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
Santa Catalina	Bgy
Talolora	Bgy
Palapag	Mun
Asum (Pob.)	Bgy
Bagacay	Bgy
Bangon	Bgy
Binay	Bgy
Cabariwan	Bgy
Cabatuan	Bgy
Campedico	Bgy
Capacujan	Bgy
Jangtud	Bgy
Laniwan (Pob.)	Bgy
Mabaras	Bgy
Magsaysay	Bgy
Manajao	Bgy
Mapno	Bgy
Maragano	Bgy
Matambag	Bgy
Monbon	Bgy
Nagbobtac	Bgy
Napo	Bgy
Natawo	Bgy
Nipa	Bgy
Osmeña	Bgy
Pangpang	Bgy
Paysud	Bgy
Sangay	Bgy
Simora	Bgy
Sinalaran	Bgy
Sumoroy	Bgy
Talolora	Bgy
Tambangan (Pob.)	Bgy
Tinampo (Pob.)	Bgy
Benigno S. Aquino, Jr.	Bgy
Pambujan	Mun
Cababto-an	Bgy
Cabari-an	Bgy
Cagbigajo	Bgy
Canjumadal	Bgy
Doña Anecita	Bgy
Camparanga	Bgy
Ge-adgawan	Bgy
Ginulgan	Bgy
Geparayan	Bgy
Igot	Bgy
Ynaguingayan	Bgy
Inanahawan	Bgy
Manahao	Bgy
Paninirongan	Bgy
Poblacion District 1	Bgy
Poblacion District 2	Bgy
Poblacion District 3	Bgy
Poblacion District 4	Bgy
Poblacion District 5	Bgy
Poblacion District 6	Bgy
Poblacion District 7	Bgy
Poblacion District 8	Bgy
San Ramon	Bgy
Senonogan	Bgy
Sixto T. Balanguit, Sr.	Bgy
Tula	Bgy
Rosario	Mun
Aguada	Bgy
Buenavista	Bgy
Jamoog	Bgy
Ligaya	Bgy
Poblacion	Bgy
Salhag	Bgy
San Lorenzo	Bgy
Bantolinao	Bgy
Commonwealth	Bgy
Guindaulan	Bgy
Kailingan	Bgy
San Antonio	Mun
Burabod	Bgy
Dalupirit	Bgy
Manraya	Bgy
Pilar	Bgy
Ward I (Pob.)	Bgy
Rizal	Bgy
San Nicolas	Bgy
Vinisitahan	Bgy
Ward II (Pob.)	Bgy
Ward III (Pob.)	Bgy
San Isidro	Mun
Alegria	Bgy
Balite	Bgy
Buenavista	Bgy
Caglanipao	Bgy
Happy Valley	Bgy
Mabuhay	Bgy
Palanit	Bgy
Poblacion Norte	Bgy
Poblacion Sur	Bgy
Salvacion	Bgy
San Juan	Bgy
San Roque	Bgy
Seven Hills	Bgy
Veriato	Bgy
San Jose	Mun
Aguadahan	Bgy
Bagong Sabang	Bgy
Balite	Bgy
Bonglas	Bgy
Da-o	Bgy
Gengarog	Bgy
Geratag	Bgy
Layuhan	Bgy
Mandugang	Bgy
P. Tingzon	Bgy
San Lorenzo	Bgy
Tubigdanao	Bgy
Barangay North (Pob.)	Bgy
Barangay South (Pob.)	Bgy
Barangay East (Pob.)	Bgy
Barangay West (Pob.)	Bgy
San Roque	Mun
Balnasan	Bgy
Balud	Bgy
Bantayan	Bgy
Coroconog	Bgy
Dale	Bgy
Ginagdanan	Bgy
Lao-angan	Bgy
Lawaan	Bgy
Malobago	Bgy
Pagsang-an	Bgy
Zone 1 (Pob.)	Bgy
Zone 2 (Pob.)	Bgy
Zone 3 (Pob.)	Bgy
Zone 4 (Pob.)	Bgy
Zone 5 (Pob.)	Bgy
Zone 6 (Pob.)	Bgy
San Vicente	Mun
Maragat	Bgy
Mongol Bongol Pob.	Bgy
Sangputan	Bgy
Sila	Bgy
Tarnate	Bgy
Destacado Pob.	Bgy
Punta Pob.	Bgy
Silvino Lobos	Mun
Balud	Bgy
Cababayogan	Bgy
Cabunga-an	Bgy
Cagda-o	Bgy
Caghilot	Bgy
Camanggaran	Bgy
Camaya-an	Bgy
Deit De Suba	Bgy
Deit De Turag	Bgy
Gebonawan	Bgy
Gebolwangan	Bgy
Gecbo-an	Bgy
Giguimitan	Bgy
Genagasan	Bgy
Geparayan De Turag	Bgy
Gusaran	Bgy
Imelda	Bgy
Montalban	Bgy
Suba (Pob.)	Bgy
San Isidro	Bgy
Senonogan de Tubang	Bgy
Tobgon	Bgy
Victory	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
San Antonio	Bgy
Victoria	Mun
Acedillo	Bgy
Buenasuerte	Bgy
Buenos Aires	Bgy
Colab-og	Bgy
Erenas	Bgy
Libertad	Bgy
Luisita	Bgy
Lungib	Bgy
Maxvilla	Bgy
Pasabuena	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
San Lazaro	Bgy
San Miguel	Bgy
San Roman	Bgy
Lope De Vega	Mun
Bayho	Bgy
Bonifacio	Bgy
Cagamesarag	Bgy
Cag-aguingay	Bgy
Curry	Bgy
Gebonawan	Bgy
Gen. Luna	Bgy
Getigo	Bgy
Henaronagan	Bgy
Lope De Vega (Pob.)	Bgy
Lower Caynaga	Bgy
Maghipid	Bgy
Magsaysay	Bgy
Osmeña	Bgy
Paguite	Bgy
Roxas	Bgy
Sampaguita	Bgy
San Francisco	Bgy
San Jose	Bgy
San Miguel	Bgy
Somoroy	Bgy
Upper Caynaga	Bgy
Samar	Prov
Almagro	Mun
Bacjao	Bgy
Biasong I	Bgy
Costa Rica	Bgy
Guin-ansan	Bgy
Kerikite	Bgy
Lunang I	Bgy
Lunang II	Bgy
Malobago	Bgy
Marasbaras	Bgy
Panjobjoban I	Bgy
Poblacion	Bgy
Talahid	Bgy
Tonga-tonga	Bgy
Imelda	Bgy
Biasong II	Bgy
Costa Rica II	Bgy
Mabuhay	Bgy
Magsaysay	Bgy
Panjobjoban II	Bgy
Roño	Bgy
San Isidro	Bgy
San Jose	Bgy
Veloso	Bgy
Basey	Mun
Amandayehan	Bgy
Anglit	Bgy
Bacubac	Bgy
Baloog	Bgy
Basiao	Bgy
Buenavista	Bgy
Burgos	Bgy
Cambayan	Bgy
Can-abay	Bgy
Cancaiyas	Bgy
Canmanila	Bgy
Catadman	Bgy
Cogon	Bgy
Dolongan	Bgy
Guintigui-an	Bgy
Guirang	Bgy
Balante	Bgy
Iba	Bgy
Inuntan	Bgy
Loog	Bgy
Mabini	Bgy
Magallanes	Bgy
Manlilinab	Bgy
Del Pilar	Bgy
May-it	Bgy
Mongabong	Bgy
New San Agustin	Bgy
Nouvelas Occidental	Bgy
San Fernando	Bgy
Old San Agustin	Bgy
Panugmonon	Bgy
Pelit	Bgy
Baybay (Pob.)	Bgy
Buscada (Pob.)	Bgy
Lawa-an (Pob.)	Bgy
Loyo (Pob.)	Bgy
Mercado (Pob.)	Bgy
Palaypay (Pob.)	Bgy
Sulod (Pob.)	Bgy
Roxas	Bgy
Salvacion	Bgy
San Antonio	Bgy
Sawa	Bgy
Serum	Bgy
Sugca	Bgy
Sugponon	Bgy
Tinaogan	Bgy
Tingib	Bgy
Villa Aurora	Bgy
Binongtu-an	Bgy
Bulao	Bgy
City of Calbayog	City
Jose A. Roño	Bgy
Acedillo	Bgy
Alibaba	Bgy
Amampacang	Bgy
Anislag	Bgy
Ba-ay	Bgy
Bagacay	Bgy
Baja	Bgy
Balud (Pob.)	Bgy
Bante	Bgy
Bantian	Bgy
Basud	Bgy
Bayo	Bgy
Begaho	Bgy
Binaliw	Bgy
Bugtong	Bgy
Buenavista	Bgy
Cabacungan	Bgy
Cabatuan	Bgy
Cabicahan	Bgy
Cabugawan	Bgy
Cacaransan	Bgy
Cag-olango	Bgy
Cag-anahaw	Bgy
Cagbanayacao	Bgy
Cagbayang	Bgy
Cagbilwang	Bgy
Cagboborac	Bgy
Caglanipao Sur	Bgy
Cagmanipis Norte	Bgy
Cag-anibong	Bgy
Cagnipa	Bgy
Cagsalaosao	Bgy
Cahumpan	Bgy
Calocnayan	Bgy
Cangomaod	Bgy
Canhumadac	Bgy
Capacuhan	Bgy
Capoocan	Bgy
Carayman	Bgy
Catabunan	Bgy
Caybago	Bgy
Central (Pob.)	Bgy
Cogon	Bgy
Dagum	Bgy
Dawo	Bgy
De Victoria	Bgy
Dinabongan	Bgy
Dinagan	Bgy
Dinawacan	Bgy
Esperanza	Bgy
Gadgaran	Bgy
Gasdo	Bgy
Helino	Bgy
Geragaan	Bgy
Guin-on	Bgy
Guinbaoyan Norte	Bgy
Guinbaoyan Sur	Bgy
Hamorawon	Bgy
Hibabngan	Bgy
Hibatang	Bgy
Higasaan	Bgy
Himalandrog	Bgy
Jimautan	Bgy
Hugon Rosales	Bgy
Jacinto	Bgy
Aguit-itan (Pob.)	Bgy
Kilikili	Bgy
La Paz	Bgy
Langoyon	Bgy
Lapaan	Bgy
Libertad	Bgy
Limarayon	Bgy
Looc	Bgy
Longsob	Bgy
Lonoy	Bgy
Mabini I	Bgy
Mabini II	Bgy
Macatingog	Bgy
Mag-ubay	Bgy
Manguino-o	Bgy
Malaga	Bgy
Malajog	Bgy
Malayog	Bgy
Malopalo	Bgy
Marcatubig	Bgy
Mancol	Bgy
Mantaong	Bgy
Matobato	Bgy
Mawacat	Bgy
Maybog	Bgy
Maysalong	Bgy
Migara	Bgy
Nabang	Bgy
Naga	Bgy
Nag-uma	Bgy
Navarro	Bgy
Nijaga	Bgy
Oboob	Bgy
Obrero	Bgy
Olera	Bgy
Oquendo (Pob.)	Bgy
Osmeña	Bgy
Palanas	Bgy
Palanogan	Bgy
Panlayahan	Bgy
Panonongon	Bgy
Panoypoy	Bgy
Patong	Bgy
Peña	Bgy
Pilar	Bgy
Pinamorotan	Bgy
Quezon	Bgy
Rawis	Bgy
Rizal I	Bgy
Rizal II	Bgy
Roxas I	Bgy
Roxas II	Bgy
Saljag	Bgy
Salvacion	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Policarpo	Bgy
San Rufino	Bgy
Saputan	Bgy
Sigo	Bgy
Sinantan	Bgy
Sinidman Occidental	Bgy
Sinidman Oriental	Bgy
Tabawan	Bgy
Talahiban	Bgy
Tapae	Bgy
Tarabucan	Bgy
Tigbe	Bgy
Tinaplacan	Bgy
Tomaliguez	Bgy
Trinidad	Bgy
Victory	Bgy
Villahermosa	Bgy
Awang East (Pob.)	Bgy
Awang West (Pob.)	Bgy
Bagong Lipunan	Bgy
Bontay	Bgy
Kalilihan	Bgy
Carmen	Bgy
Danao I	Bgy
Danao II	Bgy
Gabay	Bgy
Pagbalican	Bgy
Payahan	Bgy
Tanval	Bgy
Tinambacan Norte	Bgy
Tinambacan Sur	Bgy
Cagmanipis Sur	Bgy
Manuel Barral, Sr.	Bgy
Calbiga	Mun
Antol	Bgy
Bacyaran	Bgy
Beri	Bgy
Barobaybay	Bgy
Binanggaran	Bgy
Borong	Bgy
Bulao	Bgy
Buluan	Bgy
Caamlongan	Bgy
Calayaan	Bgy
Calingonan	Bgy
Canbagtic	Bgy
Canticum	Bgy
Daligan	Bgy
Guinbanga	Bgy
Hubasan	Bgy
Literon	Bgy
Lubang	Bgy
Mahangcao	Bgy
Macaalan	Bgy
Malabal	Bgy
Minata	Bgy
Otoc	Bgy
Panayuran	Bgy
Pasigay	Bgy
Patong	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Polangi	Bgy
Rawis	Bgy
San Ignacio	Bgy
San Mauricio	Bgy
Sinalangtan	Bgy
Timbangan	Bgy
Tinago	Bgy
Hindang	Bgy
City of Catbalogan (Capital)	City
Albalate	Bgy
Bagongon	Bgy
Bangon	Bgy
Basiao	Bgy
Buluan	Bgy
Bunuanan	Bgy
Cabugawan	Bgy
Cagudalo	Bgy
Cagusipan	Bgy
Cagutian	Bgy
Cagutsan	Bgy
Canhawan Gote	Bgy
Canlapwas (Pob.)	Bgy
Cawayan	Bgy
Cinco	Bgy
Darahuway Daco	Bgy
Darahuway Gote	Bgy
Estaka	Bgy
Guinsorongan	Bgy
Iguid	Bgy
Lagundi	Bgy
Libas	Bgy
Lobo	Bgy
Manguehay	Bgy
Maulong	Bgy
Mercedes	Bgy
Mombon	Bgy
New Mahayag	Bgy
Old Mahayag	Bgy
Palanyogon	Bgy
Pangdan	Bgy
Payao	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Poblacion 6	Bgy
Poblacion 7	Bgy
Poblacion 8	Bgy
Poblacion 9	Bgy
Poblacion 10	Bgy
Poblacion 11	Bgy
Poblacion 12	Bgy
Poblacion 13	Bgy
Muñoz	Bgy
Pupua	Bgy
Guindaponan	Bgy
Rama	Bgy
San Andres	Bgy
San Pablo	Bgy
San Roque	Bgy
San Vicente	Bgy
Silanga	Bgy
Totoringon	Bgy
Ibol	Bgy
Socorro	Bgy
Daram	Mun
Arawane	Bgy
Astorga	Bgy
Bachao	Bgy
Baclayan	Bgy
Bagacay	Bgy
Bayog	Bgy
Birawan	Bgy
Betaug	Bgy
Bono-anon	Bgy
Buenavista	Bgy
Burgos	Bgy
Cabac	Bgy
Cabil-isan	Bgy
Cabiton-an	Bgy
Cabugao	Bgy
Calawan-an	Bgy
Cambuhay	Bgy
Candugue	Bgy
Canloloy	Bgy
Campelipa	Bgy
Cansaganay	Bgy
Poblacion 3	Bgy
Casab-ahan	Bgy
Guindapunan	Bgy
Guintampilan	Bgy
Iquiran	Bgy
Jacopon	Bgy
Losa	Bgy
Mabini	Bgy
Macalpe	Bgy
Mandoyucan	Bgy
Mongolbongol	Bgy
Marupangdan	Bgy
Mayabay	Bgy
Nipa	Bgy
Parasan	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Pondang	Bgy
Poso	Bgy
Real	Bgy
Rizal	Bgy
San Antonio	Bgy
San Jose	Bgy
San Miguel	Bgy
San Roque	Bgy
Saugan	Bgy
So-ong	Bgy
Sua	Bgy
Talisay	Bgy
Tugas	Bgy
Ubo	Bgy
Valles-Bello	Bgy
Cagboboto	Bgy
Lucob-lucob	Bgy
San Vicente	Bgy
Sugod	Bgy
Yangta	Bgy
Gandara	Mun
Balocawe	Bgy
Beslig	Bgy
Burabod I (Pob.)	Bgy
Burabod II (Pob.)	Bgy
Buao	Bgy
Bunyagan	Bgy
Calirocan	Bgy
Canhumawid	Bgy
Caparangasan	Bgy
Caranas	Bgy
Carmona	Bgy
Casab-ahan	Bgy
Casandig	Bgy
Caugbusan	Bgy
Concepcion	Bgy
Dumalo-ong (Pob.)	Bgy
Elcano	Bgy
Gerali	Bgy
Giaboc	Bgy
Hampton	Bgy
Hinayagan	Bgy
Hinugacan	Bgy
Jasminez	Bgy
Lungib	Bgy
Mabuhay	Bgy
Macugo	Bgy
Minda (Pob.)	Bgy
Nacube	Bgy
Nalihugan	Bgy
Napalisan	Bgy
Natimonan	Bgy
Ngoso	Bgy
Palambrag	Bgy
Palanas	Bgy
Piñaplata	Bgy
Pizarro	Bgy
Pologon	Bgy
Purog	Bgy
Rawis	Bgy
Rizal	Bgy
Samoyao	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Enrique	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Pelayo	Bgy
San Ramon	Bgy
Santa Elena	Bgy
Santo Niño	Bgy
Senibaran	Bgy
Tagnao	Bgy
Tambongan	Bgy
Tawiran	Bgy
Tigbawon	Bgy
Marcos	Bgy
Bangahon	Bgy
Adela Heights (Pob.)	Bgy
Arong	Bgy
Catorse De Agosto	Bgy
Diaz	Bgy
Gereganan	Bgy
Hetebac	Bgy
Himamaloto	Bgy
Hiparayan	Bgy
Malayog	Bgy
Sidmon	Bgy
Hinabangan	Mun
Bagacay	Bgy
Binobucalan	Bgy
Bucalan	Bgy
Cabalagnan	Bgy
Canano	Bgy
Consolabao	Bgy
Concord	Bgy
Dalosdoson	Bgy
Lim-ao	Bgy
Osmeña	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Rawis	Bgy
San Rafael	Bgy
Tabay	Bgy
Yabon	Bgy
Cabang	Bgy
Malihao	Bgy
San Jose	Bgy
Fatima	Bgy
Mugdo	Bgy
Jiabong	Mun
Bawang	Bgy
Bugho	Bgy
Camarobo-an	Bgy
Candayao	Bgy
Cantongtong	Bgy
Casapa	Bgy
Catalina	Bgy
Cristina	Bgy
Dogongan	Bgy
Garcia	Bgy
Hinaga	Bgy
Jia-an	Bgy
Jidanao	Bgy
Lulugayan	Bgy
Macabetas	Bgy
Malino	Bgy
Malobago	Bgy
Mercedes	Bgy
Nagbac	Bgy
Parina	Bgy
Barangay No. 1 (Pob.)	Bgy
Barangay No. 2 (Pob.)	Bgy
Barangay No. 3 (Pob.)	Bgy
Barangay No. 4 (Pob.)	Bgy
Barangay No. 5 (Pob.)	Bgy
Barangay No. 6 (Pob.)	Bgy
Barangay No. 7 (Pob.)	Bgy
Barangay No. 8 (Pob.)	Bgy
Salvacion	Bgy
San Andres	Bgy
San Fernando	Bgy
San Miguel	Bgy
Tagbayaon	Bgy
Victory	Bgy
Marabut	Mun
Amambucale	Bgy
Caluwayan	Bgy
Canyoyo	Bgy
Ferreras	Bgy
Legaspi	Bgy
Logero	Bgy
Osmeña	Bgy
Pinalanga	Bgy
Pinamitinan (Pob.)	Bgy
Catato Pob.	Bgy
San Roque	Bgy
Santo Niño Pob.	Bgy
Tagalag	Bgy
Tinabanan	Bgy
Amantillo	Bgy
Binukyahan	Bgy
Lipata	Bgy
Mabuhay	Bgy
Malobago	Bgy
Odoc	Bgy
Panan-awan	Bgy
Roño	Bgy
Santa Rita	Bgy
Veloso	Bgy
Matuguinao	Mun
Angyap	Bgy
Barruz	Bgy
Camonoan	Bgy
Carolina	Bgy
Deit	Bgy
Del Rosario	Bgy
Libertad	Bgy
Ligaya	Bgy
Mabuligon Pob.	Bgy
Maduroto Pob.	Bgy
Mahanud	Bgy
Mahayag	Bgy
Nagpapacao	Bgy
Rizal	Bgy
Salvacion	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Bag-otan	Bgy
Inubod	Bgy
San Roque	Bgy
Motiong	Mun
Poblacion I	Bgy
Poblacion I-A	Bgy
Angyap	Bgy
Barayong	Bgy
Bayog	Bgy
Beri	Bgy
Bonga	Bgy
Calantawan	Bgy
Calapi	Bgy
Caluyahan	Bgy
Canvais	Bgy
Canatuan	Bgy
Candomacol	Bgy
Capaysagan	Bgy
Caranas	Bgy
Caulayanan	Bgy
Hinica-an	Bgy
Inalad	Bgy
Linonoban	Bgy
Malobago	Bgy
Malonoy	Bgy
Mararangsi	Bgy
Maypange	Bgy
New Minarog	Bgy
Oyandic	Bgy
Pamamasan	Bgy
San Andres	Bgy
Santo Niño	Bgy
Sarao	Bgy
Pusongan	Bgy
Pinabacdao	Mun
Bangon	Bgy
Barangay I (Pob.)	Bgy
Barangay II (Pob.)	Bgy
Botoc	Bgy
Bugho	Bgy
Calampong	Bgy
Canlobo	Bgy
Catigawan	Bgy
Dolores	Bgy
Lale	Bgy
Lawaan	Bgy
Laygayon	Bgy
Layo	Bgy
Loctob	Bgy
Madalunot	Bgy
Magdawat	Bgy
Mambog	Bgy
Manaing	Bgy
Obayan	Bgy
Pahug	Bgy
Parasanon	Bgy
Pelaon	Bgy
San Isidro	Bgy
Nabong	Bgy
San Jose De Buan	Mun
Aguingayan	Bgy
Babaclayon	Bgy
Can-aponte	Bgy
Cataydongan	Bgy
Gusa	Bgy
Hagbay	Bgy
Hiduroma	Bgy
Hilumot	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
San Nicolas	Bgy
Hibaca-an	Bgy
San Sebastian	Mun
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
Poblacion Barangay 3	Bgy
Poblacion Barangay 4	Bgy
Balogo	Bgy
Bontod	Bgy
Camanhagay	Bgy
Campiyak	Bgy
Dolores	Bgy
Hita-asan I	Bgy
Inobongan	Bgy
Cabaywa	Bgy
Canduyucan	Bgy
Hita-asan II	Bgy
Santa Margarita	Mun
Agrupacion	Bgy
Arapison	Bgy
Avelino	Bgy
Bahay	Bgy
Balud	Bgy
Bana-ao	Bgy
Burabod	Bgy
Cautod (Pob.)	Bgy
Camperito	Bgy
Campeig	Bgy
Can-ipulan	Bgy
Canmoros	Bgy
Cinco	Bgy
Curry	Bgy
Gajo	Bgy
Hindang	Bgy
Ilo	Bgy
Imelda	Bgy
Inoraguiao	Bgy
Jolacao	Bgy
Lambao	Bgy
Mabuhay	Bgy
Mahayag	Bgy
Monbon (Pob.)	Bgy
Nabulo	Bgy
Napuro	Bgy
Palale	Bgy
Panabatan	Bgy
Panaruan	Bgy
Roxas	Bgy
Salvacion	Bgy
Solsogon	Bgy
Sundara	Bgy
Cagsumji	Bgy
Matayonas	Bgy
Napuro II	Bgy
Santa Rita	Mun
Alegria	Bgy
Anibongan	Bgy
Aslum	Bgy
Bagolibas	Bgy
Binanalan	Bgy
Cabacungan	Bgy
Cabunga-an	Bgy
Camayse	Bgy
Cansadong	Bgy
Caticugan	Bgy
Dampigan	Bgy
Guinbalot-an	Bgy
Hinangudtan	Bgy
Igang-igang	Bgy
La Paz	Bgy
Lupig	Bgy
Magsaysay	Bgy
Maligaya	Bgy
New Manunca	Bgy
Old Manunca	Bgy
Pagsulhogon	Bgy
Salvacion	Bgy
San Eduardo	Bgy
San Isidro	Bgy
San Juan	Bgy
San Pascual	Bgy
San Pedro	Bgy
San Roque	Bgy
Santa Elena	Bgy
Tagacay	Bgy
Tominamos	Bgy
Tulay	Bgy
Union	Bgy
Bokinggan Pob.	Bgy
Bougainvilla Pob.	Bgy
Gumamela Pob.	Bgy
Rosal Pob. 	Bgy
Santan Pob.	Bgy
Santo Niño	Mun
Balatguti	Bgy
Baras	Bgy
Basud (Pob.)	Bgy
Buenavista	Bgy
Cabunga-an	Bgy
Corocawayan	Bgy
Ilijan	Bgy
Ilo (Pob.)	Bgy
Lobelobe	Bgy
Pinanangnan	Bgy
Sevilla	Bgy
Takut	Bgy
Villahermosa	Bgy
Talalora	Mun
Bo. Independencia	Bgy
Malaguining	Bgy
Mallorga	Bgy
Navatas Daku	Bgy
Navatas Guti	Bgy
Placer	Bgy
Poblacion Barangay 1	Bgy
Poblacion Barangay 2	Bgy
San Juan	Bgy
Tatabunan	Bgy
Victory	Bgy
Tarangnan	Mun
Alcazar	Bgy
Awang	Bgy
Bahay	Bgy
Balonga-as	Bgy
Balugo	Bgy
Bangon Gote	Bgy
Baras	Bgy
Binalayan	Bgy
Bisitahan	Bgy
Bonga	Bgy
Cabunga-an	Bgy
Cagtutulo	Bgy
Cambatutay Nuevo	Bgy
Cambatutay Viejo	Bgy
Canunghan	Bgy
Catan-agan	Bgy
Dapdap	Bgy
Gallego	Bgy
Imelda Pob.	Bgy
Lucerdoni	Bgy
Lahong	Bgy
Libucan Dacu	Bgy
Libucan Gote	Bgy
Majacob	Bgy
Mancares	Bgy
Marabut	Bgy
Oeste - A	Bgy
Oeste - B	Bgy
Pajo	Bgy
Palencia	Bgy
Poblacion A	Bgy
Poblacion B	Bgy
Poblacion C	Bgy
Poblacion D	Bgy
Poblacion E	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Sugod	Bgy
Talinga	Bgy
Tigdaranao	Bgy
Tizon	Bgy
Villareal	Mun
Banquil	Bgy
Bino-ongan	Bgy
Burabod	Bgy
Cambaguio	Bgy
Canmucat	Bgy
Villarosa Pob.	Bgy
Conant	Bgy
Guintarcan	Bgy
Himyangan	Bgy
Igot	Bgy
Inarumbacan	Bgy
Inasudlan	Bgy
Lam-awan	Bgy
Lamingao	Bgy
Lawa-an	Bgy
Macopa	Bgy
Mahayag	Bgy
Malonoy	Bgy
Mercado (Pob.)	Bgy
Miramar (Pob.)	Bgy
Nagcaduha	Bgy
Pacao	Bgy
Pacoyoy	Bgy
Pangpang	Bgy
Plaridel	Bgy
Central (Pob.)	Bgy
Polangi	Bgy
San Andres	Bgy
San Fernando	Bgy
San Rafael	Bgy
San Roque	Bgy
Santa Rosa	Bgy
Santo Niño	Bgy
Soledad (Pob.)	Bgy
Tayud (Pob.)	Bgy
Tomabe	Bgy
Ulayan	Bgy
Patag	Bgy
Paranas	Mun
Anagasi	Bgy
Apolonia	Bgy
Bagsa	Bgy
Balbagan	Bgy
Bato	Bgy
Buray	Bgy
Cantaguic	Bgy
Cantao-an	Bgy
Cantato	Bgy
Casandig I	Bgy
Concepcion	Bgy
Jose Roño	Bgy
Cawayan	Bgy
Lawaan I	Bgy
Lipata	Bgy
Lokilokon	Bgy
Mangcal	Bgy
Maylobe	Bgy
Minarog	Bgy
Nawi	Bgy
Pabanog	Bgy
Paco	Bgy
Pagsa-ogan	Bgy
Pagsanjan	Bgy
Patag	Bgy
Pequit	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Poblacion 6	Bgy
Salay	Bgy
San Isidro	Bgy
Santo Niño	Bgy
Sulopan	Bgy
Tabucan	Bgy
Tapul	Bgy
Tenani	Bgy
Tigbawon	Bgy
Tula	Bgy
Tutubigan	Bgy
Casandig II	Bgy
Lawaan II	Bgy
Zumarraga	Mun
Alegria	Bgy
Arteche	Bgy
Bioso	Bgy
Boblaran	Bgy
Botaera	Bgy
Buntay	Bgy
Camayse	Bgy
Canwarak	Bgy
Ibarra	Bgy
Lumalantang	Bgy
Macalunod	Bgy
Maga-an	Bgy
Maputi	Bgy
Monbon	Bgy
Mualbual	Bgy
Pangdan	Bgy
Poro	Bgy
San Isidro	Bgy
Sugod	Bgy
Tinaugan	Bgy
Tubigan	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Marapilit	Bgy
Talib	Bgy
Tagapul-An	Mun
Baguiw	Bgy
Balocawe	Bgy
Guinbarucan	Bgy
Labangbaybay	Bgy
Luna	Bgy
Mataluto	Bgy
Nipa	Bgy
Pantalan	Bgy
Pulangbato	Bgy
San Vicente	Bgy
Sugod (Pob.)	Bgy
Suarez	Bgy
San Jose (Pob.)	Bgy
Trinidad	Bgy
San Jorge	Mun
Aurora	Bgy
Blanca Aurora	Bgy
Buenavista I	Bgy
Bulao	Bgy
Bungliw	Bgy
Cogtoto-og	Bgy
Calundan	Bgy
Cantaguic	Bgy
Canyaki	Bgy
Erenas	Bgy
Guadalupe	Bgy
Hernandez	Bgy
Himay	Bgy
Janipon	Bgy
La Paz	Bgy
Libertad	Bgy
Lincoro	Bgy
Matalud	Bgy
Mobo-ob	Bgy
Quezon	Bgy
Ranera	Bgy
Rosalim	Bgy
San Isidro	Bgy
San Jorge I (Pob.)	Bgy
Sapinit	Bgy
Sinit-an	Bgy
Tomogbong	Bgy
Gayondato	Bgy
Puhagan	Bgy
Anquiana	Bgy
Bay-ang	Bgy
Buenavista II	Bgy
Cabugao	Bgy
Cag-olo-olo	Bgy
Guindapunan	Bgy
Mabuhay	Bgy
Mancol (Pob.)	Bgy
Mombon	Bgy
Rawis	Bgy
San Jorge II (Pob.)	Bgy
San Juan	Bgy
Pagsanghan	Mun
Bangon	Bgy
Buenos Aires	Bgy
Calanyugan	Bgy
Caloloma	Bgy
Cambaye	Bgy
Pañge	Bgy
San Luis	Bgy
Villahermosa Occidental	Bgy
Canlapwas (Pob.)	Bgy
Libertad	Bgy
Santo Niño	Bgy
Viejo	Bgy
Villahermosa Oriental	Bgy
Southern Leyte	Prov
Anahawan	Mun
Amagusan	Bgy
Calintaan	Bgy
Canlabian	Bgy
Capacuhan	Bgy
Kagingkingan	Bgy
Lewing	Bgy
Lo-ok	Bgy
Mahalo	Bgy
Mainit	Bgy
Manigawong	Bgy
Poblacion	Bgy
San Vicente	Bgy
Tagup-on	Bgy
Cogon	Bgy
Bontoc	Mun
Banahao	Bgy
Baugo	Bgy
Beniton	Bgy
Buenavista	Bgy
Bunga	Bgy
Casao	Bgy
Catmon	Bgy
Catoogan	Bgy
Cawayanan	Bgy
Dao	Bgy
Divisoria	Bgy
Esperanza	Bgy
Guinsangaan	Bgy
Hibagwan	Bgy
Hilaan	Bgy
Himakilo	Bgy
Hitawos	Bgy
Lanao	Bgy
Lawgawan	Bgy
Mahayahay	Bgy
Malbago	Bgy
Mauylab	Bgy
Paku	Bgy
Pamahawan	Bgy
Pamigsian	Bgy
Pangi	Bgy
Poblacion	Bgy
Sampongon	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Taa	Bgy
Talisay	Bgy
Taytagan	Bgy
Tuburan	Bgy
Union	Bgy
Olisihan	Bgy
Anahao	Bgy
Pong-on	Bgy
San Ramon	Bgy
Santo Niño	Bgy
Hinunangan	Mun
Ambacon	Bgy
Badiangon	Bgy
Bangcas A	Bgy
Bangcas B	Bgy
Biasong	Bgy
Bugho	Bgy
Calag-itan	Bgy
Calayugan	Bgy
Calinao	Bgy
Canipaan	Bgy
Catublian	Bgy
Ilaya	Bgy
Ingan	Bgy
Labrador	Bgy
Lumbog	Bgy
Manalog	Bgy
Manlico	Bgy
Matin-ao	Bgy
Nava	Bgy
Nueva Esperanza	Bgy
Otama	Bgy
Palongpong	Bgy
Panalaron	Bgy
Patong	Bgy
Poblacion	Bgy
Pondol	Bgy
Salog	Bgy
Salvacion	Bgy
San Pablo Island	Bgy
San Pedro Island	Bgy
Santo Niño I	Bgy
Santo Niño II	Bgy
Tahusan	Bgy
Talisay	Bgy
Tawog	Bgy
Toptop	Bgy
Tuburan	Bgy
Union	Bgy
Upper Bantawon	Bgy
Libas	Bgy
Hinundayan	Mun
Amaga	Bgy
Ambao	Bgy
An-an	Bgy
Baculod	Bgy
Biasong	Bgy
Bugho	Bgy
Cabulisan	Bgy
District I (Pob.)	Bgy
District II (Pob.)	Bgy
District III (Pob.)	Bgy
Hubasan	Bgy
Cat-iwing	Bgy
Lungsodaan	Bgy
Navalita	Bgy
Plaridel	Bgy
Sabang	Bgy
Sagbok	Bgy
Libagon	Mun
Biasong	Bgy
Bogasong	Bgy
Cawayan	Bgy
Gakat	Bgy
Jubas (Pob.)	Bgy
Magkasag	Bgy
Mayuga	Bgy
Nahaong	Bgy
Nahulid	Bgy
Otikon	Bgy
Pangi	Bgy
Punta	Bgy
Talisay (Pob.)	Bgy
Tigbao	Bgy
Liloan	Mun
Amaga	Bgy
Anilao	Bgy
Bahay	Bgy
Cagbungalon	Bgy
Calian	Bgy
Caligangan	Bgy
Candayuman	Bgy
Estela	Bgy
Gud-an	Bgy
Guintoylan	Bgy
Himayangan	Bgy
Ilag	Bgy
Magaupas	Bgy
Malangsa	Bgy
Pres. Quezon	Bgy
Molopolo	Bgy
Pandan	Bgy
Poblacion	Bgy
President Roxas	Bgy
San Isidro	Bgy
San Roque	Bgy
Tabugon	Bgy
Catig	Bgy
Fatima	Bgy
City of Maasin (Capital)	City
Abgao (Pob.)	Bgy
Asuncion	Bgy
Bactul II	Bgy
Bactul I	Bgy
Badiang	Bgy
Bagtican	Bgy
Basak	Bgy
Bato II	Bgy
Bato I	Bgy
Batuan	Bgy
Baugo	Bgy
Bilibol	Bgy
Bogo	Bgy
Cabadiangan	Bgy
Cabulihan	Bgy
Cagnituan	Bgy
Cambooc	Bgy
Cansirong	Bgy
Canturing	Bgy
Canyuom	Bgy
Dongon	Bgy
Gawisan	Bgy
Guadalupe	Bgy
Hanginan	Bgy
Hantag	Bgy
Hinapu Daku	Bgy
Hinapu Gamay	Bgy
Ibarra	Bgy
Isagani	Bgy
Laboon	Bgy
Lanao	Bgy
Libhu	Bgy
Lonoy	Bgy
Lunas	Bgy
Mahayahay	Bgy
Malapoc Norte	Bgy
Malapoc Sur	Bgy
Mambajao (Pob.)	Bgy
Manhilo	Bgy
Mantahan (Pob.)	Bgy
Maria Clara	Bgy
Matin-ao	Bgy
Nasaug	Bgy
Nati	Bgy
Nonok Norte	Bgy
Nonok Sur	Bgy
Panan-awan	Bgy
Pansaan	Bgy
Pinascohan	Bgy
Rizal	Bgy
San Isidro	Bgy
San Jose	Bgy
San Rafael	Bgy
Santa Cruz	Bgy
Santa Rosa	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Soro-soro	Bgy
Tagnipa (Pob.)	Bgy
Tam-is	Bgy
Tawid	Bgy
Tigbawan	Bgy
Tomoy-tomoy	Bgy
Tunga-tunga (Pob.)	Bgy
Acasia	Bgy
Combado	Bgy
Libertad	Bgy
Lib-og	Bgy
Pasay	Bgy
San Agustin	Bgy
Macrohon	Mun
Aguinaldo	Bgy
Amparo	Bgy
Buscayan	Bgy
Cambaro	Bgy
Canlusay	Bgy
Flordeliz	Bgy
Ichon	Bgy
Ilihan	Bgy
Laray	Bgy
Lower Villa Jacinta	Bgy
Mabini	Bgy
Mohon	Bgy
Molopolo	Bgy
Rizal	Bgy
Salvador	Bgy
San Isidro	Bgy
San Joaquin	Bgy
San Roque	Bgy
Sindangan	Bgy
Upper Villa Jacinta	Bgy
Asuncion	Bgy
Bagong Silang	Bgy
Danao	Bgy
Guadalupe	Bgy
San Vicente Poblacion	Bgy
Santo Niño	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santo Rosario (Pob.)	Bgy
Upper Ichon	Bgy
Malitbog	Mun
Abgao	Bgy
Aurora	Bgy
Benit	Bgy
Caaga	Bgy
Cabul-anonan (Pob.)	Bgy
Cadaruhan	Bgy
Candatag	Bgy
Cantamuac	Bgy
Caraatan	Bgy
Concepcion	Bgy
Guinabonan	Bgy
Iba	Bgy
Lambonao	Bgy
Maningning	Bgy
Maujo	Bgy
Pasil (Pob.)	Bgy
Sabang	Bgy
San Antonio (Pob.)	Bgy
San Jose	Bgy
San Roque	Bgy
San Vicente	Bgy
Sangahon	Bgy
Santa Cruz	Bgy
Taliwa (Pob.)	Bgy
Tigbawan I	Bgy
Tigbawan II	Bgy
Timba	Bgy
Asuncion	Bgy
Cadaruhan Sur	Bgy
Fatima	Bgy
Juangon	Bgy
Kauswagan	Bgy
Mahayahay	Bgy
New Katipunan	Bgy
Pancil	Bgy
San Isidro	Bgy
Santo Niño	Bgy
Padre Burgos	Mun
Buenavista	Bgy
Bunga	Bgy
Laca	Bgy
Lungsodaan	Bgy
Poblacion	Bgy
San Juan	Bgy
Santa Sofia	Bgy
Santo Rosario	Bgy
Cantutang	Bgy
Dinahugan	Bgy
Tangkaan	Bgy
Pintuyan	Mun
Badiang	Bgy
Balongbalong	Bgy
Buenavista	Bgy
Bulawan	Bgy
Canlawis	Bgy
Catbawan	Bgy
Caubang	Bgy
Cogon	Bgy
Dan-an	Bgy
Lobo	Bgy
Mainit	Bgy
Manglit	Bgy
Nueva Estrella Sur	Bgy
Poblacion Ibabao	Bgy
Poblacion Ubos	Bgy
Ponod	Bgy
Son-ok I	Bgy
Tautag	Bgy
Nueva Estrella Norte	Bgy
Pociano D. Equipilag	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Son-ok II	Bgy
Saint Bernard	Mun
Atuyan	Bgy
Ayahag	Bgy
Bantawon	Bgy
Bolodbolod	Bgy
Nueva Esperanza	Bgy
Cabagawan	Bgy
Carnaga	Bgy
Catmon	Bgy
Guinsaugon	Bgy
Himatagon (Pob.)	Bgy
Himbangan	Bgy
Himos-onan	Bgy
Hindag-an	Bgy
Kauswagan	Bgy
Libas	Bgy
Lipanto	Bgy
Magatas	Bgy
Magbagacay	Bgy
Mahayag	Bgy
Mahayahay	Bgy
Malibago	Bgy
Malinao	Bgy
Panian	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Sug-angon	Bgy
Tabontabon	Bgy
Tambis I	Bgy
Tambis II	Bgy
Hinabian	Bgy
San Francisco	Mun
Anislagon	Bgy
Bongbong	Bgy
Central (Pob.)	Bgy
Dakit (Pob.)	Bgy
Habay	Bgy
Marayag	Bgy
Napantao	Bgy
Pinamudlan	Bgy
Santa Paz Norte	Bgy
Santa Paz Sur	Bgy
Sudmon	Bgy
Tinaan	Bgy
Tuno	Bgy
Ubos (Pob.)	Bgy
Bongawisan	Bgy
Causi	Bgy
Gabi	Bgy
Cahayag	Bgy
Malico	Bgy
Pasanon	Bgy
Punta	Bgy
Santa Cruz	Bgy
San Juan	Mun
Agay-ay	Bgy
Basak	Bgy
Bobon A	Bgy
Dayanog	Bgy
Santa Filomena	Bgy
Garrido	Bgy
Minoyho	Bgy
Pong-oy	Bgy
San Jose (Pob.)	Bgy
San Vicente	Bgy
Santa Cruz (Pob.)	Bgy
Santo Niño (Pob.)	Bgy
Somoje	Bgy
Sua	Bgy
Timba	Bgy
Osao	Bgy
San Roque	Bgy
Bobon B	Bgy
San Ricardo	Mun
Benit	Bgy
Bitoon	Bgy
Cabutan	Bgy
Camang	Bgy
Esperanza	Bgy
Pinut-an	Bgy
Poblacion	Bgy
San Antonio	Bgy
San Ramon	Bgy
Saub	Bgy
Timba	Bgy
Esperanza Dos	Bgy
Kinachawa	Bgy
Inolinan	Bgy
Looc	Bgy
Silago	Mun
Balagawan	Bgy
Catmon	Bgy
Pob. District I	Bgy
Pob. District II	Bgy
Hingatungan	Bgy
Katipunan	Bgy
Laguma	Bgy
Mercedes	Bgy
Puntana	Bgy
Salvacion	Bgy
Sap-ang	Bgy
Sudmon	Bgy
Tuba-on	Bgy
Tubod	Bgy
Imelda	Bgy
Sogod	Mun
Benit	Bgy
Buac Daku	Bgy
Buac Gamay	Bgy
Cabadbaran	Bgy
Concepcion	Bgy
Consolacion	Bgy
Dagsa	Bgy
Hibod-hibod	Bgy
Hindangan	Bgy
Hipantag	Bgy
Javier	Bgy
Kahupian	Bgy
Kanangkaan	Bgy
Kauswagan	Bgy
La Purisima Concepcion	Bgy
Libas	Bgy
Lum-an	Bgy
Mabicay	Bgy
Mac	Bgy
Magatas	Bgy
Mahayahay	Bgy
Malinao	Bgy
Maria Plana	Bgy
Milagroso	Bgy
Pancho Villa	Bgy
Pandan	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Zone V (Pob.)	Bgy
Rizal	Bgy
Salvacion	Bgy
San Francisco Mabuhay	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Miguel	Bgy
San Pedro	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Suba	Bgy
Tampoong	Bgy
Olisihan	Bgy
Tomas Oppus	Mun
Anahawan	Bgy
Banday (Pob.)	Bgy
Bogo (Pob.)	Bgy
Cabascan	Bgy
Camansi	Bgy
Cambite (Pob.)	Bgy
Canlupao	Bgy
Carnaga	Bgy
Cawayan	Bgy
Hinagtikan	Bgy
Hinapo	Bgy
Hugpa	Bgy
Iniguihan Pob.	Bgy
Looc	Bgy
Maanyag	Bgy
Maslog	Bgy
Ponong	Bgy
Rizal	Bgy
San Isidro	Bgy
San Miguel	Bgy
Tinago	Bgy
Biasong	Bgy
Higosoan	Bgy
Mag-ata	Bgy
San Antonio	Bgy
San Roque	Bgy
Luan	Bgy
Mapgap	Bgy
San Agustin	Bgy
Limasawa	Mun
Cabulihan	Bgy
Lugsongan	Bgy
Magallanes	Bgy
San Agustin	Bgy
San Bernardo	Bgy
Triana	Bgy
Biliran	Prov
Almeria	Mun
Caucab	Bgy
Iyosan	Bgy
Jamorawon	Bgy
Lo-ok	Bgy
Matanga	Bgy
Pili	Bgy
Poblacion	Bgy
Pulang Bato	Bgy
Salangi	Bgy
Sampao	Bgy
Tabunan	Bgy
Talahid	Bgy
Tamarindo	Bgy
Biliran	Mun
Bato	Bgy
Burabod	Bgy
Busali	Bgy
Hugpa	Bgy
Julita	Bgy
Canila	Bgy
Pinangumhan	Bgy
San Isidro (Pob.)	Bgy
San Roque (Pob.)	Bgy
Sanggalang	Bgy
Villa Enage	Bgy
Cabucgayan	Mun
Balaquid	Bgy
Baso	Bgy
Bunga	Bgy
Caanibongan	Bgy
Casiawan	Bgy
Esperanza (Pob.)	Bgy
Langgao	Bgy
Libertad	Bgy
Looc	Bgy
Magbangon (Pob.)	Bgy
Pawikan	Bgy
Salawad	Bgy
Talibong	Bgy
Caibiran	Mun
Alegria	Bgy
Asug	Bgy
Bari-is	Bgy
Binohangan	Bgy
Cabibihan	Bgy
Kawayanon	Bgy
Looc	Bgy
Manlabang	Bgy
Caulangohan	Bgy
Maurang	Bgy
Palanay (Pob.)	Bgy
Palengke (Pob.)	Bgy
Tomalistis	Bgy
Union	Bgy
Uson	Bgy
Victory (Pob.)	Bgy
Villa Vicenta	Bgy
Culaba	Mun
Acaban	Bgy
Bacolod	Bgy
Binongtoan	Bgy
Bool Central (Pob.)	Bgy
Bool East (Pob.)	Bgy
Bool West (Pob.)	Bgy
Calipayan	Bgy
Guindapunan	Bgy
Habuhab	Bgy
Looc	Bgy
Marvel (Pob.)	Bgy
Patag	Bgy
Pinamihagan	Bgy
Culaba Central (Pob.)	Bgy
Salvacion	Bgy
San Roque	Bgy
Virginia (Pob.)	Bgy
Kawayan	Mun
Baganito	Bgy
Balacson	Bgy
Bilwang	Bgy
Bulalacao	Bgy
Burabod	Bgy
Inasuyan	Bgy
Kansanok	Bgy
Mada-o	Bgy
Mapuyo	Bgy
Masagaosao	Bgy
Masagongsong	Bgy
Poblacion	Bgy
Tabunan North	Bgy
Tubig Guinoo	Bgy
Tucdao	Bgy
Ungale	Bgy
Balite	Bgy
Buyo	Bgy
Villa Cornejo	Bgy
San Lorenzo	Bgy
Maripipi	Mun
Agutay	Bgy
Banlas	Bgy
Bato	Bgy
Binalayan West	Bgy
Binalayan East	Bgy
Burabod	Bgy
Calbani	Bgy
Canduhao	Bgy
Casibang	Bgy
Danao	Bgy
Ol-og	Bgy
Binongto-an	Bgy
Ermita	Bgy
Trabugan	Bgy
Viga	Bgy
Naval (Capital)	Mun
Agpangi	Bgy
Anislagan	Bgy
Atipolo	Bgy
Calumpang	Bgy
Capiñahan	Bgy
Caraycaray	Bgy
Catmon	Bgy
Haguikhikan	Bgy
Padre Inocentes Garcia (Pob.)	Bgy
Libertad	Bgy
Lico	Bgy
Lucsoon	Bgy
Mabini	Bgy
San Pablo	Bgy
Santo Niño	Bgy
Santissimo Rosario Pob.	Bgy
Talustusan	Bgy
Villa Caneja	Bgy
Villa Consuelo	Bgy
Borac	Bgy
Cabungaan	Bgy
Imelda	Bgy
Larrazabal	Bgy
Libtong	Bgy
Padre Sergio Eamiguel	Bgy
Sabang	Bgy
Region IX (Zamboanga Peninsula)	Reg
Zamboanga del Norte	Prov
City of Dapitan	City
Aliguay	Bgy
Antipolo	Bgy
Aseniero	Bgy
Ba-ao	Bgy
Banbanan	Bgy
Barcelona	Bgy
Baylimango	Bgy
Burgos	Bgy
Canlucani	Bgy
Carang	Bgy
Dampalan	Bgy
Daro	Bgy
Diwa-an	Bgy
Guimputlan	Bgy
Hilltop	Bgy
Ilaya	Bgy
Larayan	Bgy
Liyang	Bgy
Maria Cristina	Bgy
Maria Uray	Bgy
Masidlakon	Bgy
Napo	Bgy
Opao	Bgy
Oro	Bgy
Owaon	Bgy
Oyan	Bgy
Polo	Bgy
Potungan	Bgy
San Francisco	Bgy
San Nicolas	Bgy
San Pedro	Bgy
San Vicente	Bgy
Sicayab-Bucana	Bgy
Sigayan	Bgy
Selinog	Bgy
Sinonoc	Bgy
Santo Niño	Bgy
Sulangon	Bgy
Tag-ulo	Bgy
Taguilon	Bgy
Kauswagan	Bgy
Tamion	Bgy
Bagting (Pob.)	Bgy
Banonong (Pob.)	Bgy
Cawa-cawa (Pob.)	Bgy
Dawo (Pob.)	Bgy
Matagobtob Pob.	Bgy
Linabo (Pob.)	Bgy
Potol (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
City of Dipolog (Capital)	City
Cogon	Bgy
Dicayas	Bgy
Diwan	Bgy
Galas	Bgy
Gulayon	Bgy
Lugdungan	Bgy
Minaog	Bgy
Olingan	Bgy
Estaca (Pob.)	Bgy
Biasong (Pob.)	Bgy
Barra (Pob.)	Bgy
Central (Pob.)	Bgy
Miputak (Pob.)	Bgy
Punta	Bgy
San Jose	Bgy
Sangkol	Bgy
Santa Filomena	Bgy
Sicayab	Bgy
Sinaman	Bgy
Turno	Bgy
Santa Isabel	Bgy
Katipunan	Mun
Balok	Bgy
Basagan	Bgy
Biniray	Bgy
Bulawan	Bgy
Daanglungsod	Bgy
Dabiak	Bgy
Dr. Jose Rizal	Bgy
Fimagas	Bgy
Malugas	Bgy
Malasay	Bgy
Matam	Bgy
Mias	Bgy
Miatan	Bgy
Nanginan	Bgy
Barangay Uno (Pob.)	Bgy
Barangay Dos (Pob.)	Bgy
San Antonio	Bgy
Seres	Bgy
Seroan	Bgy
Singatong	Bgy
Sinuyak	Bgy
Sitog	Bgy
Tuburan	Bgy
Carupay	Bgy
Loyuran	Bgy
New Tambo	Bgy
Patik	Bgy
Sanao	Bgy
San Vicente	Bgy
Santo Niño	Bgy
La Libertad	Mun
El Paraiso	Bgy
La Union	Bgy
La Victoria	Bgy
Mauswagon	Bgy
Mercedes	Bgy
New Argao	Bgy
New Bataan	Bgy
New Carcar	Bgy
Poblacion	Bgy
San Jose	Bgy
Santa Catalina	Bgy
Santa Cruz	Bgy
Singaran	Bgy
Labason	Mun
Antonino (Pob.)	Bgy
Balas	Bgy
Bobongan	Bgy
Dansalan	Bgy
Gabu	Bgy
Immaculada	Bgy
Kipit	Bgy
La Union	Bgy
Lapatan	Bgy
Lawagan	Bgy
Lawigan	Bgy
Lopoc (Pob.)	Bgy
Malintuboan	Bgy
New Salvacion	Bgy
Osukan	Bgy
Patawag	Bgy
San Isidro	Bgy
Ubay	Bgy
Gil Sanchez	Bgy
Imelda	Bgy
Liloy	Mun
Banigan	Bgy
Baybay (Pob.)	Bgy
Cabangcalan	Bgy
Candelaria	Bgy
Causwagan	Bgy
Communal	Bgy
Compra	Bgy
Fatima	Bgy
Goaw	Bgy
Goin	Bgy
Kayok	Bgy
Lamao	Bgy
La Libertad	Bgy
Panabang	Bgy
Patawag	Bgy
Punta	Bgy
San Isidro	Bgy
San Francisco	Bgy
San Miguel	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Silucap	Bgy
Tapican	Bgy
Timan	Bgy
Villa M. Tejero	Bgy
Dela Paz	Bgy
El Paraiso	Bgy
Ganase	Bgy
Mabuhay	Bgy
Maigang	Bgy
Malila	Bgy
Mauswagon	Bgy
New Bethlehem	Bgy
Overview	Bgy
San Roque	Bgy
Villa Calixto Sudiacal	Bgy
Canaan	Bgy
Manukan	Mun
Dipane	Bgy
Disakan	Bgy
Gupot	Bgy
Libuton	Bgy
Linay	Bgy
Lupasang	Bgy
Mate	Bgy
Poblacion	Bgy
Saluyong	Bgy
Serongan	Bgy
Villaramos	Bgy
Don Jose Aguirre	Bgy
Lingatongan	Bgy
Meses	Bgy
Palaranan	Bgy
Pangandao	Bgy
Patagan	Bgy
San Antonio	Bgy
Upper Disakan	Bgy
East Poblacion	Bgy
Punta Blanca	Bgy
Suisayan	Bgy
Mutia	Mun
Alvenda	Bgy
Buenasuerte	Bgy
Diland	Bgy
Diolen	Bgy
Head Tipan	Bgy
New Casul	Bgy
Newland	Bgy
New Siquijor	Bgy
Paso Rio	Bgy
Poblacion	Bgy
San Miguel	Bgy
Tinglan	Bgy
Totongon	Bgy
Tubac	Bgy
Unidos	Bgy
Santo Tomas	Bgy
Piñan	Mun
Adante	Bgy
Bacuyong	Bgy
Bagong Silang	Bgy
Calican	Bgy
Ubay	Bgy
Del Pilar	Bgy
Dilawa	Bgy
Desin	Bgy
Dionum	Bgy
Lapu-lapu	Bgy
Lower Gumay	Bgy
Luzvilla	Bgy
Poblacion North	Bgy
Santa Fe	Bgy
Segabe	Bgy
Sikitan	Bgy
Silano	Bgy
Teresita	Bgy
Tinaytayan	Bgy
Upper Gumay	Bgy
Villarico	Bgy
Poblacion South	Bgy
Polanco	Mun
Anastacio	Bgy
Bandera	Bgy
Bethlehem	Bgy
Dangi	Bgy
Dansullan	Bgy
De Venta Perla	Bgy
Guinles	Bgy
Isis	Bgy
Labrador	Bgy
Lapayanbaja	Bgy
Letapan	Bgy
Linabo	Bgy
Lingasad	Bgy
Macleodes	Bgy
Magangon	Bgy
Maligaya	Bgy
Milad	Bgy
New Lebangon	Bgy
New Sicayab	Bgy
Obay	Bgy
Pian	Bgy
Poblacion South	Bgy
San Antonio	Bgy
San Miguel	Bgy
San Pedro	Bgy
Santo Niño	Bgy
Silawe	Bgy
Sianib	Bgy
Villahermosa	Bgy
Poblacion North	Bgy
Pres. Manuel A. Roxas	Mun
Balubo	Bgy
Canibongan	Bgy
Capase	Bgy
Denoman	Bgy
Dohinob	Bgy
Langatian	Bgy
Lipakan	Bgy
Marupay	Bgy
Moliton	Bgy
Nabilid	Bgy
Pangologon	Bgy
Piñalan	Bgy
Piñamar	Bgy
Sebod	Bgy
Tanayan	Bgy
Villahermoso	Bgy
Banbanan	Bgy
Cape	Bgy
Galokso	Bgy
Gubat	Bgy
Irasan	Bgy
Labakid	Bgy
Panampalay	Bgy
Piao	Bgy
Pongolan	Bgy
Salisig	Bgy
Sibatog	Bgy
Situbo	Bgy
Tantingon	Bgy
Upper Irasan	Bgy
Upper Minang	Bgy
Rizal	Mun
Birayan	Bgy
Damasing	Bgy
La Esperanza	Bgy
Mabuhay	Bgy
Mabunao	Bgy
North Mapang	Bgy
Mitimos	Bgy
Nangca	Bgy
Nangcaan	Bgy
Napilan	Bgy
Nasipang	Bgy
New Dapitan	Bgy
Nilabo	Bgy
East Poblacion	Bgy
Rizalina	Bgy
San Roque	Bgy
Sebaca	Bgy
Sipaon	Bgy
Tolon	Bgy
Balubohan	Bgy
South Mapang	Bgy
West Poblacion	Bgy
Salug	Mun
Bacong	Bgy
Balakan	Bgy
Binoni	Bgy
Calucap	Bgy
Canawan	Bgy
Caracol	Bgy
Danao	Bgy
Dinoan	Bgy
Dipolod	Bgy
Fatima	Bgy
Liguac	Bgy
Lipakan	Bgy
Mucas	Bgy
Poblacion	Bgy
Ramon Magsaysay	Bgy
Tambalang	Bgy
Tapalan	Bgy
Ipilan	Bgy
Lanawan	Bgy
Pukay	Bgy
Santo Niño	Bgy
Pacuhan	Bgy
Poblacion East	Bgy
Sergio Osmeña Sr.	Mun
Antonino	Bgy
Bagong Baguio	Bgy
Bagumbayan	Bgy
Biayon	Bgy
Buenavista	Bgy
Dampalan	Bgy
Danao	Bgy
Don Eleno	Bgy
Kauswagan	Bgy
Labiray	Bgy
Liwanag	Bgy
Mabuhay	Bgy
Macalibre	Bgy
Mahayahay	Bgy
Marapong	Bgy
Nazareth	Bgy
Nebo	Bgy
New Rizal	Bgy
New Tangub	Bgy
Nuevavista	Bgy
Pedagan	Bgy
Penacio	Bgy
Poblacion Alto	Bgy
Poblacion Bajo	Bgy
Princesa Lamaya	Bgy
Princesa Freshia	Bgy
San Antonio	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
Sinaad	Bgy
Sinai	Bgy
Situbo	Bgy
Tinago	Bgy
Tinindugan	Bgy
Tuburan	Bgy
Venus	Bgy
Wilben	Bgy
Siayan	Mun
Balok	Bgy
Datagan	Bgy
Denoyan	Bgy
Diongan	Bgy
Domogok	Bgy
Dumpilas	Bgy
Gonayen	Bgy
Guibo	Bgy
Gunyan	Bgy
Litolet	Bgy
Macasing	Bgy
Mangilay	Bgy
Moyo	Bgy
Pange	Bgy
Paranglumba (Pob.)	Bgy
Polayo	Bgy
Sayaw	Bgy
Seriac	Bgy
Siayan Proper (Pob.)	Bgy
Balunokan	Bgy
Muñoz	Bgy
Suguilon	Bgy
Sibuco	Mun
Anongan	Bgy
Basak	Bgy
Cawit-cawit	Bgy
Dinulan	Bgy
Jatian	Bgy
Lakiki	Bgy
Lambagoan	Bgy
Limpapa	Bgy
Lingayon	Bgy
Lintangan	Bgy
Litawan	Bgy
Lunday	Bgy
Malayal	Bgy
Mantivo	Bgy
Panganuran	Bgy
Pangian	Bgy
Paniran	Bgy
Poblacion	Bgy
Puliran	Bgy
Santo Niño	Bgy
Bongalao	Bgy
Cabbunan	Bgy
Culaguan	Bgy
Cusipan	Bgy
Kamarangan	Bgy
Nala (Pob.)	Bgy
Pasilnahut	Bgy
Tangarak	Bgy
Sibutad	Mun
Bagacay	Bgy
Calilic	Bgy
Calube	Bgy
Kanim	Bgy
Delapa	Bgy
Libay	Bgy
Magsaysay	Bgy
Marapong	Bgy
Minlasag	Bgy
Oyan	Bgy
Panganuran	Bgy
Poblacion	Bgy
Sawang	Bgy
Sibuloc	Bgy
Sinipay	Bgy
Sipaloc	Bgy
Sindangan	Mun
Bago	Bgy
Binuangan	Bgy
Bitoon	Bgy
Dicoyong	Bgy
Don Ricardo Macias	Bgy
Dumalogdog	Bgy
Inuman	Bgy
La Concepcion	Bgy
Lagag	Bgy
Lapero	Bgy
Mandih	Bgy
Maras	Bgy
Mawal	Bgy
Misok	Bgy
Motibot	Bgy
Nato	Bgy
Pangalalan	Bgy
Piao	Bgy
Poblacion	Bgy
Siare	Bgy
Talinga	Bgy
Tinaplan	Bgy
Tigbao	Bgy
Titik	Bgy
Bato	Bgy
Bucana	Bgy
Caluan	Bgy
Calubian	Bgy
Calatunan	Bgy
Dagohoy	Bgy
Datagan	Bgy
Disud	Bgy
Doña Josefa	Bgy
Gampis	Bgy
Goleo	Bgy
Imelda	Bgy
Joaquin Macias	Bgy
Labakid	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Upper Nipaan	Bgy
Benigno Aquino Jr.	Bgy
Fatima	Bgy
Balok	Bgy
Bantayan	Bgy
Dapaon	Bgy
Datu Tangkilan	Bgy
La Roche San Miguel	Bgy
Lawis	Bgy
Magsaysay	Bgy
Nipaan	Bgy
Upper Inuman	Bgy
Siocon	Mun
Balagunan	Bgy
Andres Micubo Jr.	Bgy
Bucana	Bgy
Bulacan	Bgy
Dionisio Riconalla	Bgy
Candiz	Bgy
Jose P. Brillantes, Sr.	Bgy
Latabon	Bgy
Mateo Francisco	Bgy
Malipot	Bgy
New Lituban	Bgy
Pisawak	Bgy
Poblacion	Bgy
Santa Maria	Bgy
Suhaile Arabi	Bgy
Tibangao	Bgy
S. Cabral	Bgy
Datu Sailila	Bgy
Makiang	Bgy
Malambuhangin	Bgy
Manaol	Bgy
Matiag	Bgy
Pangian	Bgy
Siay	Bgy
Tabayo	Bgy
Tagaytay	Bgy
Sirawai	Mun
Balatakan	Bgy
Balonkan	Bgy
Balubuan	Bgy
Bitugan	Bgy
Bongon	Bgy
Catuyan	Bgy
Culasian	Bgy
Danganon	Bgy
Doña Cecilia	Bgy
Guban	Bgy
Lagundi	Bgy
Libucon	Bgy
Lubok	Bgy
Macuyon	Bgy
Minanga	Bgy
Motong	Bgy
Napulan	Bgy
Panabutan	Bgy
Piacan	Bgy
Piña	Bgy
Pisa Puti	Bgy
Pisa Itom	Bgy
Saint Mary (Pob.)	Bgy
San Nicolas (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Pugos	Bgy
Pula Bato	Bgy
Pulang Lupa	Bgy
San Roque (Pob.)	Bgy
Sipakit	Bgy
Sipawa	Bgy
Sirawai Proper (Pob.)	Bgy
Talabiga	Bgy
Tapanayan	Bgy
Tampilisan	Mun
Cabong	Bgy
Galingon	Bgy
Lawaan	Bgy
Molos	Bgy
New Dapitan	Bgy
Situbo	Bgy
Poblacion	Bgy
Balacbaan	Bgy
Banbanan	Bgy
Barili	Bgy
Camul	Bgy
Farmington	Bgy
Lumbayao	Bgy
Malila-t	Bgy
Sandayong	Bgy
Santo Niño	Bgy
Tilubog	Bgy
Tininggaan	Bgy
Tubod	Bgy
Znac	Bgy
Jose Dalman	Mun
Dinasan	Bgy
Madalag	Bgy
Manawan	Bgy
Poblacion	Bgy
Siparok	Bgy
Tabon	Bgy
Tamil	Bgy
Balatakan	Bgy
Bitoon	Bgy
Ilihan	Bgy
Labakid	Bgy
Lipay	Bgy
Litalip	Bgy
Lopero	Bgy
Lumanping	Bgy
Marupay	Bgy
Sigamok	Bgy
Tamarok	Bgy
Gutalac	Mun
Banganon	Bgy
Cocob	Bgy
Poblacion	Bgy
La Libertad	Bgy
Lux	Bgy
Panganuran	Bgy
Pitawe	Bgy
Canupong	Bgy
Mamawan	Bgy
Sibalic	Bgy
Tipan	Bgy
Bacong	Bgy
Bagong Silang	Bgy
Bayanihan	Bgy
Buenavista	Bgy
Datagan	Bgy
Imelda	Bgy
Loay	Bgy
Malian	Bgy
Map	Bgy
Matunoy	Bgy
New Dapitan	Bgy
Pitogo	Bgy
Salvador	Bgy
San Isidro	Bgy
San Juan	Bgy
San Roque	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Sas	Bgy
Upper Gutalac	Bgy
Immaculada Concepcion	Bgy
Lower Lux	Bgy
Baliguian	Mun
Alegria	Bgy
Diangas	Bgy
Diculom	Bgy
Guimotan	Bgy
Kauswagan	Bgy
Kilalaban	Bgy
Linay	Bgy
Lumay	Bgy
Malinao	Bgy
Mamad	Bgy
Mamawan	Bgy
Milidan	Bgy
Nonoyan	Bgy
Poblacion	Bgy
San Jose	Bgy
Tamao	Bgy
Tan-awan	Bgy
Godod	Mun
Baluno	Bgy
Banuangan	Bgy
Bunawan	Bgy
Dilucot	Bgy
Dipopor	Bgy
Guisapong	Bgy
Limbonga	Bgy
Lomogom	Bgy
Mauswagon	Bgy
Miampic	Bgy
Poblacion	Bgy
Raba	Bgy
Rambon	Bgy
San Pedro	Bgy
Sarawagan	Bgy
Sianan	Bgy
Sioran	Bgy
Bacungan	Mun
Bacungan (Pob.)	Bgy
Bogabongan	Bgy
Delusom	Bgy
Mangop	Bgy
Manil	Bgy
Mawal	Bgy
Midatag	Bgy
Nasibac	Bgy
Rizon	Bgy
Sipacong	Bgy
Santa Maria	Bgy
Talinga	Bgy
Tinaplan	Bgy
Tiniguiban	Bgy
Tinuyop	Bgy
Tiogan	Bgy
Titik	Bgy
Morob	Bgy
Kalawit	Mun
Batayan	Bgy
Botong	Bgy
Concepcion	Bgy
Daniel Maing	Bgy
Fatima	Bgy
Gatas	Bgy
Kalawit (Pob.)	Bgy
Marcelo	Bgy
New Calamba	Bgy
Palalian	Bgy
Paraiso	Bgy
Pianon	Bgy
San Jose	Bgy
Tugop*	Bgy
Zamboanga del Sur	Prov
Aurora	Mun
Acad	Bgy
Alang-alang	Bgy
Alegria	Bgy
Anonang	Bgy
Bagong Mandaue	Bgy
Bagong Maslog	Bgy
Bagong Oslob	Bgy
Bagong Pitogo	Bgy
Baki	Bgy
Balas	Bgy
Balide	Bgy
Balintawak	Bgy
Bayabas	Bgy
Bemposa	Bgy
Cabilinan	Bgy
Campo Uno	Bgy
Ceboneg	Bgy
Commonwealth	Bgy
Gubaan	Bgy
Inasagan	Bgy
Inroad	Bgy
Kahayagan East	Bgy
Kahayagan West	Bgy
Kauswagan	Bgy
La Victoria	Bgy
Lantungan	Bgy
Libertad	Bgy
Lintugop	Bgy
Lubid	Bgy
Maguikay	Bgy
Mahayahay	Bgy
Monte Alegre	Bgy
Montela	Bgy
Napo	Bgy
Panaghiusa	Bgy
Poblacion	Bgy
Resthouse	Bgy
Romarate	Bgy
San Jose	Bgy
San Juan	Bgy
Sapa Loboc	Bgy
Tagulalo	Bgy
La Paz	Bgy
Waterfall	Bgy
Bayog	Mun
Baking	Bgy
Balukbahan	Bgy
Balumbunan	Bgy
Bantal	Bgy
Bobuan	Bgy
Camp Blessing	Bgy
Canoayan	Bgy
Conacon	Bgy
Dagum	Bgy
Damit	Bgy
Datagan	Bgy
Depase	Bgy
Deporehan	Bgy
Depore	Bgy
Dimalinao	Bgy
Depili	Bgy
Kahayagan	Bgy
Kanipaan	Bgy
Lamare	Bgy
Liba	Bgy
Matin-ao	Bgy
Matun-og	Bgy
Poblacion	Bgy
Pulang Bato	Bgy
Salawagan	Bgy
Sigacad	Bgy
Supon	Bgy
Pangi	Bgy
Dimataling	Mun
Bacayawan	Bgy
Baha	Bgy
Baluno	Bgy
Binuay	Bgy
Buburay	Bgy
Grap	Bgy
Kagawasan	Bgy
Lalab	Bgy
Libertad	Bgy
Mahayag	Bgy
Poblacion	Bgy
Saloagan	Bgy
Sugbay Uno	Bgy
Sumpot	Bgy
Tiniguangan	Bgy
Tinggabulong	Bgy
Tipangi	Bgy
Balanagan	Bgy
Josefina	Bgy
Magahis	Bgy
Mercedes	Bgy
San Roque	Bgy
Sumbato	Bgy
Upper Ludiong	Bgy
Dinas	Mun
Bacawan	Bgy
Benuatan	Bgy
Beray	Bgy
Dongos	Bgy
Guinicolalay	Bgy
Kinacap	Bgy
Legarda 2	Bgy
Legarda 3	Bgy
Legarda 1	Bgy
Lucoban	Bgy
Lower Dimaya	Bgy
Ludiong	Bgy
East Migpulao	Bgy
West Migpulao	Bgy
Nangka	Bgy
Ignacio Garrata	Bgy
Old Mirapao	Bgy
Poblacion	Bgy
Proper Dimaya	Bgy
Sagacad	Bgy
Sambulawan	Bgy
San Isidro	Bgy
Sumpotan	Bgy
Songayan	Bgy
Tarakan	Bgy
Upper Dimaya	Bgy
Upper Sibul	Bgy
Pisa-an	Bgy
Don Jose	Bgy
Nian	Bgy
Dumalinao	Mun
Bag-ong Misamis	Bgy
Baga	Bgy
Bag-ong Silao	Bgy
Baloboan	Bgy
Banta-ao	Bgy
Bibilik	Bgy
Calingayan	Bgy
Camalig	Bgy
Camanga	Bgy
Cuatro-cuatro	Bgy
Locuban	Bgy
Malasik	Bgy
Mama	Bgy
Matab-ang	Bgy
Mecolong	Bgy
Metokong	Bgy
Motosawa	Bgy
Pag-asa (Pob.)	Bgy
Paglaum (Pob.)	Bgy
Pantad	Bgy
Piniglibano	Bgy
Rebokon	Bgy
San Agustin	Bgy
Sibucao	Bgy
Sumadat	Bgy
Tikwas	Bgy
Tina	Bgy
Tubo-Pait	Bgy
Upper Dumalinao	Bgy
Anonang	Bgy
Dumingag	Mun
Bag-ong Valencia	Bgy
Bucayan	Bgy
Calumanggi	Bgy
Caridad	Bgy
Danlugan	Bgy
Datu Totocan	Bgy
Dilud	Bgy
Ditulan	Bgy
Dulian	Bgy
Dulop	Bgy
Guintananan	Bgy
Guitran	Bgy
Gumpingan	Bgy
La Fortuna	Bgy
Libertad	Bgy
Licabang	Bgy
Lipawan	Bgy
Lower Landing	Bgy
Lower Timonan	Bgy
Macasing	Bgy
Mahayahay	Bgy
Malagalad	Bgy
Manlabay	Bgy
Maralag	Bgy
Marangan	Bgy
New Basak	Bgy
Bagong Kauswagan	Bgy
Saad	Bgy
Salvador	Bgy
San Pablo (Pob.)	Bgy
San Pedro (Pob.)	Bgy
San Vicente	Bgy
Senote	Bgy
Sinonok	Bgy
Sunop	Bgy
Tagun	Bgy
Tamurayan	Bgy
Upper Landing	Bgy
Upper Timonan	Bgy
Bagong Silang	Bgy
Dapiwak	Bgy
Labangon	Bgy
San Juan	Bgy
Canibongan	Bgy
Kumalarang	Mun
Bogayo	Bgy
Bolisong	Bgy
Bualan	Bgy
Boyugan East	Bgy
Boyugan West	Bgy
Diplo	Bgy
Gawil	Bgy
Gusom	Bgy
Kitaan Dagat	Bgy
Limamawan	Bgy
Mahayahay	Bgy
Pangi	Bgy
Poblacion	Bgy
Picanan	Bgy
Salagmanok	Bgy
Secade	Bgy
Suminalum	Bgy
Lantawan	Bgy
Labangan	Mun
Bagalupa	Bgy
Balimbingan	Bgy
Binayan	Bgy
Bokong	Bgy
Bulanit	Bgy
Cogonan	Bgy
Dalapang	Bgy
Dipaya	Bgy
Langapod	Bgy
Lantian	Bgy
Lower Campo Islam (Pob.)	Bgy
Lower Pulacan	Bgy
New Labangan	Bgy
Noboran	Bgy
Old Labangan	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Tapodoc	Bgy
Tawagan Norte	Bgy
Upper Campo Islam (Pob.)	Bgy
Upper Pulacan	Bgy
Combo	Bgy
Dimasangca	Bgy
Lower Sang-an	Bgy
Upper Sang-an	Bgy
Lapuyan	Mun
Bulawan	Bgy
Carpoc	Bgy
Danganan	Bgy
Dansal	Bgy
Dumara	Bgy
Linokmadalum	Bgy
Luanan	Bgy
Lubusan	Bgy
Mahalingeb	Bgy
Mandeg	Bgy
Maralag	Bgy
Maruing	Bgy
Molum	Bgy
Pampang	Bgy
Pantad	Bgy
Pingalay	Bgy
Salambuyan	Bgy
San Jose	Bgy
Sayog	Bgy
Tabon	Bgy
Talabob	Bgy
Tiguha	Bgy
Tininghalang	Bgy
Tipasan	Bgy
Tugaya	Bgy
Poblacion	Bgy
Mahayag	Mun
Bag-ong Balamban	Bgy
Bag-ong Dalaguete	Bgy
Marabanan	Bgy
Boniao	Bgy
Delusom	Bgy
Diwan	Bgy
Guripan	Bgy
Kaangayan	Bgy
Kabuhi	Bgy
Lourmah	Bgy
Lower Salug Daku	Bgy
Lower Santo Niño	Bgy
Sicpao	Bgy
Malubo	Bgy
Manguiles	Bgy
Panagaan	Bgy
Paraiso	Bgy
Pedagan	Bgy
Poblacion	Bgy
Pugwan	Bgy
San Isidro	Bgy
San Jose	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Tuboran	Bgy
Tumapic	Bgy
Upper Salug Daku	Bgy
Upper Santo Niño	Bgy
Tulan	Bgy
Margosatubig	Mun
Balintawak	Bgy
Bularong	Bgy
Kalian	Bgy
Kolot	Bgy
Digon	Bgy
Guinimanan	Bgy
Igat Island	Bgy
Limamawan	Bgy
Limbatong	Bgy
Lumbog	Bgy
Poblacion	Bgy
Sagua	Bgy
Talanusa	Bgy
Tiguian	Bgy
Tulapok	Bgy
Magahis	Bgy
Josefina	Bgy
Midsalip	Mun
Bacahan	Bgy
Bibilop	Bgy
Buloron	Bgy
Canipay Norte	Bgy
Canipay Sur	Bgy
Cumaron	Bgy
Golictop	Bgy
Kahayagan	Bgy
Lumpunid	Bgy
Matalang	Bgy
New Katipunan	Bgy
New Unidos	Bgy
Palili	Bgy
Piwan	Bgy
Poblacion A	Bgy
Poblacion B	Bgy
Sigapod	Bgy
Timbaboy	Bgy
Tulbong	Bgy
Tuluan	Bgy
Pisompongan	Bgy
Cabaloran	Bgy
Dakayakan	Bgy
Duelic	Bgy
Dumalinao	Bgy
Ecuan	Bgy
Guinabot	Bgy
Guitalos	Bgy
Licuro-an	Bgy
Pawan	Bgy
Balonai	Bgy
Guma	Bgy
Pili	Bgy
Molave	Mun
Alicia	Bgy
Ariosa	Bgy
Bagong Argao	Bgy
Bagong Gutlang	Bgy
Blancia	Bgy
Bogo Capalaran	Bgy
Dalaon	Bgy
Dipolo	Bgy
Dontulan	Bgy
Gonosan	Bgy
Culo	Bgy
Lower Dimorok	Bgy
Lower Dimalinao	Bgy
Mabuhay	Bgy
Madasigon (Pob.)	Bgy
Makuguihon (Pob.)	Bgy
Maloloy-on (Pob.)	Bgy
Miligan	Bgy
Parasan	Bgy
Rizal	Bgy
Santo Rosario	Bgy
Silangit	Bgy
Simata	Bgy
Sudlon	Bgy
Upper Dimorok	Bgy
City of Pagadian (Capital)	City
Alegria	Bgy
Balangasan (Pob.)	Bgy
Balintawak	Bgy
Baloyboan	Bgy
Banale	Bgy
Bogo	Bgy
Bomba	Bgy
Buenavista	Bgy
Bulatok	Bgy
Bulawan	Bgy
Danlugan	Bgy
Dao	Bgy
Datagan	Bgy
Deborok	Bgy
Ditoray	Bgy
Gatas (Pob.)	Bgy
Gubac	Bgy
Gubang	Bgy
Kagawasan	Bgy
Kahayagan	Bgy
Kalasan	Bgy
La Suerte	Bgy
Lala	Bgy
Lapidian	Bgy
Lenienza	Bgy
Lizon Valley	Bgy
Lourdes	Bgy
Lower Sibatang	Bgy
Lumad	Bgy
Macasing	Bgy
Manga	Bgy
Muricay	Bgy
Napolan	Bgy
Palpalan	Bgy
Pedulonan	Bgy
Poloyagan	Bgy
San Francisco (Pob.)	Bgy
San Jose (Pob.)	Bgy
San Pedro (Pob.)	Bgy
Santa Lucia (Pob.)	Bgy
Santiago (Pob.)	Bgy
Tawagan Sur	Bgy
Tiguma	Bgy
Tuburan (Pob.)	Bgy
Tulawas	Bgy
Tulangan	Bgy
Upper Sibatang	Bgy
White Beach	Bgy
Kawit	Bgy
Lumbia	Bgy
Santa Maria	Bgy
Santo Niño	Bgy
Dampalan	Bgy
Dumagoc	Bgy
Ramon Magsaysay	Mun
Bagong Opon	Bgy
Bambong Daku	Bgy
Bambong Diut	Bgy
Bobongan	Bgy
Campo IV	Bgy
Campo V	Bgy
Caniangan	Bgy
Dipalusan	Bgy
Eastern Bobongan	Bgy
Esperanza	Bgy
Gapasan	Bgy
Katipunan	Bgy
Kauswagan	Bgy
Lower Sambulawan	Bgy
Mabini	Bgy
Magsaysay	Bgy
Malating	Bgy
Paradise	Bgy
Pasingkalan	Bgy
Poblacion	Bgy
San Fernando	Bgy
Santo Rosario	Bgy
Sapa Anding	Bgy
Sinaguing	Bgy
Switch	Bgy
Upper Laperian	Bgy
Wakat	Bgy
San Miguel	Mun
Betinan	Bgy
Bulawan	Bgy
Calube	Bgy
Concepcion	Bgy
Dao-an	Bgy
Dumalian	Bgy
Fatima	Bgy
Langilan	Bgy
Lantawan	Bgy
Laperian	Bgy
Libuganan	Bgy
Limonan	Bgy
Mati	Bgy
Ocapan	Bgy
Poblacion	Bgy
San Isidro	Bgy
Sayog	Bgy
Tapian	Bgy
San Pablo	Mun
Bag-ong Misamis	Bgy
Bubual	Bgy
Buton	Bgy
Culasian	Bgy
Daplayan	Bgy
Kalilangan	Bgy
Kapamanok	Bgy
Kondum	Bgy
Lumbayao	Bgy
Mabuhay	Bgy
Marcos Village	Bgy
Miasin	Bgy
Molansong	Bgy
Pantad	Bgy
Pao	Bgy
Payag	Bgy
Poblacion	Bgy
Pongapong	Bgy
Sagasan	Bgy
Sacbulan	Bgy
Senior	Bgy
Songgoy	Bgy
Tandubuay	Bgy
Ticala Island	Bgy
Taniapan	Bgy
Tubo-pait	Bgy
Villakapa	Bgy
San Juan	Bgy
Tabina	Mun
Abong-abong	Bgy
Baganian	Bgy
Baya-baya	Bgy
Capisan	Bgy
Concepcion	Bgy
Culabay	Bgy
Lumbia	Bgy
Mabuhay	Bgy
Malim	Bgy
Manikaan	Bgy
New Oroquieta	Bgy
Poblacion	Bgy
San Francisco	Bgy
Tultolan	Bgy
Doña Josefina	Bgy
Tambulig	Mun
Alang-alang	Bgy
Bag-ong Kauswagan	Bgy
Bag-ong Tabogon	Bgy
Balugo	Bgy
Fabian	Bgy
Cabgan	Bgy
Calolot	Bgy
Dimalinao	Bgy
Gabunon	Bgy
Happy Valley (Pob.)	Bgy
Libato	Bgy
Limamawan	Bgy
Lower Liasan	Bgy
Lower Lodiong (Pob.)	Bgy
Lower Tiparak	Bgy
Lower Usogan	Bgy
Maya-maya	Bgy
New Village (Pob.)	Bgy
Pelocoban	Bgy
Riverside (Pob.)	Bgy
Sagrada Familia	Bgy
San Jose	Bgy
Sumalig	Bgy
Tuluan	Bgy
Tungawan	Bgy
Upper Liason	Bgy
Upper Lodiong	Bgy
Upper Tiparak	Bgy
Angeles	Bgy
Kapalaran	Bgy
San Vicente	Bgy
Tukuran	Mun
Alindahaw	Bgy
Baclay	Bgy
Balimbingan	Bgy
Buenasuerte	Bgy
Camanga	Bgy
Curvada	Bgy
Laperian	Bgy
Libertad	Bgy
Lower Bayao	Bgy
Luy-a	Bgy
Manilan	Bgy
Manlayag	Bgy
Militar	Bgy
Navalan	Bgy
Panduma Senior	Bgy
Sambulawan	Bgy
San Antonio	Bgy
San Carlos (Pob.)	Bgy
Santo Niño (Pob.)	Bgy
Santo Rosario	Bgy
Sugod	Bgy
Tabuan	Bgy
Tagulo	Bgy
Tinotungan	Bgy
Upper Bayao	Bgy
City of Zamboanga	City
Arena Blanco	Bgy
Ayala	Bgy
Baliwasan	Bgy
Baluno	Bgy
Boalan	Bgy
Bolong	Bgy
Buenavista	Bgy
Bunguiao	Bgy
Busay	Bgy
Cabaluay	Bgy
Cabatangan	Bgy
Cacao	Bgy
Calabasa	Bgy
Calarian	Bgy
Campo Islam	Bgy
Canelar	Bgy
Cawit	Bgy
Culianan	Bgy
Curuan	Bgy
Dita	Bgy
Divisoria	Bgy
Dulian	Bgy
Dulian	Bgy
Guisao	Bgy
Guiwan	Bgy
La Paz	Bgy
Labuan	Bgy
Lamisahan	Bgy
Landang Gua	Bgy
Landang Laum	Bgy
Lanzones	Bgy
Lapakan	Bgy
Latuan	Bgy
Limaong	Bgy
Limpapa	Bgy
Lubigan	Bgy
Lumayang	Bgy
Lumbangan	Bgy
Lunzuran	Bgy
Maasin	Bgy
Malagutay	Bgy
Mampang	Bgy
Manalipa	Bgy
Mangusu	Bgy
Manicahan	Bgy
Mariki	Bgy
Mercedes	Bgy
Muti	Bgy
Pamucutan	Bgy
Pangapuyan	Bgy
Panubigan	Bgy
Pasilmanta	Bgy
Pasonanca	Bgy
Patalon	Bgy
Barangay Zone I (Pob.)	Bgy
Barangay Zone II (Pob.)	Bgy
Barangay Zone III (Pob.)	Bgy
Barangay Zone IV (Pob.)	Bgy
Putik	Bgy
Quiniput	Bgy
Recodo	Bgy
Rio Hondo	Bgy
Salaan	Bgy
San Jose Cawa-cawa	Bgy
San Jose Gusu	Bgy
San Roque	Bgy
Sangali	Bgy
Santa Barbara	Bgy
Santa Catalina	Bgy
Santa Maria	Bgy
Santo Niño	Bgy
Sibulao	Bgy
Sinubung	Bgy
Sinunoc	Bgy
Tagasilay	Bgy
Taguiti	Bgy
Talabaan	Bgy
Talisayan	Bgy
Talon-talon	Bgy
Taluksangay	Bgy
Tetuan	Bgy
Tictapul	Bgy
Tigbalabag	Bgy
Tigtabon	Bgy
Tolosa	Bgy
Tugbungan	Bgy
Tulungatung	Bgy
Tumaga	Bgy
Tumalutab	Bgy
Tumitus	Bgy
Vitali	Bgy
Capisan	Bgy
Camino Nuevo	Bgy
Licomo	Bgy
Kasanyangan	Bgy
Pasobolong	Bgy
Victoria	Bgy
Zambowood	Bgy
Lakewood	Mun
Baking	Bgy
Bagong Kahayag	Bgy
Biswangan	Bgy
Bululawan	Bgy
Dagum	Bgy
Gasa	Bgy
Gatub	Bgy
Poblacion	Bgy
Lukuan	Bgy
Matalang	Bgy
Sapang Pinoles	Bgy
Sebuguey	Bgy
Tiwales	Bgy
Tubod	Bgy
Josefina	Mun
Bogo Calabat	Bgy
Dawa	Bgy
Ebarle	Bgy
Upper Bagong Tudela (Pob.)	Bgy
Leonardo	Bgy
Litapan	Bgy
Lower Bagong Tudela	Bgy
Mansanas	Bgy
Moradji	Bgy
Nemeño	Bgy
Nopulan	Bgy
Sebukang	Bgy
Tagaytay Hill	Bgy
Gumahan (Pob.)	Bgy
Pitogo	Mun
Balabawan	Bgy
Balong-balong	Bgy
Colojo	Bgy
Liasan	Bgy
Liguac	Bgy
Limbayan	Bgy
Lower Paniki-an	Bgy
Matin-ao	Bgy
Panubigan	Bgy
Poblacion	Bgy
Punta Flecha	Bgy
Sugbay Dos	Bgy
Tongao	Bgy
Upper Paniki-an	Bgy
San Isidro	Bgy
Sominot	Mun
Bag-ong Baroy	Bgy
Bag-ong Oroquieta	Bgy
Barubuhan	Bgy
Bulanay	Bgy
Datagan	Bgy
Eastern Poblacion	Bgy
Lantawan	Bgy
Libertad	Bgy
Lumangoy	Bgy
New Carmen	Bgy
Picturan	Bgy
Poblacion	Bgy
Rizal	Bgy
San Miguel	Bgy
Santo Niño	Bgy
Sawa	Bgy
Tungawan	Bgy
Upper Sicpao	Bgy
Vincenzo A. Sagun	Mun
Bui-os	Bgy
Cogon	Bgy
Danan	Bgy
Kabatan	Bgy
Kapatagan	Bgy
Limason	Bgy
Linoguayan	Bgy
Lumbal	Bgy
Lunib	Bgy
Maculay	Bgy
Maraya	Bgy
Sagucan	Bgy
Waling-waling	Bgy
Ambulon	Bgy
Guipos	Mun
Bagong Oroquieta	Bgy
Baguitan	Bgy
Balongating	Bgy
Canunan	Bgy
Dacsol	Bgy
Dagohoy	Bgy
Dalapang	Bgy
Datagan	Bgy
Poblacion	Bgy
Guling	Bgy
Katipunan	Bgy
Lintum	Bgy
Litan	Bgy
Magting	Bgy
Regla	Bgy
Sikatuna	Bgy
Singclot	Bgy
Tigbao	Mun
Begong	Bgy
Busol	Bgy
Caluma	Bgy
Diana Countryside	Bgy
Guinlin	Bgy
Lacarayan	Bgy
Lacupayan	Bgy
Libayoy	Bgy
Limas	Bgy
Longmot	Bgy
Maragang	Bgy
Mate	Bgy
Nangan-nangan	Bgy
New Tuburan	Bgy
Nilo	Bgy
Tigbao	Bgy
Timolan	Bgy
Upper Nilo	Bgy
Zamboanga Sibugay	Prov
Alicia	Mun
Alegria	Bgy
Milagrosa	Bgy
Bella	Bgy
Calades	Bgy
Dawa-dawa	Bgy
Gulayon	Bgy
Ilisan	Bgy
Kawayan	Bgy
Kauswagan	Bgy
La Paz	Bgy
Lambuyogan	Bgy
Lapirawan	Bgy
Litayon	Bgy
Lutiman	Bgy
Naga-naga	Bgy
Pandan-pandan	Bgy
Payongan	Bgy
Poblacion	Bgy
Santa Maria	Bgy
Santo Niño	Bgy
Talaptap	Bgy
Tampalan	Bgy
Tandiong Muslim	Bgy
Timbang-timbang	Bgy
Bagong Buhay	Bgy
Concepcion	Bgy
Kapatagan	Bgy
Buug	Mun
Basalem	Bgy
Bawang	Bgy
Bulaan	Bgy
Compostela	Bgy
Del Monte	Bgy
Guitom	Bgy
Guminta	Bgy
Labrador	Bgy
Lantawan	Bgy
Mabuhay	Bgy
Maganay	Bgy
Manlin	Bgy
Muyo	Bgy
Pamintayan	Bgy
Poblacion	Bgy
Talamimi	Bgy
Villacastor	Bgy
Agutayan	Bgy
Bagong Borbon	Bgy
Bliss	Bgy
Danlugan	Bgy
Pling	Bgy
Pulog	Bgy
San Jose	Bgy
Talairan	Bgy
Datu Panas	Bgy
Guintuloan	Bgy
Diplahan	Mun
Balangao	Bgy
Pilar	Bgy
Poblacion	Bgy
Ditay	Bgy
Gaulan	Bgy
Goling	Bgy
Guinoman	Bgy
Kauswagan	Bgy
Lindang	Bgy
Lobing	Bgy
Paradise	Bgy
Sampoli A	Bgy
Songcuya	Bgy
Santa Cruz	Bgy
Butong	Bgy
Sampoli B	Bgy
Luop	Bgy
Manangon	Bgy
Mejo	Bgy
Natan	Bgy
Tinongtongan	Bgy
Tuno	Bgy
Imelda	Mun
Lower Baluran	Bgy
Baluyan	Bgy
Cana-an	Bgy
Dumpoc	Bgy
Gandiangan	Bgy
Israel	Bgy
La Victoria	Bgy
Lumpanac	Bgy
Little Baguio	Bgy
Lumbog	Bgy
Mali Little Baguio	Bgy
Poblacion	Bgy
San Jose	Bgy
Santa Barbara	Bgy
Balugo	Bgy
Balungisan	Bgy
Pulawan	Bgy
Upper Baluran	Bgy
Ipil (Capital)	Mun
Bacalan	Bgy
Bangkerohan	Bgy
Bulu-an	Bgy
Don Andres	Bgy
Guituan	Bgy
Ipil Heights	Bgy
Labi	Bgy
Lower Ipil Heights	Bgy
Lower Taway	Bgy
Lumbia	Bgy
Magdaup	Bgy
Pangi	Bgy
Poblacion	Bgy
Sanito	Bgy
Suclema	Bgy
Taway	Bgy
Tenan	Bgy
Tiayon	Bgy
Timalang	Bgy
Tomitom	Bgy
Upper Pangi	Bgy
Veteran's Village	Bgy
Makilas	Bgy
Caparan	Bgy
Domandan	Bgy
Doña Josefa	Bgy
Logan	Bgy
Maasin	Bgy
Kabasalan	Mun
Banker	Bgy
Bolo Batallion	Bgy
Buayan	Bgy
Cainglet	Bgy
Calapan	Bgy
Calubihan	Bgy
Concepcion	Bgy
Dipala	Bgy
Gacbusan	Bgy
Goodyear	Bgy
Lacnapan	Bgy
Little Baguio	Bgy
Lumbayao	Bgy
Timuay Danda	Bgy
Nazareth	Bgy
Palinta	Bgy
Peñaranda	Bgy
Poblacion	Bgy
Riverside	Bgy
Sanghanan	Bgy
Santa Cruz	Bgy
Sayao	Bgy
Simbol	Bgy
Sininan	Bgy
Shiolan	Bgy
Tampilisan	Bgy
Tamin	Bgy
Tigbangagan	Bgy
Diampak	Bgy
Mabuhay	Mun
Abunda	Bgy
Bagong Silang	Bgy
Bangkaw-bangkaw	Bgy
Caliran	Bgy
Catipan	Bgy
Tandu-Comot	Bgy
Kauswagan	Bgy
Ligaya	Bgy
Looc-Barlak	Bgy
Malinao	Bgy
Pamansaan	Bgy
Pinalim	Bgy
Poblacion	Bgy
Punawan	Bgy
Santo Niño	Bgy
Sawa	Bgy
Sioton	Bgy
Taguisian	Bgy
Malangas	Mun
Bacao	Bgy
Basak-bawang	Bgy
Camanga	Bgy
Candiis	Bgy
Catituan	Bgy
Dansulao	Bgy
Del Pilar	Bgy
Guilawa	Bgy
Kigay	Bgy
La Dicha	Bgy
Lipacan	Bgy
Logpond	Bgy
Mabini	Bgy
Malungon	Bgy
Mulom	Bgy
Overland	Bgy
Palalian	Bgy
Payag	Bgy
Poblacion	Bgy
Rebocon	Bgy
San Vicente	Bgy
Sinusayan	Bgy
Tackling	Bgy
Tigabon	Bgy
Bontong	Bgy
Naga	Mun
Aguinaldo	Bgy
Baga	Bgy
Baluno	Bgy
Cabong	Bgy
Gubawang	Bgy
Kaliantana	Bgy
La Paz	Bgy
Lower Sulitan	Bgy
Mamagon	Bgy
Marsolo	Bgy
Poblacion	Bgy
San Isidro	Bgy
Santa Clara	Bgy
Crossing Sta. Clara	Bgy
Sulo	Bgy
Tambanan	Bgy
Taytay Manubo	Bgy
Upper Sulitan	Bgy
Bangkaw-bangkaw	Bgy
Guintoloan	Bgy
Sandayong	Bgy
Tilubog	Bgy
Tipan	Bgy
Olutanga	Mun
Bateria	Bgy
Kahayagan	Bgy
Calais	Bgy
Esperanza	Bgy
Fama	Bgy
Galas	Bgy
Gandaan	Bgy
Looc Sapi	Bgy
Matim	Bgy
Noque	Bgy
Solar (Pob.)	Bgy
Pulo Laum	Bgy
Pulo Mabao	Bgy
San Isidro	Bgy
Santa Maria	Bgy
Tambanan	Bgy
Villacorte	Bgy
Villagonzalo	Bgy
San Jose	Bgy
Payao	Mun
Balian	Bgy
Balungisan	Bgy
Bulacan	Bgy
Bulawan	Bgy
Calape	Bgy
Dalama	Bgy
Fatima	Bgy
Guintolan	Bgy
Katipunan	Bgy
Kima	Bgy
Kulasian	Bgy
Labatan	Bgy
Mountain View	Bgy
Nanan	Bgy
Poblacion	Bgy
San Roque	Bgy
San Vicente	Bgy
Mayabo	Bgy
Minundas	Bgy
Talaptap	Bgy
Balogo	Bgy
Binangonan	Bgy
Guiwan	Bgy
Kulisap	Bgy
La Fortuna	Bgy
San Isidro	Bgy
Silal	Bgy
Sumilong	Bgy
Upper Sumilong	Bgy
Roseller Lim	Mun
Balansag	Bgy
Calula	Bgy
Casacon	Bgy
Gango	Bgy
Katipunan	Bgy
Kulambugan	Bgy
Mabini	Bgy
Magsaysay	Bgy
New Antique	Bgy
New Sagay	Bgy
Pres. Roxas	Bgy
San Antonio	Bgy
San Fernandino	Bgy
San Jose	Bgy
Siawang	Bgy
Silingan	Bgy
Surabay	Bgy
Tilasan	Bgy
Tupilac	Bgy
Ali Alsree	Bgy
Don Perfecto	Bgy
Malubal	Bgy
Palmera	Bgy
Remedios	Bgy
Santo Rosario	Bgy
Taruc	Bgy
Siay	Mun
Balucanan	Bgy
Balagon	Bgy
Balingasan	Bgy
Batu	Bgy
Buyogan	Bgy
Camanga	Bgy
Coloran	Bgy
Kimos	Bgy
Labasan	Bgy
Laih	Bgy
Magsaysay	Bgy
Mahayahay	Bgy
Maligaya	Bgy
Maniha	Bgy
Minsulao	Bgy
Mirangan	Bgy
Monching	Bgy
Paruk	Bgy
Poblacion	Bgy
Princesa Sumama	Bgy
Sibuguey	Bgy
Bagong Silang	Bgy
Bataan	Bgy
Lagting	Bgy
Logpond	Bgy
Salinding	Bgy
San Isidro	Bgy
Siloh	Bgy
Villagracia	Bgy
Talusan	Mun
Aurora	Bgy
Baganipay	Bgy
Bolingan	Bgy
Bualan	Bgy
Cawilan	Bgy
Florida	Bgy
Kasigpitan	Bgy
Laparay	Bgy
Mahayahay	Bgy
Moalboal	Bgy
Sagay	Bgy
Samonte	Bgy
Poblacion	Bgy
Tuburan	Bgy
Titay	Mun
Achasol	Bgy
Bangco	Bgy
Camanga	Bgy
Culasian	Bgy
Dalangin	Bgy
Dalisay	Bgy
Gomotoc	Bgy
Imelda	Bgy
Kipit	Bgy
Kitabog	Bgy
La Libertad	Bgy
Longilog	Bgy
Mabini	Bgy
Malagandis	Bgy
Mate	Bgy
Moalboal	Bgy
Namnama	Bgy
New Canaan	Bgy
Palomoc	Bgy
Poblacion	Bgy
Pulidan	Bgy
San Antonio	Bgy
Santa Fe	Bgy
Supit	Bgy
Tugop	Bgy
Tugop Muslim	Bgy
Dalangin Muslim	Bgy
Poblacion Muslim	Bgy
Azusano	Bgy
San Isidro	Bgy
Tungawan	Mun
Baluran	Bgy
Cayamcam	Bgy
Langon	Bgy
Libertad (Pob.)	Bgy
Linguisan	Bgy
Looc-labuan	Bgy
Lower Tungawan	Bgy
Malungon	Bgy
San Pedro	Bgy
Tigbanuang	Bgy
Tigbucay	Bgy
Upper Tungawan	Bgy
San Vicente	Bgy
Batungan	Bgy
Loboc	Bgy
San Isidro	Bgy
Datu Tumanggong	Bgy
Sisay	Bgy
Gaycon	Bgy
Little Margos	Bgy
Santo Niño	Bgy
Taglibas	Bgy
Tigpalay	Bgy
Masao	Bgy
Timbabauan	Bgy
City of Isabela (Not a Province)	
City of Isabela	City
Aguada	Bgy
Balatanay	Bgy
Baluno	Bgy
Begang	Bgy
Binuangan	Bgy
Busay	Bgy
Cabunbata	Bgy
Calvario	Bgy
Carbon	Bgy
Diki	Bgy
Isabela Eastside (Pob.)	Bgy
Isabela Proper (Pob.)	Bgy
Dona Ramona T. Alano	Bgy
Kapatagan Grande	Bgy
Kaumpurnah Zone I	Bgy
Kaumpurnah Zone II	Bgy
Kaumpurnah Zone III	Bgy
Kumalarang	Bgy
La Piedad (Pob.)	Bgy
Lampinigan	Bgy
Lanote	Bgy
Lukbuton	Bgy
Lumbang	Bgy
Makiri	Bgy
Maligue	Bgy
Marang-marang	Bgy
Marketsite (Pob.)	Bgy
Menzi	Bgy
Panigayan	Bgy
Panunsulan	Bgy
Port Area (Pob.)	Bgy
Riverside	Bgy
San Rafael	Bgy
Santa Barbara	Bgy
Santa Cruz (Pob.)	Bgy
Seaside (Pob.)	Bgy
Sumagdang	Bgy
Sunrise Village (Pob.)	Bgy
Tabiawan	Bgy
Tabuk (Pob.)	Bgy
Timpul	Bgy
Kapayawan	Bgy
Masula	Bgy
Small Kapatagan	Bgy
Tampalan	Bgy
Region X (Northern Mindanao)	Reg
Bukidnon	Prov
Baungon	Mun
Balintad	Bgy
Buenavista	Bgy
Danatag	Bgy
Kalilangan	Bgy
Lacolac	Bgy
Langaon	Bgy
Liboran	Bgy
Lingating	Bgy
Mabuhay	Bgy
Mabunga	Bgy
Nicdao	Bgy
Imbatug (Pob.)	Bgy
Pualas	Bgy
Salimbalan	Bgy
San Vicente	Bgy
San Miguel	Bgy
Damulog	Mun
Aludas	Bgy
Angga-an	Bgy
Tangkulan	Bgy
Kinapat	Bgy
Kiraon	Bgy
Kitingting	Bgy
Lagandang	Bgy
Macapari	Bgy
Maican	Bgy
Migcawayan	Bgy
New Compostela	Bgy
Old Damulog	Bgy
Omonay	Bgy
Poblacion	Bgy
Pocopoco	Bgy
Sampagar	Bgy
San Isidro	Bgy
Dangcagan	Mun
Barongcot	Bgy
Bugwak	Bgy
Dolorosa	Bgy
Kapalaran	Bgy
Kianggat	Bgy
Lourdes	Bgy
Macarthur	Bgy
Miaray	Bgy
Migcuya	Bgy
New Visayas	Bgy
Osmeña	Bgy
Poblacion	Bgy
Sagbayan	Bgy
San Vicente	Bgy
Don Carlos	Mun
Cabadiangan	Bgy
Bocboc	Bgy
Buyot	Bgy
Calaocalao	Bgy
Don Carlos Norte	Bgy
Embayao	Bgy
Kalubihon	Bgy
Kasigkot	Bgy
Kawilihan	Bgy
Kiara	Bgy
Kibatang	Bgy
Mahayahay	Bgy
Manlamonay	Bgy
Maraymaray	Bgy
Mauswagon	Bgy
Minsalagan	Bgy
New Nongnongan	Bgy
New Visayas	Bgy
Old Nongnongan	Bgy
Pinamaloy	Bgy
Don Carlos Sur (Pob.)	Bgy
Pualas	Bgy
San Antonio East	Bgy
San Antonio West	Bgy
San Francisco	Bgy
San Nicolas	Bgy
San Roque	Bgy
Sinangguyan	Bgy
Bismartz	Bgy
Impasug-ong	Mun
Bontongon	Bgy
Bulonay	Bgy
Capitan Bayong	Bgy
Cawayan	Bgy
Dumalaguing	Bgy
Guihean	Bgy
Hagpa	Bgy
Impalutao	Bgy
Kalabugao	Bgy
Kibenton	Bgy
La Fortuna	Bgy
Poblacion	Bgy
Sayawan	Bgy
Kadingilan	Mun
Bagongbayan	Bgy
Bagor	Bgy
Balaoro	Bgy
Baroy	Bgy
Cabadiangan	Bgy
Husayan	Bgy
Kibalagon	Bgy
Mabuhay	Bgy
Malinao	Bgy
Matampay	Bgy
Sibonga	Bgy
Pay-as	Bgy
Pinamanguhan	Bgy
Poblacion	Bgy
Salvacion	Bgy
San Andres	Bgy
Kibogtok	Bgy
Kalilangan	Mun
Bangbang	Bgy
Baborawon	Bgy
Canituan	Bgy
Kibaning	Bgy
Kinura	Bgy
Lampanusan	Bgy
Maca-opao	Bgy
Malinao	Bgy
Pamotolon	Bgy
Poblacion	Bgy
Public	Bgy
Ninoy Aquino	Bgy
San Vicente Ferrer	Bgy
West Poblacion	Bgy
Kibawe	Mun
Balintawak	Bgy
Cagawasan	Bgy
East Kibawe (Pob.)	Bgy
Gutapol	Bgy
Pinamula	Bgy
Kiorao	Bgy
Kisawa	Bgy
Labuagon	Bgy
Magsaysay	Bgy
Marapangi	Bgy
Mascariñas	Bgy
Natulongan	Bgy
New Kidapawan	Bgy
Old Kibawe	Bgy
Romagooc	Bgy
Sampaguita	Bgy
Sanipon	Bgy
Spring	Bgy
Talahiron	Bgy
Tumaras	Bgy
West Kibawe (Pob.)	Bgy
Bukang Liwayway	Bgy
Palma	Bgy
Kitaotao	Mun
Balangigay	Bgy
Balukbukan	Bgy
Bershiba	Bgy
Bobong	Bgy
Bolocaon	Bgy
Cabalantian	Bgy
Calapaton	Bgy
Sinaysayan	Bgy
Kahusayan	Bgy
Kalumihan	Bgy
Kauyonan	Bgy
Kimolong	Bgy
Kitaihon	Bgy
Kitobo	Bgy
Magsaysay	Bgy
Malobalo	Bgy
Metebagao	Bgy
Sagundanon	Bgy
Pagan	Bgy
Panganan	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Lorenzo	Bgy
Santo Rosario	Bgy
Sinuda	Bgy
Tandong	Bgy
Tawas	Bgy
White Kulaman	Bgy
Napalico	Bgy
Digongan	Bgy
Kiulom	Bgy
Binoongan	Bgy
Kipilas	Bgy
East Dalurong	Bgy
West Dalurong	Bgy
Lantapan	Mun
Alanib	Bgy
Baclayon	Bgy
Balila	Bgy
Bantuanon	Bgy
Basak	Bgy
Bugcaon	Bgy
Ka-atoan	Bgy
Capitan Juan	Bgy
Cawayan	Bgy
Kulasihan	Bgy
Kibangay	Bgy
Poblacion	Bgy
Songco	Bgy
Victory	Bgy
Libona	Mun
Capihan	Bgy
Crossing	Bgy
Gango	Bgy
Kiliog	Bgy
Kinawe	Bgy
Laturan	Bgy
Maambong	Bgy
Nangka	Bgy
Palabucan	Bgy
Poblacion	Bgy
Pongol	Bgy
San Jose	Bgy
Santa Fe	Bgy
Sil-ipon	Bgy
City of Malaybalay (Capital)	City
Aglayan	Bgy
Bangcud	Bgy
Busdi	Bgy
Cabangahan	Bgy
Caburacanan	Bgy
Canayan	Bgy
Capitan Angel	Bgy
Casisang	Bgy
Dalwangan	Bgy
Imbayao	Bgy
Indalaza	Bgy
Kalasungay	Bgy
Kabalabag	Bgy
Kulaman	Bgy
Laguitas	Bgy
Patpat	Bgy
Linabo	Bgy
Apo Macote	Bgy
Miglamin	Bgy
Magsaysay	Bgy
Maligaya	Bgy
Managok	Bgy
Manalog	Bgy
Mapayag	Bgy
Mapulo	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Saint Peter	Bgy
San Jose	Bgy
San Martin	Bgy
Santo Niño	Bgy
Silae	Bgy
Simaya	Bgy
Sinanglanan	Bgy
Sumpong	Bgy
Violeta	Bgy
Zamboanguita	Bgy
Malitbog	Mun
Kalingking	Bgy
Kiabo	Bgy
Mindagat	Bgy
Omagling	Bgy
Patpat	Bgy
Poblacion	Bgy
Sampiano	Bgy
San Luis	Bgy
Santa Ines	Bgy
Silo-o	Bgy
Sumalsag	Bgy
Manolo Fortich	Mun
Agusan Canyon	Bgy
Alae	Bgy
Dahilayan	Bgy
Dalirig	Bgy
Damilag	Bgy
Diclum	Bgy
Guilang-guilang	Bgy
Kalugmanan	Bgy
Lindaban	Bgy
Lingion	Bgy
Lunocan	Bgy
Maluko	Bgy
Mambatangan	Bgy
Mampayag	Bgy
Minsuro	Bgy
Mantibugao	Bgy
Tankulan (Pob.)	Bgy
San Miguel	Bgy
Sankanan	Bgy
Santiago	Bgy
Santo Niño	Bgy
Ticala	Bgy
Maramag	Mun
Anahawon	Bgy
Base Camp	Bgy
Bayabason	Bgy
Camp I	Bgy
Colambugan	Bgy
Dagumba-an	Bgy
Danggawan	Bgy
Dologon	Bgy
Kisanday	Bgy
Kuya	Bgy
La Roxas	Bgy
Panadtalan	Bgy
Panalsalan	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
San Miguel	Bgy
San Roque	Bgy
Tubigon	Bgy
Bagongsilang	Bgy
Kiharong	Bgy
Pangantucan	Mun
Adtuyon	Bgy
Bacusanon	Bgy
Bangahan	Bgy
Barandias	Bgy
Concepcion	Bgy
Gandingan	Bgy
Kimanait	Bgy
Kipadukan	Bgy
Langcataon	Bgy
Lantay	Bgy
Madaya	Bgy
Malipayon	Bgy
Mendis	Bgy
Nabaliwa	Bgy
New Eden	Bgy
Payad	Bgy
Pigtauranan	Bgy
Poblacion	Bgy
Portulin	Bgy
Quezon	Mun
Butong	Bgy
Cebole	Bgy
Delapa	Bgy
Dumalama	Bgy
C-Handumanan	Bgy
Cawayan	Bgy
Kiburiao	Bgy
Kipaypayon	Bgy
Libertad	Bgy
Linabo	Bgy
Lipa	Bgy
Lumintao	Bgy
Magsaysay	Bgy
Mahayag	Bgy
Manuto	Bgy
Merangerang	Bgy
Mibantang	Bgy
Minongan	Bgy
Minsamongan	Bgy
Paitan	Bgy
Palacapao	Bgy
Pinilayan	Bgy
Poblacion	Bgy
Puntian	Bgy
Salawagan	Bgy
San Isidro	Bgy
San Jose	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santa Filomena	Bgy
Minsalirak	Bgy
San Fernando	Mun
Bonacao	Bgy
Cabuling	Bgy
Kawayan	Bgy
Cayaga	Bgy
Dao	Bgy
Durian	Bgy
Iglugsad	Bgy
Kalagangan	Bgy
Kibongcog	Bgy
Little Baguio	Bgy
Nacabuklad	Bgy
Namnam	Bgy
Palacpacan	Bgy
Halapitan (Pob.)	Bgy
San Jose	Bgy
Santo Domingo	Bgy
Tugop	Bgy
Matupe	Bgy
Bulalang	Bgy
Candelaria	Bgy
Mabuhay	Bgy
Magkalungay	Bgy
Malayanan	Bgy
Sacramento Valley	Bgy
Sumilao	Mun
Kisolon	Bgy
Culasi	Bgy
Licoan	Bgy
Lupiagan	Bgy
Ocasion	Bgy
Puntian	Bgy
San Roque	Bgy
San Vicente	Bgy
Poblacion	Bgy
Vista Villa	Bgy
Talakag	Mun
Basak	Bgy
Baylanan	Bgy
Cacaon	Bgy
Colawingon	Bgy
Cosina	Bgy
Dagumbaan	Bgy
Dagundalahon	Bgy
Dominorog	Bgy
Lapok	Bgy
Indulang	Bgy
Lantud	Bgy
Liguron	Bgy
Lingi-on	Bgy
Lirongan	Bgy
Santo Niño	Bgy
Miarayon	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Sagaran	Bgy
Salucot	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Miguel	Bgy
San Rafael	Bgy
Tagbak	Bgy
Tikalaan	Bgy
City of Valencia	City
Bagontaas	Bgy
Banlag	Bgy
Barobo	Bgy
Batangan	Bgy
Catumbalon	Bgy
Colonia	Bgy
Concepcion	Bgy
Dagat-Kidavao	Bgy
Guinoyuran	Bgy
Kahapunan	Bgy
Laligan	Bgy
Lilingayon	Bgy
Lourdes	Bgy
Lumbayao	Bgy
Lumbo	Bgy
Lurogan	Bgy
Maapag	Bgy
Mabuhay	Bgy
Mailag	Bgy
Mt. Nebo	Bgy
Nabago	Bgy
Pinatilan	Bgy
Poblacion	Bgy
San Carlos	Bgy
San Isidro	Bgy
Sinabuagan	Bgy
Sinayawan	Bgy
Sugod	Bgy
Tongantongan	Bgy
Tugaya	Bgy
Vintar	Bgy
Cabanglasan	Mun
Cabulohan	Bgy
Canangaan	Bgy
Iba	Bgy
Imbatug	Bgy
Lambangan	Bgy
Mandaing	Bgy
Paradise	Bgy
Poblacion	Bgy
Anlogan	Bgy
Capinonan	Bgy
Dalacutan	Bgy
Freedom	Bgy
Mandahikan	Bgy
Mauswagon	Bgy
Jasaan	Bgy
Camiguin	Prov
Catarman	Mun
Alga	Bgy
Bonbon	Bgy
Bura	Bgy
Catibac	Bgy
Compol	Bgy
Lawigan	Bgy
Liloan	Bgy
Looc	Bgy
Mainit	Bgy
Manduao	Bgy
Panghiawan	Bgy
Poblacion	Bgy
Santo Niño	Bgy
Tangaro	Bgy
Guinsiliban	Mun
Butay	Bgy
Cabuan	Bgy
Cantaan	Bgy
Liong	Bgy
Maac	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Mahinog	Mun
Benoni	Bgy
Binatubo	Bgy
Catohugan	Bgy
Hubangon	Bgy
Owakan	Bgy
Poblacion	Bgy
Puntod	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Roque	Bgy
Tubod	Bgy
Tupsan Pequeño	Bgy
Mambajao (Capital)	Mun
Agoho	Bgy
Anito	Bgy
Balbagon	Bgy
Baylao	Bgy
Benhaan	Bgy
Bug-ong	Bgy
Kuguita	Bgy
Magting	Bgy
Naasag	Bgy
Pandan	Bgy
Poblacion	Bgy
Soro-soro	Bgy
Tagdo	Bgy
Tupsan	Bgy
Yumbing	Bgy
Sagay	Mun
Alangilan	Bgy
Bacnit	Bgy
Balite	Bgy
Bonbon	Bgy
Bugang	Bgy
Cuna	Bgy
Manuyog	Bgy
Mayana	Bgy
Poblacion	Bgy
Lanao del Norte	Prov
Bacolod	Mun
Alegria	Bgy
Babalaya	Bgy
Babalayan Townsite	Bgy
Binuni	Bgy
Demologan	Bgy
Dimarao	Bgy
Esperanza	Bgy
Kahayag	Bgy
Liangan East	Bgy
Punod	Bgy
Mati	Bgy
Minaulon	Bgy
Pagayawan	Bgy
Poblacion Bacolod	Bgy
Rupagan	Bgy
Delabayan West	Bgy
Baloi	Mun
Abaga	Bgy
Adapun-Ali	Bgy
Angandog	Bgy
Angayen	Bgy
Bangko	Bgy
Batolacongan	Bgy
Buenavista	Bgy
Cadayonan	Bgy
Landa	Bgy
Lumbac	Bgy
Mamaanun	Bgy
Maria-Cristina	Bgy
Matampay	Bgy
Nangka	Bgy
Pacalundo	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
Sandor	Bgy
Sangcad	Bgy
Sarip-Alawi	Bgy
Sigayan	Bgy
Baroy	Mun
Andil	Bgy
Bagong Dawis	Bgy
Baroy Daku	Bgy
Bato	Bgy
Cabasagan	Bgy
Dalama	Bgy
Libertad	Bgy
Limwag	Bgy
Lindongan	Bgy
Maliwanag	Bgy
Manan-ao	Bgy
Pange	Bgy
Pindolonan	Bgy
Poblacion	Bgy
Princesa	Bgy
Rawan Point	Bgy
Riverside	Bgy
Sagadan	Bgy
Salong	Bgy
Tinubdan	Bgy
Sagadan Upper	Bgy
San Juan	Bgy
Village	Bgy
City of Iligan	City
Abuno	Bgy
Bonbonon	Bgy
Bunawan	Bgy
Buru-un	Bgy
Dalipuga	Bgy
Digkilaan	Bgy
Hinaplanon	Bgy
Kabacsanan	Bgy
Kiwalan	Bgy
Mahayhay	Bgy
Mainit	Bgy
Mandulog	Bgy
Maria Cristina	Bgy
Palao	Bgy
Poblacion	Bgy
Puga-an	Bgy
Rogongon	Bgy
Santa Elena	Bgy
Santa Filomena	Bgy
Suarez	Bgy
Tambacan	Bgy
Saray-Tibanga	Bgy
Tipanoy	Bgy
Tominobo Proper	Bgy
Tominobo Upper	Bgy
Tubod	Bgy
Bagong Silang	Bgy
Del Carmen	Bgy
Dulag	Bgy
San Miguel	Bgy
Santiago	Bgy
Santo Rosario	Bgy
Tibanga	Bgy
Acmac	Bgy
Ditucalan	Bgy
Hindang	Bgy
Kalilangan	Bgy
Lanipao	Bgy
Luinab	Bgy
Panoroganan	Bgy
San Roque	Bgy
Ubaldo Laya	Bgy
Upper Hinaplanon	Bgy
Villa Verde	Bgy
Kapatagan	Mun
Bagong Badian	Bgy
Bagong Silang	Bgy
Balili	Bgy
Bansarvil	Bgy
Belis	Bgy
Buenavista	Bgy
Butadon	Bgy
Cathedral Falls	Bgy
Concepcion	Bgy
Curvada	Bgy
De Asis	Bgy
Donggoan	Bgy
Durano	Bgy
Kahayagan	Bgy
Kidalos	Bgy
La Libertad	Bgy
Lapinig	Bgy
Mahayahay	Bgy
Malinas	Bgy
Maranding	Bgy
Margos	Bgy
Poblacion	Bgy
Pulang Yuta	Bgy
San Isidro	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santo Tomas	Bgy
Suso	Bgy
Taguitic	Bgy
Tiacongan	Bgy
Tipolo	Bgy
Tulatulahan	Bgy
Waterfalls	Bgy
Sultan Naga Dimaporo	Mun
Bangaan	Bgy
Bangco	Bgy
Bansarvil II	Bgy
Bauyan	Bgy
Cabongbongan	Bgy
Calibao	Bgy
Calipapa	Bgy
Calube	Bgy
Campo Islam	Bgy
Capocao	Bgy
Dabliston	Bgy
Dangulaan	Bgy
Ditago	Bgy
Ilian	Bgy
Kauswagan	Bgy
Kirapan	Bgy
Koreo	Bgy
Lantawan	Bgy
Mabuhay	Bgy
Maguindanao	Bgy
Mahayahay	Bgy
Mamagum	Bgy
Pandanan	Bgy
Payong	Bgy
Piraka	Bgy
Pikalawag	Bgy
Pikinit	Bgy
Poblacion	Bgy
Ramain	Bgy
Rebucon	Bgy
Sigayan	Bgy
Sugod	Bgy
Tagulo	Bgy
Tantaon	Bgy
Topocon	Bgy
Dalama	Bgy
Mina	Bgy
Kauswagan	Mun
Bagumbayan (Pob.)	Bgy
Bara-ason	Bgy
Cayontor	Bgy
Delabayan	Bgy
Inudaran	Bgy
Kawit Occidental	Bgy
Kawit Oriental	Bgy
Libertad	Bgy
Paiton	Bgy
Poblacion	Bgy
Tacub	Bgy
Tingintingin	Bgy
Tugar	Bgy
Kolambugan	Mun
Austin Heights	Bgy
Baybay	Bgy
Bubong	Bgy
Caromatan	Bgy
Inudaran	Bgy
Kulasihan	Bgy
Libertad	Bgy
Lumbac	Bgy
Manga	Bgy
Matampay	Bgy
Mukas	Bgy
Muntay	Bgy
Pagalungan	Bgy
Palao	Bgy
Pantaon	Bgy
Pantar	Bgy
Poblacion	Bgy
Rebucon	Bgy
Riverside	Bgy
San Roque	Bgy
Santo Niño	Bgy
Simbuco	Bgy
Small Banisilan	Bgy
Sucodan	Bgy
Tabigue	Bgy
Titunod	Bgy
Lala	Mun
Abaga	Bgy
Andil	Bgy
Matampay Bucana	Bgy
Darumawang Bucana	Bgy
Cabasagan	Bgy
Camalan	Bgy
Darumawang Ilaya	Bgy
El Salvador	Bgy
Gumagamot	Bgy
Lala Proper (Pob.)	Bgy
Lanipao	Bgy
Magpatao	Bgy
Maranding	Bgy
Matampay Ilaya	Bgy
Pacita	Bgy
Pendolonan	Bgy
Pinoyak	Bgy
Raw-an	Bgy
Rebe	Bgy
San Isidro Lower	Bgy
San Isidro Upper	Bgy
San Manuel	Bgy
Santa Cruz Lower	Bgy
Santa Cruz Upper	Bgy
Simpak	Bgy
Tenazas	Bgy
Tuna-an	Bgy
Linamon	Mun
Busque	Bgy
Larapan	Bgy
Magoong	Bgy
Napo	Bgy
Poblacion	Bgy
Purakan	Bgy
Robocon	Bgy
Samburon	Bgy
Magsaysay	Mun
Babasalon	Bgy
Baguiguicon	Bgy
Daan Campo	Bgy
Durianon	Bgy
Ilihan	Bgy
Lamigadato	Bgy
Lemoncret	Bgy
Lubo	Bgy
Lumbac	Bgy
Malabaogan	Bgy
Mapantao	Bgy
Olango	Bgy
Pangao	Bgy
Pelingkingan	Bgy
Lower Caningag	Bgy
Poblacion	Bgy
Rarab	Bgy
Somiorang	Bgy
Upper Caningag	Bgy
Talambo	Bgy
Tambacon	Bgy
Tawinian	Bgy
Tipaan	Bgy
Tombador	Bgy
Maigo	Mun
Balagatasa	Bgy
Camp 1	Bgy
Claro M. Recto	Bgy
Inoma	Bgy
Labuay	Bgy
Liangan West	Bgy
Mahayahay	Bgy
Maliwanag	Bgy
Mentring	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Segapod	Bgy
Kulasihan	Bgy
Matungao	Mun
Bubong Radapan	Bgy
Bangco	Bgy
Batal	Bgy
Batangan	Bgy
Cadayonan	Bgy
Matampay	Bgy
Pangi	Bgy
Pasayanon	Bgy
Poblacion	Bgy
Puntod	Bgy
Santa Cruz	Bgy
Somiorang	Bgy
Munai	Mun
Bacayawan	Bgy
Balabacun	Bgy
Balintad	Bgy
Kadayonan	Bgy
Dalama	Bgy
Lindongan	Bgy
Lingco-an	Bgy
Lininding	Bgy
Lumba-Bayabao	Bgy
Madaya	Bgy
Maganding	Bgy
Matampay	Bgy
Old Poblacion	Bgy
North Cadulawan	Bgy
Panggao	Bgy
Pantao	Bgy
Pantao-A-Munai	Bgy
Pantaon	Bgy
Pindolonan	Bgy
Punong	Bgy
Ramain	Bgy
Sandigamunai	Bgy
Tagoranao	Bgy
Tambo	Bgy
Tamparan	Bgy
Taporog	Bgy
Nunungan	Mun
Abaga	Bgy
Bangco	Bgy
Canibongan	Bgy
Karcum	Bgy
Dimayon	Bgy
Inayawan	Bgy
Kaludan	Bgy
Katubuan	Bgy
Cabasaran	Bgy
Liangan	Bgy
Lupitan	Bgy
Mangan	Bgy
Malaig	Bgy
Masibay	Bgy
Poblacion	Bgy
Notongan	Bgy
Petadun	Bgy
Panganapan	Bgy
Pantar	Bgy
Paridi	Bgy
Rarab	Bgy
Raraban	Bgy
Rebucon	Bgy
Songgod	Bgy
Taraka	Bgy
Pantao Ragat	Mun
Aloon	Bgy
Banday	Bgy
Bobonga Pantao Ragat	Bgy
Bobonga Radapan	Bgy
Cabasagan	Bgy
Calawe	Bgy
Culubun	Bgy
Dilimbayan	Bgy
Dimayon	Bgy
Poblacion East	Bgy
Lomidong	Bgy
Madaya	Bgy
Maliwanag	Bgy
Matampay	Bgy
Natangcopan	Bgy
Pansor	Bgy
Pantao Marug	Bgy
Tangcal	Bgy
Tongcopan	Bgy
Poblacion West	Bgy
Poona Piagapo	Mun
Alowin	Bgy
Bubong-Dinaig	Bgy
Caromatan	Bgy
Daramba	Bgy
Dinaig	Bgy
Cabasaran	Bgy
Kablangan	Bgy
Cadayonan	Bgy
Linindingan	Bgy
Lumbatan	Bgy
Lupitan	Bgy
Madamba	Bgy
Madaya	Bgy
Maliwanag	Bgy
Nunang	Bgy
Nunungan	Bgy
Pantao Raya	Bgy
Pantaon	Bgy
Pened	Bgy
Piangamangaan	Bgy
Pendolonan	Bgy
Poblacion	Bgy
Sulo	Bgy
Tagoranao	Bgy
Tangclao	Bgy
Timbangalan	Bgy
Salvador	Mun
Barandia	Bgy
Bulacon	Bgy
Buntong	Bgy
Calimodan	Bgy
Camp III	Bgy
Curva-Miagao	Bgy
Daligdigan	Bgy
Kilala	Bgy
Mabatao	Bgy
Madaya	Bgy
Mamaanon	Bgy
Mapantao	Bgy
Mindalano	Bgy
Padianan	Bgy
Pagalongan	Bgy
Pagayawan	Bgy
Panaliwad-on	Bgy
Pangantapan	Bgy
Pansor	Bgy
Patidon	Bgy
Pawak	Bgy
Poblacion	Bgy
Saumay	Bgy
Sudlon	Bgy
Inasagan	Bgy
Sapad	Mun
Baning	Bgy
Buriasan (Pob.)	Bgy
Dansalan	Bgy
Gamal	Bgy
Inudaran I	Bgy
Inudaran II	Bgy
Karkum	Bgy
Katipunan	Bgy
Mabugnao	Bgy
Maito Salug	Bgy
Mala Salug	Bgy
Mama-anon	Bgy
Mapurog	Bgy
Pancilan	Bgy
Panoloon	Bgy
Pili	Bgy
Sapad	Bgy
Tagoloan	Mun
Dalamas	Bgy
Darimbang	Bgy
Dimayon	Bgy
Inagongan	Bgy
Kiazar (Pob.)	Bgy
Malimbato	Bgy
Panalawan	Bgy
Tangcal	Mun
Small Banisilon	Bgy
Bayabao	Bgy
Berwar	Bgy
Big Banisilon	Bgy
Big Meladoc	Bgy
Bubong	Bgy
Lamaosa	Bgy
Linao	Bgy
Lindongan	Bgy
Lingco-an	Bgy
Small Meladoc	Bgy
Papan	Bgy
Pelingkingan	Bgy
Poblacion	Bgy
Poona Kapatagan	Bgy
Punod	Bgy
Somiorang	Bgy
Tangcal Proper	Bgy
Tubod (Capital)	Mun
Barakanas	Bgy
Baris	Bgy
Bualan	Bgy
Bulod	Bgy
Camp V	Bgy
Candis	Bgy
Caniogan	Bgy
Dalama	Bgy
Kakai Renabor	Bgy
Kalilangan	Bgy
Licapao	Bgy
Malingao	Bgy
Palao	Bgy
Patudan	Bgy
Pigcarangan	Bgy
Pinpin	Bgy
Poblacion	Bgy
Pualas	Bgy
San Antonio	Bgy
Santo Niño	Bgy
Taden	Bgy
Tangueguiron	Bgy
Taguranao	Bgy
Tubaran	Bgy
Pantar	Mun
Bangcal	Bgy
Bubong Madaya	Bgy
Bowi	Bgy
Cabasaran	Bgy
Cadayonan	Bgy
Campong	Bgy
Dibarosan	Bgy
Kalanganan East	Bgy
Kalanganan Lower	Bgy
Kalilangan	Bgy
Pantao-Marug	Bgy
Pantao-Ranao	Bgy
Pantar East	Bgy
Poblacion	Bgy
Pitubo	Bgy
Poona-Punod	Bgy
Punod	Bgy
Sundiga-Punod	Bgy
Tawanan	Bgy
West Pantar	Bgy
Lumba-Punod	Bgy
Misamis Occidental	Prov
Aloran	Mun
Balintonga	Bgy
Banisilon	Bgy
Burgos	Bgy
Calube	Bgy
Caputol	Bgy
Casusan	Bgy
Conat	Bgy
Culpan	Bgy
Dalisay	Bgy
Dullan	Bgy
Ibabao	Bgy
Tubod	Bgy
Labo	Bgy
Lawa-an	Bgy
Lobogon	Bgy
Lumbayao	Bgy
Makawa	Bgy
Manamong	Bgy
Matipaz	Bgy
Maular	Bgy
Mitazan	Bgy
Mohon	Bgy
Monterico	Bgy
Nabuna	Bgy
Palayan	Bgy
Pelong	Bgy
Ospital (Pob.)	Bgy
Roxas	Bgy
San Pedro	Bgy
Santa Ana	Bgy
Sinampongan	Bgy
Taguanao	Bgy
Tawi-tawi	Bgy
Toril	Bgy
Tuburan	Bgy
Zamora	Bgy
Macubon	Bgy
Tugaya	Bgy
Baliangao	Mun
Del Pilar	Bgy
Landing	Bgy
Lumipac	Bgy
Lusot	Bgy
Mabini	Bgy
Magsaysay	Bgy
Misom	Bgy
Mitacas	Bgy
Naburos	Bgy
Northern Poblacion	Bgy
Punta Miray	Bgy
Punta Sulong	Bgy
Sinian	Bgy
Southern Poblacion	Bgy
Tugas	Bgy
Bonifacio	Mun
Bag-ong Anonang	Bgy
Bagumbang	Bgy
Baybay	Bgy
Bolinsong	Bgy
Buenavista	Bgy
Buracan	Bgy
Calolot	Bgy
Dimalco	Bgy
Dullan	Bgy
Kanaokanao	Bgy
Liloan	Bgy
Linconan	Bgy
Lodiong	Bgy
Lower Usugan	Bgy
Mapurog	Bgy
Migpange	Bgy
Montol	Bgy
Pisa-an	Bgy
Poblacion	Bgy
Remedios	Bgy
Rufino Lumapas	Bgy
Sibuyon	Bgy
Tangab	Bgy
Tiaman	Bgy
Tusik	Bgy
Upper Usogan	Bgy
Demetrio Fernan	Bgy
Digson	Bgy
Calamba	Mun
Bonifacio	Bgy
Bunawan	Bgy
Calaran	Bgy
Dapacan Alto	Bgy
Dapacan Bajo	Bgy
Langub	Bgy
Libertad	Bgy
Magcamiguing	Bgy
Mamalad	Bgy
Mauswagon	Bgy
Northern Poblacion	Bgy
Salvador	Bgy
San Isidro	Bgy
Siloy	Bgy
Singalat	Bgy
Solinog	Bgy
Southwestern Poblacion	Bgy
Sulipat	Bgy
Don Bernardo Nery Pob.	Bgy
Clarin	Mun
Bernad	Bgy
Bito-on	Bgy
Cabunga-an	Bgy
Canibungan Daku	Bgy
Canibungan Putol	Bgy
Canipacan	Bgy
Dalingap	Bgy
Dela Paz	Bgy
Dolores	Bgy
Gata Daku	Bgy
Gata Diot	Bgy
Guba	Bgy
Kinangay Norte	Bgy
Kinangay Sur	Bgy
Lapasan	Bgy
Lupagan	Bgy
Malibangcao	Bgy
Masabud	Bgy
Mialen	Bgy
Pan-ay	Bgy
Penacio	Bgy
Poblacion I	Bgy
Segatic Daku	Bgy
Segatic Diot	Bgy
Sebasi	Bgy
Tinacla-an	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Concepcion	Mun
Bagong Nayon	Bgy
Capule	Bgy
New Casul	Bgy
Guiban	Bgy
Laya-an	Bgy
Lingatongan	Bgy
Maligubaan	Bgy
Mantukoy	Bgy
Marugang	Bgy
Poblacion	Bgy
Pogan	Bgy
Small Potongan	Bgy
Soso-on	Bgy
Upper Dapitan	Bgy
Upper Dioyo	Bgy
Upper Potongan	Bgy
Upper Salimpono	Bgy
Virayan	Bgy
Jimenez	Mun
Adorable	Bgy
Butuay	Bgy
Carmen	Bgy
Corrales	Bgy
Dicoloc	Bgy
Gata	Bgy
Guintomoyan	Bgy
Malibacsan	Bgy
Macabayao	Bgy
Matugas Alto	Bgy
Matugas Bajo	Bgy
Mialem	Bgy
Naga (Pob.)	Bgy
Palilan	Bgy
Nacional (Pob.)	Bgy
Rizal (Pob.)	Bgy
San Isidro	Bgy
Santa Cruz (Pob.)	Bgy
Sibaroc	Bgy
Sinara Alto	Bgy
Sinara Bajo	Bgy
Seti	Bgy
Tabo-o	Bgy
Taraka (Pob.)	Bgy
Lopez Jaena	Mun
Alegria	Bgy
Bagong Silang	Bgy
Biasong	Bgy
Bonifacio	Bgy
Burgos	Bgy
Dalacon	Bgy
Dampalan	Bgy
Estante	Bgy
Jasa-an	Bgy
Katipa	Bgy
Luzaran	Bgy
Macalibre Alto	Bgy
Macalibre Bajo	Bgy
Mahayahay	Bgy
Manguehan	Bgy
Mansabay Bajo	Bgy
Molatuhan Alto	Bgy
Molatuhan Bajo	Bgy
Peniel	Bgy
Eastern Poblacion	Bgy
Puntod	Bgy
Rizal	Bgy
Sibugon	Bgy
Sibula	Bgy
Don Andres Soriano	Bgy
Mabas	Bgy
Mansabay Alto	Bgy
Western Poblacion	Bgy
City of Oroquieta (Capital)	City
Apil	Bgy
Binuangan	Bgy
Bolibol	Bgy
Buenavista	Bgy
Bunga	Bgy
Buntawan	Bgy
Burgos	Bgy
Canubay	Bgy
Clarin Settlement	Bgy
Dolipos Bajo	Bgy
Dolipos Alto	Bgy
Dulapo	Bgy
Dullan Norte	Bgy
Dullan Sur	Bgy
Lamac Lower	Bgy
Lamac Upper	Bgy
Langcangan Lower	Bgy
Langcangan Proper	Bgy
Langcangan Upper	Bgy
Layawan	Bgy
Loboc Lower	Bgy
Loboc Upper	Bgy
Rizal Lower	Bgy
Malindang	Bgy
Mialen	Bgy
Mobod	Bgy
Ciriaco C. Pastrano	Bgy
Paypayan	Bgy
Pines	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
San Vicente Alto	Bgy
San Vicente Bajo	Bgy
Sebucal	Bgy
Senote	Bgy
Taboc Norte	Bgy
Taboc Sur	Bgy
Talairon	Bgy
Talic	Bgy
Toliyok	Bgy
Tipan	Bgy
Tuyabang Alto	Bgy
Tuyabang Bajo	Bgy
Tuyabang Proper	Bgy
Rizal Upper	Bgy
Victoria	Bgy
Villaflor	Bgy
City of Ozamiz	City
Aguada (Pob.)	Bgy
Banadero (Pob.)	Bgy
Bacolod	Bgy
Bagakay	Bgy
Balintawak	Bgy
Baybay Santa Cruz	Bgy
Baybay Triunfo	Bgy
Bongbong	Bgy
Calabayan	Bgy
Capucao C.	Bgy
Capucao P.	Bgy
Carangan	Bgy
Catadman-Manabay	Bgy
Cavinte	Bgy
Cogon	Bgy
Dalapang	Bgy
Diguan	Bgy
Dimaluna	Bgy
Embargo	Bgy
Gala	Bgy
Gango	Bgy
Gotokan Daku	Bgy
Gotokan Diot	Bgy
Guimad	Bgy
Guingona	Bgy
Kinuman Norte	Bgy
Kinuman Sur	Bgy
Labinay	Bgy
Labo	Bgy
Lam-an	Bgy
Liposong	Bgy
Litapan	Bgy
Malaubang	Bgy
Manaka	Bgy
Maningcol	Bgy
Mentering	Bgy
Carmen	Bgy
Molicay	Bgy
Stimson Abordo	Bgy
Pantaon	Bgy
Pulot	Bgy
San Antonio	Bgy
Baybay San Roque	Bgy
Sangay Daku	Bgy
Sangay Diot	Bgy
Sinuza	Bgy
Tabid	Bgy
Tinago	Bgy
Trigos	Bgy
50th District (Pob.)	Bgy
Doña Consuelo	Bgy
Panaon	Mun
Baga	Bgy
Bangko	Bgy
Camanucan	Bgy
Dela Paz	Bgy
Lutao	Bgy
Magsaysay	Bgy
Map-an	Bgy
Mohon	Bgy
Poblacion	Bgy
Punta	Bgy
Salimpuno	Bgy
San Andres	Bgy
San Juan	Bgy
San Roque	Bgy
Sumasap	Bgy
Villalin	Bgy
Plaridel	Mun
Agunod	Bgy
Bato	Bgy
Buena Voluntad	Bgy
Calaca-an	Bgy
Cartagena Proper	Bgy
Catarman	Bgy
Cebulin	Bgy
Clarin	Bgy
Danao	Bgy
Deboloc	Bgy
Divisoria	Bgy
Eastern Looc	Bgy
Ilisan	Bgy
Katipunan	Bgy
Kauswagan	Bgy
Lao Proper	Bgy
Lao Santa Cruz	Bgy
Looc Proper	Bgy
Mamanga Daku	Bgy
Mamanga Gamay	Bgy
Mangidkid	Bgy
New Cartagena	Bgy
New Look	Bgy
Northern Poblacion	Bgy
Panalsalan	Bgy
Puntod	Bgy
Quirino	Bgy
Santa Cruz	Bgy
Southern Looc	Bgy
Southern Poblacion	Bgy
Tipolo	Bgy
Unidos	Bgy
Usocan	Bgy
Sapang Dalaga	Mun
Bautista	Bgy
Bitibut	Bgy
Boundary	Bgy
Caluya	Bgy
Capundag	Bgy
Casul	Bgy
Dasa	Bgy
Dioyo	Bgy
Guinabot	Bgy
Libertad	Bgy
Locus	Bgy
Manla	Bgy
Masubong	Bgy
Agapito Yap, Sr.	Bgy
Poblacion	Bgy
Salimpuno	Bgy
San Agustin	Bgy
Sinaad	Bgy
Sipac	Bgy
Sixto Velez, Sr.	Bgy
Upper Bautista	Bgy
Ventura	Bgy
Medallo	Bgy
Dalumpinas	Bgy
Disoy	Bgy
El Paraiso	Bgy
Macabibo	Bgy
Sapang Ama	Bgy
Sinacaban	Mun
Cagay-anon	Bgy
Camanse	Bgy
Colupan Alto	Bgy
Colupan Bajo	Bgy
Dinas	Bgy
Estrella	Bgy
Katipunan	Bgy
Libertad Alto	Bgy
Libertad Bajo	Bgy
Poblacion	Bgy
San Isidro Alto	Bgy
San Isidro Bajo	Bgy
San Vicente	Bgy
Señor	Bgy
Sinonoc	Bgy
San Lorenzo Ruiz	Bgy
Tipan	Bgy
City of Tangub	City
Santa Maria	Bgy
Balatacan	Bgy
Banglay	Bgy
Mantic	Bgy
Migcanaway	Bgy
Bintana	Bgy
Bocator	Bgy
Bongabong	Bgy
Caniangan	Bgy
Capalaran	Bgy
Catagan	Bgy
Barangay I - City Hall (Pob.)	Bgy
Barangay II - Marilou Annex (Pob.)	Bgy
Barangay IV - St. Michael (Pob.)	Bgy
Isidro D. Tan	Bgy
Garang	Bgy
Guinabot	Bgy
Guinalaban	Bgy
Kauswagan	Bgy
Kimat	Bgy
Labuyo	Bgy
Lorenzo Tan	Bgy
Barangay VI - Lower Polao (Pob.)	Bgy
Lumban	Bgy
Maloro	Bgy
Barangay V - Malubog (Pob.)	Bgy
Manga	Bgy
Maquilao	Bgy
Barangay III- Market Kalubian (Pob.)	Bgy
Minsubong	Bgy
Owayan	Bgy
Paiton	Bgy
Panalsalan	Bgy
Pangabuan	Bgy
Prenza	Bgy
Salimpuno	Bgy
San Antonio	Bgy
San Apolinario	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Silangit	Bgy
Simasay	Bgy
Sumirap	Bgy
Taguite	Bgy
Tituron	Bgy
Barangay VII - Upper Polao (Pob.)	Bgy
Villaba	Bgy
Silanga	Bgy
Aquino	Bgy
Baluk	Bgy
Huyohoy	Bgy
Matugnaw	Bgy
Sicot	Bgy
Tugas	Bgy
Tudela	Mun
Balon	Bgy
Barra	Bgy
Basirang	Bgy
Bongabong	Bgy
Buenavista	Bgy
Cabol-anonan	Bgy
Cahayag	Bgy
Camating	Bgy
Canibungan Proper	Bgy
Casilak San Agustin	Bgy
Centro Hulpa (Pob.)	Bgy
Centro Napu (Pob.)	Bgy
Centro Upper (Pob.)	Bgy
Calambutan Bajo	Bgy
Calambutan Settlement	Bgy
Duanguican	Bgy
Gala	Bgy
Gumbil	Bgy
Locso-on	Bgy
Maikay	Bgy
Maribojoc	Bgy
Mitugas	Bgy
Nailon	Bgy
Namut	Bgy
Napurog	Bgy
Pan-ay Diot	Bgy
San Nicolas	Bgy
Sebac	Bgy
Silongon	Bgy
Sinuza	Bgy
Taguima	Bgy
Tigdok	Bgy
Yahong	Bgy
Don Victoriano Chiongbian	Mun
Bagong Clarin	Bgy
Gandawan	Bgy
Lake Duminagat	Bgy
Lalud	Bgy
Lampasan	Bgy
Liboron	Bgy
Maramara	Bgy
Napangan	Bgy
Nueva Vista	Bgy
Petianan	Bgy
Tuno	Bgy
Misamis Oriental	Prov
Alubijid	Mun
Baybay	Bgy
Benigwayan	Bgy
Calatcat	Bgy
Lagtang	Bgy
Lanao	Bgy
Loguilo	Bgy
Lourdes	Bgy
Lumbo	Bgy
Molocboloc	Bgy
Poblacion	Bgy
Sampatulog	Bgy
Sungay	Bgy
Talaba	Bgy
Taparak	Bgy
Tugasnon	Bgy
Tula	Bgy
Balingasag	Mun
Balagnan	Bgy
Baliwagan	Bgy
San Francisco	Bgy
Binitinan	Bgy
Blanco	Bgy
Calawag	Bgy
Camuayan	Bgy
Cogon	Bgy
Dansuli	Bgy
Dumarait	Bgy
Hermano	Bgy
Kibanban	Bgy
Linabu	Bgy
Linggangao	Bgy
Mambayaan	Bgy
Mandangoa	Bgy
Napaliran	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Quezon	Bgy
Rosario	Bgy
Samay	Bgy
San Isidro	Bgy
San Juan	Bgy
Talusan	Bgy
Waterfall	Bgy
Balingoan	Mun
Dahilig	Bgy
Baukbauk Pob.	Bgy
Kabangasan	Bgy
Kabulakan	Bgy
Kauswagan	Bgy
Lapinig (Pob.)	Bgy
Mantangale	Bgy
Mapua	Bgy
San Alonzo	Bgy
Binuangan	Mun
Dampias	Bgy
Kitamban	Bgy
Kitambis	Bgy
Mabini	Bgy
Mosangot	Bgy
Nabataan	Bgy
Poblacion	Bgy
Valdeconcha	Bgy
City of Cagayan De Oro (Capital)	City
Agusan	Bgy
Baikingon	Bgy
Bulua	Bgy
Balubal	Bgy
Balulang	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 15 (Pob.)	Bgy
Barangay 16 (Pob.)	Bgy
Barangay 17 (Pob.)	Bgy
Barangay 18 (Pob.)	Bgy
Barangay 19 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 21 (Pob.)	Bgy
Barangay 22 (Pob.)	Bgy
Barangay 23 (Pob.)	Bgy
Barangay 24 (Pob.)	Bgy
Barangay 26 (Pob.)	Bgy
Barangay 27 (Pob.)	Bgy
Barangay 28 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 30 (Pob.)	Bgy
Barangay 32 (Pob.)	Bgy
Barangay 33 (Pob.)	Bgy
Barangay 34 (Pob.)	Bgy
Barangay 38 (Pob.)	Bgy
Barangay 39 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 40 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Bayabas	Bgy
Bayanga	Bgy
Besigan	Bgy
Bonbon	Bgy
Bugo	Bgy
Camaman-an	Bgy
Canito-an	Bgy
Carmen	Bgy
Consolacion	Bgy
Cugman	Bgy
Dansolihon	Bgy
F. S. Catanico	Bgy
Gusa	Bgy
Indahag	Bgy
Iponan	Bgy
Kauswagan	Bgy
Lapasan	Bgy
Lumbia	Bgy
Macabalan	Bgy
Macasandig	Bgy
Mambuaya	Bgy
Nazareth	Bgy
Pagalungan	Bgy
Pagatpat	Bgy
Patag	Bgy
Pigsag-an	Bgy
Puerto	Bgy
Puntod	Bgy
San Simon	Bgy
Tablon	Bgy
Taglimao	Bgy
Tagpangi	Bgy
Tignapoloan	Bgy
Tuburan	Bgy
Tumpagon	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 20 (Pob.)	Bgy
Barangay 25 (Pob.)	Bgy
Barangay 29 (Pob.)	Bgy
Barangay 31 (Pob.)	Bgy
Barangay 35 (Pob.)	Bgy
Barangay 36 (Pob.)	Bgy
Barangay 37 (Pob.)	Bgy
Claveria	Mun
Ani-e	Bgy
Aposkahoy	Bgy
Bulahan	Bgy
Cabacungan	Bgy
Pelaez	Bgy
Gumaod	Bgy
Hinaplanan	Bgy
Kalawitan	Bgy
Lanise	Bgy
Luna	Bgy
Madaguing	Bgy
Malagana	Bgy
Minalwang	Bgy
Mat-i	Bgy
Panampawan	Bgy
Pambugas	Bgy
Patrocenio	Bgy
Plaridel	Bgy
Poblacion	Bgy
Punong	Bgy
Rizal	Bgy
Santa Cruz	Bgy
Tamboboan	Bgy
Tipolohon	Bgy
City of El Salvador	City
Amoros	Bgy
Bolisong	Bgy
Bolobolo	Bgy
Calongonan	Bgy
Cogon	Bgy
Himaya	Bgy
Hinigdaan	Bgy
Kalabaylabay	Bgy
Molugan	Bgy
Poblacion	Bgy
Kibonbon	Bgy
Sambulawan	Bgy
Sinaloc	Bgy
Taytay	Bgy
Ulaliman	Bgy
City of Gingoog	City
Agay-ayan	Bgy
Alagatan	Bgy
Anakan	Bgy
Bagubad	Bgy
Bakidbakid	Bgy
Bal-ason	Bgy
Bantaawan	Bgy
Binakalan	Bgy
Capitulangan	Bgy
Daan-Lungsod	Bgy
Hindangon	Bgy
Kalagonoy	Bgy
Kibuging	Bgy
Kipuntos	Bgy
Lawaan	Bgy
Lawit	Bgy
Libertad	Bgy
Libon	Bgy
Lunao	Bgy
Lunotan	Bgy
Malibud	Bgy
Malinao	Bgy
Maribucao	Bgy
Mimbuntong	Bgy
Mimbalagon	Bgy
Mimbunga	Bgy
Minsapinit	Bgy
Murallon	Bgy
Odiongan	Bgy
Pangasihan	Bgy
Pigsaluhan	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Barangay 14 (Pob.)	Bgy
Barangay 15 (Pob.)	Bgy
Barangay 16 (Pob.)	Bgy
Barangay 17 (Pob.)	Bgy
Barangay 18-A (Pob.)	Bgy
Barangay 19 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 20 (Pob.)	Bgy
Barangay 21 (Pob.)	Bgy
Barangay 22-A (Pob.)	Bgy
Barangay 23 (Pob.)	Bgy
Barangay 24 (Pob.)	Bgy
Barangay 25 (Pob.)	Bgy
Barangay 26 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
Punong	Bgy
Ricoro	Bgy
Samay	Bgy
San Juan	Bgy
San Luis	Bgy
San Miguel	Bgy
Santiago	Bgy
Talisay	Bgy
Talon	Bgy
Tinabalan	Bgy
Tinulongan	Bgy
Barangay 18 (Pob.)	Bgy
Barangay 22 (Pob.)	Bgy
Barangay 24-A (Pob.)	Bgy
Dinawehan	Bgy
Eureka	Bgy
Kalipay	Bgy
Kamanikan	Bgy
Kianlagan	Bgy
San Jose	Bgy
Sangalan	Bgy
Tagpako	Bgy
Gitagum	Mun
Burnay	Bgy
Carlos P. Garcia	Bgy
Cogon	Bgy
Gregorio Pelaez	Bgy
Kilangit	Bgy
Matangad	Bgy
Pangayawan	Bgy
Poblacion	Bgy
Quezon	Bgy
Tala-o	Bgy
Ulab	Bgy
Initao	Mun
Aluna	Bgy
Andales	Bgy
Apas	Bgy
Calacapan	Bgy
Gimangpang	Bgy
Jampason	Bgy
Kamelon	Bgy
Kanitoan	Bgy
Oguis	Bgy
Pagahan	Bgy
Poblacion	Bgy
Pontacon	Bgy
San Pedro	Bgy
Sinalac	Bgy
Tawantawan	Bgy
Tubigan	Bgy
Jasaan	Mun
Aplaya	Bgy
Bobontugan	Bgy
Corrales	Bgy
Danao	Bgy
Jampason	Bgy
Kimaya	Bgy
Lower Jasaan (Pob.)	Bgy
Luz Banzon	Bgy
Natubo	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Nicolas	Bgy
Solana	Bgy
Upper Jasaan (Pob.)	Bgy
I. S. Cruz	Bgy
Kinoguitan	Mun
Beray	Bgy
Bolisong	Bgy
Buko	Bgy
Kalitian	Bgy
Calubo	Bgy
Campo	Bgy
Esperanza	Bgy
Kagumahan	Bgy
Kitotok	Bgy
Panabol	Bgy
Poblacion	Bgy
Salicapawan	Bgy
Salubsob	Bgy
Suarez	Bgy
Sumalag	Bgy
Lagonglong	Mun
Banglay	Bgy
Dampil	Bgy
Gaston	Bgy
Kabulawan	Bgy
Kauswagan	Bgy
Lumbo	Bgy
Manaol	Bgy
Poblacion	Bgy
Tabok	Bgy
Umagos	Bgy
Laguindingan	Mun
Aromahon	Bgy
Gasi	Bgy
Kibaghot	Bgy
Lapad	Bgy
Liberty	Bgy
Mauswagon	Bgy
Moog	Bgy
Poblacion	Bgy
Sambulawan	Bgy
Sinai	Bgy
Tubajon	Bgy
Libertad	Mun
Dulong	Bgy
Gimaylan	Bgy
Kimalok	Bgy
Lubluban	Bgy
Poblacion	Bgy
Retablo	Bgy
Santo Niño	Bgy
Tangcub	Bgy
Taytayan	Bgy
Lugait	Mun
Aya-aya	Bgy
Betahon	Bgy
Biga	Bgy
Calangahan	Bgy
Kaluknayan	Bgy
Lower Talacogon	Bgy
Poblacion	Bgy
Upper Talacogon	Bgy
Magsaysay	Mun
Abunda	Bgy
Artadi	Bgy
Bonifacio Aquino	Bgy
Cabalawan	Bgy
Cabantian	Bgy
Cabubuhan	Bgy
Candiis	Bgy
Consuelo	Bgy
Damayuhan	Bgy
Gumabon	Bgy
Kauswagan	Bgy
Kibungsod	Bgy
Mahayahay	Bgy
Mindulao	Bgy
Pag-asa	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Tibon-tibon	Bgy
Tulang	Bgy
Villa Felipa	Bgy
Katipunan	Bgy
Tama	Bgy
Tinaan	Bgy
Manticao	Mun
Argayoso	Bgy
Balintad	Bgy
Cabalantian	Bgy
Camanga	Bgy
Digkilaan	Bgy
Mahayahay	Bgy
Pagawan	Bgy
Paniangan	Bgy
Patag	Bgy
Poblacion	Bgy
Punta Silum	Bgy
Tuod	Bgy
Upper Malubog	Bgy
Medina	Mun
Bangbang	Bgy
Bulwa	Bgy
Cabug	Bgy
Dig-aguyan	Bgy
Duka	Bgy
Gasa	Bgy
Maanas	Bgy
Mananum Bag-o	Bgy
Mananum Daan	Bgy
North Poblacion	Bgy
Pahindong	Bgy
Portulin	Bgy
San Isidro	Bgy
San Jose	Bgy
San Roque	Bgy
San Vicente	Bgy
South Poblacion	Bgy
Tambagan	Bgy
Tup-on	Bgy
Naawan	Mun
Don Pedro	Bgy
Linangkayan	Bgy
Lubilan	Bgy
Mapulog	Bgy
Maputi	Bgy
Mat-i	Bgy
Patag	Bgy
Poblacion	Bgy
Tagbalogo	Bgy
Tuboran	Bgy
Opol	Mun
Awang	Bgy
Bagocboc	Bgy
Barra	Bgy
Bonbon	Bgy
Cauyonan	Bgy
Igpit	Bgy
Limonda	Bgy
Luyongbonbon	Bgy
Malanang	Bgy
Nangcaon	Bgy
Patag	Bgy
Poblacion	Bgy
Taboc	Bgy
Tingalan	Bgy
Salay	Mun
Alipuaton	Bgy
Ampenican	Bgy
Bunal	Bgy
Dinagsaan	Bgy
Guinalaban	Bgy
Ili-ilihon	Bgy
Inobulan	Bgy
Looc	Bgy
Matampa	Bgy
Membuli	Bgy
Poblacion	Bgy
Salagsag	Bgy
Salay River I	Bgy
Salay River II	Bgy
Saray	Bgy
Tinagaan	Bgy
Yungod	Bgy
Casulog	Bgy
Sugbongcogon	Mun
Alicomohan	Bgy
Ampianga	Bgy
Kaulayanan	Bgy
Kidampas	Bgy
Kiraging	Bgy
Mangga	Bgy
Mimbuahan	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Silad	Bgy
Tagoloan	Mun
Baluarte	Bgy
Casinglot	Bgy
Gracia	Bgy
Mohon	Bgy
Natumolan	Bgy
Poblacion	Bgy
Rosario	Bgy
Santa Ana	Bgy
Santa Cruz	Bgy
Sugbongcogon	Bgy
Talisayan	Mun
Bugdang	Bgy
Calamcam	Bgy
Casibole	Bgy
Macopa	Bgy
Magkarila	Bgy
Mahayag	Bgy
Mandahilag	Bgy
Mintabon	Bgy
Pangpangon	Bgy
Poblacion	Bgy
Pook	Bgy
Punta Santiago	Bgy
Puting Balas	Bgy
San Jose	Bgy
Santa Ines	Bgy
Sibantang	Bgy
Sindangan	Bgy
Tagbocboc	Bgy
Villanueva	Mun
Balacanas	Bgy
Dayawan	Bgy
Katipunan	Bgy
Kimaya	Bgy
Poblacion 1	Bgy
San Martin	Bgy
Tambobong	Bgy
Imelda	Bgy
Looc	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Region XI (Davao Region)	Reg
Davao del Norte	Prov
Asuncion	Mun
Binancian	Bgy
Buan	Bgy
Buclad	Bgy
Cabaywa	Bgy
Camansa	Bgy
Camoning	Bgy
Canatan	Bgy
Concepcion	Bgy
Doña Andrea	Bgy
Magatos	Bgy
Napungas	Bgy
New Bantayan	Bgy
New Santiago	Bgy
Pamacaun	Bgy
Cambanogoy (Pob.)	Bgy
Sagayen	Bgy
San Vicente	Bgy
Santa Filomena	Bgy
Sonlon	Bgy
New Loon	Bgy
Carmen	Mun
Alejal	Bgy
Anibongan	Bgy
Asuncion	Bgy
Cebulano	Bgy
Guadalupe	Bgy
Ising (Pob.)	Bgy
La Paz	Bgy
Mabaus	Bgy
Mabuhay	Bgy
Magsaysay	Bgy
Mangalcal	Bgy
Minda	Bgy
New Camiling	Bgy
San Isidro	Bgy
Santo Niño	Bgy
Tibulao	Bgy
Tubod	Bgy
Tuganay	Bgy
Salvacion	Bgy
Taba	Bgy
Kapalong	Mun
Semong	Bgy
Florida	Bgy
Gabuyan	Bgy
Gupitan	Bgy
Capungagan	Bgy
Katipunan	Bgy
Luna	Bgy
Mabantao	Bgy
Mamacao	Bgy
Pag-asa	Bgy
Maniki	Bgy
Sampao	Bgy
Sua-on	Bgy
Tiburcia	Bgy
New Corella	Mun
Cabidianan	Bgy
Carcor	Bgy
Del Monte	Bgy
Del Pilar	Bgy
El Salvador	Bgy
Limba-an	Bgy
Macgum	Bgy
Mambing	Bgy
Mesaoy	Bgy
New Bohol	Bgy
New Cortez	Bgy
New Sambog	Bgy
Patrocenio	Bgy
Poblacion	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santa Fe	Bgy
Santo Niño	Bgy
Suawon	Bgy
San Jose	Bgy
City of Panabo	City
A. O. Floirendo	Bgy
Datu Abdul Dadia	Bgy
Buenavista	Bgy
Cacao	Bgy
Cagangohan	Bgy
Consolacion	Bgy
Dapco	Bgy
Gredu (Pob.)	Bgy
J.P. Laurel	Bgy
Kasilak	Bgy
Katipunan	Bgy
Katualan	Bgy
Kauswagan	Bgy
Kiotoy	Bgy
Little Panay	Bgy
Lower Panaga	Bgy
Mabunao	Bgy
Maduao	Bgy
Malativas	Bgy
Manay	Bgy
Nanyo	Bgy
New Malaga	Bgy
New Malitbog	Bgy
New Pandan (Pob.)	Bgy
New Visayas	Bgy
Quezon	Bgy
Salvacion	Bgy
San Francisco (Pob.)	Bgy
San Nicolas	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santo Niño (Pob.)	Bgy
Sindaton	Bgy
Southern Davao	Bgy
Tagpore	Bgy
Tibungol	Bgy
Upper Licanan	Bgy
Waterfall	Bgy
San Pedro	Bgy
Island Garden City of Samal	City
Adecor	Bgy
Anonang	Bgy
Aumbay	Bgy
Aundanao	Bgy
Balet	Bgy
Bandera	Bgy
Caliclic	Bgy
Camudmud	Bgy
Catagman	Bgy
Cawag	Bgy
Cogon	Bgy
Cogon	Bgy
Dadatan	Bgy
Del Monte	Bgy
Guilon	Bgy
Kanaan	Bgy
Kinawitnon	Bgy
Libertad	Bgy
Libuak	Bgy
Licup	Bgy
Limao	Bgy
Linosutan	Bgy
Mambago-A	Bgy
Mambago-B	Bgy
Miranda (Pob.)	Bgy
Moncado (Pob.)	Bgy
Pangubatan	Bgy
Peñaplata (Pob.)	Bgy
Poblacion	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Isidro	Bgy
San Jose	Bgy
San Miguel	Bgy
San Remigio	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Sion	Bgy
Tagbaobo	Bgy
Tagbay	Bgy
Tagbitan-ag	Bgy
Tagdaliao	Bgy
Tagpopongan	Bgy
Tambo	Bgy
Toril	Bgy
Santo Tomas	Mun
Balagunan	Bgy
Bobongon	Bgy
Esperanza	Bgy
Kimamon	Bgy
Kinamayan	Bgy
La Libertad	Bgy
Lungaog	Bgy
Magwawa	Bgy
New Katipunan	Bgy
Pantaron	Bgy
Tibal-og (Pob.)	Bgy
San Jose	Bgy
San Miguel	Bgy
Talomo	Bgy
Casig-Ang	Bgy
New Visayas	Bgy
Salvacion	Bgy
San Vicente	Bgy
Tulalian	Bgy
City of Tagum (Capital)	City
Apokon	Bgy
Bincungan	Bgy
Busaon	Bgy
Canocotan	Bgy
Cuambogan	Bgy
La Filipina	Bgy
Liboganon	Bgy
Madaum	Bgy
Magdum	Bgy
Mankilam	Bgy
New Balamban	Bgy
Nueva Fuerza	Bgy
Pagsabangan	Bgy
Pandapan	Bgy
Magugpo Poblacion	Bgy
San Agustin	Bgy
San Isidro	Bgy
San Miguel	Bgy
Visayan Village	Bgy
Magugpo East	Bgy
Magugpo North	Bgy
Magugpo South	Bgy
Magugpo West	Bgy
Talaingod	Mun
Dagohoy	Bgy
Palma Gil	Bgy
Santo Niño	Bgy
Braulio E. Dujali	Mun
Cabayangan	Bgy
Dujali	Bgy
Magupising	Bgy
New Casay	Bgy
Tanglaw	Bgy
San Isidro	Mun
Dacudao	Bgy
Datu Balong	Bgy
Igangon	Bgy
Kipalili	Bgy
Libuton	Bgy
Linao	Bgy
Mamangan	Bgy
Monte Dujali	Bgy
Pinamuno	Bgy
Sabangan	Bgy
San Miguel	Bgy
Santo Niño	Bgy
Sawata	Bgy
Davao del Sur	Prov
Bansalan	Mun
Alegre	Bgy
Alta Vista	Bgy
Anonang	Bgy
Bitaug	Bgy
Bonifacio	Bgy
Buenavista	Bgy
Darapuay	Bgy
Dolo	Bgy
Eman	Bgy
Kinuskusan	Bgy
Libertad	Bgy
Linawan	Bgy
Mabuhay	Bgy
Mabunga	Bgy
Managa	Bgy
Marber	Bgy
New Clarin	Bgy
Poblacion	Bgy
Rizal	Bgy
Santo Niño	Bgy
Sibayan	Bgy
Tinongtongan	Bgy
Tubod	Bgy
Union	Bgy
Poblacion Dos	Bgy
City of Davao	City
Acacia	Bgy
Agdao	Bgy
Alambre	Bgy
Atan-Awe	Bgy
Bago Gallera	Bgy
Bago Oshiro	Bgy
Baguio (Pob.)	Bgy
Balengaeng	Bgy
Baliok	Bgy
Bangkas Heights	Bgy
Baracatan	Bgy
Bato	Bgy
Bayabas	Bgy
Biao Escuela	Bgy
Biao Guianga	Bgy
Biao Joaquin	Bgy
Binugao	Bgy
Bucana	Bgy
Buhangin (Pob.)	Bgy
Bunawan (Pob.)	Bgy
Cabantian	Bgy
Cadalian	Bgy
Calinan (Pob.)	Bgy
Callawa	Bgy
Camansi	Bgy
Carmen	Bgy
Catalunan Grande	Bgy
Catalunan Pequeño	Bgy
Catigan	Bgy
Cawayan	Bgy
Colosas	Bgy
Communal	Bgy
Crossing Bayabas	Bgy
Dacudao	Bgy
Dalag	Bgy
Dalagdag	Bgy
Daliao	Bgy
Daliaon Plantation	Bgy
Dominga	Bgy
Dumoy	Bgy
Eden	Bgy
Fatima	Bgy
Gatungan	Bgy
Gumalang	Bgy
Ilang	Bgy
Indangan	Bgy
Kilate	Bgy
Lacson	Bgy
Lamanan	Bgy
Lampianao	Bgy
Langub	Bgy
Alejandra Navarro	Bgy
Lizada	Bgy
Los Amigos	Bgy
Lubogan	Bgy
Lumiad	Bgy
Ma-a	Bgy
Mabuhay	Bgy
Magtuod	Bgy
Mahayag	Bgy
Malabog	Bgy
Malagos	Bgy
Malamba	Bgy
Manambulan	Bgy
Mandug	Bgy
Manuel Guianga	Bgy
Mapula	Bgy
Marapangi	Bgy
Marilog	Bgy
Matina Aplaya	Bgy
Matina Crossing	Bgy
Matina Pangi	Bgy
Matina Biao	Bgy
Mintal	Bgy
Mudiang	Bgy
Mulig	Bgy
New Carmen	Bgy
New Valencia	Bgy
Pampanga	Bgy
Panacan	Bgy
Panalum	Bgy
Pandaitan	Bgy
Pangyan	Bgy
Paquibato (Pob.)	Bgy
Paradise Embak	Bgy
Riverside	Bgy
Salapawan	Bgy
Salaysay	Bgy
San Isidro	Bgy
Sasa	Bgy
Sibulan	Bgy
Sirawan	Bgy
Sirib	Bgy
Suawan	Bgy
Subasta	Bgy
Sumimao	Bgy
Tacunan	Bgy
Tagakpan	Bgy
Tagluno	Bgy
Tagurano	Bgy
Talandang	Bgy
Talomo (Pob.)	Bgy
Talomo River	Bgy
Tamayong	Bgy
Tambobong	Bgy
Tamugan	Bgy
Tapak	Bgy
Tawan-tawan	Bgy
Tibuloy	Bgy
Tibungco	Bgy
Tigatto	Bgy
Toril (Pob.)	Bgy
Tugbok (Pob.)	Bgy
Tungakalan	Bgy
Ula	Bgy
Wangan	Bgy
Wines	Bgy
Barangay 1-A (Pob.)	Bgy
Barangay 2-A (Pob.)	Bgy
Barangay 3-A (Pob.)	Bgy
Barangay 4-A (Pob.)	Bgy
Barangay 5-A (Pob.)	Bgy
Barangay 6-A (Pob.)	Bgy
Barangay 7-A (Pob.)	Bgy
Barangay 8-A (Pob.)	Bgy
Barangay 9-A (Pob.)	Bgy
Barangay 10-A (Pob.)	Bgy
Barangay 11-B (Pob.)	Bgy
Barangay 12-B (Pob.)	Bgy
Barangay 13-B (Pob.)	Bgy
Barangay 14-B (Pob.)	Bgy
Barangay 15-B (Pob.)	Bgy
Barangay 16-B (Pob.)	Bgy
Barangay 17-B (Pob.)	Bgy
Barangay 18-B (Pob.)	Bgy
Barangay 19-B (Pob.)	Bgy
Barangay 20-B (Pob.)	Bgy
Barangay 21-C (Pob.)	Bgy
Barangay 22-C (Pob.)	Bgy
Barangay 23-C (Pob.)	Bgy
Barangay 24-C (Pob.)	Bgy
Barangay 25-C (Pob.)	Bgy
Barangay 26-C (Pob.)	Bgy
Barangay 27-C (Pob.)	Bgy
Barangay 28-C (Pob.)	Bgy
Barangay 29-C (Pob.)	Bgy
Barangay 30-C (Pob.)	Bgy
Barangay 31-D (Pob.)	Bgy
Barangay 32-D (Pob.)	Bgy
Barangay 33-D (Pob.)	Bgy
Barangay 34-D (Pob.)	Bgy
Barangay 35-D (Pob.)	Bgy
Barangay 36-D (Pob.)	Bgy
Barangay 37-D (Pob.)	Bgy
Barangay 38-D (Pob.)	Bgy
Barangay 39-D (Pob.)	Bgy
Barangay 40-D (Pob.)	Bgy
Angalan	Bgy
Baganihan	Bgy
Bago Aplaya	Bgy
Bantol	Bgy
Buda	Bgy
Centro	Bgy
Datu Salumay	Bgy
Gov. Paciano Bangoy	Bgy
Gov. Vicente Duterte	Bgy
Gumitan	Bgy
Inayangan	Bgy
Kap. Tomas Monteverde, Sr.	Bgy
Lapu-lapu	Bgy
Leon Garcia, Sr.	Bgy
Magsaysay	Bgy
Megkawayan	Bgy
Rafael Castillo	Bgy
Saloy	Bgy
San Antonio	Bgy
Santo Niño	Bgy
Ubalde	Bgy
Waan	Bgy
Wilfredo Aquino	Bgy
Alfonso Angliongto Sr.	Bgy
Vicente Hizon Sr.	Bgy
City of Digos (Capital)	City
Aplaya	Bgy
Balabag	Bgy
San Jose	Bgy
Binaton	Bgy
Cogon	Bgy
Colorado	Bgy
Dawis	Bgy
Dulangan	Bgy
Goma	Bgy
Igpit	Bgy
Kiagot	Bgy
Lungag	Bgy
Mahayahay	Bgy
Matti	Bgy
Kapatagan	Bgy
Ruparan	Bgy
San Agustin	Bgy
San Miguel	Bgy
San Roque	Bgy
Sinawilan	Bgy
Soong	Bgy
Tiguman	Bgy
Tres De Mayo	Bgy
Zone 1 (Pob.)	Bgy
Zone 2 (Pob.)	Bgy
Zone 3 (Pob.)	Bgy
Hagonoy	Mun
Balutakay	Bgy
Clib	Bgy
Guihing Aplaya	Bgy
Guihing	Bgy
Hagonoy Crossing	Bgy
Kibuaya	Bgy
La Union	Bgy
Lanuro	Bgy
Lapulabao	Bgy
Leling	Bgy
Mahayahay	Bgy
Malabang Damsite	Bgy
Maliit Digos	Bgy
New Quezon	Bgy
Paligue	Bgy
Poblacion	Bgy
Sacub	Bgy
San Guillermo	Bgy
San Isidro	Bgy
Sinayawan	Bgy
Tologan	Bgy
Kiblawan	Mun
Abnate	Bgy
Bagong Negros	Bgy
Bagong Silang	Bgy
Bagumbayan	Bgy
Balasiao	Bgy
Bonifacio	Bgy
Bunot	Bgy
Cogon-Bacaca	Bgy
Dapok	Bgy
Ihan	Bgy
Kibongbong	Bgy
Kimlawis	Bgy
Kisulan	Bgy
Lati-an	Bgy
Manual	Bgy
Maraga-a	Bgy
Molopolo	Bgy
New Sibonga	Bgy
Panaglib	Bgy
Pasig	Bgy
Poblacion	Bgy
Pocaleel	Bgy
San Isidro	Bgy
San Jose	Bgy
San Pedro	Bgy
Santo Niño	Bgy
Tacub	Bgy
Tacul	Bgy
Waterfall	Bgy
Bulol-Salo	Bgy
Magsaysay	Mun
Bacungan	Bgy
Balnate	Bgy
Barayong	Bgy
Blocon	Bgy
Dalawinon	Bgy
Dalumay	Bgy
Glamang	Bgy
Kanapulo	Bgy
Kasuga	Bgy
Lower Bala	Bgy
Mabini	Bgy
Malawanit	Bgy
Malongon	Bgy
New Ilocos	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Miguel	Bgy
Tacul	Bgy
Tagaytay	Bgy
Upper Bala	Bgy
Maibo	Bgy
New Opon	Bgy
Malalag	Mun
Baybay	Bgy
Bolton	Bgy
Bulacan	Bgy
Caputian	Bgy
Ibo	Bgy
Kiblagon	Bgy
Lapu-Lapu	Bgy
Mabini	Bgy
New Baclayon	Bgy
Pitu	Bgy
Poblacion	Bgy
Tagansule	Bgy
Bagumbayan	Bgy
Rizal	Bgy
San Isidro	Bgy
Matanao	Mun
Asbang	Bgy
Asinan	Bgy
Bagumbayan	Bgy
Bangkal	Bgy
Buas	Bgy
Buri	Bgy
Camanchiles	Bgy
Ceboza	Bgy
Colonsabak	Bgy
Dongan-Pekong	Bgy
Kabasagan	Bgy
Kapok	Bgy
Kauswagan	Bgy
Kibao	Bgy
La Suerte	Bgy
Langa-an	Bgy
Lower Marber	Bgy
Cabligan	Bgy
Manga	Bgy
New Katipunan	Bgy
New Murcia	Bgy
New Visayas	Bgy
Poblacion	Bgy
Saboy	Bgy
San Jose	Bgy
San Miguel	Bgy
San Vicente	Bgy
Saub	Bgy
Sinaragan	Bgy
Sinawilan	Bgy
Tamlangon	Bgy
Towak	Bgy
Tibongbong	Bgy
Padada	Mun
Almendras (Pob.)	Bgy
Don Sergio Osmeña, Sr.	Bgy
Harada Butai	Bgy
Lower Katipunan	Bgy
Lower Limonzo	Bgy
Lower Malinao	Bgy
N C Ordaneza District (Pob.)	Bgy
Northern Paligue	Bgy
Palili	Bgy
Piape	Bgy
Punta Piape	Bgy
Quirino District (Pob.)	Bgy
San Isidro	Bgy
Southern Paligue	Bgy
Tulogan	Bgy
Upper Limonzo	Bgy
Upper Malinao	Bgy
Santa Cruz	Mun
Astorga	Bgy
Bato	Bgy
Coronon	Bgy
Darong	Bgy
Inawayan	Bgy
Jose Rizal	Bgy
Matutungan	Bgy
Melilia	Bgy
Zone I (Pob.)	Bgy
Saliducon	Bgy
Sibulan	Bgy
Sinoron	Bgy
Tagabuli	Bgy
Tibolo	Bgy
Tuban	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Sulop	Mun
Balasinon	Bgy
Buguis	Bgy
Carre	Bgy
Clib	Bgy
Harada Butai	Bgy
Katipunan	Bgy
Kiblagon	Bgy
Labon	Bgy
Laperas	Bgy
Lapla	Bgy
Litos	Bgy
Luparan	Bgy
Mckinley	Bgy
New Cebu	Bgy
Osmeña	Bgy
Palili	Bgy
Parame	Bgy
Poblacion	Bgy
Roxas	Bgy
Solongvale	Bgy
Tagolilong	Bgy
Tala-o	Bgy
Talas	Bgy
Tanwalang	Bgy
Waterfall	Bgy
Davao Oriental	Prov
Baganga	Mun
Baculin	Bgy
Banao	Bgy
Batawan	Bgy
Batiano	Bgy
Binondo	Bgy
Bobonao	Bgy
Campawan	Bgy
Central (Pob.)	Bgy
Dapnan	Bgy
Kinablangan	Bgy
Lambajon	Bgy
Mahanub	Bgy
Mikit	Bgy
Salingcomot	Bgy
San Isidro	Bgy
San Victor	Bgy
Lucod	Bgy
Saoquegue	Bgy
Banaybanay	Mun
Cabangcalan	Bgy
Caganganan	Bgy
Calubihan	Bgy
Causwagan	Bgy
Punta Linao	Bgy
Mahayag	Bgy
Maputi	Bgy
Mogbongcogon	Bgy
Panikian	Bgy
Pintatagan	Bgy
Piso Proper	Bgy
Poblacion	Bgy
San Vicente	Bgy
Rang-ay	Bgy
Boston	Mun
Cabasagan	Bgy
Caatihan	Bgy
Cawayanan	Bgy
Poblacion	Bgy
San Jose	Bgy
Sibajay	Bgy
Carmen	Bgy
Simulao	Bgy
Caraga	Mun
Alvar	Bgy
Caningag	Bgy
Don Leon Balante	Bgy
Lamiawan	Bgy
Manorigao	Bgy
Mercedes	Bgy
Palma Gil	Bgy
Pichon	Bgy
Poblacion	Bgy
San Antonio	Bgy
San Jose	Bgy
San Luis	Bgy
San Miguel	Bgy
San Pedro	Bgy
Santa Fe	Bgy
Santiago	Bgy
Sobrecarey	Bgy
Cateel	Mun
Abijod	Bgy
Alegria	Bgy
Aliwagwag	Bgy
Aragon	Bgy
Baybay	Bgy
Maglahus	Bgy
Mainit	Bgy
Malibago	Bgy
San Alfonso	Bgy
San Antonio	Bgy
San Miguel	Bgy
San Rafael	Bgy
San Vicente	Bgy
Santa Filomena	Bgy
Taytayan	Bgy
Poblacion	Bgy
Governor Generoso	Mun
Anitap	Bgy
Manuel Roxas	Bgy
Don Aurelio Chicote	Bgy
Lavigan	Bgy
Luzon	Bgy
Magdug	Bgy
Monserrat	Bgy
Nangan	Bgy
Oregon	Bgy
Poblacion	Bgy
Pundaguitan	Bgy
Sergio Osmeña	Bgy
Surop	Bgy
Tagabebe	Bgy
Tamban	Bgy
Tandang Sora	Bgy
Tibanban	Bgy
Tiblawan	Bgy
Upper Tibanban	Bgy
Crispin Dela Cruz	Bgy
Lupon	Mun
Bagumbayan	Bgy
Cabadiangan	Bgy
Calapagan	Bgy
Cocornon	Bgy
Corporacion	Bgy
Don Mariano Marcos	Bgy
Ilangay	Bgy
Langka	Bgy
Lantawan	Bgy
Limbahan	Bgy
Macangao	Bgy
Magsaysay	Bgy
Mahayahay	Bgy
Maragatas	Bgy
Marayag	Bgy
New Visayas	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Jose	Bgy
Tagboa	Bgy
Tagugpo	Bgy
Manay	Mun
Capasnan	Bgy
Cayawan	Bgy
Central (Pob.)	Bgy
Concepcion	Bgy
Del Pilar	Bgy
Guza	Bgy
Holy Cross	Bgy
Mabini	Bgy
Manreza	Bgy
Old Macopa	Bgy
Rizal	Bgy
San Fermin	Bgy
San Ignacio	Bgy
San Isidro	Bgy
New Taokanga	Bgy
Zaragosa	Bgy
Lambog	Bgy
City of Mati (Capital)	City
Badas	Bgy
Bobon	Bgy
Buso	Bgy
Cabuaya	Bgy
Central (Pob.)	Bgy
Culian	Bgy
Dahican	Bgy
Danao	Bgy
Dawan	Bgy
Don Enrique Lopez	Bgy
Don Martin Marundan	Bgy
Don Salvador Lopez, Sr.	Bgy
Langka	Bgy
Lawigan	Bgy
Libudon	Bgy
Luban	Bgy
Macambol	Bgy
Mamali	Bgy
Matiao	Bgy
Mayo	Bgy
Sainz	Bgy
Sanghay	Bgy
Tagabakid	Bgy
Tagbinonga	Bgy
Taguibo	Bgy
Tamisan	Bgy
San Isidro	Mun
Baon	Bgy
Bitaogan	Bgy
Cambaleon	Bgy
Dugmanon	Bgy
Iba	Bgy
La Union	Bgy
Lapu-lapu	Bgy
Maag	Bgy
Manikling	Bgy
Maputi	Bgy
Batobato (Pob.)	Bgy
San Miguel	Bgy
San Roque	Bgy
Santo Rosario	Bgy
Sudlon	Bgy
Talisay	Bgy
Tarragona	Mun
Cabagayan	Bgy
Central (Pob.)	Bgy
Dadong	Bgy
Jovellar	Bgy
Limot	Bgy
Lucatan	Bgy
Maganda	Bgy
Ompao	Bgy
Tomoaong	Bgy
Tubaon	Bgy
Davao de Oro	Prov
Compostela	Mun
Bagongon	Bgy
Gabi	Bgy
Lagab	Bgy
Mangayon	Bgy
Mapaca	Bgy
Maparat	Bgy
New Alegria	Bgy
Ngan	Bgy
Osmeña	Bgy
Panansalan	Bgy
Poblacion	Bgy
San Jose	Bgy
San Miguel	Bgy
Siocon	Bgy
Tamia	Bgy
Aurora	Bgy
Laak	Mun
Aguinaldo	Bgy
Banbanon	Bgy
Binasbas	Bgy
Cebulida	Bgy
Il Papa	Bgy
Kaligutan	Bgy
Kapatagan	Bgy
Kidawa	Bgy
Kilagding	Bgy
Kiokmay	Bgy
Langtud	Bgy
Longanapan	Bgy
Naga	Bgy
Laac (Pob.)	Bgy
San Antonio	Bgy
Amor Cruz	Bgy
Ampawid	Bgy
Andap	Bgy
Anitap	Bgy
Bagong Silang	Bgy
Belmonte	Bgy
Bullucan	Bgy
Concepcion	Bgy
Datu Ampunan	Bgy
Datu Davao	Bgy
Doña Josefa	Bgy
El Katipunan	Bgy
Imelda	Bgy
Inacayan	Bgy
Mabuhay	Bgy
Macopa	Bgy
Malinao	Bgy
Mangloy	Bgy
Melale	Bgy
New Bethlehem	Bgy
Panamoren	Bgy
Sabud	Bgy
Santa Emilia	Bgy
Santo Niño	Bgy
Sisimon	Bgy
Mabini	Mun
Cadunan	Bgy
Pindasan	Bgy
Cuambog (Pob.)	Bgy
Tagnanan	Bgy
Anitapan	Bgy
Cabuyuan	Bgy
Del Pilar	Bgy
Libodon	Bgy
Golden Valley	Bgy
Pangibiran	Bgy
San Antonio	Bgy
Maco	Mun
Anibongan	Bgy
Anislagan	Bgy
Binuangan	Bgy
Bucana	Bgy
Calabcab	Bgy
Concepcion	Bgy
Dumlan	Bgy
Elizalde	Bgy
Pangi	Bgy
Gubatan	Bgy
Hijo	Bgy
Kinuban	Bgy
Langgam	Bgy
Lapu-lapu	Bgy
Libay-libay	Bgy
Limbo	Bgy
Lumatab	Bgy
Magangit	Bgy
Malamodao	Bgy
Manipongol	Bgy
Mapaang	Bgy
Masara	Bgy
New Asturias	Bgy
Panibasan	Bgy
Panoraon	Bgy
Poblacion	Bgy
San Juan	Bgy
San Roque	Bgy
Sangab	Bgy
Taglawig	Bgy
Mainit	Bgy
New Barili	Bgy
New Leyte	Bgy
New Visayas	Bgy
Panangan	Bgy
Tagbaros	Bgy
Teresa	Bgy
Maragusan	Mun
Bagong Silang	Bgy
Mapawa	Bgy
Maragusan (Pob.)	Bgy
New Albay	Bgy
Tupas	Bgy
Bahi	Bgy
Cambagang	Bgy
Coronobe	Bgy
Katipunan	Bgy
Lahi	Bgy
Langgawisan	Bgy
Mabugnao	Bgy
Magcagong	Bgy
Mahayahay	Bgy
Mauswagon	Bgy
New Katipunan	Bgy
New Manay	Bgy
New Panay	Bgy
Paloc	Bgy
Pamintaran	Bgy
Parasanon	Bgy
Talian	Bgy
Tandik	Bgy
Tigbao	Bgy
Mawab	Mun
Andili	Bgy
Bawani	Bgy
Concepcion	Bgy
Malinawon	Bgy
Nueva Visayas	Bgy
Nuevo Iloco	Bgy
Poblacion	Bgy
Salvacion	Bgy
Saosao	Bgy
Sawangan	Bgy
Tuboran	Bgy
Monkayo	Mun
Awao	Bgy
Babag	Bgy
Banlag	Bgy
Baylo	Bgy
Casoon	Bgy
Inambatan	Bgy
Haguimitan	Bgy
Macopa	Bgy
Mamunga	Bgy
Naboc	Bgy
Olaycon	Bgy
Pasian	Bgy
Poblacion	Bgy
Rizal	Bgy
Salvacion	Bgy
San Isidro	Bgy
San Jose	Bgy
Tubo-tubo	Bgy
Upper Ulip	Bgy
Union	Bgy
Mount Diwata	Bgy
Montevista	Mun
Banagbanag	Bgy
Banglasan	Bgy
Bankerohan Norte	Bgy
Bankerohan Sur	Bgy
Camansi	Bgy
Camantangan	Bgy
Concepcion	Bgy
Dauman	Bgy
Canidkid	Bgy
Lebanon	Bgy
Linoan	Bgy
Mayaon	Bgy
New Calape	Bgy
New Dalaguete	Bgy
New Cebulan	Bgy
New Visayas	Bgy
Prosperidad	Bgy
San Jose (Pob.)	Bgy
San Vicente	Bgy
Tapia	Bgy
Nabunturan (Capital)	Mun
Anislagan	Bgy
Antequera	Bgy
Basak	Bgy
Cabacungan	Bgy
Cabidianan	Bgy
Katipunan	Bgy
Libasan	Bgy
Linda	Bgy
Magading	Bgy
Magsaysay	Bgy
Mainit	Bgy
Manat	Bgy
Matilo	Bgy
Mipangi	Bgy
New Dauis	Bgy
New Sibonga	Bgy
Ogao	Bgy
Pangutosan	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Maria	Bgy
Santo Niño	Bgy
Sasa	Bgy
Tagnocon	Bgy
Bayabas	Bgy
Bukal	Bgy
New Bataan	Mun
Bantacan	Bgy
Batinao	Bgy
Camanlangan	Bgy
Cogonon	Bgy
Fatima	Bgy
Katipunan	Bgy
Magsaysay	Bgy
Magangit	Bgy
Pagsabangan	Bgy
Panag	Bgy
Cabinuangan (Pob.)	Bgy
San Roque	Bgy
Andap	Bgy
Kahayag	Bgy
Manurigao	Bgy
Tandawan	Bgy
Pantukan	Mun
Bongabong	Bgy
Bongbong	Bgy
P. Fuentes	Bgy
Kingking (Pob.)	Bgy
Magnaga	Bgy
Matiao	Bgy
Napnapan	Bgy
Tagdangua	Bgy
Tambongon	Bgy
Tibagon	Bgy
Las Arenas	Bgy
Araibo	Bgy
Tag-Ugpo	Bgy
Davao Occidental	Prov
Don Marcelino	Mun
Calian	Bgy
Kiobog	Bgy
North Lamidan	Bgy
Lawa (Pob.)	Bgy
Nueva Villa	Bgy
Talagutong (Pob.)	Bgy
Baluntaya	Bgy
Dalupan	Bgy
Kinanga	Bgy
Lanao	Bgy
Lapuan	Bgy
Linadasan	Bgy
Mabuhay	Bgy
South Lamidan	Bgy
West Lamidan	Bgy
Jose Abad Santos	Mun
Buguis	Bgy
Balangonan	Bgy
Bukid	Bgy
Butuan	Bgy
Butulan	Bgy
Caburan Big	Bgy
Caburan Small (Pob.)	Bgy
Camalian	Bgy
Carahayan	Bgy
Cayaponga	Bgy
Culaman	Bgy
Kalbay	Bgy
Kitayo	Bgy
Magulibas	Bgy
Malalan	Bgy
Mangile	Bgy
Marabutuan	Bgy
Meybio	Bgy
Molmol	Bgy
Nuing	Bgy
Patulang	Bgy
Quiapo	Bgy
San Isidro	Bgy
Sugal	Bgy
Tabayon	Bgy
Tanuman	Bgy
Malita (Capital)	Mun
Bito	Bgy
Bolila	Bgy
Buhangin	Bgy
Culaman	Bgy
Datu Danwata	Bgy
Demoloc	Bgy
Felis	Bgy
Fishing Village	Bgy
Kibalatong	Bgy
Kidalapong	Bgy
Kilalag	Bgy
Kinangan	Bgy
Lacaron	Bgy
Lagumit	Bgy
Lais	Bgy
Little Baguio	Bgy
Macol	Bgy
Mana	Bgy
Manuel Peralta	Bgy
New Argao	Bgy
Pangian	Bgy
Pinalpalan	Bgy
Poblacion	Bgy
Sangay	Bgy
Talogoy	Bgy
Tical	Bgy
Ticulon	Bgy
Tingolo	Bgy
Tubalan	Bgy
Pangaleon	Bgy
Santa Maria	Mun
Basiawan	Bgy
Buca	Bgy
Cadaatan	Bgy
Kidadan	Bgy
Kisulad	Bgy
Malalag Tubig	Bgy
Mamacao	Bgy
Ogpao	Bgy
Poblacion	Bgy
Pongpong	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Juan	Bgy
San Pedro	Bgy
San Roque	Bgy
Tanglad	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Datu Daligasao	Bgy
Datu Intan	Bgy
Kinilidan	Bgy
Sarangani	Mun
Batuganding	Bgy
Konel	Bgy
Lipol	Bgy
Mabila (Pob.)	Bgy
Patuco	Bgy
Laker	Bgy
Tinina	Bgy
Camahual	Bgy
Camalig	Bgy
Gomtago	Bgy
Tagen	Bgy
Tucal	Bgy
Region XII (SOCCSKSARGEN)	Reg
Cotabato	Prov
Alamada	Mun
Bao	Bgy
Barangiran	Bgy
Camansi	Bgy
Dado	Bgy
Guiling	Bgy
Kitacubong (Pob.)	Bgy
Macabasa	Bgy
Malitubog	Bgy
Mapurok	Bgy
Pacao	Bgy
Paruayan	Bgy
Pigcawaran	Bgy
Polayagan	Bgy
Rangayen	Bgy
Lower Dado	Bgy
Mirasol	Bgy
Raradangan	Bgy
Carmen	Mun
Aroman	Bgy
Bentangan	Bgy
Cadiis	Bgy
General Luna	Bgy
Katanayanan	Bgy
Kib-Ayao	Bgy
Kibenes	Bgy
Kibugtongan	Bgy
Kilala	Bgy
Kimadzil	Bgy
Kitulaan	Bgy
Langogan	Bgy
Lanoon	Bgy
Liliongan	Bgy
Ugalingan	Bgy
Macabenban	Bgy
Malapag	Bgy
Manarapan	Bgy
Manili	Bgy
Nasapian	Bgy
Palanggalan	Bgy
Pebpoloan	Bgy
Poblacion	Bgy
Ranzo	Bgy
Tacupan	Bgy
Tambad	Bgy
Tonganon	Bgy
Tupig	Bgy
Kabacan	Mun
Aringay	Bgy
Bangilan	Bgy
Bannawag	Bgy
Buluan	Bgy
Cuyapon	Bgy
Dagupan	Bgy
Katidtuan	Bgy
Kayaga	Bgy
Kilagasan	Bgy
Magatos	Bgy
Malamote	Bgy
Malanduague	Bgy
Nanga-an	Bgy
Osias	Bgy
Paatan Lower	Bgy
Paatan Upper	Bgy
Pedtad	Bgy
Pisan	Bgy
Poblacion	Bgy
Salapungan	Bgy
Sanggadong	Bgy
Simbuhay	Bgy
Simone	Bgy
Tamped	Bgy
City of Kidapawan (Capital)	City
Amas	Bgy
Amazion	Bgy
Balabag	Bgy
Balindog	Bgy
Benoligan	Bgy
Berada	Bgy
Gayola	Bgy
Ginatilan	Bgy
Ilomavis	Bgy
Indangan	Bgy
Junction	Bgy
Kalaisan	Bgy
Kalasuyan	Bgy
Katipunan	Bgy
Lanao	Bgy
Linangcob	Bgy
Luvimin	Bgy
Macabolig	Bgy
Malinan	Bgy
Manongol	Bgy
Marbel	Bgy
Mateo	Bgy
Meochao	Bgy
Mua-an	Bgy
New Bohol	Bgy
Nuangan	Bgy
Onica	Bgy
Paco	Bgy
Patadon	Bgy
Perez	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Roque	Bgy
Santo Niño	Bgy
Sibawan	Bgy
Sikitan	Bgy
Singao	Bgy
Sudapin	Bgy
Sumbao	Bgy
Magsaysay	Bgy
Libungan	Mun
Abaga	Bgy
Baguer	Bgy
Barongis	Bgy
Batiocan	Bgy
Cabaruyan	Bgy
Cabpangi	Bgy
Demapaco	Bgy
Grebona	Bgy
Gumaga	Bgy
Kapayawi	Bgy
Kiloyao	Bgy
Kitubod	Bgy
Malengen	Bgy
Montay	Bgy
Nica-an	Bgy
Palao	Bgy
Poblacion	Bgy
Sinapangan	Bgy
Sinawingan	Bgy
Ulamian	Bgy
Magpet	Mun
Alibayon	Bgy
Bagumbayan	Bgy
Bangkal	Bgy
Bantac	Bgy
Basak	Bgy
Binay	Bgy
Bongolanon	Bgy
Datu Celo	Bgy
Del Pilar	Bgy
Doles	Bgy
Gubatan	Bgy
Ilian	Bgy
Inac	Bgy
Kamada	Bgy
Kauswagan	Bgy
Kisandal	Bgy
Magcaalam	Bgy
Mahongcog	Bgy
Manobo	Bgy
Noa	Bgy
Owas	Bgy
Pangao-an	Bgy
Poblacion	Bgy
Sallab	Bgy
Tagbac	Bgy
Temporan	Bgy
Amabel	Bgy
Balete	Bgy
Don Panaca	Bgy
Imamaling	Bgy
Kinarum	Bgy
Manobisa	Bgy
Makilala	Mun
Batasan	Bgy
Bato	Bgy
Biangan	Bgy
Buena Vida	Bgy
Buhay	Bgy
Bulakanon	Bgy
Cabilao	Bgy
Concepcion	Bgy
Dagupan	Bgy
Garsika	Bgy
Guangan	Bgy
Indangan	Bgy
Jose Rizal	Bgy
Katipunan II	Bgy
Kawayanon	Bgy
Kisante	Bgy
Leboce	Bgy
Libertad	Bgy
Luayon	Bgy
Luna Norte	Bgy
Luna Sur	Bgy
Malabuan	Bgy
Malasila	Bgy
Malungon	Bgy
New Baguio	Bgy
New Bulatukan	Bgy
New Cebu	Bgy
Old Bulatukan	Bgy
Poblacion	Bgy
Rodero	Bgy
Saguing	Bgy
San Vicente	Bgy
Santa Felomina	Bgy
Santo Niño	Bgy
Sinkatulan	Bgy
Taluntalunan	Bgy
Villaflores	Bgy
New Israel	Bgy
Matalam	Mun
New Alimodian	Bgy
Arakan	Bgy
Bangbang	Bgy
Bato	Bgy
Central Malamote	Bgy
Dalapitan	Bgy
Estado	Bgy
Ilian	Bgy
Kabulacan	Bgy
Kibia	Bgy
Kibudoc	Bgy
Kidama	Bgy
Kilada	Bgy
Lampayan	Bgy
Latagan	Bgy
Linao	Bgy
Lower Malamote	Bgy
Manubuan	Bgy
Manupal	Bgy
Marbel	Bgy
Minamaing	Bgy
Natutungan	Bgy
New Bugasong	Bgy
New Pandan	Bgy
Patadon West	Bgy
Poblacion	Bgy
Salvacion	Bgy
Santa Maria	Bgy
Sarayan	Bgy
Taculen	Bgy
Taguranao	Bgy
Tamped	Bgy
New Abra	Bgy
Pinamaton	Bgy
Midsayap	Mun
Agriculture	Bgy
Anonang	Bgy
Arizona	Bgy
Bagumba	Bgy
Baliki	Bgy
Bitoka	Bgy
Bual Norte	Bgy
Bual Sur	Bgy
Central Bulanan	Bgy
Damatulan	Bgy
Central Glad	Bgy
Ilbocean	Bgy
Kadigasan	Bgy
Kadingilan	Bgy
Kapinpilan	Bgy
Central Katingawan	Bgy
Kimagango	Bgy
Kudarangan	Bgy
Central Labas	Bgy
Lagumbingan	Bgy
Lomopog	Bgy
Lower Glad	Bgy
Lower Katingawan	Bgy
Macasendeg	Bgy
Malamote	Bgy
Malingao	Bgy
Milaya	Bgy
Mudseng	Bgy
Nabalawag	Bgy
Nalin	Bgy
Nes	Bgy
Olandang	Bgy
Patindeguen	Bgy
Barangay Poblacion 1	Bgy
Barangay Poblacion 2	Bgy
Barangay Poblacion 3	Bgy
Barangay Poblacion 4	Bgy
Barangay Poblacion 5	Bgy
Barangay Poblacion 6	Bgy
Barangay Poblacion 7	Bgy
Barangay Poblacion 8	Bgy
Palongoguen	Bgy
Rangaban	Bgy
Sadaan	Bgy
Salunayan	Bgy
Sambulawan	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Tugal	Bgy
Tumbras	Bgy
Bulanan Upper	Bgy
Upper Glad I	Bgy
Upper Glad II	Bgy
Upper Labas	Bgy
Villarica	Bgy
Kiwanan	Bgy
San Pedro	Bgy
M'Lang	Mun
Bagontapay	Bgy
Bialong	Bgy
Buayan	Bgy
Calunasan	Bgy
Dalipe	Bgy
Dagong	Bgy
Dungo-an	Bgy
Gaunan	Bgy
Inas	Bgy
Katipunan	Bgy
La Fortuna	Bgy
La Suerte	Bgy
Langkong	Bgy
Lepaga	Bgy
Liboo	Bgy
Lika	Bgy
Luz Village	Bgy
Magallon	Bgy
Malayan	Bgy
New Antique	Bgy
New Barbaza	Bgy
New Kalibo	Bgy
New Consolacion	Bgy
New Esperanza	Bgy
New Janiuay	Bgy
New Lawa-an	Bgy
New Rizal	Bgy
Nueva Vida	Bgy
Pag-asa	Bgy
Poblacion	Bgy
Pulang-lupa	Bgy
Sangat	Bgy
Tawantawan	Bgy
Tibao	Bgy
Ugpay	Bgy
Palma-Perez	Bgy
Poblacion B	Bgy
Pigkawayan	Mun
Anick	Bgy
Upper Baguer	Bgy
Balacayon	Bgy
Balogo	Bgy
Banucagon	Bgy
Bulucaon	Bgy
Buluan	Bgy
Buricain	Bgy
Capayuran	Bgy
Central Panatan	Bgy
Datu Binasing	Bgy
Datu Mantil	Bgy
Kadingilan	Bgy
Kimarayang	Bgy
Libungan Torreta	Bgy
Lower Baguer	Bgy
Lower Pangangkalan	Bgy
Malagakit	Bgy
Maluao	Bgy
North Manuangan	Bgy
Matilac	Bgy
Midpapan I	Bgy
Mulok	Bgy
New Culasi	Bgy
New Igbaras	Bgy
New Panay	Bgy
Upper Pangangkalan	Bgy
Patot	Bgy
Payong-payong	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Presbitero	Bgy
Renibon	Bgy
Simsiman	Bgy
South Manuangan	Bgy
Tigbawan	Bgy
Tubon	Bgy
Midpapan II	Bgy
Cabpangi	Bgy
Pikit	Mun
Bagoaingud	Bgy
Balabak	Bgy
Balatican	Bgy
Balong	Bgy
Balungis	Bgy
Barungis	Bgy
Batulawan	Bgy
Bualan	Bgy
Buliok	Bgy
Bulod	Bgy
Bulol	Bgy
Calawag	Bgy
Dalingaoen	Bgy
Damalasak	Bgy
Fort Pikit	Bgy
Ginatilan	Bgy
Gligli	Bgy
Gokoton	Bgy
Inug-ug	Bgy
Kabasalan	Bgy
Kalacacan	Bgy
Katilacan	Bgy
Kolambog	Bgy
Ladtingan	Bgy
Lagunde	Bgy
Langayen	Bgy
Macabual	Bgy
Macasendeg	Bgy
Manaulanan	Bgy
Nabundas	Bgy
Nalapaan	Bgy
Nunguan	Bgy
Paidu Pulangi	Bgy
Panicupan	Bgy
Poblacion	Bgy
Punol	Bgy
Rajah Muda	Bgy
Silik	Bgy
Takipan	Bgy
Talitay	Bgy
Tinutulan	Bgy
Pamalian	Bgy
President Roxas	Mun
Alegria	Bgy
Bato-bato	Bgy
Del Carmen	Bgy
F. Cajelo	Bgy
Idaoman	Bgy
Ilustre	Bgy
Kamarahan	Bgy
Camasi	Bgy
Kisupaan	Bgy
La Esperanza	Bgy
Labu-o	Bgy
Lamalama	Bgy
Lomonay	Bgy
New Cebu	Bgy
Poblacion	Bgy
Sagcungan	Bgy
Salat	Bgy
Sarayan	Bgy
Tuael	Bgy
Greenhill	Bgy
Cabangbangan	Bgy
Datu Indang	Bgy
Datu Sandongan	Bgy
Kimaruhing	Bgy
Mabuhay	Bgy
Tulunan	Mun
Bagumbayan	Bgy
Banayal	Bgy
Batang	Bgy
Bituan	Bgy
Bual	Bgy
Daig	Bgy
Damawato	Bgy
Dungos	Bgy
Kanibong	Bgy
La Esperanza	Bgy
Lampagang	Bgy
Bunawan	Bgy
Magbok	Bgy
Maybula	Bgy
Minapan	Bgy
New Caridad	Bgy
New Culasi	Bgy
New Panay	Bgy
Paraiso	Bgy
Poblacion	Bgy
Popoyon	Bgy
Sibsib	Bgy
Tambac	Bgy
Tuburan	Bgy
F. Cajelo	Bgy
Bacong	Bgy
Galidan	Bgy
Genoveva Baynosa	Bgy
Nabundasan	Bgy
Antipas	Mun
Camutan	Bgy
Canaan	Bgy
Dolores	Bgy
Kiyaab	Bgy
Luhong	Bgy
Magsaysay	Bgy
Malangag	Bgy
Malatad	Bgy
Malire	Bgy
New Pontevedra	Bgy
Poblacion	Bgy
B. Cadungon	Bgy
Datu Agod	Bgy
Banisilan	Mun
Banisilan Poblacion	Bgy
Busaon	Bgy
Capayangan	Bgy
Carugmanan	Bgy
Gastay	Bgy
Kalawaig	Bgy
Kiaring	Bgy
Malagap	Bgy
Malinao	Bgy
Miguel Macasarte	Bgy
Pantar	Bgy
Paradise	Bgy
Pinamulaan	Bgy
Poblacion II	Bgy
Puting-bato	Bgy
Solama	Bgy
Thailand	Bgy
Tinimbacan	Bgy
Tumbao-Camalig	Bgy
Wadya	Bgy
Aleosan	Mun
Bagolibas	Bgy
Cawilihan	Bgy
Dualing	Bgy
Dunguan	Bgy
Katalicanan	Bgy
Lawili	Bgy
Lower Mingading	Bgy
Luanan	Bgy
Malapang	Bgy
New Leon	Bgy
New Panay	Bgy
Pagangan	Bgy
Palacat	Bgy
Pentil	Bgy
San Mateo	Bgy
Santa Cruz	Bgy
Tapodoc	Bgy
Tomado	Bgy
Upper Mingading	Bgy
Arakan	Mun
Allab	Bgy
Anapolon	Bgy
Badiangon	Bgy
Binoongan	Bgy
Dallag	Bgy
Datu Ladayon	Bgy
Datu Matangkil	Bgy
Doroluman	Bgy
Gambodes	Bgy
Ganatan	Bgy
Greenfield	Bgy
Kabalantian	Bgy
Katipunan	Bgy
Kinawayan	Bgy
Kulaman Valley	Bgy
Lanao Kuran	Bgy
Libertad	Bgy
Makalangot	Bgy
Malibatuan	Bgy
Maria Caridad	Bgy
Meocan	Bgy
Naje	Bgy
Napalico	Bgy
Salasang	Bgy
San Miguel	Bgy
Santo Niño	Bgy
Sumalili	Bgy
Tumanding	Bgy
South Cotabato	Prov
Banga	Mun
Benitez (Pob.)	Bgy
Cabudian	Bgy
Cabuling	Bgy
Cinco	Bgy
Derilon	Bgy
El Nonok	Bgy
Improgo Village (Pob.)	Bgy
Kusan	Bgy
Lam-Apos	Bgy
Lamba	Bgy
Lambingi	Bgy
Lampari	Bgy
Liwanay	Bgy
Malaya	Bgy
Punong Grande	Bgy
Rang-ay	Bgy
Reyes (Pob.)	Bgy
Rizal	Bgy
Rizal Poblacion	Bgy
San Jose	Bgy
San Vicente	Bgy
Yangco Poblacion	Bgy
City of General Santos	City
Baluan	Bgy
Buayan	Bgy
Bula	Bgy
Conel	Bgy
Dadiangas East (Pob.)	Bgy
Katangawan	Bgy
Lagao	Bgy
Labangal	Bgy
Ligaya	Bgy
Mabuhay	Bgy
San Isidro	Bgy
San Jose	Bgy
Sinawal	Bgy
Tambler	Bgy
Tinagacan	Bgy
Apopong	Bgy
Siguel	Bgy
Upper Labay	Bgy
Batomelong	Bgy
Calumpang	Bgy
City Heights	Bgy
Dadiangas North	Bgy
Dadiangas South	Bgy
Dadiangas West	Bgy
Fatima	Bgy
Olympog	Bgy
City of Koronadal (Capital)	City
Assumption	Bgy
Avanceña	Bgy
Cacub	Bgy
Caloocan	Bgy
Carpenter Hill	Bgy
Concepcion	Bgy
Esperanza	Bgy
General Paulino Santos	Bgy
Mabini	Bgy
Magsaysay	Bgy
Mambucal	Bgy
Morales	Bgy
Namnama	Bgy
New Pangasinan	Bgy
Paraiso	Bgy
Zone I (Pob.)	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Rotonda	Bgy
San Isidro	Bgy
San Jose	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Santo Niño	Bgy
Sarabia	Bgy
Zulueta	Bgy
Norala	Mun
Dumaguil	Bgy
Esperanza	Bgy
Kibid	Bgy
Lapuz	Bgy
Liberty	Bgy
Lopez Jaena	Bgy
Matapol	Bgy
Poblacion	Bgy
Puti	Bgy
San Jose	Bgy
San Miguel	Bgy
Simsiman	Bgy
Tinago	Bgy
Benigno Aquino, Jr.	Bgy
Polomolok	Mun
Bentung	Bgy
Crossing Palkan	Bgy
Glamang	Bgy
Kinilis	Bgy
Klinan 6	Bgy
Koronadal Proper	Bgy
Lam-Caliaf	Bgy
Landan	Bgy
Lumakil	Bgy
Maligo	Bgy
Palkan	Bgy
Poblacion	Bgy
Polo	Bgy
Magsaysay	Bgy
Rubber	Bgy
Silway 7	Bgy
Silway 8	Bgy
Sulit	Bgy
Sumbakil	Bgy
Upper Klinan	Bgy
Lapu	Bgy
Cannery Site	Bgy
Pagalungan	Bgy
Surallah	Mun
Buenavista	Bgy
Centrala	Bgy
Colongulo	Bgy
Dajay	Bgy
Duengas	Bgy
Canahay	Bgy
Lambontong	Bgy
Lamian	Bgy
Lamsugod	Bgy
Libertad (Pob.)	Bgy
Little Baguio	Bgy
Moloy	Bgy
Naci	Bgy
Talahik	Bgy
Tubiala	Bgy
Upper Sepaka	Bgy
Veterans	Bgy
Tampakan	Mun
Albagan	Bgy
Kipalbig	Bgy
Lambayong	Bgy
Liberty	Bgy
Maltana	Bgy
Poblacion	Bgy
Tablu	Bgy
Buto	Bgy
Lampitak	Bgy
Palo	Bgy
Pula-bato	Bgy
Danlag	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Tantangan	Mun
Bukay Pait	Bgy
Cabuling	Bgy
Dumadalig	Bgy
Libas	Bgy
Magon	Bgy
Maibo	Bgy
Mangilala	Bgy
New Iloilo	Bgy
New Lambunao	Bgy
Poblacion	Bgy
San Felipe	Bgy
New Cuyapo	Bgy
Tinongcop	Bgy
T'Boli	Mun
Basag	Bgy
Edwards (Pob.)	Bgy
Kematu	Bgy
Laconon	Bgy
Lamsalome	Bgy
New Dumangas	Bgy
Sinolon	Bgy
Lambangan	Bgy
Maan	Bgy
Afus	Bgy
Lambuling	Bgy
Lamhako	Bgy
Poblacion	Bgy
Talcon	Bgy
Talufo	Bgy
Tudok	Bgy
Aflek	Bgy
Datal Bob	Bgy
Desawo	Bgy
Dlanag	Bgy
Lemsnolon	Bgy
Malugong	Bgy
Mongocayo	Bgy
Salacafe	Bgy
T'bolok	Bgy
Tupi	Mun
Acmonan	Bgy
Bololmala	Bgy
Bunao	Bgy
Cebuano	Bgy
Crossing Rubber	Bgy
Kablon	Bgy
Kalkam	Bgy
Linan	Bgy
Lunen	Bgy
Miasong	Bgy
Palian	Bgy
Poblacion	Bgy
Polonuling	Bgy
Simbo	Bgy
Tubeng	Bgy
Santo Niño	Mun
Ambalgan	Bgy
Guinsang-an	Bgy
Katipunan	Bgy
Manuel Roxas	Bgy
Panay	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Vicente	Bgy
Teresita	Bgy
Sajaneba	Bgy
Lake Sebu	Mun
Bacdulong	Bgy
Denlag	Bgy
Halilan	Bgy
Hanoon	Bgy
Klubi	Bgy
Lake Lahit	Bgy
Lamcade	Bgy
Lamdalag	Bgy
Lamfugon	Bgy
Lamlahak	Bgy
Lower Maculan	Bgy
Luhib	Bgy
Ned	Bgy
Poblacion	Bgy
Siluton	Bgy
Talisay	Bgy
Takunel	Bgy
Upper Maculan	Bgy
Tasiman	Bgy
Sultan Kudarat	Prov
Bagumbayan	Mun
Bai Sarifinang	Bgy
Biwang	Bgy
Busok	Bgy
Daguma	Bgy
Kapaya	Bgy
Kinayao	Bgy
Masiag	Bgy
Poblacion	Bgy
South Sepaka	Bgy
Tuka	Bgy
Chua	Bgy
Daluga	Bgy
Kabulanan	Bgy
Kanulay	Bgy
Monteverde	Bgy
Santo Niño	Bgy
Sumilil	Bgy
Titulok	Bgy
Sison	Bgy
Columbio	Mun
Bantangan	Bgy
Datablao	Bgy
Eday	Bgy
Elbebe	Bgy
Libertad	Bgy
Lomoyon	Bgy
Makat	Bgy
Maligaya	Bgy
Mayo	Bgy
Natividad	Bgy
Poblacion	Bgy
Polomolok	Bgy
Sinapulan	Bgy
Sucob	Bgy
Telafas	Bgy
Lasak	Bgy
Esperanza	Mun
Ala	Bgy
Daladap	Bgy
Dukay	Bgy
Guiamalia	Bgy
Ilian	Bgy
Kangkong	Bgy
Margues	Bgy
New Panay	Bgy
Numo	Bgy
Pamantingan	Bgy
Poblacion	Bgy
Sagasa	Bgy
Salabaca	Bgy
Villamor	Bgy
Laguinding	Bgy
Magsaysay	Bgy
Paitan	Bgy
Saliao	Bgy
Salumping	Bgy
Isulan (Capital)	Mun
Bambad	Bgy
Bual	Bgy
D'Lotilla	Bgy
Dansuli	Bgy
Impao	Bgy
Kalawag I (Pob.)	Bgy
Kalawag II (Pob.)	Bgy
Kalawag III (Pob.)	Bgy
Kenram	Bgy
Kudanding	Bgy
Kolambog	Bgy
Lagandang	Bgy
Laguilayan	Bgy
Mapantig	Bgy
New Pangasinan	Bgy
Sampao	Bgy
Tayugo	Bgy
Kalamansig	Mun
Bantogon	Bgy
Cadiz	Bgy
Dumangas Nuevo	Bgy
Hinalaan	Bgy
Limulan	Bgy
Obial	Bgy
Paril	Bgy
Poblacion	Bgy
Sangay	Bgy
Santa Maria	Bgy
Datu Ito Andong	Bgy
Datu Wasay	Bgy
Nalilidan	Bgy
Sabanal	Bgy
Pag-asa	Bgy
Lebak	Mun
Barurao	Bgy
Basak	Bgy
Bululawan	Bgy
Capilan	Bgy
Christiannuevo	Bgy
Datu Karon	Bgy
Kalamongog	Bgy
Keytodac	Bgy
Kinodalan	Bgy
New Calinog	Bgy
Nuling	Bgy
Pansud	Bgy
Pasandalan	Bgy
Poblacion	Bgy
Poloy-poloy	Bgy
Purikay	Bgy
Ragandang	Bgy
Salaman	Bgy
Salangsang	Bgy
Taguisa	Bgy
Tibpuan	Bgy
Tran	Bgy
Villamonte	Bgy
Barurao II	Bgy
Aurelio F. Freires	Bgy
Poblacion III	Bgy
Bolebok	Bgy
Lutayan	Mun
Antong	Bgy
Bayasong	Bgy
Blingkong	Bgy
Lutayan Proper	Bgy
Maindang	Bgy
Mamali	Bgy
Manili	Bgy
Sampao	Bgy
Sisiman	Bgy
Tamnag (Pob.)	Bgy
Palavilla	Bgy
Lambayong	Mun
Caridad	Bgy
Didtaras	Bgy
Gansing	Bgy
Kabulakan	Bgy
Kapingkong	Bgy
Katitisan	Bgy
Lagao	Bgy
Lilit	Bgy
Madanding	Bgy
Maligaya	Bgy
Mamali	Bgy
Matiompong	Bgy
Midtapok	Bgy
New Cebu	Bgy
Palumbi	Bgy
Pidtiguian	Bgy
Pimbalayan	Bgy
Pinguiaman	Bgy
Poblacion	Bgy
Sadsalan	Bgy
Seneben	Bgy
Sigayan	Bgy
Tambak	Bgy
Tinumigues	Bgy
Tumiao	Bgy
Udtong	Bgy
Palimbang	Mun
Akol	Bgy
Badiangon	Bgy
Baliango	Bgy
Baranayan	Bgy
Barongis	Bgy
Batang-baglas	Bgy
Butril	Bgy
Domolol	Bgy
Kabuling	Bgy
Kalibuhan	Bgy
Kanipaan	Bgy
Kisek	Bgy
Kidayan	Bgy
Kiponget	Bgy
Kulong-kulong	Bgy
Kraan	Bgy
Langali	Bgy
Libua	Bgy
Lumitan	Bgy
Maganao	Bgy
Maguid	Bgy
Malatuneng	Bgy
Malisbong	Bgy
Milbuk	Bgy
Molon	Bgy
Namat Masla	Bgy
Napnapon	Bgy
Poblacion	Bgy
San Roque	Bgy
Colobe	Bgy
Tibuhol	Bgy
Wal	Bgy
Bambanen	Bgy
Lopoken	Bgy
Mina	Bgy
Medol	Bgy
Wasag	Bgy
Balwan	Bgy
Ligao	Bgy
Datu Maguiales	Bgy
President Quirino	Mun
Bagumbayan	Bgy
Bannawag	Bgy
Bayawa	Bgy
Estrella	Bgy
Kalanawe I	Bgy
Kalanawe II	Bgy
Katico	Bgy
Malingon	Bgy
Mangalen	Bgy
C. Mangilala	Bgy
Pedtubo	Bgy
Poblacion	Bgy
Romualdez	Bgy
San Jose	Bgy
Sinakulay	Bgy
Suben	Bgy
Tinaungan	Bgy
Tual	Bgy
San Pedro	Bgy
City of Tacurong	City
Baras	Bgy
Buenaflor	Bgy
Calean	Bgy
Carmen	Bgy
D'Ledesma	Bgy
Virginia Griño	Bgy
Kalandagan	Bgy
Enrique JC Montilla	Bgy
New Isabela	Bgy
New Lagao	Bgy
New Passi	Bgy
Poblacion	Bgy
Rajah Nuda	Bgy
San Antonio	Bgy
San Emmanuel	Bgy
San Pablo	Bgy
Upper Katungal	Bgy
Tina	Bgy
San Rafael	Bgy
Lancheta	Bgy
Sen. Ninoy Aquino	Mun
Banali	Bgy
Basag	Bgy
Buenaflores	Bgy
Bugso	Bgy
Buklod	Bgy
Gapok	Bgy
Kadi	Bgy
Kapatagan	Bgy
Kiadsam	Bgy
Kuden	Bgy
Kulaman	Bgy
Lagubang	Bgy
Langgal	Bgy
Limuhay	Bgy
Malegdeg	Bgy
Midtungok	Bgy
Nati	Bgy
Sewod	Bgy
Tacupis	Bgy
Tinalon	Bgy
Sarangani	Prov
Alabel (Capital)	Mun
Alegria	Bgy
Bagacay	Bgy
Baluntay	Bgy
Datal Anggas	Bgy
Domolok	Bgy
Kawas	Bgy
Maribulan	Bgy
Pag-Asa	Bgy
Paraiso	Bgy
Poblacion	Bgy
Spring	Bgy
Tokawal	Bgy
Ladol	Bgy
Glan	Mun
Baliton	Bgy
Batotuling	Bgy
Batulaki	Bgy
Big Margus	Bgy
Burias	Bgy
Cablalan	Bgy
Calabanit	Bgy
Calpidong	Bgy
Congan	Bgy
Cross	Bgy
Datalbukay	Bgy
E. Alegado	Bgy
Glan Padidu	Bgy
Gumasa	Bgy
Ilaya	Bgy
Kaltuad	Bgy
Kapatan	Bgy
Lago	Bgy
Laguimit	Bgy
Mudan	Bgy
New Aklan	Bgy
Pangyan	Bgy
Poblacion	Bgy
Rio Del Pilar	Bgy
San Jose	Bgy
San Vicente	Bgy
Small Margus	Bgy
Sufatubo	Bgy
Taluya	Bgy
Tango	Bgy
Tapon	Bgy
Kiamba	Mun
Badtasan	Bgy
Datu Dani	Bgy
Gasi	Bgy
Kapate	Bgy
Katubao	Bgy
Kayupo	Bgy
Kling	Bgy
Lagundi	Bgy
Lebe	Bgy
Lomuyon	Bgy
Luma	Bgy
Maligang	Bgy
Nalus	Bgy
Poblacion	Bgy
Salakit	Bgy
Suli	Bgy
Tablao	Bgy
Tamadang	Bgy
Tambilil	Bgy
Maasim	Mun
Amsipit	Bgy
Bales	Bgy
Colon	Bgy
Daliao	Bgy
Kabatiol	Bgy
Kablacan	Bgy
Kamanga	Bgy
Kanalo	Bgy
Lumasal	Bgy
Lumatil	Bgy
Malbang	Bgy
Nomoh	Bgy
Pananag	Bgy
Poblacion	Bgy
Seven Hills	Bgy
Tinoto	Bgy
Maitum	Mun
Bati-an	Bgy
Kalaneg	Bgy
Kalaong	Bgy
Kiambing	Bgy
Kiayap	Bgy
Mabay	Bgy
Maguling	Bgy
Malalag (Pob.)	Bgy
Mindupok	Bgy
New La Union	Bgy
Old Poblacion	Bgy
Pangi	Bgy
Pinol	Bgy
Sison	Bgy
Ticulab	Bgy
Tuanadatu	Bgy
Upo	Bgy
Wali	Bgy
Zion	Bgy
Malapatan	Mun
Daan Suyan	Bgy
Kihan	Bgy
Kinam	Bgy
Libi	Bgy
Lun Masla	Bgy
Lun Padidu	Bgy
Patag	Bgy
Poblacion	Bgy
Sapu Masla	Bgy
Sapu Padidu	Bgy
Tuyan	Bgy
Upper Suyan	Bgy
Malungon	Mun
Alkikan	Bgy
Ampon	Bgy
Atlae	Bgy
Banahaw	Bgy
Banate	Bgy
B'Laan	Bgy
Datal Batong	Bgy
Datal Bila	Bgy
Datal Tampal	Bgy
J.P. Laurel	Bgy
Kawayan	Bgy
Kibala	Bgy
Kiblat	Bgy
Kinabalan	Bgy
Lower Mainit	Bgy
Lutay	Bgy
Malabod	Bgy
Malalag Cogon	Bgy
Malandag	Bgy
Malungon Gamay	Bgy
Nagpan	Bgy
Panamin	Bgy
Poblacion	Bgy
San Juan	Bgy
San Miguel	Bgy
San Roque	Bgy
Talus	Bgy
Tamban	Bgy
Upper Biangan	Bgy
Upper Lumabat	Bgy
Upper Mainit	Bgy
City of Cotabato (Not a Province)	
City of Cotabato	City
Bagua	Bgy
Kalanganan	Bgy
Poblacion	Bgy
Rosary Heights	Bgy
Tamontaka	Bgy
Bagua I	Bgy
Bagua II	Bgy
Bagua III	Bgy
Kalanganan I	Bgy
Kalanganan II	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
Poblacion VI	Bgy
Poblacion VII	Bgy
Poblacion VIII	Bgy
Poblacion IX	Bgy
Rosary Heights I	Bgy
Rosary Heights II	Bgy
Rosary Heights III	Bgy
Rosary Heights IV	Bgy
Rosary Heights V	Bgy
Rosary Heights VI	Bgy
Rosary Heights VII	Bgy
Rosary Heights VIII	Bgy
Rosary Heights IX	Bgy
Rosary Heights X	Bgy
Rosary Heights XI	Bgy
Rosary Heights XII	Bgy
Rosary Heights XIII	Bgy
Tamontaka I	Bgy
Tamontaka II	Bgy
Tamontaka III	Bgy
Tamontaka IV	Bgy
Tamontaka V	Bgy
National Capital Region (NCR)	Reg
NCR, City of Manila, First District (Not a Province)	Dist
City of Manila	City
Tondo I/II	SubMun
Barangay 1	Bgy
Barangay 2	Bgy
Barangay 3	Bgy
Barangay 4	Bgy
Barangay 5	Bgy
Barangay 6	Bgy
Barangay 7	Bgy
Barangay 8	Bgy
Barangay 9	Bgy
Barangay 10	Bgy
Barangay 11	Bgy
Barangay 12	Bgy
Barangay 13	Bgy
Barangay 14	Bgy
Barangay 15	Bgy
Barangay 16	Bgy
Barangay 17	Bgy
Barangay 18	Bgy
Barangay 19	Bgy
Barangay 20	Bgy
Barangay 25	Bgy
Barangay 26	Bgy
Barangay 28	Bgy
Barangay 29	Bgy
Barangay 30	Bgy
Barangay 31	Bgy
Barangay 32	Bgy
Barangay 33	Bgy
Barangay 34	Bgy
Barangay 35	Bgy
Barangay 36	Bgy
Barangay 37	Bgy
Barangay 38	Bgy
Barangay 39	Bgy
Barangay 41	Bgy
Barangay 42	Bgy
Barangay 43	Bgy
Barangay 44	Bgy
Barangay 45	Bgy
Barangay 46	Bgy
Barangay 47	Bgy
Barangay 48	Bgy
Barangay 49	Bgy
Barangay 50	Bgy
Barangay 51	Bgy
Barangay 52	Bgy
Barangay 53	Bgy
Barangay 54	Bgy
Barangay 55	Bgy
Barangay 56	Bgy
Barangay 57	Bgy
Barangay 58	Bgy
Barangay 59	Bgy
Barangay 60	Bgy
Barangay 61	Bgy
Barangay 62	Bgy
Barangay 63	Bgy
Barangay 64	Bgy
Barangay 65	Bgy
Barangay 66	Bgy
Barangay 67	Bgy
Barangay 68	Bgy
Barangay 69	Bgy
Barangay 70	Bgy
Barangay 71	Bgy
Barangay 72	Bgy
Barangay 73	Bgy
Barangay 74	Bgy
Barangay 75	Bgy
Barangay 76	Bgy
Barangay 77	Bgy
Barangay 78	Bgy
Barangay 79	Bgy
Barangay 80	Bgy
Barangay 81	Bgy
Barangay 82	Bgy
Barangay 83	Bgy
Barangay 84	Bgy
Barangay 85	Bgy
Barangay 86	Bgy
Barangay 87	Bgy
Barangay 88	Bgy
Barangay 89	Bgy
Barangay 90	Bgy
Barangay 91	Bgy
Barangay 92	Bgy
Barangay 93	Bgy
Barangay 94	Bgy
Barangay 95	Bgy
Barangay 96	Bgy
Barangay 97	Bgy
Barangay 98	Bgy
Barangay 99	Bgy
Barangay 100	Bgy
Barangay 101	Bgy
Barangay 102	Bgy
Barangay 103	Bgy
Barangay 104	Bgy
Barangay 105	Bgy
Barangay 106	Bgy
Barangay 107	Bgy
Barangay 108	Bgy
Barangay 109	Bgy
Barangay 110	Bgy
Barangay 111	Bgy
Barangay 112	Bgy
Barangay 116	Bgy
Barangay 117	Bgy
Barangay 118	Bgy
Barangay 119	Bgy
Barangay 120	Bgy
Barangay 121	Bgy
Barangay 122	Bgy
Barangay 123	Bgy
Barangay 124	Bgy
Barangay 125	Bgy
Barangay 126	Bgy
Barangay 127	Bgy
Barangay 128	Bgy
Barangay 129	Bgy
Barangay 130	Bgy
Barangay 131	Bgy
Barangay 132	Bgy
Barangay 133	Bgy
Barangay 134	Bgy
Barangay 135	Bgy
Barangay 136	Bgy
Barangay 137	Bgy
Barangay 138	Bgy
Barangay 139	Bgy
Barangay 140	Bgy
Barangay 141	Bgy
Barangay 142	Bgy
Barangay 143	Bgy
Barangay 144	Bgy
Barangay 145	Bgy
Barangay 146	Bgy
Barangay 147	Bgy
Barangay 148	Bgy
Barangay 149	Bgy
Barangay 150	Bgy
Barangay 151	Bgy
Barangay 152	Bgy
Barangay 153	Bgy
Barangay 154	Bgy
Barangay 155	Bgy
Barangay 156	Bgy
Barangay 157	Bgy
Barangay 158	Bgy
Barangay 159	Bgy
Barangay 160	Bgy
Barangay 161	Bgy
Barangay 162	Bgy
Barangay 163	Bgy
Barangay 164	Bgy
Barangay 165	Bgy
Barangay 166	Bgy
Barangay 167	Bgy
Barangay 168	Bgy
Barangay 169	Bgy
Barangay 170	Bgy
Barangay 171	Bgy
Barangay 172	Bgy
Barangay 173	Bgy
Barangay 174	Bgy
Barangay 175	Bgy
Barangay 176	Bgy
Barangay 177	Bgy
Barangay 178	Bgy
Barangay 179	Bgy
Barangay 180	Bgy
Barangay 181	Bgy
Barangay 182	Bgy
Barangay 183	Bgy
Barangay 184	Bgy
Barangay 185	Bgy
Barangay 186	Bgy
Barangay 187	Bgy
Barangay 188	Bgy
Barangay 189	Bgy
Barangay 190	Bgy
Barangay 191	Bgy
Barangay 192	Bgy
Barangay 193	Bgy
Barangay 194	Bgy
Barangay 195	Bgy
Barangay 196	Bgy
Barangay 197	Bgy
Barangay 198	Bgy
Barangay 199	Bgy
Barangay 200	Bgy
Barangay 201	Bgy
Barangay 202	Bgy
Barangay 202-A	Bgy
Barangay 203	Bgy
Barangay 204	Bgy
Barangay 205	Bgy
Barangay 206	Bgy
Barangay 207	Bgy
Barangay 208	Bgy
Barangay 209	Bgy
Barangay 210	Bgy
Barangay 211	Bgy
Barangay 212	Bgy
Barangay 213	Bgy
Barangay 214	Bgy
Barangay 215	Bgy
Barangay 216	Bgy
Barangay 217	Bgy
Barangay 218	Bgy
Barangay 219	Bgy
Barangay 220	Bgy
Barangay 221	Bgy
Barangay 222	Bgy
Barangay 223	Bgy
Barangay 224	Bgy
Barangay 225	Bgy
Barangay 226	Bgy
Barangay 227	Bgy
Barangay 228	Bgy
Barangay 229	Bgy
Barangay 230	Bgy
Barangay 231	Bgy
Barangay 232	Bgy
Barangay 233	Bgy
Barangay 234	Bgy
Barangay 235	Bgy
Barangay 236	Bgy
Barangay 237	Bgy
Barangay 238	Bgy
Barangay 239	Bgy
Barangay 240	Bgy
Barangay 241	Bgy
Barangay 242	Bgy
Barangay 243	Bgy
Barangay 244	Bgy
Barangay 245	Bgy
Barangay 246	Bgy
Barangay 247	Bgy
Barangay 248	Bgy
Barangay 249	Bgy
Barangay 250	Bgy
Barangay 251	Bgy
Barangay 252	Bgy
Barangay 253	Bgy
Barangay 254	Bgy
Barangay 255	Bgy
Barangay 256	Bgy
Barangay 257	Bgy
Barangay 258	Bgy
Barangay 259	Bgy
Barangay 260	Bgy
Barangay 261	Bgy
Barangay 262	Bgy
Barangay 263	Bgy
Barangay 264	Bgy
Barangay 265	Bgy
Barangay 266	Bgy
Barangay 267	Bgy
Binondo	SubMun
Barangay 287	Bgy
Barangay 288	Bgy
Barangay 289	Bgy
Barangay 290	Bgy
Barangay 291	Bgy
Barangay 292	Bgy
Barangay 293	Bgy
Barangay 294	Bgy
Barangay 295	Bgy
Barangay 296	Bgy
Quiapo	SubMun
Barangay 383	Bgy
Barangay 384	Bgy
Barangay 385	Bgy
Barangay 386	Bgy
Barangay 387	Bgy
Barangay 388	Bgy
Barangay 389	Bgy
Barangay 390	Bgy
Barangay 391	Bgy
Barangay 392	Bgy
Barangay 393	Bgy
Barangay 394	Bgy
Barangay 306	Bgy
Barangay 307	Bgy
Barangay 308	Bgy
Barangay 309	Bgy
San Nicolas	SubMun
Barangay 268	Bgy
Barangay 269	Bgy
Barangay 270	Bgy
Barangay 271	Bgy
Barangay 272	Bgy
Barangay 273	Bgy
Barangay 274	Bgy
Barangay 275	Bgy
Barangay 276	Bgy
Barangay 281	Bgy
Barangay 282	Bgy
Barangay 283	Bgy
Barangay 284	Bgy
Barangay 285	Bgy
Barangay 286	Bgy
Santa Cruz	SubMun
Barangay 297	Bgy
Barangay 298	Bgy
Barangay 299	Bgy
Barangay 300	Bgy
Barangay 301	Bgy
Barangay 302	Bgy
Barangay 303	Bgy
Barangay 304	Bgy
Barangay 305	Bgy
Barangay 310	Bgy
Barangay 311	Bgy
Barangay 312	Bgy
Barangay 313	Bgy
Barangay 314	Bgy
Barangay 315	Bgy
Barangay 316	Bgy
Barangay 317	Bgy
Barangay 318	Bgy
Barangay 319	Bgy
Barangay 320	Bgy
Barangay 321	Bgy
Barangay 322	Bgy
Barangay 323	Bgy
Barangay 324	Bgy
Barangay 325	Bgy
Barangay 326	Bgy
Barangay 327	Bgy
Barangay 328	Bgy
Barangay 329	Bgy
Barangay 330	Bgy
Barangay 331	Bgy
Barangay 332	Bgy
Barangay 333	Bgy
Barangay 334	Bgy
Barangay 335	Bgy
Barangay 336	Bgy
Barangay 337	Bgy
Barangay 338	Bgy
Barangay 339	Bgy
Barangay 340	Bgy
Barangay 341	Bgy
Barangay 342	Bgy
Barangay 343	Bgy
Barangay 344	Bgy
Barangay 345	Bgy
Barangay 346	Bgy
Barangay 347	Bgy
Barangay 348	Bgy
Barangay 349	Bgy
Barangay 350	Bgy
Barangay 351	Bgy
Barangay 352	Bgy
Barangay 353	Bgy
Barangay 354	Bgy
Barangay 355	Bgy
Barangay 356	Bgy
Barangay 357	Bgy
Barangay 358	Bgy
Barangay 359	Bgy
Barangay 360	Bgy
Barangay 361	Bgy
Barangay 362	Bgy
Barangay 363	Bgy
Barangay 364	Bgy
Barangay 365	Bgy
Barangay 366	Bgy
Barangay 367	Bgy
Barangay 368	Bgy
Barangay 369	Bgy
Barangay 370	Bgy
Barangay 371	Bgy
Barangay 372	Bgy
Barangay 373	Bgy
Barangay 374	Bgy
Barangay 375	Bgy
Barangay 376	Bgy
Barangay 377	Bgy
Barangay 378	Bgy
Barangay 379	Bgy
Barangay 380	Bgy
Barangay 381	Bgy
Barangay 382	Bgy
Sampaloc	SubMun
Barangay 395	Bgy
Barangay 396	Bgy
Barangay 397	Bgy
Barangay 398	Bgy
Barangay 399	Bgy
Barangay 400	Bgy
Barangay 401	Bgy
Barangay 402	Bgy
Barangay 403	Bgy
Barangay 404	Bgy
Barangay 405	Bgy
Barangay 406	Bgy
Barangay 407	Bgy
Barangay 408	Bgy
Barangay 409	Bgy
Barangay 410	Bgy
Barangay 411	Bgy
Barangay 412	Bgy
Barangay 413	Bgy
Barangay 414	Bgy
Barangay 415	Bgy
Barangay 416	Bgy
Barangay 417	Bgy
Barangay 418	Bgy
Barangay 419	Bgy
Barangay 420	Bgy
Barangay 421	Bgy
Barangay 422	Bgy
Barangay 423	Bgy
Barangay 424	Bgy
Barangay 425	Bgy
Barangay 426	Bgy
Barangay 427	Bgy
Barangay 428	Bgy
Barangay 429	Bgy
Barangay 430	Bgy
Barangay 431	Bgy
Barangay 432	Bgy
Barangay 433	Bgy
Barangay 434	Bgy
Barangay 435	Bgy
Barangay 436	Bgy
Barangay 437	Bgy
Barangay 438	Bgy
Barangay 439	Bgy
Barangay 440	Bgy
Barangay 441	Bgy
Barangay 442	Bgy
Barangay 443	Bgy
Barangay 444	Bgy
Barangay 445	Bgy
Barangay 446	Bgy
Barangay 447	Bgy
Barangay 448	Bgy
Barangay 449	Bgy
Barangay 450	Bgy
Barangay 451	Bgy
Barangay 452	Bgy
Barangay 453	Bgy
Barangay 454	Bgy
Barangay 455	Bgy
Barangay 456	Bgy
Barangay 457	Bgy
Barangay 458	Bgy
Barangay 459	Bgy
Barangay 460	Bgy
Barangay 461	Bgy
Barangay 462	Bgy
Barangay 463	Bgy
Barangay 464	Bgy
Barangay 465	Bgy
Barangay 466	Bgy
Barangay 467	Bgy
Barangay 468	Bgy
Barangay 469	Bgy
Barangay 470	Bgy
Barangay 471	Bgy
Barangay 472	Bgy
Barangay 473	Bgy
Barangay 474	Bgy
Barangay 475	Bgy
Barangay 476	Bgy
Barangay 477	Bgy
Barangay 478	Bgy
Barangay 479	Bgy
Barangay 480	Bgy
Barangay 481	Bgy
Barangay 482	Bgy
Barangay 483	Bgy
Barangay 484	Bgy
Barangay 485	Bgy
Barangay 486	Bgy
Barangay 487	Bgy
Barangay 488	Bgy
Barangay 489	Bgy
Barangay 490	Bgy
Barangay 491	Bgy
Barangay 492	Bgy
Barangay 493	Bgy
Barangay 494	Bgy
Barangay 495	Bgy
Barangay 496	Bgy
Barangay 497	Bgy
Barangay 498	Bgy
Barangay 499	Bgy
Barangay 500	Bgy
Barangay 501	Bgy
Barangay 502	Bgy
Barangay 503	Bgy
Barangay 504	Bgy
Barangay 505	Bgy
Barangay 506	Bgy
Barangay 507	Bgy
Barangay 508	Bgy
Barangay 509	Bgy
Barangay 510	Bgy
Barangay 511	Bgy
Barangay 512	Bgy
Barangay 513	Bgy
Barangay 514	Bgy
Barangay 515	Bgy
Barangay 516	Bgy
Barangay 517	Bgy
Barangay 518	Bgy
Barangay 519	Bgy
Barangay 520	Bgy
Barangay 521	Bgy
Barangay 522	Bgy
Barangay 523	Bgy
Barangay 524	Bgy
Barangay 525	Bgy
Barangay 526	Bgy
Barangay 527	Bgy
Barangay 528	Bgy
Barangay 529	Bgy
Barangay 530	Bgy
Barangay 531	Bgy
Barangay 532	Bgy
Barangay 533	Bgy
Barangay 534	Bgy
Barangay 535	Bgy
Barangay 536	Bgy
Barangay 537	Bgy
Barangay 538	Bgy
Barangay 539	Bgy
Barangay 540	Bgy
Barangay 541	Bgy
Barangay 542	Bgy
Barangay 543	Bgy
Barangay 544	Bgy
Barangay 545	Bgy
Barangay 546	Bgy
Barangay 547	Bgy
Barangay 548	Bgy
Barangay 549	Bgy
Barangay 550	Bgy
Barangay 551	Bgy
Barangay 552	Bgy
Barangay 553	Bgy
Barangay 554	Bgy
Barangay 555	Bgy
Barangay 556	Bgy
Barangay 557	Bgy
Barangay 558	Bgy
Barangay 559	Bgy
Barangay 560	Bgy
Barangay 561	Bgy
Barangay 562	Bgy
Barangay 563	Bgy
Barangay 564	Bgy
Barangay 565	Bgy
Barangay 566	Bgy
Barangay 567	Bgy
Barangay 568	Bgy
Barangay 569	Bgy
Barangay 570	Bgy
Barangay 571	Bgy
Barangay 572	Bgy
Barangay 573	Bgy
Barangay 574	Bgy
Barangay 575	Bgy
Barangay 576	Bgy
Barangay 577	Bgy
Barangay 578	Bgy
Barangay 579	Bgy
Barangay 580	Bgy
Barangay 581	Bgy
Barangay 582	Bgy
Barangay 583	Bgy
Barangay 584	Bgy
Barangay 585	Bgy
Barangay 586	Bgy
Barangay 587	Bgy
Barangay 587-A	Bgy
Barangay 588	Bgy
Barangay 589	Bgy
Barangay 590	Bgy
Barangay 591	Bgy
Barangay 592	Bgy
Barangay 593	Bgy
Barangay 594	Bgy
Barangay 595	Bgy
Barangay 596	Bgy
Barangay 597	Bgy
Barangay 598	Bgy
Barangay 599	Bgy
Barangay 600	Bgy
Barangay 601	Bgy
Barangay 602	Bgy
Barangay 603	Bgy
Barangay 604	Bgy
Barangay 605	Bgy
Barangay 606	Bgy
Barangay 607	Bgy
Barangay 608	Bgy
Barangay 609	Bgy
Barangay 610	Bgy
Barangay 611	Bgy
Barangay 612	Bgy
Barangay 613	Bgy
Barangay 614	Bgy
Barangay 615	Bgy
Barangay 616	Bgy
Barangay 617	Bgy
Barangay 618	Bgy
Barangay 619	Bgy
Barangay 620	Bgy
Barangay 621	Bgy
Barangay 622	Bgy
Barangay 623	Bgy
Barangay 624	Bgy
Barangay 625	Bgy
Barangay 626	Bgy
Barangay 627	Bgy
Barangay 628	Bgy
Barangay 629	Bgy
Barangay 630	Bgy
Barangay 631	Bgy
Barangay 632	Bgy
Barangay 633	Bgy
Barangay 634	Bgy
Barangay 635	Bgy
Barangay 636	Bgy
San Miguel	SubMun
Barangay 637	Bgy
Barangay 638	Bgy
Barangay 639	Bgy
Barangay 640	Bgy
Barangay 641	Bgy
Barangay 642	Bgy
Barangay 643	Bgy
Barangay 644	Bgy
Barangay 645	Bgy
Barangay 646	Bgy
Barangay 647	Bgy
Barangay 648	Bgy
Ermita	SubMun
Barangay 659	Bgy
Barangay 659-A	Bgy
Barangay 660	Bgy
Barangay 660-A	Bgy
Barangay 661	Bgy
Barangay 666	Bgy
Barangay 667	Bgy
Barangay 668	Bgy
Barangay 669	Bgy
Barangay 670	Bgy
Barangay 663	Bgy
Barangay 663-A	Bgy
Barangay 664	Bgy
Intramuros	SubMun
Barangay 654	Bgy
Barangay 655	Bgy
Barangay 656	Bgy
Barangay 657	Bgy
Barangay 658	Bgy
Malate	SubMun
Barangay 689	Bgy
Barangay 690	Bgy
Barangay 691	Bgy
Barangay 692	Bgy
Barangay 693	Bgy
Barangay 694	Bgy
Barangay 695	Bgy
Barangay 696	Bgy
Barangay 697	Bgy
Barangay 698	Bgy
Barangay 699	Bgy
Barangay 700	Bgy
Barangay 701	Bgy
Barangay 702	Bgy
Barangay 703	Bgy
Barangay 704	Bgy
Barangay 705	Bgy
Barangay 706	Bgy
Barangay 707	Bgy
Barangay 708	Bgy
Barangay 709	Bgy
Barangay 710	Bgy
Barangay 711	Bgy
Barangay 712	Bgy
Barangay 713	Bgy
Barangay 714	Bgy
Barangay 715	Bgy
Barangay 716	Bgy
Barangay 717	Bgy
Barangay 718	Bgy
Barangay 719	Bgy
Barangay 720	Bgy
Barangay 721	Bgy
Barangay 722	Bgy
Barangay 723	Bgy
Barangay 724	Bgy
Barangay 725	Bgy
Barangay 726	Bgy
Barangay 727	Bgy
Barangay 728	Bgy
Barangay 729	Bgy
Barangay 730	Bgy
Barangay 731	Bgy
Barangay 732	Bgy
Barangay 733	Bgy
Barangay 738	Bgy
Barangay 739	Bgy
Barangay 740	Bgy
Barangay 741	Bgy
Barangay 742	Bgy
Barangay 743	Bgy
Barangay 744	Bgy
Barangay 688	Bgy
Barangay 735	Bgy
Barangay 736	Bgy
Barangay 737	Bgy
Barangay 734	Bgy
Paco	SubMun
Barangay 662	Bgy
Barangay 664-A	Bgy
Barangay 671	Bgy
Barangay 672	Bgy
Barangay 673	Bgy
Barangay 674	Bgy
Barangay 675	Bgy
Barangay 676	Bgy
Barangay 677	Bgy
Barangay 678	Bgy
Barangay 679	Bgy
Barangay 680	Bgy
Barangay 681	Bgy
Barangay 682	Bgy
Barangay 683	Bgy
Barangay 684	Bgy
Barangay 685	Bgy
Barangay 809	Bgy
Barangay 810	Bgy
Barangay 811	Bgy
Barangay 812	Bgy
Barangay 813	Bgy
Barangay 814	Bgy
Barangay 815	Bgy
Barangay 816	Bgy
Barangay 817	Bgy
Barangay 818	Bgy
Barangay 819	Bgy
Barangay 820	Bgy
Barangay 821	Bgy
Barangay 822	Bgy
Barangay 823	Bgy
Barangay 824	Bgy
Barangay 825	Bgy
Barangay 826	Bgy
Barangay 827	Bgy
Barangay 828	Bgy
Barangay 829	Bgy
Barangay 830	Bgy
Barangay 831	Bgy
Barangay 832	Bgy
Barangay 686	Bgy
Barangay 687	Bgy
Pandacan	SubMun
Barangay 833	Bgy
Barangay 834	Bgy
Barangay 835	Bgy
Barangay 836	Bgy
Barangay 837	Bgy
Barangay 838	Bgy
Barangay 839	Bgy
Barangay 840	Bgy
Barangay 841	Bgy
Barangay 842	Bgy
Barangay 843	Bgy
Barangay 844	Bgy
Barangay 845	Bgy
Barangay 846	Bgy
Barangay 847	Bgy
Barangay 848	Bgy
Barangay 849	Bgy
Barangay 850	Bgy
Barangay 851	Bgy
Barangay 852	Bgy
Barangay 853	Bgy
Barangay 855	Bgy
Barangay 856	Bgy
Barangay 857	Bgy
Barangay 858	Bgy
Barangay 859	Bgy
Barangay 860	Bgy
Barangay 861	Bgy
Barangay 862	Bgy
Barangay 863	Bgy
Barangay 864	Bgy
Barangay 865	Bgy
Barangay 867	Bgy
Barangay 868	Bgy
Barangay 870	Bgy
Barangay 871	Bgy
Barangay 872	Bgy
Barangay 869	Bgy
Port Area	SubMun
Barangay 649	Bgy
Barangay 650	Bgy
Barangay 651	Bgy
Barangay 652	Bgy
Barangay 653	Bgy
Santa Ana	SubMun
Barangay 745	Bgy
Barangay 746	Bgy
Barangay 747	Bgy
Barangay 748	Bgy
Barangay 749	Bgy
Barangay 750	Bgy
Barangay 751	Bgy
Barangay 752	Bgy
Barangay 753	Bgy
Barangay 755	Bgy
Barangay 756	Bgy
Barangay 757	Bgy
Barangay 758	Bgy
Barangay 759	Bgy
Barangay 760	Bgy
Barangay 761	Bgy
Barangay 762	Bgy
Barangay 763	Bgy
Barangay 764	Bgy
Barangay 765	Bgy
Barangay 766	Bgy
Barangay 767	Bgy
Barangay 768	Bgy
Barangay 769	Bgy
Barangay 770	Bgy
Barangay 771	Bgy
Barangay 772	Bgy
Barangay 773	Bgy
Barangay 774	Bgy
Barangay 775	Bgy
Barangay 776	Bgy
Barangay 777	Bgy
Barangay 778	Bgy
Barangay 779	Bgy
Barangay 780	Bgy
Barangay 781	Bgy
Barangay 782	Bgy
Barangay 783	Bgy
Barangay 784	Bgy
Barangay 785	Bgy
Barangay 786	Bgy
Barangay 787	Bgy
Barangay 788	Bgy
Barangay 789	Bgy
Barangay 790	Bgy
Barangay 791	Bgy
Barangay 792	Bgy
Barangay 793	Bgy
Barangay 794	Bgy
Barangay 795	Bgy
Barangay 796	Bgy
Barangay 797	Bgy
Barangay 798	Bgy
Barangay 799	Bgy
Barangay 800	Bgy
Barangay 801	Bgy
Barangay 802	Bgy
Barangay 803	Bgy
Barangay 804	Bgy
Barangay 805	Bgy
Barangay 806	Bgy
Barangay 807	Bgy
Barangay 866	Bgy
Barangay 873	Bgy
Barangay 874	Bgy
Barangay 875	Bgy
Barangay 876	Bgy
Barangay 877	Bgy
Barangay 878	Bgy
Barangay 879	Bgy
Barangay 880	Bgy
Barangay 881	Bgy
Barangay 882	Bgy
Barangay 883	Bgy
Barangay 884	Bgy
Barangay 885	Bgy
Barangay 886	Bgy
Barangay 887	Bgy
Barangay 888	Bgy
Barangay 889	Bgy
Barangay 890	Bgy
Barangay 891	Bgy
Barangay 892	Bgy
Barangay 893	Bgy
Barangay 894	Bgy
Barangay 895	Bgy
Barangay 896	Bgy
Barangay 897	Bgy
Barangay 898	Bgy
Barangay 899	Bgy
Barangay 900	Bgy
Barangay 901	Bgy
Barangay 902	Bgy
Barangay 903	Bgy
Barangay 904	Bgy
Barangay 905	Bgy
Barangay 754	Bgy
Barangay 808	Bgy
Barangay 818-A	Bgy
NCR, Second District (Not a Province)	Dist
City of Mandaluyong	City
Addition Hills	Bgy
Bagong Silang	Bgy
Barangka Drive	Bgy
Barangka Ibaba	Bgy
Barangka Ilaya	Bgy
Barangka Itaas	Bgy
Burol	Bgy
Buayang Bato	Bgy
Daang Bakal	Bgy
Hagdang Bato Itaas	Bgy
Hagdang Bato Libis	Bgy
Harapin Ang Bukas	Bgy
Highway Hills	Bgy
Hulo	Bgy
Mabini-J. Rizal	Bgy
Malamig	Bgy
Mauway	Bgy
Namayan	Bgy
New Zañiga	Bgy
Old Zañiga	Bgy
Pag-asa	Bgy
Plainview	Bgy
Pleasant Hills	Bgy
Poblacion	Bgy
San Jose	Bgy
Vergara	Bgy
Wack-wack Greenhills	Bgy
City of Marikina	City
Barangka	Bgy
Calumpang	Bgy
Concepcion Uno	Bgy
Jesus De La Peña	Bgy
Malanday	Bgy
Nangka	Bgy
Parang	Bgy
San Roque	Bgy
Santa Elena (Pob.)	Bgy
Santo Niño	Bgy
Tañong	Bgy
Concepcion Dos	Bgy
Marikina Heights	Bgy
Industrial Valley	Bgy
Fortune	Bgy
Tumana	Bgy
City of Pasig	City
Bagong Ilog	Bgy
Bagong Katipunan	Bgy
Bambang	Bgy
Buting	Bgy
Caniogan	Bgy
Dela Paz	Bgy
Kalawaan	Bgy
Kapasigan	Bgy
Kapitolyo	Bgy
Malinao	Bgy
Manggahan	Bgy
Maybunga	Bgy
Oranbo	Bgy
Palatiw	Bgy
Pinagbuhatan	Bgy
Pineda	Bgy
Rosario	Bgy
Sagad	Bgy
San Antonio	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Miguel	Bgy
San Nicolas (Pob.)	Bgy
Santa Cruz	Bgy
Santa Rosa	Bgy
Santo Tomas	Bgy
Santolan	Bgy
Sumilang	Bgy
Ugong	Bgy
Santa Lucia	Bgy
Quezon City	City
Alicia	Bgy
Amihan	Bgy
Apolonio Samson	Bgy
Aurora	Bgy
Baesa	Bgy
Bagbag	Bgy
Bagumbuhay	Bgy
Bagong Lipunan Ng Crame	Bgy
Bagong Pag-asa	Bgy
Bagong Silangan	Bgy
Bagumbayan	Bgy
Bahay Toro	Bgy
Balingasa	Bgy
Bayanihan	Bgy
Blue Ridge A	Bgy
Blue Ridge B	Bgy
Botocan	Bgy
Bungad	Bgy
Camp Aguinaldo	Bgy
Central	Bgy
Claro	Bgy
Commonwealth	Bgy
New Era	Bgy
Kristong Hari	Bgy
Culiat	Bgy
Damar	Bgy
Damayan	Bgy
Damayang Lagi	Bgy
Del Monte	Bgy
Dioquino Zobel	Bgy
Doña Imelda	Bgy
Doña Josefa	Bgy
Don Manuel	Bgy
Duyan-duyan	Bgy
E. Rodriguez	Bgy
East Kamias	Bgy
Escopa I	Bgy
Escopa II	Bgy
Escopa III	Bgy
Escopa IV	Bgy
Fairview	Bgy
N.S. Amoranto	Bgy
Gulod	Bgy
Horseshoe	Bgy
Immaculate Concepcion	Bgy
Kaligayahan	Bgy
Kalusugan	Bgy
Kamuning	Bgy
Katipunan	Bgy
Kaunlaran	Bgy
Krus Na Ligas	Bgy
Laging Handa	Bgy
Libis	Bgy
Lourdes	Bgy
Loyola Heights	Bgy
Maharlika	Bgy
Malaya	Bgy
Manresa	Bgy
Mangga	Bgy
Mariana	Bgy
Mariblo	Bgy
Marilag	Bgy
Masagana	Bgy
Masambong	Bgy
Santo Domingo	Bgy
Matandang Balara	Bgy
Milagrosa	Bgy
Nagkaisang Nayon	Bgy
Nayong Kanluran	Bgy
Novaliches Proper	Bgy
Obrero	Bgy
Old Capitol Site	Bgy
Paang Bundok	Bgy
Pag-ibig Sa Nayon	Bgy
Paligsahan	Bgy
Paltok	Bgy
Pansol	Bgy
Paraiso	Bgy
Pasong Putik Proper	Bgy
Pasong Tamo	Bgy
Phil-Am	Bgy
Pinyahan	Bgy
Pinagkaisahan	Bgy
Project 6	Bgy
Quirino 2-A	Bgy
Quirino 2-B	Bgy
Quirino 2-C	Bgy
Quirino 3-A	Bgy
Ramon Magsaysay	Bgy
Roxas	Bgy
Sacred Heart	Bgy
Saint Ignatius	Bgy
Saint Peter	Bgy
Salvacion	Bgy
San Agustin	Bgy
San Antonio	Bgy
San Bartolome	Bgy
San Isidro	Bgy
San Isidro Labrador	Bgy
San Jose	Bgy
San Martin De Porres	Bgy
San Roque	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Lucia	Bgy
Santa Monica	Bgy
Santa Teresita	Bgy
Santo Cristo	Bgy
Santo Niño	Bgy
Santol	Bgy
Sauyo	Bgy
Sienna	Bgy
Sikatuna Village	Bgy
Silangan	Bgy
Socorro	Bgy
South Triangle	Bgy
Tagumpay	Bgy
Talayan	Bgy
Talipapa	Bgy
Tandang Sora	Bgy
Tatalon	Bgy
Teachers Village East	Bgy
Teachers Village West	Bgy
U.P. Campus	Bgy
U.P. Village	Bgy
Ugong Norte	Bgy
Unang Sigaw	Bgy
Valencia	Bgy
Vasra	Bgy
Veterans Village	Bgy
Villa Maria Clara	Bgy
West Kamias	Bgy
West Triangle	Bgy
White Plains	Bgy
Balong Bato	Bgy
Capri	Bgy
Sangandaan	Bgy
Payatas	Bgy
Batasan Hills	Bgy
Holy Spirit	Bgy
Greater Lagro	Bgy
North Fairview	Bgy
City of San Juan	City
Addition Hills	Bgy
Balong-Bato	Bgy
Batis	Bgy
Corazon De Jesus	Bgy
Ermitaño	Bgy
Halo-halo	Bgy
Isabelita	Bgy
Kabayanan	Bgy
Little Baguio	Bgy
Maytunas	Bgy
Onse	Bgy
Pasadeña	Bgy
Pedro Cruz	Bgy
Progreso	Bgy
Rivera	Bgy
Salapan	Bgy
San Perfecto	Bgy
Santa Lucia	Bgy
Tibagan	Bgy
West Crame	Bgy
Greenhills	Bgy
NCR, Third District (Not a Province)	Dist
City of Caloocan	City
Barangay 1	Bgy
Barangay 2	Bgy
Barangay 3	Bgy
Barangay 4	Bgy
Barangay 5	Bgy
Barangay 6	Bgy
Barangay 7	Bgy
Barangay 8	Bgy
Barangay 9	Bgy
Barangay 10	Bgy
Barangay 11	Bgy
Barangay 12	Bgy
Barangay 13	Bgy
Barangay 14	Bgy
Barangay 15	Bgy
Barangay 16	Bgy
Barangay 17	Bgy
Barangay 18	Bgy
Barangay 19	Bgy
Barangay 20	Bgy
Barangay 21	Bgy
Barangay 22	Bgy
Barangay 23	Bgy
Barangay 24	Bgy
Barangay 25	Bgy
Barangay 26	Bgy
Barangay 27	Bgy
Barangay 28	Bgy
Barangay 29	Bgy
Barangay 30	Bgy
Barangay 31	Bgy
Barangay 32	Bgy
Barangay 33	Bgy
Barangay 34	Bgy
Barangay 35	Bgy
Barangay 36	Bgy
Barangay 37	Bgy
Barangay 38	Bgy
Barangay 39	Bgy
Barangay 40	Bgy
Barangay 41	Bgy
Barangay 42	Bgy
Barangay 43	Bgy
Barangay 44	Bgy
Barangay 45	Bgy
Barangay 46	Bgy
Barangay 47	Bgy
Barangay 48	Bgy
Barangay 49	Bgy
Barangay 50	Bgy
Barangay 51	Bgy
Barangay 52	Bgy
Barangay 53	Bgy
Barangay 54	Bgy
Barangay 55	Bgy
Barangay 56	Bgy
Barangay 57	Bgy
Barangay 58	Bgy
Barangay 59	Bgy
Barangay 60	Bgy
Barangay 61	Bgy
Barangay 62	Bgy
Barangay 63	Bgy
Barangay 64	Bgy
Barangay 65	Bgy
Barangay 66	Bgy
Barangay 67	Bgy
Barangay 68	Bgy
Barangay 69	Bgy
Barangay 70	Bgy
Barangay 71	Bgy
Barangay 72	Bgy
Barangay 73	Bgy
Barangay 74	Bgy
Barangay 75	Bgy
Barangay 76	Bgy
Barangay 77	Bgy
Barangay 78	Bgy
Barangay 79	Bgy
Barangay 80	Bgy
Barangay 81	Bgy
Barangay 82	Bgy
Barangay 83	Bgy
Barangay 84	Bgy
Barangay 85	Bgy
Barangay 86	Bgy
Barangay 87	Bgy
Barangay 88	Bgy
Barangay 89	Bgy
Barangay 90	Bgy
Barangay 91	Bgy
Barangay 92	Bgy
Barangay 93	Bgy
Barangay 94	Bgy
Barangay 95	Bgy
Barangay 96	Bgy
Barangay 97	Bgy
Barangay 98	Bgy
Barangay 99	Bgy
Barangay 100	Bgy
Barangay 101	Bgy
Barangay 102	Bgy
Barangay 103	Bgy
Barangay 104	Bgy
Barangay 105	Bgy
Barangay 106	Bgy
Barangay 107	Bgy
Barangay 108	Bgy
Barangay 109	Bgy
Barangay 110	Bgy
Barangay 111	Bgy
Barangay 112	Bgy
Barangay 113	Bgy
Barangay 114	Bgy
Barangay 115	Bgy
Barangay 116	Bgy
Barangay 117	Bgy
Barangay 118	Bgy
Barangay 119	Bgy
Barangay 120	Bgy
Barangay 121	Bgy
Barangay 122	Bgy
Barangay 123	Bgy
Barangay 124	Bgy
Barangay 125	Bgy
Barangay 126	Bgy
Barangay 127	Bgy
Barangay 128	Bgy
Barangay 129	Bgy
Barangay 130	Bgy
Barangay 131	Bgy
Barangay 132	Bgy
Barangay 133	Bgy
Barangay 134	Bgy
Barangay 135	Bgy
Barangay 136	Bgy
Barangay 137	Bgy
Barangay 138	Bgy
Barangay 139	Bgy
Barangay 140	Bgy
Barangay 141	Bgy
Barangay 142	Bgy
Barangay 143	Bgy
Barangay 144	Bgy
Barangay 145	Bgy
Barangay 146	Bgy
Barangay 147	Bgy
Barangay 148	Bgy
Barangay 149	Bgy
Barangay 150	Bgy
Barangay 151	Bgy
Barangay 152	Bgy
Barangay 153	Bgy
Barangay 154	Bgy
Barangay 155	Bgy
Barangay 156	Bgy
Barangay 157	Bgy
Barangay 158	Bgy
Barangay 159	Bgy
Barangay 160	Bgy
Barangay 161	Bgy
Barangay 162	Bgy
Barangay 163	Bgy
Barangay 164	Bgy
Barangay 165	Bgy
Barangay 166	Bgy
Barangay 167	Bgy
Barangay 168	Bgy
Barangay 169	Bgy
Barangay 170	Bgy
Barangay 171	Bgy
Barangay 172	Bgy
Barangay 173	Bgy
Barangay 174	Bgy
Barangay 175	Bgy
Barangay 176	Bgy
Barangay 177	Bgy
Barangay 178	Bgy
Barangay 179	Bgy
Barangay 180	Bgy
Barangay 181	Bgy
Barangay 182	Bgy
Barangay 183	Bgy
Barangay 184	Bgy
Barangay 185	Bgy
Barangay 186	Bgy
Barangay 187	Bgy
Barangay 188	Bgy
City of Malabon	City
Acacia	Bgy
Baritan	Bgy
Bayan-bayanan	Bgy
Catmon	Bgy
Concepcion	Bgy
Dampalit	Bgy
Flores	Bgy
Hulong Duhat	Bgy
Ibaba	Bgy
Longos	Bgy
Maysilo	Bgy
Muzon	Bgy
Niugan	Bgy
Panghulo	Bgy
Potrero	Bgy
San Agustin	Bgy
Santolan	Bgy
Tañong (Pob.)	Bgy
Tinajeros	Bgy
Tonsuya	Bgy
Tugatog	Bgy
City of Navotas	City
Sipac-Almacen	Bgy
Bagumbayan North	Bgy
Bagumbayan South	Bgy
Bangculasi	Bgy
Daanghari	Bgy
Navotas East	Bgy
Navotas West	Bgy
North Bay Boulevard North	Bgy
NBBS Kaunlaran	Bgy
San Jose (Pob.)	Bgy
San Rafael Village	Bgy
San Roque	Bgy
Tangos South	Bgy
Tanza 1	Bgy
NBBS Dagat-dagatan	Bgy
NBBS Proper	Bgy
Tangos North	Bgy
Tanza 2	Bgy
City of Valenzuela	City
Arkong Bato	Bgy
Bagbaguin	Bgy
Balangkas	Bgy
Parada	Bgy
Bignay	Bgy
Bisig	Bgy
Canumay West	Bgy
Karuhatan	Bgy
Coloong	Bgy
Dalandanan	Bgy
Gen. T. De Leon	Bgy
Isla	Bgy
Lawang Bato	Bgy
Lingunan	Bgy
Mabolo	Bgy
Malanday	Bgy
Malinta	Bgy
Mapulang Lupa	Bgy
Marulas	Bgy
Maysan	Bgy
Palasan	Bgy
Pariancillo Villa	Bgy
Paso De Blas	Bgy
Pasolo	Bgy
Poblacion	Bgy
Pulo	Bgy
Punturin	Bgy
Rincon	Bgy
Tagalag	Bgy
Ugong	Bgy
Viente Reales	Bgy
Wawang Pulo	Bgy
Canumay East	Bgy
NCR, Fourth District (Not a Province)	Dist
City of Las Piñas	City
Almanza Uno	Bgy
Daniel Fajardo	Bgy
Elias Aldana	Bgy
Ilaya	Bgy
Manuyo Uno	Bgy
Pamplona Uno	Bgy
Pulang Lupa Uno	Bgy
Talon Uno	Bgy
Zapote	Bgy
Almanza Dos	Bgy
B. F. International Village	Bgy
Manuyo Dos	Bgy
Pamplona Dos	Bgy
Pamplona Tres	Bgy
Pilar	Bgy
Pulang Lupa Dos	Bgy
Talon Dos	Bgy
Talon Tres	Bgy
Talon Kuatro	Bgy
Talon Singko	Bgy
City of Makati	City
Bangkal	Bgy
Bel-Air	Bgy
Cembo	Bgy
Comembo	Bgy
Carmona	Bgy
Dasmariñas	Bgy
East Rembo	Bgy
Forbes Park	Bgy
Guadalupe Nuevo	Bgy
Guadalupe Viejo	Bgy
Kasilawan	Bgy
La Paz	Bgy
Magallanes	Bgy
Olympia	Bgy
Palanan	Bgy
Pembo	Bgy
Pinagkaisahan	Bgy
Pio Del Pilar	Bgy
Pitogo	Bgy
Poblacion	Bgy
Post Proper Northside	Bgy
Post Proper Southside	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Lorenzo	Bgy
Santa Cruz	Bgy
Singkamas	Bgy
South Cembo	Bgy
Tejeros	Bgy
Urdaneta	Bgy
Valenzuela	Bgy
West Rembo	Bgy
Rizal	Bgy
City of Muntinlupa	City
Alabang	Bgy
Bayanan	Bgy
Buli	Bgy
Cupang	Bgy
Poblacion	Bgy
Putatan	Bgy
Sucat	Bgy
Tunasan	Bgy
New Alabang Village	Bgy
City of Parañaque	City
Baclaran	Bgy
Don Galo	Bgy
La Huerta	Bgy
San Dionisio	Bgy
Santo Niño	Bgy
Tambo	Bgy
B. F. Homes	Bgy
Don Bosco	Bgy
Marcelo Green Village	Bgy
Merville	Bgy
Moonwalk	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Martin De Porres	Bgy
Sun Valley	Bgy
Vitalez	Bgy
Pasay City	City
Barangay 1	Bgy
Barangay 2	Bgy
Barangay 3	Bgy
Barangay 4	Bgy
Barangay 5	Bgy
Barangay 6	Bgy
Barangay 7	Bgy
Barangay 8	Bgy
Barangay 9	Bgy
Barangay 10	Bgy
Barangay 11	Bgy
Barangay 12	Bgy
Barangay 13	Bgy
Barangay 14	Bgy
Barangay 15	Bgy
Barangay 16	Bgy
Barangay 17	Bgy
Barangay 18	Bgy
Barangay 19	Bgy
Barangay 20	Bgy
Barangay 21	Bgy
Barangay 22	Bgy
Barangay 23	Bgy
Barangay 24	Bgy
Barangay 25	Bgy
Barangay 26	Bgy
Barangay 27	Bgy
Barangay 28	Bgy
Barangay 29	Bgy
Barangay 30	Bgy
Barangay 31	Bgy
Barangay 32	Bgy
Barangay 33	Bgy
Barangay 34	Bgy
Barangay 35	Bgy
Barangay 36	Bgy
Barangay 37	Bgy
Barangay 38	Bgy
Barangay 39	Bgy
Barangay 40	Bgy
Barangay 41	Bgy
Barangay 42	Bgy
Barangay 43	Bgy
Barangay 44	Bgy
Barangay 45	Bgy
Barangay 46	Bgy
Barangay 47	Bgy
Barangay 48	Bgy
Barangay 49	Bgy
Barangay 50	Bgy
Barangay 51	Bgy
Barangay 52	Bgy
Barangay 53	Bgy
Barangay 54	Bgy
Barangay 55	Bgy
Barangay 56	Bgy
Barangay 57	Bgy
Barangay 58	Bgy
Barangay 59	Bgy
Barangay 60	Bgy
Barangay 61	Bgy
Barangay 62	Bgy
Barangay 63	Bgy
Barangay 64	Bgy
Barangay 65	Bgy
Barangay 66	Bgy
Barangay 67	Bgy
Barangay 68	Bgy
Barangay 69	Bgy
Barangay 70	Bgy
Barangay 71	Bgy
Barangay 72	Bgy
Barangay 73	Bgy
Barangay 74	Bgy
Barangay 75	Bgy
Barangay 76	Bgy
Barangay 77	Bgy
Barangay 78	Bgy
Barangay 79	Bgy
Barangay 80	Bgy
Barangay 81	Bgy
Barangay 82	Bgy
Barangay 83	Bgy
Barangay 84	Bgy
Barangay 85	Bgy
Barangay 86	Bgy
Barangay 87	Bgy
Barangay 88	Bgy
Barangay 89	Bgy
Barangay 90	Bgy
Barangay 91	Bgy
Barangay 92	Bgy
Barangay 93	Bgy
Barangay 94	Bgy
Barangay 95	Bgy
Barangay 96	Bgy
Barangay 97	Bgy
Barangay 98	Bgy
Barangay 99	Bgy
Barangay 100	Bgy
Barangay 101	Bgy
Barangay 102	Bgy
Barangay 103	Bgy
Barangay 104	Bgy
Barangay 105	Bgy
Barangay 106	Bgy
Barangay 107	Bgy
Barangay 108	Bgy
Barangay 109	Bgy
Barangay 110	Bgy
Barangay 111	Bgy
Barangay 112	Bgy
Barangay 113	Bgy
Barangay 114	Bgy
Barangay 115	Bgy
Barangay 116	Bgy
Barangay 117	Bgy
Barangay 118	Bgy
Barangay 119	Bgy
Barangay 120	Bgy
Barangay 121	Bgy
Barangay 122	Bgy
Barangay 123	Bgy
Barangay 124	Bgy
Barangay 125	Bgy
Barangay 126	Bgy
Barangay 127	Bgy
Barangay 128	Bgy
Barangay 129	Bgy
Barangay 130	Bgy
Barangay 131	Bgy
Barangay 132	Bgy
Barangay 133	Bgy
Barangay 134	Bgy
Barangay 135	Bgy
Barangay 136	Bgy
Barangay 137	Bgy
Barangay 138	Bgy
Barangay 139	Bgy
Barangay 140	Bgy
Barangay 141	Bgy
Barangay 142	Bgy
Barangay 143	Bgy
Barangay 144	Bgy
Barangay 145	Bgy
Barangay 146	Bgy
Barangay 147	Bgy
Barangay 148	Bgy
Barangay 149	Bgy
Barangay 150	Bgy
Barangay 151	Bgy
Barangay 152	Bgy
Barangay 153	Bgy
Barangay 154	Bgy
Barangay 155	Bgy
Barangay 156	Bgy
Barangay 157	Bgy
Barangay 158	Bgy
Barangay 159	Bgy
Barangay 160	Bgy
Barangay 161	Bgy
Barangay 162	Bgy
Barangay 163	Bgy
Barangay 164	Bgy
Barangay 165	Bgy
Barangay 166	Bgy
Barangay 167	Bgy
Barangay 168	Bgy
Barangay 169	Bgy
Barangay 170	Bgy
Barangay 171	Bgy
Barangay 172	Bgy
Barangay 173	Bgy
Barangay 174	Bgy
Barangay 175	Bgy
Barangay 176	Bgy
Barangay 177	Bgy
Barangay 178	Bgy
Barangay 179	Bgy
Barangay 180	Bgy
Barangay 181	Bgy
Barangay 182	Bgy
Barangay 183	Bgy
Barangay 184	Bgy
Barangay 185	Bgy
Barangay 186	Bgy
Barangay 187	Bgy
Barangay 188	Bgy
Barangay 189	Bgy
Barangay 190	Bgy
Barangay 191	Bgy
Barangay 192	Bgy
Barangay 193	Bgy
Barangay 194	Bgy
Barangay 195	Bgy
Barangay 196	Bgy
Barangay 197	Bgy
Barangay 198	Bgy
Barangay 199	Bgy
Barangay 200	Bgy
Barangay 201	Bgy
Pateros	Mun
Aguho	Bgy
Magtanggol	Bgy
Martires Del 96	Bgy
Poblacion	Bgy
San Pedro	Bgy
San Roque	Bgy
Santa Ana	Bgy
Santo Rosario-Kanluran	Bgy
Santo Rosario-Silangan	Bgy
Tabacalera	Bgy
City of Taguig	City
Tanyag	Bgy
Bagumbayan	Bgy
Bambang	Bgy
Calzada	Bgy
Hagonoy	Bgy
Ibayo-Tipas	Bgy
Ligid-Tipas	Bgy
Lower Bicutan	Bgy
Maharlika Village	Bgy
Napindan	Bgy
Palingon	Bgy
Santa Ana	Bgy
Central Signal Village	Bgy
Tuktukan	Bgy
Upper Bicutan	Bgy
Ususan	Bgy
Wawa	Bgy
Western Bicutan	Bgy
Central Bicutan	Bgy
Fort Bonifacio	Bgy
Katuparan	Bgy
New Lower Bicutan	Bgy
North Daang Hari	Bgy
North Signal Village	Bgy
Pinagsama	Bgy
San Miguel	Bgy
South Daang Hari	Bgy
South Signal Village	Bgy
Cordillera Administrative Region (CAR)	Reg
Abra	Prov
Bangued (Capital)	Mun
Agtangao	Bgy
Angad	Bgy
Bañacao	Bgy
Bangbangar	Bgy
Cabuloan	Bgy
Calaba	Bgy
Tablac	Bgy
Cosili West	Bgy
Cosili East	Bgy
Dangdangla	Bgy
Lingtan	Bgy
Lipcan	Bgy
Lubong	Bgy
Macarcarmay	Bgy
Maoay	Bgy
Macray	Bgy
Malita	Bgy
Palao	Bgy
Patucannay	Bgy
Sagap	Bgy
San Antonio	Bgy
Santa Rosa	Bgy
Sao-atan	Bgy
Sappaac	Bgy
Zone 2 Pob.	Bgy
Zone 3 Pob.	Bgy
Zone 4 Pob.	Bgy
Zone 5 Pob.	Bgy
Zone 6 Pob. 	Bgy
Zone 7 Pob.	Bgy
Zone 1 Pob.	Bgy
Boliney	Mun
Amti	Bgy
Bao-yan	Bgy
Danac East	Bgy
Dao-angan	Bgy
Dumagas	Bgy
Kilong-Olao	Bgy
Poblacion	Bgy
Danac West	Bgy
Bucay	Mun
Abang	Bgy
Bangbangcag	Bgy
Bangcagan	Bgy
Banglolao	Bgy
Bugbog	Bgy
Calao	Bgy
Dugong	Bgy
Labon	Bgy
Layugan	Bgy
Madalipay	Bgy
Pagala	Bgy
Palaquio	Bgy
Pakiling	Bgy
Patoc	Bgy
North Poblacion	Bgy
South Poblacion	Bgy
Quimloong	Bgy
Salnec	Bgy
San Miguel	Bgy
Siblong	Bgy
Tabiog	Bgy
Bucloc	Mun
Ducligan	Bgy
Labaan	Bgy
Lingay	Bgy
Lamao (Pob.)	Bgy
Daguioman	Mun
Ableg	Bgy
Cabaruyan	Bgy
Pikek	Bgy
Tui (Pob.)	Bgy
Danglas	Mun
Abaquid	Bgy
Cabaruan	Bgy
Caupasan (Pob.)	Bgy
Danglas	Bgy
Nagaparan	Bgy
Padangitan	Bgy
Pangal	Bgy
Dolores	Mun
Bayaan	Bgy
Cabaroan	Bgy
Calumbaya	Bgy
Cardona	Bgy
Isit	Bgy
Kimmalaba	Bgy
Libtec	Bgy
Lub-lubba	Bgy
Mudiit	Bgy
Namit-ingan	Bgy
Pacac	Bgy
Poblacion	Bgy
Salucag	Bgy
Talogtog	Bgy
Taping	Bgy
La Paz	Mun
Benben	Bgy
Bulbulala	Bgy
Buli	Bgy
Canan	Bgy
Liguis	Bgy
Malabbaga	Bgy
Mudeng	Bgy
Pidipid	Bgy
Poblacion	Bgy
San Gregorio	Bgy
Toon	Bgy
Udangan	Bgy
Lacub	Mun
Bacag	Bgy
Buneg	Bgy
Guinguinabang	Bgy
Lan-ag	Bgy
Pacoc	Bgy
Poblacion	Bgy
Lagangilang	Mun
Aguet	Bgy
Bacooc	Bgy
Balais	Bgy
Cayapa	Bgy
Dalaguisen	Bgy
Laang	Bgy
Lagben	Bgy
Laguiben	Bgy
Nagtipulan	Bgy
Nagtupacan	Bgy
Paganao	Bgy
Pawa	Bgy
Poblacion	Bgy
Presentar	Bgy
San Isidro	Bgy
Tagodtod	Bgy
Taping	Bgy
Lagayan	Mun
Ba-i	Bgy
Collago	Bgy
Pang-ot	Bgy
Poblacion	Bgy
Pulot	Bgy
Langiden	Mun
Baac	Bgy
Dalayap	Bgy
Mabungtot	Bgy
Malapaao	Bgy
Poblacion	Bgy
Quillat	Bgy
Licuan-Baay	Mun
Bonglo	Bgy
Bulbulala	Bgy
Cawayan	Bgy
Domenglay	Bgy
Lenneng	Bgy
Mapisla	Bgy
Mogao	Bgy
Nalbuan	Bgy
Poblacion	Bgy
Subagan	Bgy
Tumalip	Bgy
Luba	Mun
Ampalioc	Bgy
Barit	Bgy
Gayaman	Bgy
Lul-luno	Bgy
Luzong	Bgy
Nagbukel-Tuquipa	Bgy
Poblacion	Bgy
Sabnangan	Bgy
Malibcong	Mun
Bayabas	Bgy
Binasaran	Bgy
Buanao	Bgy
Dulao	Bgy
Duldulao	Bgy
Gacab	Bgy
Lat-ey	Bgy
Malibcong (Pob.)	Bgy
Mataragan	Bgy
Pacgued	Bgy
Taripan	Bgy
Umnap	Bgy
Manabo	Mun
Catacdegan Viejo	Bgy
Luzong	Bgy
Ayyeng (Pob.)	Bgy
San Jose Norte	Bgy
San Jose Sur	Bgy
San Juan Norte	Bgy
San Juan Sur	Bgy
San Ramon East	Bgy
San Ramon West	Bgy
Santo Tomas	Bgy
Catacdegan Nuevo	Bgy
Peñarrubia	Mun
Dumayco	Bgy
Lusuac	Bgy
Namarabar	Bgy
Patiao	Bgy
Malamsit	Bgy
Poblacion	Bgy
Riang	Bgy
Santa Rosa	Bgy
Tattawa	Bgy
Pidigan	Mun
Alinaya	Bgy
Arab	Bgy
Garreta	Bgy
Immuli	Bgy
Laskig	Bgy
Naguirayan	Bgy
Monggoc	Bgy
Pamutic	Bgy
Pangtud	Bgy
Poblacion East	Bgy
Poblacion West	Bgy
San Diego	Bgy
Sulbec	Bgy
Suyo	Bgy
Yuyeng	Bgy
Pilar	Mun
Bolbolo	Bgy
Brookside	Bgy
Ocup	Bgy
Dalit	Bgy
Dintan	Bgy
Gapang	Bgy
Kinabiti	Bgy
Maliplipit	Bgy
Nagcanasan	Bgy
Nanangduan	Bgy
Narnara	Bgy
Pang-ot	Bgy
Patad	Bgy
Poblacion	Bgy
San Juan East	Bgy
San Juan West	Bgy
South Balioag	Bgy
Tikitik	Bgy
Villavieja	Bgy
Sallapadan	Mun
Bazar	Bgy
Bilabila	Bgy
Gangal (Pob.)	Bgy
Maguyepyep	Bgy
Naguilian	Bgy
Saccaang	Bgy
Sallapadan	Bgy
Subusob	Bgy
Ud-udiao	Bgy
San Isidro	Mun
Cabayogan	Bgy
Dalimag	Bgy
Langbaban	Bgy
Manayday	Bgy
Pantoc	Bgy
Poblacion	Bgy
Sabtan-olo	Bgy
San Marcial	Bgy
Tangbao	Bgy
San Juan	Mun
Abualan	Bgy
Ba-ug	Bgy
Badas	Bgy
Cabcaborao	Bgy
Colabaoan	Bgy
Culiong	Bgy
Daoidao	Bgy
Guimba	Bgy
Lam-ag	Bgy
Lumobang	Bgy
Nangobongan	Bgy
Pattaoig	Bgy
Poblacion North	Bgy
Poblacion South	Bgy
Quidaoen	Bgy
Sabangan	Bgy
Silet	Bgy
Supi-il	Bgy
Tagaytay	Bgy
San Quintin	Mun
Labaan	Bgy
Palang	Bgy
Pantoc	Bgy
Poblacion	Bgy
Tangadan	Bgy
Villa Mercedes	Bgy
Tayum	Mun
Bagalay	Bgy
Basbasa	Bgy
Budac	Bgy
Bumagcat	Bgy
Cabaroan	Bgy
Deet	Bgy
Gaddani	Bgy
Patucannay	Bgy
Pias	Bgy
Poblacion	Bgy
Velasco	Bgy
Tineg	Mun
Poblacion	Bgy
Alaoa	Bgy
Anayan	Bgy
Apao	Bgy
Belaat	Bgy
Caganayan	Bgy
Cogon	Bgy
Lanec	Bgy
Lapat-Balantay	Bgy
Naglibacan	Bgy
Tubo	Mun
Alangtin	Bgy
Amtuagan	Bgy
Dilong	Bgy
Kili	Bgy
Poblacion	Bgy
Supo	Bgy
Tiempo	Bgy
Tubtuba	Bgy
Wayangan	Bgy
Tabacda	Bgy
Villaviciosa	Mun
Ap-apaya	Bgy
Bol-lilising	Bgy
Cal-lao	Bgy
Lap-lapog	Bgy
Lumaba	Bgy
Poblacion	Bgy
Tamac	Bgy
Tuquib	Bgy
Benguet	Prov
Atok	Mun
Abiang	Bgy
Caliking	Bgy
Cattubo	Bgy
Naguey	Bgy
Paoay	Bgy
Pasdong	Bgy
Poblacion	Bgy
Topdac	Bgy
City of Baguio	City
Apugan-Loakan	Bgy
Asin Road	Bgy
Atok Trail	Bgy
Bakakeng Central	Bgy
Bakakeng North	Bgy
Happy Hollow	Bgy
Balsigan	Bgy
Bayan Park West	Bgy
Bayan Park East	Bgy
Brookspoint	Bgy
Brookside	Bgy
Cabinet Hill-Teacher's Camp	Bgy
Camp Allen	Bgy
Camp 7	Bgy
Camp 8	Bgy
Campo Filipino	Bgy
City Camp Central	Bgy
City Camp Proper	Bgy
Country Club Village	Bgy
Cresencia Village	Bgy
Dagsian, Upper	Bgy
DPS Area	Bgy
Dizon Subdivision	Bgy
Quirino Hill, East	Bgy
Engineers' Hill	Bgy
Fairview Village	Bgy
Fort del Pilar	Bgy
General Luna, Upper	Bgy
General Luna, Lower	Bgy
Gibraltar	Bgy
Greenwater Village	Bgy
Guisad Central	Bgy
Guisad Sorong	Bgy
Hillside	Bgy
Holy Ghost Extension	Bgy
Holy Ghost Proper	Bgy
Imelda Village	Bgy
Irisan	Bgy
Kayang Extension	Bgy
Kias	Bgy
Kagitingan	Bgy
Loakan Proper	Bgy
Lopez Jaena	Bgy
Lourdes Subdivision Extension	Bgy
Dagsian, Lower	Bgy
Lourdes Subdivision, Lower	Bgy
Quirino Hill, Lower	Bgy
General Emilio F. Aguinaldo	Bgy
Lualhati	Bgy
Lucnab	Bgy
Magsaysay, Lower	Bgy
Magsaysay Private Road	Bgy
Aurora Hill Proper	Bgy
Bal-Marcoville	Bgy
Quirino Hill, Middle	Bgy
Military Cut-off	Bgy
Mines View Park	Bgy
Modern Site, East	Bgy
Modern Site, West	Bgy
New Lucban	Bgy
Aurora Hill, North Central	Bgy
Sanitary Camp, North	Bgy
Outlook Drive	Bgy
Pacdal	Bgy
Pinget	Bgy
Pinsao Pilot Project	Bgy
Pinsao Proper	Bgy
Poliwes	Bgy
Pucsusan	Bgy
MRR-Queen Of Peace	Bgy
Rock Quarry, Lower	Bgy
Salud Mitra	Bgy
San Antonio Village	Bgy
San Luis Village	Bgy
San Roque Village	Bgy
San Vicente	Bgy
Santa Escolastica	Bgy
Santo Rosario	Bgy
Santo Tomas School Area	Bgy
Santo Tomas Proper	Bgy
Scout Barrio	Bgy
Session Road Area	Bgy
Slaughter House Area	Bgy
Sanitary Camp, South	Bgy
Saint Joseph Village	Bgy
Teodora Alonzo	Bgy
Trancoville	Bgy
Rock Quarry, Upper	Bgy
Victoria Village	Bgy
Quirino Hill, West	Bgy
Andres Bonifacio	Bgy
Legarda-Burnham-Kisad	Bgy
Imelda R. Marcos	Bgy
Lourdes Subdivision, Proper	Bgy
Quirino-Magsaysay, Upper	Bgy
A. Bonifacio-Caguioa-Rimando	Bgy
Ambiong	Bgy
Aurora Hill, South Central	Bgy
Abanao-Zandueta-Kayong-Chugum-Otek	Bgy
Bagong Lipunan	Bgy
BGH Compound	Bgy
Bayan Park Village	Bgy
Camdas Subdivision	Bgy
Palma-Urbano	Bgy
Dominican Hill-Mirador	Bgy
Alfonso Tabora	Bgy
Dontogan	Bgy
Ferdinand	Bgy
Happy Homes	Bgy
Harrison-Claudio Carantes	Bgy
Honeymoon	Bgy
Kabayanihan	Bgy
Kayang-Hilltop	Bgy
Gabriela Silang	Bgy
Liwanag-Loakan	Bgy
Malcolm Square-Perfecto	Bgy
Manuel A. Roxas	Bgy
Padre Burgos	Bgy
Quezon Hill, Upper	Bgy
Rock Quarry, Middle	Bgy
Phil-Am	Bgy
Quezon Hill Proper	Bgy
Middle Quezon Hill Subdivision	Bgy
Rizal Monument Area	Bgy
SLU-SVP Housing Village	Bgy
South Drive	Bgy
Magsaysay, Upper	Bgy
Market Subdivision, Upper	Bgy
Padre Zamora	Bgy
Bakun	Mun
Ampusongan	Bgy
Bagu	Bgy
Dalipey	Bgy
Gambang	Bgy
Kayapa	Bgy
Poblacion	Bgy
Sinacbat	Bgy
Bokod	Mun
Ambuclao	Bgy
Bila	Bgy
Bobok-Bisal	Bgy
Daclan	Bgy
Ekip	Bgy
Karao	Bgy
Nawal	Bgy
Pito	Bgy
Poblacion	Bgy
Tikey	Bgy
Buguias	Mun
Abatan	Bgy
Amgaleyguey	Bgy
Amlimay	Bgy
Baculongan Norte	Bgy
Bangao	Bgy
Buyacaoan	Bgy
Calamagan	Bgy
Catlubong	Bgy
Loo	Bgy
Natubleng	Bgy
Poblacion	Bgy
Baculongan Sur	Bgy
Lengaoan	Bgy
Sebang	Bgy
Itogon	Mun
Ampucao	Bgy
Dalupirip	Bgy
Gumatdang	Bgy
Loacan	Bgy
Poblacion	Bgy
Tinongdan	Bgy
Tuding	Bgy
Ucab	Bgy
Virac	Bgy
Kabayan	Mun
Adaoay	Bgy
Anchukey	Bgy
Ballay	Bgy
Bashoy	Bgy
Batan	Bgy
Duacan	Bgy
Eddet	Bgy
Gusaran	Bgy
Kabayan Barrio	Bgy
Lusod	Bgy
Pacso	Bgy
Poblacion	Bgy
Tawangan	Bgy
Kapangan	Mun
Balakbak	Bgy
Beleng-Belis	Bgy
Boklaoan	Bgy
Cayapes	Bgy
Cuba	Bgy
Datakan	Bgy
Gadang	Bgy
Gasweling	Bgy
Labueg	Bgy
Paykek	Bgy
Poblacion Central	Bgy
Pudong	Bgy
Pongayan	Bgy
Sagubo	Bgy
Taba-ao	Bgy
Kibungan	Mun
Badeo	Bgy
Lubo	Bgy
Madaymen	Bgy
Palina	Bgy
Poblacion	Bgy
Sagpat	Bgy
Tacadang	Bgy
La Trinidad (Capital)	Mun
Alapang	Bgy
Alno	Bgy
Ambiong	Bgy
Bahong	Bgy
Balili	Bgy
Beckel	Bgy
Bineng	Bgy
Betag	Bgy
Cruz	Bgy
Lubas	Bgy
Pico	Bgy
Poblacion	Bgy
Puguis	Bgy
Shilan	Bgy
Tawang	Bgy
Wangal	Bgy
Mankayan	Mun
Balili	Bgy
Bedbed	Bgy
Bulalacao	Bgy
Cabiten	Bgy
Colalo	Bgy
Guinaoang	Bgy
Paco	Bgy
Palasaan	Bgy
Poblacion	Bgy
Sapid	Bgy
Tabio	Bgy
Taneg	Bgy
Sablan	Mun
Bagong	Bgy
Balluay	Bgy
Banangan	Bgy
Banengbeng	Bgy
Bayabas	Bgy
Kamog	Bgy
Pappa	Bgy
Poblacion	Bgy
Tuba	Mun
Ansagan	Bgy
Camp One	Bgy
Camp 3	Bgy
Camp 4	Bgy
Nangalisan	Bgy
Poblacion	Bgy
San Pascual	Bgy
Tabaan Norte	Bgy
Tabaan Sur	Bgy
Tadiangan	Bgy
Taloy Norte	Bgy
Taloy Sur	Bgy
Twin Peaks	Bgy
Tublay	Mun
Ambassador	Bgy
Ambongdolan	Bgy
Ba-ayan	Bgy
Basil	Bgy
Daclan	Bgy
Caponga (Pob.)	Bgy
Tublay Central	Bgy
Tuel	Bgy
Ifugao	Prov
Banaue	Mun
Amganad	Bgy
Anaba	Bgy
Bangaan	Bgy
Batad	Bgy
Bocos	Bgy
Banao	Bgy
Cambulo	Bgy
Ducligan	Bgy
Gohang	Bgy
Kinakin	Bgy
Poblacion	Bgy
Poitan	Bgy
San Fernando	Bgy
Balawis	Bgy
Ohaj	Bgy
Tam-an	Bgy
View Point	Bgy
Pula	Bgy
Hungduan	Mun
Abatan	Bgy
Bangbang	Bgy
Maggok	Bgy
Poblacion	Bgy
Bokiawan	Bgy
Hapao	Bgy
Lubo-ong	Bgy
Nungulunan	Bgy
Ba-ang	Bgy
Kiangan	Mun
Ambabag	Bgy
Baguinge	Bgy
Bokiawan	Bgy
Dalligan	Bgy
Duit	Bgy
Hucab	Bgy
Julongan	Bgy
Lingay	Bgy
Mungayang	Bgy
Nagacadan	Bgy
Pindongan	Bgy
Poblacion	Bgy
Tuplac	Bgy
Bolog	Bgy
Lagawe (Capital)	Mun
Abinuan	Bgy
Banga	Bgy
Boliwong	Bgy
Burnay	Bgy
Buyabuyan	Bgy
Caba	Bgy
Cudog	Bgy
Dulao	Bgy
Jucbong	Bgy
Luta	Bgy
Montabiong	Bgy
Olilicon	Bgy
Poblacion South	Bgy
Ponghal	Bgy
Pullaan	Bgy
Tungngod	Bgy
Tupaya	Bgy
Poblacion East	Bgy
Poblacion North	Bgy
Poblacion West	Bgy
Lamut	Mun
Ambasa	Bgy
Hapid	Bgy
Lawig	Bgy
Lucban	Bgy
Mabatobato	Bgy
Magulon	Bgy
Nayon	Bgy
Panopdopan	Bgy
Payawan	Bgy
Pieza	Bgy
Poblacion East	Bgy
Pugol	Bgy
Salamague	Bgy
Bimpal	Bgy
Holowon	Bgy
Poblacion West	Bgy
Sanafe	Bgy
Umilag	Bgy
Mayoyao	Mun
Aduyongan	Bgy
Alimit	Bgy
Ayangan	Bgy
Balangbang	Bgy
Banao	Bgy
Banhal	Bgy
Bongan	Bgy
Buninan	Bgy
Chaya	Bgy
Chumang	Bgy
Guinihon	Bgy
Inwaloy	Bgy
Langayan	Bgy
Liwo	Bgy
Maga	Bgy
Magulon	Bgy
Mapawoy	Bgy
Mayoyao Proper	Bgy
Mongol	Bgy
Nalbu	Bgy
Nattum	Bgy
Palaad	Bgy
Poblacion	Bgy
Talboc	Bgy
Tulaed	Bgy
Bato-Alatbang	Bgy
Epeng	Bgy
Alfonso Lista	Mun
Bangar	Bgy
Busilac	Bgy
Calimag	Bgy
Calupaan	Bgy
Caragasan	Bgy
Dolowog	Bgy
Kiling	Bgy
Namnama	Bgy
Namillangan	Bgy
Pinto	Bgy
Poblacion	Bgy
San Jose	Bgy
San Juan	Bgy
San Marcos	Bgy
San Quintin	Bgy
Santa Maria	Bgy
Santo Domingo	Bgy
Little Tadian	Bgy
Ngileb	Bgy
Laya	Bgy
Aguinaldo	Mun
Awayan	Bgy
Bunhian	Bgy
Butac	Bgy
Chalalo	Bgy
Damag	Bgy
Galonogon	Bgy
Halag	Bgy
Itab	Bgy
Jacmal	Bgy
Majlong	Bgy
Mongayang	Bgy
Posnaan	Bgy
Ta-ang	Bgy
Talite	Bgy
Ubao	Bgy
Buwag	Bgy
Hingyon	Mun
Anao	Bgy
Bangtinon	Bgy
Bitu	Bgy
Cababuyan	Bgy
Mompolia	Bgy
Namulditan	Bgy
O-ong	Bgy
Piwong	Bgy
Poblacion	Bgy
Ubuag	Bgy
Umalbong	Bgy
Northern Cababuyan	Bgy
Tinoc	Mun
Ahin	Bgy
Ap-apid	Bgy
Binablayan	Bgy
Danggo	Bgy
Eheb	Bgy
Gumhang	Bgy
Impugong	Bgy
Luhong	Bgy
Tinoc	Bgy
Tukucan	Bgy
Tulludan	Bgy
Wangwang	Bgy
Asipulo	Mun
Amduntog	Bgy
Antipolo	Bgy
Camandag	Bgy
Cawayan	Bgy
Hallap	Bgy
Namal	Bgy
Nungawa	Bgy
Panubtuban	Bgy
Pula	Bgy
Liwon	Bgy
Kalinga	Prov
Balbalan	Mun
Ababa-an	Bgy
Balantoy	Bgy
Balbalan Proper	Bgy
Balbalasang	Bgy
Buaya	Bgy
Dao-angan	Bgy
Gawa-an	Bgy
Mabaca	Bgy
Maling	Bgy
Pantikian	Bgy
Poswoy	Bgy
Poblacion	Bgy
Talalang	Bgy
Tawang	Bgy
Lubuagan	Mun
Dangoy	Bgy
Mabilong	Bgy
Mabongtot	Bgy
Poblacion	Bgy
Tanglag	Bgy
Lower Uma	Bgy
Upper Uma	Bgy
Antonio Canao	Bgy
Uma del Norte	Bgy
Pasil	Mun
Ableg	Bgy
Balatoc	Bgy
Balinciagao Norte	Bgy
Cagaluan	Bgy
Colayo	Bgy
Dalupa	Bgy
Dangtalan	Bgy
Galdang	Bgy
Guina-ang (Pob.)	Bgy
Magsilay	Bgy
Malucsad	Bgy
Pugong	Bgy
Balenciagao Sur	Bgy
Bagtayan	Bgy
Pinukpuk	Mun
Aciga	Bgy
Allaguia	Bgy
Ammacian	Bgy
Apatan	Bgy
Ba-ay	Bgy
Ballayangon	Bgy
Bayao	Bgy
Wagud	Bgy
Camalog	Bgy
Katabbogan	Bgy
Dugpa	Bgy
Cawagayan	Bgy
Asibanglan	Bgy
Limos	Bgy
Magaogao	Bgy
Malagnat	Bgy
Mapaco	Bgy
Pakawit	Bgy
Pinukpuk Junction	Bgy
Socbot	Bgy
Taga (Pob.)	Bgy
Pinococ	Bgy
Taggay	Bgy
Rizal	Mun
Babalag East (Pob.)	Bgy
Calaocan	Bgy
Kinama	Bgy
Liwan East	Bgy
Liwan West	Bgy
Macutay	Bgy
San Pascual	Bgy
San Quintin	Bgy
Santor	Bgy
Babalag West (Pob.)	Bgy
Bulbol	Bgy
Romualdez	Bgy
San Francisco	Bgy
San Pedro	Bgy
City of Tabuk (Capital)	City
Agbannawag	Bgy
Amlao	Bgy
Appas	Bgy
Bagumbayan	Bgy
Balawag	Bgy
Balong	Bgy
Bantay	Bgy
Bulanao	Bgy
Cabaritan	Bgy
Cabaruan	Bgy
Calaccad	Bgy
Calanan	Bgy
Dilag	Bgy
Dupag	Bgy
Gobgob	Bgy
Guilayon	Bgy
Lanna	Bgy
Laya East	Bgy
Laya West	Bgy
Lucog	Bgy
Magnao	Bgy
Magsaysay	Bgy
Malalao	Bgy
Masablang	Bgy
Nambaran	Bgy
Nambucayan	Bgy
Naneng	Bgy
Dagupan Centro (Pob.)	Bgy
San Juan	Bgy
Suyang	Bgy
Tuga	Bgy
Bado Dangwa	Bgy
Bulo	Bgy
Casigayan	Bgy
Cudal	Bgy
Dagupan Weste	Bgy
Lacnog	Bgy
Malin-awa	Bgy
New Tanglag	Bgy
San Julian	Bgy
Bulanao Norte	Bgy
Ipil	Bgy
Lacnog West	Bgy
Tanudan	Mun
Anggacan	Bgy
Babbanoy	Bgy
Dacalan	Bgy
Gaang	Bgy
Lower Mangali	Bgy
Lower Taloctoc	Bgy
Lower Lubo	Bgy
Upper Lubo	Bgy
Mabaca	Bgy
Pangol	Bgy
Poblacion	Bgy
Upper Taloctoc	Bgy
Anggacan Sur	Bgy
Dupligan	Bgy
Lay-asan	Bgy
Mangali Centro	Bgy
Tinglayan	Mun
Ambato Legleg	Bgy
Bangad Centro	Bgy
Basao	Bgy
Belong Manubal	Bgy
Butbut	Bgy
Bugnay	Bgy
Buscalan	Bgy
Dananao	Bgy
Loccong	Bgy
Luplupa	Bgy
Mallango	Bgy
Poblacion	Bgy
Sumadel 1	Bgy
Sumadel 2	Bgy
Tulgao East	Bgy
Tulgao West	Bgy
Upper Bangad	Bgy
Ngibat	Bgy
Old Tinglayan	Bgy
Lower Bangad	Bgy
Mountain Province	Prov
Barlig	Mun
Chupac	Bgy
Fiangtin	Bgy
Kaleo	Bgy
Latang	Bgy
Lias Kanluran	Bgy
Lingoy	Bgy
Lunas	Bgy
Macalana	Bgy
Ogo-og	Bgy
Gawana (Pob.)	Bgy
Lias Silangan	Bgy
Bauko	Mun
Abatan	Bgy
Bagnen Oriente	Bgy
Bagnen Proper	Bgy
Balintaugan	Bgy
Banao	Bgy
Bila	Bgy
Guinzadan Central	Bgy
Guinzadan Norte	Bgy
Guinzadan Sur	Bgy
Lagawa	Bgy
Leseb	Bgy
Mabaay	Bgy
Mayag	Bgy
Monamon Norte	Bgy
Monamon Sur	Bgy
Mount Data	Bgy
Otucan Norte	Bgy
Otucan Sur	Bgy
Poblacion	Bgy
Sadsadan	Bgy
Sinto	Bgy
Tapapan	Bgy
Besao	Mun
Agawa	Bgy
Ambaguio	Bgy
Banguitan	Bgy
Besao East	Bgy
Besao West	Bgy
Catengan	Bgy
Gueday	Bgy
Lacmaan	Bgy
Laylaya	Bgy
Padangan	Bgy
Payeo	Bgy
Suquib	Bgy
Tamboan	Bgy
Kin-iway (Pob.)	Bgy
Bontoc (Capital)	Mun
Alab Proper	Bgy
Alab Oriente	Bgy
Balili	Bgy
Bayyo	Bgy
Bontoc Ili	Bgy
Caneo	Bgy
Dalican	Bgy
Gonogon	Bgy
Guinaang	Bgy
Mainit	Bgy
Maligcong	Bgy
Samoki	Bgy
Talubin	Bgy
Tocucan	Bgy
Poblacion	Bgy
Caluttit	Bgy
Natonin	Mun
Alunogan	Bgy
Balangao	Bgy
Banao	Bgy
Banawal	Bgy
Butac	Bgy
Maducayan	Bgy
Poblacion	Bgy
Saliok	Bgy
Sta. Isabel	Bgy
Tonglayan	Bgy
Pudo	Bgy
Paracelis	Mun
Anonat	Bgy
Bacarri	Bgy
Bananao	Bgy
Bantay	Bgy
Butigue	Bgy
Bunot	Bgy
Buringal	Bgy
Palitud	Bgy
Poblacion	Bgy
Sabangan	Mun
Bao-angan	Bgy
Bun-ayan	Bgy
Busa	Bgy
Camatagan	Bgy
Capinitan	Bgy
Data	Bgy
Gayang	Bgy
Lagan	Bgy
Losad	Bgy
Namatec	Bgy
Napua	Bgy
Pingad	Bgy
Poblacion	Bgy
Supang	Bgy
Tambingan	Bgy
Sadanga	Mun
Anabel	Bgy
Belwang	Bgy
Betwagan	Bgy
Bekigan	Bgy
Poblacion	Bgy
Sacasacan	Bgy
Saclit	Bgy
Demang	Bgy
Sagada	Mun
Aguid	Bgy
Tetepan Sur	Bgy
Ambasing	Bgy
Angkeling	Bgy
Antadao	Bgy
Balugan	Bgy
Bangaan	Bgy
Dagdag (Pob.)	Bgy
Demang (Pob.)	Bgy
Fidelisan	Bgy
Kilong	Bgy
Madongo	Bgy
Poblacion	Bgy
Pide	Bgy
Nacagang	Bgy
Suyo	Bgy
Taccong	Bgy
Tanulong	Bgy
Tetepan Norte	Bgy
Tadian	Mun
Balaoa	Bgy
Banaao	Bgy
Bantey	Bgy
Batayan	Bgy
Bunga	Bgy
Cadad-anan	Bgy
Cagubatan	Bgy
Duagan	Bgy
Dacudac	Bgy
Kayan East	Bgy
Lenga	Bgy
Lubon	Bgy
Mabalite	Bgy
Masla	Bgy
Pandayan	Bgy
Poblacion	Bgy
Sumadel	Bgy
Tue	Bgy
Kayan West	Bgy
Apayao	Prov
Calanasan	Mun
Butao	Bgy
Cadaclan	Bgy
Langnao	Bgy
Lubong	Bgy
Naguilian	Bgy
Namaltugan	Bgy
Poblacion	Bgy
Sabangan	Bgy
Santa Filomena	Bgy
Tubongan	Bgy
Tanglagan	Bgy
Tubang	Bgy
Don Roque Ablan Sr.	Bgy
Eleazar	Bgy
Eva Puzon	Bgy
Kabugawan	Bgy
Macalino	Bgy
Santa Elena	Bgy
Conner	Mun
Allangigan	Bgy
Buluan	Bgy
Caglayan	Bgy
Calafug	Bgy
Cupis	Bgy
Daga	Bgy
Guinamgaman	Bgy
Karikitan	Bgy
Katablangan	Bgy
Malama	Bgy
Manag	Bgy
Nabuangan	Bgy
Paddaoan	Bgy
Puguin	Bgy
Ripang	Bgy
Sacpil	Bgy
Talifugo	Bgy
Banban	Bgy
Guinaang	Bgy
Ili	Bgy
Mawigue	Bgy
Flora	Mun
Allig	Bgy
Anninipan	Bgy
Atok	Bgy
Bagutong	Bgy
Balasi	Bgy
Balluyan	Bgy
Malayugan	Bgy
Malubibit Norte	Bgy
Poblacion East	Bgy
Tamalunog	Bgy
Mallig	Bgy
Malubibit Sur	Bgy
Poblacion West	Bgy
San Jose	Bgy
Santa Maria	Bgy
Upper Atok	Bgy
Kabugao (Capital)	Mun
Badduat	Bgy
Baliwanan	Bgy
Bulu	Bgy
Dagara	Bgy
Dibagat	Bgy
Cabetayan	Bgy
Karagawan	Bgy
Kumao	Bgy
Laco	Bgy
Lenneng	Bgy
Lucab	Bgy
Luttuacan	Bgy
Madatag	Bgy
Madduang	Bgy
Magabta	Bgy
Maragat	Bgy
Musimut	Bgy
Nagbabalayan	Bgy
Poblacion	Bgy
Tuyangan	Bgy
Waga	Bgy
Luna	Mun
Bacsay	Bgy
Capagaypayan	Bgy
Dagupan	Bgy
Lappa	Bgy
Marag	Bgy
Poblacion	Bgy
Quirino	Bgy
Salvacion	Bgy
San Francisco	Bgy
San Isidro Norte	Bgy
San Sebastian	Bgy
Santa Lina	Bgy
Tumog	Bgy
Zumigui	Bgy
Cagandungan	Bgy
Calabigan	Bgy
Cangisitan	Bgy
Luyon	Bgy
San Gregorio	Bgy
San Isidro Sur	Bgy
Shalom	Bgy
Turod	Bgy
Pudtol	Mun
Aga	Bgy
Alem	Bgy
Cabatacan	Bgy
Cacalaggan	Bgy
Capannikian	Bgy
Lower Maton	Bgy
Malibang	Bgy
Mataguisi	Bgy
Poblacion	Bgy
San Antonio	Bgy
Swan	Bgy
Upper Maton	Bgy
Amado	Bgy
Aurora	Bgy
Doña Loreta	Bgy
Emilia	Bgy
Imelda	Bgy
Lt. Balag	Bgy
Lydia	Bgy
San Jose	Bgy
San Luis	Bgy
San Mariano	Bgy
Santa Marcela	Mun
Barocboc	Bgy
Consuelo	Bgy
Imelda	Bgy
Malekkeg	Bgy
Marcela (Pob.)	Bgy
Nueva	Bgy
Panay	Bgy
San Antonio	Bgy
Sipa Proper	Bgy
Emiliana	Bgy
San Carlos	Bgy
San Juan	Bgy
San Mariano	Bgy
Autonomous Region In Muslim Mindanao (ARMM)	Reg
Basilan	Prov
City of Lamitan (Capital)	City
Arco	Bgy
Ba-as	Bgy
Baimbing	Bgy
Balagtasan	Bgy
Balas	Bgy
Balobo	Bgy
Bato	Bgy
Boheyakan	Bgy
Buahan	Bgy
Boheibu	Bgy
Bohesapa	Bgy
Bulingan	Bgy
Cabobo	Bgy
Campo Uno	Bgy
Colonia	Bgy
Calugusan	Bgy
Kulay Bato	Bgy
Limo-ok	Bgy
Lo-ok	Bgy
Lumuton	Bgy
Luksumbang	Bgy
Malo-ong Canal	Bgy
Malo-ong San Jose	Bgy
Parangbasak	Bgy
Santa Clara	Bgy
Tandong Ahas	Bgy
Tumakid	Bgy
Ubit	Bgy
Bohebessey	Bgy
Baungos	Bgy
Danit-Puntocan	Bgy
Sabong	Bgy
Sengal	Bgy
Ulame	Bgy
Bohenange	Bgy
Boheyawas	Bgy
Bulanting	Bgy
Lebbuh	Bgy
Maganda	Bgy
Malakas	Bgy
Maligaya	Bgy
Malinis (Pob.)	Bgy
Matatag	Bgy
Matibay	Bgy
Simbangon	Bgy
Lantawan	Mun
Atong-atong	Bgy
Baungis	Bgy
Bulanza	Bgy
Lantawan Proper (Pob.)	Bgy
Lower Manggas	Bgy
Matarling	Bgy
Matikang	Bgy
Tairan	Bgy
Upper Manggas	Bgy
Bagbagon	Bgy
Bulan-bulan	Bgy
Suba-an	Bgy
Lower Bañas	Bgy
Upper Bañas	Bgy
Calugusan	Bgy
Canibungan	Bgy
Landugan	Bgy
Lawila	Bgy
Lawi-lawi	Bgy
Pamucalin	Bgy
Switch Yakal	Bgy
Paniongan	Bgy
Luuk-Maluha	Bgy
Calayan	Bgy
Parian-Baunoh	Bgy
Maluso	Mun
Abong-Abong	Bgy
Batungal	Bgy
Calang Canas	Bgy
Guanan North	Bgy
Guanan South	Bgy
Limbubong	Bgy
Mahayahay Lower	Bgy
Muslim Area	Bgy
Port Holland Zone I Pob.	Bgy
Port Holland Zone II Pob.	Bgy
Port Holland Zone III Pob.	Bgy
Port Holland Zone IV	Bgy
Townsite (Pob.)	Bgy
Taberlongan	Bgy
Fuente Maluso	Bgy
Tamuk	Bgy
Tubigan	Bgy
Mahayahay Upper	Bgy
Port Holland Zone V	Bgy
Upper Garlayan	Bgy
Sumisip	Mun
Bacung	Bgy
Benembengan Lower	Bgy
Buli-buli	Bgy
Cabcaban	Bgy
Guiong	Bgy
Manaul	Bgy
Mangal (Pob.)	Bgy
Sumisip Central	Bgy
Tumahubong	Bgy
Libug	Bgy
Tongsengal	Bgy
Baiwas	Bgy
Basak	Bgy
Cabengbeng Lower	Bgy
Cabengbeng Upper	Bgy
Luuk-Bait	Bgy
Mahatalang	Bgy
Benembengan Upper	Bgy
Bohe-languyan	Bgy
Ettub-ettub	Bgy
Kaum-Air	Bgy
Limbocandis	Bgy
Lukketon	Bgy
Marang	Bgy
Mebak	Bgy
Sahaya Bohe Bato	Bgy
Sapah Bulak	Bgy
Tikus	Bgy
Kaumpamatsakem	Bgy
Tipo-Tipo	Mun
Badja	Bgy
Bohebaca	Bgy
Bohelebung	Bgy
Lagayas	Bgy
Limbo-Upas	Bgy
Tipo-tipo Proper (Pob.)	Bgy
Baguindan	Bgy
Banah	Bgy
Bohe-Tambak	Bgy
Silangkum	Bgy
Bangcuang	Bgy
Tuburan	Mun
Lahi-lahi	Bgy
Sinulatan	Bgy
Bohetambis	Bgy
Duga-a	Bgy
Mahawid	Bgy
Lower Sinangkapan	Bgy
Tablas Usew	Bgy
Calut	Bgy
Katipunan	Bgy
Lower Tablas	Bgy
Akbar	Mun
Caddayan	Bgy
Linongan	Bgy
Lower Bato-bato	Bgy
Mangalut	Bgy
Manguso	Bgy
Paguengan	Bgy
Semut	Bgy
Upper Bato-bato	Bgy
Upper Sinangkapan	Bgy
Al-Barka	Mun
Apil-apil	Bgy
Bato-bato	Bgy
Bohe-Piang	Bgy
Bucalao	Bgy
Cambug	Bgy
Danapah	Bgy
Guinanta	Bgy
Kailih	Bgy
Kinukutan	Bgy
Kuhon	Bgy
Kuhon Lennuh	Bgy
Linuan	Bgy
Lookbisaya	Bgy
Macalang	Bgy
Magcawa	Bgy
Sangkahan	Bgy
Hadji Mohammad Ajul	Mun
Basakan	Bgy
Buton	Bgy
Candiis	Bgy
Langil	Bgy
Langong	Bgy
Languyan	Bgy
Pintasan	Bgy
Seronggon	Bgy
Sibago	Bgy
Sulutan Matangal	Bgy
Tuburan Proper (Pob.)	Bgy
Ungkaya Pukan	Mun
Amaloy	Bgy
Bohe-Pahuh	Bgy
Bohe-Suyak	Bgy
Cabangalan	Bgy
Danit	Bgy
Kamamburingan	Bgy
Matata	Bgy
Materling	Bgy
Pipil	Bgy
Sungkayut	Bgy
Tongbato	Bgy
Ulitan	Bgy
Hadji Muhtamad	Mun
Baluk-baluk	Bgy
Dasalan	Bgy
Lubukan	Bgy
Luukbongsod	Bgy
Mananggal	Bgy
Palahangan	Bgy
Panducan	Bgy
Sangbay Big	Bgy
Sangbay Small	Bgy
Tausan	Bgy
Tabuan-Lasa	Mun
Babag	Bgy
Balanting	Bgy
Boloh-boloh	Bgy
Bukut-Umus	Bgy
Kaumpurnah	Bgy
Lanawan	Bgy
Pisak-pisak	Bgy
Saluping	Bgy
Suligan	Bgy
Sulloh	Bgy
Tambulig Buton	Bgy
Tong-Umus	Bgy
Lanao del Sur	Prov
Bacolod-Kalawi	Mun
Ampao	Bgy
Bagoaingud	Bgy
Balut	Bgy
Barua	Bgy
Buadiawani	Bgy
Bubong	Bgy
Dilabayan	Bgy
Dipatuan	Bgy
Daramoyod	Bgy
Gandamato	Bgy
Gurain	Bgy
Ilian	Bgy
Lama	Bgy
Liawao	Bgy
Lumbaca-Ingud	Bgy
Madanding	Bgy
Orong	Bgy
Pindolonan	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Raya	Bgy
Rorowan	Bgy
Sugod	Bgy
Tambo	Bgy
Tuka I	Bgy
Tuka II	Bgy
Balabagan	Mun
Banago	Bgy
Barorao	Bgy
Batuan	Bgy
Budas	Bgy
Calilangan	Bgy
Igabay	Bgy
Lalabuan	Bgy
Magulalung Occidental	Bgy
Magulalung Oriental	Bgy
Molimoc	Bgy
Narra	Bgy
Plasan	Bgy
Purakan	Bgy
Buisan	Bgy
Buenavista	Bgy
Lorenzo	Bgy
Lower Itil	Bgy
Macao	Bgy
Poblacion	Bgy
Upper Itil	Bgy
Bagoaingud	Bgy
Ilian	Bgy
Lumbac	Bgy
Matampay	Bgy
Matanog	Bgy
Pindolonan	Bgy
Tataya	Bgy
Balindong	Mun
Abaga	Bgy
Poblacion	Bgy
Pantaragoo	Bgy
Bantoga Wato	Bgy
Barit	Bgy
Bubong	Bgy
Bubong Cadapaan	Bgy
Borakis	Bgy
Bualan	Bgy
Cadapaan	Bgy
Cadayonan	Bgy
Kaluntay	Bgy
Dadayag	Bgy
Dado	Bgy
Dibarusan	Bgy
Dilausan	Bgy
Dimarao	Bgy
Ingud	Bgy
Lalabuan	Bgy
Lilod	Bgy
Lumbayao	Bgy
Limbo	Bgy
Lumbac Lalan	Bgy
Lumbac Wato	Bgy
Magarang	Bgy
Nusa Lumba Ranao	Bgy
Padila	Bgy
Pagayawan	Bgy
Paigoay	Bgy
Raya	Bgy
Salipongan	Bgy
Talub	Bgy
Tomarompong	Bgy
Tantua Raya	Bgy
Tuka Bubong	Bgy
Bolinsong	Bgy
Lati	Bgy
Malaig	Bgy
Bayang	Mun
Bagoaingud	Bgy
Bairan (Pob.)	Bgy
Bandingun	Bgy
Biabi	Bgy
Bialaan	Bgy
Cadayonan	Bgy
Lumbac Cadayonan	Bgy
Lalapung Central	Bgy
Condaraan Pob.	Bgy
Cormatan	Bgy
Ilian	Bgy
Lalapung Proper (Pob.)	Bgy
Bubong Lilod	Bgy
Linao	Bgy
Linuk (Pob.)	Bgy
Liong	Bgy
Lumbac	Bgy
Mimbalawag	Bgy
Maliwanag	Bgy
Mapantao	Bgy
Cadingilan Occidental	Bgy
Cadingilan Oriental	Bgy
Palao	Bgy
Pama-an	Bgy
Pamacotan	Bgy
Pantar	Bgy
Parao	Bgy
Patong	Bgy
Porotan	Bgy
Rantian	Bgy
Raya Cadayonan	Bgy
Rinabor (Pob.)	Bgy
Sapa	Bgy
Samporna (Pob.)	Bgy
Silid	Bgy
Sugod	Bgy
Sultan Pandapatan	Bgy
Sumbag (Pob.)	Bgy
Tagoranao	Bgy
Tangcal	Bgy
Tangcal Proper (Pob.)	Bgy
Tuca (Pob.)	Bgy
Tomarompong	Bgy
Tomongcal Ligi	Bgy
Torogan	Bgy
Lalapung Upper	Bgy
Gandamato	Bgy
Bubong Raya	Bgy
Poblacion	Bgy
Binidayan	Mun
Badak	Bgy
Baguiangun	Bgy
Balut Maito	Bgy
Basak	Bgy
Bubong	Bgy
Bubonga-Ranao	Bgy
Dansalan Dacsula	Bgy
Ingud	Bgy
Kialilidan	Bgy
Lumbac	Bgy
Macaguiling	Bgy
Madaya	Bgy
Magonaya	Bgy
Maindig	Bgy
Masolun	Bgy
Olama	Bgy
Pagalamatan (Pob.)	Bgy
Pantar	Bgy
Picalilangan	Bgy
Picotaan	Bgy
Pindolonan	Bgy
Poblacion	Bgy
Soldaroro	Bgy
Tambac	Bgy
Timbangan	Bgy
Tuca	Bgy
Bubong	Mun
Bagoaingud	Bgy
Bansayan	Bgy
Basingan	Bgy
Batangan	Bgy
Bualan	Bgy
Poblacion	Bgy
Bubonga Didagun	Bgy
Carigongan	Bgy
Bacolod	Bgy
Dilabayan	Bgy
Dimapatoy	Bgy
Dimayon Proper	Bgy
Diolangan	Bgy
Guiguikun	Bgy
Dibarosan	Bgy
Madanding	Bgy
Malungun	Bgy
Masorot	Bgy
Matampay Dimarao	Bgy
Miabalawag	Bgy
Montiaan	Bgy
Pagayawan	Bgy
Palao	Bgy
Panalawan	Bgy
Pantar	Bgy
Pendogoan	Bgy
Polayagan	Bgy
Ramain Bubong	Bgy
Rogero	Bgy
Salipongan	Bgy
Sunggod	Bgy
Taboro	Bgy
Dalaon	Bgy
Dimayon	Bgy
Pindolonan	Bgy
Punud	Bgy
Butig	Mun
Butig Proper	Bgy
Cabasaran	Bgy
Coloyan Tambo	Bgy
Dilimbayan	Bgy
Dolangan	Bgy
Pindolonan	Bgy
Bayabao Poblacion	Bgy
Poktan	Bgy
Ragayan	Bgy
Raya	Bgy
Samer	Bgy
Sandab Madaya	Bgy
Sundig	Bgy
Tiowi	Bgy
Timbab	Bgy
Malungun	Bgy
Ganassi	Mun
Bagoaingud	Bgy
Balintad	Bgy
Barit	Bgy
Bato Batoray	Bgy
Campong a Raya	Bgy
Gadongan	Bgy
Gui	Bgy
Linuk	Bgy
Lumbac	Bgy
Macabao	Bgy
Macaguiling	Bgy
Pagalongan	Bgy
Panggawalupa	Bgy
Pantaon A	Bgy
Para-aba	Bgy
Pindolonan	Bgy
Poblacion	Bgy
Baya	Bgy
Sogod Madaya	Bgy
Tabuan	Bgy
Taganonok	Bgy
Taliogon	Bgy
Masolun	Bgy
Lumbacaingud	Bgy
Sekun Matampay	Bgy
Dapaan	Bgy
Balintad A	Bgy
Barorao	Bgy
Campong Sabela	Bgy
Pangadapun	Bgy
Pantaon	Bgy
Pamalian	Bgy
Kapai	Mun
Cormatan	Bgy
Dimagaling	Bgy
Dimunda	Bgy
Doronan	Bgy
Poblacion	Bgy
Malna Proper	Bgy
Pagalongan	Bgy
Parao	Bgy
Kasayanan	Bgy
Kasayanan West	Bgy
Dilabayan	Bgy
Dilimbayan	Bgy
Kibolos	Bgy
Kining	Bgy
Pindolonan	Bgy
Babayog	Bgy
Gadongan	Bgy
Lidasan	Bgy
Macadar	Bgy
Pantaon	Bgy
Lumba-Bayabao	Mun
Bacolod I	Bgy
Bacolod II	Bgy
Bantayao	Bgy
Barit	Bgy
Baugan	Bgy
Buad Lumbac	Bgy
Cabasaran	Bgy
Calilangan	Bgy
Carandangan-Mipaga	Bgy
Cormatan Langban	Bgy
Dialongana	Bgy
Dilindongan-Cadayonan	Bgy
Gadongan	Bgy
Galawan	Bgy
Gambai	Bgy
Kasola	Bgy
Lalangitun	Bgy
Lama	Bgy
Lindongan Dialongana	Bgy
Lobo Basara	Bgy
Lumbac Bacayawan	Bgy
Macaguiling	Bgy
Mapantao	Bgy
Mapoling	Bgy
Pagayawan	Bgy
Maribo (Pob.)	Bgy
Posudaragat	Bgy
Rumayas	Bgy
Sabala Bantayao	Bgy
Salaman	Bgy
Salolodun Berwar	Bgy
Sarigidan Madiar	Bgy
Sunggod	Bgy
Taluan	Bgy
Tamlang	Bgy
Tongcopan	Bgy
Turogan	Bgy
Minaring Diladigan	Bgy
Lumbatan	Mun
Alog	Bgy
Basayungun	Bgy
Buad	Bgy
Budi	Bgy
Dago-ok	Bgy
Dalama	Bgy
Dalipuga	Bgy
Lalapung	Bgy
Lumbac	Bgy
Lumbac Bacayawan	Bgy
Lunay	Bgy
Macadar	Bgy
Madaya	Bgy
Minanga	Bgy
Pantar	Bgy
Picotaan	Bgy
Poblacion	Bgy
Tambac	Bgy
Bubong Macadar	Bgy
Penaring	Bgy
Ligue	Bgy
Madalum	Mun
Abaga	Bgy
Bagoaingud	Bgy
Basak	Bgy
Bato	Bgy
Bubong	Bgy
Kormatan	Bgy
Dandamun	Bgy
Diampaca	Bgy
Dibarosan	Bgy
Delausan	Bgy
Gadongan	Bgy
Gurain	Bgy
Cabasaran	Bgy
Cadayonan	Bgy
Liangan I	Bgy
Lilitun	Bgy
Linao	Bgy
Linuk	Bgy
Madaya	Bgy
Pagayawan	Bgy
Poblacion	Bgy
Punud	Bgy
Raya	Bgy
Riray	Bgy
Sabanding	Bgy
Salongabanding	Bgy
Sugod	Bgy
Tamporong	Bgy
Tongantongan	Bgy
Udangun	Bgy
Liangan	Bgy
Lumbac	Bgy
Paridi-Kalimodan	Bgy
Racotan	Bgy
Bacayawan	Bgy
Padian Torogan I	Bgy
Sogod Kaloy	Bgy
Madamba	Mun
Balintad	Bgy
Balagunun	Bgy
Bawang	Bgy
Biabe	Bgy
Bubong Uyaan	Bgy
Cabasaran	Bgy
Dibarusan	Bgy
Lakitan	Bgy
Liangan	Bgy
Linuk	Bgy
Lumbaca Ingud	Bgy
Palao	Bgy
Pantaon	Bgy
Pantar	Bgy
Madamba	Bgy
Punud	Bgy
Tubaran	Bgy
Tambo	Bgy
Tuca	Bgy
Uyaan Proper (Pob.)	Bgy
Tulay	Bgy
Ilian	Bgy
Pagayonan	Bgy
Pangadapan	Bgy
Malabang	Mun
Bacayawan	Bgy
Badak Lumao	Bgy
Bagoaingud	Bgy
Boniga	Bgy
BPS Village	Bgy
Betayan	Bgy
Campo Muslim	Bgy
China Town (Pob.)	Bgy
Corahab	Bgy
Diamaro	Bgy
Inandayan	Bgy
Cabasaran	Bgy
Calumbog	Bgy
Lamin	Bgy
Mable	Bgy
Mananayo	Bgy
Manggahan	Bgy
Masao	Bgy
Montay	Bgy
Pasir	Bgy
Rebocun	Bgy
Sarang	Bgy
Tacub	Bgy
Tambara	Bgy
Tiongcop	Bgy
Tuboc	Bgy
Matampay	Bgy
Calibagat	Bgy
Jose Abad Santos	Bgy
Macuranding	Bgy
Matalin	Bgy
Matling	Bgy
Pialot	Bgy
Sumbagarogong	Bgy
Banday	Bgy
Bunk House	Bgy
Madaya	Bgy
Marantao	Mun
Bacayawan	Bgy
Cawayan Bacolod	Bgy
Bacong	Bgy
Camalig Bandara Ingud	Bgy
Camalig Bubong	Bgy
Camalig (Pob.)	Bgy
Inudaran Campong	Bgy
Cawayan	Bgy
Daanaingud	Bgy
Cawayan Kalaw	Bgy
Kialdan	Bgy
Lumbac Kialdan	Bgy
Cawayan Linuk	Bgy
Lubo	Bgy
Inudaran Lumbac	Bgy
Mantapoli	Bgy
Matampay	Bgy
Maul	Bgy
Nataron	Bgy
Pagalongan Bacayawan	Bgy
Pataimas	Bgy
Poona Marantao	Bgy
Punud Proper	Bgy
Tacub	Bgy
Maul Ilian	Bgy
Palao	Bgy
Banga-Pantar	Bgy
Batal-Punud	Bgy
Bubong Madanding	Bgy
Ilian	Bgy
Inudaran Loway	Bgy
Maul Lumbaca Ingud	Bgy
Poblacion	Bgy
Tuca Kialdan	Bgy
City of Marawi (Capital)	City
Ambolong	Bgy
Bacolod Chico Proper	Bgy
Banga	Bgy
Bangco	Bgy
Banggolo Poblacion	Bgy
Bangon	Bgy
Beyaba-Damag	Bgy
Bito Buadi Itowa	Bgy
Bito Buadi Parba	Bgy
Bubonga Pagalamatan	Bgy
Bubonga Lilod Madaya	Bgy
Boganga	Bgy
Boto Ambolong	Bgy
Bubonga Cadayonan	Bgy
Bubong Lumbac	Bgy
Bubonga Marawi	Bgy
Bubonga Punod	Bgy
Cabasaran	Bgy
Cabingan	Bgy
Cadayonan	Bgy
Cadayonan I	Bgy
Calocan East	Bgy
Calocan West	Bgy
Kormatan Matampay	Bgy
Daguduban	Bgy
Dansalan	Bgy
Datu Sa Dansalan	Bgy
Dayawan	Bgy
Dimaluna	Bgy
Dulay	Bgy
Dulay West	Bgy
East Basak	Bgy
Emie Punud	Bgy
Fort	Bgy
Gadongan	Bgy
Buadi Sacayo	Bgy
Guimba	Bgy
Kapantaran	Bgy
Kilala	Bgy
Lilod Madaya (Pob.)	Bgy
Lilod Saduc	Bgy
Lomidong	Bgy
Lumbaca Madaya (Pob.)	Bgy
Lumbac Marinaut	Bgy
Lumbaca Toros	Bgy
Malimono	Bgy
Basak Malutlut	Bgy
Gadongan Mapantao	Bgy
Amito Marantao	Bgy
Marinaut East	Bgy
Marinaut West	Bgy
Matampay	Bgy
Pantaon	Bgy
Mipaga Proper	Bgy
Moncado Colony	Bgy
Moncado Kadingilan	Bgy
Moriatao Loksadato	Bgy
Datu Naga	Bgy
Navarro	Bgy
Olawa Ambolong	Bgy
Pagalamatan Gambai	Bgy
Pagayawan	Bgy
Panggao Saduc	Bgy
Papandayan	Bgy
Paridi	Bgy
Patani	Bgy
Pindolonan	Bgy
Poona Marantao	Bgy
Pugaan	Bgy
Rapasun MSU	Bgy
Raya Madaya I	Bgy
Raya Madaya II	Bgy
Raya Saduc	Bgy
Rorogagus Proper	Bgy
Rorogagus East	Bgy
Sabala Manao	Bgy
Sabala Manao Proper	Bgy
Saduc Proper	Bgy
Sagonsongan	Bgy
Sangcay Dansalan	Bgy
Somiorang	Bgy
South Madaya Proper	Bgy
Sugod Proper	Bgy
Tampilong	Bgy
Timbangalan	Bgy
Tuca Ambolong	Bgy
Tolali	Bgy
Toros	Bgy
Tuca	Bgy
Tuca Marinaut	Bgy
Tongantongan-Tuca Timbangalan	Bgy
Wawalayan Calocan	Bgy
Wawalayan Marinaut	Bgy
Marawi Poblacion	Bgy
Norhaya Village	Bgy
Papandayan Caniogan	Bgy
Masiu	Mun
Abdullah Buisan	Bgy
Caramian Alim Raya	Bgy
Alip Lalabuan	Bgy
Alumpang Paino Mimbalay	Bgy
Mai Ditimbang Balindong	Bgy
Mai Sindaoloan Dansalan	Bgy
Lakadun	Bgy
Maranat Bontalis	Bgy
Dalog Balut	Bgy
Gindolongan Alabat	Bgy
Gondarangin Asa Adigao	Bgy
Sawir	Bgy
Moriatao-Bai Labay	Bgy
Laila Lumbac Bacon	Bgy
Lanco Dimapatoy	Bgy
Talub Langi	Bgy
Lomigis Sucod	Bgy
Macabangan Imbala	Bgy
Macadaag Talaguian	Bgy
Macalupang Lumbac Caramian	Bgy
Magayo Bagoaingud	Bgy
Macompara Apa Mimbalay	Bgy
Manalocon Talub	Bgy
Putad Marandang Buisan	Bgy
Matao Araza	Bgy
Mocamad Tangul	Bgy
Pantao	Bgy
Sambowang Atawa	Bgy
Tamboro Cormatan	Bgy
Towanao Arangga	Bgy
Tomambiling Lumbaca Ingud	Bgy
Unda Dayawan	Bgy
Buadi Amloy	Bgy
Kalilangan	Bgy
Lumbaca Ingud	Bgy
Mulondo	Mun
Bangon	Bgy
Bubonga Guilopa	Bgy
Buadi-Abala	Bgy
Buadi-Bayawa	Bgy
Buadi-Insuba	Bgy
Bubong	Bgy
Cabasaran	Bgy
Cairatan	Bgy
Cormatan	Bgy
Poblacion	Bgy
Dalama	Bgy
Dansalan	Bgy
Dimarao	Bgy
Guilopa	Bgy
Ilian	Bgy
Kitambugun	Bgy
Lama	Bgy
Lilod	Bgy
Lilod Raybalai	Bgy
Lumbaca Ingud	Bgy
Lumbac	Bgy
Madaya	Bgy
Pindolonan	Bgy
Salipongan	Bgy
Sugan	Bgy
Bagoaingud	Bgy
Pagayawan	Mun
Ayong	Bgy
Bandara Ingud	Bgy
Bangon (Pob.)	Bgy
Biala-an	Bgy
Diampaca	Bgy
Guiarong	Bgy
Ilian	Bgy
Madang	Bgy
Mapantao	Bgy
Ngingir	Bgy
Padas	Bgy
Paigoay	Bgy
Pinalangca	Bgy
Poblacion	Bgy
Rangiran	Bgy
Rubokun	Bgy
Linindingan	Bgy
Kalaludan	Bgy
Piagapo	Mun
Aposong	Bgy
Bagoaingud	Bgy
Bangco (Pob.)	Bgy
Bansayan	Bgy
Basak	Bgy
Bobo	Bgy
Bubong Tawa-an	Bgy
Bubonga Mamaanun	Bgy
Bualan	Bgy
Bubong Ilian	Bgy
Gacap	Bgy
Katumbacan	Bgy
Ilian Poblacion	Bgy
Kalanganan	Bgy
Lininding	Bgy
Lumbaca Mamaan	Bgy
Mamaanun	Bgy
Mentring	Bgy
Olango	Bgy
Palacat	Bgy
Palao	Bgy
Paling	Bgy
Pantaon	Bgy
Pantar	Bgy
Paridi	Bgy
Pindolonan	Bgy
Radapan	Bgy
Radapan Poblacion	Bgy
Sapingit	Bgy
Talao	Bgy
Tambo	Bgy
Tapocan	Bgy
Taporug	Bgy
Tawaan	Bgy
Udalo	Bgy
Rantian	Bgy
Ilian	Bgy
Poona Bayabao	Mun
Ataragadong	Bgy
Bangon	Bgy
Bansayan	Bgy
Bubong-Dimunda	Bgy
Bugaran	Bgy
Bualan	Bgy
Cadayonan	Bgy
Calilangan Dicala	Bgy
Calupaan	Bgy
Dimayon	Bgy
Dilausan	Bgy
Dongcoan	Bgy
Gadongan	Bgy
Poblacion	Bgy
Liangan	Bgy
Lumbac	Bgy
Lumbaca Ingud	Bgy
Madanding	Bgy
Pantao	Bgy
Punud	Bgy
Ragayan	Bgy
Rogan Cairan	Bgy
Talaguian	Bgy
Rogan Tandiong Dimayon	Bgy
Taporog	Bgy
Pualas	Mun
Badak	Bgy
Bantayan	Bgy
Basagad	Bgy
Bolinsong	Bgy
Boring	Bgy
Bualan	Bgy
Danugan	Bgy
Dapao	Bgy
Diamla	Bgy
Gadongan	Bgy
Ingud	Bgy
Linuk	Bgy
Lumbac	Bgy
Maligo	Bgy
Masao	Bgy
Notong	Bgy
Porug	Bgy
Romagondong	Bgy
Tambo (Pob.)	Bgy
Tamlang	Bgy
Tuka	Bgy
Tomarompong	Bgy
Yaran	Bgy
Ditsaan-Ramain	Mun
Bagoaingud	Bgy
Barimbingan	Bgy
Bayabao	Bgy
Buadi Babai	Bgy
Buadi Alao	Bgy
Buadi Oloc	Bgy
Pagalongan Buadiadingan	Bgy
Dado	Bgy
Dangimprampiai	Bgy
Darimbang	Bgy
Dilausan	Bgy
Ditsaan	Bgy
Gadongan	Bgy
Pagalongan Ginaopan	Bgy
Baclayan Lilod	Bgy
Linamon	Bgy
Lumbatan Ramain	Bgy
Buayaan Madanding	Bgy
Maindig Ditsaan	Bgy
Mandara	Bgy
Maranao Timber	Bgy
Pagalongan Proper	Bgy
Polo	Bgy
Ramain Poblacion	Bgy
Ramain Proper	Bgy
Baclayan Raya	Bgy
Buayaan Raya	Bgy
Rantian	Bgy
Sundiga Bayabao	Bgy
Talub	Bgy
Buayaan Lilod	Bgy
Bubong Dangiprampiai	Bgy
Pagalongan Masioon	Bgy
Sultan Pangadapun	Bgy
Upper Pugaan	Bgy
Saguiaran	Mun
Alinun	Bgy
Bagoaingud	Bgy
Batangan	Bgy
Cadayon	Bgy
Cadingilan	Bgy
Lumbac Toros	Bgy
Comonal	Bgy
Dilausan	Bgy
Gadongan	Bgy
Linao	Bgy
Limogao	Bgy
Lumbayanague	Bgy
Basak Maito	Bgy
Maliwanag	Bgy
Mapantao	Bgy
Mipaga	Bgy
Natangcopan	Bgy
Pagalamatan	Bgy
Pamacotan	Bgy
Panggao	Bgy
Pantao Raya	Bgy
Pantaon	Bgy
Patpangkat	Bgy
Pawak	Bgy
Dilimbayan	Bgy
Pindolonan	Bgy
Poblacion	Bgy
Salocad	Bgy
Sungcod	Bgy
Bubong	Bgy
Tamparan	Mun
Bocalan	Bgy
Bangon	Bgy
Cabasaran	Bgy
Dilausan	Bgy
Lalabuan	Bgy
Lilod Tamparan	Bgy
Lindongan	Bgy
Linuk	Bgy
Occidental Linuk	Bgy
Linuk Oriental	Bgy
Lumbaca Ingud	Bgy
Lumbacaingud South	Bgy
Lumbaca Lilod	Bgy
Balutmadiar	Bgy
Mala-abangon	Bgy
Maliwanag	Bgy
Maidan Linuk	Bgy
Miondas	Bgy
New Lumbacaingud	Bgy
Pimbago-Pagalongan	Bgy
Pagayawan	Bgy
Picarabawan	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Raya Niondas	Bgy
Raya Buadi Barao	Bgy
Raya Tamparan	Bgy
Salongabanding	Bgy
Saminunggay	Bgy
Talub	Bgy
Tatayawan North	Bgy
Tatayawan South	Bgy
Tubok	Bgy
Beruar	Bgy
Dasomalong	Bgy
Ginaopan	Bgy
Lumbac	Bgy
Minanga	Bgy
Lilod Tubok	Bgy
Mariatao Datu	Bgy
Pagalamatan Linuk	Bgy
Pindolonan Mariatao Sarip	Bgy
Taraka	Mun
Bandera Buisan	Bgy
Boriongan	Bgy
Borowa	Bgy
Buadi Dingun	Bgy
Buadi Amao	Bgy
Buadi Amunta	Bgy
Buadi Arorao	Bgy
Buadi Atopa	Bgy
Buadi Dayomangga	Bgy
Buadi Ongcalo	Bgy
Bucalan	Bgy
Cadayonan Bagumbayan	Bgy
Caramat	Bgy
Carandangan Calopaan	Bgy
Datu Ma-as	Bgy
Dilabayan	Bgy
Dimayon	Bgy
Gapao Balindong	Bgy
Ilian	Bgy
Lumasa	Bgy
Lumbac Bagoaingud	Bgy
Lumbac Bubong Maindang	Bgy
Lumbac Pitakus	Bgy
Malungun	Bgy
Maruhom	Bgy
Masolun	Bgy
Moriatao Loksa Datu	Bgy
Pagalamatan	Bgy
Pindolonan	Bgy
Pitakus	Bgy
Ririk	Bgy
Salipongan	Bgy
Lumasa Proper	Bgy
Sambolawan	Bgy
Samporna Salamatollah	Bgy
Somiorang Bandingun	Bgy
Sigayan Proper	Bgy
Sunggod	Bgy
Sunding	Bgy
Supangan	Bgy
Tupa-an Buadiatupa	Bgy
Buadi Amunud	Bgy
Mangayao	Bgy
Tubaran	Mun
Alog	Bgy
Beta	Bgy
Poblacion	Bgy
Campo	Bgy
Datumanong	Bgy
Dinaigan	Bgy
Guiarong	Bgy
Mindamudag	Bgy
Paigoay-Pimbataan	Bgy
Polo	Bgy
Riantaran	Bgy
Tangcal	Bgy
Tubaran Proper	Bgy
Wago	Bgy
Bagiangun	Bgy
Gadongan	Bgy
Gaput	Bgy
Madaya	Bgy
Malaganding	Bgy
Metadicop	Bgy
Pagalamatan	Bgy
Tugaya	Mun
Bagoaingud	Bgy
Bubong	Bgy
Buadi Alawang	Bgy
Buadi Dico	Bgy
Campong Talao	Bgy
Cayagan	Bgy
Dandanun	Bgy
Dilimbayan	Bgy
Gurain	Bgy
Poblacion	Bgy
Lumbac	Bgy
Maidan	Bgy
Mapantao	Bgy
Pagalamatan	Bgy
Pandiaranao	Bgy
Pindolonan I	Bgy
Pindolonan II	Bgy
Putad	Bgy
Raya	Bgy
Sugod I	Bgy
Sugod Mawatan	Bgy
Sumbaga Rogong	Bgy
Tangcal	Bgy
Wao	Mun
Amoyong	Bgy
Balatin	Bgy
Banga	Bgy
Buntongun	Bgy
Bo-ot	Bgy
Cebuano Group	Bgy
Christian Village	Bgy
Eastern Wao	Bgy
Extension Poblacion	Bgy
Gata	Bgy
Kabatangan	Bgy
Kadingilan	Bgy
Katutungan (Pob.)	Bgy
Kilikili East	Bgy
Kilikili West	Bgy
Malaigang	Bgy
Manila Group	Bgy
Milaya	Bgy
Mimbuaya	Bgy
Muslim Village	Bgy
Pagalongan	Bgy
Panang	Bgy
Park Area (Pob.)	Bgy
Pilintangan	Bgy
Serran Village	Bgy
Western Wao (Pob.)	Bgy
Marogong	Mun
Balut	Bgy
Bagumbayan	Bgy
Bitayan	Bgy
Bolawan	Bgy
Bonga	Bgy
Cabasaran	Bgy
Cahera	Bgy
Cairantang	Bgy
Canibongan	Bgy
Diragun	Bgy
Mantailoco	Bgy
Mapantao	Bgy
Marogong East	Bgy
Marogong Proper (Pob.)	Bgy
Mayaman	Bgy
Pabrica	Bgy
Paigoay Coda	Bgy
Pasayanan	Bgy
Piangologan	Bgy
Puracan	Bgy
Romagondong	Bgy
Sarang	Bgy
Cadayonan	Bgy
Calumbog	Bgy
Calanogas	Mun
Bubonga Ranao	Bgy
Calalaoan (Pob.)	Bgy
Gas	Bgy
Inudaran	Bgy
Inoma	Bgy
Luguna	Bgy
Mimbalawag	Bgy
Ngingir	Bgy
Pagalongan	Bgy
Panggawalupa	Bgy
Pantaon	Bgy
Piksan	Bgy
Pindolonan	Bgy
Punud	Bgy
Tagoranao	Bgy
Taliboboka	Bgy
Tambac	Bgy
Buadiposo-Buntong	Mun
Bacolod	Bgy
Bangon	Bgy
Buadiposo Lilod	Bgy
Buadiposo Proper	Bgy
Bubong	Bgy
Buntong Proper	Bgy
Cadayonan	Bgy
Dansalan	Bgy
Gata	Bgy
Kalakala	Bgy
Katogonan	Bgy
Lumbac	Bgy
Lumbatan Manacab	Bgy
Lumbatan Pataingud	Bgy
Manacab (Pob.)	Bgy
Minanga	Bgy
Paling	Bgy
Pindolonan	Bgy
Pualas	Bgy
Buadiposo Raya	Bgy
Sapot	Bgy
Tangcal	Bgy
Tarik	Bgy
Tuka	Bgy
Bangon Proper	Bgy
Raya Buntong	Bgy
Lunduban	Bgy
Ragondingan East	Bgy
Ragondingan Proper	Bgy
Ragondingan West	Bgy
Boto Ragondingan	Bgy
Datu Tambara	Bgy
Dirisan	Bgy
Maguing	Mun
Agagan	Bgy
Balagunun	Bgy
Bolao	Bgy
Balawag	Bgy
Balintao	Bgy
Borocot	Bgy
Bato-bato	Bgy
Borrowa	Bgy
Buadiangkay	Bgy
Bubong Bayabao	Bgy
Botud	Bgy
Camalig	Bgy
Cambong	Bgy
Dilausan (Pob.)	Bgy
Dilimbayan	Bgy
Ilalag	Bgy
Kianodan	Bgy
Lumbac	Bgy
Madanding	Bgy
Madaya	Bgy
Maguing Proper	Bgy
Malungun	Bgy
Pagalongan	Bgy
Panayangan	Bgy
Pilimoknan	Bgy
Ragayan	Bgy
Lilod Maguing	Bgy
Bubong	Bgy
Lilod Borocot	Bgy
Malungun Borocot	Bgy
Malungun Pagalongan	Bgy
Sabala Dilausan	Bgy
Lumbac-Dimarao	Bgy
Pindolonan	Bgy
Picong	Mun
Bara-as	Bgy
Biasong	Bgy
Bulangos	Bgy
Durian	Bgy
Ilian	Bgy
Liangan (Pob.)	Bgy
Maganding	Bgy
Maladi	Bgy
Mapantao	Bgy
Micalubo	Bgy
Pindolonan	Bgy
Punong	Bgy
Ramitan	Bgy
Torogan	Bgy
Tual	Bgy
Tuca	Bgy
Ubanoban	Bgy
Anas	Bgy
Mimbalawag	Bgy
Lumbayanague	Mun
Bagoaingud	Bgy
Balaigay	Bgy
Bualan	Bgy
Cadingilan	Bgy
Casalayan	Bgy
Dala	Bgy
Dilimbayan	Bgy
Cabuntungan	Bgy
Lamin	Bgy
Diromoyod	Bgy
Kabasaran (Pob.)	Bgy
Nanagun	Bgy
Mapantao-Balangas	Bgy
Miniros	Bgy
Pantaon	Bgy
Pindolonan	Bgy
Pitatanglan	Bgy
Poctan	Bgy
Singcara	Bgy
Wago	Bgy
Cadayonan	Bgy
Cadingilan A	Bgy
Amai Manabilang	Mun
Poblacion	Bgy
Bagumbayan	Bgy
Bandara-Ingud	Bgy
Comara	Bgy
Frankfort	Bgy
Lambanogan	Bgy
Lico	Bgy
Mansilano	Bgy
Natangcopan	Bgy
Pagonayan	Bgy
Pagalamatan	Bgy
Piagma	Bgy
Punud	Bgy
Ranao-Baning	Bgy
Salam	Bgy
Sagua-an	Bgy
Sumugot	Bgy
Tagoloan Ii	Mun
Bantalan	Bgy
Bayog	Bgy
Cadayonan	Bgy
Dagonalan	Bgy
Dimalama	Bgy
Gayakay	Bgy
Inodaran	Bgy
Kalilangan	Bgy
Kianibong	Bgy
Kingan	Bgy
Kitaon	Bgy
Bagoaingud	Bgy
Malinao	Bgy
Malingon	Bgy
Mama-an Pagalongan	Bgy
Marawi	Bgy
Maimbaguiang	Bgy
Sigayan	Bgy
Tagoloan Poblacion	Bgy
Kapatagan	Mun
Bakikis	Bgy
Barao	Bgy
Bongabong	Bgy
Daguan	Bgy
Inudaran	Bgy
Kabaniakawan	Bgy
Kapatagan	Bgy
Lusain	Bgy
Matimos	Bgy
Minimao	Bgy
Pinantao	Bgy
Salaman	Bgy
Sigayan	Bgy
Tabuan	Bgy
Upper Igabay	Bgy
Sultan Dumalondong	Mun
Bacayawan	Bgy
Buta	Bgy
Dinganun Guilopa	Bgy
Lumbac	Bgy
Malalis	Bgy
Pagalongan	Bgy
Tagoranao	Bgy
Lumbaca-Unayan	Mun
Bangon	Bgy
Beta	Bgy
Calalon	Bgy
Calipapa	Bgy
Dilausan	Bgy
Dimapaok	Bgy
Lumbac Dilausan	Bgy
Oriental Beta	Bgy
Tringun	Bgy
Maguindanao	Prov
Ampatuan	Mun
Dicalongan (Pob.)	Bgy
Kakal	Bgy
Kamasi	Bgy
Kapinpilan	Bgy
Kauran	Bgy
Malatimon	Bgy
Matagabong	Bgy
Saniag	Bgy
Tomicor	Bgy
Tubak	Bgy
Salman	Bgy
Buldon	Mun
Ampuan	Bgy
Aratuc	Bgy
Cabayuan	Bgy
Calaan (Pob.)	Bgy
Karim	Bgy
Dinganen	Bgy
Edcor	Bgy
Kulimpang	Bgy
Mataya	Bgy
Minabay	Bgy
Nuyo	Bgy
Oring	Bgy
Pantawan	Bgy
Piers	Bgy
Rumidas	Bgy
Buluan	Mun
Digal	Bgy
Lower Siling	Bgy
Maslabeng	Bgy
Poblacion	Bgy
Popol	Bgy
Talitay	Bgy
Upper Siling	Bgy
Datu Paglas	Mun
Alip (Pob.)	Bgy
Damawato	Bgy
Katil	Bgy
Malala	Bgy
Mangadeg	Bgy
Manindolo	Bgy
Puya	Bgy
Sepaka	Bgy
Lomoyon	Bgy
Kalumenga	Bgy
Palao sa Buto	Bgy
Damalusay	Bgy
Bonawan	Bgy
Bulod	Bgy
Datang	Bgy
Elbebe	Bgy
Lipao	Bgy
Madidis	Bgy
Makat	Bgy
Mao	Bgy
Napok	Bgy
Poblacion	Bgy
Salendab	Bgy
Datu Piang	Mun
Alonganan	Bgy
Ambadao	Bgy
Balanakan	Bgy
Balong	Bgy
Buayan	Bgy
Dado	Bgy
Damabalas	Bgy
Duaminanga	Bgy
Kalipapa	Bgy
Liong	Bgy
Magaslong	Bgy
Masigay	Bgy
Montay	Bgy
Poblacion	Bgy
Reina Regente	Bgy
Kanguan	Bgy
Datu Odin Sinsuat	Mun
Ambolodto	Bgy
Awang	Bgy
Badak	Bgy
Bagoenged	Bgy
Baka	Bgy
Benolen	Bgy
Bitu	Bgy
Bongued	Bgy
Bugawas	Bgy
Capiton	Bgy
Dados	Bgy
Dalican Poblacion	Bgy
Dinaig Proper	Bgy
Dulangan	Bgy
Kakar	Bgy
Kenebeka	Bgy
Kurintem	Bgy
Kusiong	Bgy
Labungan	Bgy
Linek	Bgy
Makir	Bgy
Margues	Bgy
Nekitan	Bgy
Mompong	Bgy
Sapalan	Bgy
Semba	Bgy
Sibuto	Bgy
Sifaren	Bgy
Tambak	Bgy
Tamontaka	Bgy
Tanuel	Bgy
Tapian	Bgy
Taviran	Bgy
Tenonggos	Bgy
Shariff Aguak (Capital)	Mun
Bagong	Bgy
Bialong	Bgy
Kuloy	Bgy
Labu-labu	Bgy
Lapok	Bgy
Malingao	Bgy
Poblacion	Bgy
Satan	Bgy
Tapikan	Bgy
Timbangan	Bgy
Tina	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Matanog	Mun
Bayanga Norte	Bgy
Bayanga Sur	Bgy
Bugasan Norte	Bgy
Bugasan Sur (Pob.)	Bgy
Kidama	Bgy
Sapad	Bgy
Langco	Bgy
Langkong	Bgy
Pagalungan	Mun
Bagoenged	Bgy
Buliok	Bgy
Damalasak	Bgy
Galakit	Bgy
Inug-ug	Bgy
Kalbugan	Bgy
Kilangan	Bgy
Kudal	Bgy
Layog	Bgy
Linandangan	Bgy
Poblacion	Bgy
Dalgan	Bgy
Parang	Mun
Gadungan	Bgy
Gumagadong Calawag	Bgy
Guiday T. Biruar	Bgy
Landasan	Bgy
Limbayan	Bgy
Bongo Island	Bgy
Magsaysay	Bgy
Making	Bgy
Nituan	Bgy
Orandang	Bgy
Pinantao	Bgy
Poblacion	Bgy
Polloc	Bgy
Tagudtongan	Bgy
Tuca-Maror	Bgy
Manion	Bgy
Macasandag	Bgy
Cotongan	Bgy
Poblacion II	Bgy
Samberen	Bgy
Kabuan	Bgy
Campo Islam	Bgy
Datu Macarimbang Biruar	Bgy
Gadunganpedpandaran	Bgy
Moro Point	Bgy
Sultan Kudarat	Mun
Alamada	Bgy
Banatin	Bgy
Banubo	Bgy
Bulalo	Bgy
Bulibod	Bgy
Calsada	Bgy
Crossing Simuay	Bgy
Dalumangcob (Pob.)	Bgy
Darapanan	Bgy
Gang	Bgy
Inawan	Bgy
Kabuntalan	Bgy
Kakar	Bgy
Kapimpilan	Bgy
Katidtuan	Bgy
Katuli	Bgy
Ladia	Bgy
Limbo	Bgy
Maidapa	Bgy
Makaguiling	Bgy
Katamlangan	Bgy
Matengen	Bgy
Mulaug	Bgy
Nalinan	Bgy
Nekitan	Bgy
Olas	Bgy
Panatan	Bgy
Pigcalagan	Bgy
Pigkelegan	Bgy
Pinaring	Bgy
Pingping	Bgy
Raguisi	Bgy
Rebuken	Bgy
Salimbao	Bgy
Sambolawan	Bgy
Senditan	Bgy
Ungap	Bgy
Damaniog	Bgy
Nara	Bgy
Sultan Sa Barongis	Mun
Angkayamat	Bgy
Barurao	Bgy
Bulod	Bgy
Darampua	Bgy
Gadungan	Bgy
Kulambog	Bgy
Langgapanan	Bgy
Masulot	Bgy
Papakan	Bgy
Tugal	Bgy
Tukanakuden	Bgy
Paldong	Bgy
Kabuntalan	Mun
Bagumbayan	Bgy
Dadtumog	Bgy
Gambar	Bgy
Ganta	Bgy
Katidtuan	Bgy
Langeban	Bgy
Liong	Bgy
Maitong	Bgy
Matilak	Bgy
Pagalungan	Bgy
Payan	Bgy
Pened	Bgy
Pedtad	Bgy
Poblacion	Bgy
Upper Taviran	Bgy
Buterin	Bgy
Lower Taviran	Bgy
Upi	Mun
Borongotan	Bgy
Bayabas	Bgy
Blensong	Bgy
Bugabungan	Bgy
Bungcog	Bgy
Darugao	Bgy
Ganasi	Bgy
Kabakaba	Bgy
Kibleg	Bgy
Kibucay	Bgy
Kiga	Bgy
Kinitan	Bgy
Mirab	Bgy
Nangi	Bgy
Nuro Poblacion	Bgy
Bantek	Bgy
Ranao Pilayan	Bgy
Rempes	Bgy
Renede	Bgy
Renti	Bgy
Rifao	Bgy
Sefegefen	Bgy
Tinungkaan	Bgy
Talayan	Mun
Boboguiron	Bgy
Damablac	Bgy
Fugotan	Bgy
Fukol	Bgy
Katibpuan	Bgy
Kedati	Bgy
Lanting	Bgy
Linamunan	Bgy
Marader	Bgy
Binangga North	Bgy
Binangga South	Bgy
Talayan	Bgy
Tamar	Bgy
Tambunan I	Bgy
Timbaluan	Bgy
South Upi	Mun
Kuya	Bgy
Biarong	Bgy
Bongo	Bgy
Itaw	Bgy
Kigan	Bgy
Lamud	Bgy
Looy	Bgy
Pandan	Bgy
Pilar	Bgy
Romangaob (Pob.)	Bgy
San Jose	Bgy
Barira	Mun
Barira (Pob.)	Bgy
Bualan	Bgy
Gadung	Bgy
Liong	Bgy
Lipa	Bgy
Lipawan	Bgy
Marang	Bgy
Nabalawag	Bgy
Rominimbang	Bgy
Togaig	Bgy
Minabay	Bgy
Korosoyan	Bgy
Lamin	Bgy
Panggao	Bgy
Gen. S.K. Pendatun	Mun
Badak	Bgy
Bulod	Bgy
Kaladturan	Bgy
Kulasi	Bgy
Lao-lao	Bgy
Lasangan	Bgy
Lower Idtig	Bgy
Lumabao	Bgy
Makainis	Bgy
Midconding	Bgy
Midpandacan	Bgy
Panosolen	Bgy
Ramcor	Bgy
Tonggol	Bgy
Pidtiguian	Bgy
Quipolot	Bgy
Sadangen	Bgy
Sumakubay	Bgy
Upper Lasangan	Bgy
Mamasapano	Mun
Bagumbong	Bgy
Dabenayan	Bgy
Daladap	Bgy
Dasikil	Bgy
Liab	Bgy
Libutan	Bgy
Lusay	Bgy
Mamasapano	Bgy
Manongkaling	Bgy
Pidsandawan	Bgy
Pimbalakan	Bgy
Sapakan	Bgy
Tuka	Bgy
Tukanalipao	Bgy
Talitay	Mun
Bintan	Bgy
Gadungan	Bgy
Kiladap	Bgy
Kilalan	Bgy
Kuden	Bgy
Makadayon	Bgy
Manggay	Bgy
Pageda	Bgy
Talitay	Bgy
Pagagawan	Mun
Balatungkayo	Bgy
Bulit	Bgy
Bulod	Bgy
Dungguan	Bgy
Limbalud	Bgy
Maridagao	Bgy
Nabundas	Bgy
Pagagawan	Bgy
Talapas	Bgy
Talitay	Bgy
Tunggol	Bgy
Paglat	Mun
Damakling	Bgy
Damalusay	Bgy
Paglat	Bgy
Upper Idtig	Bgy
Campo	Bgy
Kakal	Bgy
Salam	Bgy
Tual	Bgy
Sultan Mastura	Mun
Balut	Bgy
Boliok	Bgy
Bungabong	Bgy
Dagurongan	Bgy
Kirkir	Bgy
Macabico	Bgy
Namuken	Bgy
Simuay/Seashore	Bgy
Solon	Bgy
Tambo	Bgy
Tapayan	Bgy
Tariken	Bgy
Tuka	Bgy
Guindulungan	Mun
Ahan	Bgy
Bagan	Bgy
Datalpandan	Bgy
Kalumamis	Bgy
Kateman	Bgy
Lambayao	Bgy
Macasampen	Bgy
Muslim	Bgy
Muti	Bgy
Sampao	Bgy
Tambunan II	Bgy
Datu Saudi-Ampatuan	Mun
Dapiawan	Bgy
Elian	Bgy
Gawang	Bgy
Kabengi	Bgy
Kitango	Bgy
Kitapok	Bgy
Madia	Bgy
Salbu	Bgy
Datu Unsay	Mun
Bulayan	Bgy
Iganagampong	Bgy
Macalag	Bgy
Maitumaig	Bgy
Malangog	Bgy
Meta	Bgy
Panangeti	Bgy
Tuntungan	Bgy
Datu Abdullah Sangki	Mun
Banaba	Bgy
Dimampao	Bgy
Guinibon	Bgy
Kaya-kaya	Bgy
Maganoy	Bgy
Mao	Bgy
Maranding	Bgy
Sugadol	Bgy
Talisawa	Bgy
Tukanolocong	Bgy
Rajah Buayan	Mun
Baital	Bgy
Bakat	Bgy
Dapantis	Bgy
Gaunan	Bgy
Malibpolok	Bgy
Mileb	Bgy
Panadtaban	Bgy
Pidsandawan	Bgy
Sampao	Bgy
Sapakan (Pob.)	Bgy
Tabungao	Bgy
Datu Blah T. Sinsuat	Mun
Kinimi	Bgy
Laguitan	Bgy
Lapaken	Bgy
Matuber	Bgy
Meti	Bgy
Nalkan	Bgy
Penansaran	Bgy
Resa	Bgy
Sedem	Bgy
Sinipak	Bgy
Tambak	Bgy
Tubuan	Bgy
Pura	Bgy
Datu Anggal Midtimbang	Mun
Adaon	Bgy
Brar	Bgy
Mapayag	Bgy
Midtimbang (Pob.)	Bgy
Nunangan	Bgy
Tugal	Bgy
Tulunan	Bgy
Mangudadatu	Mun
Daladagan	Bgy
Kalian	Bgy
Luayan	Bgy
Paitan	Bgy
Panapan	Bgy
Tenok	Bgy
Tinambulan	Bgy
Tumbao	Bgy
Pandag	Mun
Kabuling	Bgy
Kayaga	Bgy
Kayupo	Bgy
Lepak	Bgy
Lower Dilag	Bgy
Malangit	Bgy
Pandag	Bgy
Upper Dilag	Bgy
Northern Kabuntalan	Mun
Balong	Bgy
Damatog	Bgy
Gayonga	Bgy
Guiawa	Bgy
Indatuan	Bgy
Kapimpilan	Bgy
Libungan	Bgy
Montay	Bgy
Paulino Labio	Bgy
Sabaken	Bgy
Tumaguinting	Bgy
Datu Hoffer Ampatuan	Mun
Kubentong	Bgy
Labu-labu I	Bgy
Labu-labu II	Bgy
Limpongo	Bgy
Sayap	Bgy
Taib	Bgy
Talibadok	Bgy
Tuayan	Bgy
Tuayan I	Bgy
Macalag	Bgy
Tuntungan	Bgy
Datu Salibo	Mun
Alonganan	Bgy
Andavit	Bgy
Balanakan	Bgy
Buayan	Bgy
Butilen	Bgy
Dado	Bgy
Damabalas	Bgy
Duaminanga	Bgy
Kalipapa	Bgy
Liong	Bgy
Magaslong	Bgy
Masigay	Bgy
Pagatin	Bgy
Pandi	Bgy
Penditen	Bgy
Sambulawan	Bgy
Tee	Bgy
Shariff Saydona Mustapha	Mun
Bakat	Bgy
Dale-Bong	Bgy
Dasawao	Bgy
Datu Bakal	Bgy
Datu Kilay	Bgy
Duguengen	Bgy
Ganta	Bgy
Inaladan	Bgy
Linantangan	Bgy
Nabundas	Bgy
Pagatin	Bgy
Pamalian	Bgy
Pikeg	Bgy
Pusao	Bgy
Libutan	Bgy
Pagatin	Bgy
Sulu	Prov
Indanan	Mun
Adjid	Bgy
Bangalan	Bgy
Bato-bato	Bgy
Buanza	Bgy
Bud-Taran	Bgy
Bunut	Bgy
Jati-Tunggal	Bgy
Kabbon Maas	Bgy
Kagay	Bgy
Kajatian	Bgy
Kan Islam	Bgy
Kandang Tukay	Bgy
Karawan	Bgy
Katian	Bgy
Kuppong	Bgy
Lambayong	Bgy
Langpas	Bgy
Licup	Bgy
Malimbaya	Bgy
Manggis	Bgy
Manilop	Bgy
Paligue	Bgy
Panabuan	Bgy
Pasil	Bgy
Poblacion	Bgy
Sapah Malaum	Bgy
Panglima Misuari	Bgy
Sionogan	Bgy
Tagbak	Bgy
Timbangan	Bgy
Tubig Dakulah	Bgy
Tubig Parang	Bgy
Tumantangis	Bgy
Sawaki	Bgy
Jolo (Capital)	Mun
Alat	Bgy
Asturias	Bgy
Bus-bus	Bgy
Chinese Pier	Bgy
San Raymundo	Bgy
Takut-takut	Bgy
Tulay	Bgy
Walled City (Pob.)	Bgy
Kalingalan Caluang	Mun
Kambing	Bgy
Kanlagay	Bgy
Pang	Bgy
Pangdan Pangdan	Bgy
Pitogo	Bgy
Karungdong (Pob.)	Bgy
Tunggol	Bgy
Masjid Bayle	Bgy
Masjid Punjungan	Bgy
Luuk	Mun
Bual	Bgy
Kan-Bulak	Bgy
Kan-Mindus	Bgy
Lambago	Bgy
Lianutan	Bgy
Lingah	Bgy
Mananti	Bgy
Tubig-Puti (Pob.)	Bgy
Tandu-Bato	Bgy
Tulayan Island	Bgy
Guimbaun	Bgy
Niog-niog	Bgy
Maimbung	Mun
Anak Jati	Bgy
Bato Ugis	Bgy
Bualo Lahi	Bgy
Bualo Lipid	Bgy
Bulabog	Bgy
Ratag Limbon	Bgy
Duhol Kabbon	Bgy
Gulangan	Bgy
Ipil	Bgy
Kandang	Bgy
Kapok-Punggol	Bgy
Kulasi	Bgy
Labah	Bgy
Lagasan Asibih	Bgy
Lantong	Bgy
Laud Kulasi	Bgy
Laum Maimbung	Bgy
Lunggang	Bgy
Lower Tambaking	Bgy
Lapa	Bgy
Matatal	Bgy
Patao	Bgy
Poblacion	Bgy
Tabu-Bato	Bgy
Tandu Patong	Bgy
Tubig-Samin	Bgy
Upper Tambaking	Bgy
Hadji Panglima Tahil	Mun
Bubuan	Bgy
Kabuukan	Bgy
Pag-asinan	Bgy
Bangas (Pob.)	Bgy
Teomabal	Bgy
Old Panamao	Mun
Asin	Bgy
Bakud	Bgy
Bangday	Bgy
Baunoh	Bgy
Bitanag	Bgy
Bud Seit	Bgy
Bulangsi	Bgy
Datag	Bgy
Kamalig	Bgy
Kan Ukol	Bgy
Kan Asaali	Bgy
Kan-Dayok	Bgy
Kan-Sipat	Bgy
Kawasan	Bgy
Kulay-kulay	Bgy
Lakit	Bgy
Lunggang	Bgy
Parang	Bgy
Lower Patibulan	Bgy
Upper Patibulan	Bgy
Pugad Manaul	Bgy
Puhagan	Bgy
Seit Higad	Bgy
Seit Lake (Pob.)	Bgy
Su-uh	Bgy
Tabu Manuk	Bgy
Tandu-tandu	Bgy
Tayungan	Bgy
Tinah	Bgy
Tubig Gantang	Bgy
Tubig Jati	Bgy
Pangutaran	Mun
Alu Bunah	Bgy
Bangkilay	Bgy
Kawitan	Bgy
Kehi Niog	Bgy
Lantong Babag	Bgy
Lumah Dapdap	Bgy
Pandan Niog	Bgy
Panducan	Bgy
Panitikan	Bgy
Patutol	Bgy
Simbahan (Pob.)	Bgy
Se-ipang	Bgy
Suang Bunah	Bgy
Tonggasang	Bgy
Tubig Nonok	Bgy
Tubig Sallang	Bgy
Parang	Mun
Kaha	Bgy
Alu Pangkoh	Bgy
Bagsak	Bgy
Bawisan	Bgy
Biid	Bgy
Bukid	Bgy
Buli Bawang	Bgy
Buton	Bgy
Buton Mahablo	Bgy
Danapa	Bgy
Duyan Kabao	Bgy
Gimba Lagasan	Bgy
Kahoy Sinah	Bgy
Kanaway	Bgy
Kutah Sairap	Bgy
Lagasan Higad	Bgy
Laum Buwahan	Bgy
Laum Suwah	Bgy
Alu Layag-Layag	Bgy
Liang	Bgy
Linuho	Bgy
Lipunos	Bgy
Lungan Gitong	Bgy
Lumbaan Mahaba	Bgy
Lupa Abu	Bgy
Nonokan	Bgy
Paugan	Bgy
Payuhan	Bgy
Piyahan	Bgy
Poblacion	Bgy
Saldang	Bgy
Sampunay	Bgy
Silangkan	Bgy
Taingting	Bgy
Tikong	Bgy
Tukay	Bgy
Tumangas	Bgy
Wanni Piyanjihan	Bgy
Lanao Dakula	Bgy
Lower Sampunay	Bgy
Pata	Mun
Andalan	Bgy
Daungdong	Bgy
Kamawi	Bgy
Kanjarang	Bgy
Kayawan (Pob.)	Bgy
Kiput	Bgy
Likud	Bgy
Luuk-tulay	Bgy
Niog-niog	Bgy
Patian	Bgy
Pisak-pisak	Bgy
Saimbangon	Bgy
Sangkap	Bgy
Timuddas	Bgy
Patikul	Mun
Anuling	Bgy
Bakong	Bgy
Bangkal	Bgy
Bonbon	Bgy
Buhanginan	Bgy
Bungkaung	Bgy
Danag	Bgy
Igasan	Bgy
Kabbon Takas	Bgy
Kadday Mampallam	Bgy
Kan Ague	Bgy
Kaunayan	Bgy
Langhub	Bgy
Latih	Bgy
Liang	Bgy
Maligay	Bgy
Mauboh	Bgy
Pangdanon	Bgy
Panglayahan	Bgy
Pansul	Bgy
Patikul Higad	Bgy
Sandah	Bgy
Taglibi (Pob.)	Bgy
Tandu-Bagua	Bgy
Tanum	Bgy
Taung	Bgy
Timpok	Bgy
Tugas	Bgy
Umangay	Bgy
Gandasuli	Bgy
Siasi	Mun
Bakud	Bgy
Buan	Bgy
Bulansing Tara	Bgy
Bulihkullul	Bgy
Campo Islam	Bgy
Poblacion	Bgy
Duggo	Bgy
Duhol Tara	Bgy
East Kungtad	Bgy
East Sisangat	Bgy
Ipil	Bgy
Jambangan	Bgy
Kabubu	Bgy
Kong-Kong Laminusa	Bgy
Kud-kud	Bgy
Kungtad West	Bgy
Luuk Laminusa	Bgy
Latung	Bgy
Luuk Tara	Bgy
Manta	Bgy
Minapan	Bgy
Nipa-nipa	Bgy
North Laud	Bgy
North Manta	Bgy
North Musu Laud	Bgy
North Silumpak	Bgy
Punungan	Bgy
Pislong	Bgy
Ratag	Bgy
Sablay	Bgy
Sarukot	Bgy
Siburi	Bgy
Singko	Bgy
Siolakan	Bgy
Siundoh	Bgy
Siowing	Bgy
Sipanding	Bgy
Sisangat	Bgy
South Musu Laud	Bgy
South Silumpak	Bgy
Southwestern Bulikullul	Bgy
Subah Buaya	Bgy
Tampakan Laminusa	Bgy
Tengah Laminusa	Bgy
Tong Laminusa	Bgy
Tong-tong	Bgy
Tonglabah	Bgy
Tubig Kutah	Bgy
Tulling	Bgy
Puukan Laminusa	Bgy
Talipao	Mun
Andalan	Bgy
Bagsak	Bgy
Bandang	Bgy
Bilaan (Pob.)	Bgy
Bud Bunga	Bgy
Buntod	Bgy
Buroh	Bgy
Dalih	Bgy
Gata	Bgy
Kabatuhan Tiis	Bgy
Kabungkol	Bgy
Kagay	Bgy
Kahawa	Bgy
Kandaga	Bgy
Kanlibot	Bgy
Kiutaan	Bgy
Kuhaw	Bgy
Kulamboh	Bgy
Kuttong	Bgy
Lagtoh	Bgy
Lambanah	Bgy
Liban	Bgy
Liu-Bud Pantao	Bgy
Lower Binuang	Bgy
Lower Kamuntayan	Bgy
Lower Laus	Bgy
Lower Sinumaan	Bgy
Lower Talipao	Bgy
Lumbayao	Bgy
Lumping Pigih Daho	Bgy
Lungkiaban	Bgy
Mabahay	Bgy
Mahala	Bgy
Mampallam	Bgy
Marsada	Bgy
Mauboh	Bgy
Mungit-mungit	Bgy
Niog-Sangahan	Bgy
Pantao	Bgy
Samak	Bgy
Talipao Proper	Bgy
Tampakan	Bgy
Tiis	Bgy
Tinggah	Bgy
Tubod	Bgy
Tuyang	Bgy
Upper Kamuntayan	Bgy
Upper Laus	Bgy
Upper Sinumaan	Bgy
Upper Talipao	Bgy
Kabatuhan Bilaan	Bgy
Upper Binuang	Bgy
Tapul	Mun
Banting	Bgy
Hawan	Bgy
Alu-Kabingaan	Bgy
Kalang (Pob.)	Bgy
Kamaunggi	Bgy
Kanmangon	Bgy
Kanaway	Bgy
Kaumpang	Bgy
Pagatpat	Bgy
Pangdan	Bgy
Puok	Bgy
Sayli	Bgy
Sumambat	Bgy
Tangkapaan	Bgy
Tulakan	Bgy
Tongkil	Mun
Bakkaan	Bgy
Bangalaw	Bgy
Danao	Bgy
Dungon	Bgy
Kahikukuk	Bgy
Luuk (Pob.)	Bgy
North Paarol	Bgy
Sigumbal	Bgy
South Paarol	Bgy
Tabialan	Bgy
Tainga-Bakkao	Bgy
Tambun-bun	Bgy
Tinutungan	Bgy
Tattalan	Bgy
Panglima Estino	Mun
Gagguil	Bgy
Gata-gata	Bgy
Kamih-Pungud	Bgy
Lihbug Kabaw	Bgy
Lubuk-lubuk	Bgy
Pandakan	Bgy
Punay (Pob.)	Bgy
Tiptipon	Bgy
Jinggan	Bgy
Likbah	Bgy
Marsada	Bgy
Paiksa	Bgy
Lugus	Mun
Alu Bus-Bus	Bgy
Alu-Duyong	Bgy
Bas Lugus	Bgy
Gapas Rugasan	Bgy
Gapas Tubig Tuwak	Bgy
Huwit-huwit Proper	Bgy
Huwit-huwit Bas Nonok	Bgy
Kutah Parang	Bgy
Laha	Bgy
Larap	Bgy
Lugus Proper	Bgy
Mangkallay	Bgy
Mantan	Bgy
Pait	Bgy
Parian Kayawan	Bgy
Sibul	Bgy
Tingkangan	Bgy
Pandami	Mun
Baligtang	Bgy
Bud Sibaud	Bgy
Hambilan	Bgy
Kabbon	Bgy
Lahi	Bgy
Lapak	Bgy
Malanta	Bgy
Mamanok	Bgy
North Manubul	Bgy
Parian Dakula	Bgy
Sibaud Proper	Bgy
Siganggang	Bgy
South Manubul	Bgy
Suba-suba	Bgy
Tenga Manubul	Bgy
Laud Sibaud	Bgy
Omar	Mun
Andalan	Bgy
Angilan	Bgy
Capual Island	Bgy
Huwit-huwit	Bgy
Lahing-Lahing	Bgy
Niangkaan	Bgy
Sucuban	Bgy
Tangkuan	Bgy
Tawi-Tawi	Prov
Panglima Sugala	Mun
Balimbing Proper	Bgy
Batu-batu (Pob.)	Bgy
Buan	Bgy
Dungon	Bgy
Luuk Buntal	Bgy
Parangan	Bgy
Tabunan	Bgy
Tungbangkaw	Bgy
Bauno Garing	Bgy
Belatan Halu	Bgy
Karaha	Bgy
Kulape	Bgy
Liyaburan	Bgy
Magsaggaw	Bgy
Malacca	Bgy
Sumangday	Bgy
Tundon	Bgy
Bongao (Capital)	Mun
Ipil	Bgy
Kamagong	Bgy
Karungdong	Bgy
Lakit Lakit	Bgy
Lamion	Bgy
Lapid Lapid	Bgy
Lato Lato	Bgy
Luuk Pandan	Bgy
Luuk Tulay	Bgy
Malassa	Bgy
Mandulan	Bgy
Masantong	Bgy
Montay Montay	Bgy
Pababag	Bgy
Pagasinan	Bgy
Pahut	Bgy
Pakias	Bgy
Paniongan	Bgy
Pasiagan	Bgy
Bongao Poblacion	Bgy
Sanga-sanga	Bgy
Silubog	Bgy
Simandagit	Bgy
Sumangat	Bgy
Tarawakan	Bgy
Tongsinah	Bgy
Tubig Basag	Bgy
Ungus-ungus	Bgy
Lagasan	Bgy
Nalil	Bgy
Pagatpat	Bgy
Pag-asa	Bgy
Tubig Tanah	Bgy
Tubig-Boh	Bgy
Tubig-Mampallam	Bgy
Mapun	Mun
Boki	Bgy
Duhul Batu	Bgy
Kompang	Bgy
Lupa Pula (Pob.)	Bgy
Guppah	Bgy
Mahalo	Bgy
Pawan	Bgy
Sikub	Bgy
Tabulian	Bgy
Tanduan	Bgy
Umus Mataha	Bgy
Erok-erok	Bgy
Liyubud	Bgy
Lubbak Parang	Bgy
Sapa	Bgy
Simunul	Mun
Bakong	Bgy
Manuk Mangkaw	Bgy
Mongkay	Bgy
Tampakan (Pob.)	Bgy
Tonggosong	Bgy
Tubig Indangan	Bgy
Ubol	Bgy
Doh-Tong	Bgy
Luuk Datan	Bgy
Maruwa	Bgy
Pagasinan	Bgy
Panglima Mastul	Bgy
Sukah-Bulan	Bgy
Timundon	Bgy
Bagid*	Bgy
Sitangkai	Mun
South Larap	Bgy
Sitangkai Poblacion	Bgy
Tongmageng	Bgy
Tongusong	Bgy
Datu Baguinda Putih	Bgy
Imam Sapie	Bgy
North Larap	Bgy
Panglima Alari	Bgy
Sipangkot	Bgy
South Ubian	Mun
Babagan	Bgy
Bengkol	Bgy
Bintawlan	Bgy
Bohe	Bgy
Bubuan	Bgy
Bunay Bunay Tong	Bgy
Bunay Bunay Lookan	Bgy
Bunay Bunay Center	Bgy
Lahad Dampong	Bgy
East Talisay	Bgy
Nunuk	Bgy
Laitan	Bgy
Lambi-lambian	Bgy
Laud	Bgy
Likud Tabawan	Bgy
Nusa-nusa	Bgy
Nusa	Bgy
Pampang	Bgy
Putat	Bgy
Sollogan	Bgy
Talisay	Bgy
Tampakan Dampong	Bgy
Tinda-tindahan	Bgy
Tong Tampakan	Bgy
Tubig Dayang Center	Bgy
Tubig Dayang Riverside	Bgy
Tubig Dayang	Bgy
Tukkai	Bgy
Unas-unas	Bgy
Likud Dampong	Bgy
Tangngah	Bgy
Tandubas	Mun
Baliungan	Bgy
Kakoong	Bgy
Kepeng	Bgy
Lahay-lahay	Bgy
Naungan	Bgy
Sallangan	Bgy
Sapa	Bgy
Silantup	Bgy
Tapian	Bgy
Tongbangkaw	Bgy
Tangngah	Bgy
Ballak	Bgy
Butun	Bgy
Himbah	Bgy
Kalang-kalang	Bgy
Salamat	Bgy
Sibakloon	Bgy
Tandubato	Bgy
Tapian Sukah	Bgy
Taruk	Bgy
Turtle Islands	Mun
Taganak Poblacion	Bgy
Likud Bakkao	Bgy
Languyan	Mun
Bakong	Bgy
Bas-bas Proper	Bgy
Basnunuk	Bgy
Darussalam	Bgy
Languyan Proper (Pob.)	Bgy
Maraning	Bgy
Simalak	Bgy
Tuhog-tuhog	Bgy
Tumahubong	Bgy
Tumbagaan	Bgy
Parang Pantay	Bgy
Adnin	Bgy
Bakaw-bakaw	Bgy
BasLikud	Bgy
Jakarta	Bgy
Kalupag	Bgy
Kiniktal	Bgy
Marang-marang	Bgy
Sikullis	Bgy
Tubig Dakula	Bgy
Sapa-Sapa	Mun
Baldatal Islam	Bgy
Lookan Banaran	Bgy
Tonggusong Banaran	Bgy
Butun	Bgy
Dalo-dalo	Bgy
Palate Gadjaminah	Bgy
Kohec	Bgy
Latuan	Bgy
Lakit-lakit	Bgy
Tangngah	Bgy
Tabunan Likud Sikubong	Bgy
Malanta	Bgy
Mantabuan Tabunan	Bgy
Sapa-sapa (Pob.)	Bgy
Tapian Bohe North	Bgy
Look Natuh	Bgy
Lookan Latuan	Bgy
Nunuk Likud Sikubong	Bgy
Pamasan	Bgy
Sapaat	Bgy
Sukah-sukah	Bgy
Tapian Bohe South	Bgy
Tup-tup Banaran	Bgy
Sibutu	Mun
Ambulong Sapal	Bgy
Datu Amilhamja Jaafar	Bgy
Hadji Imam Bidin	Bgy
Hadji Mohtar Sulayman	Bgy
Hadji Taha	Bgy
Imam Hadji Mohammad	Bgy
Ligayan	Bgy
Nunukan	Bgy
Sheik Makdum	Bgy
Sibutu (Pob.)	Bgy
Talisay	Bgy
Tandu Banak	Bgy
Taungoh	Bgy
Tongehat	Bgy
Tongsibalo	Bgy
Ungus-ungus	Bgy
Region XIII (Caraga)	Reg
Agusan del Norte	Prov
Buenavista	Mun
Abilan	Bgy
Agong-ong	Bgy
Alubijid	Bgy
Guinabsan	Bgy
Macalang	Bgy
Malapong	Bgy
Malpoc	Bgy
Manapa	Bgy
Matabao	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Poblacion 6	Bgy
Poblacion 7	Bgy
Poblacion 8	Bgy
Poblacion 9	Bgy
Poblacion 10	Bgy
Rizal	Bgy
Sacol	Bgy
Sangay	Bgy
Talo-ao	Bgy
Lower Olave	Bgy
Simbalan	Bgy
City of Butuan (Capital)	City
Agao Pob.	Bgy
Agusan Pequeño	Bgy
Ambago	Bgy
Amparo	Bgy
Ampayon	Bgy
Anticala	Bgy
Antongalon	Bgy
Aupagan	Bgy
Baan KM 3	Bgy
Babag	Bgy
Bading Pob.	Bgy
Bancasi	Bgy
Banza	Bgy
Baobaoan	Bgy
Basag	Bgy
Bayanihan Pob.	Bgy
Bilay	Bgy
Bit-os	Bgy
Bitan-agan	Bgy
Bobon	Bgy
Bonbon	Bgy
Bugabus	Bgy
Buhangin Pob.	Bgy
Cabcabon	Bgy
Camayahan	Bgy
Baan Riverside Pob.	Bgy
Dankias	Bgy
Imadejas Pob.	Bgy
Diego Silang Pob.	Bgy
Doongan	Bgy
Dumalagan	Bgy
Golden Ribbon Pob.	Bgy
Dagohoy Pob.	Bgy
Jose Rizal Pob.	Bgy
Holy Redeemer Pob.	Bgy
Humabon Pob.	Bgy
Kinamlutan	Bgy
Lapu-lapu Pob.	Bgy
Lemon	Bgy
Leon Kilat Pob.	Bgy
Libertad	Bgy
Limaha Pob.	Bgy
Los Angeles	Bgy
Lumbocan	Bgy
Maguinda	Bgy
Mahay	Bgy
Mahogany Pob.	Bgy
Maibu	Bgy
Mandamo	Bgy
Manila de Bugabus	Bgy
Maon Pob.	Bgy
Masao	Bgy
Maug	Bgy
Port Poyohon Pob.	Bgy
New Society Village Pob.	Bgy
Ong Yiu Pob.	Bgy
Pianing	Bgy
Pinamanculan	Bgy
Rajah Soliman Pob.	Bgy
San Ignacio Pob.	Bgy
San Mateo	Bgy
San Vicente	Bgy
Sikatuna Pob.	Bgy
Silongan Pob.	Bgy
Sumilihon	Bgy
Tagabaca	Bgy
Taguibo	Bgy
Taligaman	Bgy
Tandang Sora Pob.	Bgy
Tiniwisan	Bgy
Tungao	Bgy
Urduja Pob.	Bgy
Villa Kananga	Bgy
Obrero Pob.	Bgy
Bugsukan	Bgy
De Oro	Bgy
Dulag	Bgy
Florida	Bgy
Nong-nong	Bgy
Pagatpatan	Bgy
Pangabugan	Bgy
Salvacion	Bgy
Santo Niño	Bgy
Sumile	Bgy
Don Francisco	Bgy
Pigdaulan	Bgy
City of Cabadbaran	City
Antonio Luna	Bgy
Bay-ang	Bgy
Bayabas	Bgy
Caasinan	Bgy
Cabinet	Bgy
Calamba	Bgy
Calibunan	Bgy
Comagascas	Bgy
Concepcion	Bgy
Del Pilar	Bgy
Katugasan	Bgy
Kauswagan	Bgy
La Union	Bgy
Mabini	Bgy
Poblacion 1	Bgy
Poblacion 10	Bgy
Poblacion 11	Bgy
Poblacion 12	Bgy
Poblacion 2	Bgy
Poblacion 3	Bgy
Poblacion 4	Bgy
Poblacion 5	Bgy
Poblacion 6	Bgy
Poblacion 7	Bgy
Poblacion 8	Bgy
Poblacion 9	Bgy
Puting Bato	Bgy
Sanghan	Bgy
Soriano	Bgy
Tolosa	Bgy
Mahaba	Bgy
Carmen	Mun
Cahayagan	Bgy
Gosoon	Bgy
Manoligao	Bgy
Poblacion	Bgy
Rojales	Bgy
San Agustin	Bgy
Tagcatong	Bgy
Vinapor	Bgy
Jabonga	Mun
Baleguian	Bgy
Bangonay	Bgy
A. Beltran	Bgy
Bunga	Bgy
Colorado	Bgy
Cuyago	Bgy
Libas	Bgy
Magdagooc	Bgy
Magsaysay	Bgy
Maraiging	Bgy
Poblacion	Bgy
San Jose	Bgy
San Pablo	Bgy
San Vicente	Bgy
Santo Niño	Bgy
Kitcharao	Mun
Bangayan	Bgy
Canaway	Bgy
Hinimbangan	Bgy
Jaliobong	Bgy
Mahayahay	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Roque	Bgy
Sangay	Bgy
Crossing	Bgy
Songkoy	Bgy
Las Nieves	Mun
Ambacon	Bgy
Bonifacio	Bgy
Consorcia	Bgy
Katipunan	Bgy
Lingayao	Bgy
Malicato	Bgy
Maningalao	Bgy
Marcos Calo	Bgy
Mat-i	Bgy
Pinana-an	Bgy
Poblacion	Bgy
San Isidro	Bgy
Tinucoran	Bgy
Balungagan	Bgy
Eduardo G. Montilla	Bgy
Durian	Bgy
Ibuan	Bgy
Rosario	Bgy
San Roque	Bgy
Casiklan	Bgy
Magallanes	Mun
Buhang	Bgy
Caloc-an	Bgy
Guiasan	Bgy
Poblacion	Bgy
Taod-oy	Bgy
Marcos	Bgy
Santo Niño	Bgy
Santo Rosario	Bgy
Nasipit	Mun
Aclan	Bgy
Amontay	Bgy
Ata-atahon	Bgy
Camagong	Bgy
Cubi-cubi	Bgy
Culit	Bgy
Jaguimitan	Bgy
Kinabjangan	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Punta	Bgy
Santa Ana	Bgy
Talisay	Bgy
Triangulo	Bgy
Santiago	Mun
Curva	Bgy
Jagupit	Bgy
La Paz	Bgy
Poblacion I	Bgy
San Isidro	Bgy
Tagbuyacan	Bgy
Estanislao Morgado	Bgy
Poblacion II	Bgy
Pangaylan-IP	Bgy
Tubay	Mun
Binuangan	Bgy
Cabayawa	Bgy
Doña Rosario	Bgy
La Fraternidad	Bgy
Lawigan	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
Santa Ana	Bgy
Tagmamarkay	Bgy
Tagpangahoy	Bgy
Tinigbasan	Bgy
Victory	Bgy
Doña Telesfora	Bgy
Remedios T. Romualdez	Mun
Poblacion I	Bgy
Balangbalang	Bgy
Basilisa	Bgy
Humilog	Bgy
Panaytayon	Bgy
San Antonio	Bgy
Tagbongabong	Bgy
Poblacion II	Bgy
Agusan del Sur	Prov
City of Bayugan	City
Calaitan	Bgy
Charito	Bgy
Fili	Bgy
Hamogaway	Bgy
Katipunan	Bgy
Mabuhay	Bgy
Marcelina	Bgy
Maygatasan	Bgy
Noli	Bgy
Osmeña	Bgy
Panaytay	Bgy
Poblacion	Bgy
Sagmone	Bgy
Saguma	Bgy
Salvacion	Bgy
San Isidro	Bgy
Santa Irene	Bgy
Taglatawan	Bgy
Verdu	Bgy
Wawa	Bgy
Berseba	Bgy
Bucac	Bgy
Cagbas	Bgy
Canayugan	Bgy
Claro Cortez	Bgy
Gamao	Bgy
Getsemane	Bgy
Grace Estate	Bgy
Magkiangkang	Bgy
Mahayag	Bgy
Montivesta	Bgy
Mt. Ararat	Bgy
Mt. Carmel	Bgy
Mt. Olive	Bgy
New Salem	Bgy
Pinagalaan	Bgy
San Agustin	Bgy
San Juan	Bgy
Santa Teresita	Bgy
Santo Niño	Bgy
Taglibas	Bgy
Tagubay	Bgy
Villa Undayon	Bgy
Bunawan	Mun
Bunawan Brook	Bgy
Consuelo	Bgy
Libertad	Bgy
Mambalili	Bgy
Poblacion	Bgy
San Andres	Bgy
San Marcos	Bgy
Imelda	Bgy
Nueva Era	Bgy
San Teodoro	Bgy
Esperanza	Mun
Anolingan	Bgy
Bakingking	Bgy
Bentahon	Bgy
Bunaguit	Bgy
Catmonon	Bgy
Concordia	Bgy
Dakutan	Bgy
Duangan	Bgy
Mac-Arthur	Bgy
Guadalupe	Bgy
Hawilian	Bgy
Labao	Bgy
Maasin	Bgy
Mahagcot	Bgy
Milagros	Bgy
Nato	Bgy
Oro	Bgy
Poblacion	Bgy
Remedios	Bgy
Salug	Bgy
San Toribio	Bgy
Santa Fe	Bgy
Segunda	Bgy
Tagabase	Bgy
Taganahaw	Bgy
Tagbalili	Bgy
Tahina	Bgy
Tandang Sora	Bgy
Agsabu	Bgy
Aguinaldo	Bgy
Balubo	Bgy
Cebulan	Bgy
Crossing Luna	Bgy
Cubo	Bgy
Guibonon	Bgy
Kalabuan	Bgy
Kinamaybay	Bgy
Langag	Bgy
Maliwanag	Bgy
New Gingoog	Bgy
Odiong	Bgy
Piglawigan	Bgy
San Isidro	Bgy
San Jose	Bgy
San Vicente	Bgy
Sinakungan	Bgy
Valentina	Bgy
La Paz	Mun
Bataan	Bgy
Comota	Bgy
Halapitan	Bgy
Langasian	Bgy
Osmeña, Sr.	Bgy
Poblacion	Bgy
Sagunto	Bgy
Villa Paz	Bgy
Angeles	Bgy
Kasapa II	Bgy
Lydia	Bgy
Panagangan	Bgy
Sabang Adgawan	Bgy
San Patricio	Bgy
Valentina	Bgy
Loreto	Mun
Binucayan	Bgy
Johnson	Bgy
Magaud	Bgy
Nueva Gracia	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Mariano	Bgy
San Vicente	Bgy
Santa Teresa	Bgy
Santo Tomas	Bgy
Violanta	Bgy
Waloe	Bgy
Kasapa	Bgy
Katipunan	Bgy
Kauswagan	Bgy
Santo Niño	Bgy
Sabud	Bgy
Prosperidad (Capital)	Mun
Aurora	Bgy
Awa	Bgy
Azpetia	Bgy
Poblacion	Bgy
La Caridad	Bgy
La Suerte	Bgy
La Union	Bgy
Las Navas	Bgy
Libertad	Bgy
Los Arcos	Bgy
Lucena	Bgy
Mabuhay	Bgy
Magsaysay	Bgy
Mapaga	Bgy
New Maug	Bgy
Napo	Bgy
Patin-ay	Bgy
Salimbogaon	Bgy
Salvacion	Bgy
San Joaquin	Bgy
San Jose	Bgy
San Lorenzo	Bgy
San Martin	Bgy
San Pedro	Bgy
San Rafael	Bgy
San Salvador	Bgy
San Vicente	Bgy
Santa Irene	Bgy
Santa Maria	Bgy
La Perian	Bgy
La Purisima	Bgy
San Roque	Bgy
Rosario	Mun
Bayugan 3	Bgy
Cabantao	Bgy
Cabawan	Bgy
Marfil	Bgy
Novele	Bgy
Poblacion	Bgy
Santa Cruz	Bgy
Tagbayagan	Bgy
Wasi-an	Bgy
Libuac	Bgy
Maligaya	Bgy
San Francisco	Mun
Alegria	Bgy
Bayugan 2	Bgy
Borbon	Bgy
Caimpugan	Bgy
Ebro	Bgy
Hubang	Bgy
Lapinigan	Bgy
Lucac	Bgy
Mate	Bgy
New Visayas	Bgy
Pasta	Bgy
Pisa-an	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Rizal	Bgy
San Isidro	Bgy
Santa Ana	Bgy
Tagapua	Bgy
Bitan-agan	Bgy
Buenasuerte	Bgy
Das-agan	Bgy
Karaus	Bgy
Ladgadan	Bgy
Ormaca	Bgy
San Luis	Mun
Anislagan	Bgy
Baylo	Bgy
Coalicion	Bgy
Culi	Bgy
Nuevo Trabajo	Bgy
Poblacion	Bgy
Santa Ines	Bgy
Balit	Bgy
Binicalan	Bgy
Cecilia	Bgy
Dimasalang	Bgy
Don Alejandro	Bgy
Don Pedro	Bgy
Doña Flavia	Bgy
Mahagsay	Bgy
Mahapag	Bgy
Mahayahay	Bgy
Muritula	Bgy
Policarpo	Bgy
San Isidro	Bgy
San Pedro	Bgy
Santa Rita	Bgy
Santiago	Bgy
Wegguam	Bgy
Doña Maxima	Bgy
Santa Josefa	Mun
Angas	Bgy
Aurora	Bgy
Awao	Bgy
Tapaz	Bgy
Patrocinio	Bgy
Poblacion	Bgy
San Jose	Bgy
Santa Isabel	Bgy
Sayon	Bgy
Concepcion	Bgy
Pag-asa	Bgy
Talacogon	Mun
BuenaGracia	Bgy
Causwagan	Bgy
Culi	Bgy
Del Monte	Bgy
Desamparados	Bgy
Labnig	Bgy
Sabang Gibung	Bgy
San Agustin (Pob.)	Bgy
San Isidro (Pob.)	Bgy
San Nicolas (Pob.)	Bgy
Zamora	Bgy
Zillovia	Bgy
La Flora	Bgy
Maharlika	Bgy
Marbon	Bgy
Batucan	Bgy
Trento	Mun
Basa	Bgy
Cuevas	Bgy
Kapatungan	Bgy
Langkila-an	Bgy
New Visayas	Bgy
Poblacion	Bgy
Pulang-lupa	Bgy
Salvacion	Bgy
San Ignacio	Bgy
San Isidro	Bgy
San Roque	Bgy
Santa Maria	Bgy
Tudela	Bgy
Cebolin	Bgy
Manat	Bgy
Pangyan	Bgy
Veruela	Mun
Binongan	Bgy
Del Monte	Bgy
Don Mateo	Bgy
La Fortuna	Bgy
Limot	Bgy
Magsaysay	Bgy
Masayan	Bgy
Poblacion	Bgy
Sampaguita	Bgy
San Gabriel	Bgy
Santa Emelia	Bgy
Sinobong	Bgy
Anitap	Bgy
Bacay II	Bgy
Caigangan	Bgy
Candiis	Bgy
Katipunan	Bgy
Santa Cruz	Bgy
Sawagan	Bgy
Sisimon	Bgy
Sibagat	Mun
Afga	Bgy
Anahawan	Bgy
Banagbanag	Bgy
Del Rosario	Bgy
El Rio	Bgy
Ilihan	Bgy
Kauswagan	Bgy
Kioya	Bgy
Magkalape	Bgy
Magsaysay	Bgy
Mahayahay	Bgy
New Tubigon	Bgy
Padiay	Bgy
Perez	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Maria	Bgy
Sinai	Bgy
Tabon-tabon	Bgy
Tag-uyango	Bgy
Villangit	Bgy
Kolambugan	Bgy
Surigao del Norte	Prov
Alegria	Mun
Poblacion	Bgy
Alipao	Bgy
Budlingin	Bgy
Camp Eduard	Bgy
Ombong	Bgy
Pongtud	Bgy
San Pedro	Bgy
Ferlda	Bgy
Julio Ouano (Pob.)	Bgy
San Juan	Bgy
Anahaw	Bgy
Gamuton	Bgy
Bacuag	Mun
Cabugao	Bgy
Cambuayon	Bgy
Campo	Bgy
Dugsangon	Bgy
Pautao	Bgy
Payapag	Bgy
Poblacion	Bgy
Pungtod	Bgy
Santo Rosario	Bgy
Burgos	Mun
Baybay	Bgy
Bitaug	Bgy
Poblacion 1	Bgy
Poblacion 2	Bgy
San Mateo	Bgy
Matin-ao	Bgy
Claver	Mun
Cabugo	Bgy
Cagdianao	Bgy
Daywan	Bgy
Hayanggabon	Bgy
Ladgaron (Pob.)	Bgy
Lapinigan	Bgy
Magallanes	Bgy
Panatao	Bgy
Tayaga	Bgy
Bagakay	Bgy
Sapa	Bgy
Taganito	Bgy
Urbiztondo	Bgy
Wangke	Bgy
Dapa	Mun
Bagakay	Bgy
Barangay 1 (Pob.)	Bgy
Barangay 13 (Pob.)	Bgy
Buenavista	Bgy
Cabawa	Bgy
Cambas-ac	Bgy
Consolacion	Bgy
Corregidor	Bgy
Dagohoy	Bgy
Don Paulino	Bgy
Jubang	Bgy
Montserrat	Bgy
Osmeña	Bgy
Barangay 10 (Pob.)	Bgy
Barangay 11 (Pob.)	Bgy
Barangay 12 (Pob.)	Bgy
Barangay 2 (Pob.)	Bgy
Barangay 3 (Pob.)	Bgy
Barangay 4 (Pob.)	Bgy
Barangay 5 (Pob.)	Bgy
Barangay 6 (Pob.)	Bgy
Barangay 7 (Pob.)	Bgy
Barangay 8 (Pob.)	Bgy
Barangay 9 (Pob.)	Bgy
San Carlos	Bgy
San Miguel	Bgy
Santa Fe	Bgy
Union	Bgy
Santa Felomina	Bgy
Del Carmen	Mun
Bagakay	Bgy
Antipolo	Bgy
Bitoon	Bgy
Cabugao	Bgy
Cancohoy	Bgy
Caub	Bgy
Del Carmen (Pob.)	Bgy
Domoyog	Bgy
Esperanza	Bgy
Jamoyaon	Bgy
Katipunan	Bgy
Lobogon	Bgy
Mabuhay	Bgy
Mahayahay	Bgy
Quezon	Bgy
San Fernando	Bgy
San Jose (Pob.)	Bgy
Sayak	Bgy
Tuboran	Bgy
Halian	Bgy
General Luna	Mun
Anajawan	Bgy
Cabitoonan	Bgy
Catangnan	Bgy
Consuelo	Bgy
Corazon	Bgy
Daku	Bgy
Poblacion I	Bgy
Poblacion II	Bgy
Poblacion III	Bgy
Poblacion IV	Bgy
Poblacion V	Bgy
La Januza	Bgy
Libertad	Bgy
Magsaysay	Bgy
Malinao	Bgy
Santa Cruz	Bgy
Santa Fe	Bgy
Suyangan	Bgy
Tawin-tawin	Bgy
Gigaquit	Mun
Alambique (Pob.)	Bgy
Anibongan	Bgy
Camam-onan	Bgy
Cam-boayon	Bgy
Ipil (Pob.)	Bgy
Lahi	Bgy
Mahanub	Bgy
Poniente	Bgy
San Antonio	Bgy
San Isidro	Bgy
Sico-sico	Bgy
Villaflor	Bgy
Villafranca	Bgy
Mainit	Mun
Binga	Bgy
Bobona-on	Bgy
Cantugas	Bgy
Dayano	Bgy
Mabini	Bgy
Magpayang	Bgy
Magsaysay (Pob.)	Bgy
Mansayao	Bgy
Marayag	Bgy
Matin-ao	Bgy
Paco	Bgy
Quezon (Pob.)	Bgy
Roxas	Bgy
San Francisco	Bgy
San Isidro	Bgy
San Jose	Bgy
Siana	Bgy
Silop	Bgy
Tagbuyawan	Bgy
Tapi-an	Bgy
Tolingon	Bgy
Malimono	Mun
Doro	Bgy
Bunyasan	Bgy
Cantapoy	Bgy
Cagtinae	Bgy
Cayawan	Bgy
Hanagdong	Bgy
Karihatag	Bgy
Masgad	Bgy
Pili	Bgy
San Isidro (Pob.)	Bgy
Tinago	Bgy
Cansayong	Bgy
Can-aga	Bgy
Villariza	Bgy
Pilar	Mun
Caridad	Bgy
Katipunan	Bgy
Maasin	Bgy
Mabini	Bgy
Mabuhay	Bgy
Salvacion	Bgy
San Roque	Bgy
Asinan (Pob.)	Bgy
Centro (Pob.)	Bgy
Pilaring (Pob.)	Bgy
Punta (Pob.)	Bgy
Consolacion	Bgy
Datu	Bgy
Dayaohay	Bgy
Jaboy	Bgy
Placer	Mun
Amoslog	Bgy
Anislagan	Bgy
Bad-as	Bgy
Boyongan	Bgy
Bugas-bugas	Bgy
Central (Pob.)	Bgy
Ellaperal	Bgy
Ipil (Pob.)	Bgy
Lakandula	Bgy
Mabini	Bgy
Macalaya	Bgy
Magsaysay (Pob.)	Bgy
Magupange	Bgy
Pananay-an	Bgy
Panhutongan	Bgy
San Isidro	Bgy
Santa Cruz	Bgy
Suyoc	Bgy
Tagbongabong	Bgy
Sani-sani	Bgy
San Benito	Mun
Bongdo	Bgy
Maribojoc	Bgy
Nuevo Campo	Bgy
San Juan	Bgy
Santa Cruz (Pob.)	Bgy
Talisay (Pob.)	Bgy
San Francisco	Mun
Amontay	Bgy
Balite	Bgy
Banbanon	Bgy
Diaz	Bgy
Honrado	Bgy
Jubgan	Bgy
Linongganan	Bgy
Macopa	Bgy
Magtangale	Bgy
Oslao	Bgy
Poblacion	Bgy
San Isidro	Mun
Buhing Calipay	Bgy
Del Carmen (Pob.)	Bgy
Del Pilar	Bgy
Macapagal	Bgy
Pacifico	Bgy
Pelaez	Bgy
Roxas	Bgy
San Miguel	Bgy
Santa Paz	Bgy
Santo Niño	Bgy
Tambacan	Bgy
Tigasao	Bgy
Santa Monica	Mun
Abad Santos	Bgy
Alegria	Bgy
T. Arlan (Pob.)	Bgy
Bailan	Bgy
Garcia	Bgy
Libertad	Bgy
Mabini	Bgy
Mabuhay (Pob.)	Bgy
Magsaysay	Bgy
Rizal	Bgy
Tangbo	Bgy
Sison	Mun
Biyabid	Bgy
Gacepan	Bgy
Ima	Bgy
Lower Patag	Bgy
Mabuhay	Bgy
Mayag	Bgy
Poblacion	Bgy
San Isidro	Bgy
San Pablo	Bgy
Tagbayani	Bgy
Tinogpahan	Bgy
Upper Patag	Bgy
Socorro	Mun
Del Pilar	Bgy
Helene	Bgy
Honrado	Bgy
Navarro (Pob.)	Bgy
Nueva Estrella	Bgy
Pamosaingan	Bgy
Rizal (Pob.)	Bgy
Salog	Bgy
San Roque	Bgy
Santa Cruz	Bgy
Sering	Bgy
Songkoy	Bgy
Sudlon	Bgy
Albino Taruc	Bgy
City of Surigao (Capital)	City
Alang-alang	Bgy
Alegria	Bgy
Anomar	Bgy
Aurora	Bgy
Serna	Bgy
Balibayon	Bgy
Baybay	Bgy
Bilabid	Bgy
Bitaugan	Bgy
Bonifacio	Bgy
Buenavista	Bgy
Cabongbongan	Bgy
Cagniog	Bgy
Cagutsan	Bgy
Cantiasay	Bgy
Capalayan	Bgy
Catadman	Bgy
Danao	Bgy
Danawan	Bgy
Day-asan	Bgy
Ipil	Bgy
Libuac	Bgy
Lipata	Bgy
Lisondra	Bgy
Luna	Bgy
Mabini	Bgy
Mabua	Bgy
Manyagao	Bgy
Mapawa	Bgy
Mat-i	Bgy
Nabago	Bgy
Nonoc	Bgy
Poctoy	Bgy
Punta Bilar	Bgy
Quezon	Bgy
Rizal	Bgy
Sabang	Bgy
San Isidro	Bgy
San Jose	Bgy
San Juan	Bgy
San Pedro	Bgy
San Roque	Bgy
Sidlakan	Bgy
Silop	Bgy
Sugbay	Bgy
Sukailang	Bgy
Taft (Pob.)	Bgy
Talisay	Bgy
Togbongon	Bgy
Trinidad	Bgy
Orok	Bgy
Washington (Pob.)	Bgy
Zaragoza	Bgy
Canlanipa	Bgy
Tagana-An	Mun
Aurora (Pob.)	Bgy
Azucena (Pob.)	Bgy
Banban	Bgy
Cawilan	Bgy
Fabio	Bgy
Himamaug	Bgy
Laurel	Bgy
Lower Libas	Bgy
Opong	Bgy
Sampaguita (Pob.)	Bgy
Talavera	Bgy
Union	Bgy
Upper Libas	Bgy
Patino	Bgy
Tubod	Mun
Capayahan	Bgy
Cawilan	Bgy
Del Rosario	Bgy
Marga	Bgy
Motorpool	Bgy
Poblacion	Bgy
San Isidro	Bgy
Timamana	Bgy
San Pablo	Bgy
Surigao del Sur	Prov
Barobo	Mun
Amaga	Bgy
Bahi	Bgy
Cabacungan	Bgy
Cambagang	Bgy
Causwagan	Bgy
Dapdap	Bgy
Dughan	Bgy
Gamut	Bgy
Javier	Bgy
Kinayan	Bgy
Mamis	Bgy
Poblacion	Bgy
Rizal	Bgy
San Jose	Bgy
San Vicente	Bgy
Sua	Bgy
Sudlon	Bgy
Unidad	Bgy
Wakat	Bgy
San Roque	Bgy
Tambis	Bgy
Bayabas	Mun
Amag	Bgy
Balete (Pob.)	Bgy
Cabugo	Bgy
Cagbaoto	Bgy
La Paz	Bgy
Magobawok	Bgy
Panaosawon	Bgy
City of Bislig	City
Bucto	Bgy
Burboanan	Bgy
San Roque	Bgy
Caguyao	Bgy
Coleto	Bgy
Labisma	Bgy
Lawigan	Bgy
Mangagoy	Bgy
Mone	Bgy
Pamaypayan	Bgy
Poblacion	Bgy
San Antonio	Bgy
San Fernando	Bgy
San Isidro	Bgy
San Jose	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Sibaroy	Bgy
Tabon	Bgy
Tumanan	Bgy
Pamanlinan	Bgy
Kahayag	Bgy
Maharlika	Bgy
Comawas	Bgy
Cagwait	Mun
Aras-Asan	Bgy
Bacolod	Bgy
Bitaugan East	Bgy
Bitaugan West	Bgy
Tawagan	Bgy
Lactudan	Bgy
Mat-e	Bgy
La Purisima	Bgy
Poblacion	Bgy
Unidad	Bgy
Tubo-tubo	Bgy
Cantilan	Mun
Bugsukan	Bgy
Buntalid	Bgy
Cabangahan	Bgy
Cabas-an	Bgy
Calagdaan	Bgy
Consuelo	Bgy
General Island	Bgy
Lininti-an (Pob.)	Bgy
Magasang	Bgy
Magosilom (Pob.)	Bgy
Pag-Antayan	Bgy
Palasao	Bgy
Parang	Bgy
Tapi	Bgy
Tigabong	Bgy
Lobo	Bgy
San Pedro	Bgy
Carmen	Mun
Antao	Bgy
Cancavan	Bgy
Carmen (Pob.)	Bgy
Esperanza	Bgy
Puyat	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Hinapoyan	Bgy
Carrascal	Mun
Adlay	Bgy
Babuyan	Bgy
Bacolod	Bgy
Baybay (Pob.)	Bgy
Bon-ot	Bgy
Caglayag	Bgy
Dahican	Bgy
Doyos (Pob.)	Bgy
Embarcadero (Pob.)	Bgy
Gamuton	Bgy
Panikian	Bgy
Pantukan	Bgy
Saca (Pob.)	Bgy
Tag-Anito	Bgy
Cortes	Mun
Balibadon	Bgy
Burgos	Bgy
Capandan	Bgy
Mabahin	Bgy
Madrelino	Bgy
Manlico	Bgy
Matho	Bgy
Poblacion	Bgy
Tag-Anongan	Bgy
Tigao	Bgy
Tuboran	Bgy
Uba	Bgy
Hinatuan	Mun
Baculin	Bgy
Bigaan	Bgy
Cambatong	Bgy
Campa	Bgy
Dugmanon	Bgy
Harip	Bgy
La Casa (Pob.)	Bgy
Loyola	Bgy
Maligaya	Bgy
Pagtigni-an	Bgy
Pocto	Bgy
Port Lamon	Bgy
Roxas	Bgy
San Juan	Bgy
Sasa	Bgy
Tagasaka	Bgy
Talisay	Bgy
Tarusan	Bgy
Tidman	Bgy
Tiwi	Bgy
Benigno Aquino	Bgy
Zone II (Pob.)	Bgy
Zone III Maharlika (Pob.)	Bgy
Tagbobonga	Bgy
Lanuza	Mun
Agsam	Bgy
Bocawe	Bgy
Bunga	Bgy
Gamuton	Bgy
Habag	Bgy
Mampi	Bgy
Nurcia	Bgy
Sibahay	Bgy
Zone I (Pob.)	Bgy
Pakwan	Bgy
Zone II (Pob.)	Bgy
Zone III (Pob.)	Bgy
Zone IV (Pob.)	Bgy
Lianga	Mun
Anibongan	Bgy
Banahao	Bgy
Ban-as	Bgy
Baucawe	Bgy
Diatagon	Bgy
Ganayon	Bgy
Liatimco	Bgy
Manyayay	Bgy
Payasan	Bgy
Poblacion	Bgy
Saint Christine	Bgy
San Isidro	Bgy
San Pedro	Bgy
Lingig	Mun
Anibongan	Bgy
Barcelona	Bgy
Bongan	Bgy
Bogak	Bgy
Handamayan	Bgy
Mahayahay	Bgy
Mandus	Bgy
Mansa-ilao	Bgy
Pagtila-an	Bgy
Palo Alto	Bgy
Poblacion	Bgy
Rajah Cabungso-an	Bgy
Sabang	Bgy
Salvacion	Bgy
San Roque	Bgy
Tagpoporan	Bgy
Union	Bgy
Valencia	Bgy
Madrid	Mun
Bagsac	Bgy
Bayogo	Bgy
Magsaysay	Bgy
Manga	Bgy
Panayogon	Bgy
Patong Patong	Bgy
Quirino (Pob.)	Bgy
San Antonio	Bgy
San Juan	Bgy
San Roque	Bgy
San Vicente	Bgy
Songkit	Bgy
Union	Bgy
Linibonan	Bgy
Marihatag	Mun
Alegria	Bgy
Amontay	Bgy
Antipolo	Bgy
Arorogan	Bgy
Bayan	Bgy
Mahaba	Bgy
Mararag	Bgy
Poblacion	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Pedro	Bgy
Santa Cruz	Bgy
San Agustin	Mun
Bretania	Bgy
Buatong	Bgy
Buhisan	Bgy
Gata	Bgy
Hornasan	Bgy
Janipaan	Bgy
Kauswagan	Bgy
Oteiza	Bgy
Poblacion	Bgy
Pong-on	Bgy
Pongtod	Bgy
Salvacion	Bgy
Santo Niño	Bgy
San Miguel	Mun
Bagyang	Bgy
Baras	Bgy
Bitaugan	Bgy
Bolhoon	Bgy
Calatngan	Bgy
Carromata	Bgy
Castillo	Bgy
Libas Gua	Bgy
Libas Sud	Bgy
Magroyong	Bgy
Mahayag	Bgy
Patong	Bgy
Poblacion	Bgy
Sagbayan	Bgy
San Roque	Bgy
Siagao	Bgy
Umalag	Bgy
Tina	Bgy
Tagbina	Mun
Batunan	Bgy
Carpenito	Bgy
Kahayagan	Bgy
Lago	Bgy
Maglambing	Bgy
Maglatab	Bgy
Magsaysay	Bgy
Malixi	Bgy
Manambia	Bgy
Osmeña	Bgy
Poblacion	Bgy
Quezon	Bgy
San Vicente	Bgy
Santa Cruz	Bgy
Santa Fe	Bgy
Santa Juana	Bgy
Santa Maria	Bgy
Sayon	Bgy
Soriano	Bgy
Tagongon	Bgy
Trinidad	Bgy
Ugoban	Bgy
Villaverde	Bgy
Doña Carmen	Bgy
Hinagdanan	Bgy
Tago	Mun
Alba	Bgy
Anahao Bag-o	Bgy
Anahao Daan	Bgy
Badong	Bgy
Bajao	Bgy
Bangsud	Bgy
Cagdapao	Bgy
Camagong	Bgy
Caras-an	Bgy
Cayale	Bgy
Dayo-an	Bgy
Gamut	Bgy
Jubang	Bgy
Kinabigtasan	Bgy
Layog	Bgy
Lindoy	Bgy
Mercedes	Bgy
Purisima (Pob.)	Bgy
Sumo-sumo	Bgy
Umbay	Bgy
Unaban	Bgy
Unidos	Bgy
Victoria	Bgy
Cabangahan	Bgy
City of Tandag (Capital)	City
Awasian	Bgy
Bagong Lungsod (Pob.)	Bgy
Bioto	Bgy
Buenavista	Bgy
Bongtod Pob.	Bgy
Dagocdoc (Pob.)	Bgy
Mabua	Bgy
Mabuhay	Bgy
Maitum	Bgy
Maticdum	Bgy
Pandanon	Bgy
Pangi	Bgy
Quezon	Bgy
Rosario	Bgy
Salvacion	Bgy
San Agustin Norte	Bgy
San Agustin Sur	Bgy
San Antonio	Bgy
San Isidro	Bgy
San Jose	Bgy
Telaje	Bgy
Dinagat Islands	Prov
Basilisa	Mun
Catadman	Bgy
Columbus	Bgy
Coring	Bgy
Cortes	Bgy
Doña Helene	Bgy
Ferdinand	Bgy
Geotina	Bgy
Imee	Bgy
Melgar	Bgy
Montag	Bgy
Navarro	Bgy
Poblacion	Bgy
Puerto Princesa	Bgy
Rita Glenda	Bgy
Roxas	Bgy
Sering	Bgy
Tag-abaca	Bgy
Benglen	Bgy
Diegas	Bgy
Edera	Bgy
New Nazareth	Bgy
Roma	Bgy
Santa Monica	Bgy
Santo Niño	Bgy
Sombrado	Bgy
Villa Ecleo	Bgy
Villa Pantinople	Bgy
Cagdianao	Mun
Boa	Bgy
Cabunga-an	Bgy
Del Pilar	Bgy
Laguna	Bgy
Legaspi	Bgy
Ma-atas	Bgy
Nueva Estrella	Bgy
Poblacion	Bgy
San Jose	Bgy
Santa Rita	Bgy
Tigbao	Bgy
Valencia	Bgy
Mabini	Bgy
R. Ecleo, Sr.	Bgy
Dinagat	Mun
Cab-ilan	Bgy
Cabayawan	Bgy
Escolta (Pob.)	Bgy
Gomez	Bgy
Magsaysay	Bgy
Mauswagon (Pob.)	Bgy
White Beach (Pob.)	Bgy
Bagumbayan	Bgy
New Mabuhay	Bgy
Wadas	Bgy
Cayetano	Bgy
Justiniana Edera	Bgy
Libjo	Mun
Albor (Pob.)	Bgy
Arellano	Bgy
Bayanihan	Bgy
Doña Helen	Bgy
Garcia	Bgy
General Aguinaldo	Bgy
Kanihaan	Bgy
Magsaysay	Bgy
Osmeña	Bgy
Plaridel	Bgy
Quezon	Bgy
San Antonio (Pob.)	Bgy
San Jose	Bgy
Santo Niño	Bgy
Llamera	Bgy
Rosita	Bgy
Loreto	Mun
Carmen (Pob.)	Bgy
Esperanza	Bgy
Ferdinand	Bgy
Helene	Bgy
Liberty	Bgy
Magsaysay	Bgy
Panamaon	Bgy
San Juan (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
Santiago (Pob.)	Bgy
San Jose (Capital)	Mun
Aurelio	Bgy
Cuarinta	Bgy
Don Ruben Ecleo	Bgy
Jacquez	Bgy
Justiniana Edera	Bgy
Luna	Bgy
Mahayahay	Bgy
Matingbe	Bgy
San Jose (Pob.)	Bgy
San Juan	Bgy
Santa Cruz	Bgy
Wilson	Bgy
Tubajon	Mun
Imelda	Bgy
Mabini	Bgy
Malinao	Bgy
Navarro	Bgy
Diaz	Bgy
Roxas	Bgy
San Roque (Pob.)	Bgy
San Vicente (Pob.)	Bgy
Santa Cruz (Pob.)	Bgy
`.trim().split('\n').map((line) => line.split('\t')).map(([name, type]) => ({ name, type }));

let lastProvince = null;
let lastCity = null;

for (let i = 0; i < data.length; i++) {
  const item = data[i];

  if (item.type === 'Prov') {
    lastProvince = item.name;
  }

  if (item.type === 'Mun' || item.type === 'City') {
    const name = item.name
      .replace(/ \(Capital\)/g, '')
      .replace(/City of (.+)/g, '$1 City');

    lastCity = name;
  }

  if (item.type === 'Bgy' && lastProvince === 'Capiz') {
    console.log(`${item.name}\t${lastCity} | ${lastProvince}`);
  }
}