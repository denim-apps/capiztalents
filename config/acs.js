import { listRecords } from '../denim/airtable/data';
import { quoteValue } from '../denim/airtable/schema';
import { throwValidationError } from '../denim/secure-endpoints/validation-error';
import { invalidate } from '../utils/cacher';

/**
 * @param {import('../denim/airtable/schema-retriever').AirTable} tableSchema 
 * @param {string[]} [fields]
 * @param {string[]} [disallowedFields]
 */
const filterFields = (tableSchema, fields, disallowedFields) => {
  const allowedFields = tableSchema
    .columns.map(({ name }) => name)
    .filter((name) => name.indexOf('Record ID') === -1 && (!disallowedFields || !disallowedFields.includes(name)));

  if (fields) {
    return fields.filter((field) => allowedFields.includes(field));
  }

  return allowedFields;
};

/**
 * @param {import('../denim/lark/api').LarkUser} user 
 */
const getAccessIds = async (user) => {
  const aspirantRecords = [];
  const representativeSchools = [];
  const adminSchools = [];

  user.roles.forEach((role) => {
    if (role.approved) {
      if (role.type === 'aspirant') {
        aspirantRecords.push(role.recordId);
      }

      if (role.type === 'school representative') {
        representativeSchools.push(role.schoolId);
      }

      if (role.type === 'administrator') {
        adminSchools.push(role.schoolId);
      }
    }
  });

  return {
    aspirantRecords,
    representativeSchools,
    adminSchools,
  };
};

/**
 * @param {any} fields 
 * @param {string[]} allowed
 */
const filterInputFields = (fields, allowed) => {
  const newFields = {};

  Object.keys(fields).forEach((field) => {
    if (allowed.includes(field)) {
      newFields[field] = fields[field];
    }
  });

  return newFields;
};

const createVisbilityFilter = (adminSchools, representativeSchools, aspirantRecords, { aspirantIdField = 'RECORD_ID()', representativeFilter = '1' } = {}) => {
  return `OR(${adminSchools.map((schoolId) => `{School Record ID} = ${quoteValue(schoolId)}`)
    .concat(representativeSchools.map((schoolId) => `AND({School Record ID} = ${quoteValue(schoolId)}, ${representativeFilter})`))
    .concat(aspirantRecords.map((aspirantId) => `${aspirantIdField}=${quoteValue(aspirantId)}`))
    .join(', ')})`;
}

/**
 * @param {import("../denim/airtable/data").ListRecordsOptions} options
 * @param {import("../denim/airtable/schema-retriever").AirTable} tableSchema
 */
export const preList = async (options, tableSchema) => {
  if (options.user) {
    const { aspirantRecords, representativeSchools, adminSchools } = await getAccessIds(options.user);

    if (tableSchema.name === 'Profiles') {
      const profileFilter = createVisbilityFilter(adminSchools, representativeSchools, aspirantRecords, {
        representativeFilter: '{Role} = "Aspirant"',
      });
      options.filterByFormula = `AND(${profileFilter}, ${options.filterByFormula || '1'})`;
      options.fields = filterFields(tableSchema, options.fields);
    }

    if (tableSchema.name === 'Profile Details') {
      const profileFilter = createVisbilityFilter(adminSchools, representativeSchools, aspirantRecords, {
        aspirantIdField: '{Aspirant Record ID}',
      });
      options.filterByFormula = `AND(${profileFilter}, ${options.filterByFormula || '1'})`;
      options.fields = filterFields(tableSchema, options.fields);
    }

    if (tableSchema.name === 'Schools') {
      options.filterByFormula = options.filterByFormula?.replace(/\$unaffiliated/, 'AND(' + options.user.roles.map(({ schoolId }) => `RECORD_ID() != ${quoteValue(schoolId)}`).concat('1').join(', ') + ')');
    }

    if (tableSchema.name === 'Events' && options.filterByFormula?.indexOf('$unregistered') > -1) {
      const { records } = await listRecords('Profile Details', {
        fields: ['Event'],
        user: options.user,
        all: true,
      });

      options.filterByFormula = options.filterByFormula.replace(/\$unregistered/g, `AND(${records.map(({ fields: { Event } }) => Event[0]).map((id) => `RECORD_ID() != ${quoteValue(id)}`).concat('1').join(', ')})`);
    }
  }
};

/**
 * @param {import('../denim/airtable/data').AirTableRecord[]} recordsInput 
 * @param {boolean} isCreate 
 * @param {import('../denim/airtable/schema-retriever').AirTable} tableSchema 
 * @param {import('../denim/lark/api').LarkUser} executingUser
 */
export const preUpdate = async (recordsInput, isCreate, tableSchema, executingUser) => {
  if (executingUser) {
    const { aspirantRecords, representativeSchools, adminSchools } = await getAccessIds(executingUser);

    if (isCreate) {
      if (tableSchema.name === 'Profiles') {
        // Only allow this for school application.
        return recordsInput.map((record) => {
          return {
            fields: {
              School: record.fields.School,
              Intent: record.fields.Intent,
              Role: executingUser.role.record.fields.Role,
              User: executingUser.record.id,
            },
          };
        });
      }

      if (tableSchema.name === 'Profile Details') {
        const { records: events } = await listRecords('Events', {
          user: executingUser,
          all: true,
          fields: ['School'],
          ids: recordsInput.map(({ fields: { Event } }) => Event),
          filterByFormula: '$unregistered',
        });

        return events.map(({ fields: { School }, id }) => ({
          fields: {
            Event: id,
            Profile: executingUser.roles.find(({ schoolId }) => schoolId === School[0]).recordId,
          },
        }));
      }
    } else {
      if (tableSchema.name === 'Profiles') {
        const updateableIds = await listRecords(tableSchema.name, {
          all: true,
          ids: recordsInput.map(({ id }) => id),
          fields: ['Approved', 'Rejected', 'Role'],
          user: executingUser,
        });

        return recordsInput.filter(({ id }) => {
          const record = updateableIds.records.find(({ id: allowedId }) => id === allowedId);

          // Make sure they're in the set and either are the aspirant themselves or aren't yet approved/rejected.
          return record
            && (aspirantRecords.includes(id) || !(record.fields.Approved || record.fields.Rejected));
        }).map((input) => {
          const newFields = filterInputFields(
            input.fields,
            aspirantRecords.includes(input.id) ? ['Intent'] : ['Approved', 'Rejected'],
          );

          if (newFields.Approved) {
            newFields['Date Approved'] = new Date();
          }

          return {
            ...input,
            fields: newFields,
          };
        });
      }

      if (tableSchema.name === 'Profile Details') {
        const updateableIds = await listRecords(tableSchema.name, {
          all: true,
          ids: recordsInput.map(({ id }) => id),
          fields: ['School Record ID'],
          user: executingUser,
        });

        return recordsInput.filter(({ id }) => {
          const record = updateableIds.records.find(({ id: allowedId }) => id === allowedId);

          // Make sure they're in the set and either are the aspirant themselves or aren't yet approved/rejected.
          return [...representativeSchools, ...adminSchools].includes(record?.fields['School Record ID'][0]);
        }).map((input) => ({
          ...input,
          fields: filterInputFields(input.fields, ['Status']),
        }));
      }
    }

    if (tableSchema.name === 'Users') {
      // This endpoint is locked down by other means.
      return recordsInput;
    }
  } else {
    if (tableSchema.name === 'Users' || tableSchema.name === 'Profiles') {
      // This endpoint is locked down by other means.
      return recordsInput;
    }
  }

  // Anything else is read-only.
  throwValidationError('Readonly');
};

/**
 * @param {import('../denim/airtable/data').DeleteRecordsOptions} options
 * @param {import('../denim/airtable/schema-retriever').AirTable} tableSchema 
 */
export const preDelete = async (options, tableSchema) => {
  // Anything else is read-only.
  throwValidationError('Readonly');
};

/**
 * @param {import('../denim/airtable/schema-retriever').AirTable} tableSchema 
 * @param {import('../denim/lark/api').LarkUser} executingUser
 */
export const postUpdate = async (tableSchema, executingUser) => {
  if (tableSchema.name === 'Profiles' && executingUser) {
    await invalidate('user-roles', executingUser.user_record_email);
  }
};
