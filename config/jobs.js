export default {
  /**
   * @param {any} options 
   * @param {async (data: any) => void} updateState 
   */
  'test': async (options, updateState) => {
    updateState({
      state: 'STARTED',
      progress: 1,
      max: 7,
      options,
    });

    setTimeout(() => {
      updateState({
        progress: 2,
      });
    }, 500);

    setTimeout(() => {
      updateState({
        state: 'RUNNING',
        progress: 3,
      });  
    }, 1000);

    setTimeout(() => {
      updateState({
        progress: 4,
      });  
    }, 1500);

    setTimeout(() => {
      updateState({
        progress: 5,
      });  
    }, 2000);

    setTimeout(() => {
      updateState({
        progress: 6,
      });  
    }, 2500);

    setTimeout(() => {
      updateState({
        state: 'COMPLETED',
        progress: 7,
        __final: true,
      });  
    }, 3000);
  },
};
