/**
 * Converts a record from the dummy Users table into a Lark User.
 * 
 * @param {import("../denim/airtable/data").AirTableRecord} userRecord 
 * @returns {import("../denim/lark/api").LarkUser}
 */
const DummyAuth = async (userRecord) => {
  const user = {
    open_id: userRecord.id,
    name: userRecord.fields['Full Name'],
    en_name: userRecord.fields['Full Name'],
  };

  return user;
};

export default DummyAuth;
