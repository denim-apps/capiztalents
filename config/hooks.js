import { listRecords, retrieveRecord } from '../denim/airtable/data';
import { quoteValue } from '../denim/airtable/schema';
import { createCacher } from '../utils/cacher';
import { getUserRoles } from '../utils/get-user-roles';

// Hooks go here.
const retrieveUserRecord = createCacher('user-record', {
  retrieve: async (id) => {
    if (process.env.DUMMY_AUTH_TABLE) {
      return await retrieveRecord(process.env.DUMMY_AUTH_TABLE, id, {
        expand: ['Residence Barangay', 'Birth City'],
      });
    } else {
      const { records: [foundRecord] } = await listRecords(process.env.DUMMY_AUTH_TABLE, {
        filterByFormula: `{Lark ID} = ${quoteValue(id)}`,
        expand: ['Residence Barangay', 'Birth City'],
      });

      if (foundRecord) {
        userRecord = foundRecord;
      }
    }

    return null;
  },
});

export const postAuth = async (req, res) => {
  if (req.user) {
    const userRecord = await retrieveUserRecord(req.user.open_id);

    if (userRecord) {
      req.user.user_record_email = userRecord.fields['Email'];
      req.user.record = userRecord;

      const roles = await getUserRoles(userRecord.fields['Email']);
      req.user.roles = roles;

      const roleId = (req.req || req).session.get('role_id');

      if (roleId) {
        const role = roles.find(({ recordId, approved }) => recordId === roleId && approved);

        if (role) {
          req.user.role = role;
        }
      }
    }
  }
};
