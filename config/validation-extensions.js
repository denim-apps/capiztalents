import Yup from 'yup';

/**
 * @type {{
 *  [table: string]: {
 *    [column: string]: (validation: Yup.BaseSchema) => Yup.BaseSchema
 *  }
 * }}
 */
export default {
  'Table Name': {
    'Column Name': (validation) => {
      return validation.required(true).nullable(false);
    },
  },
  'Users': {
    'Email': (validation) => {
      return validation.required().nullable(false);
    },
    'First Name': (validation) => {
      return validation.required().nullable(false);
    },
    'Middle Name': (validation) => {
      return validation.required().nullable(false);
    },
    'Last Name': (validation) => {
      return validation.required().nullable(false);
    },
    'Mobile No': (validation) => {
      return validation.required().nullable(false);
    },
    'Birth Date': (validation) => {
      return validation.required().nullable(false);
    },
    'Birth City': (validation) => {
      return validation.required().nullable(false);
    },
    'Birth Province': (validation) => {
      return validation.required().nullable(false);
    },
    'Residence Barangay': (validation) => {
      return validation.required().nullable(false);
    },
    'Registration School': (validation) => {
      return validation.required().nullable(false);
    },
  },
  'Profiles': {
    'School': (validation) => {
      return validation.required().nullable(false);
    },
  },
  'Applications': {
    'School': (validation) => {
      return validation.required().nullable(false);
    },
    'Course': (validation) => {
      return validation.required().nullable(false);
    },
  },
};
