import withAuth, { requireAuth } from '../denim/auth';
import withSession from '../denim/session';

export const rolePage = (handler, role) => withSession(
  withAuth(
    requireAuth(
      async (req, res) => {
        const roleMatch = new RegExp(role);

        if (!req.user.role || (role && !roleMatch.exec(role))) {
          if (res) {
            return res.status(403).end();
          }

          return {
            redirect: {
              destination: '/',
            },
          };
        }

        const response = await handler(req, res);

        return {
          ...response,
          props: {
            ...(response?.props || { }),
            user: req.user,
          },
        };
      },
    )
  )
);
