import { notification } from 'antd';

export const notifyErrors = (errors) => {
  notification.error({
    message: errors.length > 1
      ? (errors.map(({ message }) => `- ${message}`).join('\n'))
      : errors[0].message,
  });
};
