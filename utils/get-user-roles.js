import { listRecords } from '../denim/airtable/data';
import { quoteValue } from '../denim/airtable/schema';
import { createCacher } from './cacher';

const retrieveUserRoles = createCacher('user-roles', {
  retrieve: async (email) => {
    // Retrieve aspirants.
    const { records } = await listRecords('Profiles', {
      filterByFormula: `{User} = ${quoteValue(email)}`,
      expand: ['School'],
    });
  
    return records.map((record) => ({
      type: record.fields['Role'].toLowerCase(),
      recordId: record.id,
      school: record.children['School'][0].fields['School Name'],
      schoolId: record.fields['School'][0],
      roleText: record.fields['Role'],
      approved: record.fields['Approved'] || false,
      record,
    }));
  },
});

export const getUserRoles = retrieveUserRoles;
