/**
 * @param {RequestInfo} input
 * @param {RequestInit} [init]
 * @returns 
 */
export const jsonFetch = async (input, init) => {
  const response = await fetch(input, {
    ...init,
    headers: {
      ...(init.headers || { }),
      'Content-Type': 'application/json',
    },
    body: init.body ? JSON.stringify(init.body) : init.body,
  });

  return await response.json();
};
