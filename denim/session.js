import { withIronSession } from 'next-iron-session';
import AppConfig from '../config/app';

/**
 * @param {*} handler 
 * @returns {(...args: any[]) => Promise<any>}
 */
const withSession = (handler) => withIronSession(handler, {
  password: AppConfig.session_cookie_password,
  cookieName: AppConfig.session_cookie_name,
  cookieOptions: {
    secure: process.env.NODE_ENV === 'production',
  },
});

export default withSession;
