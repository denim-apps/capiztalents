import { useCallback, useEffect, useMemo, useState } from 'react';
import { CheckOutlined } from '@ant-design/icons';
import { Rate, Tag } from 'antd';
import moment from 'moment';
import AirTableClient from '../airtable/airtable-client';
import { findTableSchema } from '../airtable/schema';
import numeral from 'numeral';
import { getAirTableNumberFormat } from '../airtable/validation';
import AirTableForeignKeyRenderer from './AirTableForeignKeyRenderer';

/**
 * @param {string} table 
 * @param {string[]} columns
 * @param {import('../airtable/data').ListRecordsOptions} options
 * @param {{
 *  records: import('../airtable/data').AirTableRecord[],
 *  offset?: string,
 * }} prefetch
 */
export const useAirTableCollection = (table, columns, options, prefetch) => {
  const [innerPrefetch, setInnerPrefetch] = useState(prefetch);
  const [records, setRecords] = useState([]);
  const [isRetrieving, setIsRetrieving] = useState(false);
  const [offset, setOffset] = useState(null);

  const tableSchema = useMemo(() => {
    return findTableSchema(table);
  }, [table]);

  const retrieveMore = useCallback((cancelCheck) => {
    setIsRetrieving(true);

    (async () => {
      const { records, offset: newOffset } = await AirTableClient.list(table, {
        ...(options || {}),
        offset: offset || undefined,
      });

      if (!cancelCheck || typeof(cancelCheck) !== 'function' || !cancelCheck()) {
        setRecords((r) => r.concat(...records));
        setOffset(newOffset);
        setIsRetrieving(false);
      }
    })();
  }, [tableSchema, isRetrieving, options, columns, offset]);

  const refresh = useCallback((cancelCheck = () => false) => {
    setRecords([]);

    if (innerPrefetch) {
      setRecords(innerPrefetch.records);
      setOffset(innerPrefetch.offset);
      setInnerPrefetch(null);
    } else {
      retrieveMore(cancelCheck);
    }
  }, [retrieveMore, innerPrefetch]);

  useEffect(() => {
    let cancelled = false;
    refresh(() => cancelled);

    return () => {
      cancelled = true;
    };
  }, [table, columns, options]);

  const mappedColumns = useMemo(() => {
    return columns.map((column) => {
      const columnSchema = tableSchema.columns.find(({ name, id }) => name === column || id === column);
      let otherOptions = {};

      if (columnSchema) {
        if (columnSchema.type === 'checkbox') {
          otherOptions.render = (value, record, index) => {
            return value ? (<CheckOutlined />) : null;
          };
        } else if (columnSchema.type === 'multiSelect' || columnSchema.type === 'select') {
          otherOptions.render = (value) => {
            const values = value ? (Array.isArray(value) ? value : [value]) : [];

            return values.map((value) => (
              <Tag key={value} color={Object.values(columnSchema.typeOptions.choices || {}).find(({ name }) => name === value)?.color}>
                {value}
              </Tag>
            ));
          };
        } else if (columnSchema.type === 'date') {
          otherOptions.render = (value) => {
            if (value) {
              return columnSchema.typeOptions.isDateTime ? moment(value).format('l LT') : moment(value).format('l');
            }

            return value;
          };
        } else if (columnSchema.type === 'number' || (columnSchema.type === 'formula' && columnSchema.typeOptions.resultType === 'number')) {
          const numberFormat = getAirTableNumberFormat(columnSchema);

          otherOptions.render = (value) => {
            if (typeof (value) === 'number') {
              return numeral(value).format(numberFormat);
            }

            return value;
          };
        } else if (columnSchema.type === 'rating') {
          const max = columnSchema.typeOptions?.max;

          otherOptions.render = (value) => {
            return (
              <Rate
                value={value}
                count={max}
                disabled
              />
            );
          };
        } else if (columnSchema.type === 'foreignKey') {
          otherOptions.render = (value, record) => {
            return (
              <AirTableForeignKeyRenderer
                column={columnSchema}
                value={value}
                recordChildren={record.recordChildren?.[columnSchema.name]}
              />
            );
          };
        }
      }

      return {
        key: columnSchema?.name || column,
        title: columnSchema?.name || column,
        dataIndex: columnSchema?.name || column,
        ...otherOptions,
      };
    });
  }, [tableSchema, columns]);

  return {
    records: useMemo(() => {
      return records.map((originalRecord) => {
        const { fields, children, ...record } = originalRecord;

        return {
          key: originalRecord.id,
          ...record,
          recordChildren: children,
          ...fields,
          originalRecord,
        };
      });
    }, [records]),
    hasMore: !!offset,
    retrieveMore,
    isRetrieving,
    columns: mappedColumns,
    refresh,
  };
};
