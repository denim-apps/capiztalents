const fs = require('fs');
const readline = require('readline');
const { retrieveAirtableSchema } = require('./schema-retriever');
const path = require('path');

const [, , script, ...args] = process.argv;

(async () => {
  if (script === 'refresh') {
    console.log('Refreshing cache...');

    if (!fs.existsSync('.atcache.json')) {
      console.log('No existing cache found. Have you already run `schema init`?');
      return;
    }

    const { username, password, bases } = require(path.join(process.cwd(), '.atcache.json'));

    const result = await retrieveAirtableSchema(username, password, ...Object.keys(bases));
    fs.writeFileSync('.atcache.json', JSON.stringify({
      username,
      password,
      ...result,
    }, null, '  '));
    console.log('AirTable schema cache rebuilt.');
    return;
  } else if (script === 'delete') {
    if (!fs.existsSync('.atcache.json')) {
      console.log('No existing cache found. Have you already run `schema init`?');
      return;
    }

    const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

    rl.question('Are you sure? [Y/n] ', (answer) => {
      if (answer.toLowerCase() === 'y') {
        console.log('Deleting cache...');
        fs.unlinkSync('.atcache.json');
      }

      rl.close();
    });

    return;
  } else if (script === 'init') {
    const [username, password, ...bases] = args;

    if (username && password) {
      let result = await retrieveAirtableSchema(username, password, ...bases);
      
      if (!bases.length) {
        console.log('Please select which bases to include.');
        console.log('Enter numbers separated by spaces (i.e. 1 2 65 7), or ? and a string of text to perform a text match (i.e. ?somesearch)');
        console.log('');

        Object.keys(result.bases).forEach((base, i) => {
          console.log(`[${i + 1}] [${result.bases[base].category}] ${result.bases[base].name}`);
        });

        console.log('');

        const answer = await new Promise((resolve) => {
          const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

          rl.question('Bases: ', (answer) => {
            resolve(answer);
            rl.close();
          });
        });

        let baseIds = [];
    
        if (answer[0] === '?') {
          baseIds = Object.keys(result.bases).filter((base) => {
            return result.bases[base].name.toLowerCase().indexOf(answer.substring(1).toLowerCase()) > -1;
          });
        } else {
          const indices = answer.split(' ').map(Number);

          baseIds = Object.keys(result.bases).filter((id, index) => {
            return indices.includes(index + 1);
          });
        }

        if (baseIds.length) {
          console.log('You have selected:');
          console.log(baseIds.map((base) => `- [${result.bases[base].category}] ${result.bases[base].name}`).join('\n'));
          result = await retrieveAirtableSchema(username, password, ...baseIds);
        } else {
          console.log('No bases selected.');
          return;
        }
      }

      fs.writeFileSync('.atcache.json', JSON.stringify({
        username,
        password,
        ...result,
      }, null, '  '));
      console.log('AirTable schema cache built.');
      return;
    }
  }

  console.log(`
schema [command] [...args]

    $ schema init [username] [password] [...base IDs]
    Initializes the AirTable schema cache.

    Example:
      schema init my@email.com mypassword appAAAAAAAAAAAAAA appAAAAAAAAAAAAAA

    $ schema refresh
    Refreshes the existing schema cache. Must be executed after a successful init call.

    Example:
      schema refresh

    $ schema delete
    Deletes the existing schema cache.

    Example:
      schema delete
    `.trim());
})();
