import { ExclamationCircleOutlined, SearchOutlined } from '@ant-design/icons';
import { Input, Modal, notification } from 'antd';
import NProgress from 'nprogress';
import { useEffect, useMemo, useState } from 'react';
import { jsonFetch } from '../../utils/json-fetch';
import AirTableClient from './airtable-client';
import { findTableSchema, quoteValue } from './schema';

const AirTableFormHelpers = {
  /**
   * @param {string} table
   * @param {{
   *  redirectUrl: string,
   *  router: any,
   *  secureEndpoint: string,
   *  callback: (record: import('./data').AirTableRecord) => void,
   * }} options
   */
  submitCreateHandler: (table, options) => async (values, actions) => {
    NProgress.start();

    const createdRecord = options.secureEndpoint
      ? await jsonFetch(options.secureEndpoint, {
        method: 'POST',
        body: {
          fields: values,
        },
      })
      : await AirTableClient.create(table, {
        fields: values,
      });

    if (createdRecord.errors) {
      notification.error({
        message: createdRecord.errors.length > 1
          ? (createdRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
          : createdRecord.errors[0].message,
      });
    } else {
      if (options.redirectUrl) {
        options.router.push(options.redirectUrl.replace(/:id/g, createdRecord.id));
      }

      if (options.callback) {
        await options.callback(createdRecord, actions);
      }
    }

    NProgress.done();
  },
  /**
   * @param {string} table
   * @param {{
   *  currentRecord: import('./data').AirTableRecord,
   *  setCurrentRecord: (record: import('./data').AirTableRecord) => void,
   *  callback: (record: import('./data').AirTableRecord) => void,
   *  secureEndpoint: string,
   * }} options
   */
  submitUpdateHandler: (table, options = { }) => async (values, actions) => {
    NProgress.start();

    const updatedRecord = options.secureEndpoint
      ? await jsonFetch(options.secureEndpoint.replace(/:id/g, options.currentRecord.id), {
        method: 'PATCH',
        body: {
          id: options.currentRecord.id,
          fields: AirTableClient.diffValues(options.currentRecord, values),
        },
      })
      : await AirTableClient.update(table, {
        id: options.currentRecord.id,
        fields: AirTableClient.diffValues(options.currentRecord, values),
      });

    if (updatedRecord.errors) {
      notification.error({
        message: updatedRecord.errors.length > 1
          ? (updatedRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
          : updatedRecord.errors[0].message,
      });
    } else {
      if (options.callback) {
        await options.callback(updatedRecord, actions);
      }

      if (options.setCurrentRecord) {
        options.setCurrentRecord(updatedRecord);
      }
    }

    NProgress.done();

    return updatedRecord.fields;
  },
  /**
   * @param {string} table 
   * @param {string} id 
   * @param {{
   *  postDelete: ({
   *    deleted: boolean;
   *    id: string;
   *  }[]) => any,
   *  title: string,
   *  description: string,
   *  yesText: string,
   *  noText: string,
   *  onCancel: () => any,
   * }} param2
   */
  deleteRecord: (
    table,
    id,
    {
      postDelete = () => { },
      title = `Are you sure you want to delete this record?`,
      description = 'This record will be permanently deleted.',
      yesText = 'Yes',
      noText = 'No',
      onCancel,
      deleteCallback = (() => AirTableClient.destroy(table, { ids: [id] })),
    } = {},
  ) => {
    Modal.confirm({
      title,
      icon: <ExclamationCircleOutlined />,
      content: description,
      okText: yesText,
      okType: 'danger',
      cancelText: noText,
      onOk: async () => {
        const result = await deleteCallback();
        await postDelete(result);
      },
      onCancel,
    });
  },
  /**
   * @param {{
   *  table: string,
   *  column: string,
   *  formula: string,
   * }} options 
   * @param {import('./data').ListRecordsOptions} listOptions 
   */
  useSearchControl: (options, listOptions) => {
    const tableSchema = useMemo(() => findTableSchema(options.table), [options.table]);
    const columnName = useMemo(() => {
      if (tableSchema) {
        return tableSchema.columns[0].name;
      }

      if (options.column) {
        return options.column;
      }

      return null;
    }, [tableSchema, options.column]);
    const searchFormula = useMemo(() => {
      if (options.formula) {
        return options.formula;
      }

      return `SEARCH(LOWER($search), LOWER({${columnName || 'Name'}}))>0`;
    }, [columnName, options.formula]);
    const [pendingSearch, setPendingSearch] = useState('');
    const [search, setSearch] = useState('');

    const finalOptions = useMemo(() => {
      const opts = {
        ...(listOptions || {}),
      };

      if (search.length) {
        opts.filterByFormula = searchFormula.replace('$search', quoteValue(search));
      }

      return opts;
    }, [searchFormula, search, listOptions]);

    useEffect(() => {
      const id = setTimeout(() => {
        setSearch(pendingSearch);
      }, 300);

      return () => {
        clearTimeout(id);
      };
    }, [pendingSearch]);

    return {
      search: pendingSearch,
      setSearch: setPendingSearch,
      renderField: () => (
        <Input
          placeholder="Type here to search..."
          suffix={
            <SearchOutlined style={{ color: 'rgba(0,0,0,.45)' }} />
          }
          value={pendingSearch}
          onChange={(e) => {
            setPendingSearch(e.target.value);
          }}
        />
      ),
      finalOptions,
    };
  },
};

export default AirTableFormHelpers;
