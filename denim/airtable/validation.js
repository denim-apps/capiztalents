import dayjs from 'dayjs';
import * as Yup from 'yup';
import validationExtensions from '../../config/validation-extensions';

export const getAirTableNumberFormat = (column) => {
  if (column.typeOptions?.format === 'decimal') {
    return `0,000.${new Array(column.typeOptions?.precision || 0)
      .fill('0')
      .join('')}`;
  }

  if (column.typeOptions?.format === 'duration') {
    return `0:00:00`;
  }

  if (column.typeOptions?.format === 'percentV2') {
    return `0,000.${new Array(column.typeOptions?.precision || 0)
      .fill('0')
      .join('')}%`;
  }

  if (column.typeOptions?.format === 'currency') {
    return `${column.typeOptions.symbol || '$'}0,000.${new Array(
      column.typeOptions?.precision || 0
    )
      .fill('0')
      .join('')}`;
  }

  return null;
};

/**
 * @param {import('./schema-retriever').AirTableColumn} column 
 * @returns {Yup.BaseSchema}
 */
export const createValidatorForAirTableColumn = (column) => {
  if (column.type === 'text' || column.type === 'multilineText') {
    const validator = Yup.string();

    if (column.typeOptions?.validatorName === 'email') {
      return validator.email();
    }

    if (column.typeOptions?.validatorName === 'url') {
      return validator.url();
    }

    return validator;
  }

  if (column.type === 'phone') {
    return Yup.string().matches(/^\+\d+$/g, '${label} must be a valid phone number in international format, and without spaces (e.x. +639485009483 or +14085995049)');
  }

  if (column.type === 'select') {
    return Yup.string().oneOf(
      Object.values(column.typeOptions.choices).map(({ name }) => name)
    );
  }

  if (column.type === 'multiSelect') {
    return Yup.array(
      Yup.string().oneOf(
        Object.values(column.typeOptions.choices).map(({ name }) => name)
      )
    );
  }

  if (column.type === 'checkbox') {
    return Yup.boolean();
  }

  if (column.type === 'date') {
    if (column.typeOptions?.isDateTime) {
      return Yup.string()
        .nullable(true)
        .transform(function (value) {
          if (!value) {
            return null;
          }

          // First validate that this is a date.
          const parsed = dayjs(value);

          if (parsed.isValid()) {
            return parsed.toISOString();
          }

          return new Date('');
        })
        .typeError('Please enter a valid date and time.');
    } else {
      return Yup.string()
        .nullable(true)
        .transform(function (value) {
          if (!value) {
            return null;
          }

          // First validate that this is a date.
          const parsed = dayjs(value);

          if (parsed.isValid()) {
            return parsed.format('YYYY-MM-DD');
          }

          return new Date('');
        })
        .typeError('Please enter a valid date.');
    }
  }

  if (column.type === 'number' || column.type === 'rating') {
    return Yup.number();
  }

  if (column.type === 'foreignKey') {
    return Yup.array(Yup.string()).transform(function (value, originalValue) {
      if (!value) {
        if (typeof(originalValue) === 'string') {
          return [originalValue];
        }
      }

      return value;
    });
  }
};

/**
 * @param {import("./schema-retriever").AirTable} schema 
 * @returns {import("../validation/record-validation").RecordValidationShape}
 */
export const createValidationShapeFromSchema = (schema) => {
  const shape = {};

  schema.columns.forEach((column) => {
    const columnValidation = createValidatorForAirTableColumn(column);

    if (columnValidation) {
      shape[column.name] = columnValidation.nullable(true).label(column.name);
      const tableExtensions = validationExtensions[schema.name];

      if (tableExtensions) {
        const columnExtensions = tableExtensions[column.name];

        if (columnExtensions) {
          shape[column.name] = columnExtensions(shape[column.name]);
        }
      }
    }
  });

  return shape;
};
