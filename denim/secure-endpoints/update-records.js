import { updateRecords } from '../airtable/data';

/**
 * @param {{
 *  preUpdate: (table: string, input: import('../airtable/data').AirTableRecord | import('../airtable/data').AirTableRecord[], isCreate: boolean, user: import('../lark/api').LarkUser, req: Request) => Promise<[
 *    string,
 *    import('../airtable/data').AirTableRecord | import('../airtable/data').AirTableRecord[],
 *    boolean,
 *    import('../lark/api').LarkUser
 *  ]>,
 *  postUpdate: (records: import('../airtable/data').AirTableRecord | import('../airtable/data').AirTableRecord[]) => Promise<import('../airtable/data').AirTableRecord | import('../airtable/data').AirTableRecord[]>
 * }} param0 
 * @returns {(req: Request, res: Response) => Promise<void>}
 */
export const secureUpdateRecordsHandler = ({
  preUpdate = async (table, input, isCreate, user, req) => [table, input, isCreate, user],
  postUpdate = async (records) => records,
}) => async (req, res) => {
  try {
    const [table, input, isCreate, user] = await preUpdate(
      req.query.table,
      {
        ...req.body,
      },
      req.method === 'POST',
      req.user,
      req,
    );

    const response = await updateRecords(table, input, isCreate, user);
    const records = await postUpdate(response, req, res);

    return res.send(records);
  } catch (e) {
    if (e.isValidationError) {
      return res.status(422).send({
        errors: e.validationErrors,
      });
    }

    throw e;
  }
};
