import { listRecords } from '../airtable/data';

/**
 * @param {{
 *  preRetrieve: (table: string, options: import('../airtable/data').ListRecordsOptions, req: Request) => Promise<[string, import('../airtable/data').ListRecordsOptions]>,
 *  postRetrieve: (response: {
 *    records: import('../airtable/data').AirTableRecord[],
 *    offset?: string,
 *  }, req: Request, res: Response) => Promise<{
 *    records: import('../airtable/data').AirTableRecord[],
 *    offset?: string,
 * }>,
 * }} param0 
 * @returns 
 */
export const secureListRecordsHandler = ({
  preRetrieve = async (table, options, req) => [table, options],
  postRetrieve = async (response, req, res) => response,
}) => async (req, res) => {
  try {
    const [table, options] = await preRetrieve(
      req.query.table,
      {
        ...req.query,
        user: req.user,
      },
      req,
    );

    const response = await listRecords(table, options);
    const records = await postRetrieve(response, req, res);

    return res.send(records);
  } catch (e) {
    if (e.isValidationError) {
      return res.status(422).send({
        errors: e.validationErrors,
      });
    }

    throw e;
  }
};
