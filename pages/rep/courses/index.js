import { notification, Tag, Table, Descriptions, Button } from 'antd';
import { SubmitButton } from 'formik-antd';
import { useMemo, useState } from 'react';
import AppLayout from '../../../components/layout';
import AirTableClient from '../../../denim/airtable/airtable-client';
import AirTableFormHelpers from '../../../denim/airtable/form-helpers';
import AirTableField from '../../../denim/components/AirTableField';
import AirTableForm from '../../../denim/components/AirTableForm';
import { useAirTableCollection } from '../../../denim/components/useAirTableCollection';
import { rolePage } from '../../../utils/role-page';

export const getServerSideProps = rolePage(
  async (ctx) => {
    return {
      props: {},
    };
  },
  /(?:administrator|representative)/,
);

const Courses = ({
  user,
}) => {
  const {
    records,
    columns,
    isRetrieving,
    refresh,
  } = useAirTableCollection(
    'Profile Details',
    useMemo(() => [
      'Status',
    ], []),
    useMemo(() => ({
      all: true,
      expand: [
        'Profile',
        'Event',
      ],
    }), [])
  );

  return (
    <AppLayout
      title="Event Status"
      breadcrumb={[
        {
          label: 'Event Status',
          key: 'courses',
        }
      ]}
      menuItemKey="courses"
    >
      <Table
        dataSource={records}
        columns={[
          {
            key: 'Aspirant',
            title: 'Aspirant',
            dataIndex: 'Aspirant',
            render: (value, record) => {
              return (
                <>
                  {record.recordChildren?.['Profile']?.[0]?.fields['Full Name']}
                  &nbsp;<Tag>{record.recordChildren?.['Profile']?.[0]?.fields['Email']}</Tag>
                </>
              );
            },
          },
          {
            key: 'Event',
            title: 'Event',
            dataIndex: 'Event',
            render: (value, record) => {
              return (
                <>
                  {record.recordChildren?.['Event']?.[0]?.fields['Course Name']}
                  &nbsp;<Tag>{record.recordChildren?.['Event']?.[0]?.fields['Course ID']}</Tag>
                </>
              );
            },
          },
          ...columns,
        ]}
        loading={isRetrieving}
        pagination={false}
        style={{ marginBottom: 16 }}
        expandable={{
          expandedRowRender: (row) => {
            return (
              <AirTableForm
                table="Profile Details"
                existingRecord={row.originalRecord}
                onSubmit={AirTableFormHelpers.submitUpdateHandler('Profile Details', {
                  currentRecord: row.originalRecord,
                  setCurrentRecord: () => { },
                  callback: () => refresh(),
                })}
              >
                <AirTableField
                  columnName="Status"
                />
                <div style={{ textAlign: 'right' }}>
                  <SubmitButton>Update</SubmitButton>
                </div>
              </AirTableForm>
            );
          },
          expandedRowClassName: () => 'aspirant-details',
        }}
      />
    </AppLayout>
  );
};

export default Courses;
