import { notification, Tag, Table, Descriptions, Button, Space } from 'antd';
import { useMemo, useState } from 'react';
import AppLayout from '../../../components/layout';
import AirTableClient from '../../../denim/airtable/airtable-client';
import { quoteValue } from '../../../denim/airtable/schema';
import { useAirTableCollection } from '../../../denim/components/useAirTableCollection';
import { rolePage } from '../../../utils/role-page';

export const getServerSideProps = rolePage(
  async (ctx) => {
    return {
      props: {},
    };
  },
  /(?:administrator|representative)/,
);

const Applications = ({
  user,
}) => {
  const {
    records,
    columns,
    isRetrieving,
    refresh,
  } = useAirTableCollection(
    'Profiles',
    useMemo(() => [], []),
    useMemo(() => ({
      all: true,
      filterByFormula: `AND({Role}="Aspirant", {School Record ID}=${quoteValue(user.role.schoolId)}, {Rejected} = 0, {Approved} = 0)`,
      expand: ['User', 'User.Residence Barangay'],
    }), [])
  );
  const [loading, setLoading] = useState(false);
  const [selectedIds, setSelectedIds] = useState([]);

  return (
    <AppLayout
      title="School Applications"
      breadcrumb={[
        {
          label: 'School Applications',
          key: 'applications',
        }
      ]}
      menuItemKey="applications"
    >
      {selectedIds.length > 0 ? (
        <Space style={{ marginBottom: 16 }}>
          <span>{selectedIds.length} selected</span>
          <Button
            type="primary"
            onClick={async () => {
              try {
                setLoading(true);
                const updatedRecord = await AirTableClient.update('Profiles', selectedIds.map((id) => ({
                  id,
                  fields: {
                    Approved: true,
                  },
                })));

                if (updatedRecord.errors) {
                  notification.error({
                    message: createdRecord.errors.length > 1
                      ? (createdRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
                      : createdRecord.errors[0].message,
                  });
                }

                refresh();
                setSelectedIds([]);
              } catch (e) {
                console.error(e);
              }

              setLoading(false);
            }}
            loading={loading}
          >
            Approve
          </Button>
          <Button
            onClick={async () => {
              try {
                setLoading(true);
                const updatedRecord = await AirTableClient.update('Profiles', selectedIds.map((id) => ({
                  id,
                  fields: {
                    Rejected: true,
                  },
                })));

                if (updatedRecord.errors) {
                  notification.error({
                    message: createdRecord.errors.length > 1
                      ? (createdRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
                      : createdRecord.errors[0].message,
                  });
                }

                refresh();
                setSelectedIds([]);
              } catch (e) {
                console.error(e);
              }

              setLoading(false);
            }}
            loading={loading}
          >
            Reject
          </Button>
        </Space>
      ) : null}
      <Table
        dataSource={records}
        columns={[
          {
            key: 'Aspirant',
            title: 'Aspirant',
            dataIndex: 'Aspirant',
            render: (value, record) => {
              return (
                <>
                  {record['Full Name']}
                  &nbsp;<Tag>{record['Email']}</Tag>
                </>
              );
            },
          },
          ...columns,
        ]}
        rowSelection={{
          type: 'checkbox',
          onChange: setSelectedIds,
          selectedRowKeys: selectedIds,
        }}
        loading={isRetrieving}
        pagination={false}
        style={{ marginBottom: 16 }}
        expandable={{
          expandedRowRender: (record) => {
            const aspirant = record;

            return (
              <div>
                <Descriptions title="Aspirant Info" bordered>
                  <Descriptions.Item label="Full Name">
                    {aspirant['Full Name']}
                  </Descriptions.Item>
                  <Descriptions.Item label="Email">
                    {aspirant['Email']}
                  </Descriptions.Item>
                  <Descriptions.Item label="Mobile No">
                    {aspirant['Mobile No']}
                  </Descriptions.Item>
                  <Descriptions.Item label="Intent">
                    {aspirant['Intent'].map((intent) => (<Tag>{intent}</Tag>))}
                  </Descriptions.Item>
                  <Descriptions.Item label="Birth Date">
                    {aspirant.recordChildren['User'][0].fields['Birth Date']}
                  </Descriptions.Item>
                  <Descriptions.Item label="Birth City">
                    {aspirant.recordChildren['User'][0].fields['Birth City']}
                  </Descriptions.Item>
                  <Descriptions.Item label="Residence">
                    {aspirant.recordChildren['User'][0].fields['Residence Street'] ? `${aspirant.recordChildren['User'][0].fields['Residence Street']}, ` : ''}{aspirant.recordChildren['User'][0].children['Residence Barangay'][0].fields['Barangay']}
                  </Descriptions.Item>
                </Descriptions>
                <div style={{ marginTop: 16, textAlign: 'right' }}>
                  <Button
                    type="primary"
                    onClick={async () => {
                      try {
                        setLoading(true);
                        const updatedRecord = await AirTableClient.update('Profiles', {
                          id: record.id,
                          fields: {
                            Approved: true,
                            'Date Approved': new Date(),
                          },
                        });

                        if (updatedRecord.errors) {
                          notification.error({
                            message: createdRecord.errors.length > 1
                              ? (createdRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
                              : createdRecord.errors[0].message,
                          });
                        }

                        refresh();
                      } catch (e) {
                        console.error(e);
                      }

                      setLoading(false);
                    }}
                    loading={loading}
                  >
                    Approve
                  </Button>
                  &nbsp;
                  <Button
                    onClick={async () => {
                      try {
                        setLoading(true);
                        const updatedRecord = await AirTableClient.update('Profiles', {
                          id: record.id,
                          fields: {
                            Rejected: true,
                          },
                        });

                        if (updatedRecord.errors) {
                          notification.error({
                            message: createdRecord.errors.length > 1
                              ? (createdRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
                              : createdRecord.errors[0].message,
                          });
                        }

                        refresh();
                      } catch (e) {
                        console.error(e);
                      }

                      setLoading(false);
                    }}
                    loading={loading}
                  >
                    Reject
                  </Button>
                </div>
              </div>
            );
          },
          expandedRowClassName: () => 'aspirant-details',
        }}
      />
    </AppLayout>
  );
};

export default Applications;
