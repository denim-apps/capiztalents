import { Card, Space } from 'antd';
import { useState } from 'react';
import ApplicationForm from '../../components/application-form';
import AppLayout from '../../components/layout';
import ProfileForm from '../../components/profile-form';
import { rolePage } from '../../utils/role-page';

export const getServerSideProps = rolePage(
  async (ctx) => {
    return {
      props: {},
    };
  },
  /school representative|administrator/g,
);

const Profile = ({
  user,
}) => {
  const [record, setRecord] = useState(user.record);

  return (
    <AppLayout
      title="Representative Profile"
      breadcrumb={[
        {
          label: 'Representative Profile',
          key: 'profile',
        }
      ]}
      menuItemKey="profile"
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        <ProfileForm record={record} setRecord={setRecord} />
        <Card title="Apply to New School">
          <ApplicationForm user={user} />
        </Card>
      </Space>
    </AppLayout>
  );
};

export default Profile;
