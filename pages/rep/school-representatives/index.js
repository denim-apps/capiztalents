import { Table, Button } from 'antd';
import { CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { useMemo, useState } from 'react';
import AppLayout from '../../../components/layout';
import { quoteValue } from '../../../denim/airtable/schema';
import { useAirTableCollection } from '../../../denim/components/useAirTableCollection';
import { rolePage } from '../../../utils/role-page';
import AirTableFormHelpers from '../../../denim/airtable/form-helpers';
import AirTableClient from '../../../denim/airtable/airtable-client';

export const getServerSideProps = rolePage(
  async (ctx) => {
    return {
      props: {},
    };
  },
  /(?:administrator)/,
);

const SchoolRepresentatives = ({
  user,
}) => {
  const {
    records,
    columns,
    isRetrieving,
    refresh,
  } = useAirTableCollection(
    'Profiles',
    useMemo(() => ['Email', 'Full Name', 'Role', 'Date Approved'], []),
    useMemo(() => ({
      all: true,
      filterByFormula: `AND({Role}!="Aspirant", {School Record ID}=${quoteValue(user.role.schoolId)}, {Rejected} = 0)`,
    }), [])
  );
  const [loading, setLoading] = useState(false);

  return (
    <AppLayout
      title="School Representatives"
      breadcrumb={[
        {
          label: 'School Representatives',
          key: 'applications',
        }
      ]}
      menuItemKey="school-representatives"
    >
      <Table
        dataSource={records}
        columns={[
          ...columns,
          {
            title: 'Actions',
            render: (value, record) => {
              return (
                <div style={{ textAlign: 'right' }}>
                  {!record.Approved ? (
                    <Button
                      shape="circle"
                      icon={<CheckOutlined />}
                      type="primary"
                      onClick={async () => {
                        try {
                          setLoading(true);
                          const updatedRecord = await AirTableClient.update('Profiles', {
                            id: record.id,
                            fields: {
                              Approved: true,
                              'Date Approved': new Date(),
                            },
                          });

                          if (updatedRecord.errors) {
                            notification.error({
                              message: createdRecord.errors.length > 1
                                ? (createdRecord.errors.map(({ message }) => `- ${message}`).join('\n'))
                                : createdRecord.errors[0].message,
                            });
                          }

                          refresh();
                        } catch (e) {
                          console.error(e);
                        }

                        setLoading(false);
                      }}
                      loading={loading}
                    />
                  ) : null}
                  &nbsp;
                  <Button
                    shape="circle"
                    icon={<CloseOutlined />}
                    type="danger"
                    onClick={() => AirTableFormHelpers.deleteRecord('Profiles', record.id, {
                      deleteCallback: () => AirTableClient.update('Profiles', {
                        id: record.id,
                        fields: {
                          Rejected: true,
                        },
                      }),
                      postDelete: refresh,
                      title: 'Are you sure you want to reject this representative?',
                      description: `This representative${record.Approved ? '' : `'s request`} will be permanently removed.`,
                    })}
                  />
                </div>
              );
            },
          },
        ]}
        loading={isRetrieving}
        pagination={false}
        style={{ marginBottom: 16 }}
      />
    </AppLayout>
  );
};

export default SchoolRepresentatives;
