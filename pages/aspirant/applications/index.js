import { Card, Tag, Table, Space } from 'antd';
import { useFormikContext } from 'formik';
import { SubmitButton } from 'formik-antd';
import { useMemo } from 'react';
import AppLayout from '../../../components/layout';
import AirTableFormHelpers from '../../../denim/airtable/form-helpers';
import { quoteValue } from '../../../denim/airtable/schema';
import AirTableField from '../../../denim/components/AirTableField';
import AirTableForm from '../../../denim/components/AirTableForm';
import { useAirTableCollection } from '../../../denim/components/useAirTableCollection';
import { rolePage } from '../../../utils/role-page';

export const getServerSideProps = rolePage(
  async (ctx) => {
    return {
      props: {},
    };
  },
  'aspirant'
);

const EventList = ({
  schoolId,
  user,
}) => {
  const {
    records,
    columns: hookColumns,
    isRetrieving,
    refresh,
  } = useAirTableCollection(
    'Profile Details',
    useMemo(() => ['Event', 'Status'], []),
    useMemo(() => ({
      all: true,
      filterByFormula: `AND({Profile}=${quoteValue(user.user_record_email)}, {School Record ID}=${quoteValue(schoolId)})`,
      expand: ['Event'],
    }), [])
  );

  const columns = useMemo(() => {
    return hookColumns.map((column) => {
      if (column.key === 'Event') {
        return {
          ...column,
          render: (value, record) => {
            const course = record.recordChildren?.Event?.[0];

            return (
              <>
                {course?.fields['Course Name']}
                &nbsp;
                <Tag>
                  {course?.fields['Course ID']}
                </Tag>
                <Tag>
                  {course?.fields['Course Type']}
                </Tag>
              </>
            );
          },
        };
      }

      return column;
    });
  }, [hookColumns]);

  return (
    <>
      <Table
        dataSource={records}
        columns={columns}
        loading={isRetrieving}
        pagination={false}
        style={{ marginBottom: 16 }}
      />
      <Card title="Apply to Event">
        <AirTableForm
          layout="horizontal"
          table="Profile Details"
          innerFormProps={{
            labelCol: { span: 4 },
            wrapperCol: { span: 20 },
          }}
          onSubmit={AirTableFormHelpers.submitCreateHandler('Profile Details', {
            callback: (record, actions) => {
              actions.resetForm();
              refresh();
            },
          })}
        >
          <EventField schoolId={schoolId} />
          <div style={{ textAlign: 'right' }}>
            <SubmitButton>Apply</SubmitButton>
          </div>
        </AirTableForm>
      </Card>
    </>
  );
};

const Applications = ({
  user,
}) => {
  return (
    <AppLayout
      title="Event Status"
      breadcrumb={[
        {
          label: 'Event Status',
          key: 'applications',
        }
      ]}
      menuItemKey="applications"
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        {user.roles.filter(({ approved }) => approved).map(({ schoolId, school, record }) => (
          <Card title={school} key={record.id}>
            <EventList
              schoolId={schoolId}
              user={user}
            />
          </Card>
        ))}
      </Space>
    </AppLayout>
  );
};

const EventField = ({
  schoolId,
}) => {
  return (
    <AirTableField
      columnName="Event"
      lookupFilterFormula={`AND( {School Record ID}=${quoteValue(schoolId)}, SEARCH(LOWER($search), LOWER({Search})) > 0, $unregistered )`}
      lookupField={(record) => {
        return (
          <>
            {record.fields['Course Name']}
            &nbsp;
            <Tag>
              {record.fields['Course ID']}
            </Tag>
            <Tag>
              {record.fields['Course Type']}
            </Tag>
          </>
        );
      }}
    />
  );
};

export default Applications;
