import { Card, Space } from 'antd';
import { useState } from 'react';
import ApplicationForm from '../../components/application-form';
import AppLayout from '../../components/layout';
import ProfileForm from '../../components/profile-form';
import { rolePage } from '../../utils/role-page';

export const getServerSideProps = rolePage(
  async (ctx) => {
    return {
      props: {},
    };
  },
  'aspirant'
);

const Profile = ({
  user,
}) => {
  const [record, setRecord] = useState(user.record);

  return (
    <AppLayout
      title="Aspirant Profile"
      breadcrumb={[
        {
          label: 'Aspirant Profile',
          key: 'profile',
        }
      ]}
      menuItemKey="profile"
    >
      <Space direction="vertical" style={{ width: '100%' }}>
        <ProfileForm record={record} setRecord={setRecord} />
        <Card title="Apply to New School">
          <ApplicationForm user={user} isAspirant />
        </Card>
      </Space>
    </AppLayout>
  );
};

export default Profile;
