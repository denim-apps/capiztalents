import { Card, Col, Row } from 'antd';
import Title from 'antd/lib/typography/Title';
import { ProfileOutlined } from '@ant-design/icons';
import Link from 'next/link';
import withAuth from '../denim/auth';
import withSession from '../denim/session';

export const getServerSideProps = withSession(
  withAuth(
    async (ctx) => {
      if (ctx.user) {
        if (ctx.user.role) {
          if (ctx.user.role.type === 'aspirant') {
            return {
              redirect: {
                destination: '/aspirant/profile',
              },
            };
          }

          return {
            redirect: {
              destination: '/rep/approvals',
            },
          };
        }

        return {
          redirect: {
            destination: '/auth/picker',
          },
        };
      }

      return {
        props: {},
      };
    },
  )
);

const Home = () => {
  return (
    <div className="home-content">
      <Title>Welcome to CapizTalents!</Title>
      <Row gutter="16">
        <Col span="8">
          <Card
            title="Aspirant"
            actions={[
              <Link href="/register/aspirant">
                <a><ProfileOutlined /> Register</a>
              </Link>
            ]}
          >
            I want to register for schools and courses.
          </Card>
        </Col>
        <Col span="8">
          <Card
            title="School Rep"
            actions={[
              <Link href="/register/school">
                <a><ProfileOutlined /> Register</a>
              </Link>
            ]}
          >
            I want to register to represent my school.
          </Card>
        </Col>
        <Col span="8">
          <Card
            title="Existing User"
            actions={[
              <Link href="/api/auth/login">
                <a><ProfileOutlined /> Login</a>
              </Link>
            ]}
          >
            I already have an account.
          </Card>
        </Col>
      </Row>
    </div>
  );
};

export default Home;
