import { Card, Tag } from 'antd';
import { SubmitButton } from 'formik-antd';
import * as Yup from 'yup';
import withAuth from '../../denim/auth';
import withSession from '../../denim/session';
import AirTableForm from '../../denim/components/AirTableForm';
import AirTableField from '../../denim/components/AirTableField';
import { useRouter } from 'next/dist/client/router';
import AirTableFormHelpers from '../../denim/airtable/form-helpers';
import AirTableSchemaProvider from '../../denim/components/AirTableSchemaProvider';
import SchoolLookupField from '../../components/school-lookup-field';

export const getServerSideProps = withSession(
  withAuth(
    async (ctx) => {
      if (ctx.user) {
        return {
          redirect: {
            destination: '/',
          },
        };
      }

      if (!['aspirant', 'school'].includes(ctx.query.type)) {
        return {
          notFound: true,
        };
      }

      return {
        props: {
          type: ctx.query.type,
        },
      };
    },
  )
);

const titles = {
  aspirant: 'Register as an Aspirant',
  school: 'Register as a School Representative',
};

const intents = {
  aspirant: 'Aspirant',
  school: 'School Representative',
};

const RegisterPage = ({
  type,
}) => {
  const router = useRouter();

  return (
    <div className="home-content">
      <Card title={titles[type]}>
        <AirTableForm
          table="Users"
          layout="horizontal"
          innerFormProps={{
            labelCol: { span: 4 },
            wrapperCol: { span: 20 },
          }}
          onSubmit={AirTableFormHelpers.submitCreateHandler('Users', {
            secureEndpoint: '/api/auth/register?intent=' + encodeURIComponent(intents[type]),
            router,
            redirectUrl: '/',
          })}
          additionalValidation={type === 'aspirant' ? {
            Intent: Yup.array(Yup.string()).required().nullable(false).min(1, 'Please select at least one option.'),
          } : { }}
        >
          <AirTableField columnName="Email" />
          <AirTableField columnName="Mobile No" />
          <AirTableField columnName="First Name" />
          <AirTableField columnName="Middle Name" />
          <AirTableField columnName="Last Name" />
          <AirTableField columnName="Birth Date" />
          <AirTableField columnName="Birth City" placeholder="Enter 'Out of Province' if not within Capiz" />
          <AirTableField columnName="Residence Street" />
          <AirTableField columnName="Residence Barangay" />
          <SchoolLookupField columnName="Registration School" />
          {type === 'aspirant' ? (
            <AirTableSchemaProvider table="Profiles">
              <AirTableField
                columnName="Intent"
                multiSelectAsCheckboxes
              />
            </AirTableSchemaProvider>
          ) : null}
          <div style={{ textAlign: 'right' }}>
            <SubmitButton>Register</SubmitButton>
          </div>
        </AirTableForm>
      </Card>
    </div>
  );
};

export default RegisterPage;
