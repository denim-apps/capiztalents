// pages/_app.js
import '../components/app.css';
import '../components/antd.css';
import Router from 'next/router';
import NProgress from 'nprogress';
import UserProvider from '../denim/components/UserProvider';

Router.onRouteChangeStart = () => {
  NProgress.start();
};

Router.onRouteChangeComplete = () => {
  NProgress.done();
};

Router.onRouteChangeError = () => {
  NProgress.done();
};

const App = ({ Component, pageProps }) => {
  return (
    <UserProvider user={pageProps.user}>
      <Component {...pageProps} />
    </UserProvider>
  );
}

export default App;
