import withAuth from '../../../../denim/auth';
import withSession from '../../../../denim/session';
import { getUserRoles } from '../../../../utils/get-user-roles';

const handler = withSession(
  withAuth(
    async (req, res) => {
      const roles = await getUserRoles(req.user.user_record_email);
      const role = roles.find(({ recordId }) => recordId === req.query.id);
      const redirectUrl = req.session.get('selection_redirect_url');

      if (!role) {
        return res.redirect('/auth/picker');
      }

      req.session.set('role_id', req.query.id);
      await req.session.save();

      if (redirectUrl) {
        req.session.unset('selection_redirect_url');
        await req.session.save();
        return res.redirect(redirectUrl);
      }

      return res.redirect('/');
    }
  )
);

export default handler;
