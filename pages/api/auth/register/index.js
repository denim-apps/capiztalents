import { listRecords, updateRecords } from '../../../../denim/airtable/data';
import { quoteValue } from '../../../../denim/airtable/schema';
import { secureUpdateRecordsHandler } from '../../../../denim/secure-endpoints/update-records';
import { throwValidationError } from '../../../../denim/secure-endpoints/validation-error';
import withSession from '../../../../denim/session';
import { invalidate } from '../../../../utils/cacher';

const handler = withSession(
  secureUpdateRecordsHandler({
    preUpdate: async (table, input, isCreate, user, req) => {
      if (Array.isArray(input)) {
        throw new Error('Unsupported.');
      }

      req.intent = input.fields['Intent'];

      Object.keys(input.fields).forEach((key) => {
        if (!['Email', 'Mobile No', 'First Name', 'Middle Name', 'Last Name', 'Birth Date', 'Birth City', 'Birth Province', 'Residence Street', 'Residence Barangay', 'Registration School'].includes(key)) {
          delete input.fields[key];
        }
      });

      input.fields['Registration Intent'] = req.query.intent;

      const { records: existing } = await listRecords('Users', {
        filterByFormula: `{Email}=${quoteValue(input.fields.Email)}`,
        maxRecords: 1,
        fields: ['ID'],
      });
  
      if (existing.length) {
        throwValidationError('Email already exists');
      }
  
      return ['Users', input, true];
    },
    postUpdate: async (record, req) => {
      // Only one record is expected.
      if (!process.env.DUMMY_AUTH_TABLE) {
        // STUB: create Lark ID and save it to session
      }

      const { id, fields } = record;

      await updateRecords('Profiles', {
        fields: {
          User: [id],
          Intent: req.intent || [],
          Role: fields['Registration Intent'],
          School: fields['Registration School'],
        },
      }, true);

      await invalidate('user-roles', fields['Email']);
  
      if (process.env.DUMMY_AUTH_TABLE) {
        req.session.set('lark_user_id', record.id);
        await req.session.save();
      }

      return {
        status: 'OK',
      };
    },
  })
);

export default handler;
