import { updateRecords } from '../../../../denim/airtable/data';
import withAuth, { requireAuth } from '../../../../denim/auth';
import withSession from '../../../../denim/session';
import { invalidate } from '../../../../utils/cacher';

const handler = withSession(
  withAuth(
    requireAuth(
      async (req, res) => {
        Object.keys(req.body).forEach((key) => {
          if (!['Intent', 'Birth Date', 'Birth City', 'Birth Province', 'Residence Street', 'Residence Barangay', 'School'].includes(key)) {
            delete req.body[key];
          }
        });

        try {
          await invalidate('user-roles', req.user.record.fields['Email']);
          await updateRecords('Aspirants', {
            fields: {
              ...req.body,
              User: [req.user.record.id],
            },
          }, true);
        } catch (e) {
          if (e.isValidationError) {
            return res.status(422).send({
              errors: e.validationErrors,
            });
          }

          throw e;
        }

        res.send({
          status: 'OK',
        });
      },
    )
  )
);

export default handler;
