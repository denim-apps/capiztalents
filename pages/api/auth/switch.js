import withSession from '../../../denim/session';

const handler = withSession(async (req, res) => {
  req.session.unset('role_id');
  await req.session.save();

  return res.redirect('/auth/picker');
});

export default handler;
