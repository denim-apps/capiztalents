import { secureUpdateRecordsHandler } from '../../../denim/secure-endpoints/update-records';
import { rolePage } from '../../../utils/role-page';

const handler = rolePage(
  secureUpdateRecordsHandler({
    preUpdate: async (table, input, isCreate, user, req) => {
      if (Array.isArray(input)) {
        throw new Error('Unsupported');
      }

      Object.keys(input.fields).forEach((key) => {
        if (!['First Name', 'Middle Name', 'Last Name', 'Mobile No', 'Birth Date', 'Birth City', 'Residence Street', 'Residence Barangay'].includes(key)) {
          delete input.fields[key];
        }
      });

      return ['Users', {
        id: req.user.record.id,
        fields: input.fields,
      }, false, user];
    },
  }),
  'aspirant',
);

export default handler;
