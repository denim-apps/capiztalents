import { Button, Card, Col, Row, Tag } from 'antd';
import Title from 'antd/lib/typography/Title';
import Link from 'next/link';
import withAuth, { requireAuth } from '../../denim/auth';
import withSession from '../../denim/session';

export const getServerSideProps = withSession(
  withAuth(
    requireAuth(
      async (ctx) => {
        if (ctx.user.role) {
          return {
            redirect: {
              destination: '/',
            },
          };
        }

        if ((ctx.user.roles.length === 1 || (!ctx.user.roles.map(({ type }) => type).includes('administrator') && !ctx.user.roles.map(({ type }) => type).includes('school representative'))) && ctx.user.roles[0].approved) {
          return {
            redirect: {
              destination: '/api/auth/select/' + ctx.user.roles[0].recordId,
            },
          };
        }

        return {
          props: {
            roles: ctx.user.roles,
            user: ctx.user,
          },
        };
      },
    )
  )
);

const AuthPicker = ({
  roles,
  user,
}) => {
  return (
    <div className="home-content">
      {roles.map((role) => {
        const content = (
          <Card>
            <Row gutter="16">
              <Col>
                <img src="//placehold.it/70x70" />
              </Col>
              <Col>
                <Title level={4}>{role.school}</Title>
                <Tag>{role.roleText}</Tag>
                {!role.approved ? (
                  <Tag color="orange">Awaiting Approval</Tag>
                ) : null}
              </Col>
            </Row>
          </Card>
        );

        return (
          <Row gutter="16" key={role.recordId} style={{ marginBottom: '12px' }}>
            <Col span="24">
              {role.approved ? (
                <Link href={`/api/auth/select/${role.recordId}`}>
                  <a>
                    {content}
                  </a>
                </Link>
              ) : (
                content
              )}
            </Col>
          </Row>
        );
      })}
      <Button href="/api/auth/logout" type="primary">Log Out</Button>
    </div>
  );
};

export default AuthPicker;
