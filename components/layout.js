import React from 'react';
import Head from 'next/head';
import Link from 'next/link';
import { Layout, Menu, Breadcrumb, Avatar, Row, Col, Tag, Dropdown } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import AppConfig from '../config/app';
import AirTableCacheProvider from '../denim/components/AirTableCache';
import { useUser } from '../denim/components/UserProvider';
import useBreakpoint from 'antd/lib/grid/hooks/useBreakpoint';

const { Header, Content } = Layout;

/**
 * @type {React.FunctionComponent<{
 *  menuItemKey: string,
 *  breadcrumb: { label: string, href: string, key: string }[],
 *  title: string,
 * }>}
 */
const AppLayout = ({
  breadcrumb,
  menuItemKey,
  children,
  title,
}) => {
  const breakpoints = useBreakpoint();
  const { user } = useUser();

  return (
    <AirTableCacheProvider>
      <Layout className="layout">
        <Head>
          <title>{title ? `${title} - ` : ''}{AppConfig.name}</title>
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        </Head>
        <Header>
          <Row>
            {breakpoints.md ? (
              <Col>
                <div className="app-logo">
                  {AppConfig.name}
                </div>
              </Col>
            ) : null}
            <Col flex="auto">
              <Menu theme="dark" mode="horizontal" selectedKeys={[menuItemKey]}>
                {user.role.type === 'aspirant' ? (
                  <>
                    <Menu.Item key="profile">
                      <Link href="/aspirant/profile">
                        <a>Profile</a>
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="applications">
                      <Link href="/aspirant/applications">
                        <a>Event Status</a>
                      </Link>
                    </Menu.Item>
                  </>
                ) : (
                  <>
                    <Menu.Item key="applications">
                      <Link href="/rep/approvals">
                        <a>School Applications</a>
                      </Link>
                    </Menu.Item>
                    <Menu.Item key="courses">
                      <Link href="/rep/courses">
                        <a>Event Status</a>
                      </Link>
                    </Menu.Item>
                    {user.role.type === 'administrator' ? (
                      <Menu.Item key="school-representatives">
                        <Link href="/rep/school-representatives">
                          <a>School Representatives</a>
                        </Link>
                      </Menu.Item>
                    ) : null}
                    <Menu.Item key="profile">
                      <Link href="/rep/profile">
                        <a>Profile</a>
                      </Link>
                    </Menu.Item>
                  </>
                )}
              </Menu>
            </Col>
            <Dropdown
              placement="bottomRight"
              arrow
              overlay={
                <Menu>
                  {user.role.school ? (
                    <Menu.Item>
                      {user.role.school}<br />
                      <Tag>{user.role.roleText}</Tag>
                    </Menu.Item>
                  ) : null}
                  {user.roles.length > 1 ? (
                    <Menu.Item>
                      <Link href="/api/auth/switch">
                        <a>Switch Account</a>
                      </Link>
                    </Menu.Item>
                  ) : null}
                  <Menu.Item>
                    <Link href="/api/auth/logout">
                      <a>Log Out</a>
                    </Link>
                  </Menu.Item>
                </Menu>
              }
            >
              <Col>
                <Row gutter={16}>
                  <Col>
                    <Avatar>
                      {user.name[0]}
                    </Avatar>
                  </Col>
                  {breakpoints.md ? (
                    <Col style={{ color: 'white' }}>
                      <Row>
                        <Col>
                          <b>{user.name}</b>
                        </Col>
                      </Row>
                    </Col>
                  ) : null}
                </Row>
              </Col>
            </Dropdown>
          </Row>
        </Header>
        <Layout>
          <Layout style={{ padding: '0 24px 24px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>
                {AppConfig.name}
              </Breadcrumb.Item>
              {breadcrumb.map(({ label, href, key }) => {
                return (
                  <Breadcrumb.Item key={key}>
                    {href ? (
                      <Link href={href}>
                        <a>{label}</a>
                      </Link>
                    ) : (
                      label
                    )}
                  </Breadcrumb.Item>
                );
              })}
            </Breadcrumb>
            <Content className="main-content">
              {children}
            </Content>
          </Layout>
        </Layout>
      </Layout>
    </AirTableCacheProvider>
  );
};

export default AppLayout;
