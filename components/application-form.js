import { Card, notification, Space, Tag } from 'antd';
import { SubmitButton } from 'formik-antd';
import { useRouter } from 'next/dist/client/router';
import * as Yup from 'yup';
import SchoolLookupField from '../components/school-lookup-field';
import AirTableFormHelpers from '../denim/airtable/form-helpers';
import AirTableField from '../denim/components/AirTableField';
import AirTableForm from '../denim/components/AirTableForm';

const ApplicationForm = ({
  isAspirant,
  user,
}) => {
  const router = useRouter();

  return (
    <Space style={{ width: '100%' }} direction="vertical">
      <Card title="You are a member of the following schools:">
        {user.roles.filter(({ approved }) => approved).map(({ recordId, school }) => (<Tag key={recordId}>{school}</Tag>))}
      </Card>
      <AirTableForm
        table="Profiles"
        onSubmit={AirTableFormHelpers.submitCreateHandler('Profiles', {
          callback: (record, actions) => {
            actions.resetForm();

            notification.success({
              message: 'Your application has been received.',
            });

            router.reload(window.location.pathname);
          },
        })}
        additionalValidation={isAspirant ? {
          Intent: Yup.array(Yup.string()).required().nullable(false).min(1, 'Please select at least one option.'),
        } : {}}
      >
        <SchoolLookupField columnName="School" filterFormula="$unaffiliated" />
        {isAspirant ? (
          <AirTableField columnName="Intent" multiSelectAsCheckboxes />
        ) : null}
        <div style={{ textAlign: 'right' }}>
          <SubmitButton>Apply</SubmitButton>
        </div>
      </AirTableForm>
    </Space>
  );
};

export default ApplicationForm;
