import { Tag } from 'antd';
import AirTableField from '../denim/components/AirTableField';

const SchoolLookupField = ({
  filterFormula = '',
  ...props
}) => {
  return (
    <AirTableField
      {...props}
      lookupField={(record) => {
        return (
          <>
            {record.fields['School Name']}
            &nbsp;
            <Tag>
              {record.fields['School ID']}
            </Tag>
            <Tag>
              {record.fields['School Type']}
            </Tag>
          </>
        );
      }}
      label="School"
      lookupFilterFormula={`AND(OR(SEARCH(LOWER($search), LOWER({School ID}))>0, SEARCH(LOWER($search), LOWER({School Name}))>0), ${filterFormula || '1'})`}
    />
  );
};

export default SchoolLookupField;
