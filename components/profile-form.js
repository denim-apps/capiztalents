import { SubmitButton } from 'formik-antd';
import AirTableFormHelpers from '../denim/airtable/form-helpers';
import AirTableField from '../denim/components/AirTableField';
import AirTableForm from '../denim/components/AirTableForm';

const ProfileForm = ({
  record,
  setRecord,
  intent,
}) => {
  return (
    <AirTableForm
      layout="horizontal"
      table="Users"
      existingRecord={record}
      innerFormProps={{
        labelCol: { span: 4 },
        wrapperCol: { span: 20 },
      }}
      onSubmit={AirTableFormHelpers.submitUpdateHandler('Users', {
        secureEndpoint: '/api/users/update',
        currentRecord: record,
        setCurrentRecord: setRecord,
      })}
    >
      <AirTableField columnName="First Name" />
      <AirTableField columnName="Middle Name" />
      <AirTableField columnName="Last Name" />
      <AirTableField columnName="Mobile No" />
      <AirTableField columnName="Birth Date" />
      <AirTableField columnName="Birth City" />
      <AirTableField columnName="Residence Street" />
      <AirTableField columnName="Residence Barangay" />
      <div style={{ textAlign: 'right' }}>
        <SubmitButton>Save</SubmitButton>
      </div>
    </AirTableForm>
  );
};

export default ProfileForm;
